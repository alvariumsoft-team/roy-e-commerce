<?php

// Report all PHP errors
error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);


if (!defined('BASE_URL')) {
  define('BASE_URL', ($_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['SCRIPT_NAME']));
}
if (!defined('API_KEY')) {
  define('API_KEY', 'cdb98ac50d658f1d0f11d3e1524676e3a18f33f7');
}

if (!defined('API_URL')) {
  define('API_URL', 'https://testapi.multisafepay.com/v1/json/'); //test is https://testapi.multisafepay.com/v1/json/, for live environment use https://api.multisafepay.com/v1/json/
}

if (!defined('TOOLKIT_VERSION')) {
  define('TOOLKIT_VERSION', '2.0.3');
}
?>