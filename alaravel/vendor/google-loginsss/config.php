<?php
/*
 * Basic Site Settings and API Configuration
 */

// Database configuration
define('DB_HOST', 'MySQL_Database_Host');
define('DB_USERNAME', 'MySQL_Database_Username');
define('DB_PASSWORD', 'MySQL_Database_Password');
define('DB_NAME', 'MySQL_Database_Name');
define('DB_USER_TBL', 'users');

// Google API configuration
define('GOOGLE_CLIENT_ID', '618944127990-524e36q6klirgip7utodlik7onac609p.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'lygIINhhNThCVeBxBu4aBWSE');
define('GOOGLE_REDIRECT_URL', 'http://inviolateinfotech.com/google-login');

// Start session
if(!session_id()){
    session_start();
}

// Include Google API client library
require_once 'vendor/autoload.php';


// Call Google API
$gClient = new Google_Client();
$gClient->setApplicationName('Login to CodexWorld.com');
$gClient->setClientId(GOOGLE_CLIENT_ID);
$gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
$gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
//$gClient->setScopes(Google_Service_Gmail::GMAIL_READONLY);
$gClient->addScope("https://www.googleapis.com/auth/userinfo.profile");
$gClient->addScope("https://www.googleapis.com/auth/userinfo.email");
$gClient->addScope("https://www.googleapis.com/auth/plus.login");
//$gClient->setAuthConfig('credentials.json');

$google_oauthV2 = new Google_Service_Oauth2($gClient);