<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}


	public function render($view)
    {
       $category=null; $subcategory=null; $product=null;
    	$cat = null;
		$subcat = null;
		$prod = null;
		$data = array();
		$categories = Category::where('parentid', '=', 0)->get();

		foreach($categories as $k=>$sbcat){
			$categories[$k]->subcategories = Category::where('parentid', '=', $sbcat->id)->get();
		}

		// if(($category != null) && ($subcategory != null) && ($product != null)){
		// 	foreach($categories as $k=>$sbcat){
		// 		if($categories[$k]->slug == $category){
		// 			$cat = $categories[$k];
		// 			foreach($categories[$k]->subcategories as $kk=>$sub){
		// 				if($sub->slug == $subcategory){
		// 					$subcat = $sub;
		// 				}
		// 			}
		// 		}
		// 	}
		//   }
			
				//$data = $this->get_product_data($subcat, $product);
				$data['categories'] = $categories;
				$data['currentcat'] = $cat;
				$data['currentsubcat'] = $subcat;
				$data['currentproduct'] = $prod;

				$categories = $categories;
				$currentcat = $cat;
				$currentsubcat = $subcat;
				$currentproduct = $prod;
			
        //dd($view);
        return View::make($view, array('data'=>$data,'categories'=>$categories,'currentcat'=>$currentcat,'currentsubcat'=>$currentsubcat,'currentproduct'=>$currentproduct));	
        
    }

}