<?php
		
class ProductsController extends BaseController {

	public function getEdit($id){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$product=Products::where('id', '=', $id)->whereIn('type', array('simple', 'variable'))->get();
		if(!isset($product[0])){
			return Redirect::to('/wlproducts')->with('message-failure', 'Product not available!');
		}
		$product = $product[0];
		$metas = Productmetas::where('product_id', '=', $product->id)->get();
		if($product->type == 'simple'){
			if(isset($metas[0])){
				foreach($metas as $kk=>$v){
					$product->{$v->meta_key}=$v->meta_value;
				}
			}
		}
		else{
			$pp=Products::where('parent', '=', $id)->whereIn('type', array('variation'))->get();
			$ppids = array();
			$pmetas = array();
			foreach($pp as $ppp ){
				$ppids[] = $ppp->id;
			}
			
			$metas = Productmetas::whereIn('product_id', $ppids)->get();
			
			if(isset($metas[0])){
				foreach($metas as $kk=>$v){
					$pmetas[$v->product_id][$v->meta_key] = $v->meta_value;
				}
			}
			
			$product->metas = $pmetas;
		}
		$cat = Category::where('parentid', '=', $product->parentcatid)->get();
		$propcat  = ProductProperties::where('category_id', '=', $product->catid)->get();
		$parent = Category::where('parentid', '=', 0)->get();
		$parentcat=[];
		foreach($parent as $vl){
			$parentcat[$vl->id]=$vl->categoryname;
		} 
		$prop  = ProductProperties::all();
		$properties = array();
		foreach($prop as $p){
			$vals = ProductPropertyValues::where('property_id', '=', $p->id)->get();
			$properties[] = array('property'=>$p, 'values'=>$vals); 
		}
		//echo '<pre>';print_r($product);die;
		return View::make('webshop/edit_product', array( 'page'=>'webshop', 'parentcat'=>$parentcat, 'properties'=>$properties, 'product'=>$product, 'cat'=>$cat, 'prop'=>$propcat));
	}


	public function postEdit($id){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$product=Products::where('id', '=', $id)->whereIn('type', array('simple', 'variable'))->get();
		if(!isset($product[0])){
			return Redirect::to('/wlproducts')->with('message-failure', 'Product not available!');
		}
		$product = $product[0];
		/*echo '<pre>';
		print_r(Input::all());
		die;*/
		
		$parentcategory=Input::get('parentcategory');
		$category = Input::get('category');
		$name = Input::get('product_name');
		$description = Input::get('description');
		$composition = Input::get('composition');
		$delivery_return = Input::get('delivery_return');
		$payment = Input::get('payment');
		$warranty = Input::get('warranty');
		$product_type = Input::get('product_type');
		
		if($product->type == 'simple'){
			Productmetas::where('product_id', '=', $product->id)->delete();
		}
		else{
			$pp=Products::where('parent', '=', $id)->whereIn('type', array('variation'))->get();
			$ppids = array();
			$pmetas = array();
			foreach($pp as $ppp ){
				$ppids[] = $ppp->id;
			}
			Productmetas::whereIn('product_id', $ppids)->delete();
			Products::whereIn('id', $ppids)->delete();
		}
		
		$product->parentcatid = $parentcategory;
		$product->parent = 0;
		$product->catid = $category;
		$product->name = $name;
		$product->description = $description;
		$product->composition = $composition;
		$product->delivery_return = $delivery_return;
		$product->payment = $payment;
		$product->warranty = $warranty;
		$product->type = $product_type;
		
		if(Input::hasFile('feature_image')){
			$file = Input::file('feature_image'); 
			$dest = 'public/webshop/products'; 
			$ret = $this->save_file($dest,$file);
			if($ret != null){$product->img = $ret;}
		}

		if($product_type == 'simple'){
			$sale_price = Input::get('simple_sale_price');
			$product->price = $sale_price;
		}

		$p_price = [];

		$product->save();
		
		if(Input::hasFile('product_images')){
			$dest = 'public/webshop/products';
			$fimages = Input::file('product_images'); 
			foreach($fimages as $fi){
				$ret = $this->save_file($dest,$fi);
				if($ret != null){
					$productimg = new Productimages;
					$productimg->productid=$product->id;
					$productimg->imagename=$ret;
					$productimg->save();
				}
			}
		}
		
		if($product_type == 'simple'){
			$properties = Input::get('property');
			if(isset($properties['regular_price'])){
				Products::where('id', '=',$product->id)->update(array('price'=>$properties['regular_price']));
			}
			if(isset($properties['quantity'])){
				Products::where('id', '=',$product->id)->update(array('quantity'=>$properties['quantity']));
			}
			$pp = array();
			foreach($properties as $k=>$v){
				$k = '_'.strtolower($k);
				$pp[] = array('product_id'=>$product->id, 'meta_key'=>$k, 'meta_value'=>$v);
			}
			if(!empty($pp)){
				Productmetas::insert($pp);
			}
			return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');
		}
		else if($product_type == 'variable'){
			$variations = array();
			$properties = Input::get('variation_property');
			$pp = array();
			$p_price = 0;
			$p_q = 0;
			foreach($properties as $kk=>$vv){
				$vproduct = new Products;
				$vproduct->parentcatid = $product->parentcatid;
				$vproduct->catid = $product->catid;
				$vproduct->name = $product->id.'#'.$product->name;
				$vproduct->description = $product->name.'-'.$product->description;
				$vproduct->type = 'variation';
				$vproduct->parent = $product->id;
				$vproduct->save();
				$fimages = Input::file('variation_image'); 
				if(is_array($fimages)){
					
					$dest = 'public/webshop/products';
					if(!empty($fimages[$kk])){
						foreach($fimages[$kk] as $i=>$fi){
							$ret = $this->save_file($dest,$fi);
							if($ret != null){
								$productimg = new Productimages;
								$productimg->productid=$vproduct->id;
								$productimg->imagename=$ret;
								$productimg->save();
								if($i == 0){
									Products::where('id', '=',$vproduct->id)->update(array('img'=>$productimg->imagename));
								}
							}
						}
					}
					//Products::where('id', '=',$vproduct->id)->update(array('img'=>$productimg->imagename));
					
				}
				$pmetas = array();
				if(isset($vv['regular_price'])){
					if($p_price == 0){
						$p_price = $vv['regular_price'];
					}
					else{
						if($p_price > $vv['regular_price']){
							$p_price = $vv['regular_price'];
						}
					}
					Products::where('id', '=',$vproduct->id)->update(array('price'=>$vv['regular_price']));
				}
				if(isset($vv['quantity'])){
					if($p_q == 0){
						$p_q = $vv['quantity'];
					}
					else{
						$p_q = $p_q + $vv['quantity'];
					}
					Products::where('id', '=',$vproduct->id)->update(array('quantity'=>$vv['quantity']));
				}
				foreach($vv as $k=>$v){
					$k = '_'.strtolower($k);
					$pp[] = array('product_id'=>$vproduct->id, 'meta_key'=>$k, 'meta_value'=>$v);
				}
			}
			if(!empty($pp)){
				Productmetas::insert($pp);
			}
			
			Products::where('id', '=',$product->id)->update(array('price'=>$p_price, 'quantity'=>$p_q));
			
			
			return Redirect::back()->with('message-success', 'Edited Successfully');
		
		}
		else{
			
		}
		
		return Redirect::to('/wlproducts');
	}

	public function getAdd(){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$parent = Category::where('parentid', '=', 0)->get();
		$parentcat=[];
		foreach($parent as $vl){
			$parentcat[$vl->id]=$vl->categoryname;
		} 
		$prop  = ProductProperties::all();
		$properties = array();
		foreach($prop as $p){
			$vals = ProductPropertyValues::where('property_id', '=', $p->id)->get();
			$properties[] = array('property'=>$p, 'values'=>$vals); 
		}
		return View::make('webshop/add_product', array( 'page'=>'webshop', 'parentcat'=>$parentcat, 'properties'=>$properties));
	}

	public function postAdd(){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		/*echo '<pre>';
		print_r(Input::all());
		die;*/
		
		$parentcategory=Input::get('parentcategory');
		$category = Input::get('category');
		$name = Input::get('product_name');
		$description = Input::get('description');
		$composition = Input::get('composition');
		$delivery_return = Input::get('delivery_return');
		$payment = Input::get('payment');
		$warranty = Input::get('warranty');
		$product_type = Input::get('product_type');
		
		$product = new Products;
		$product->parentcatid = $parentcategory;
		$product->parent = 0;
		$product->catid = $category;
		$product->name = $name;
		$product->description = $description;
		$product->composition = $composition;
		$product->delivery_return = $delivery_return;
		$product->payment = $payment;
		$product->warranty = $warranty;
		$product->type = $product_type;
		
		if(Input::hasFile('feature_image')){
			$file = Input::file('feature_image'); 
			$dest = 'public/webshop/products'; 
			$ret = $this->save_file($dest,$file);
			if($ret != null){$product->img = $ret;}
		}

		if($product_type == 'simple'){
			$sale_price = Input::get('simple_sale_price');
			$product->price = $sale_price;
		}

		$p_price = [];

		$product->save();
		
		if(Input::hasFile('product_images')){
			$dest = 'public/webshop/products';
			$fimages = Input::file('product_images'); 
			foreach($fimages as $fi){
				$ret = $this->save_file($dest,$fi);
				if($ret != null){
					$productimg = new Productimages;
					$productimg->productid=$product->id;
					$productimg->imagename=$ret;
					$productimg->save();
				}
			}
		}
		
		if($product_type == 'simple'){
			$properties = Input::get('property');
			if(isset($properties['regular_price'])){
				Products::where('id', '=',$product->id)->update(array('price'=>$properties['regular_price']));
			}
			if(isset($properties['quantity'])){
				Products::where('id', '=',$product->id)->update(array('quantity'=>$properties['quantity']));
			}
			$pp = array();
			foreach($properties as $k=>$v){
				$k = '_'.strtolower($k);
				$pp[] = array('product_id'=>$product->id, 'meta_key'=>$k, 'meta_value'=>$v);
			}
			if(!empty($pp)){
				Productmetas::insert($pp);
			}
			return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');
		}
		else if($product_type == 'variable'){
			$variations = array();
			$properties = Input::get('variation_property');
			$pp = array();
			$p_price = 0;
			$p_q = 0;
			foreach($properties as $kk=>$vv){
				$vproduct = new Products;
				$vproduct->parentcatid = $product->parentcatid;
				$vproduct->catid = $product->catid;
				$vproduct->name = $product->id.'#'.$product->name;
				$vproduct->description = $product->name.'-'.$product->description;
				$vproduct->type = 'variation';
				$vproduct->parent = $product->id;
				$vproduct->save();
				$fimages = Input::file('variation_image'); 
				if(is_array($fimages)){
					
					$dest = 'public/webshop/products';
					
					foreach($fimages[$kk] as $i=>$fi){
						$ret = $this->save_file($dest,$fi);
						if($ret != null){
							$productimg = new Productimages;
							$productimg->productid=$vproduct->id;
							$productimg->imagename=$ret;
							$productimg->save();
							if($i == 0){
								Products::where('id', '=',$vproduct->id)->update(array('img'=>$productimg->imagename));
							}
						}
					}
					//Products::where('id', '=',$vproduct->id)->update(array('img'=>$productimg->imagename));
					
				}
				$pmetas = array();
				if(isset($vv['regular_price'])){
					if($p_price == 0){
						$p_price = $vv['regular_price'];
					}
					else{
						if($p_price > $vv['regular_price']){
							$p_price = $vv['regular_price'];
						}
					}
					Products::where('id', '=',$vproduct->id)->update(array('price'=>$vv['regular_price']));
				}
				if(isset($vv['quantity'])){
					if($p_q == 0){
						$p_q = $vv['quantity'];
					}
					else{
						$p_q = $p_q + $vv['quantity'];
					}
					Products::where('id', '=',$vproduct->id)->update(array('quantity'=>$vv['quantity']));
				}
				foreach($vv as $k=>$v){
					$k = '_'.strtolower($k);
					$pp[] = array('product_id'=>$vproduct->id, 'meta_key'=>$k, 'meta_value'=>$v);
				}
			}
			if(!empty($pp)){
				Productmetas::insert($pp);
			}
			
			Products::where('id', '=',$product->id)->update(array('price'=>$p_price, 'quantity'=>$p_q));
			
			
			return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');
		
		}
		else{
			
		}
		
		return Redirect::to('/wlproducts');
	}
	public function getProperties(){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$prop = ProductProperties::all();
		$categories = Category::all();
		return View::make('webshop/add_product_properties', array( 'page'=>'webshop', 'properties'=>$prop, 'categories'=>$categories));
	}
	
	public function postProperties(){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$validator = Validator::make(Input::all(), array(
			'submit'=> 'required',
			'product_property'=> 'required',
			'category'=> 'required',
			'product_property_values'=> 'required',
			)
		);
		if($validator->passes()){
			if(Input::get('submit') == 'add'){
				$prop = new ProductProperties;
				$prop->name = Input::get('product_property');
				$prop->category_id = Input::get('category');
				$prop->save();
				
				$val = Input::get('product_property_values');
				if(!is_array($val)){$val = array();}
				foreach($val as $v){
					if($v != ''){
						$propv = new ProductPropertyValues;
						$propv->name = $v;
						$propv->property_id = $prop->id;
						$propv->save();
					}
				}
				return Redirect::to('products/properties')->with('message',"Product property created successfully!");
			}
			else if(Input::get('submit') == 'update'){
				$pid = Input::get('pid');
				$prop = ProductProperties::where('id', '=', $pid)->get();
				if(!empty($prop[0])){
					$prop = $prop[0];
				}
				else{
					return Redirect::to('products/properties')->with('message',"Product property doesn't exists!");
				}
				$upd['name'] =  Input::get('product_property');
				$upd['category_id'] =  Input::get('category');
				ProductProperties::where('id', '=', $pid)->update($upd);
				ProductPropertyValues::where('property_id', '=', $pid)->delete();
				$val = Input::get('product_property_values');
				if(!is_array($val)){$val = array();}
				foreach($val as $v){
					if($v != ''){
						$propv = new ProductPropertyValues;
						$propv->name = $v;
						$propv->property_id = $pid;
						$propv->save();
					}
				}
				return Redirect::to('products/properties')->with('message',"Product property updated successfully!");
			}
			else{
			
			}
		}
		else{
			return Redirect::to('products/properties')->with('message','Following errors occured')->withErrors($validator)->withInput();
		}
	
		echo '<pre>';
		print_r(Input::all());
		die;
	}
	
	public function getPropertydelete($id){
		if(!Auth::check()){
			return Redirect::to('/')->with('message-failure', 'Please Login!');
		}
		$prop = ProductProperties::where('id', '=', $id)->get();
		if(!empty($prop[0])){
			ProductProperties::where('id', '=', $id)->delete();
			ProductPropertyValues::where('property_id', '=', $id)->delete();
			return Redirect::to('products/properties')->with('message',"Property deleted successfully!");
		}
		else{
			return Redirect::to('products/properties')->with('message',"Property doesn't exists!");
		}
		
	}
	public function getPropertyvalues($id){
	
		$prop = ProductProperties::where('id', '=', $id)->get();
		if(!empty($prop[0])){
			$prop = $prop[0];
			$data = array();
			$data['prop'] = $prop->name;
			$data['category'] = $prop->category_id;
			$pv = ProductPropertyValues::where('property_id', '=', $id)->get();
			foreach($pv as $v){
				$data['val'][] = $v->name; 
			}
			echo json_encode($data);die;
		}
		else{
			echo json_encode(array());die;
		}
	}
	
	public function postPropertyvalues($id){
		$prop = ProductProperties::where('id', '=', $id)->get();
		if(!empty($prop[0])){
			$prop = $prop[0];
			$data = array();
			$data['prop'] = $prop->name;
			$pv = ProductPropertyValues::where('property_id', '=', $id)->get();
			foreach($pv as $v){
				$data['values'][] = $v; 
			}
			echo json_encode($data);die;
		}
		else{
			echo json_encode(array());die;
		}
	}
	
	public function save_file($dest, $file){
		
		$file_name = $file->getClientOriginalName();
		
		$ret = $file->move($dest,$file_name);
		
		if($ret){return $dest.'/'.$file_name;}return null;
	}
}
