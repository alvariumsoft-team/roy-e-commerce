<?php
define('BIGBUY_API_KEY', 'NmFkMGJhOTQ0NTBkNjU2YTU2MDg3ZjdmYTQ4ZWJjMWZkMzM4OTYwNTNmNjYzZDhmMTRhYWRjMmE2ODNjZThlYQ');
define('SANDOX_BIGBUY_API_KEY', 'Mjc0N2JlYjNlNTZmZWQ3N2U2MzQ4OTdiZjFjZWRkODY2NGVkZTgwODg2NmIwNTdjMjc1M2RlMDAzNmNlZGRjNA');
define('BIGBUY_END_POINT', 'https://api.bigbuy.eu');
define('SANDOX_BIGBUY_END_POINT', 'https://api.sandbox.bigbuy.eu');
define('BIGBUG_MODE', 'PRODUCTION');
define('ISOCODE', 'en');

class BigbuyController extends BaseController {

	public function __construct(){

	}

	// Function will be used to insert attribute groups from bigbuy
	public function getAttributegroups(){
		die;
		$end_point='rest/catalog/attributegroups.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$Attributegroup = new Attributegroup;
				$Attributegroup->sup_attribute_group_id = $resp->id ;
				$Attributegroup->name = $resp->name;
				$Attributegroup->isocode = $resp->isoCode;
				$Attributegroup->save();
			}
		}
		die;
	}


	// Function will be used to insert attributes from bigbuy
	public function getAttributes(){
		die;
		$end_point='rest/catalog/attributes.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$att_groups = Attributegroup::get();
			$attgroups=array();
			if(!empty($att_groups)){
				foreach($att_groups as $ag){
					$attgroups[$ag->sup_attribute_group_id]=$ag->id;
				}
				foreach($jsonArrayResponse  as $resp){
					$Attribute = new Attribute;
					$Attribute->sup_attribute_id = $resp->id ;
					$Attribute->attribute_group_id = $attgroups[$resp->attributeGroup];
					$Attribute->name = $resp->name;
					$Attribute->isocode = $resp->isoCode;
					$Attribute->save();
				}
			}
		}
		die;
	}

	// Function will be used to insert tags from bigbuy
	public function getTags(){
		die;
		$end_point='rest/catalog/tags.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$Tags = new Tag;
				$Tags->sup_tag_id = $resp->id ;
				$Tags->name = $resp->name;
				$Tags->linkrewrite = $resp->linkRewrite;
				$Tags->isocode = $resp->language;
				$Tags->save();
			}
		}
		die;
	}

	// Function will be used to insert manufacturer from bigbuy
	public function getManufacturer(){
		die;
		$end_point='rest/catalog/manufacturers.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$Manufacturer = new Manufacturers;
				$Manufacturer->sup_manufacturer_id = $resp->id ;
				$Manufacturer->name = $resp->name;
				$Manufacturer->urlimage = $resp->urlImage;
				//$Manufacturer->image = $resp->language;
				//$Manufacturer->get_image = $resp->language;
				$Manufacturer->save();
			}
		}
		die;
	}

	// Function will be used to insert category from bigbuy
	public function getCategory(){

		$end_point='rest/catalog/categories.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		echo "<pre>";
		print_r($jsonArrayResponse);
		die;
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$BigCategory = new BigCategory;
				$BigCategory->sup_category_id = $resp->id ;
				$BigCategory->name = $resp->name;
				$BigCategory->sup_parent_category_id = $resp->parentCategory;
				$BigCategory->url = $resp->url;
				$BigCategory->dateupd = $resp->dateUpd;
				$BigCategory->dateadd = $resp->dateAdd ;
				if(isset($resp->urlImages[0])){
					$BigCategory->sup_header_img = $resp->urlImages[0];
				}
				if(isset($resp->urlImages[1])){
					$BigCategory->sup_menu_img = $resp->urlImages[1];
				}
				if(isset($resp->urlImages[2])){
					$BigCategory->sup_mini_img = $resp->urlImages[2];
				}
				if(isset($resp->urlImages[3])){
					$BigCategory->sup_small_img = $resp->urlImages[3];
				}
				if(isset($resp->urlImages[4])){
					$BigCategory->sup_big_img = $resp->urlImages[4];
				}
				$BigCategory->isocode = $resp->isoCode;
				$BigCategory->save();
			}
		}
		die;
	}

	// Function will be used to update parent category id in categories table

	public function getUpdateparentcat(){
		die;
		$allcats=array();
		$all_cats = BigCategory::get();
		foreach($all_cats as $cat){
			$allcats[$cat->sup_category_id]=$cat->id;
		}

		foreach($all_cats as $cat){
			if($cat->sup_parent_category_id==2){
				$parent_cat=0;
			}
			else{
				$parent_cat=$allcats[$cat->sup_parent_category_id];
			}
			BigCategory::where('id', '=', $cat->id)->update(['parent_category_id'=>$parent_cat]);
		}
		die;
	}

	// Save manufacturer images in our server from bigbuy and update in database
	public function getUpdatemanuimage(){
		die;
		$all_manufacturers = Manufacturers::where('get_image', '=', 0)->orderBy('id', 'desc')->get();
		foreach($all_manufacturers as $manu){
			$img_type_arr=explode(".",$manu->urlimage);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$manu->id."_mini.".$img_type;
			$image_data = @file_get_contents($manu->urlimage);
			if($image_data){
				$file='public/webshop/manufacturer/'.$image_name;
				if(file_put_contents($file, $image_data)){
					Manufacturers::where('id', '=', $manu->id)->update(['image'=>$image_name,'get_image'=>1]);
				}
			}
		}
		die;
	}

	// Save Header category images in our server from bigbuy and update in database
	public function getUpdateheadercatimage(){
		$all_cats = BigCategory::whereNull('header_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_header_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_header.".$img_type;
			$image_data = @file_get_contents($cats->sup_header_img);
			if($image_data){
				$file='public/webshop/categories/header/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['header_img'=>$image_name]);
				}
			}

		}
		die;
	}

	// Save Menu category images in our server from bigbuy and update in database
	public function getUpdatemenucatimage(){
		$all_cats = BigCategory::whereNull('menu_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_menu_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_menu.".$img_type;
			$image_data = @file_get_contents($cats->sup_menu_img);
			if($image_data){
				$file='public/webshop/categories/menu/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['menu_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Mini category images in our server from bigbuy and update in database
	public function getUpdateminicatimage(){
		$all_cats = BigCategory::whereNull('mini_img')->orderBy('id', 'desc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_mini_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_mini.".$img_type;
			$image_data = @file_get_contents($cats->sup_mini_img);
			if($image_data){
				$file='public/webshop/categories/mini/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['mini_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Small category images in our server from bigbuy and update in database
	public function getUpdatesmallcatimage(){
		$all_cats = BigCategory::whereNull('small_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_small_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_small.".$img_type;
			$image_data = @file_get_contents($cats->sup_small_img);
			if($image_data){
				$file='public/webshop/categories/small/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['small_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Big category images in our server from bigbuy and update in database
	public function getUpdatebigcatimage(){
		$all_cats = BigCategory::whereNull('big_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_big_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_big.".$img_type;
			$image_data = @file_get_contents($cats->sup_big_img);
			if($image_data){
				$file='public/webshop/categories/big/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['big_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Function will be used to insert category from bigbuy
	public function getProducts(){
		die;
		$end_point='rest/catalog/products.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);

		$jsonArrayResponse = json_decode($response);

		echo count($jsonArrayResponse);

		$se_end_point='rest/catalog/productsinformation.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$se_curl_url=BIGBUY_END_POINT."/".$se_end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$se_curl_url=SANDOX_BIGBUY_END_POINT."/".$se_end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$se_cURLConnection = curl_init();
		curl_setopt($se_cURLConnection, CURLOPT_URL, $se_curl_url);
		curl_setopt($se_cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($se_cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$se_response = curl_exec($se_cURLConnection);
		curl_close($se_cURLConnection);
		$jsonArrayResponseInfo = json_decode($se_response);
		$prinfo=array();
		foreach($jsonArrayResponseInfo as $k => $info){
			$prinfo[$info->id]=$info;
		}
		$myarr=array();
		if(!empty($jsonArrayResponse)){
			$allmans=array();
			$all_mans = Manufacturers::get();
			foreach($all_mans as $mans){
				$allmans[$mans->sup_manufacturer_id]=$mans->id;
			}
			$allcats=array();
			$all_cats = BigCategory::get();
			foreach($all_cats as $cats){
				$allcats[$cats->sup_category_id]=$cats->id;
			}
			$BigTst = new BigTst;
			$BigTst->descr = count($jsonArrayResponse);
			$BigTst->tm = count($prinfo) ;
			$BigTst->save();
			$alltsts = BigTst::get();
			$cn=count($alltsts);
			if($cn==1){
			foreach($jsonArrayResponse  as $k => $resp){
				if($k>20000){
					$BigProduct = new BigProduct;
					if($resp->id){
						$BigProduct->sup_product_id = $resp->id ;
					}
					if($resp->manufacturer){
						$BigProduct->sup_manufacturer_id = $resp->manufacturer;
					}
					if(isset($allmans[$resp->manufacturer])){
						$BigProduct->manufacturer_id = $allmans[$resp->manufacturer];
					}
					else{
						$BigProduct->manufacturer_id = 0;
					}
					$BigProduct->sku = $resp->sku;
					$BigProduct->ean13 = $resp->ean13;
					$BigProduct->weight = $resp->weight ;
					$BigProduct->height = $resp->height;
					$BigProduct->width = $resp->width ;
					$BigProduct->depth = $resp->depth;
					$BigProduct->dateupd = $resp->dateUpd;
					if($resp->category){
						$BigProduct->sup_category_id = $resp->category;
					}
					if(isset($allcats[$resp->category])){
						$BigProduct->category_id = $allcats[$resp->category];
					}
					else{
						$BigProduct->category_id = 0;
					}
					if($resp->categories){
						$BigProduct->categories = $resp->categories ;
					}
					$BigProduct->dateupddescription = $resp->dateUpdDescription;
					$BigProduct->dateupdstock = $resp->dateUpdStock ;
					$BigProduct->wholesaleprice = $resp->wholesalePrice;
					$BigProduct->retailprice = $resp->retailPrice;
					$BigProduct->dateadd = $resp->dateAdd;
					$BigProduct->video = $resp->video;
					if($resp->active){
						$BigProduct->active = $resp->active ;
					}
					if($resp->images){
						$BigProduct->images = $resp->images;
					}
					if($resp->attributes){
						$BigProduct->attributes = $resp->attributes ;
					}
					if($resp->tags){
						$BigProduct->tags = $resp->tags;
					}
					$BigProduct->taxrate = $resp->taxRate;
					if($resp->taxId){
						$BigProduct->taxid = $resp->taxId;
					}
					$BigProduct->dateupdproperties = $resp->dateUpdProperties;
					$BigProduct->dateupdcategories = $resp->dateUpdCategories ;
					$BigProduct->inshopsprice = $resp->inShopsPrice;
					$BigProduct->dateupdimages = $resp->dateUpdImages ;
					if(isset($prinfo[$resp->id]->name)){
						$BigProduct->name = $prinfo[$resp->id]->name ;
					}
					if(isset($prinfo[$resp->id]->isoCode)){
						$BigProduct->isocode = $prinfo[$resp->id]->isoCode ;
					}
					if(isset($prinfo[$resp->id]->url)){
						$BigProduct->url = $prinfo[$resp->id]->url ;
					}
					if(isset($prinfo[$resp->id]->description)){
						$BigProduct->description = $prinfo[$resp->id]->description ;
					}
					$BigProduct->save();
				}

			}
			}
		}
		echo "all completed";
		die;
	}


	// Function will be used to update product information from bigbuy
	public function getUpdateproductinfo(){
		die;
		$end_point='rest/catalog/productsinformation.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				BigProduct::where('sup_product_id', '=', $resp->id)->update(['name'=>$resp->name,'description'=>$resp->description,'url'=>$resp->url,'isocode'=>$resp->isoCode]);
			}
		}
		die;
	}

	// Function will be used to insert variations from bigbuy
	public function getVariations(){
		die;
		$end_point='rest/catalog/variations.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$att_ids=array();
				if(isset($resp->attributes[0])){
					foreach($resp->attributes as $att){
						$att_ids[]=$att->id;
					}
				}
				$Variations = new Variations;
				$Variations->sup_variation_id = $resp->id ;
				$Variations->attributes_ids = implode(",",$att_ids);
				$Variations->save();
			}
		}
		die;
	}

	// Function will be used to insert product variations from bigbuy
	public function getProductvariations(){
		die;
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$end_point='rest/catalog/productsvariations.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allvari=array();
			$all_cari = Variations::get();
			foreach($all_cari as $vari){
				$allvari[$vari->sup_variation_id]=$vari->id;
			}
			foreach($jsonArrayResponse  as $resp){
				$ProductVariations = new ProductVariations;
				$ProductVariations->sup_variation_id = $resp->id ;
				$ProductVariations->variation_id = $allvari[$resp->id] ;
				$ProductVariations->sup_product_id = $resp->product ;
				$ProductVariations->product_id = $allpros[$resp->product] ;
				$ProductVariations->sku = $resp->sku ;
				$ProductVariations->ean13 = $resp->ean13 ;
				$ProductVariations->extraweight = $resp->extraWeight ;
				$ProductVariations->wholesaleprice = $resp->wholesalePrice ;
				$ProductVariations->retailprice = $resp->retailPrice ;
				$ProductVariations->width = $resp->width ;
				$ProductVariations->height = $resp->height ;
				$ProductVariations->depth = $resp->depth ;
				$ProductVariations->inshopsprice = $resp->inShopsPrice ;
				$ProductVariations->save();
			}
		}
		die;
	}

	// Function will be used to insert product images from bigbuy
	public function getProductsimages(){
		die;
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$end_point='rest/catalog/productsimages.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				if($k>30000 && $k <=40000){
					foreach($resp->images as $img){
						$Productsimages = new Productsimages;
						$Productsimages->sup_product_id = $resp->id ;
						if(isset($allpros[$resp->id])){
							$Productsimages->product_id =$allpros[$resp->id];
						}
						else{
							$Productsimages->product_id =0;
						}
						$Productsimages->sup_image_id = $img->id ;
						$Productsimages->name = $img->name ;
						$Productsimages->iscover = $img->isCover ;
						$Productsimages->url = $img->url ;
						$Productsimages->save();
					}
				}

			}
		}

		echo "all completed";
		die;
	}

	// Save Products images in our server from bigbuy and update in database
	public function getUpdateproductimages(){
		$all_images = Productsimages::select('id','url')->where('get_image', '=', 0)->orderBy('id', 'asc')->get();
		foreach($all_images  as $img){
			$img_type_arr=explode(".",$img->url);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$img->id."_product.".$img_type;
			$image_data = @file_get_contents($img->url);
			if($image_data){
				$file='public/webshop/bigproducts/'.$image_name;
				if(file_put_contents($file, $image_data)){
					Productsimages::where('id', '=', $img->id)->update(['img_name'=>$image_name,'get_image'=>1]);
				}
			}
		}
		die;
	}


	// Function will be used to update variation product information
	public function getUpdatevarproductinfo(){
		die;
		$var_sup_pro=array();
		$all_pros = ProductVariations::select('variation_id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$var_sup_pro[$pros->variation_id]=$pros->sup_product_id;
		}

		$var_sup_pro_id=array();
		$all_pros = ProductVariations::select('variation_id','product_id')->get();
		foreach($all_pros as $pros){
			$var_sup_pro_id[$pros->variation_id]=$pros->product_id;
		}
		$all_var_data = Variations::select('id')->where('is_updated', '=', 0)->get();
		if(!empty($all_var_data)){
			foreach($all_var_data as $k){
				if(isset($var_sup_pro[$k->id]) && isset($var_sup_pro_id[$k->id])){
					Variations::where('id', '=', $k->id)->update(['sup_product_id'=>$var_sup_pro[$k->id],'product_id'=>$var_sup_pro_id[$k->id],'is_updated'=> 1]);
				}
			}
		}
		die;
	}


	// Function will be used to insert product tags from bigbuy
	public function getProductstags(){
		die;
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$allprotag=array();
		$all_pros_tag = Tag::select('id','sup_tag_id')->get();
		foreach($all_pros_tag as $tag){
			$allprotag[$tag->sup_tag_id]=$tag->id;
		}

		$end_point='rest/catalog/productstags.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k =>$resp){
				if($k >160000 && $k <=180000){
					$ProductTags = new ProductTags;
					$ProductTags->sup_product_id = $resp->id;
					if(isset($allpros[$resp->id])){
						$ProductTags->product_id = $allpros[$resp->id];
					}
					else{
						$ProductTags->product_id = 0;
					}
					$ProductTags->sup_tag_id = $resp->tag->id;
					if(isset($allprotag[$resp->tag->id])){
						$ProductTags->tag_id = $allprotag[$resp->tag->id];
					}
					else{
						$ProductTags->tag_id = 0;
					}
					$ProductTags->save();
				}
			}
		}
		echo "completed";
		die;
	}

	// Function will be used to update big_product table quantity, minhandling, maxhandling from bigbuy
	public function getUpdateproductquant(){
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$end_point='rest/catalog/productsstock.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				//if($k >15000 && $k <=40000){
					$pr_id=0;
					if(isset($allpros[$resp->id])){
						$pr_id=$allpros[$resp->id];
					}
					if($pr_id >0){
						BigProduct::where('id', '=', $pr_id)->update(['quantity'=>$resp->stocks[0]->quantity,'minhandlingdays'=>$resp->stocks[0]->minHandlingDays,'maxhandlingdays'=>$resp->stocks[0]->maxHandlingDays,]);
					}
				//}
			}
		}
		echo "completed";
		die;
	}

	// Function will be used to update big_product table quantity, minhandling, maxhandling from bigbuy
	public function getUpdateproductvarquant(){
		$allpros=array();
		$all_pros = ProductVariations::select('id','sup_variation_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_variation_id]=$pros->id;
		}
		$end_point='rest/catalog/productsvariationsstock.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				//if($k >15000 && $k <=40000){
					$pr_id=0;
					if(isset($allpros[$resp->id])){
						$pr_id=$allpros[$resp->id];
					}
					if($pr_id >0){
						ProductVariations::where('id', '=', $pr_id)->update(['quantity'=>$resp->stocks[0]->quantity,'minhandlingdays'=>$resp->stocks[0]->minHandlingDays,'maxhandlingdays'=>$resp->stocks[0]->maxHandlingDays,]);
					}
				//}

			}
		}
		echo "completed";
		die;
	}

	// Function will be used to insert categories of products
	public function getProductscategory(){
		die;
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$allcats=array();
		$allcats = BigCategory::select('id','sup_category_id')->get();
		foreach($allcats as $pros){
			$allcats[$pros->sup_category_id]=$pros->id;
		}
		$end_point='rest/catalog/productscategories.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				if($k >131070 ){
					$ProductCategories = new ProductCategories;
					$ProductCategories->sup_product_category_id = $resp->id ;
					if(isset($allpros[$resp->product])){
						$ProductCategories->product_id = $allpros[$resp->product];
					}
					else{
						$ProductCategories->product_id = 0;
					}

					if(isset($allcats[$resp->category])){
						$ProductCategories->category_id = $allcats[$resp->category];
					}
					else{
						$ProductCategories->category_id = 0;
					}
					$ProductCategories->sup_product_id = $resp->product;
					$ProductCategories->sup_category_id = $resp->category;
					$ProductCategories->save();

				}
			}

		}
		echo "completed";
		die;
	}

	// Function will be used to insert tracking carriers
	public function getCarrierstrack(){
	die;
		$end_point='rest/tracking/carriers.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key,'Content-Type: application/json'
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse  as $resp){
				$TrackCarrier = new TrackCarrier;
				$TrackCarrier->carrier_id = $resp->id;
				$TrackCarrier->carrier_name = $resp->name;
				$TrackCarrier->save();
			}
		}
		echo "completed";
		die;

	}
	/* All Cron Functions are here */

	public function getCrnattributegroups(){
		$end_point='rest/catalog/attributegroups.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allattgrps=array();
			$allgrps = Attributegroup::select('sup_attribute_group_id')->get();
			foreach($allgrps as $grps){
				$allattgrps[$grps->sup_attribute_group_id]=$grps->sup_attribute_group_id;
			}
			foreach($jsonArrayResponse  as $resp){
				if(!isset($allattgrps[$resp->id])){
					$Attributegroup = new Attributegroup;
					$Attributegroup->sup_attribute_group_id = $resp->id ;
					$Attributegroup->name = $resp->name;
					$Attributegroup->isocode = $resp->isoCode;
					$Attributegroup->save();
				}
			}
		}
		die;
	}

	// Function will be used to insert attributes from bigbuy
	public function getCrnattributes(){
		$end_point='rest/catalog/attributes.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$att_groups = Attributegroup::select('sup_attribute_group_id','id')->get();
			$attgroups=array();
			if(!empty($att_groups)){
				foreach($att_groups as $ag){
					$attgroups[$ag->sup_attribute_group_id]=$ag->id;
				}
				$allatts=array();
				$allatt = Attribute::select('sup_attribute_id')->get();
				foreach($allatt as $atts){
					$allatts[$atts->sup_attribute_id]=$atts->sup_attribute_id;
				}
				foreach($jsonArrayResponse  as $resp){
					if(!isset($allatts[$resp->id])){
						if(isset($attgroups[$resp->attributeGroup])){
							$Attribute = new Attribute;
							$Attribute->sup_attribute_id = $resp->id ;
							$Attribute->attribute_group_id = $attgroups[$resp->attributeGroup];
							$Attribute->name = $resp->name;
							$Attribute->isocode = $resp->isoCode;
							$Attribute->save();
						}
					}
				}
			}
		}
		die;
	}

	// Function will be used to insert tags from bigbuy
	public function getCrntags(){
		$end_point='rest/catalog/tags.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$alltags=array();
			$alltag = Tag::select('sup_tag_id')->get();
			foreach($alltag as $tg){
				$alltags[$tg->sup_tag_id]=$tg->sup_tag_id;
			}
			foreach($jsonArrayResponse  as $resp){
				if(!isset($alltags[$resp->id])){
					$Tags = new Tag;
					$Tags->sup_tag_id = $resp->id ;
					$Tags->name = $resp->name;
					$Tags->linkrewrite = $resp->linkRewrite;
					$Tags->isocode = $resp->language;
					$Tags->save();
				}
			}
		}
		die;
	}

	// Function will be used to insert manufacturer from bigbuy
	public function getCrnmanufacturer(){
		$end_point='rest/catalog/manufacturers.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allmanuf=array();
			$allman = Manufacturers::select('sup_manufacturer_id')->get();
			foreach($allman as $mn){
				$allmanuf[$mn->sup_manufacturer_id]=$mn->sup_manufacturer_id;
			}
			foreach($jsonArrayResponse  as $resp){
				if(!isset($allmanuf[$resp->id])){
					$Manufacturer = new Manufacturers;
					$Manufacturer->sup_manufacturer_id = $resp->id ;
					$Manufacturer->name = $resp->name;
					$Manufacturer->urlimage = $resp->urlImage;
					$Manufacturer->save();
				}
			}
		}
		die;
	}

	// Save manufacturer images in our server from bigbuy and update in database
	public function getCrnupdatemanuimage(){
		$all_manufacturers = Manufacturers::select('urlimage','id')->where('get_image', '=', 0)->orderBy('id', 'desc')->get();
		foreach($all_manufacturers as $manu){
			$img_type_arr=explode(".",$manu->urlimage);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$manu->id."_mini.".$img_type;
			$image_data = @file_get_contents($manu->urlimage);
			if($image_data){
				$file='public/webshop/manufacturer/'.$image_name;
				if(file_put_contents($file, $image_data)){
					Manufacturers::where('id', '=', $manu->id)->update(['image'=>$image_name,'get_image'=>1]);
				}
			}
		}
		die;
	}

	// Function will be used to insert category from bigbuy
	public function getCrncategory(){
		$end_point='rest/catalog/categories.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allcategory=array();
			$allcat = BigCategory::select('sup_category_id')->get();
			foreach($allcat as $ct){
				$allcategory[$ct->sup_category_id]=$ct->sup_category_id;
			}
			foreach($jsonArrayResponse  as $resp){
				if(!isset($allcategory[$resp->id])){
					$BigCategory = new BigCategory;
					$BigCategory->sup_category_id = $resp->id ;
					$BigCategory->name = $resp->name;
					$BigCategory->sup_parent_category_id = $resp->parentCategory;
					$BigCategory->url = $resp->url;
					$BigCategory->dateupd = $resp->dateUpd;
					$BigCategory->dateadd = $resp->dateAdd ;
					if(isset($resp->urlImages[0])){
						$BigCategory->sup_header_img = $resp->urlImages[0];
					}
					if(isset($resp->urlImages[1])){
						$BigCategory->sup_menu_img = $resp->urlImages[1];
					}
					if(isset($resp->urlImages[2])){
						$BigCategory->sup_mini_img = $resp->urlImages[2];
					}
					if(isset($resp->urlImages[3])){
						$BigCategory->sup_small_img = $resp->urlImages[3];
					}
					if(isset($resp->urlImages[4])){
						$BigCategory->sup_big_img = $resp->urlImages[4];
					}
					$BigCategory->isocode = $resp->isoCode;
					$BigCategory->save();
				}
			}
		}
		die;
	}

	// Function will be used to update parent category id in categories table

	public function getCrnupdateparentcat(){
		$allcats=array();
		$all_cats = BigCategory::select('sup_category_id','id','sup_parent_category_id')->get();
		foreach($all_cats as $cat){
			$allcats[$cat->sup_category_id]=$cat->id;
		}
		foreach($all_cats as $cat){
			if($cat->sup_parent_category_id==2){
				$parent_cat=0;
			}
			else{
				$parent_cat=$allcats[$cat->sup_parent_category_id];
			}
			BigCategory::where('id', '=', $cat->id)->update(['parent_category_id'=>$parent_cat]);
		}
		die;
	}

	// Save Header category images in our server from bigbuy and update in database
	public function getCrnupdateheadercatimage(){
		$all_cats = BigCategory::select('sup_header_img','id')->whereNull('header_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_header_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_header.".$img_type;
			$image_data = @file_get_contents($cats->sup_header_img);
			if($image_data){
				$file='public/webshop/categories/header/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['header_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Menu category images in our server from bigbuy and update in database
	public function getCrnupdatemenucatimage(){
		$all_cats = BigCategory::select('sup_menu_img','id')->whereNull('menu_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_menu_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_menu.".$img_type;
			$image_data = @file_get_contents($cats->sup_menu_img);
			if($image_data){
				$file='public/webshop/categories/menu/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['menu_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Mini category images in our server from bigbuy and update in database
	public function getCrnupdateminicatimage(){
		$all_cats = BigCategory::select('sup_mini_img','id')->whereNull('mini_img')->orderBy('id', 'desc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_mini_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_mini.".$img_type;
			$image_data = @file_get_contents($cats->sup_mini_img);
			if($image_data){
				$file='public/webshop/categories/mini/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['mini_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Small category images in our server from bigbuy and update in database
	public function getCrnupdatesmallcatimage(){
		$all_cats = BigCategory::select('sup_small_img','id')->whereNull('small_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_small_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_small.".$img_type;
			$image_data = @file_get_contents($cats->sup_small_img);
			if($image_data){
				$file='public/webshop/categories/small/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['small_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Save Big category images in our server from bigbuy and update in database
	public function getCrnupdatebigcatimage(){
		$all_cats = BigCategory::select('sup_big_img','id')->whereNull('big_img')->orderBy('id', 'asc')->get();
		foreach($all_cats as $cats){
			$img_type_arr=explode(".",$cats->sup_big_img);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$cats->id."_big.".$img_type;
			$image_data = @file_get_contents($cats->sup_big_img);
			if($image_data){
				$file='public/webshop/categories/big/'.$image_name;
				if(file_put_contents($file, $image_data)){
					BigCategory::where('id', '=', $cats->id)->update(['big_img'=>$image_name]);
				}
			}
		}
		die;
	}

	// Function will be used to insert category from bigbuy
	public function getCrnproducts(){
		$end_point='rest/catalog/products.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		$se_end_point='rest/catalog/productsinformation.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$se_curl_url=BIGBUY_END_POINT."/".$se_end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$se_curl_url=SANDOX_BIGBUY_END_POINT."/".$se_end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$se_cURLConnection = curl_init();
		curl_setopt($se_cURLConnection, CURLOPT_URL, $se_curl_url);
		curl_setopt($se_cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($se_cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$se_response = curl_exec($se_cURLConnection);
		curl_close($se_cURLConnection);
		$jsonArrayResponseInfo = json_decode($se_response);
		$prinfo=array();
		foreach($jsonArrayResponseInfo as $k => $info){
			$prinfo[$info->id]=$info;
		}
		if(!empty($jsonArrayResponse)){
			$allmans=array();
			$all_mans = Manufacturers::select('sup_manufacturer_id','id')->get();
			foreach($all_mans as $mans){
				$allmans[$mans->sup_manufacturer_id]=$mans->id;
			}
			$allcats=array();
			$all_cats = BigCategory::select('sup_category_id','id')->get();
			foreach($all_cats as $cats){
				$allcats[$cats->sup_category_id]=$cats->id;
			}
			$allbigproduct=array();
			$allbigpt = BigProduct::select('sup_product_id')->get();
			foreach($allbigpt as $pt){
				$allbigproduct[$pt->sup_product_id]=$pt->sup_product_id;
			}
			foreach($jsonArrayResponse  as $k => $resp){
				if(!isset($allbigproduct[$resp->id])){
					$BigProduct = new BigProduct;
					if($resp->id){
						$BigProduct->sup_product_id = $resp->id ;
					}
					if($resp->manufacturer){
						$BigProduct->sup_manufacturer_id = $resp->manufacturer;
					}
					if(isset($allmans[$resp->manufacturer])){
						$BigProduct->manufacturer_id = $allmans[$resp->manufacturer];
					}
					else{
						$BigProduct->manufacturer_id = 0;
					}
					$BigProduct->sku = $resp->sku;
					$BigProduct->ean13 = $resp->ean13;
					$BigProduct->weight = $resp->weight ;
					$BigProduct->height = $resp->height;
					$BigProduct->width = $resp->width ;
					$BigProduct->depth = $resp->depth;
					$BigProduct->dateupd = $resp->dateUpd;
					if($resp->category){
						$BigProduct->sup_category_id = $resp->category;
					}
					if(isset($allcats[$resp->category])){
						$BigProduct->category_id = $allcats[$resp->category];
					}
					else{
						$BigProduct->category_id = 0;
					}
					if($resp->categories){
						$BigProduct->categories = $resp->categories ;
					}
					$BigProduct->dateupddescription = $resp->dateUpdDescription;
					$BigProduct->dateupdstock = $resp->dateUpdStock ;
					$BigProduct->wholesaleprice = $resp->wholesalePrice;
					$BigProduct->retailprice = $resp->retailPrice;
					$BigProduct->dateadd = $resp->dateAdd;
					$BigProduct->video = $resp->video;
					if($resp->active){
						$BigProduct->active = $resp->active ;
					}
					if($resp->images){
						$BigProduct->images = $resp->images;
					}
					if($resp->attributes){
						$BigProduct->attributes = $resp->attributes ;
					}
					if($resp->tags){
						$BigProduct->tags = $resp->tags;
					}
					$BigProduct->taxrate = $resp->taxRate;
					if($resp->taxId){
						$BigProduct->taxid = $resp->taxId;
					}
					$BigProduct->dateupdproperties = $resp->dateUpdProperties;
					$BigProduct->dateupdcategories = $resp->dateUpdCategories ;
					$BigProduct->inshopsprice = $resp->inShopsPrice;
					$BigProduct->dateupdimages = $resp->dateUpdImages ;
					if(isset($prinfo[$resp->id]->name)){
						$BigProduct->name = $prinfo[$resp->id]->name ;
					}
					if(isset($prinfo[$resp->id]->isoCode)){
						$BigProduct->isocode = $prinfo[$resp->id]->isoCode ;
					}
					if(isset($prinfo[$resp->id]->url)){
						$BigProduct->url = $prinfo[$resp->id]->url ;
					}
					if(isset($prinfo[$resp->id]->description)){
						$BigProduct->description = $prinfo[$resp->id]->description ;
					}
					$BigProduct->save();
				}
			}
		}
		echo "all completed";
		die;
	}

	// Function will be used to insert variations from bigbuy
	public function getCrnvariations(){
		$end_point='rest/catalog/variations.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allbigvariations=array();
			$allbigvar = Variations::select('sup_variation_id')->get();
			foreach($allbigvar as $var){
				$allbigvariations[$var->sup_variation_id]=$var->sup_variation_id;
			}
			foreach($jsonArrayResponse  as $resp){
				if(!isset($allbigvariations[$resp->id])){
					$att_ids=array();
					if(isset($resp->attributes[0])){
						foreach($resp->attributes as $att){
							$att_ids[]=$att->id;
						}
					}
					$Variations = new Variations;
					$Variations->sup_variation_id = $resp->id ;
					$Variations->attributes_ids = implode(",",$att_ids);
					$Variations->save();
				}
			}
		}
		echo "all completed";
		die;
	}

	// Function will be used to insert product variations from bigbuy
	public function getCrnproductvariations(){
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$end_point='rest/catalog/productsvariations.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			$allvari=array();
			$all_cari = Variations::select('sup_variation_id','id')->get();
			foreach($all_cari as $vari){
				$allvari[$vari->sup_variation_id]=$vari->id;
			}
			$allbigprovariations=array();
			$allbigprovar = ProductVariations::select('sup_variation_id')->get();
			foreach($allbigprovar as $provar){
				$allbigprovariations[$provar->sup_variation_id]=$provar->sup_variation_id;
			}

			foreach($jsonArrayResponse  as $resp){
				if(!isset($allbigprovariations[$resp->id])){
					$ProductVariations = new ProductVariations;
					$ProductVariations->sup_variation_id = $resp->id ;
					if(isset($allvari[$resp->id])){
						$ProductVariations->variation_id = $allvari[$resp->id] ;
					}
					else{
						$ProductVariations->variation_id = 0 ;
					}
					$ProductVariations->sup_product_id = $resp->product ;
					if(isset($allpros[$resp->product])){
						$ProductVariations->product_id = $allpros[$resp->product] ;
					}
					else{
						$ProductVariations->product_id = 0 ;
					}
					$ProductVariations->sku = $resp->sku ;
					$ProductVariations->ean13 = $resp->ean13 ;
					$ProductVariations->extraweight = $resp->extraWeight ;
					$ProductVariations->wholesaleprice = $resp->wholesalePrice ;
					$ProductVariations->retailprice = $resp->retailPrice ;
					$ProductVariations->width = $resp->width ;
					$ProductVariations->height = $resp->height ;
					$ProductVariations->depth = $resp->depth ;
					$ProductVariations->inshopsprice = $resp->inShopsPrice ;
					$ProductVariations->save();
				}
			}
		}
		echo "all complted";
		die;
	}

	// Function will be used to insert product images from bigbuy
	public function getCrnproductsimages(){
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		unset($all_pros);
		$end_point='rest/catalog/productsimages.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		$response=array();
		if(!empty($jsonArrayResponse)){
			$allbigproimages=array();
			$allbigproimg = Productsimages::select('sup_image_id')->get();
			foreach($allbigproimg as $proimg){
				$allbigproimages[$proimg->sup_image_id]=$proimg->sup_image_id;
			}
			unset($allbigproimg);
			foreach($jsonArrayResponse as $k => $resp){
				if(!isset($allbigproimages[$resp->id])){
					foreach($resp->images as $img){
						$Productsimages = new Productsimages;
						$Productsimages->sup_product_id = $resp->id ;
						if(isset($allpros[$resp->id])){
							$Productsimages->product_id =$allpros[$resp->id];
						}
						else{
							$Productsimages->product_id =0;
						}
						$Productsimages->sup_image_id = $img->id ;
						$Productsimages->name = $img->name ;
						$Productsimages->iscover = $img->isCover ;
						$Productsimages->url = $img->url ;
						$Productsimages->save();
					}
				}
			}
		}
		echo "all completed";
		die;
	}

	// Save Products images in our server from bigbuy and update in database
	public function getCrnupdateproductimages(){
		$all_images = Productsimages::select('id','url')->where('get_image', '=', 0)->orderBy('id', 'asc')->get();
		foreach($all_images  as $img){
			$img_type_arr=explode(".",$img->url);
			$in=count($img_type_arr)-1;
			$img_type=$img_type_arr[$in];
			$image_name=$img->id."_product.".$img_type;
			$image_data = @file_get_contents($img->url);
			if($image_data){
				$file='public/webshop/bigproducts/'.$image_name;
				if(file_put_contents($file, $image_data)){
					Productsimages::where('id', '=', $img->id)->update(['img_name'=>$image_name,'get_image'=>1]);
				}
			}
		}
		echo "all completed";
		die;
	}

	// Function will be used to update variation product information
	public function getCrnupdatevarproductinfo(){
		$var_sup_pro=array();
		$all_pros = ProductVariations::select('variation_id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$var_sup_pro[$pros->variation_id]=$pros->sup_product_id;
		}

		$var_sup_pro_id=array();
		$all_pros = ProductVariations::select('variation_id','product_id')->get();
		foreach($all_pros as $pros){
			$var_sup_pro_id[$pros->variation_id]=$pros->product_id;
		}
		$all_var_data = Variations::select('id')->where('is_updated', '=', 0)->get();
		if(!empty($all_var_data)){
			foreach($all_var_data as $k){
				if(isset($var_sup_pro[$k->id]) && isset($var_sup_pro_id[$k->id])){
					Variations::where('id', '=', $k->id)->update(['sup_product_id'=>$var_sup_pro[$k->id],'product_id'=>$var_sup_pro_id[$k->id],'is_updated'=> 1]);
				}
			}
		}
		echo "all completed";
		die;
	}

	// Function will be used to insert product tags from bigbuy
	public function getCrnproductstags(){
		$end_point='rest/catalog/productstags.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		$response=array();
		if(!empty($jsonArrayResponse)){
			//$allbigproducttags=array();
			//$allbigprtags = ProductTags::select('sup_product_id')->get();
			//foreach($allbigprtags as $prtag){
				//$allbigproducttags[$prtag->sup_product_id]=$prtag->sup_product_id;
			//}
			$allpros=array();
			$all_pros = BigProduct::select('id','sup_product_id')->get();
			foreach($all_pros as $pros){
				$allpros[$pros->sup_product_id]=$pros->id;
			}
			$all_pros=array();
			$allprotag=array();
			$all_pros_tag = Tag::select('id','sup_tag_id')->get();
			foreach($all_pros_tag as $tag){
				$allprotag[$tag->sup_tag_id]=$tag->id;
			}
			$all_pros_tag=array();
			foreach($jsonArrayResponse as $k =>$resp){
				//if(!isset($allbigproducttags[$resp->id])){
					$ProductTags = new ProductTags;
					$ProductTags->sup_product_id = $resp->id;
					if(isset($allpros[$resp->id])){
						$ProductTags->product_id = $allpros[$resp->id];
					}
					else{
						$ProductTags->product_id = 0;
					}
					if(isset($resp->tag->id)){
						$tg_id=$resp->tag->id;
						$ProductTags->sup_tag_id = $tg_id;
						if(isset($allprotag[$tg_id])){
							$ProductTags->tag_id = $allprotag[$tg_id];
						}
						else{
							$ProductTags->tag_id = 0;
						}
					}
					else{
						$ProductTags->sup_tag_id = 0;
						$ProductTags->tag_id = 0;
					}
					$ProductTags->save();
				//}
			}
		}
		echo "completed";
		die;
	}

	// Function will be used to update big_product table quantity, minhandling, maxhandling from bigbuy
	public function getCrnupdateproductquant(){
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$end_point='rest/catalog/productsstock.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				$pr_id=0;
				if(isset($allpros[$resp->id])){
					$pr_id=$allpros[$resp->id];
				}
				if($pr_id >0){
					BigProduct::where('id', '=', $pr_id)->update(['quantity'=>$resp->stocks[0]->quantity,'minhandlingdays'=>$resp->stocks[0]->minHandlingDays,'maxhandlingdays'=>$resp->stocks[0]->maxHandlingDays,]);
				}
			}
		}
		echo "completed";
		die;
	}

	// Function will be used to update big_product table quantity, minhandling, maxhandling from bigbuy
	public function getCrnupdateproductvarquant(){
		$allpros=array();
		$all_pros = ProductVariations::select('id','sup_variation_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_variation_id]=$pros->id;
		}
		$end_point='rest/catalog/productsvariationsstock.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		if(!empty($jsonArrayResponse)){
			foreach($jsonArrayResponse as $k => $resp){
				$pr_id=0;
				if(isset($allpros[$resp->id])){
					$pr_id=$allpros[$resp->id];
				}
				if($pr_id >0){
					ProductVariations::where('id', '=', $pr_id)->update(['quantity'=>$resp->stocks[0]->quantity,'minhandlingdays'=>$resp->stocks[0]->minHandlingDays,'maxhandlingdays'=>$resp->stocks[0]->maxHandlingDays,]);
				}
			}
		}
		echo "completed";
		die;
	}

	// Function will be used to insert categories of products
	public function getCrnproductscategory(){
		$allpros=array();
		$all_pros = BigProduct::select('id','sup_product_id')->get();
		foreach($all_pros as $pros){
			$allpros[$pros->sup_product_id]=$pros->id;
		}
		$all_pros=array();
		$allcats=array();
		$all_cats = BigCategory::select('id','sup_category_id')->get();
		foreach($all_cats as $pros){
			$allcats[$pros->sup_category_id]=$pros->id;
		}
		$all_cats =array();
		$end_point='rest/catalog/productscategories.json';
		if(BIGBUG_MODE=='PRODUCTION'){
			$curl_url=BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=BIGBUY_API_KEY;
		}
		else{
			$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point."?isoCode=".ISOCODE;
			$api_key=SANDOX_BIGBUY_API_KEY;
		}
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer '.$api_key
		));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$jsonArrayResponse = json_decode($response);
		$response=array();
		if(!empty($jsonArrayResponse)){
			$allbigproductcat=array();
			$allbigprcat = ProductCategories::select('sup_product_category_id')->get();
			foreach($allbigprcat as $cat){
				$allbigproductcat[$cat->sup_product_category_id]=$cat->sup_product_category_id;
			}
			$allbigprcat=array();
			foreach($jsonArrayResponse as $k => $resp){
				if(!isset($allbigproductcat[$resp->id])){
					$ProductCategories = new ProductCategories;
					$ProductCategories->sup_product_category_id = $resp->id ;
					if(isset($allpros[$resp->product])){
						$ProductCategories->product_id = $allpros[$resp->product];
					}
					else{
						$ProductCategories->product_id = 0;
					}
					if(isset($allcats[$resp->category])){
						$ProductCategories->category_id = $allcats[$resp->category];
					}
					else{
						$ProductCategories->category_id = 0;
					}
					$ProductCategories->sup_product_id = $resp->product;
					$ProductCategories->sup_category_id = $resp->category;
					$ProductCategories->save();
				}
			}
		}
		echo "completed";
		die;
	}


}
