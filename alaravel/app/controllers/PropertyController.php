<?php
class PropertyController extends BaseController {
public function getChec(){

	echo '<pre>';
	print_r($_SERVER['DOCUMENT_ROOT']);

}

	public function postIndex(){
		return Redirect::to('/');
	}

	public function postLocations(){

		$index = $this->_get_index_values();
		if(Request::ajax()){

			$l= Listing::select('state')->where('active','=',1)->groupBy('state')->get();

			$loc = [];

			foreach($l as $state){
				$s = strtolower($state->state);
				$loc[] = array('idCountry'=>$s, 'nameCountry'=>ucwords($s));
			}

			$data = $loc;
			return $data;
		}else{
			return Redirect::back();
		}

	}

	public function postMapsearch(){

		$locations = [];

		if(Input::has('location')){
			$loc = Input::get('location');
			foreach($loc as $l){
				$locations[] = $l['idCountry'];
			}
		}
		if(Input::has('buy_rent')){
			$buy_rent = Input::get('buy_rent');
		}
		if(Input::has('list_type')){
			$list_type = Input::get('list_type');
		}

		 $center = array(23.90, 70.34);
			$l = new Listing;

			$ll = 	$l->where(function($l) use ($locations){
						foreach($locations as $location){
							$l->orWhere('state','like','%'.$location.'%');
						}
					});
			if(isset($buy_rent)){$ll = $ll->where('status', '=', $buy_rent);}
			if(isset($list_type)){$ll = $ll->where('list_type', '=', $list_type);}

			//$zz = $ll->toSql();
			//print_r(Input::get());
			//print_r($zz);die;

			$zz = $ll->get();
			//print_r($zz);die;
			$cord = [];
			$data = [];
			$i = 0;
			foreach($zz as $z){$i++;
				$prc = rand(10,99);
				$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 98px; height: 50px;width: 97px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
				$center = array($z->map_lat, $z->map_long);
			}

			$sidelisting = '';

			foreach($zz as $p){
				$url = URL::to('microsite/listing/'.$p->id);
				$qwe = '<a target="_blank" href="'.$url.'" class="list_a"><div class="row">';
				$url = URL::to($p->img);
				$qwe .= '<div class="col-md-6"><img style="height:80px;width: 127px;" class="img-responsive" src="'.$url.' "></div><div class="col-md-6"> <div class="">';
				$qwe .= '<div style="float:left;padding:0px;padding-right:5px;"><b>';
				if($p->beds != 0){$beds = $p->beds.' BHK';}
				$qwe .= $beds.'</b></div>';
				$qwe .='<div style="float:left;padding:0px;padding-right:5px;"><i>'.$p->area.'sqft.</i></div></div><div style="clear:both;"></div>';
				$qwe .= '<div>'.$p->location.'</div></div></div> </a>';
				$sidelisting .=$qwe;
			}
			if($sidelisting == ''){$sidelisting = 'No records found.';}
			$jsn = array(
				'center'=>array('lat'=>$center[0], 'lng'=> $center[1]),
				'max'	=> $i,
				'list'	=> $data,
				'listings'=>array('list'=>$sidelisting)
			);
			$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);
		 echo $jsn;
	}


	public function postTraveltime(){

	//echo '<pre>';print_r(Input::all());die;

	if(Input::has('ttime')){
		$time = (float)Input::get('ttime');
	}
	if(Input::has('tmode')){
		$mode = Input::get('tmode');
	}
	if(Input::has('tdistance')){
		$distance = (float)Input::get('tdistance');
	}
	if(Input::has('prop_type')){
		$pt = Input::get('prop_type');
		$tp = [];
		foreach($pt as $p){
			$tp[] = 'z.list_type = '."'$p'".' ';
		}
		$prop = implode(' OR ', $tp);
		$prop = " AND ($prop) ";
	}else{$prop='';}
	if(Input::has('bedtype')){
		$beds = Input::get('bedtype');
		$beds = " AND z.beds = '$beds' ";
	}else{$beds='';}

	if(Input::has('buy_rent')){
		$buy_rent = Input::get('buy_rent');
		$buy_rent = " AND z.status = '$buy_rent' ";
	}else{$buy_rent='';}

	$lat = Input::get('lat');
	$lng = Input::get('lng');
	$dist= 2000;

	$key = "AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc";

	$ugeo = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$key";

	$gloc = [];

	$ret = $this->call($ugeo);

	foreach($ret['data']['results'] as $r){
		foreach($r['address_components'] as $a){

			if((in_array('administrative_area_level_1', $a['types'])) || (in_array('administrative_area_level_2', $a['types']))){
				$gloc[] = $a['long_name'];
			}

		}
	}

	$gloc = array_unique($gloc);


	if(isset($mode)){

			$dbr = DB::select('SELECT  z.*, p.distance_unit * DEGREES(ACOS(COS(RADIANS(p.latpoint)) * COS(RADIANS(z.map_lat)) * COS(RADIANS(p.longpoint) - RADIANS(z.map_long)) + SIN(RADIANS(p.latpoint)) * SIN(RADIANS(z.map_lat)))) AS distance_in_km
		  FROM realestate_listing AS z
		  JOIN (
				SELECT  '.$lat.'  AS latpoint,  '.$lng.' AS longpoint,
						'.$dist.' AS radius,      111.045 AS distance_unit
			) AS p ON 1=1
		  WHERE (z.map_lat
			 BETWEEN p.latpoint  - (p.radius / p.distance_unit)
				 AND p.latpoint  + (p.radius / p.distance_unit)
			AND z.map_long
			 BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
				 AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
			  )
			'.$buy_rent.' '.$prop.' '.$beds.'
		  ORDER BY distance_in_km');
	}else{
		$buy_rent = Input::get('buy_rent');
		$beds = Input::get('bedtype');
		$qdbr = new Listing;
		if(isset($gloc)){$qdbr = $qdbr->whereIn('state', $gloc);}
		if(isset($buy_rent)){$qdbr = $qdbr->where('status', '=', $buy_rent);}
		if(isset($pt)){$qdbr = $qdbr->whereIn('list_type', $pt);}
		if(isset($beds)){$qdbr = $qdbr->where('beds', '=', $beds);}
		$dbr = $qdbr->get();
	}

			$dist = 0;
			$q = [];

			foreach($dbr as $a){
				//$a = (array)$a;
				$q[] = $a->map_lat.','.$a->map_long;
			}

			if(!empty($q)){


				if(isset($distance)){

					$q = implode('|', $q);

					$key = "AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc";

					$destinations = $q;

					$origins = $lat.','.$lng;

					if(isset($mode)){
						$url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=$mode&origins=$origins&destinations=$destinations&key=$key";
					}else{
						$url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$origins&destinations=$destinations&key=$key";
					}

					$ret = $this->call($url);

					$zz = [];
					$i = $j = 0;

					if($ret['status'] == 1){
						$data = $ret['data'];
						foreach($data['rows'][0]['elements'] as $row){
							if($row['status'] == 'OK'){
								$dist = (float)($row['distance']['value']/1000);
								$duration = (float)($row['duration']['value']/60.0);
								//if(isset($time)){echo "time: $time duration: $duration \n";}
								//if(isset($distance)){echo "distance: $distance dist: $dist \n";}
								if((isset($time)) && ($time >= $duration)){
									$zz[] = $dbr[$i];
									$j++;
								}
								if((isset($distance)) && ($distance >= $dist)){
									$zz[] = $dbr[$i];
									$j++;
								}
							}
							$i++;
						}
					}else{
					$sidelisting = 'No records found.';
					$jsn = array(
						'center'=>array('lat'=>$lat, 'lng'=> $lng),
						'max'	=> 0,
						'list'	=> '',
						'listings'=>array('list'=>$sidelisting)
					);
					$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);
					echo $jsn;
								}
				}else{$zz = $dbr;}
	//$ret = DB::table('listing')->select($dbr)->get();
	  $data = [];$i = $j = 0;
				foreach($zz as $z){$i++;$j++;
					$prc = rand(10,99);
					$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 98px; height: 50px;width: 97px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
					$center = array($z->map_lat, $z->map_long);
				}
				$sidelisting = $sideproj = $sidelocality = '';
				$listids = $localities = [];
				foreach($zz as $p){
					$url = URL::to('microsite/listing/'.$p->id);
					$qwe = '<a target="_blank" href="'.$url.'" class="list_a"><div class="row">';
					$url = URL::to($p->img);
					$qwe .= '<div class="col-md-6"><img style="height:80px;width: 127px;" class="img-responsive" src="'.$url.' "></div><div class="col-md-6"> <div class="">';
					$qwe .= '<div style="float:left;padding:0px;padding-right:5px;"><b>';
					if($p->beds != 0){$beds = $p->beds.' BHK';}
					$qwe .= $beds.'</b></div>';
					$qwe .='<div style="float:left;padding:0px;padding-right:5px;"><i>'.$p->area.'sqft.</i></div></div><div style="clear:both;"></div>';
					$qwe .= '<div>'.$p->location.'</div></div></div> </a>';
					$sidelisting .=$qwe;
					$listids[] = $p->project_id;
					$localities[] = $z->locality;
				}
				if($sidelisting == ''){$sidelisting = 'No records found.';}

				$listids = array_unique($listids);

				$localities = array_unique($localities);

				$sideproj = $sideloc = '';

				foreach($localities as $k=>$v){
					$local = preg_replace('/\P{Xan}+/u', '-', $v);

						$url_loc = URL::to('locality-properties/'.$local);
						$url_proj = URL::to('locality-project/'.$local);
						$qwe = '<div class="list_a" style="padding:5px;"><div class="row"><div class="col-md-6"><h4>'.$local.'</h4><h6>1321321 sq.ft.</h6></div>';
						$qwe .= '<div class="col-md-6"><div></div> </div>';
						$qwe .='<div class="col-md-12 clearfix"><div class="col-md-6" style="float:left;padding:5px 0px;padding-right:17px;"><a target="_blank" href="'.$url_loc.'">See all properties</a></div>';
						$qwe .= '<div class="col-md-6" style="float:left;padding:5px 0px;padding-left:17px;"><a target="_blank" href="'.$url_proj.'">See all projects</a></div><div style="clear:both;"></div>';
						$qwe .= '</div></div></div>';
						$sideloc .=$qwe;



				}

				if($sideloc == ''){$sideloc = 'No records found.';}

				if(!empty($listids)){
					$zzz = Project::whereIn('id', $listids)->get();
					foreach($zzz as $p){
						$url = URL::to('microsite/project/'.$p->id);
						$qwe = '<a target="_blank" href="'.$url.'" class="list_a"><div class="row">';
						$url = URL::to($p->header);
						$qwe .= '<div class="col-md-6"><img style="height:80px;width: 127px;" class="img-responsive" src="'.$url.' "></div><div class="col-md-6"> <div class="">';
						$qwe .='<div style="float:left;padding:0px;padding-right:5px;"><b>'.$p->name.'</b></div><div style="float:left;padding:0px;padding-right:5px;"><i>'.$p->project_area.'sqft.</i></div></div><div style="clear:both;"></div>';
						$qwe .= '<div>'.$p->location.'</div></div></div> </a>';
						$sideproj .=$qwe;
					}
					if($sideproj == ''){$sideproj = 'No records found.';}
				}else{$sideproj = 'No records found.';}
				$jsn = array(
					'center'=>array('lat'=>$lat, 'lng'=> $lng),
					'max'	=> $j,
					'list'	=> $data,
					'listings'=>array('list'=>$sidelisting, 'proj'=>$sideproj, 'local'=>$sideloc)
				);
				$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);
			 echo $jsn;
		}else{
			$sidelisting = 'No records found.';
			$sideproj = 'No records found.';
			$sideloc = 'No records found.';
			$jsn = array(
				'center'=>array('lat'=>$lat, 'lng'=> $lng),
				'max'	=> 0,
				'list'	=> '',
				'listings'=>array('list'=>$sidelisting, 'proj'=>$sideproj, 'local'=>$sideloc)
			);
			$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);
			echo $jsn;
		}

	}


	function call($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		$data = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if(curl_errno($ch)){
			$data = "Error: ".curl_error($ch);
			$ret = array('status'=>0, 'httpstatus'=>$status, 'data'=>$data);
		return $ret;
		}else{
			$data = json_decode($data, true);
			curl_close($ch);
		}
		$ret = array('status'=>1, 'httpstatus'=>$status, 'data'=>$data);
	return $ret;

	}


public function getMapsearchh(){
		$index = $this->_get_index_values();

		$ip = $_SERVER['REMOTE_ADDR'];
		$ip = '124.253.49.186';
		$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
		$center = explode(',', $details->loc);
		$l = new Listing;
		$zz = $l->where('address', 'like', '%'.$details->city.'%')->get();

		$cord = [];
		$data = [];
		$i = 0;
		foreach($zz as $z){$i++;
			$prc = rand(10,99);
			$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 98px; height: 50px;width: 97px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
			$cord[$z->name] = array('lat'=>$z->map_lat, 'lng'=>$z->map_long);
		}

		$jsn = array(
			'center'=>array('lat'=>$center[0], 'lng'=> $center[1]),
			'max'	=> $i,
			'list'	=> $data
		);

		$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);

		return View::make('mapsearch',array('Index'=>$index, 'jsn'=>$jsn));
}

	public function _get_phone_number(){

	$phone = '1-888-999-5454';

	return $phone;
	}

public function getX12($sender = null,$sender_address = null,$code = null){

$headers = array();

$headers[] = "MIME-VERSION: 1.0";
$headers[] = "CONTENT-TYPE:text/html; charset=iso-8859-1";
$headers[] = "FROM: SENDER NAME <admin@localhost.com>";
$headers[] = "SUBJECT: TEST";
$headers[] = "Cc: admin@localhost.com";
$headers[] = "Bcc: adminn@localhost.com";
$headers[] = "X-MAILER: PHP/".phpversion();

$message = View::make('emails.confirm',array('code'=>'Your Code Here!!'));

$result = mail('code0crawler@gmail.com','Subject',$message,implode("\r\n",$headers));

return $result;

}


public function getX13($sender = null,$code = null){

Mail::send('emails.confirm',array('code'=>$code),function($message){

$message->to($sender,'Admin')->subject('Confirmation Code');

});

if(count(Mail::failures()) > 0){dd('error');}

return true;
}





public function Sendmail($to = null, $subject = null, $message = null){

$headers = array();

$headers[] = "MIME-VERSION: 1.0";
$headers[] = "CONTENT-TYPE:text/html; charset=iso-8859-1";
$headers[] = "FROM: SENDER NAME <admin@localhost.com>";
$headers[] = "SUBJECT: TEST";
$headers[] = "Cc: admin@localhost.com";
$headers[] = "Bcc: adminn@localhost.com";
$headers[] = "X-MAILER: PHP/".phpversion();

$message = View::make('emails.confirm',array('code'=>$message));

$result = mail($to,$subject,$message,implode("\r\n",$headers));

return $result;

}

public function send_user_confirmation_code($to = null, $code = null){

$headers = array();

$headers[] = "MIME-VERSION: 1.0";
$headers[] = "CONTENT-TYPE:text/html; charset=iso-8859-1";
$headers[] = "FROM: GPC-Admin <gpc-admin@localhost.com>";
$headers[] = "SUBJECT: Confirmation Code";
$headers[] = "Cc: gpc-admin@localhost.com";
$headers[] = "Bcc: gpc-adminn@localhost.com";
$headers[] = "X-MAILER: PHP/".phpversion();

$message = View::make('emails.confirm',array('code'=>$code));

$result = mail($to,'Confirmation Code',$message,implode("\r\n",$headers));

return $result;

}




public function save_file($dest, $file){

	$file_name = $file->getClientOriginalName();

	$ret = $file->move($dest,$file_name);

	if($ret){return $dest.'/'.$file_name;}return null;
}



	public function _get_social_media(){

		$top = array();
		$f = array('title'=>'facebook','name'=>'facebook','url'=>'https://www.facebook.com/globalpropertycowboys/','class'=>'fa fa-facebook fa-3x','li-class'=>'facebook');
		$top['facebook'] = $f;

		$t = array('title'=>'twitter','name'=>'twitter','url'=>'https://twitter.com/CowboyGlobal','class'=>'fa fa-twitter','li-class'=>'twitter');
		$top['twitter'] = $t;

		$t = array('title'=>'youtube','name'=>'youtube','url'=>'https://www.youtube.com/channel/UCHINpiUff1YdH3jkIjVrksA','class'=>'fa fa-youtube','li-class'=>'youtube');
		$top['youtube'] = $t;

		$t = array('title'=>'instagram','name'=>'instagram','url'=>'https://www.instagram.com/globalpropertycowboys14/','class'=>'fa fa-instagram','li-class'=>'instagram');
		$top['instagram'] = $t;

	return $top;
	}


	public function _get_top_menu(){

	$menu = array();

	$menu['Webshop']		=	array('name'=>'Webshop','url'=>'#','class'=>'','child'=>array());
	$menu['Two Week Trial']	=	array('name'=>'Two Week Trial','url'=>'#','class'=>'','child'=>array());
	$menu['About Us']		=	array('name'=>'About Us','url'=>'about','class'=>'','child'=>array());
	$menu['Contact Us']		=	array('name'=>'Contact Us','url'=>'contact','class'=>'','child'=>array());
	$menu['Interior']		=	array('name'=>'Interior','url'=>'#','class'=>'','child'=>array());
	$menu['Experts']		=	array('name'=>'Experts','url'=>'#','class'=>'','child'=>array());

	return $menu;
	}


	public function _get_property_listing(){

	$prop = array();

	$prop[0]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/4.jpg','address'=>'address1');
	$prop[1]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/5.jpg','address'=>'address1');
	$prop[2]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/4.jpg','address'=>'address1');
	$prop[3]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/5.jpg','address'=>'address1');
	$prop[4]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/4.jpg','address'=>'address1');
	$prop[5]	= array('name'=>'name1','url'=>'/property/projectsite/1','img'=>'/property/packages/realestate/img/full/5.jpg','address'=>'address1');

	return $prop;
	}


	public function _get_experts(){

	$experts = array();

	$experts[0]	= array('name'=>'name1','img'=>'/property/packages/realestate/img/expert.jpg','address'=>'address1');
	$experts[1]	= array('name'=>'name1','img'=>'/property/packages/realestate/img/expert.jpg','address'=>'address1');
	$experts[2]	= array('name'=>'name1','img'=>'/property/packages/realestate/img/expert.jpg','address'=>'address1');

	return $experts;
	}

	public function _get_contact_us(){

	$contact = array();

	$contact['address'] = '101 Front St.<br />San Diego, CA 92101<br />Netherlands ';
	$contact['phone'] = '619-555-9785';
	$contact['email'] = 'mail@yourdomain.com';

	return $contact;
	}

	public function _get_copyright(){

	$copy = 'Global Property Cowboys';

	return $copy;
	}


	public function _get_index_values(){

	$index = array();

	$index['phone']			= $this->_get_phone_number();

	$index['social-media']	= $this->_get_social_media();

	$index['top-menu']		= $this->_get_top_menu();

	$index['slider']		= '';

	$index['property-list']	= $this->_get_property_listing();

	$index['experts']		= $this->_get_experts();

	$index['contact-us']	= $this->_get_contact_us();

	$index['copyright']		= $this->_get_copyright();

	return $index;
	}

	public function getIndex()
	{


	//if(Auth::viaRemember()){dd('!');}

	$index = $this->_get_index_values();

	$list = new ListingController;

	$listings = Listing::where('active','=','1')->paginate(6);

	$index['listings'] = $listings;

	$index['index'] = 'index';

	$countries = Countries::all();

	$cc = [];

	foreach($countries as $country){
		if($country['location'] == 'eu'){
			$cc['eu'][] = $country;
		}
		else{
			$cc['world'][] = $country;
		}
	}

	return View::make('index',array('Index'=>$index, 'countries'=>$cc));
	}

	public function getListings(){

	$list = new Listing;

	$index = $this->_get_index_values();

	$listings = $list->where('active','=','1')->paginate(10);

	return View::make('listings',array('Index'=>$index,'listings'=>$listings));

	}


	public function getTest(){

	return View::make('test');

	}


	public function getListingsite($id){

	$index = $this->_get_index_values();

	if(Auth::check()){

	$list = new Listing;

	$listing = $list->where('id','=',$id)->firstorfail();

	return View::make('listingsite',array('listing'=>$listing),array('Index'=>$index));
	}
	else{

	return Redirect::to('login')->with('message',"Please Login To Get Access To The Features!!");

	}

	}




	public function _get_about_us(){

	$about = <<<Q
        I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.

        I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.

        I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.

        I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.

        I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
Q;
	return $about;
	}

	public function getAbout(){

	$index			= $this->_get_index_values();

	$index['about']	= $this->_get_about_us();

	return View::make('about',array('Index'=>$index));
	}

	public function getContact(){

	$index			= $this->_get_index_values();

	return View::make('contact',array('Index'=>$index));
	}


	public function getProjectsite($project='null'){

	if($project == 'null'){return Redirect::to('projectsite/project');}

	$index			= $this->_get_index_values();


	$p = Project::where('name','=',$project)->firstorfail();

	$u = BHKUnit::where('project_id','=',$p->id)->get();


	return View::make('projectsite',array('Index'=>$index,'p'=>$p,'u'=>$u));
	}

	public function getLogin(){

	$index			= $this->_get_index_values();

	return View::make('login',array('Index'=>$index));
	}


	public function postLogin(){

	App::setLocale(Session::get('lang'));

	$validator = Validator::make(Input::all(), array(

		'username'=> 'required|alpha_num|min:1|max:17',
		'password'=> 'required|between:1,13',

	));

	if(Request::ajax()){


		if(!$validator->passes()){$data['message']='';$data['success'] = '0';$data['errors'] = $validator->errors();$data['d'] = count($validator->errors());return $data;}

		$data = array();

		$user = Input::get('username');
		$password = Input::get('password');

		$remember = Input::get('remember');
		if($remember == 1){$remember = true;}else{$remember = false;}
		if(Auth::attempt(array('username'=>$user,'password'=>$password),$remember)){

		if(Auth::user()->status == 1){

		$data['url'] = URL::to('profile');$data['success'] = '1';


		return $data;
		}
		else{Auth::logout();$data['message'] = 'Please Confirm Before Log In';$data['success'] = '2';return $data;}

		}
		else{$data['success'] = '0';$data['message'] = 'Unable To Log In: Please provide your correct username / password combination.';}

		return $data;

	}

	$user = Input::get('user');
	$password = Input::get('password');

	if(Auth::attempt(array('username'=>$user,'password'=>$password))){

	if(Auth::user()->status == 1){

	return Redirect::to('profile');
	}
	else{Auth::logout();return Redirect::to('confirmuser');}

	}

	return Redirect::to('login')->with('message','Unable To Log In: Please provide us your correct username/password combination.')->withInput();
	}


	public function getChangepassword(){

	if(Auth::check()){

	$index			= $this->_get_index_values();

	return View::make('changepassword',array('Index'=>$index));
	}
	return Redirect::to('/');

	}

	public function postChangepassword(){

	if(Auth::check()){

	$index			= $this->_get_index_values();

	$pass =  Input::get('password');
	$pass =  Input::get('cpassword');

	if($pass == $cpass){$user = Auth::user(); $user->password = $pass; $user->save();}

	return Redirect::to('login')->with('message','Your Password Has Been Changed, Please Login To Continue!!');

	}
	return Redirect::to('/');

	}

	public function getLostpassword(){

	$index			= $this->_get_index_values();

	return View::make('lostpassword',array('Index'=>$index));
	}

	public function postLostpassword(){

	App::setLocale(Session::get('lang'));

	$validator = Validator::make(Input::all(), array(

		'username'=> 'required|alpha_num|min:1|max:17',
		'password'=> 'required|between:1,13',

	));

	if(Request::ajax()){

	if(!$validator->passes()){$data['message']='';$data['success'] = '0';$data['errors'] = $validator->errors();$data['d'] = count($validator->errors());return $data;}

		$data = array();

		$user = Input::get('user');
		$email = Input::get('email');

		try{
		$u = WebUser::where('username','=',$user)->firstorfail();
		}
		catch(Exception $e){
		$data['success'] = '0'; $data['message'] = 'Please Provide Correct Information0!!';
		return $data;
		}
		if($u->email == $email){
		$code = dechex(mt_rand(1000,99999));
		$u->ccode = $code;
		$u->save();
		$mailc = $this->send_user_confirmation_code($u->email,"Password Confirmation Code: $code");
		if($mailc === false){$data['success'] = '0'; $data['message'] = 'Unable To Send Email, Please Try Again!!';return $data;}

		$data['success'] = '1'; $data['message'] = "We've Sent An Email To Your Mail Account, Please Check It For Confirmation Code!!";
		return $data;
		}


		$data['success'] = '0'; $data['message'] = 'Please Provide Correct Information1!!';
		return $data;

	}

		$index	= $this->_get_index_values();

		$user	= Input::get('user');
		$email	= Input::get('email');

		if(Auth::attempt(array('username'=>$user,'email'=>$email))){Auth::logout();

		//send mail
		return Redirect::to('confirmpassword');

		}

		return Redirect::back()->with('message','Please Provide Correct Information!!');
	}

	public function getConfirmpassword(){

		$index			= $this->_get_index_values();
		return View::make('lconfirmuser',array('Index'=>$index));

	}

	public function postConfirmpassword(){

	if(Request::ajax()){

		$data = array();

		$user = Input::get('user');
		$email = Input::get('email');
		$ccode = Input::get('ccode');
		$password = Input::get('password');
		$password_confirmation = Input::get('cpassword');

		try{
		$u = WebUser::where('username','=',$user)->firstorfail();
		if(($u->email == $email) && ($u->ccode == $ccode) && ($password == $password_confirmation)){
		$u->password = Hash::make($password);
		$u->save();
		$data['success'] = '1'; $data['message'] = "Password Changed Successfully, Now You Can Login!!";
		return $data;
		}
		}
		catch(Exception $e){
		$data['success'] = '0'; $data['message'] = 'Please Provide Correct Information!!';
		return $data;
		}

		$data['success'] = '0'; $data['message'] = 'Please Provide Correct Information!!';
		return $data;

	}

		$index	= $this->_get_index_values();

		$user	= Input::get('user');
		$email	= Input::get('email');
		$code	= Input::get('ccode');

		if(Auth::attempt(array('username'=>$user,'email'=>$email,'ccode'=>$code))){

		return Redirect::to('changepassword');

		}
		else{return Redirect::back();}

	}

	public function getConfirmationemail(){

	$index			= $this->_get_index_values();

	return View::make('confirmemail',array('Index'=>$index));

	}

	public function postConfirmationemail(){

	$index			= $this->_get_index_values();

	$email = Input::get('email');
	$password = Input::get('password');
	if(Auth::attempt(array('email'=>$email,'password'=>$password,'status'=>0))){

	$mailc = $this->send_user_confirmation_code($email,"Confirmation Code: $code");

	if($mailc == true){return Redirect::to('confirmuser');}

	}

	return Redirect::to('confirmationemail')->with('message','Please Enter Correct information!!');

	}

	public function getConfirmuser(){

	$index			= $this->_get_index_values();

	return View::make('confirmuser',array('Index'=>$index));

	}

	public function postConfirmuser(){

	$username = Input::get('user');
	$password = Input::get('password');
	$confirm_code = Input::get('ccode');

	$check_code = '';
	$data = array();

	if(($confirm_code === null) || ($confirm_code == '')){
	$check_code = true;
	}
	else{$check_code = false;}

	if(Request::ajax()){

	if($check_code){
	$data['success'] = '0';
	$data['message'] = 'Please Enter A Valid Confirmation Code!!';
	return $data;
	}

	if(Auth::attempt(array('username'=>$username,'password'=>$password, 'ccode'=>$confirm_code))){

	$user = Auth::user();
	$user->ccode = '';
	$user->status = 1;
	$user->save();

	$data['success'] = '1';
	$data['message'] = "You Are Now A Registered user<br />We've Sent An Email To Your Mail Account, Please Check It For Confirmation Code!!<br/>(It May Take Some Time, So Have patience)";
	return $data;
	}
	else{

	$data['success'] = '2';
	$data['message'] = 'Please Enter Valid Details??';
	return $data;

	}

	}



	if($check_code){return Redirect::to('register')->with('message','Error: Please Enter A Valid Confirmation Code!!');}


	if(Auth::attempt(array('username'=>$username,'password'=>$password, 'ccode'=>$confirm_code))){

	$user = Auth::user();
	$user->ccode = '';
	$user->status = 1;
	$user->save();

	return Redirect::to('/');
	}

	return Redirect::to('register')->with('message','Error: Either You Are Not A Register User Or Your Confirmation Code Is Wrong!!');

	}


	public function getDashboard(){

	$index = $this->_get_index_values();

	if(Auth::check()){

	$dashboard_links = array();//array('url'=>'','name'=>'')

	switch(Auth::user()->type){

	case 0:{
	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');
	break;}//admin
	case 1:{
	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');

	break;}//builder
	case 2:{

	break;}//agent
	case 3:{
	$dashboard_links[] = array('url'=>'','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'postproject','name'=>'Post Property');
	break;}//buyer/seller
	case 4:{

	break;}//Non-Active User
	default:{break;}//Something else

	}

	//return View::make('dashboard',array('dlinks'=>$dashboard_links,'Index'=>$index));

	return View::make('dashboard',array('dlinks'=>$dashboard_links,'Index'=>$index));
	}

	return Redirect::to('login')->with('message','Please Sign In To Get Access This Page!!');
	}

	public function getLogout(){
	Auth::logout();
	return Redirect::to('/')->with('message','You are Successfully Logged Out!!');
	}

	public function getPlans(){


	return View::make('plans',array('page'=>'plans'));
	}

	public function getShowplans(){


	return View::make('showplans',array('page'=>'plans'));
	}

	public function getRegister(){

	//if(Auth::check()){

	$index			= $this->_get_index_values();

	return View::make('register',array('Index'=>$index));
	//}
	//else{

	//return Redirect::to('login')->with('message','Please Login To Continue!!');

	//}
	}


	public function postRegister(){


	if(Request::ajax()){

	App::setLocale(Session::get('lang'));

	$data = array();

	$validator = Validator::make(Input::all(), WebUser::$rules);

	if($validator->passes()){
	try{
	$user = new WebUser;

	$user->username = Input::get('username');
	$user->firstname = Input::get('first_name');
	$user->lastname = Input::get('last_name');
	$user->email = Input::get('email');
	$user->password = Hash::make(Input::get('password'));
	if(Input::hasFile('file')){
		$file = Input::file('file');
		$dest = 'public/user_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$user->img = $ret;}
	}
	else{$user->img = '/packages/images/user.png';}

	$code = dechex(mt_rand(1000,99999));

	$user->ccode = $code;

	$user->save();
	}
	catch(Exception $e){
	$data['success'] = '0';
	$data['message'] = 'Please Select Another Username Or Email!!';
	return $data;
	}

	$mailc = $this->send_user_confirmation_code($user->email,"Registration Confirmation Code: $code");
	//$mailc = true;
	//$mailc = $this->X13('','<a href="laravel.inviolateinfotech.com">Confirmation Code</a>');

	if($mailc === true){
	$data['success'] = '1';
	$data['message'] = "You Are Now A Registered user!!<br />We've Sent An Email To Your Mail Account, Please Check It For Confirmation Code!!(It May Take Some Time, So Have patience)";
	return $data;
	}
	else{
	$data['success'] = '2';
	$data['message'] = "You are now a registered user.<br />Unable to send Email We'll contact you soon.";
	return $data;
	}
	}


	$data['success']	= '0';
	$data['message']	= "";
	$data['errors']		= $validator->errors();


	return $data;
	}

	dd('!');

	$validator = Validator::make(Input::all(), WebUser::$rules);

	if($validator->passes()){

	$user = new WebUser;

	$user->username = Input::get('username');
	$user->firstname = Input::get('firstname');
	$user->lastname = Input::get('lastname');
	$user->email = Input::get('email');
	$user->password = Hash::make(Input::get('password'));

	$code = dechex(mt_rand(1000,99999));

	$user->ccode = $code;
	$user->save();

	$mailc = $this->send_user_confirmation_code($user->email,"Confirmation Code: $code");

	if($mailc === true){
	return Redirect::to('login')->with('message',"You are now a registered user. $user->firstname!!");
	}
	else{return Redirect::to('login')->with('message',"You are now a registered user.<br />Unable to send Email We'll contact you soon.");}
	}


	return Redirect::to('register')->with('message','Following errors occured')->withErrors($validator)->withInput();
	}

	public function postUseredit(){

	if(Auth::check()){
	$user = WebUser::where('id','=',Auth::user()->id)->firstorfail();

	$user->username = Input::get('username');
	$user->firstname = Input::get('firstname');
	$user->lastname = Input::get('lastname');
	$user->email = Input::get('email');
	$user->address = Input::get('address');
	$user->password = Hash::make(Input::get('password'));

	$user->save();

	return Redirect::to('profile')->with('message',"Your profile is updated now!!");

	//}

	return Redirect::to('profile')->with('message','Following errors occured')->withErrors($validator)->withInput();

	}
	else{return Redirect::to('login')->with('message','Please Sign In To Get Access!!');}

	}

	public function getCreateproject(){

	if(Auth::check()){

	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');

	$company = Company::where('user_id','=',Auth::user()->id)->get();

	$c = array();

	foreach($company as $comp){

	$c[$comp->id] = $comp->name;

	}

	return View::make('createproject',array('dlinks'=>$dashboard_links,'company'=>$c));
	}
	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

public function getUsers(){

$users = WebUser::paginate(7);

return View::make('users',array('users'=>$users,'page'=>'user'));

}

public function getUser(){

$users = WebUser::all();

return View::make('user',array('page'=>'user'));

}

	public function getUprofile(){

	$user = Auth::user();

	return View::make('uprofile',array('user'=>$user, 'page'=>'user'));

	}

	public function postUprofile(){

	$user = Auth::user();

	$user->username = Input::get('username');
	$user->firstname = Input::get('firstname');
	$user->lastname = Input::get('lastname');
	if(Input::hasFile('file')){
		$file = Input::file('file');
		$dest = 'public/user_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$user->img = $ret;}
	}
	$user->phone = Input::get('phone');
	$user->email = Input::get('email');
	$user->website = Input::get('website');
	$user->country = Input::get('country');
	$user->state = Input::get('state');
	$user->city = Input::get('city');

	$user->save();

	return Redirect::back()->with('message',"Your profile is updated now!!");

	}


	public function getChpassword(){
		return View::make('chpassword',array('page'=>'user'));
	}

	public function postChpassword(){
		$pass	= Input::get('password');
		$cpass	= Input::get('password_confirmation');
		if($pass == $cpass){
			Auth::user()->password = Hash::make($pass);
			return Redirect::back()->with('message','congratulations you have changed your password.');
		}
		return Redirect::back()->with('message','Please provide correct details.');
	}


	public function postCreateproject(){

	if(Auth::check()){

	$project = new Project;

	$project->name = Input::get('project_name');
	$project->description = Input::get('project_description');
	$project->phone = Input::get('phone');
	$project->email = Input::get('email');
	$project->website = Input::get('website');
	$project->address = Input::get('address');
	$project->country = Input::get('country');
	$project->state = Input::get('state');
	$project->city = Input::get('city');
	$project->company_id = Input::get('company');

	$project->save();

	return Redirect::to('createproject')->with('message',"Congratulations You have Added A Project Profile!!");
	}


	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

	public function getPostproject(){

	//if(Auth::check()){return 'hello';}

	return View::make('postproject');
	}

	public function getProfile(){

		if(Auth::check()){

		$list = Listing::where('active','=','1')->orderBy('id','desc')->paginate(4);

		return View::make('profile',array('listings'=>$list));
		}
		return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

	public function postLimages(){

		$pid	= Input::get('pid');
		$id		= Input::get('id');

		$par	= array();
		$iar	= array();

		if($pid != null){
			$im = Images::where('project_id', $pid)->get();
			foreach($im as $m){
			$par[] = URL::to($m->image);
			}
		}

		if($id != null){
			$im = Images::where('unit_id', $id)->get();
			foreach($im as $m){
			$iar[] = URL::to($m->image);
			}
		}

		$arr = array_merge($par,$iar);

		return json_encode($arr);

	}

	public function getUserp($username = null){

	$index = $this->_get_index_values();

	try{
	$user	= WebUser::where('username','=',$username)->firstorfail();
	}
	catch(Exception $e){$user = null;}

	if($user != null){

	return View::make('userp',array('Index'=>$index,'user'=>$user));

	}
	return View::make('userp',array('Index'=>$index,'user'=>$user));

	}


	public function getPprofile(){

		if(Auth::check()){


		return View::make('pprofile');
		}
		return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}


	public function getCompany(){

	if(Auth::check()){

	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');

	return View::make('company',array('dlinks'=>$dashboard_links));
	}
	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

	public function postCompany(){

	if(Auth::check()){

	$company = new Company;

	$company->name = Input::get('company_name');
	if(Input::hasFile('company_logo')){

	$file = Input::file('company_logo');

	$file_name = Input::file('company_logo')->getClientOriginalName();
	$file_destination = public_path().'/../../packages/realestate/adb/img/';

	$uploadSuccess = $file->move($file_destination,$file_name);

	$company->logo = '/property/packages/realestate/adb/img/'.$file_name;

	}
	else{}
	$company->phone = Input::get('phone');
	$company->email = Input::get('email');
	$company->website = Input::get('website');
	$company->address = Input::get('address');
	$company->country = Input::get('country');
	$company->state = Input::get('state');
	$company->city = Input::get('city');
	$company->user_id = Auth::user()->id;

	$company->save();

	return Redirect::to('company')->with('message',"Congratulations You have Added A Company!!");
	}


	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}


	public function getAddunit(){

	if(Auth::check()){

	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');
	$c = array();/*
	$company = Company::where('user_id','=',Auth::user()->id)->get();

	foreach($company as $comp){

	$c[$comp->id] = $comp->name;

	}*/

	$pro = Project::orderBy('id','desc')->get();

	foreach($pro as $p){$c[$p->id] = $p->name;}//$this->ldebug($c);

	return View::make('addunit',array('dlinks'=>$dashboard_links,'projects'=>$c,'page'=>'project'));
	}
	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

	public function postAddunit(){

	if(Auth::check()){
	/*
	$unit = new BHKUnit;
	$unit->type = Input::get('unit_type');
	$unit->price = Input::get('price');
	$unit->area = Input::get('area');
	$unit->furnished = Input::get('furnished');
	$unit->bedrooms = Input::get('bedroom');
	$unit->bathrooms = Input::get('bathroom');
	$unit->balconys = Input::get('balcony');
	$unit->name = $unit->bedrooms.' BHK';
	$unit->project_id = Input::get('project');
	$unit->save();
	*/

	$list = new Listing;
	$list->beds				= Input::get('bedroom');
	$list->project_id 		= Input::get('project');
	$p						= Project::where('id','=',$list->project_id)->firstorfail();
	$list->name				= $p->name;
	$list->location			= $p->address;
	$list->description		= $p->decription;
	if(Input::hasFile('file')){
		$file = Input::file('file');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->floor_plan = $ret;}
	}
	else{$list->img = 'packages/images/home-img1.jpg';}

	if(Input::hasFile('floor_plan')){
		$file = Input::file('floor_plan');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->floor_plan = $ret;}
	}
	else{$list->floor_plan = '';}

	if(Input::hasFile('floor_plan_3d')){
		$file = Input::file('floor_plan_3d');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->floorplan3d = $ret;}
	}
	else{$list->floorplan3d = '';}

	$list->area				= Input::get('area');
	$list->price			= Input::get('price');
	$list->list_type		= Input::get('unit_type');
	$list->status 			= Input::get('list_status');
	$list->build_status 	= Input::get('build_status');
	$list->baths			= Input::get('bathroom');
	$list->balconys 		= Input::get('balcony');
	$list->furnished 		= Input::get('furnished');

	$list->map_lat			= Input::get('latitude');

	$list->map_long 		= Input::get('longitude');

	$list->price_sqft		= Input::get('price_per_sqft');

	if(Input::has('balcony')){
		$list->has_balcony	=	1;
	}
	if(Input::has('bathtub')){
		$list->has_bathtub	=	1;
	}
	if(Input::has('commercial_property')){

		//$commercial_property	= Input::get('commercial_property');
	}
	if(Input::has('monumental_building')){
		//$q = $q->where('has_balcony', '=', 1);
	}
	if(Input::has('ch_builder')){
		$list->has_ch_builder	=	1;
	}
	if(Input::has('fire_place')){
		$list->has_fire_place	=	1;
	}
	if(Input::has('roof_terrace')){
		$list->has_roof_terrace	=	1;
	}
	if(Input::has('sauna')){
		$list->has_sauna	=	1;
	}
	if(Input::has('renewable_energy')){
		$list->has_renewable_energy	=	1;
	}
	if(Input::has('shed_storage')){
		$list->has_shed_storage	=	1;
	}
	if(Input::has('garage')){
		$list->has_garage	=	1;
	}
	if(Input::has('steam_cabin')){
		$list->has_steam_cabin	=	1;
	}
	if(Input::has('jacuzzi')){
		$list->has_jacuzzi	=	1;
	}
	if(Input::has('garden')){
		$list->has_garden	=	1;
	}
	if(Input::has('fixer_upper')){
		$list->has_fixer_upper	=	1;
	}
	if(Input::has('swimming_pool')){
		$list->has_swimming_pool	=	1;
	}
	if(Input::has('elevator')){
		$list->has_elevator	=	1;
	}

	$list->user_id			= Auth::user()->id;
	$list->active			= 1;
	$list->agent			= Auth::user()->username;
	$list->save();


	if(Input::file('files') !== null){

		foreach(Input::file('files') as $f){

			$img = new Images;

			$img->project_id = $list->project_id;
			$img->unit_id = $list->id;

			$dest = 'public/listing_img';
			$ret = $this->save_file($dest,$f);
			$img->image = $ret;

			$img->save();

			}
	}


	return Redirect::to('addunit')->with('message',"Congratulations you've succssfully added a unit to the project!!");
	}

	return Redirect::to('login')->with('message','Please Sign In To Get Access!!');
	}

	public function getAdd360(){
		if(Auth::check()){
			$pro = Project::orderBy('id','desc')->get();
			foreach($pro as $p){$c[$p->id] = $p->name;}
		return View::make('360',array('page'=>'project','projects'=>$c));
		}
	}


/*

echo base_path();

echo app_path();

echo public_path();

echo storage_path();

*/


	public function postAdd360(){
		if(Auth::check()){

			$f360 = new F360;

			$project_id	= Input::get('project');
			$unit_id	= Input::get('unit');
			$type		= Input::get('type');
			if(($project_id != null) && ($project_id != '')){
				if(Input::hasFile('file')){
					$file = Input::file('file');
					$dir = __DIR__.'../../../../public/f360/'.$project_id;

					$dest = 'public/f360/'.$project_id;
					$ret = $this->save_file($dest,$file);
					if($ret != null){
						$f360->image = $ret;
						$f360->project_id 	= $project_id;
						$f360->unit_id		= $unit_id;
						$f360->type			= $type;
						$f360->save();
						echo json_encode(array('status'=>'1', 'img'=>URL::to($ret)));
					}
				}
				else{echo json_encode(array('status'=>'0', 'img'=>''));}
			}else{echo json_encode(array('status'=>'0', 'img'=>''));}
		}
	}

	public function postProjunit(){
		if(Auth::check()){
			$id = Input::get('data');
			$pro = Listing::where('project_id','=',$id)->orderBy('id','desc')->get();
			$html = '';
			foreach($pro as $p){
				$html .= '<option value="'.$p->id.'">'.$p->name.'</option>';
			}
			echo json_encode(array('data'=>$html));
		}
	}


	public function getMedia(){


		return View::make('media');
	}





	public function getMicrosite($type, $id = null){
		if($type == 'listing'){
			$index = $this->_get_index_values();
			$listing = Listing::where('id',$id)->firstorfail();
			return View::make('listingsites',array('listing'=>$listing, 'Index'=>$index));
		}
		else if($type == 'project'){
			$proj = Project::where('id','=',$id)->firstorfail();
			$unit = Listing::where('project_id',$id)->get();
			$images = Images::where('project_id', $id)->get();
			$f360 = F360::where('project_id', $id)->get();
			$amen = Amenities::where('project_id','=',5)->get();
			$index = $this->_get_index_values();
			return View::make('micrositess',array('project'=>$proj,'amenities'=>$amen,'units'=>$unit, 'images'=>$images, 'f360'=>$f360, 'Index'=>$index));
		}
		else{
			return Redirect::back();
		}
	}

	public function postProjectspec(){

	$id = Input::get('project_id');

	$p = Spec::where('project_id','=',$id)->get();

	$d = '<ul>';

		foreach($p as $s){

		$d .= '<li>'.$s->spec.'</li>';

		}
	$d .= '</ul>';

		return $d;
	}

	public function postProjectamenities(){

	$id = Input::get('project_id');

	$p = Amenities::where('project_id','=',$id)->get();

	$d = '<ul>';

		foreach($p as $s){

		$d .= '<li>'.$s->amenities.'</li>';

		}
	$d .= '</ul>';

		return $d;
	}

	public function getShowunits(){

	$list = BHKUnit::where('id','>','0')->paginate(7);

	return View::make('showunits',array('lists'=>$list,'page'=>'project'));
	}

	public function getSsearch(){

	return View::make('searchall');
	}

	public function getSearch($arg1 = null, $arg2 = null){

	$list		= new Listing;
	$index		= $this->_get_index_values();
	$country		= Input::get('country');

	if($arg1 == null){
		$listings = Listing::where('active','=','1')->orderBy('id','desc')->paginate(5);
		$index['listings'] = $listings;
	}else if($arg1 == 'all'){
			$listings = Listing::where('active','=','1')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
	}else if($arg1 == 'buy_rent'){
		if($arg2 == 'sale'){
			$listings = Listing::where('active','=','1')->where('status','=','sale')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'rent'){
			$listings = Listing::where('active','=','1')->where('status','=','rent')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
	}else if($arg1 == 'construction'){
		if($arg2 == 'ready'){
			$listings = Listing::where('active','=','1')->where('status','=','ready')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'underconstruction'){
			$listings = Listing::where('active','=','1')->where('status','=','under')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
	}else if($arg1 == 'property_type'){
		if($arg2 == 'house'){
			$listings = Listing::where('active','=','1')->where('list_type','=','house')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'apartment'){
			$listings = Listing::where('active','=','1')->where('list_type','=','apartment')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'land'){
			$listings = Listing::where('active','=','1')->where('list_type','=','land')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'storage'){
			$listings = Listing::where('active','=','1')->where('list_type','=','storage')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'berth'){
			$listings = Listing::where('active','=','1')->where('list_type','=','berth')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
		if($arg2 == 'substructure'){
			$listings = Listing::where('active','=','1')->where('list_type','=','substructure')->orderBy('id','desc')->paginate(5);
			$index['listings'] = $listings;
		}
	}else if($arg1 == 'beds'){
		$from 	= Input::get('bed_from');
		$to 	= Input::get('bed_to');
		$listings = Listing::where('active','=','1')->where('beds','>=',$from)->where('beds','<=',$to)->orderBy('id','desc')->paginate(5);
		$index['listings'] = $listings;
	}else if($arg1 == 'area'){
		$from 	= Input::get('area_from');
		$to 	= Input::get('area_to');
		$listings = Listing::where('active','=','1')->where('area','>=',$from)->where('area','<=',$to)->orderBy('id','desc')->paginate(5);
		$index['listings'] = $listings;
	}else{
		$listings = Listing::where('active','=','1')->orderBy('id','desc')->paginate(5);
		$index['listings'] = $listings;
	}

	$countries = Countries::all();

	$cc = [];

	foreach($countries as $country){
		if($country['location'] == 'eu'){
			$cc['eu'][] = $country;
		}
		else{
			$cc['world'][] = $country;
		}
	}



	return View::make('ssearch',array('Index'=>$index,'listings'=>$listings, 'countries'=>$cc));
	}

	public function postSearch(){

	$index			= $this->_get_index_values();
	$otype			= Input::get('own_type');
	$location		= Input::get('location');
	$property_type	= Input::get('ptype');
	$budget_min		= Input::get('bmin');
	$budget_max		= Input::get('bmax');



	$country		= Input::get('country');

	$city			= Input::get('city');

	$budget_type	= array($budget_min, $budget_max);

	if($otype == 'buy'){$otype = 'sale';}

	$l = new Listing;

	$li =0;

	if(!empty($property_type)){
		$li = 1;
		$listing = $l->where(function($l) use ($property_type){

		if(isset($country) && ($country != '') && ($country != null)){
			$l->where('country','=',$country);
		}

		if(isset($city) && ($city != '') && ($city != null)){
			$l->where('city','=',$city);
		}

		if(!empty($property_type)){

		foreach($property_type as $v){

		$l->orWhere('list_type','=',$v);

		}
		}

		})->where('status','=',$otype)->where(function($l) use ($budget_type){

			if(empty( $budget_type[0])){$l->orWhere('price','>',0);}
			else{$l->where('price','>',$budget_type[0]);}

			if(empty( $budget_type[1])){}
			else{$l->where('price','<',$budget_type[1]);}

		})->where(function($l) use ($location){

			//$l->orWhere('name','like','%'.$location.'%');

		})->orderBy('id','desc');

	}
	if(!empty($otype)){
		$li = 1;
		$listing = $l->where('status','=',$otype)->orderBy('id','desc');

	}
	if( isset($country) && ($country != '') && ($country != null)){
		$li = 1;
		$listing = $l->where('country','like','%'.strtolower($country).'%')->orderBy('id','desc');

	}
	if(isset($city) && ($city != '') && ($city != null)){
		$li = 1;
		$listing = $l->where('city','like','%'.strtolower($city).'%')->orderBy('id','desc');

	}
	if($li < 1){$listing = $l->where('active','=','1')->orderBy('id','desc');}

	$listing = $listing->paginate(4);



	$ii = 0;
	foreach($listing as $l){
		$ii = $ii+1;
	}
	if($ii == 0){$listing = [];}

	return View::make('ssearch',array('Index'=>$index,'listings'=>$listing));
	}

	public function postSsearch(){

	$unit = array();

	switch(Input::get('search')){

	case 'builder':{
	$vars = WebUser::where('type','=','1')->get();


	break;}
	case 'company':{
	$vars = Company::all();
	break;}
	case 'project':{
	$vars = Project::all();
	if((Input::get('unit') < 1) && (Input::get('unit') > -1)){

	foreach($vars as $var){
	$unit[$var->name][] = BHKUnit::where('project_id','=',$var->id)->get();
	}
	}
	if(Input::get('unit') > 0){
	foreach($vars as $var){
	$unit[$var->name][] = BHKUnit::where('project_id','=',$var->id)->where('type','=',Input::get('unit'))->get();
	}
	}

	break;}
	default:{
	$vars = Input::get('search');
	break;}

	}

	return View::make('searchall',array('vars'=>$vars,'units'=>$unit));
	}


	public function getAddlisting(){

	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');
	$dashboard_links[] = array('url'=>'showlisting','name'=>'Show Listing');

	return View::make('addlisting',array('dlinks'=>$dashboard_links,'page'=>'listing'));
	}

	public function getAddcompanylisting(){

	return View::make('addcompanylisting',array('page'=>'project'));
	}

	public function postAddlisting(){

	$list = new Listing;

	$list->name				= Input::get('list_name');
	$list->description		= Input::get('list_description');
	if(Input::hasFile('file')){
		$file = Input::file('file');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->img = $ret;}
	}
	else{$list->img = 'packages/images/home-img1.jpg';}
	$list->area				= Input::get('list_area');
	$list->price			= Input::get('list_price');
	$list->list_type		= Input::get('list_type');
	$list->status 			= Input::get('list_status');
	$list->beds				= Input::get('list_beds');
	$list->baths			= Input::get('list_baths');
	$list->phone			= Input::get('list_phone');
	$list->email			= Input::get('list_email');
	$list->website			= Input::get('list_website');
	$list->address			= Input::get('list_address');
	$list->location			= Input::get('list_country');

	$list->map_lat				= Input::get('latitude');
	$list->map_long				= Input::get('longitude');

	$list->city				= Input::get('list_city');
	$list->state			= Input::get('list_state');
	$list->country			= Input::get('list_country');

	$list->price_sqft		= Input::get('price_per_sqft');

	if(Input::has('balcony')){
		$list->has_balcony	=	1;
	}
	if(Input::has('bathtub')){
		$list->has_bathtub	=	1;
	}
	if(Input::has('commercial_property')){

		//$commercial_property	= Input::get('commercial_property');
	}
	if(Input::has('monumental_building')){
		//$q = $q->where('has_balcony', '=', 1);
	}
	if(Input::has('ch_builder')){
		$list->has_ch_builder	=	1;
	}
	if(Input::has('fire_place')){
		$list->has_fire_place	=	1;
	}
	if(Input::has('roof_terrace')){
		$list->has_roof_terrace	=	1;
	}
	if(Input::has('sauna')){
		$list->has_sauna	=	1;
	}
	if(Input::has('renewable_energy')){
		$list->has_renewable_energy	=	1;
	}
	if(Input::has('shed_storage')){
		$list->has_shed_storage	=	1;
	}
	if(Input::has('garage')){
		$list->has_garage	=	1;
	}
	if(Input::has('steam_cabin')){
		$list->has_steam_cabin	=	1;
	}
	if(Input::has('jacuzzi')){
		$list->has_jacuzzi	=	1;
	}
	if(Input::has('garden')){
		$list->has_garden	=	1;
	}
	if(Input::has('fixer_upper')){
		$list->has_fixer_upper	=	1;
	}
	if(Input::has('swimming_pool')){
		$list->has_swimming_pool	=	1;
	}
	if(Input::has('elevator')){
		$list->has_elevator	=	1;
	}

	$list->user_id			= Auth::user()->id;
	$list->agent			= Auth::user()->username;

	$list->save();



	return Redirect::to('addlisting')->with('message',"Congratulations You have Added A Listing Profile!!");
	}

	public function postAddcompanylisting(){

	$list = new Project;

	$list->name				= Input::get('name');
	$list->description		= Input::get('description');
	if(Input::hasFile('logo')){
		$file = Input::file('logo');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->logo = $ret;}
	}

	if(Input::hasFile('header')){
		$file = Input::file('header');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->header = $ret;}
	}


	if(Input::hasFile('video')){
		$file = Input::file('video');
		$dest = 'public/listing_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$list->video = $ret;}
	}

	$list->vlink			= Input::get('video_link');
	$list->parent_name		= Input::get('cname');
	$list->project_area		= Input::get('area');
	$list->c_status			= Input::get('cstat');
	$list->phone			= Input::get('phone');
	$list->towers			= Input::get('towers');
	$list->floors			= Input::get('floors');
	$list->units			= Input::get('units');
	$list->open_space		= Input::get('open_space');

	$list->possession_day		= Input::get('possession_day');
	$list->possession_month		= Input::get('possession_month');
	$list->possession_year		= Input::get('possession_year');

	$list->launch_day		= Input::get('launch_day');
	$list->launch_month		= Input::get('launch_month');
	$list->launch_year		= Input::get('launch_year');

	$list->map_lat			= Input::get('latitude');

	$list->map_long 		= Input::get('longitude');

	$list->email			= Input::get('email');
	$list->website			= Input::get('website');
	$list->address			= Input::get('address');
	$list->city				= Input::get('city');
	$list->state			= Input::get('state');
	$list->country			= Input::get('country');
	$list->user_id			= Auth::user()->id;
	$list->agent			= Auth::user()->username;

	$list->save();

	$amen = Input::get('amenities');
	$spec = Input::get('spec');
	if($amen == ''){$amen = null;}
	if($spec == ''){$spec = null;}
	if(!empty($amen)){foreach($amen as $a){$am = new Amenities; $am->project_id = $list->id; $am->amenities = $a; $am->save();}}
	if(!empty($spec)){foreach($spec as $s){$sp = new Spec; $sp->project_id = $list->id; $sp->spec = $a; $sp->save();}}

	return Redirect::to('addunit')->with('message',"Congratulations on your new project, add some units to it and have fun!!");
	}

	public function getShowlisting(){

	$dashboard_links[] = array('url'=>'profile','name'=>'Edit Profile');
	$dashboard_links[] = array('url'=>'unit','name'=>'Create Unit Profile');
	$dashboard_links[] = array('url'=>'createproject','name'=>'Create Project Profile');
	$dashboard_links[] = array('url'=>'company','name'=>'Create Company Profile');

	$dashboard_links[] = array('url'=>'addlisting','name'=>'Add A Listing');
	$dashboard_links[] = array('url'=>'showlisting','name'=>'Show Listing');

	$list = Listing::where('id','>','0')->paginate(7);

	return View::make('showlisting',array('dlinks'=>$dashboard_links,'lists'=>$list,'page'=>'listing'));
	}

	public function getShowcompanylisting(){

	$list = Project::where('id','>','0')->paginate(7);

	return View::make('showprojects',array('lists'=>$list,'page'=>'project'));
	}

	public function postUser(){

	$user = new WebUser();
	$user->username = Input::get('username');
	$user->firstname = Input::get('firstname');
	$user->lastname = Input::get('lastname');
	if(Input::hasFile('file')){
		$file = Input::file('file');
		$dest = 'public/user_img';
		$ret = $this->save_file($dest,$file);
		if($ret != null){$user->img = $ret;}
	}
	else{

		$user->img = '/packages/images/user.png';

	}
	$user->email = Input::get('email');
	$user->password = Hash::make(Input::get('password'));

	$user->save();

	return Redirect::back()->with('message',"User has been successfully added.");

	}

	public function getLocale(){return App::getLocale();}

	public function getLang($lang){

	Session::put('lang',$lang);
	App::setLocale($lang);

	return Redirect::to('/');
	}

	public function getAsettings(){


		return View::make('asettings');
	}

	public function event_listing_list(){

			$list = new Listing;
			$listing = $list->where('id','<',5)->get();
			$listing = json_encode($listing,true);
	}

	public function getListingevent(){

		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		//echo "retry:3000\n";

		if (true)
		{
		 // echo "data: $listing\n\n";
		}

		flush();

	}


	public function getChat(){

	$users = WebUser::all();

	return View::make('chat',array('users'=>$users));

	}

	public function postChat(){

	$to = Input::get('to');
	$from = Input::get('from');
	$message = Input::get('message');

	$chat = new Chat;

	$chat->to = $to;
	$chat->from = $from;
	$chat->message = $message;

	$chat->save();

	return 1;

	}


	function get_blog_archives(){
		$archives		=  DB::table('blog_posts')
							->select(DB::raw('YEAR(created_on) year, MONTH(created_on) month, MONTHNAME(created_on) month_name, COUNT(*) post_count'))
							->groupBy('year')
							->groupBy('month')
							->orderBy('year', 'desc')
							->orderBy('month', 'desc')
							->where('post_status', '=', 'open')
							->get();
		return $archives;
	}

	function get_blog_recent_posts(){

		$recent = Posts::whereRaw('MONTH(created_on) = MONTH(NOW())')->whereRaw('YEAR(created_on) = YEAR(NOW())')->orderBy('post_id', 'desc')->where('post_status', '=', 'open')->paginate(5);

		return $recent;
	}

	function get_blog_side_data(){
		$data['archives']		= $this->get_blog_archives();
		$data['recent_posts']	= $this->get_blog_recent_posts();
		$data['categories']		= Categories::orderBy('category_id', 'desc')->get();
		return $data;
	}

	public function getBlog(){
		$posts			= Posts::where('post_status', '=', 'open')->orderBy('post_id', 'desc')->paginate(3);

		$data			= $this->get_blog_side_data();

		$recent_posts	= $data['recent_posts'];

		$categories		= $data['categories'];

		$archives		= $data['archives'];

		return View::make('blog',array('posts'=>$posts, 'recent_posts'=>$recent_posts, 'categories'=>$categories, 'archives'=>$archives));
	}

	public function getBlogpost($post){

		$post			= Posts::where('post_name', '=', trim($post))->get()[0];

		$data			= $this->get_blog_side_data();

		$recent_posts	= $data['recent_posts'];

		$categories		= $data['categories'];

		$archives		= $data['archives'];

		$commentss		= Comments::where('comment_approved', '=', 'open')->where('comment_type', '=', 'parent')->where('post_id', '=', $post->post_id)->orderBy('comments_id', 'desc')->get();

		$comments		= array();

		foreach($commentss as $comment){
			$cc = array();
			$cc['id'] = $comment->comments_id;
			$cc['uid'] = $comment->comment_uid;
			$cc['comment'] = $comment;
			$cc['child'] = Comments::where('comment_approved', '=', 'open')->where('comment_type', '=', 'child')->where('post_id', '=', $post->post_id)->where('comment_parent', '=', $comment->comments_id)->orderBy('comments_id', 'desc')->get();

			$comments[] = $cc;
		}

		return View::make('blogpost',array('post'=>$post, 'recent_posts'=>$recent_posts, 'categories'=>$categories, 'archives'=>$archives, 'comments'=>$comments));
	}

	public function getCategory($cat){

		$cat			= Postcategory::where('category_slug', '=', $cat)->get();

		$cc = array();

		foreach($cat as $c){
			if(!in_array($c->post_id, $cc)){
				$posts[]			= Posts::where('post_id', '=', $c->post_id)->where('post_status', '=', 'open')->orderBy('post_id', 'desc')->get();
				$cc[]				= $c->post_id;
			}
		}

		if(!isset($posts)){$posts = array();}

		$data			= $this->get_blog_side_data();

		$recent_posts	= $data['recent_posts'];

		$categories		= $data['categories'];

		$archives		= $data['archives'];

		return View::make('blogcat',array('posts'=>$posts, 'recent_posts'=>$recent_posts, 'categories'=>$categories, 'archives'=>$archives));
	}

	public function getArchive($year){

		$posts			= Posts::whereYear('created_on', '=',trim($year))->where('post_status', '=', 'open')->orderBy('post_id', 'desc')->paginate(3);

		$data			= $this->get_blog_side_data();

		$recent_posts	= $data['recent_posts'];

		$categories		= $data['categories'];

		$archives		= $data['archives'];

		return View::make('blog',array('posts'=>$posts, 'recent_posts'=>$recent_posts, 'categories'=>$categories, 'archives'=>$archives));
	}

	public function getBlogsearch(){
		return Redirect::to('blog');
	}

	public function postBlogsearch(){

		$q = Input::get('bquery');

		$posts			= Posts::where('post_content','like','%'.$q.'%')->where('post_status', '=', 'open')->orderBy('post_id', 'desc')->paginate(3);

		$data			= $this->get_blog_side_data();

		$recent_posts	= $data['recent_posts'];

		$categories		= $data['categories'];

		$archives		= $data['archives'];

		return View::make('blog',array('posts'=>$posts, 'recent_posts'=>$recent_posts, 'categories'=>$categories, 'archives'=>$archives));
	}

	public function postComment(){
		//post id
		//comment author
		//comment author img
		//comment content
		//comment type
		//comment parent
		//created on

		if(Auth::check()){

			$comment = new Comments;
			$comment->post_id				= Input::get('post_id');
			$comment->comment_author		= Auth::user()->username;
			$comment->comment_author_img	= Auth::user()->img;
			$comment->comment_content		= Input::get('comment');
			if(Input::has('comment_parent')){
				$parent = Input::get('comment_parent');
				$cp							= Comments::where('comment_uid', '=', $parent)->get()[0];

				$comment->comment_parent	= $cp->comments_id;
				$comment->comment_type		= 'child';
			}
			$comment->created_on			= date("Y-m-d H:i:s");;

			$uid							= (string)rand(1002, 2999868348);

			$uid							.= (string)rand(12, 348);

			$uid							.= (string)rand(1255, 348676);

			$comment->comment_uid			= md5($uid);

			if((Input::get('comment') != '') && (Input::get('comment') != null)){

				$comment->save();

			}

			return Redirect::back();

		}

	}

///////////////////////////////////////////

	public function getBlogs($slug=null){
		if(Auth::check()){
		if(Auth::user()->is_admin == 1){
			if($slug == null){
				$posts = Posts::orderBy('post_id', 'desc')->paginate(10);
				return View::make('blogs',array('page'=>'blog', 'posts'=>$posts));
			}else{

			}
		}else{
			return Redirect::back();
		}
		}
		return Redirect::to('/');
	}

	public function getBlogactivate($slug){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$post = Posts::where('post_name', '=', $slug)->update(array('post_status' => 'open'));
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getBlogdeactivate($slug){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$post = Posts::where('post_name', '=', $slug)->update(array('post_status' => 'closed'));
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getBlogdelete($slug){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$post = Posts::where('post_name', '=', $slug)->delete();
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getNewpost(){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$categories = Categories::all();
				return View::make('newpost',array('page'=>'blog', 'categories'=>$categories));
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getCategories(){

		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$cat = Categories::all();
				return View::make('categories',array('page'=>'blog','categories'=>$cat));
				}else{
					return Redirect::back();
				}
		}else{
			return Redirect::to('/');
		}
	}

	public function postCategories(){

		if(Auth::check()){
			if(Auth::user()->is_admin == 1){

				$name = Input::get('category_name');
				$slug = Input::get('category_slug');
				$desc = Input::get('category_description');

				$category = new Categories;

				$category->category_name = $name;
				if(($slug != '') && ($slug != null)){
					$category->category_slug = $slug;
				}else{
					$slug 	= str_replace(' ', '-', $name);
					$slug	= preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
					$category->category_slug = $slug;
				}

				$category->category_description = $desc;

				$a = Categories::where('category_slug', '=', $slug)->get();

				if(isset($a[0])){
					$category = null;
				}else{
					$category->save();
				}

				$cat = Categories::all();

				return View::make('categories',array('page'=>'blog','categories'=>$cat));
			}else{
					return Redirect::back();
				}
		}else{
			return Redirect::to('/');
		}
	}

	public function getComments(){

		if(Auth::check()){
			if(Auth::user()->is_admin == 1){

		$cl = Comments::orderBy('comments_id', 'desc')->paginate(10);
		$comments = array();
		foreach($cl as $comment){
			$cc['id'] = $comment->comments_id;
			$cc['content'] = $comment->comment_content;
			$cc['status'] = $comment->comment_approved;
			$cc['date'] = $comment->created_on;
			$cc['post'] = Posts::where('post_id', '=', $comment->post_id)->get()[0]->post_title;
			$cc['post'] = substr($cc['post'], 0, 50);
			$cc['post'] = $cc['post'].'...';
			$cc['type'] = $comment->comment_type;
			$cc['author'] = $comment->comment_author;
			$comments[] = $cc;
		}

		return View::make('getcomments',array('page'=>'blog', 'comments'=>$comments, 'comments_link'=>$cl));

			}else{
					return Redirect::back();
				}
		}else{
			return Redirect::to('/');
		}

	}

	public function postNewpost(){

		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
					$title		= trim(Input::get('title'));
					$content	= trim(Input::get('post'));
					$cat		= Input::get('cat');
					$author		= Auth::user()->username;
					$excerpt	= substr($content, 0, 500);

					$post_name	= $title;
					$post_name 	= str_replace(' ', '-', $post_name);
					//$post_name	= preg_replace('/[^A-Za-z0-9\-]/', '', $post_name);
					$post_name	= preg_replace('/\P{Xan}+/u', '', $post_name);

					$post		= new Posts;

					if(Input::hasFile('featured_image')){
						$file = Input::file('featured_image');
						$dest = 'public/blog/featured_image/'.$post_name;
						$ret = $this->save_file($dest,$file);
						if($ret != null){$post->featured_image = $ret;}
					}

					$post->post_title	= $title;
					$post->post_content	= $content;
					$post->post_author	= $author;
					$post->post_excerpt	= $excerpt;
					$post->post_name	= $post_name;
					$post->created_on	= date("Y-m-d H:i:s");

					$post->save();

					if(!empty($cat)){
						foreach($cat as $k=>$v){
							if($v == 'on'){
								$catp = new Postcategory;
								$catp->category_slug = $k;
								$catp->post_id = $post->id;
								$catp->save();
							}
						}
					}
					return Redirect::back();

				}else{
					return Redirect::back();
				}

		}
		return Redirect::to('/');
	}

	public function getCommentactivate($id){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$comment = Comments::where('comments_id', '=', $id)->update(array('comment_approved' => 'open'));
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getCommentdeactivate($id){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$comment = Comments::where('comments_id', '=', $id)->update(array('comment_approved' => 'closed'));
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}

	public function getCommentdelete($id){
		if(Auth::check()){
			if(Auth::user()->is_admin == 1){
				$comment = Comments::where('comments_id', '=', $id)->delete();
				return Redirect::back();
			}else{
				return Redirect::back();
			}
		}
		return Redirect::to('/');
	}


///////////////////////////////////////////////////////////
//////////////////////////////////////////Advanced Search
//////////////////////////////////////////////////////////

	public function getAdvancedsearch(){
		$index = $this->_get_index_values();
		return View::make('advanced_search',array('Index'=>$index));
	}

	public function postAdvancedsearch(){
		$index = $this->_get_index_values();

		$l = new Listing;

			$budget_min				= Input::get('budget_min');
			$budget_max				= Input::get('budget_max');

			$q = $l->where('price', '>', $budget_min)->where('price', '<', $budget_max);

			$location				= Input::get('location');

			foreach($location as $k=>$v){
				if($v == ''){unset($location[$k]);}
			}

			if((is_array($location)) && (!empty($location))){
				$q = $q->where(function($q) use ($location){
					foreach($location as $loc){
					 $q = $q->orWhere('location','like','%'.$loc.'%');
					}
				});
			}

			$property_type			= Input::get('property_type');

			if($property_type != null){$q = $q->where('list_type', '=', $property_type);}

			$stat				= Input::get('status');

			if($stat != null){$q = $q->where('status', '=', $stat);}

			$property_status_type	= Input::get('property_status_type');

			if($property_status_type != null){$q = $q->where('status_type', '=', $property_status_type);}

			$no_of_rooms			= Input::get('no_of_rooms');

			if($no_of_rooms != null){$q = $q->where('beds', '=', $no_of_rooms);}

			$floor_area				= Input::get('floor_area');

			if($floor_area != null){$q = $q->where('area', '=', $floor_area);}

			$age_of_property		= Input::get('age_of_property');

			if($age_of_property != null){
				$page = explode('-', $age_of_property);
				$q = $q->whereYear('list_launch', '>', $page[0])->whereYear('list_launch', '<', $page[1]);
			}

			$near_by				= Input::get('near_by');
			$accessibility			= Input::get('accessibility');
			$energy_label			= Input::get('energy_label');
			$show_listing_from		= Input::get('show_listing_from');

			if($show_listing_from != null){$q = $q->whereDay('created_at', '<=', $show_listing_from);}

			$open_house				= Input::get('open_house');
			$auction				= Input::get('auction');
			$extras					= Input::get('extras');
			$type_of_commercial_property = Input::get('type_of_commercial_property');
			$garden_placement		= Input::get('garden_placement');
			$surface_area_of_garden	= Input::get('surface_area_of_garden');
			$type_of_garage			= Input::get('type_of_garage');
			$capacity				= Input::get('capacity');
			///////
			if(Input::has('balcony')){
				$q = $q->where('has_balcony', '=', 1);
				$balcony				= Input::get('balcony');
			}
			if(Input::has('bathtub')){
				$q = $q->where('has_bathtub', '=', 1);
				$bathtub				= Input::get('bathtub');
			}
			if(Input::has('commercial_property')){
				$commercial_property	= Input::get('commercial_property');
			}
			if(Input::has('monumental_building')){
				//$q = $q->where('has_balcony', '=', 1);
				$monumental_building	= Input::get('monumental_building');
			}
			if(Input::has('ch_builder')){
				$q = $q->where('has_ch_builder', '=', 1);
				$ch_builder				= Input::get('ch_builder');
			}
			if(Input::has('fire_place')){
				$q = $q->where('has_fire_place', '=', 1);
				$fire_place				= Input::get('fire_place');
			}
			if(Input::has('roof_terrace')){
				$q = $q->where('has_roof_terrace', '=', 1);
				$roof_terrace			= Input::get('roof_terrace');
			}
			if(Input::has('sauna')){
				$q = $q->where('has_sauna', '=', 1);
				$sauna					= Input::get('sauna');
			}
			if(Input::has('renewable_energy')){
				$q = $q->where('has_renewable_energy', '=', 1);
				$renewable_energy		= Input::get('renewable_energy');
			}
			if(Input::has('shed_storage')){
				$q = $q->where('has_shed_storage', '=', 1);
				$shed_storage			= Input::get('shed_storage');
			}
			if(Input::has('garage')){
				$q = $q->where('has_garage', '=', 1);
				$garage					= Input::get('garage');
			}
			if(Input::has('steam_cabin')){
				$q = $q->where('has_steam_cabin', '=', 1);
				$steam_cabin			= Input::get('steam_cabin');
			}
			if(Input::has('jacuzzi')){
				$q = $q->where('has_jacuzzi', '=', 1);
				$jacuzzi				= Input::get('jacuzzi');
			}
			if(Input::has('garden')){
				$q = $q->where('has_garden', '=', 1);
				$garden					= Input::get('garden');
			}
			if(Input::has('fixer_upper')){
				$q = $q->where('has_fixer_upper', '=', 1);
				$fixer_upper			= Input::get('fixer_upper');
			}
			if(Input::has('swimming_pool')){
				$q = $q->where('has_swimming_pool', '=', 1);
				$swimming_pool			= Input::get('swimming_pool');
			}
			if(Input::has('elevator')){
				$q = $q->where('has_elevator', '=', 1);
				$elevator				= Input::get('elevator');
			}


			//$q = $q->get();

			$listings = $q->get();


			//$listings = $q->toSql();
			//$this->ldebug($listings);

			return View::make('ssearch',array('Index'=>$index, 'listings'=>$listings));

		//return View::make('advanced_search',array('Index'=>$index));
	}




/////////////////////////
///////////Search by map
////////////////////////

	public function getMapsearch(){
		if(Auth::check()){
			$index = $this->_get_index_values();
			$ip = $_SERVER['REMOTE_ADDR'];
			$ip = '124.253.49.186';
			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
			$center = explode(',', $details->loc);
			$l = new Listing;
			$zz = $l->where('address', 'like', '%'.$details->city.'%')->get();

			$cord = [];
			$data = [];
			$i = 0;
			$prjids = [];
			$localities = [];

			foreach($zz as $z){$i++;
				$prc = rand(10,99);
				$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 90px; height: 50px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
				$cord[$z->name] = array('lat'=>$z->map_lat, 'lng'=>$z->map_long);
				$prjids[] = $z->project_id;
				$localities[] = $z->locality;
			}

			$zzz = Project::whereIn('id', array_unique($prjids))->get();

			$jsn = array(
				'center'=>array('lat'=>$center[0], 'lng'=> $center[1]),
				'max'	=> $i,
				'list'	=> $data
			);

			$localities = array_unique($localities);

			$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);

			//echo '<pre>';print_r($localities);die;

			return View::make('mapsearch',array('Index'=>$index, 'jsn'=>$jsn,'prop'=>$zz, 'proj'=>$zzz, 'localities'=>$localities));
		}else{
			return Redirect::to('login');
		}
	}

	public function getTravelsearch(){

		if(Auth::check()){
			$index = $this->_get_index_values();
			$ip = $_SERVER['REMOTE_ADDR'];
			$ip = '124.253.49.186';
			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
			$center = explode(',', $details->loc);
			$l = new Listing;
			$zz = $l->where('address', 'like', '%'.$details->city.'%')->get();

			$cord = [];
			$data = [];
			$i = 0;
			$prjids = [];
			$localities = [];

			foreach($zz as $z){$i++;
				$prc = rand(10,99);
				$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 90px; height: 50px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
				$cord[$z->name] = array('lat'=>$z->map_lat, 'lng'=>$z->map_long);
				$prjids[] = $z->project_id;
				$localities[] = $z->locality;
			}

			$zzz = Project::whereIn('id', array_unique($prjids))->get();

			$jsn = array(
				'center'=>array('lat'=>$center[0], 'lng'=> $center[1]),
				'max'	=> $i,
				'list'	=> $data
			);

			$localities = array_unique($localities);

			$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);

			//echo '<pre>';print_r($localities);die;

			return View::make('traveltime',array('Index'=>$index, 'jsn'=>$jsn,'prop'=>$zz, 'proj'=>$zzz, 'localities'=>$localities));
		}else{
			return Redirect::to('login');
		}
	}

	public function getAddresssuggest(){

	}

	public function postAmapsearch(){
		 $city = Input::get('term');
		 $buy_rent = Input::get('buy_rent');
		 $list_type = Input::get('list_type');
		 $center = array(23.90, 70.34);
			$l = new Listing;
			$zz = $l->where('city', 'like', '%'.$city.'%')->where('status', '=', $buy_rent)->where('list_type', '=', $list_type)->get();
			//print_r($zzz);die;
			$cord = [];
			$data = [];
			$i = 0;
			foreach($zz as $z){$i++;
				$prc = rand(10,99);
				$data[] = array('id'=>$z->id, 'content'=>'<a href="microsite/listing/'.$z->id.'" target="_blank" class="minfo"><p class="mapdp maphs"><img style="margin:10px 0px !important;left: 0px; top: 0px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: 90px; height: 50px;" src="'.$z->img.'" draggable="false"></p><p class="mapdp"><b>'.$z->beds.' BHK </b><span class="maphss">'.$z->list_type.'</span></p><p class="mapdp"><b>'.$prc.'L </b><span class="maphss">'.$z->area.' Sq-ft</span></p></a>', 'loc'=> array('lat'=>$z->map_lat, 'lng'=>$z->map_long));
				$center = array($z->map_lat, $z->map_long);
			}

			$jsn = array(
				'center'=>array('lat'=>$center[0], 'lng'=> $center[1]),
				'max'	=> $i,
				'list'	=> $data
			);

			$jsn = json_encode($jsn, JSON_NUMERIC_CHECK);
		 echo $jsn;
	}

	public function getLocalityProperties($loc = null){

		if($loc == null){Redirect::to('/');}

		$loc = preg_replace('/-/',' ', $loc);

		$index		= $this->_get_index_values();

		$listings	= Listing::where('locality', '=', $loc)->get();

	return View::make('ssearch',array('Index'=>$index,'listings'=>$listings));
	}

	public function getLocalityProject($loc = null){

		if($loc == null){Redirect::to('/');}

		$index		= $this->_get_index_values();
		$listings	= Listing::where('project_id', '=', $loc)->get();
	return View::make('ssearch',array('Index'=>$index,'listings'=>$listings));
	}

	public function getAddcountry(){
		$index		= $this->_get_index_values();
		return View::make('addcountry',array('Index'=>$index,'page'=>'countries'));
	}

	public function postAddcountry(){

		$location	= Input::get('location');
		$country	= Input::get('country');
		$city		= Input::get('city');

		$c			= new Countries;

		$city		= array_values(array_filter($city));

		if((!isset($city[0])) && ($city[0] == '') && ($city[0] == null)){$city = null;}

		if($location != null){$c->location	= $location;}
		if($country != null){$c->country	= $country;}
		if($city != null){$c->city		= json_encode($city);}

		$c->save();

		return Redirect::to('listcountry');

	}

	public function getEditcountry($id=null){
		$index		= $this->_get_index_values();

		if($id != null){
			$country = Countries::where('id', '=', $id)->firstorFail();

		}
		else{
			return Redirect::to('addcountry');
		}

		return View::make('editcountry',array('Index'=>$index,'page'=>'countries', 'country'=>$country));
	}

	public function getWabanner(){

		$c = Parentcategory::all();
			$_parentcat =[];
		foreach($c as $parentdata){
			$_parentcat[$parentdata['id']]=$parentdata['name'];
		}
		return View::make('webshop/addbanner',array('parents'=>$_parentcat));

	}
	Public function postWabanner(){
		$bannername=Input::get('name');
		$parent=Input::get('parent');
		$path='assets/img';
		$str=[];
		foreach ($_FILES['image']['name'] as $f => $name) {
			$ext = pathinfo($_FILES['image']['name'][$f], PATHINFO_EXTENSION);
			$dest = $path . $name;
			if(move_uploaded_file($_FILES["image"]["tmp_name"][$f], $dest)) {
				$str[]= $name;
			}
		}
		$banner=new Banner;
		$banner->name = $bannername;
		$banner->image=implode(',',$str);
		$banner->catid=$parent;
		if($banner->save()){

		return Redirect::to('/wlbanner')->with('message-success', 'Product Deleted Successfully');
		}

	}

	public function getWlbanner(){
		$banners = DB::table('tblbanner')->paginate(10);
		foreach($banners as $ban){
			$c=[];
			$c['id'] = $ban->id;
			$c['name'] = $ban->name;
			$c['image'] = $ban->image;

			 $categoryid=$ban->catid;

			$categorydata = Parentcategory::where('id', '=', $categoryid)->get();
			if($categorydata!=''){
			$catdata1=$categorydata[0]->id;
			 if($categoryid ==$catdata1){
				$c['categoryname']=$categorydata[0]->name;
			 }
				$_banners[] = $c;
			}
		}
		return View::make('webshop/listbanner', array( 'page'=>'webshop','banner'=>$_banners));
	}
	Public function getWebanner(){
		$id=Input::get('wbanid');
		$c=[];
	$getbanners = Banner::where('id', '=', $id)->get();
		$c['id']=$id;
		if(isset($getbanners[0])){
			$c['bannername']=$getbanners[0]->name;
		}
		if(isset($getbanners[0])){
			 $c['cid']=$getbanners[0]->catid;

			$categorydata = Parentcategory::where('id', '=', $c['cid'])->get();
			if(isset($categorydata[0])){
				 $c['categoryname']=$categorydata[0]->name;
			}
		}
		if(isset($getbanners[0])){
			 $c['image']=$getbanners[0]->image;
		}

		 $Parentcategory = Parentcategory::all();

		return  View::make('webshop/editbanner',array('getbanners'=>$c,'category'=>$Parentcategory));
	}
	public function postEditcountry($id=null){

		$location	= Input::get('location');
		$country	= Input::get('country');
		$city		= Input::get('city');
		$id			= Input::get('id');

		$c			= Countries::where('id', '=', $id)->firstorFail();

		$city		= array_values(array_filter($city));

		if((!isset($city[0])) && ($city[0] == '') && ($city[0] == null)){$city = null;}

		if($location != null){$c->location	= $location;}
		if($country != null){$c->country	= $country;}
		if($city != null){$c->city			= json_encode($city);}

		$c->save();

		return Redirect::to('listcountry');

	}

	public function getDeletecountry($id=null){

		Countries::where('id', '=', $id)->delete();

		return Redirect::to('listcountry');
	}

	public function getListcountry(){
		$index		= $this->_get_index_values();

		$countries = Countries::all();

		return View::make('listcountry',array('Index'=>$index,'page'=>'countries', 'countries'=>$countries));
	}

	public function getViewcities($country=null){
		if($country != null){
			$cities = Countries::where('country', '=', $country)->firstorFail();
			$cities = $cities['city'];
		}
		else{
			$cities = [];
		}
		echo $cities;
	}

	public function getCountryloc(){
		$loc = Input::get('loc');
		$data = '';
		if(($loc == 'eu') || ($loc == 'world')){
			$countries = Countries::where('location', '=', $loc)->get();
			foreach($countries as $country){
				$cities = json_decode($country['city']);
				if(!empty($cities)){
					$data .= '<li><a class="country-class has-cities" href="javascript:void(0);" data-loc="'.strtolower($country['location']).'" data-country="'.strtolower($country['country']).'">'.$country['country'].'</a></li>';
				}
				else{
					$data .= '<li><a class="country-class" href="javascript:void(0);" data-loc="'.strtolower($country['location']).'" data-country="'.strtolower($country['country']).'">'.$country['country'].'</a></li>';
				}
			}
		}
		echo $data;
		die;
	}

	public function getCityloc(){
		$loc = Input::get('loc');
		$data = '';
		$country = Countries::where('country', '=', $loc)->firstorFail();

		$cities = json_decode($country['city'], true);
		foreach($cities as $city){
			$data .= '<li><a class="city-class" href="javascript:void(0);" data-loc="'.$loc.'" data-city="'.strtolower($city).'">'.ucwords($city).'</a></li>';
		}
		echo $data;
		die;
	}

	public function getSetcloc(){
		$loc = Input::get('country');
		Session::put('country', $loc);
	}

	public function getSetcity(){
		$loc = Input::get('city');
		Session::put('city', $loc);
	}

///////////////////////////////////////////////////////////
//////////////////////////////////////////Webshop
//////////////////////////////////////////////////////////


	public function getWlcategory(){
		$parentcategory = Category::where('parentid', '=', 0)->get();
		$p = array();
		foreach($parentcategory as $par){
			$p[$par->id] = $par->categoryname;
		}

		$categories = Category::paginate(10);

		foreach($categories as $k=>$v){
			if($v['parentid'] == 0){
				$categories[$k]['parencategoryname'] = 'No parent';
			}
			else{
				$categories[$k]['parencategoryname'] = $p[$v['parentid']];
			}
		}
		return View::make('webshop/list_categories', array('categories'=>$categories, 'page'=>'webshop'));
	}

	public function getWacategory(){

		$parentcategory = $categories = Category::where('parentid', '=', 0)->get();
		foreach($parentcategory as $parentdata){
			$_parentcat[$parentdata['id']]=$parentdata['categoryname'];
		}

		return View::make('webshop/add_category', array( 'page'=>'webshop','parents'=>$_parentcat));
	}

	public function postWacategory(){
		$parentcat=Input::get('parent');
		$name=Input::get('category');
		$imagename  = '';
		$target_dir = "assets/img/";
		$target_file = $target_dir . basename($_FILES["image"]["name"]);
		if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
			$imagename=$_FILES["image"]["name"];
		}

		$category = new Category;

		if($parentcat == 'no parent'){
			$category->parentid = 0;
		}
		else{
			$category->parentid = $parentcat;
		}
		$category->categoryname = $name;
		$slug	= preg_replace('/[^ \w]+/u', '', $name);
		$slug 	= str_replace(' ', '-', $slug);
		$category->slug = $slug;
		$category->imgurl=$imagename;
		$category->save();
		return Redirect::to('/wlcategory')->with('message-success', 'Added Successfully');
	}
	public function postWebanner(){
		$bannername=Input::get('name');
		$category=Input::get('category');
		$id=Input::get('id');
		 $path='assets/img/';
		$str=[];
		foreach ($_FILES['image']['name'] as $f => $name) {
				$ext = pathinfo($_FILES['image']['name'][$f], PATHINFO_EXTENSION);
				$uniq_name = uniqid() . '.' .$ext;
				$dest = $path . $name;
				if(move_uploaded_file($_FILES["image"]["tmp_name"][$f], $dest)) {
					$str[]= $name;
				}
		}

		$ban= Banner::where('id','=',$id)->update(array('name'=>$bannername,'image'=>implode(',',$str),'catid'=>$category));
		return Redirect::to('/wlbanner')->with('message-success', 'Updated Successfully');

	}
	Public function getWdbanner(){
		 $wbanid=Input::get('wbanid');
		$banners=Banner::find($wbanid);
		if($banners->delete()){
			return Redirect::to('/wlbanner')->with('message-success', 'Banner Deleted Successfully');
		}
	}
	public function getWecategory(){
		$parentcategory  = Category::where('parentid', '=', 0)->get();
		$id = Input::get('wcatid');
		$catt=Category::where('id', '=', $id)->get()[0];
		return View::make('webshop/edit_category', array( 'page'=>'webshop','parents'=>$parentcategory, 'category'=>$catt, 'ptype'=>'edit'));
	}

	public function getWdcategory(){
		if(Auth::check()){
			$id = Input::get('wcatid');
			Category::where('id', '=', $id)->delete();
		}
		return Redirect::to('/wlcategory')->with('message-success', 'Deleted Successfully');
	}

	public function postWecategory(){
		 $name=Input::get('category');
		 $parentcat=Input::get('parent');
		 $id=Input::get('wcatid');
		$imagename  = '';
		if($_FILES["image"]["name"]!=''){
		$target_dir = "assets/img/";
		$target_file = $target_dir . basename($_FILES["image"]["name"]);
		 if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
			 $imagename=$_FILES["image"]["name"];
		 }
		}
		$slug	= preg_replace('/[^ \w]+/u', '', $name);
		$slug 	= str_replace(' ', '-', $slug);
		$affectedRows=Category::where('id', '=',$id)->update(array('categoryname' =>$name,'slug'=>$slug,'parentid'=>$parentcat,'imgurl'=>$imagename));
		return Redirect::to('/wlcategory')->with('message-success', 'Updated Successfully');
	}

	public function getWlproducts(){
		$products = DB::table('products')->whereIn('type', array('simple', 'variable'))->orderBy('id', 'desc')->paginate(10);

		foreach($products as $prod){

			$metas = Productmetas::where('product_id', '=', $prod->id)->get();
			$pmetas = array();
			if(isset($metas[0])){
				foreach($metas as $kk=>$v){
					$prod->{$v->meta_key}=$v->meta_value;
				}
			}

		//echo '<pre>';print_r($prod);die;
			$c=[];
			$c['id'] = $prod->id;
			$c['productname'] = $prod->name;
			if(isset($prod->_size)){
				$c['size'] = $prod->_size;
			}
			else{
				$c['size'] = '';
			}
			$c['price'] = $prod->price;
			$c['quantity'] = $prod->quantity;
			$categoryid = $prod->catid;
			$category = new Category;
			$categorydata = Category::where('id', '=', $categoryid)->get();
			//dd($categorydata);
			if(count($categorydata) > 0){
			$catdata1=$categorydata[0]->id;
			 if($categoryid ==$catdata1){
				$c['categoryname']=$categorydata[0]->categoryname;
			 }
				$_products[] = $c;
			}
		}
		/*echo "<pre>";
		print_r($_products);
		die();*/
		return View::make('webshop/list_products', array( 'page'=>'webshop','products'=>$_products,'prodpage'=>$products));
	}

	public function getWaproducts(){
		$parent = Category::where('parentid', '=', 0)->get();
	$parentcat=[];
		foreach($parent as $vl){
			$parentcat[$vl->id]=$vl->categoryname;
		}
		//foreach($catt as $value){ $_catval[$value->id]=$value->categoryname; }
		return View::make('webshop/add_product', array( 'page'=>'webshop', 'parentcat'=>$parentcat));
	}

	public function postGcat(){
		$id = Input::get('s');
		$gcat = Category::where('parentid', '=',$id)->get()->toJSON();
		echo $gcat;die;
	}

	public function postGpcat(){
		$id = Input::get('s');
		$cat = Category::where('id', '=',$id)->get();
		$html = '';
		if(isset($cat[0])){
			$cat = $cat[0];
			$p = ProductProperties::where('category_id', '=', $id)->get();
			if(isset($p[0])){
			//$html = '<h5>Properties</h5>';
			foreach($p as $property){
				$html .= '<div class="col-md-6">';
					$html .= '<h5>'.ucwords($property->name).'</h5>';
					$html .= '<select name="property['.$property->name.']" class="form-control">';
					$property->values = ProductPropertyValues::where('property_id', '=', $property->id)->get();
					foreach($property->values as $val){
						$html .= '<option value="'.$val->name.'">'.$val->name.'</option>';
					}
					$html .= '</select>';
				$html .= '</div>';
			}
			/*	foreach($p as $property){
					$html .= '<div style="float:left;padding-right:8px;margin-right:12px;padding-left:0px;margin-left:0px;">';
						$html .= '<span><input type="checkbox" name="property['.$property->name.']" value="'.$property->id.'">&nbsp; '.ucwords($property->name).'</span></div>';
				}
			*/
			}
		}

		echo json_encode(array('html'=>$html));die;
	}

	public function postWaproducts(){

		$parentcategory = Input::get('parentcategory');
		$category = Input::get('category');
		$product_name = Input::get('product_name');
		$product_description = Input::get('product_description');
		$product_type = Input::get('product_type');

		$product = new Products;
		$product->parentcatid = $parentcategory;
		$product->parent = 0;
		$product->catid = $category;
		$product->name = $product_name;
		$product->description = $product_description;
		$product->type = $product_type;

		if(Input::hasFile('feature_image')){
			$file = Input::file('feature_image');
			$dest = 'public/webshop/products';
			$ret = $this->save_file($dest,$file);
			if($ret != null){$product->img = $ret;}
		}
		else{}

		if($product_type == 'simple'){
			$sale_price = Input::get('simple_sale_price');
			$product->price = $sale_price;
		}

		$p_price = [];

		$product->save();

		if($product_type == 'simple'){
			$regular_price = Input::get('simple_regular_price');
			$sale_price = Input::get('simple_sale_price');
			Productmetas::insert(array(
									array('product_id'=>$product->id, 'meta_key'=>'regular_price', 'meta_value'=>$regular_price),
									array('product_id'=>$product->id, 'meta_key'=>'sale_price', 'meta_value'=>$sale_price)
								));
			Productmetas::where('id', '=',$attr_id)->update(array('product_id'=>$product->id));
			return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');
		}
		else if($product_type == 'variable'){
			$variations = array();
			$attr_id = Input::get('attr_id');
			$attrs = Productmetas::where('id', '=', $attr_id)->get();
			if(!empty($attrs[0])){
				$attrs = unserialize($attrs[0]['meta_value']);
				$attributes = array();
				foreach($attrs as $k=>$attr){
					if(Input::has($k)){
						$attributes[$k] = Input::get($k);
					}
				}
				$variation_sku = Input::get('variation_sku');
				$variation_regular_price = Input::get('variation_regular_price');
				$variation_sale_price = Input::get('variation_sale_price');
				$variation_stock = Input::get('variation_stock');
				$variation_quantity = Input::get('variation_quantity');
				$variation_weight = Input::get('variation_weight');
				$variation_dimension = Input::get('variation_dimension');
				$var_len = count($variation_sku);
				for($i=0;$i<$var_len;$i++){
					$p_price[] = $variation_sale_price[$i];
					$variations[$i]['_sku'] = $variation_sku[$i];
					$variations[$i]['_regular_price'] = $variation_regular_price[$i];
					$variations[$i]['_sale_price'] = $variation_sale_price[$i];
					$variations[$i]['_stock'] = $variation_stock[$i];
					$variations[$i]['_quantity'] = $variation_quantity[$i];
					$variations[$i]['_weight'] = $variation_weight[$i];
					$variations[$i]['_dimension'] = $variation_dimension[$i];
					foreach($attributes as $k=>$v){
						$variations[$i]['_'.$k] = $v[$i];
					}
				}
				foreach($variations as $variation){
					$vproduct = new Products;
					$vproduct->parentcatid = $product->parentcatid;
					$vproduct->catid = $product->catid;
					$vproduct->name = $product->id.'#'.$variation['_sku'];
					$vproduct->description = $product->name.'-'.$product->description;
					$vproduct->type = 'variation';
					$vproduct->parent = $product->id;
					$vproduct->save();
					$pmetas = array();
					foreach($variation as $k=>$v){
						$pmetas[] = array('product_id'=>$vproduct->id, 'meta_key'=>$k, 'meta_value'=>$v);
					}
					if(!empty($pmetas)){
						Productmetas::insert($pmetas);
					}
				}

				$p_price = natsort($p_price);
				$p_price_len = count($p_price);
				if($p_price_len > 1){
					$p_price = $p_price[0].' - '. $p_price[$p_price_len - 1];
				}
				else{
					$p_price = $p_price[0];
				}

				Products::where('id', '=',$product->id)->update(array('price'=>$p_price));

				Productmetas::where('id', '=',$attr_id)->update(array('product_id'=>$product->id));

				return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');
			}
		}
		else{

		}






		//print_r($product);

		print_r(Input::all());
		die;
	}

	public function postWaproductss(){
		$name = Input::get('product');
		$parentcategory=Input::get('parentcategory');
		$category = Input::get('category');
		$price=Input::get('price');
		$size=Input::get('size');
		$quantity=Input::get('quantity');
		$startdate=Input::get('startdate');
		$enddate=Input::get('enddate');
		$product=new Products;
		$product->productname=$name;
		$product->parentcatid=$parentcategory;
		$product->catid=$category;
		$product->price=$price;
		$product->size=$size;
		$product->quantity=$quantity;
		$product->startdate=date('y-m-d h:i:s',strtotime($startdate));
		$product->enddate=date('y-m-d h:i:s',strtotime($enddate));
		$product->save();
		$productimg = new Productimages;
		$path='assets/img';
		$str=[];
		foreach ($_FILES['image']['name'] as $f => $name) {
				$ext = pathinfo($_FILES['image']['name'][$f], PATHINFO_EXTENSION);
				$uniq_name = uniqid() . '.' .$ext;
				$dest = $path . $name;
				if(move_uploaded_file($_FILES["image"]["tmp_name"][$f], $dest)) {

					$str[]= $name;

				}
		}
		$productimg->productid=$product->id;
		$productimg->imagename=implode(',',$str);
		$productimg->save();
		return Redirect::to('/wlproducts')->with('message-success', 'Added Successfully');

	}
	public function getWeproducts(){
	 $wprodid = Input::get('wprodid');
	 $c=[];
	$productdata = Products::where('id', '=', $wprodid)->get();
	$c['id']=$wprodid;
		if(isset($productdata[0])){
			$c['productname']=$productdata[0]->productname;
		}
		if(isset($productdata[0])){
			 $c['cid']=$productdata[0]->catid;

			$categorydata = Category::where('id', '=', $c['cid'])->get();
			if(isset($categorydata[0])){
				 $c['categoryname']=$categorydata[0]->categoryname;
			}
		}
		if(isset($productdata[0])){
			 $c['parentcatid']=$productdata[0]->parentcatid;

			$parentcategorydata = Parentcategory::where('id', '=', $c['parentcatid'])->get();
			if(isset($parentcategorydata[0])){
				 $c['parentcategorydata']=$parentcategorydata[0]->name;
			}
		}
		if(isset($productdata[0])){
			 $c['size']=$productdata[0]->size;
		}
		 if(isset($productdata[0])){
		 $c['price']=$productdata[0]->price;
		 }
		if(isset($productdata[0])){
		 $c['quantity']=$productdata[0]->quantity;
		}

		if(isset($productdata[0])){
		 $c['startdate']=$productdata[0]->startdate;
		}
		if(isset($productdata[0])){
		 $c['enddate']=$productdata[0]->enddate;
		}
			$imagedata = Images::where('productid', '=', $wprodid)->get();
		 if(isset($imagedata[0])){
			$c['image']=$imagedata[0]->imagename;
		 }
		 $category = Category::all();
		 $parentcategory = Parentcategory::all();

		return View::make('webshop/editproduct', array( 'page'=>'webshop','products'=>$c,'category'=>$category,'parentcategory'=>$parentcategory));
	}
Public function postWeproducts(){
$parentcategoryid=Input::get('parentcategory');
$categoryid=Input::get('category');
$id=Input::get('wprodid');
$productname=Input::get('product');
$price=Input::get('price');
$size=Input::get('size');
$quantity=Input::get('quantity');
$startdate=Input::get('startdate');
$enddate=Input::get('enddate');
 $img=Input::hasFile('image');

if(isset($img)){
	foreach($_FILES['image']['name'] as $key=>$val){
		if($val!=''){
		$imagedata= Images::where('productid', '=', $id)->get();
		foreach($_FILES['image']['name'] as $f=>$val){
			$ext = pathinfo($_FILES['image']['name'][$f], PATHINFO_EXTENSION);
			$path='assets/img/';
			$dest = $path . $val;
			if(move_uploaded_file($_FILES["image"]["tmp_name"][$f], $dest)) {
				$str[]= $val;
			}
		}
		$rr=implode(',',$str);
		$productimg = new Productimages;
		$affectedRows =Productimages::where('productid', '=',$id)->update(array('imagename' =>$rr));
		}else{
			$productimg = new Productimages;
			$path='assets/img';
			$str=[];
			foreach ($_FILES['image']['name'] as $f => $name) {
				$ext = pathinfo($_FILES['image']['name'][$f], PATHINFO_EXTENSION);
				$dest = $path . $name;
				if(move_uploaded_file($_FILES["image"]["tmp_name"][$f], $dest)) {
					$str[]= $name;
				}
			}
			$productimg->productid=$id;
			$productimg->imagename=implode(',',$str);
			$productimg->save();
		}

	}

}

		$products = Products::find($id);
		$products->productname = $productname;
		$products->parentcatid = $parentcategoryid;
		$products->catid = $categoryid;
		$products->size = $size;
		$products->price = $price;
		$products->quantity = $quantity;
		$products->startdate =date('Y-m-d h:i:s',strtotime($startdate));
		$products->enddate =date('Y-m-d h:i:s',strtotime($enddate));
		if($products->save()) {
			return Redirect::to('/wlproducts')->with('message-success', 'Product Updated Successfully');

		}
}
public function getWdproducts(){
	$id=Input::get('wprodid');
	$products=Products::find($id);
	if($products->delete()){
		return Redirect::to('/wlproducts')->with('message-success', 'Product Deleted Successfully');
	}
}
//////////////////////////////////////////Extras//////////////////


	public function getNew(){return View::make('new');}

	public function ldebug($var){echo "<pre>";print_r($var);dd('.');echo "</pre>";}

}
