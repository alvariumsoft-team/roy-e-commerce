<?php
		
class NewsletterController extends BaseController {

	public function getNewsletter(){
		$templates = Newsletter::orderBy('id', 'desc')->paginate(20);
		return View::make('newsletter/newsletters', array('templates'=>$templates));
	}
	
	public function getAddnewsletter(){
		return View::make('newsletter/add_newsletter');
	}
	
	public function postAddnewsletter(){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'content'=> 'required',
			)
		);
		$et = new Newsletter;
		$et->name = Input::get('name');
		$et->slug = strtolower(str_replace(" ", "-", $et->name));
		$et->content = Input::get('content');
		$et->save();
		return Redirect::to('newsletters')->with('message',"Newsletter  added successfully!");
	}
	
	public function getEditnewsletter($id){
		$template = Newsletter::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			return View::make('newsletter/add_newsletter', array('name'=>$template->name, 'content'=>$template->content));
		}
		return Redirect::to('newsletters')->with('message',"Newsletter not available.");
	}
	
	public function postEditnewsletter($id){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'content'=> 'required',
			)
		);
		$template = Newsletter::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			$template->name = Input::get('name');
			$template->content = Input::get('content');
			$template->save();
			return Redirect::to('newsletter/edit/'.$id)->with('message',"Newsletter  added successfully!");
		}
		return Redirect::to('newsletters')->with('message',"Unable to edit template.");
	}
	
	public function getDeletenewsletter($id){
		$template = Newsletter::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			Newsletter::where('id', '=' ,$id)->delete();
			return Redirect::to('newsletters')->with('message',"Newsletter deleted successfully!");
		}
		return Redirect::to('newsletters')->with('message',"Unable to delete newsletter.");
	}
	
	public function getViewnewsletter($id){
		$template = Newsletter::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			return View::make('newsletter/view_newsletter', array('name'=>$template->name, 'content'=>$template->content));
		}
		return Redirect::to('newsletters')->with('message',"Newsletter not available.");
	}

	public function getSubscribers(){
		$data = array();
		$newsletters = Newsletter::select('id', 'name')->get();
		if($newsletters[0]){
			$data['curr_newsletter'] = $newsletters[0]->id;
			$subscribers = Newsletterlist::where('newsletter_id', '=', $newsletters[0]->id)->orderBy('id', 'desc')->get();
			//echo '<pre>';print_r([$subscribers, $newsletters[0]->id]);die;
			foreach($subscribers as $k=>$subscriber){
			
				$subscribers[$k]->newsletter = $newsletters[0]->name;
			}
			$data['subscribers'] = $subscribers;
		}
		 $data['newsletters'] = $newsletters;
		return View::make('newsletter/subscribers', $data);
	}

	public function getNsubscribers($id){
		$data = array();
		$newsletters = Newsletter::select('id', 'name')->where('id', '=', $id)->get();
		if($newsletters[0]){
			
			$subscribers = Newsletterlist::where('newsletter_id', '=', $newsletters[0]->id)->orderBy('id', 'desc')->get();
			//echo '<pre>';print_r([$subscribers, $newsletters[0]->id]);die;
			foreach($subscribers as $k=>$subscriber){
			
				$subscribers[$k]->newsletter = $newsletters[0]->name;
			}
			$data = $subscribers->toArray();
			echo json_encode(array('success'=>true, 'subscribers'=>$data));die;
		}
		 echo json_encode(array('success'=>false, 'subscribers'=>$data));die;
	}

	public function getSubscriber($id){
		$data = array();
		$subscribers = Newsletterlist::where('id', '=', $id)->get();
		if($subscribers[0]){
			$subscriber = $subscribers[0];
			$newsletters = Newsletter::select('id', 'name')->where('newsletter_id', '=', $id)->get();
			if(isset($newsletters[0])){
				$subscriber->newsletter = $newsletters[0];
				
			}
			$data['subscriber'] = $subscriber;
		}
		// $data['newsletters'] = $newsletters;
		//return View::make('newsletter/subscribers', $data);
	}

	public function getDeletesubscriber($id){
		$template = Newsletterlist::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			Newsletterlist::where('id', '=' ,$id)->delete();
			return Redirect::to('/newsletter/subscribers')->with('message',"Subscriber deleted successfully!");
		}
		return Redirect::to('/newsletter/subscribers')->with('message',"Unable to delete subscriber.");
	}
	
	public function send_mail($newsletter, $subscriber ){
		$to	= $subscriber->email;
		$subject = $newsletter->name;
		$message = $newsletter->content;
		

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: webmaster@inviolateinfotech.com' . "\r\n";
		$headers .= 'Reply-To: webmaster@inviolateinfotech.com' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
		//echo '<pre>';print_r([$headers, $message]);die;
		mail($to, $subject, $message, $headers);
		usleep(2000);
	}
	
	public function send_newsletter_mails(){
		$pq = Newsletterqueue::where('status', '=', 'A')->get();
		if(isset($pq[0])){
			foreach($pq as $pqu){
				$newsletters = Newsletter::where('id', '=', $pqu->newsletter_id)->get();
				if(isset($newsletters[0])){
					$newsletter = $newsletters[0];
					$queue = [$pqu];
					if(isset($queue[0])){
						$queue = $queue[0];
						if($queue->subscriber_id == 0){
							$subscribers = Newsletterlist::where('id', '>' ,0)->limit(25)->get();
							if(isset($subscribers[0])){
								$subscriber_id = 0;
								foreach($subscribers as $subscriber){
									$this->send_mail( $newsletter, $subscriber );
									$subscriber_id = $subscriber->id;
								}
								Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->update(['subscriber_id'=>$subscriber_id]);
							}
							else{
								Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->update(['subscriber_id'=>0, 'status'=>'D']);
							}
						}
						else{
							$subscribers = Newsletterlist::where('id', '>' ,$queue->subscriber_id)->limit(25)->get();
							if(isset($subscribers[0])){
								$subscriber_id = $queue->subscriber_id;
								foreach($subscribers as $subscriber){
									$this->send_mail( $newsletter, $subscriber );
									$subscriber_id = $subscriber->id;
								}
								Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->update(['subscriber_id'=>$subscriber_id]);
							}
							else{
								Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->update(['subscriber_id'=>0, 'status'=>'D']);
							}
						}
					}
				}
			}
		}
	}

	public function send_newsletter(){
		$id = (int)Input::get('newsletter');
		if($id > 0){
			$newsletters = Newsletter::select('id', 'name')->where('id', '=', $id)->get();
			if(isset($newsletters[0])){
				$newsletter = $newsletters[0];
				$queue = Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->get();
				if(!isset($queue[0])){
					$nl = new Newsletterqueue;
					$nl->newsletter_id = $newsletter->id;
					$nl->subscriber_id = 0;
					$nl->status = 'A';
					$nl->save();
				}
				else{
					if($queue[0]->subscriber_id == 0){
						Newsletterqueue::where('newsletter_id', '=', $newsletter->id)->update(['status'=>'A']);
						return Redirect::to('/newsletter/subscribers')->with('message',"Newsletter are queued to be sent.");
					}
					else{
						return Redirect::to('/newsletter/subscribers')->with('message',"There's already a queue for the newsletter wait it to finish first.");
					}
				}
			}
			else{
				return Redirect::to('/newsletter/subscribers')->with('message',"Sorry unable to find newsletter.");
			}
		}
		return Redirect::to('/newsletter/subscribers')->with('message',"Sorry unable to find newsletter.");
	}

}
