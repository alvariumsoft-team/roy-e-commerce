<?php
		
class SettingsController extends BaseController {

	public function getPrivacy(){
		$privacy = Settings::where('key', '=', 'privacy_policy')->get();
		if(isset($privacy[0])){
			$privacy = $privacy[0]->value;
		}
		else{
			$privacy = '';
		}
		return View::make('webshop/privacy', array('policy'=>$privacy));
	}

	public function getaddprivacy(){
		$privacy = Settings::where('key', '=', 'privacy_policy')->get();
		if(isset($privacy[0])){
			$privacy = $privacy[0]->value;
		}
		else{
			$privacy = '';
		}
		return View::make('settings/add_privacy', array('privacy'=>$privacy));
	}
	
	public function postaddprivacy(){
		$validator = Validator::make(Input::all(), array(
			'policy'=> 'required',
			)
		);
		$policy = Input::get('policy');
		$privacy = Settings::where('key', '=', 'privacy_policy')->get();
		if(isset($privacy[0])){
			Settings::where('key', '=', 'privacy_policy')->update(['value'=>$policy]);
		}
		else{
			$p = new Settings;
			$p->key = 'privacy_policy';
			$p->value = $policy;
			$p->save();
		}
		return Redirect::to('settings/addprivacy')->with('message',"Privacy policy added successfully!");
	}
	
	
	public function getEmailtemplates(){
		$templates = Emailtemplates::orderBy('id', 'desc')->paginate(20);
		return View::make('settings/email_templates', array('templates'=>$templates));
	}
	
	public function getAddemailtemplate(){
		return View::make('settings/add_email_template');
	}
	
	public function postAddemailtemplate(){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'template'=> 'required',
			)
		);
		$et = new Emailtemplates;
		$et->name = Input::get('name');
		$et->template = Input::get('template');
		$et->save();
		return Redirect::to('settings/emailtemplates')->with('message',"Email template  added successfully!");
	}
	
	public function getEditemailtemplate($id){
		$template = Emailtemplates::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			return View::make('settings/add_email_template', array('name'=>$template->name, 'template'=>$template->template));
		}
		return Redirect::to('settings/emailtemplates')->with('message',"Email template not available.");
	}
	
	public function postEditemailtemplate($id){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'template'=> 'required',
			)
		);
		$template = Emailtemplates::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			$template->name = Input::get('name');
			$template->template = Input::get('template');
			$template->save();
			return Redirect::to('settings/emailtemplates')->with('message',"Email template  added successfully!");
		}
		return Redirect::to('settings/emailtemplates')->with('message',"Unable to edit template.");
	}
	
	public function getDeleteemailtemplate($id){
		$template = Emailtemplates::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			Emailtemplates::where('id', '=' ,$id)->delete();
			return Redirect::to('settings/emailtemplates')->with('message',"Email template  deleted successfully!");
		}
		return Redirect::to('settings/emailtemplates')->with('message',"Unable to delete template.");
	}
	
	public function getViewemailtemplate($id){
		$template = Emailtemplates::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			$template = $template[0];
			return View::make('settings/view_email_template', array('name'=>$template->name, 'template'=>$template->template));
		}
		return Redirect::to('settings/emailtemplates')->with('message',"Email template not available.");
	}
	
	public function getListpages(){
		$pages = Pages::orderBy('id', 'desc')->paginate(20);
		return View::make('settings/pages', array('pages'=>$pages));
	}

	public function getAddpage(){
		return View::make('settings/add_page');
	}
	
	public function postAddpage(){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'template'=> 'required',
			)
		);
		$et = new Pages;
		$et->name = Input::get('name');
		$et->slug = Input::get('name');
		$et->value = Input::get('value');
		$et->save();
		return Redirect::to('settings/listpages')->with('message',"Page  added successfully!");
	}
	
	public function getDeletepage($id){
		$template = Pages::where('id', '=' ,$id)->get();
		if(isset($template[0])){
			Pages::where('id', '=' ,$id)->delete();
			return Redirect::to('settings/listpages')->with('message',"Page  deleted successfully!");
		}
		return Redirect::to('settings/listpages')->with('message',"Unable to delete page.");
	}
	
	public function getEditpage($id){
		$page = Pages::where('id', '=' ,$id)->get();
		if(isset($page[0])){
			$page = $page[0];
			return View::make('settings/add_page', array('cmspage'=>$page));
		}
		return Redirect::to('settings/listpages')->with('message',"Page not available.");
	}
	
	public function postEditpage($id){
		$validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'value'=> 'required',
			)
		);
		$page = Pages::where('id', '=' ,$id)->get();
		if(isset($page[0])){
			$page = $page[0];
			$page->name = Input::get('name');
			$page->slug = Input::get('name');
			$page->value = Input::get('value');
			$page->save();
			return Redirect::to('settings/listpages')->with('message',"Page  added successfully!");
		}
		return Redirect::to('settings/listpages')->with('message',"Uanble to edit page.");
	}
	
}
