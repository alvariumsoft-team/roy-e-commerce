<?php
use Mollie\Laravel\Facades\Mollie;
define('MOLLIE_PATH', base_path().'/vendor/mollie');
//autoload
require_once MOLLIE_PATH.'/autoload.php';
//api_key
define('API_KEY', 'test_dcr8HFH6HM59sgrrMtuSkQbdECVdPb');
//baseurl
define('BASE_URL', ($_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['SCRIPT_NAME']));
class ConnectmollieController extends BaseController{
	private $api_key = '';
	function __construct(){
		$this->api_key = API_KEY;
	}

	function startTransaction() {
		$url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		preg_match("/[^\/]+$/", $url, $matches);
		$num = $matches[0];
		$total = $num / 100;
		$total=number_format($total,2);
		$mollie = new \Mollie\Api\MollieApiClient();
		$mollie->setApiKey($this->api_key);
		try {
			$order_id = time();
			$payment = $mollie->payments->create([
				"amount" => ["currency" => "EUR", "value" => "{$total}",], 
				"description" => "Webshop Orders",
				"redirectUrl" => BASE_URL . "checkout/placeorder?view=connectComplete&transactionid={$order_id}",
				"metadata" => ["order_id" => $order_id],
			]);
			header("Location: " . $payment->getCheckoutUrl());;
		} 
		catch (Exception $e) {
		  echo "Error " . htmlspecialchars($e->getMessage());
		}
	}
}
?>