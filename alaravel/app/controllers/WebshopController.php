<?php
define('PAGINATION_COUNT', 20);
define('Tax_Neth_Co', 21);
define('Tax_Neth_In', 21);
define('Tax_Eu_Co', 0);
define('Tax_Eu_In', 21);
define('Tax_Outside_Eu', 0);
define('DOMPDF_PATH', base_path().'/vendor/dompdf/dompdf');
define('Mandrill_PATH', base_path().'/vendor/Mandrill');
define('MailChimp_PATHS', base_path().'/vendor/mailchimp');
define('GOOGLE_LOGIN_PATHS', base_path().'/vendor/google-login');
define('FACEBOOK_LOGIN_PATHS', base_path().'/vendor/facebook-php-graph-sdk');
define('GOOGLE_CLIENT_ID', '618944127990-524e36q6klirgip7utodlik7onac609p.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'lygIINhhNThCVeBxBu4aBWSE');
define('GOOGLE_REDIRECT_URL', url('/webshop/glogin'));
define('FB_APP_ID', '2244397325596092');
define('FB_APP_SECRET', '7bc9f8a4e8d049f9569edfe297a95e9d');
define('FB_REDIRECT_URL', url('/webshop/fblogin/'));
define('ISO_CODE', 'en');
define("SNEL_API_KEY","37614a4a9b504edc9166af069675f545");
define("SNEL_CLIENT_KEY","SjJrVW0rRVBTTmVpa0hmSjI2VkFuaEtXeXFTbGtQOEJsVWozM1AvVDh2REJGUFgweUsyU1N1RWluc29QMVNpMGFUc0hvRVp5bmFoQ1RNYXFSNVBuN2h4TUc4UVhteVJoRzN6OXRySFJOUEZIdFA0K0VWMjY3Mk80M0wwckJndVArLzlzbm9acVRSRERFa253ZVZDbUszZ2thd04yK0FhL0dzclB3K1FJTWpTSWxlVVJKRFpHTnBUNGRDZlFQMTlERHVadDc4M2xPNkZZb2xkV1J6UDVabGVaSUpTYmFkTWI2THA3b0JaOG5mYzhNNGdHdzRMc1NxWmliaTBtcXExODpjdWpLSGFlaG5WQXBBaE0zMDl5d0V4VE5kdGhLTEtHNmNtL09hakF4SnRTblZpWTg4ZkNuRy9WWktkbG1LN1NwWXk0QjFIT3JUR0t4SnNlSFpyenkvN0lGeFRpa25tekdoTUJzSCtiZ0RpcTFuTitmZmFOMHNaS3lTSWkxNTJWQ3FDOGhBbjYzd0V0ditPYmR6ZlU0bDdqQmtmellJUEpQZ2t2Vm9xTDFjdmtKWXhRL2pCN0h2QmF5M2pOajF1QmFIdjA4dmFkUDhyRW9OV284cnJMRUlLa1MrUGl6Z1dBV29mNjZUS0I2ZERZb0NTQTBSdlJFUEJuWVhKelJPSzJa");
require_once MailChimp_PATHS.'/MailChimp.php';
require_once DOMPDF_PATH.'/lib/html5lib/Parser.php';
require_once DOMPDF_PATH.'/src/Autoloader.php';
require_once Mandrill_PATH.'/Mandrill.php';
require_once GOOGLE_LOGIN_PATHS.'/autoload.php';
require_once FACEBOOK_LOGIN_PATHS.'/autoload.php';
use \DrewM\MailChimp\MailChimp;
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
//use Cache;




class WebshopController extends BaseController {


    public function __construct(){
		// Call Google API

        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to CodexWorld.com');
        $gClient->setClientId(GOOGLE_CLIENT_ID);
        $gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
        $gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
        //$gClient->setScopes(Google_Service_Gmail::GMAIL_READONLY);
        $gClient->addScope("https://www.googleapis.com/auth/userinfo.profile");
        $gClient->addScope("https://www.googleapis.com/auth/userinfo.email");
        $gClient->addScope("https://www.googleapis.com/auth/plus.login");
        //$gClient->setAuthConfig('credentials.json');

        $google_oauthV2 = new Google_Service_Oauth2($gClient);
        $authUrl = $gClient->createAuthUrl();
		// Call Facebook API
		$fb = new Facebook(array(
			'app_id' => FB_APP_ID,
			'app_secret' => FB_APP_SECRET,
			'default_graph_version' => 'v3.2',
		));

		// Get redirect login helper
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // Optional permissions
		$loginURL = $helper->getLoginUrl(FB_REDIRECT_URL, $permissions);


    $categories = Cache::rememberForever('categories', function()
    {
      $categories = BigCategory::select('id','parent_category_id','name','url')->where('parent_category_id', '=', 0)->where('isocode', '=', ISO_CODE)->get();
      foreach($categories as $k=>$sbcat){
        $subcategories= BigCategory::select('id','parent_category_id','name','url')->where('parent_category_id', '=', $sbcat->id)->where('isocode', '=', ISO_CODE)->get();
        $categories[$k]->subcategories=$subcategories;
        foreach($subcategories as $i=>$subcat){
          $subsubcategories= BigCategory::select('id','parent_category_id','name','url')->where('parent_category_id', '=', $subcat->id)->where('isocode', '=', ISO_CODE)->get();
          $categories[$k]->subcategories[$i]->subcategories=$subsubcategories;
        }
      }
      return $categories;
    });

		View::share('authUrl', $authUrl);
		View::share('fbauthUrl', $loginURL);
		View::share('sup_categories', $categories);
	}

	public function getProduct($url=null){
		$data = array();

		if($url != ""){
			$data['slug'] = $url;
			$product = BigProduct::where('url', '=', $url)->where('active', '=', 1)->first();
			if(isset($product->id)){
				$supproimages = Productsimages::where('product_id', '=', $product->id)->get();
				$data['product'] = $product;
				$data['product_images'] = $supproimages;
				$prds=BigProduct::select('id')->where('id', '!=', $product->id)->where('category_id', '=', $product->category_id)->where('active', '=', 1)->orderBy(DB::raw('RAND()'))->limit(8)->get();
				$prds_id=array();
				if(isset($prds[0])){
					foreach ($prds as $k =>$prd){
						$prds_id[]=$prd->id;
					}
				}
				$pro_arr=array();
				if(!empty($prds_id)){
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name','big_products.attributes','big_products.quantity')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();

					foreach ($subsupprod as $k =>$pr){
						if(isset($pro_arr[$pr->id])){
							$img=$pro_arr[$pr->id]->images;
							$n=count($img);
							$img[$n]=$pr->img_name;
						}
						else{
							$img=array();
							$img[0]=$pr->img_name;
						}
						$pro_arr[$pr->id]=$pr;
						$pro_arr[$pr->id]->images=$img;
					}
				}
				$product_vars=array();
				foreach($pro_arr  as $i => $os){
					if($os->attributes==1){
						$os_att_details=array();
						$os_att_grp_details=array();
						$pr_vars=Variations::select('id','attributes_ids')->where('product_id', '=', $os->id)->get();
						$all_attributes=array();
						if(!empty($pr_vars)){
							foreach($pr_vars as $pvar){
								$atts=explode(",",$pvar->attributes_ids);
								foreach($atts as $at){
									$all_attributes[$at]=$at;
								}
							}
						}
						if(!empty($all_attributes)){
							$os_att_details=Attribute::select('id','sup_attribute_id','attribute_group_id','name')->whereIn('sup_attribute_id', $all_attributes)->orderBy('id', 'asc')->get();
							$att_grp=array();
							if(!empty($os_att_details)){
								foreach($os_att_details as $ad){
									$att_grp[$ad->attribute_group_id]=$ad->attribute_group_id;
								}
								$os_att_grp_details=Attributegroup::select('id','sup_attribute_group_id','name')->whereIn('id',$att_grp)->orderBy('id', 'asc')->get();
							}
						}
						$pro_arr[$i]->att_details=$os_att_details;
						$pro_arr[$i]->att_grp_details=$os_att_grp_details;
						$min_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $os->id)->orderBy('retailprice', 'asc')->first();
						$max_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $os->id)->orderBy('retailprice', 'desc')->first();
						$pro_arr[$i]->min_pro_price=$min_pro_price;
						$pro_arr[$i]->max_pro_price=$max_pro_price;
						$provars = ProductVariations::select('product_variations.id','product_variations.product_id','product_variations.sku','product_variations.retailprice','product_variations.quantity','variations.attributes_ids')->leftJoin('variations','variations.id','=','product_variations.variation_id')->where('product_variations.product_id', $os->id)->get();
						foreach($provars as $pvar){
							$att_id=$pvar->attributes_ids;
							$product_vars[$os->id][$att_id]['product_variation_id']=$pvar->id;
							$product_vars[$os->id][$att_id]['product_id']=$pvar->product_id;
							$product_vars[$os->id][$att_id]['sku']=$pvar->sku;
							$product_vars[$os->id][$att_id]['retailprice']=number_format($pvar->retailprice,2);
							$product_vars[$os->id][$att_id]['quantity']=$pvar->quantity;
						}
					}
				}
				$data['mightinterested'] = $pro_arr;
				$data['might_int_product_vars']=json_encode($product_vars);

				$prds=BigProduct::select('id')->where('id', '!=', $product->id)->where('category_id', '=', $product->category_id)->where('active', '=', 1)->orderBy(DB::raw('RAND()'))->limit(8)->get();
				$prds_id=array();
				if(isset($prds[0])){
					foreach ($prds as $k =>$prd){
						$prds_id[]=$prd->id;
					}
				}
				$pro_arr=array();
				if(!empty($prds_id)){
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name','big_products.attributes','big_products.quantity')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();
					$pro_arr=array();
					foreach ($subsupprod as $k =>$pr){
						if(isset($pro_arr[$pr->id])){
							$img=$pro_arr[$pr->id]->images;
							$n=count($img);
							$img[$n]=$pr->img_name;
						}
						else{
							$img=array();
							$img[0]=$pr->img_name;
						}
						$pro_arr[$pr->id]=$pr;
						$pro_arr[$pr->id]->images=$img;
					}
				}

				$currcat= BigCategory::select('name','url')->where('id', '=', $product->category_id)->first();
				$nextpr=BigProduct::where('id', '>', $product->id)->where('active', '=', 1)->where('category_id', '=', $product->category_id)->orderBy('id', 'asc')->first();
				$prevpr=BigProduct::where('id', '<', $product->id)->where('active', '=', 1)->where('category_id', '=', $product->category_id)->orderBy('id', 'desc')->first();

				$att_grp_details=array();
				$att_details=array();
				if($product->attributes==1){
					$pr_vars=Variations::select('id','attributes_ids')->where('product_id', '=', $product->id)->get();
					$all_attributes=array();
					if(!empty($pr_vars)){
						foreach($pr_vars as $pvar){
							$atts=explode(",",$pvar->attributes_ids);
							foreach($atts as $at){
								$all_attributes[$at]=$at;
							}
						}
					}
					if(!empty($all_attributes)){
						$att_details=Attribute::select('id','sup_attribute_id','attribute_group_id','name')->whereIn('sup_attribute_id', $all_attributes)->orderBy('id', 'asc')->get();
						$att_grp=array();
						if(!empty($att_details)){
							foreach($att_details as $ad){
								$att_grp[$ad->attribute_group_id]=$ad->attribute_group_id;
							}
							$att_grp_details=Attributegroup::select('id','sup_attribute_group_id','name')->whereIn('id',$att_grp)->orderBy('id', 'asc')->get();
						}
					}
					$min_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $product->id)->orderBy('retailprice', 'asc')->first();
					$max_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $product->id)->orderBy('retailprice', 'desc')->first();
					$data['min_pro_price']=$min_pro_price;
					$data['max_pro_price']=$max_pro_price;
					$min_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $product->id)->orderBy('retailprice', 'asc')->first();

					$provars = ProductVariations::select('product_variations.id','product_variations.product_id','product_variations.sku','product_variations.retailprice','product_variations.quantity','variations.attributes_ids')->leftJoin('variations','variations.id','=','product_variations.variation_id')->where('product_variations.product_id', $product->id)->get();
					$product_vars=array();
					foreach($provars as $pvar){
						$att_id=$pvar->attributes_ids;
						$product_vars[$att_id]['product_variation_id']=$pvar->id;
						$product_vars[$att_id]['product_id']=$pvar->product_id;
						$product_vars[$att_id]['sku']=$pvar->sku;
						$product_vars[$att_id]['retailprice']=number_format($pvar->retailprice,2);
						$product_vars[$att_id]['quantity']=$pvar->quantity;
					}
					$data['product_vars']=json_encode($product_vars);
				}
				$data['att_grp_details']=$att_grp_details;
				$data['att_details']=$att_details;
				$data['prattributes']=$product->attributes;
				$product_vars=array();
				foreach($pro_arr  as $i => $os){
					if($os->attributes==1){
						$os_att_details=array();
						$os_att_grp_details=array();
						$pr_vars=Variations::select('id','attributes_ids')->where('product_id', '=', $os->id)->get();
						$all_attributes=array();
						if(!empty($pr_vars)){
							foreach($pr_vars as $pvar){
								$atts=explode(",",$pvar->attributes_ids);
								foreach($atts as $at){
									$all_attributes[$at]=$at;
								}
							}
						}
						if(!empty($all_attributes)){
							$os_att_details=Attribute::select('id','sup_attribute_id','attribute_group_id','name')->whereIn('sup_attribute_id', $all_attributes)->orderBy('id', 'asc')->get();
							$att_grp=array();
							if(!empty($os_att_details)){
								foreach($os_att_details as $ad){
									$att_grp[$ad->attribute_group_id]=$ad->attribute_group_id;
								}
								$os_att_grp_details=Attributegroup::select('id','sup_attribute_group_id','name')->whereIn('id',$att_grp)->orderBy('id', 'asc')->get();
							}
						}
						$pro_arr[$i]->att_details=$os_att_details;
						$pro_arr[$i]->att_grp_details=$os_att_grp_details;
						$min_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $os->id)->orderBy('retailprice', 'asc')->first();
						$max_pro_price=ProductVariations::select('retailprice')->where('product_id', '=', $os->id)->orderBy('retailprice', 'desc')->first();
						$pro_arr[$i]->min_pro_price=$min_pro_price;
						$pro_arr[$i]->max_pro_price=$max_pro_price;

						$provars = ProductVariations::select('product_variations.id','product_variations.product_id','product_variations.sku','product_variations.retailprice','product_variations.quantity','variations.attributes_ids')->leftJoin('variations','variations.id','=','product_variations.variation_id')->where('product_variations.product_id', $os->id)->get();
						foreach($provars as $pvar){
							$att_id=$pvar->attributes_ids;
							$product_vars[$os->id][$att_id]['product_variation_id']=$pvar->id;
							$product_vars[$os->id][$att_id]['product_id']=$pvar->product_id;
							$product_vars[$os->id][$att_id]['sku']=$pvar->sku;
							$product_vars[$os->id][$att_id]['retailprice']=number_format($pvar->retailprice,2);
							$product_vars[$os->id][$att_id]['quantity']=$pvar->quantity;
						}
					}
				}
				$data['our_sel_product_vars']=json_encode($product_vars);

				$data['ourselection'] = $pro_arr;
				$wishlists=array();
				if(Auth::check()){
					$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
					foreach($wishlists as $k=>$wish){
						$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
						$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product->id)->orderBy('id', 'desc')->count();
						$wishlists[$k]->count=$ct;
						$wishlists[$k]->is_pr=$is_pr;
					}
				}
				$data['wishlists']=$wishlists;
				$data['morecat']=$currcat;
				$data['nextpr']=$nextpr;
				$data['prevpr']=$prevpr;
				return View::make('webshop/web_categorydetail',$data);

			}
			else{
				return Redirect::to('/webshop');
			}
		}
		else{
			return Redirect::to('/webshop');
		}
	}


	public function getCategory($url=null){
		if(trim($url) !=""){
			$currentcat=trim($url);
			$data['slug'] = $url;
			$supcats = BigCategory::where('url', '=', $url)->first();
			if(isset($supcats->id)){
				$subsupcats = BigCategory::where('parent_category_id', '=', $supcats->id)->get();
				if(isset($subsupcats[0])){
					$data['subsupcats']=$subsupcats;
					$data['currentcat']=$currentcat;
					if($supcats->parent_category_id==0){
						$data['parentcat']="";
						$data['ppcat']="";
					}
					else{
						$parsubsupcats = BigCategory::where('id', '=', $supcats->parent_category_id)->first();
						$data['parentcat']=$parsubsupcats->url;
						$data['ppcat']="";
					}
					return View::make('webshop/all_cat', $data);
				}
				else{
					$pro_arr=array();
					$total_prdss=0;
					$prds_all=ProductCategories::select('product_id')->where('category_id', '=', $supcats->id)->get();
					$prds_id_all=array();
					if(isset($prds_all[0])){
						foreach ($prds_all as $k =>$prd){
							$prds_id_all[]=$prd->product_id;
						}
					}
					if(!empty($prds_id_all)){
						$total_prdss=BigProduct::whereIn('id',$prds_id_all)->where('active', '=', 1)->count();;
						$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->paginate(PAGINATION_COUNT);
						$prds_id=array();
						if(isset($prds[0])){
							foreach ($prds as $k =>$prd){
								$prds_id[]=$prd->id;
							}
						}

						if(!empty($prds_id)){
							$subsupprod = BigProduct::select('big_products.id',
                                              'big_products.name',
                                              'big_products.retailprice',
                                              'big_products.url',
                                              'big_products.quantity',
                                              'productsimages.img_name'
                                              )
                                      ->leftJoin('productsimages',
                                                  'productsimages.product_id','=','big_products.id')
                                      ->whereIn('big_products.id', $prds_id)
                                      ->get();
							foreach ($subsupprod as $k =>$pr){
								if(isset($pro_arr[$pr->id])){
									$img=$pro_arr[$pr->id]->images;
									$n=count($img);
									$img[$n]=$pr->img_name;
								}
								else{
									$img=array();
									$img[0]=$pr->img_name;
								}
								$pro_arr[$pr->id]=$pr;
								$pro_arr[$pr->id]->images=$img;
							}
						}
					}
					$data['products']=$pro_arr;
					$data['total_products']=$total_prdss;
					$data['currentcat']=$currentcat;
					$parcat = BigCategory::where('id', '=', $supcats->parent_category_id)->first();
					$data['parentcat']=$parcat->url;
					$parparcat = BigCategory::where('id', '=', $parcat->parent_category_id)->first();
					if(isset($parparcat->url)){
						$data['ppcat']=$parparcat->url;
					}
					else{
						$data['ppcat']="";
					}
					return View::make('webshop/web_categories', $data);
				}
			}
			else{
				return Redirect::to('/webshop');
			}
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getAllcat($id){
		$cat = null;
		$subcat = null;
		$prod = null;
		$data = array();
		$cats = Category::where('slug', '=', $id)->get();
		if(isset($cats[0])){
			$subcat=$cats[0];
			$pcatsId = $cats[0]->parentid;
			$Pcats = Category::where('id', '=', $pcatsId)->get();
			if(isset($Pcats[0])){
				$cat = $Pcats[0];
				$subcats = Category::where('parentid', '=', $cats[0]->id)->get();
				$categories = Category::where('parentid', '=', 0)->get();
				foreach($categories as $k=>$sbcat){
					$categories[$k]->subcategories = Category::where('parentid', '=', $sbcat->id)->get();
				}
				$data['categories'] = $categories;
				$data['currentcat'] = $cat;
				$data['currentsubcat'] = $subcat;
				$data['currentproduct'] = $prod;
				$data['cats'] = $cats[0];
				$data['subcats'] = $subcats;
				return View::make('webshop/all_cat', $data);
			}
			else{
				return Redirect::to('/webshop');
			}
		}
		else{
			return Redirect::to('/webshop');
		}
	}


	public function postColorvariation($id){
		$product=Products::where('id','=',$id)->whereIn('type', array('variation'))->get();
		$img = array();
		if(isset($product[0])){
			$product = $product[0];
			$images = Productimages::select('imagename')->where('productid','=',$product->id)->get();
			foreach($images as $image){
				$img[] = url($image->imagename);
			}
		}
		else{
			echo json_encode(array('success'=>false)); die;
		}

		echo json_encode(array('success'=>true, 'images'=>$img)); die;
	}

	public function postColorvariations($id){
		$product=Products::where('id','=',$id)->whereIn('type', array('variation'))->get();
		$img = array();
		$products = array();
		if(isset($product[0])){
			$product = $product[0];
			$color = '';
			$met = Productmetas::where('product_id', '=', $id)->get();

			foreach($met as $kk=>$mt){
				if($product->meta_value == '_color'){
					$color = $mt->meta_value;
					$product->{$mt->meta_key} = $mt->meta_value;
				}
				else{

					$product->{$mt->meta_key} = ucwords($mt->meta_value);
				}
			}

			$images = Productimages::select('imagename')->where('productid','=',$product->id)->get();
			foreach($images as $image){
				$img[] = url($image->imagename);
			}
			$pp = Products::where('parent','=',$product->parent)->whereIn('type', array('variation'))->get();

			if(isset($pp[0])){
				foreach($pp as $pr){
					$metas = Productmetas::where('product_id', '=', $pr->id)->get();
					$pmetas = array();
					if(isset($metas[0])){
						foreach($metas as $kk=>$v){
							$pmetas[$v->meta_key]=$v->meta_value;
						}
						$products[$pr->id] = $pmetas;
					}
				}
			}
			else{
				$products = array();
			}
			$sizes = array();
			$quantity = 0;

			foreach($products as $pr){
				if($pr['_color'] == $product->_color){
					$sizes[] = ucwords($pr['_size']);
				}
				if($pr['_quantity'] > 0){
					$quantity = $pr['_quantity'];
				}
			}
		}
		else{
			echo json_encode(array('success'=>false)); die;
		}

		echo json_encode(array('success'=>true, 'images'=>$img, 'product'=>$product, 'products'=>array('size'=>$sizes, 'quantity'=>$quantity))); die;
	}

	public function getIndex(){
		$categories = BigCategory::where('parent_category_id', '=', 0)->get();
		$data['categories'] = $categories;
		$prds=BigProduct::select('id')->where('active', '=', 1)->where('attributes', '=', 0	)->orderBy(DB::raw('RAND()'))->limit(10)->get();
		$prds_id=array();
		if(isset($prds[0])){
			foreach ($prds as $k =>$prd){
				$prds_id[]=$prd->id;
			}
		}
		$pro_arr=array();
		if(!empty($prds_id)){
			$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name','big_products.attributes','big_products.quantity')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();

			foreach ($subsupprod as $k =>$pr){
				if(isset($pro_arr[$pr->id])){
					$img=$pro_arr[$pr->id]->images;
					$n=count($img);
					$img[$n]=$pr->img_name;
				}
				else{
					$img=array();
					$img[0]=$pr->img_name;
				}
				$pro_arr[$pr->id]=$pr;
				$pro_arr[$pr->id]->images=$img;
			}
		}
		$data['products'] = $pro_arr;
		return View::make('webshop/webshop_index', $data);
	}





	public function getWeb(){

		$categories = Category::where('parentid', '=', 0)->get();

		return View::make('webshop/index', array('categories'=>$categories));
	}


	public function postCategorydata(){
		$catid=Input::get('catid');
		$category = Category::where('id', '=', $catid)->get();
		$c1 = $c2 = array();
		if((isset($category[0])) && (!empty($category[0]))){

			$c1['categoryname'] = $category[0]->categoryname;
			$c1['slug'] = $category[0]->slug;
			$c1['imgurl'] = $category[0]->imgurl;

			$categories = Category::where('parentid', '=', $catid)->get();

			foreach($categories as $k=>$cat){
				$c2[$k]['categoryname'] = $cat->categoryname;
				$c2[$k]['slug'] = $cat->slug;
				$c2[$k]['imgurl'] = $cat->imgurl;
			}
		}
		$result = array('parent'=>$c1, 'childrens'=>$c2);
		echo json_encode($result, true);
	}

	public function getCategories($slug){
		$categories = Category::where('parentid', '=', 0)->get();
		$category = Category::select('*')->where('slug','=',$slug)->get();
		//echo '<pre>';print_r($category);die;
		$products = array();
		if(!empty($category[0])){
			$catid = $category[0]->id;

			$category_attributes = ProductProperties::where('category_id', '=', $catid)->get();
			if(isset($category_attributes[0])){
				foreach($category_attributes as $k=>$catattr){
					$category_attributes[$k]->attr_values = ProductPropertyValues::where('property_id', '=', $catattr->id)->get();
				}
			}
			$catparent = Category::where('parentid','=',$category[0]->parentid)->get();
			$products=Products::where('catid','=',$catid)->whereIn('type', array('simple','variable'))->paginate(10);
			foreach($products as $k=>$product){
				$metas = Productmetas::where('product_id', '=', $product->id)->get();
				$pmetas = array();
				if(isset($metas[0])){
					foreach($metas as $kk=>$v){
						$pmetas[$v->meta_key]=$v->meta_value;
					}
					$products[$k]->metas = $pmetas;
				}
			}
			$total_products = Products::where('catid','=',$catid)->whereIn('type', array('simple','variable'))->count();
		}
		return View::make('webshop/categories', array('slug'=>$slug, 'products'=>$products, 'catparent'=>$catparent, 'categories'=>$categories, 'attrs'=>$category_attributes, 'total_products'=>$total_products));
	}

	public function postPload(){
		$html="";
		$data = array();
		$url = Input::get('slug');
		$order = Input::get('order');
		$page = Input::get('page');
		$per_page = PAGINATION_COUNT;
		if(trim($url) !=""){
			$currentcat=trim($url);
			$data['slug'] = $url;
			$supcats = BigCategory::where('url', '=', $url)->first();
			if(isset($supcats->id)){
				$subsupcats = BigCategory::where('parent_category_id', '=', $supcats->id)->get();
				if(!(isset($subsupcats[0]))){
					$pro_arr=array();
					$prds_all=ProductCategories::select('product_id')->where('category_id', '=', $supcats->id)->get();
					$prds_id_all=array();
					if(isset($prds_all[0])){
						foreach ($prds_all as $k =>$prd){
							$prds_id_all[]=$prd->product_id;
						}
					}
					if(!empty($prds_id_all)){
						if($order=='highest'){
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->orderBy('retailprice', 'desc')->paginate($per_page);
						}
						else if($order=='lowest'){
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->orderBy('retailprice', 'asc')->paginate($per_page);
						}
						else{
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->paginate($per_page);
						}
						$prds_id=array();
						if(isset($prds[0])){
							foreach ($prds as $k =>$prd){
								$prds_id[]=$prd->id;
							}
						}
						if(!empty($prds_id)){
							if($order=='highest'){
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('big_products.retailprice', 'desc')->get();
							}
							else if($order=='lowest'){
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('big_products.retailprice', 'asc')->get();
							}
							else{
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();
							}

							foreach ($subsupprod as $k =>$pr){
								if(isset($pro_arr[$pr->id])){
									$img=$pro_arr[$pr->id]->images;
									$n=count($img);
									$img[$n]=$pr->img_name;
								}
								else{
									$img=array();
									$img[0]=$pr->img_name;
								}
								$pro_arr[$pr->id]=$pr;
								$pro_arr[$pr->id]->images=$img;
							}
						}
					}
					$data['products']=$pro_arr;
					foreach($pro_arr as $k=>$product){
						$html .='<div class="col-md-3 col-sm-3 padd-sec display_2_4"><div class="product_content">
						<div class="carousel slide" id="carousel-example-generic'.$k.'" data-ride="carousel" data-interval="false">
						<ol class="carousel-indicators">';
						foreach($product->images as $i=> $img){
							if($i==0){
								$cls='active';
							}
							else{
								$cls='';
							}
							$html .='<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="'.$cls.'">
							</li>';
						}
						$html .='</ol>
						<div class="carousel-inner" role="listbox">';
							foreach($product->images as $j=> $img){
								if($j==0){
									$clss='active';
								}
								else{
									$clss='';
								}
								$html .='<div class="item '.$clss.'">
									<a href="'.url('/webshop/product').'/'.$product['url'].'">
										<img src="'.url('public/webshop/bigproducts').'/'.$img.'" class="img-responsive" alt="'.$product['name'].'">
									</a>
								</div>';
							}
						$html .='</div>
						<a href="#carousel-example-generic'.$k.'" class="left carousel-control" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-generic'.$k.'" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> </div><h2><a href="'.url('/webshop/product').'/'.$product['url'].'">'.$product['name'].'</a></h2><p>'.Config::get('view.currency').''.number_format($product['retailprice'],2).'</p></div></div>';
					}
				}
			}
		}
		echo json_encode(array('data'=>$data, 'html'=>$html));
	}

	public function postPsearch(){
		$html="";
		$data = array();
		$url = Input::get('slug');
		$order = Input::get('order');
		$per_page = PAGINATION_COUNT;
		if(trim($url) !=""){
			$currentcat=trim($url);
			$data['slug'] = $url;
			$supcats = BigCategory::where('url', '=', $url)->first();
			if(isset($supcats->id)){
				$subsupcats = BigCategory::where('parent_category_id', '=', $supcats->id)->get();
				$data['total']=0;
				if(!(isset($subsupcats[0]))){
					$pro_arr=array();
					$prds_all=ProductCategories::select('product_id')->where('category_id', '=', $supcats->id)->get();
					$prds_id_all=array();
					if(isset($prds_all[0])){
						foreach ($prds_all as $k =>$prd){
							$prds_id_all[]=$prd->product_id;
						}
					}
					if(!empty($prds_id_all)){
						$data['total']=BigProduct::whereIn('id',$prds_id_all)->where('active', '=', 1)->count();;
						if($order=='highest'){
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->orderBy('retailprice', 'desc')->paginate($per_page);
						}
						else if($order=='lowest'){
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->orderBy('retailprice', 'asc')->paginate($per_page);
						}
						else{
							$prds=BigProduct::select('id')->whereIn('id',$prds_id_all)->where('active', '=', 1)->paginate($per_page);
						}
						$prds_id=array();
						if(isset($prds[0])){
							foreach ($prds as $k =>$prd){
								$prds_id[]=$prd->id;
							}
						}
						$pro_arr=array();
						if(!empty($prds_id)){
							if($order=='highest'){
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('big_products.retailprice', 'desc')->get();
							}
							else if($order=='lowest'){
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('big_products.retailprice', 'asc')->get();
							}
							else{
								$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();
							}
							foreach ($subsupprod as $k =>$pr){
								if(isset($pro_arr[$pr->id])){
									$img=$pro_arr[$pr->id]->images;
									$n=count($img);
									$img[$n]=$pr->img_name;
								}
								else{
									$img=array();
									$img[0]=$pr->img_name;
								}
								$pro_arr[$pr->id]=$pr;
								$pro_arr[$pr->id]->images=$img;
							}
						}
					}
					$data['products']=$pro_arr;
					foreach($pro_arr as $k=>$product){
						$html .='<div class="col-md-3 col-sm-3 padd-sec display_2_4"><div class="product_content">
						<div class="carousel slide" id="carousel-example-generic'.$k.'" data-ride="carousel" data-interval="false">
						<ol class="carousel-indicators">';
						foreach($product->images as $i=> $img){
							if($i==0){
								$cls='active';
							}
							else{
								$cls='';
							}
							$html .='<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="'.$cls.'">
							</li>';
						}
						$html .='</ol>
						<div class="carousel-inner" role="listbox">';
							foreach($product->images as $j=> $img){
								if($j==0){
									$clss='active';
								}
								else{
									$clss='';
								}
								$html .='<div class="item '.$clss.'">
									<a href="'.url('/webshop/product').'/'.$product['url'].'">
										<img src="'.url('public/webshop/bigproducts').'/'.$img.'" class="img-responsive" alt="'.$product['name'].'">
									</a>
								</div>';
							}
						$html .='</div>
						<a href="#carousel-example-generic'.$k.'" class="left carousel-control" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-generic'.$k.'" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> </div><h2><a href="'.url('/webshop/product').'/'.$product['url'].'">'.$product['name'].'</a></h2><p>'.Config::get('view.currency').''.number_format($product['retailprice'],2).'</p></div></div>';
					}
				}
			}
		}
		echo json_encode(array('data'=>$data, 'html'=>$html));
	}




	public function get_product_data($category, $id){
		$p = [];
		$product_images = [];
		$product = Products::where('id', '=', $id)->get();
		if(!isset($product[0])){
			return Redirect::to('webshop');
			die('no product');
		}else
		{
		if(!empty($product[0])){
			$product = $product[0];
			$p['product_id'] = $product->id;
			$p['img'] = $product->img;
			$p['name'] = $product->name;
			$p['description'] = $product->description;
			$p['composition'] = $product->composition;
			$p['delivery_return'] = $product->delivery_return;
			$p['payments'] = $product->payment;
			$p['warranty'] = $product->warranty;
			$p['price'] = $product->price;
			$p['quantity'] = $product->quantity;
			$p['type'] = $product->type;
			if($product->type == 'simple'){
				$pmetas = Productmetas::where('product_id', '=', $product->id)->get();
				$pmeta = array();
				foreach($pmetas as $val){
					$pmeta[$val['meta_key']] = $val['meta_value'];
				}
				$p['meta'] = $pmeta;
			}
			else if($product->type == 'variable'){
				$pvars = Products::where('parent', '=', $id)->get();
				$pmeta = array();
				foreach($pvars as $k=>$pvar){
					$pvarmeta = Productmetas::where('product_id', '=', $pvar->id)->get();
					foreach($pvarmeta as $val){
						$pmeta[$pvar->id][$val['meta_key']] = $val['meta_value'];
					}
				}
				$p['meta'] = $pmeta;
			}

			foreach($p['meta'] as $k=>$v){
				if(isset($v['_size'])){
					$p['sizes'][] = $v['_size'];
				}
				if(isset($v['_regular_price'])){
					if(!isset($p['price_min'])){
						$p['price_min'] = $v['_regular_price'];
						$p['price_max'] = $v['_regular_price'];
					}
					else{
						if($p['price_min'] > $v['_regular_price']){
							$p['price_min'] = $v['_regular_price'];
						}
						else if($p['price_max'] < $v['_regular_price']){
							$p['price_max'] = $v['_regular_price'];
						}
					}
				}
				if(isset($v['_sale_price'])){
					if(!isset($p['price_min_sale'])){
						$p['price_min_sale'] = $v['_sale_price'];
						$p['price_max_sale'] = $v['_sale_price'];
					}
					else{
						if($p['price_min_sale'] > $v['_sale_price']){
							$p['price_min_sale'] = $v['_sale_price'];
						}
						else if($p['price_max_sale'] < $v['_sale_price']){
							$p['price_max_sale'] = $v['_sale_price'];
						}
					}
				}
			}

			$product_images = Images::select('*')->where('productid','=',$product->id)->get();

		}
        if(isset($category->id) && $category->id!=''){
            $catid = $category->id;
        }else{
           $catid = 1;
        }

		$attrs = array();
		$category_attributes = ProductProperties::where('category_id', '=', $catid)->get();
		if(isset($category_attributes[0])){
			foreach($category_attributes as $k=>$catattr){
				$category_attributes[$k]->attr_values = ProductPropertyValues::where('property_id', '=', $catattr->id)->get();
				$attrs[$category_attributes[$k]->slug] = $category_attributes[$k];
			}
		}

		$complete_look = $this->complete_look($id);
		$might_be_interested = $this->might_be_interested($id);
		return array('product'=>$p, 'attrs'=>$attrs, 'product_images'=>$product_images, 'complete_look'=>$complete_look, 'might_be_interested'=>$might_be_interested);
	  }
	}


	public function get_product_all(){


		$allProduct = [];
		$products = Products::offset(15)->limit(12)->get();
		//$product = Products::where('id', '=', $id)->get();

        foreach ($products as  $product) {
        $p = [];
		$product_images = [];

        if(!empty($product)){
			//$product = $product[0];
			$p['product_id'] = $product->id;
			$p['img'] = $product->img;
			$p['name'] = $product->name;
			$p['description'] = $product->description;
			$p['composition'] = $product->composition;
			$p['delivery_return'] = $product->delivery_return;
			$p['payments'] = $product->payment;
			$p['warranty'] = $product->warranty;
			$p['price'] = $product->price;
			$p['quantity'] = $product->quantity;
			$p['type'] = $product->type;
			if($product->type == 'simple'){
				$pmetas = Productmetas::where('product_id', '=', $product->id)->get();
				$pmeta = array();
				foreach($pmetas as $val){
					$pmeta[$val['meta_key']] = $val['meta_value'];
				}
				$p['meta'] = $pmeta;
			}
			else if($product->type == 'variable'){
				$pvars = Products::where('parent', '=', $id)->get();
				$pmeta = array();
				foreach($pvars as $k=>$pvar){
					$pvarmeta = Productmetas::where('product_id', '=', $pvar->id)->get();
					foreach($pvarmeta as $val){
						$pmeta[$pvar->id][$val['meta_key']] = $val['meta_value'];
					}
				}
				$p['meta'] = $pmeta;
			}

			foreach($p['meta'] as $k=>$v){
				if(isset($v['_size'])){
					$p['sizes'][] = $v['_size'];
				}
				if(isset($v['_regular_price'])){
					if(!isset($p['price_min'])){
						$p['price_min'] = $v['_regular_price'];
						$p['price_max'] = $v['_regular_price'];
					}
					else{
						if($p['price_min'] > $v['_regular_price']){
							$p['price_min'] = $v['_regular_price'];
						}
						else if($p['price_max'] < $v['_regular_price']){
							$p['price_max'] = $v['_regular_price'];
						}
					}
				}
				if(isset($v['_sale_price'])){
					if(!isset($p['price_min_sale'])){
						$p['price_min_sale'] = $v['_sale_price'];
						$p['price_max_sale'] = $v['_sale_price'];
					}
					else{
						if($p['price_min_sale'] > $v['_sale_price']){
							$p['price_min_sale'] = $v['_sale_price'];
						}
						else if($p['price_max_sale'] < $v['_sale_price']){
							$p['price_max_sale'] = $v['_sale_price'];
						}
					}
				}
			}

			$product_images = Images::select('*')->where('productid','=',$product->id)->get();

		}

		$attrs = array();
		$category_attributes = ProductProperties::where('category_id', '=', $product->catid)->get();
		if(isset($category_attributes[0])){
			foreach($category_attributes as $k=>$catattr){
				$category_attributes[$k]->attr_values = ProductPropertyValues::where('property_id', '=', $catattr->id)->get();
				$attrs[$category_attributes[$k]->slug] = $category_attributes[$k];
			}
		}

           $allProduct[]  =    array('product'=>$p, 'attrs'=>$attrs, 'product_images'=>$product_images);
        }


  //       if(isset($category->id) && $category->id!=''){
  //           $catid = $category->id;
  //       }else{
  //          $catid = 1;
  //       }




		return $allProduct;
	}


	public function getProductt($slug, $id){
		$categories = Category::where('parentid', '=', 0)->get();
		$category = Category::select('*')->where('slug','=',$slug)->get();
		$p = [];
		$product_images = [];
		$product = Products::where('id', '=', $id)->get();
		if(!empty($product[0])){
			$product = $product[0];
			$p['product_id'] = $product->id;
			$p['img'] = $product->img;
			$p['name'] = $product->name;
			$p['description'] = $product->description;
			$p['composition'] = $product->composition;
			$p['delivery_return'] = $product->delivery_return;
			$p['payments'] = $product->payment;
			$p['warranty'] = $product->warranty;
			$p['price'] = $product->price;
			$p['quantity'] = $product->quantity;
			$p['type'] = $product->type;
			if($product->type == 'simple'){
				$pmetas = Productmetas::where('product_id', '=', $product->id)->get();
				$pmeta = array();
				foreach($pmetas as $val){
					$pmeta[$val['meta_key']] = $val['meta_value'];
				}
				$p['meta'] = $pmeta;
			}
			else if($product->type == 'variable'){
				$pvars = Products::where('parent', '=', $id)->get();
				$pmeta = array();
				foreach($pvars as $k=>$pvar){
					$pvarmeta = Productmetas::where('product_id', '=', $pvar->id)->get();
					foreach($pvarmeta as $val){
						$pmeta[$k][$val['meta_key']] = $val['meta_value'];
					}
				}
				$p['meta'] = $pmeta;
			}

			foreach($p['meta'] as $k=>$v){
				if(isset($v['_size'])){
					$p['sizes'][] = $v['_size'];
				}
				if(isset($v['_regular_price'])){
					if(!isset($p['price_min'])){
						$p['price_min'] = $v['_regular_price'];
						$p['price_max'] = $v['_regular_price'];
					}
					else{
						if($p['price_min'] > $v['_regular_price']){
							$p['price_min'] = $v['_regular_price'];
						}
						else if($p['price_max'] < $v['_regular_price']){
							$p['price_max'] = $v['_regular_price'];
						}
					}
				}
				if(isset($v['_sale_price'])){
					if(!isset($p['price_min_sale'])){
						$p['price_min_sale'] = $v['_sale_price'];
						$p['price_max_sale'] = $v['_sale_price'];
					}
					else{
						if($p['price_min_sale'] > $v['_sale_price']){
							$p['price_min_sale'] = $v['_sale_price'];
						}
						else if($p['price_max_sale'] < $v['_sale_price']){
							$p['price_max_sale'] = $v['_sale_price'];
						}
					}
				}
			}

			$product_images = Images::select('*')->where('productid','=',$product->id)->get();

		}
		if(!empty($category[0])){
			$catid = $category[0]->id;
			$attrs = array();
			$category_attributes = ProductProperties::where('category_id', '=', $catid)->get();
			if(isset($category_attributes[0])){
				foreach($category_attributes as $k=>$catattr){
					$category_attributes[$k]->attr_values = ProductPropertyValues::where('property_id', '=', $catattr->id)->get();
					$attrs[$category_attributes[$k]->slug] = $category_attributes[$k];
				}
			}
		}
		$complete_look = $this->complete_look($id);
		$might_be_interested = $this->might_be_interested($id);
		return View::make('webshop/categorydetail', array('categories'=>$categories, 'product'=>$p, 'attrs'=>$attrs, 'product_images'=>$product_images, 'complete_look'=>$complete_look, 'might_be_interested'=>$might_be_interested));
	}


	public function getCategoriess(){
		$id=Input::get('id');
		$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
		foreach($catg as $val){
			 $_cat[$parentcategory['id']]['child'][]=	$val;
		}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		$parentcatid=Input::get('parentcatid');
		$product=new Products;
		$products11=Products::select('*')->where('parentcatid','=',$parentcatid)->get();
		$prod=[];
		foreach($products11 as $key =>$val11){
			$productid=$val11->id;
			$prod[$val11->id][]=$val11;
			$productimg=Images::select('*')->where('productid','=',$productid)->get();
			$prod[$val11->id]['productimages']=$productimg;
		}

		return View::make('webshop/categories')->with('catdata',$_cat)->with('products',$prod);
	}
	public function getLogin(){

		$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;


			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		return View::make('webshop/login')->with('catdata',$_cat);
	}

	// public function postLogin(){

	//  	$session = Session::getId();
	// 	$username = Input::get('username');
	// 	$password = Input::get('password');
	// 	if (Auth::attempt(['username' => $username, 'password' => $password])) {
	// 		if(Auth::user()->status == 1)
	// 		{
	// 			$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
	// 			$checkout = Checkout::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
	// 			return Redirect::to('/webshop');
	// 		}
	// 		else
	// 		{
	// 			Auth::logout();
	// 			return Redirect::to('/webshop/login');
	// 		}
	// 	}
	// 	else{
	// 		Session::flash('message-error','The username and password combination is not correct.');
	// 		return Redirect::to('/webshop/login');
	// 	}
	// }

	public function postLogin(){

        $rules = array(
            'username' => 'required',
            'password' => 'required',
        );

        $data = [
            'username' => trim(Input::get('username')),
            'password' => trim(Input::get('password')),

            ];
        $validator = Validator::make($data,$rules);

	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    //return  response()->json(['errors'=>$validator->errors()]);
	    }else
	    {

	 	$session = Session::getId();
		$username = Input::get('username');
		$password = Input::get('password');
         //print_r(Input::get('username'));exit();
        if((isset($username) && $username!='') && (isset($password) && $password!='')){
			if (Auth::attempt(['username' => $username, 'password' => $password]) || Auth::attempt(['email' => $username, 'password' => $password])) {
				//echo Auth::user()->status; exit();
				if(Auth::user()->status == 1)
				{
					$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
					//print_r($products);exit();
					$checkout = Checkout::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
				   $resultSet['MESSAGE'] = 'Your will be redirected in few seconds';
                   $resultSet['Success'] = Redirect::to('/checkout/checkout');
                   //echo json_encode($resultSet);

                   //return response()->json($resultSet);
					return Redirect::back();
				}
				else
				{
					Auth::logout();
					$resultSet['Error'] = 'The username and password combination is not correct.';
					//echo json_encode($resultSet);
					//return Redirect::to('/checkout/login');
				}
			}
			else{
				//echo "string"; exit();
				$resultSet['Error'] = 'The username and password combination is not correct.';
				//echo json_encode($resultSet);
				//Session::flash('message-error','The username and password combination is not correct.');
				//return Redirect::to('/checkout/login');
			}

	    }
	     else{
			//echo "string"; exit();
                   $resultSet['Error'] = 'The username and password combination is not correct.';
                   //echo json_encode($resultSet);
                   //return Redirect::to('/checkout/login');
		}
	  }
	}


	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/webshop/login');
	}

	public function getRegister(){

			$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;


			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		return View::make('webshop.register')->with('catdata',$_cat);
	}


	public function postRegister(){
		$rules = array(
			'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required',
            'policy' => 'required',
        );

		$data = [
            'email' => trim(Input::get('email')),
            'name' => trim(Input::get('name')),
            'password' => trim(Input::get('password')),
            'policy' => trim(Input::get('policy')),
        ];
        $validator = Validator::make($data,$rules);

	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);

	    }
		else{
			$email = Input::get('email');
			$username = preg_replace('/@.*?$/', '', $email).rand(1000,9999) ;
			$session = Session::getId();
			$name = Input::get('name');
			$password = Input::get('password');
			$privacypolicy=Input::get('policy');
			$session=Input::get('session');
			$receiveeml=Input::get('receiveeml');
			$sneluserid=$this->getRelations($username,$email);

			$user = new WebUser;
			$user->firstname = $name;
			$user->username = $username;
			$user->email = $email;
			$user->password =  Hash::make($password);
			$user->privacy_policy=$privacypolicy;
			if(isset($receiveeml)){
				$user->receieveemail=$receiveeml;
			}
			else{
				$user->receieveemail='';
			}
			$user->via=1;
			$user->type=0;
			$user->snel_id=$sneluserid;
			$user->save();
			if(isset($receiveeml)){
				$this->sendmail($email, $user->id);
			}
			//$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>$user->id]);
			//$checkout = Checkout::where('guest_session', '=', $session)->update(['user_id'=>$user->id]);
			//return Redirect::back()->with('message-success', 'Registration Successfully');
			$resultSet['Success'] = 'Registration Successfully';
            echo json_encode($resultSet);
	    }
	}

	public function getWgbanner(){
		//echo'sfsd';die;
		 $catid = Input::get('catid');
		$banner=new Banner;
		$banners= Banner::where('catid','=',$catid)->get();
		$pagerec='';
		$i=0;
		foreach($banners as $key=>$val){
				 $class='';
			if($i==0){
				$class='active';
			}
			 $pagerec.='<div class="item'.$class.'"><img src="http://inviolateinfotech.com/laravel/assets/img/'.$val->image.'"></div>';
			 //$return_arr[] =  $val->image;
			 $i++;
		}
		echo $pagerec;
	//	echo json_encode(array('images'=>$return_arr));
	}
	public function postCregister(){

		$rules = array(
            'firstname' => 'required',
            'email' => 'required|email|unique:users',
            'surname' => 'required',
            'password' => 'required',
            'policy' => 'required',
            'companyname' => 'required',
            'taxidcode' => 'required',
			'chamberofcommerce' => 'required',
        );

        $data = [
            'firstname' => trim(Input::get('name')),
            'email' => trim(Input::get('email')),
            'surname' => trim(Input::get('surname')),
            'password' => trim(Input::get('password')),
            'policy' => trim(Input::get('policy')),
            'companyname' => trim(Input::get('companyname')),
            'taxidcode' => trim(Input::get('taxidcode')),
			'chamberofcommerce' => trim(Input::get('chamberofcommerce')),
		];
        $validator = Validator::make($data,$rules);

	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    }
		else{
			$email = Input::get('email');
			$username = preg_replace('/@.*?$/', '', $email).rand(1000,9999) ;
            $session = Session::getId();
			$company = Input::get('company');
	        $cname=Input::get('companyname');
	        $taxidcode=Input::get('taxidcode');
	        $firstname=Input::get('name');
	        $lastname=Input::get('surname');
			$password = Input::get('password');
			$privacypolicy=Input::get('policy');
			$receiveeml=Input::get('receiveeml');
			$c_c = Input::get('chamberofcommerce');
			$sneluserid=$this->getRelations($username,$email);

			$user=new WebUser;
			$user->username=$username;
			$user->firstname=$firstname;
			$user->lastname=$lastname;
			$user->email=$email;
			$user->password=Hash::make($password);
			$user->privacy_policy=$privacypolicy;
			$user->chamber_commerce=$c_c;
			$user->vat=$taxidcode;
			$user->comp_name=$cname;

			if(isset($receiveeml)){
				$user->receieveemail=$receiveeml;
			}else{
				$user->receieveemail='';
			}
			$user->endsession=$session;
			if(isset($session)){
				$user->endsession=$session;
			}
			else{
				$user->endsession='';
			}
			$user->type=1;
			$user->via=1;
			$user->snel_id=$sneluserid;
			$user->save();
			$userid = $user->id;
			if(isset($receiveeml)){
				$this->sendmail($email, $userid);
			}
			$resultSet['Success'] = 'Registration Successfully';
			echo json_encode($resultSet);
		}
	}

	 private function sendmail($email, $user_id){

        $newsletter = Newsletter::where('slug', '=', 'user-subscription')->get();

		if(isset($newsletter[0])){
			$newsletter = $newsletter[0];
			$data = new Newsletterlist;
			$data->newsletter_id = $newsletter->id;
			$data->user_id = $user_id;
			$data->email = $email;
			$data->save();
		}


	    $MailChimp = new MailChimp('1c6c8a83195c12fe9f42831c9dbb2b78-us20');
		$result = $MailChimp->get('lists');
		//echo "<pre>";
		//print_r($result);

		$list_id = '63cfb27cff';

		$result = $MailChimp->post("lists/$list_id/members", [
						'email_address' => $email,
						'status'        => 'subscribed',
					]);


     }

	public function getCategorydetail($id){
		//echo "string"; exit();
		$categories = Category::where('parentid', '=', 0)->get();

		$category=new Parentcategory;
		//$sizes =  new Productsize;
		//$sizes = Productsize::all();
		$c = Parentcategory::all();
		$_cat =[];

		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;


			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		$sizes = Productsize::select('*')->where('product_id',$id)->get();
		$getproductdata=Products::select('*')->where('id','=',$id)->get();
		$productdata=$getproductdata[0];
		$getimages=Images::select('*')->where('productid','=',$id)->get();
		return View::make('webshop/categorydetail', array('categories'=>$categories))->with('catdata',$_cat)->with('img',$getimages)->with('product',$productdata)->with('sizes', $sizes);
	}


	public function postSubscribe(){

		$newsletter = Newsletter::where('slug', '=', 'user-subscription')->get();

		if(isset($newsletter[0])){
			$newsletter = $newsletter[0];
			$et = new Newsletterlist;
			$et->newsletter_id = $newsletter->id;
			$et->email = Input::get('email');
			$et->save();
		}

		return  Redirect::back();


	}



	/*
	public function postSubscribe(){


			$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;


			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}


$to=$_POST['email'];
$subject = 'Subscribe';
$message ='Thanks For your Subscription.We will get review get back to you';
$headers = 'From: webmaster@example.com' . "\r\n" .
    'Reply-To: webmaster@example.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
return  Redirect::to('/webshop/index')->with('catdata', $_cat);


	}
	*/
	public function postCart(){
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->get();
			$q = 0;
			foreach($products as $product){
				$q += $product->quantity;
			}
			echo json_encode(array('products'=>$products, 'len'=>$q, 'au'=>Auth::user()->id));
			die;
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->get();
			$q = 0;
			foreach($products as $product){
				$q += $product->quantity;
			}
			echo json_encode(array('products'=>$products, 'len'=>$q));
			die;
		}
	}

	public function getEmptycart(){
		if(Auth::check()){
			Productsessions::where('user_id', '=', Auth::user()->id)->delete();
		}
		else{
			Productsessions::where('guest_session', '=', Session::getId())->delete();
		}
		echo json_encode(array('success'=>true));
		die;
	}
	public function getDeletecartproduct($id){
		$product = Productsessions::where('id', '=', $id)->get();
		if(!empty($product[0])){
			if(Auth::check()){
				Productsessions::where('user_id', '=', Auth::user()->id)->where('id', '=', $id)->delete();
				echo json_encode(array('success'=>true));
				die;
			}
			else{
				Productsessions::where('guest_session', '=', Session::getId())->where('id', '=', $id)->delete();
				echo json_encode(array('success'=>true));
				die;
			}
		}
		echo json_encode(array('success'=>false));
		die;
	}

	public function getUpdatecartproduct($id){
		$product = Productsessions::where('id', '=', $id)->get();
		$q = Input::get('quantity');
		if(!empty($product[0])){
			if(Auth::check()){
				Productsessions::where('user_id', '=', Auth::user()->id)->where('id', '=', $id)->update(['quantity'=>$q]);
				echo json_encode(array('success'=>true));
				die;
			}
			else{
				Productsessions::where('guest_session', '=', Session::getId())->where('id', '=', $id)->update(['quantity'=>$q]);
				echo json_encode(array('success'=>true));
				die;
			}
		}
		echo json_encode(array('success'=>false));
		die;
	}

	public function postAddtocart(){
		$lessstock=0;
		$id = trim(Input::get('id'));
		$prvariation_id = trim(Input::get('prvariation_id'));
		$quantity = trim(Input::get('quantity'));
		$atts="";
		$product = BigProduct::where('id', '=', $id)->where('active', '=', 1)->get();
		if(isset($product[0])){
			$stock=$product[0]->quantity;
			if($prvariation_id != ""){
				$prvars=ProductVariations::select('variation_id','retailprice','quantity')->where('id', '=', $prvariation_id)->first();
				if(isset($prvars->variation_id)){
					$stock=$prvars->quantity;
					$product[0]->retailprice=$prvars->retailprice;
					$vproduct=Variations::select('id','attributes_ids')->where('id', '=', $prvars->variation_id)->where('product_id', '=', $id)->get();
					if(isset($vproduct[0])){
						$atts_name=array();
						$all_attributes=array();
						foreach($vproduct as $pvar){
							$atts=explode(",",$pvar->attributes_ids);
							foreach($atts as $at){
								$all_attributes[$at]=$at;
							}
						}
						foreach($all_attributes as $ats){
							$att_details=Attribute::select('name')->where('sup_attribute_id', $ats)->orderBy('id', 'asc')->first();
							if(isset($att_details->name)){
								$atts_name[]=$att_details->name;
							}
						}
						if(!empty($atts_name)){
							$atts=implode(",",$atts_name);
						}
					}
				}
			}
			$isok=1;
			if($prvariation_id !=""){
				if($atts ==""){
					$isok=0;
				}
			}
			if($isok==1){
				if(Auth::check()){
					if($prvariation_id==""){
						$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$id)->first();
					}
					else{
						$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$id)->where('product_variation_id', '=',$prvariation_id)->first();
					}
					if(isset($product_check->id)){
						$ttqty=$product_check->quantity+$quantity;
						if($ttqty > $stock){
							$ttqty=$stock;
							$lessstock=1;
						}
						$product_check->quantity = $ttqty;
						$product_check->save();
					}
					else{
						if($quantity > $stock){
							$quantity=$stock;
							$lessstock=1;
						}
						$session = new Productsessions;
						$session->user_id = Auth::user()->id;
						$session->product_id = $id;
						$session->price = $product[0]->retailprice;
						$session->name = $product[0]->name;
						$pr_images = Productsimages::select('img_name')->where('product_id', '=', $id)->orderBy('id', 'asc')->first();
						if(isset($pr_images->img_name)){
							$session->img = $pr_images->img_name;
						}
						else{
							$session->img = "";
						}
						$session->attributes = $atts;
						if($prvariation_id !=""){
							$session->product_variation_id = $prvariation_id;
						}
						else{
							$session->product_variation_id = 0;
						}
						$session->quantity = $quantity;
						$session->save();
					}
					$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
					$html = '';
					$q = 0;
					foreach($products as $product){
						$img_url=url('public/webshop/bigproducts').'/'.$product->img;
						$html .= '<div class="row">';
						$html .='<div class="col-md-4 col-sm-4 col-xs-4 basket_right"><img  src="'.$img_url.'" /></div>';
						$html .='<div class="col-md-8 col-sm-8 col-xs-8 basket_left"><div class="basket_res"><a href="#">'.$product->name.'</a></div><div class="basket_detailsec"><p>'.$product->attributes.'</p><p>'.$product->quantity.'x'.Config::get('view.currency').number_format($product->price,2).'</p></div></div>';
						$html .= '</div >';
						$q += $product->quantity;
					}
					\Session::put('data', $products->toArray());
					echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html));
					//echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html));
					die;
				}
				else{
					if($prvariation_id==""){
						$product_check = Productsessions::where('guest_session', '=', Session::getId())->where('product_id', '=',$id)->first();
					}
					else{
						$product_check = Productsessions::where('guest_session', '=', Session::getId())->where('product_id', '=',$id)->where('product_variation_id', '=',$prvariation_id)->first();
					}
					if(isset($product_check->id)){
						$ttqty=$product_check->quantity+$quantity;
						if($ttqty > $stock){
							$ttqty=$stock;
							$lessstock=1;
						}
						$product_check->quantity = $ttqty;
						$product_check->save();
					}
					else{
						if($quantity > $stock){
							$quantity=$stock;
							$lessstock=1;
						}
						$session = new Productsessions;
						$session->user_id = 0;
						$session->product_id = $id;
						$session->price = $product[0]->retailprice;
						$session->name = $product[0]->name;
						$pr_images = Productsimages::select('img_name')->where('product_id', '=', $id)->orderBy('id', 'asc')->first();
						if(isset($pr_images->img_name)){
							$session->img = $pr_images->img_name;
						}
						else{
							$session->img = "";
						}
						$session->attributes = $atts;
						if($prvariation_id !=""){
							$session->product_variation_id = $prvariation_id;
						}
						else{
							$session->product_variation_id = 0;
						}
						$session->quantity = $quantity;
						$session->guest_session = Session::getId();
						$session->save();
					}
					$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
					$html = '';
					$q = 0;
					foreach($products as $product){
						$img_url=url('public/webshop/bigproducts').'/'.$product->img;
						$html .= '<div class="row">';
						$html .='<div class="col-md-4 col-sm-4 col-xs-4 basket_right"><img  src="'.$img_url.'" /></div>';
						$html .='<div class="col-md-8 col-sm-8 col-xs-8 basket_left"><div class="basket_res"><a href="#">'.$product->name.'</a></div><div class="basket_detailsec"><p>'.$product->attributes.'</p><p>'.$product->quantity.'x'.Config::get('view.currency').number_format($product->price,2).'</p></div></div>';
						$html .= '</div >';
						$q += $product->quantity;


					}

					\Session::put('data', $products->toArray());


					echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html,'lessstock'=>$lessstock));
					//echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html));
					die;
				}
			}
		}
	}



	public function getShopGuide(){
		//$data = $this->datas();
		//$categories = Category::where('parentid', '=', 0)->get();
		return $this->render('webshop/faqs');
		//return View::make('webshop/faqs', array('data'=>$data));
		//return $this->render('webshop/faqs');
	}
	public function getContactShopping(){

		$categories = Category::where('parentid', '=', 0)->get();

		return $this->render('webshop/contact');
	}



	public function postContactShopping(){

		// dd(Input::all());

       $validator = Validator::make(Input::all(), array(
			'name'=> 'required',
			'category'=> 'required',
			'email'=> 'required',
			'phone'=> 'required',
			'privacy_policy'=> 'required',
			)
		);

		if($validator->passes()){
			$name = Input::get('name');
			$surname = Input::get('surname');
			$category = Input::get('category');
			$email = Input::get('email');
			$phone = Input::get('phone');
			$privacy_policy = Input::get('privacy_policy');
			$customer_services = Input::get('customer_services');
			$street_name = Input::get('street_name');
			$city = Input::get('city');
			$service = Input::get('service');
			$message = Input::get('message');
            $services ='';
			foreach ($service as  $value) {

				$services.= '<li>'.$value.'</li>';
			}


		try {

         $html = '<h2>Category: </h2><p>'.$category.'</p><h2>Name: </h2><p>'.$name.'</p><h2>Surname: </h2><p>'.$surname.'</p><h2>Email: </h2><p>'.$email.'</p></br><h2>Phone Number: </h2><p>'.$phone.'</p><h2>Customer Services: </h2><p>'.$customer_services.'</p></br>'.$services.'<h2>Street: </h2><p>'.$street_name.'</p><h2>City: </h2><p>'.$city.'</p></br><h2>Message: </h2><p>'.$message.'</p>';

		$mandrill = new Mandrill('8QLJqvtkRO2ZXkGdcEOWwA');
		$message = array(
		    'html' => $html,
		    'subject' => 'Contact Us',
		    'from_email' => 'test@inviolateinfotech.com',
		    'from_name' => 'globalpropertycowboys',
		    'to' => array(
		        array(
		           // 'email' => 'ak.php.develop@gmail.com',
		            'email' => 'phpengineer8@gmail.com',
		            //'email' => 'shahnoman1001@gmail.com',
		            'name' =>  'globalpropertycowboys',
		            'type' => 'to'
		        )
		    )
		);
		$result = $mandrill->messages->send($message);
		//print_r($result); exit();
		 return Redirect::back()->with('message-success', 'Your Message successfully send to Customer Service!');

		} catch(Mandrill_Error $e) {
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
		}

	}else{

		return Redirect::back()->with("message-failure", "Please provide all the required information!")->withInput();

	}

	}





	public function getReturnConditions(){

		$categories = Category::where('parentid', '=', 0)->get();

		return View::make('webshop/return_conditions', array('categories'=>$categories));
	}
	public function getRevocation(){

		$categories = Category::where('parentid', '=', 0)->get();

		return View::make('webshop/revocation', array('categories'=>$categories));
	}

	public function getAddfaq(){

		if(Auth::check()){
			$t = Faqtopic::all();
			return View::make('addfaq', array('page'=>'faqs', 'topics'=>$t));
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getAddfaqtopic(){
		if(Auth::check()){
			return View::make('addfaqtopic', array('page'=>'faqs'));
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function postAddfaq(){
		if(Auth::check()){
			$q = Input::get('q');
			$a = Input::get('a');
			$t = Input::get('topic');


			$slug 	= preg_replace('/[^ \w-]/', '', $q);
			$slug 	= strtolower(str_replace(' ', '-', $slug));


			$faq = new Faqs;
			$faq->question = $q;
			$faq->slug = $slug;
			$faq->answer = $a;
			$faq->topic_id = $t;
			$faq->save();
			return Redirect::to('/webshop/addfaq')->with('message-success', 'FAQ Added Successfully');
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function postAddfaqtopic(){
		if(Auth::check()){
			$topic = Input::get('topic');

			$slug 	= preg_replace('/[^ \w-]/', '', $topic);
			$slug 	= strtolower(str_replace(' ', '-', $slug));

			$faq = new Faqtopic;
			$faq->slug = $slug;
			$faq->name = $topic;

			if(Input::hasFile('img')){
				$file = Input::file('img');
				$dest = 'public/faqs';
				$ret = $this->save_file($dest,$file);
				if($ret != null){$faq->img = $ret;}
			}
			else{}

			$faq->save();
			return Redirect::to('/webshop/addfaqtopic')->with('message-success', 'FAQ Topic Added Successfully');
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getSanswers(){
		$topic = Input::get('q');

		$qa = Faqs::where('question', 'like', '%'.$topic.'%')->orWhere('answer', 'like', '%'.$topic.'%')->get();

		$ret = [];

		foreach($qa as $obj){
			$ret[] = array('label'=>$obj['question'], 'id'=>$obj['id'], 'slug'=>$obj['slug']);
		}

		$ret = array('Search'=>$ret);
		$ret = json_encode($ret);
		echo $ret;
	}

	public function getFaqanswer($slug){
		$qa = Faqs::where('slug', '=', $slug)->get();

		if(!empty($qa[0])){
			$qa = $qa[0];

			$topic = Faqtopic::where('id', '=', $qa['topic_id'])->get();
			$topic = $topic[0];
		}
		else{
			$topic = [];
			$qa = [];
		}
		return View::make('webshop/fanswer', array('page'=>'faqs', 'topic'=>$topic, 'qa'=>$qa));
	}

	public function getFaqtopics($slug){
		$t = Faqtopic::where('slug', '=', $slug)->get();

		if(!empty($t[0])){
			$t = $t[0];
			$qa = Faqs::where('topic_id', '=', $t['id'])->orderBy('id','desc')->paginate(10);
			foreach($qa as $val){
				echo $val['question'].'<br />';
			}
			echo $qa->links();
		}

	}

	public function getViewfaqs(){
		if(Auth::check()){
			$faqs = Faqs::orderBy('id','desc')->paginate(10);
			return View::make('viewfaqs', array('page'=>'faqs', 'faqs'=>$faqs));
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getViewfaqtopics(){
		if(Auth::check()){
			$faqtopics = Faqtopic::orderBy('id','desc')->paginate(10);
			return View::make('viewfaqtopics', array('page'=>'faqs', 'faqtopics'=>$faqtopics));
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function save_file($dest, $file){

		$file_name = $file->getClientOriginalName();

		$ret = $file->move($dest,$file_name);

		if($ret){return $dest.'/'.$file_name;}return null;
	}

	public function getBasket(){
		$products = array();
		$europe_county = array('austria','belgium','bulgaria','croatia','cyprus','czechia','denmark','estonia','finland','france','germany','greece','hungary','ireland','italy','latvia','lithuania','luxembourg','malta','netherlands','poland','portugal','romania','slovakia','slovenia','spain','sweden','united kingdom');
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
			$webusers = UserWeb::select('id','country','type')->where('id', '=', Auth::user()->id)->first();
			if(($webusers->type)=='1'){
				if($webusers->country !=""){
					if((strtolower($webusers->country))=='netherlands'){
						$data['tax']=Tax_Neth_Co;
					}
					else if(in_array(strtolower($webusers->country), $europe_county)){
						$data['tax']=Tax_Eu_Co;
					}
					else{
						$data['tax']=Tax_Outside_Eu;
					}
				}
				else{
					$countr=$this->getUsercountry();
					if($countr==strtolower('Netherlands')){
						$data['tax']=Tax_Neth_Co;
					}
					else if(in_array(strtolower($countr), $europe_county)){
						$data['tax']=Tax_Eu_Co;
					}
					else{
						$data['tax']=Tax_Outside_Eu;
					}
				}
			}
			else{
				if($webusers->country !=""){
					$cont=strtolower($webusers->country);
					if(($webusers->country)=='netherlands'){
						$data['tax']=Tax_Neth_In;
					}
					else if(in_array($cont,$europe_county)){
						$data['tax']=Tax_Eu_In;
					}
					else{
						$data['tax']=Tax_Outside_Eu;

					}
				}
				else{
					$countr=$this->getUsercountry();
					if($countr==strtolower('Netherlands')){
						$data['tax']=Tax_Neth_In;
					}
					else if(in_array(strtolower($countr), $europe_county)){
						$data['tax']=Tax_Eu_In;
					}
					else{
						$data['tax']=Tax_Outside_Eu;
					}
				}

			}
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
			$guestuser = GuestUser::where('guest_session', '=', Session::getId())->first();
			if(isset($guestuser->id)){
				if(($guestuser->type)=='1'){
					if($guestuser->country !=""){
						$cont=strtolower(trim($guestuser->country));
						if($cont=='netherlands'){
							$data['tax']=Tax_Neth_Co;
						}
						else if(in_array($cont, $europe_county)){
							$data['tax']=Tax_Eu_Co;
						}
						else{
							$data['tax']=Tax_Outside_Eu;
						}
					}
					else{
						$countr=$this->getUsercountry();
						if($countr=='netherlands'){
							$data['tax']=Tax_Neth_Co;
						}
						else if(in_array($countr, $europe_county)){
							$data['tax']=Tax_Eu_Co;
						}
						else{
							$data['tax']=Tax_Outside_Eu;
						}
					}
				}
				else{
					if($guestuser->country !=""){
						$cont=strtolower(trim($guestuser->country));
						if($cont=='netherlands'){
							$data['tax']=Tax_Neth_In;
						}
						else if(in_array($cont,$europe_county)){
							$data['tax']=Tax_Eu_In;
						}
						else{
							$data['tax']=Tax_Outside_Eu;

						}
					}
					else{
						$countr=$this->getUsercountry();
						if($countr=='netherlands'){
							$data['tax']=Tax_Neth_In;
						}
						else if(in_array($countr, $europe_county)){
							$data['tax']=Tax_Eu_In;
						}
						else{
							$data['tax']=Tax_Outside_Eu;
						}
					}

				}
			}
			else{
				$countr=$this->getUsercountry();
				if($countr=='netherlands'){
					$data['tax']=Tax_Neth_In;
				}
				else if(in_array($countr, $europe_county)){
					$data['tax']=Tax_Eu_In;
				}
				else{
					$data['tax']=Tax_Outside_Eu;
				}
			}
		}
		$bigprod=array();
		foreach($products as $k=>$s){
			$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
			$products[$k]->products=$prod_detail;
			if($s->product_variation_id !=0){
				$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
				$products[$k]->prvars=$prvars ;
			}
		}
		$data['page'] = 'basket';
		$data['products'] = $products;
		return View::make('webshop/basket', $data);
	}

	public function getGuestcheckout(){
		$checkout = new Checkout;
		if(Auth::check()){
			$checkout->user_id = Auth::user()->id;
			$checkout->save();
			return Redirect::to('/checkout/checkout');
		}
		else{
			$checkout->guest_session = Session::getId();
			$checkout->save();
			return Redirect::to('/checkout/checkout');
		}
	}

	public function getCheckout(){

		$countries = Shipcountriesnames::orderBy('countries_name', 'asc')->get();
		$allcountrues=array();
		foreach($countries as $k=>$ct){
			$allcountrues[$ct->countries_name]=$ct->countries_name;
		}


		$categories = Category::where('parentid', '=', 0)->get();
		$products = array();
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->get();
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->get();
		}
		foreach($products as $k=>$s){
			$p=Products::where('id','=',$s->product_id)->get();
			$ss = Productmetas::where('product_id', '=', $s->product_id)->get();
			if(isset($ss[0])){
				foreach($ss as $ms){
					$products[$k]->{$ms->meta_key} = $ms->meta_value;
				}
			}
			if(isset($p[0])){$p = $p[0];
				$products[$k]->cat = Category::select('slug')->where('id', '=', $p->catid)->get();
				$products[$k]->parentcat = Category::select('slug')->where('id', '=', $p->parentcatid)->get();
			}


		}

		$checkout = null;

		if(Auth::check()){
			$checkout = Checkout::where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
		}
		else{
			$checkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
		}
		if(isset($checkout[0])){
			$checkout = $checkout[0];
		}
		else{
			if(!Auth::check()){
				return View::make('webshop/check_checkout');
			}
		}

		return View::make('webshop/checkout', array('page'=>'basket', 'products'=>$products, 'categories'=>$categories, 'checkout'=>$checkout,'allcountrues'=>$allcountrues));
	}

	public function postCheckout(){


		$products = array();
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->get();

		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->get();
		}

		$items = 0;
		$total = 0;
		foreach($products as $session){
			$items = $items + $session->quantity;
			$total = $total + ($session->quantity * $session->price);
		}
		//echo '';print_r([Input::all(), $total]);die;
		$validator = Validator::make(Input::all(), array(
			'email'=> 'required',
			'name'=> 'required',
			//'surname'=> 'required',
			'number'=> 'required',
			'nif'=> 'required',
			'address'=> 'required',
			'province'=> 'required',
			'city'=> 'required',
			'telephone'=> 'required',
			'postal_code'=> 'required',
			'delivery_type'=> 'required',
			'total_price'=> 'required'
			)
		);


		if($validator->passes() && (Input::get('total_price') == $total)){

			$use_as_blling = Input::get('use_as_blling');
			$email = Input::get('email');
			$name = Input::get('name');
			//$surname = Input::get('surname');
			$nif = Input::get('nif');
			$province = Input::get('province');
			$address = Input::get('address');
			$number = Input::get('number');
			$postal_code = Input::get('postal_code');
			$city = Input::get('city');
			$telephone = Input::get('telephone');
			$delivery_type = Input::get('delivery_type');

			if(Auth::check()){
				$checkout = Checkout::where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			else{
				$checkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			if(isset($checkout[0])){
				$checkout = $checkout[0];
			}
			else{
				$checkout = new Checkout;
			}

			if(Auth::check()){
				$checkout->user_id = Auth::user()->id;
			}
			else{
				$checkout->guest_session = Session::getId();
			}

			$checkout->shipping_method = $delivery_type;
			$checkout->name = $name;
			//$checkout->surname = $surname;
			$checkout->email = $email;
			$checkout->nif = $nif;
			$checkout->province = $province;
			$checkout->number = $number;
			$checkout->telephone = $telephone;
			$checkout->order_number = '#'.time();
			if($use_as_blling == 'on'){
				$checkout->billing_address = $address;
				$checkout->billing_city = '';
				$checkout->billing_state = '';
				$checkout->billing_postalcode = $postal_code;
			}
			else{

			}
			$checkout->shipping_address = $address;
			$checkout->shipping_city = $city;
			$checkout->shipping_state = '';
			$checkout->shipping_postalcode = $postal_code;
			$checkout->shipping_method = '';
			$checkout->shipping_cost = '';
			$checkout->total_cost = $total;
			$checkout->order_notes = '';
			$checkout->save();

			if(Auth::check()){
				Productsessions::where('user_id', '=', Auth::user()->id)->update(array('checkout_id'=>$checkout->id));

			}
			else{
				Productsessions::where('guest_session', '=', Session::getId())->update(array('checkout_id'=>$checkout->id));
			}

			return Redirect::to('/webshop/payment');

		}
		return Redirect::to('/webshop/checkout')->with("message-failure", "Please provide all the required information!")->withInput();
	}

	public function getPayment(){
		$categories = Category::where('parentid', '=', 0)->get();
		$products = array();
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->get();
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->get();
		}
		foreach($products as $k=>$s){
			$p=Products::where('id','=',$s->product_id)->get();
			$ss = Productmetas::where('product_id', '=', $s->product_id)->get();
			if(isset($ss[0])){
				foreach($ss as $ms){
					$products[$k]->{$ms->meta_key} = $ms->meta_value;
				}
			}
			if(isset($p[0])){$p = $p[0];
				$products[$k]->cat = Category::select('slug')->where('id', '=', $p->catid)->get();
				$products[$k]->parentcat = Category::select('slug')->where('id', '=', $p->parentcatid)->get();
			}
		}
		return View::make('webshop/payment', array('page'=>'basket', 'products'=>$products, 'categories'=>$categories));
	}

	public function getAddtoc(){

if (Session::has('products'))
{
//Session::forget('products');
    $p = Session::get('products');echo '<pre>';print_r($p);
}
	die('qw');


		//$p = Session::forget('products');echo '<pre>';print_r($p);

		$p = Session::get('products');echo '<pre>';print_r($p);die;

		/*$i = Session::get('products');
		if(empty($i)){
			Session::put('products', []);
		}*/

		$item_type = Input::get('type');
		$quantity = Input::get('quantity');
		$item = array();
		if($item_type == "item_name")
		{
			$item_id = Input::get('item_id');

			$i = Session::get('products');
			//echo '<pre>';print_r($i);die;
			if(empty($i)){
				if(!empty($item_id)){
					$product=Products::where('id','=',$item_id)->get();
					$image=Images::where('productid','=',$item_id)->limit(1)->get();
					if(isset($product[0])){
						$product=$product[0];
						$image=$image[0];
						$item['name'] = $product['productname'];
						$item['price'] = $product['price'];
						$item['img'] = $image['imagename'];
						$item['size'] = $product['size'];
						$item['quantity'] = $quantity;
						echo '<pre>';print_r($item);
						Session::put('products', $item);
						$products = Session::get('products');
						print_r($products);die;
					}
				}
			}
			else
			{
				if(!empty($item_id)){
					$product=Products::where('id','=',$item_id)->get();
					$image=Images::where('productid','=',$item_id)->limit(1)->get();
					if(isset($product[0])){
						$product=$product[0];
						$image=$image[0];
						$item['name'] = $product['productname'];
						$item['price'] = $product['price'];
						$item['img'] = $image['imagename'];
						$item['size'] = $product['size'];
						$item['quantity'] = $quantity;
						echo '<pre><b>pushing..</b>';print_r($item);
						Session::push('products', $item);
						$products = Session::get('products');
						print_r($products);die;
					}
				}
				/*$_SESSION['name'][]=$_POST['item_name'];
				$_SESSION['price'][]=$_POST['item_price'];
				$_SESSION['src'][]=$_POST['item_src'];
				echo count($_SESSION['name']);*/
			}

			exit;
		}
	 	if($item_type == "total_cart_items")
		{
			echo count($_SESSION['name']);
			exit();
		}
		if($item_type == "show_cart")
		{
			for($i=0;$i<count($_SESSION['name']);$i++)
			{
			 /* echo "<div class='cart_items'>";
			  echo "<img src='http://inviolateinfotech.com/laravel/assets/img/".$_SESSION['src'][$i]."'  width='100px' height='100px>";
			  echo "<p>".$_SESSION['name'][$i]."</p>";
			  echo "<p>".$_SESSION['price'][$i]."</p>";
			  echo "</div>";*/
			}
			exit();

		}
	}

	public function postAddattr(){
		$attr_names = Input::get('attr_name');
		$attr_values = Input::get('attr_values');
		$attrs = [];
		if(count($attr_names) == count($attr_values)){
			$len = count($attr_values);
			for($i = 0; $i< $len; $i++){
				$vals = explode('|', $attr_values[$i]);
				foreach($vals as $val){
					$attrs[$attr_names[$i]][] = trim($val);
				}
			}
			$attrs = serialize($attrs);
			$pmeta = new Productmetas;
			$pmeta->product_id = 0;
			$pmeta->meta_key = 'product_attributes';
			$pmeta->meta_value = $attrs;
			$pmeta->save();
			echo $pmeta->id;
			die;
		}
		echo 0;
		die;
	}

	public function postGetattr(){
		$attr_id = Input::get('attr_id');
		$attr = Productmetas::where('id', '=', $attr_id)->get();
		echo json_encode(unserialize($attr[0]['meta_value']));
		die;
	}

	public function postGetvariations(){

		$cat = Input::get('cat');
		$type = Input::get('type');
		//$attrs = Productmetas::where('id', '=', $attr_id)->get();
		$prop  = ProductProperties::where('category_id', '=', $cat)->get();
		$html = '';
		//if(!empty($attrs[0])){
			/*$html .= '<div class="col-md-12" style="margin-top:15px !important; border-top:1px solid #dedede;">';
			if(!empty($attrs[0])){
				$attrs = unserialize($attrs[0]['meta_value']);
				foreach($attrs as $k=>$attr){
					$html .= '<div class="col-md-6">'.$k.'<br /><select name="'.$k.'[]" class="form-control">';
					foreach($attr as $atr){
						$html .= '<option value="'.$atr.'">'.ucwords($atr).'</option>';
					}
					$html .= '</select><br /></div>';
				}
			}*/

			$html .= '
				<script type="text/javascript">
				jQuery(function(){
					jQuery(".colorpicker").hexColorPicker();
				});
				</script>
				<div class="col-md-12" style="margin-top:15px !important;padding:8px; border-top:1px solid #dedede;">
					<div class="col-md-6">
						Regular Price
						<input name="variation_property['.$type.'][regular_price]" class="form-control" />
					</div>
					<div class="col-md-6">
						Sale Price
						<input name="variation_property['.$type.'][sale_price]" class="form-control" />
					</div>
					<div class="col-md-6">
						Stock
						<select name="variation_property['.$type.'][stock]" class="form-control">
							<option value="in">In Stock</option>
							<option value="out">Out Of Stock</option>
						</select>
					</div>
					<div class="col-md-6">
						Quantity
						<input name="variation_property['.$type.'][quantity]" class="form-control" />
					</div>

					<div class="col-md-6">
						<div style="cursor:pointer;color: #535644;font-size:14px;" class="">
							<span >Upload Image</span>
							<input type="file" name="variation_image['.$type.'][]"  multiple />
						</div>
					</div>
					<div class="col-md-6">
						Ref
						<input name="variation_property['.$type.'][ref]" class="form-control" />
					</div>
					<div class="col-md-6">
						Color
						<input name="variation_property['.$type.'][color]" class="form-control colorpicker" />
					</div>
			';

			foreach($prop as $property){
				$html .= '<div class="col-md-6">';
				$html .= $property->name;
				$html .= '<select name="variation_property['.$type.']['.$property->name.']" class="form-control">';
				$vals = ProductPropertyValues::where('property_id', '=', $property->id)->get();
				foreach($vals as $val){
					$html .= '<option value="'.$val->name.'">'.$val->name.'</option>';
				}
				$html .= '</select>';
				$html .= '</div>';
			}
			$html .= '</div>';
		//}



		echo json_encode(['html'=>$html]);
		die;
	}

	public function complete_look($product_id){
		$product = Products::where('id', '=', $product_id)->whereIn('type', array('simple', 'variable'))->get();
		if(!isset($product[0])){
			die('no product');
		}
		$parentcatid = $product[0]->parentcatid;
		$categories = Category::where('parentid', '=', $parentcatid)->get();
		$cids = array();
		$cats = array();
		foreach($categories as $cat){
			if($product[0]->catid != $cat->id){
				$cids[] = $cat->id;
			}
			$cats[$cat->id] = $cat;
		}
		$cids = array_unique($cids);
		$products = Products::whereIn('catid', $cids)->distinct('catid')->get();
		$products = Products::whereIn('catid', $cids)->orderBy('views', 'desc')->whereIn('type', array('simple','variable'))->distinct('catid')->limit(5)->get();
		foreach($products as$k=>$p){
			$products[$k]->category_name = $cats[$p->catid]->slug;
			$products[$k]->cat = Category::select('slug')->where('id', '=', $p->catid)->get();
			$products[$k]->parentcat = Category::select('slug')->where('id', '=', $p->parentcatid)->get();
		}
		return $products;
	}

	public function might_be_interested($product_id){
		$product = Products::where('id', '=', $product_id)->whereIn('type', array('simple', 'variable'))->get();
		if(!isset($product[0])){
			die('no product');
		}
		$parentcatid = $product[0]->parentcatid;
		$categories = Category::where('parentid', '!=', 0)->get();
		$cids = array();
		$cats = array();
		foreach($categories as $cat){
			if($product[0]->catid != $cat->id){
				$cids[] = $cat->id;
			}
			$cats[$cat->id] = $cat;
		}
		$cids = array_unique($cids);
		$products = Products::whereIn('catid', $cids)->orderBy('views', 'asc')->whereIn('type', array('simple','variable'))->limit(5)->get();
		foreach($products as$k=>$p){
			$products[$k]->category_name = $cats[$p->catid]->slug;
			$products[$k]->cat = Category::select('slug')->where('id', '=', $p->catid)->get();
			$products[$k]->parentcat = Category::select('slug')->where('id', '=', $p->parentcatid)->get();
		}
		return $products;
	}

	public function getCompletelook($product_id){
		$products = $this->complete_look($product_id);

		echo '<pre>';print_r($products);die;

	}

	public function postVariablesize(){
		$product_id = Input::get('id');
		$size = Input::get('size');
		$product = Products::where('id', '=', $product_id)->get();
		$size_product = '';
		if(isset($product[0])){
			$product = $product[0];
			if(($product->type == 'variable') || ($product->type == 'variation')){
				if(($product->type == 'variation')){
					$product = Products::where('id', '=', $product->parent)->get();
					if(isset($product[0])){
						$product = $product[0];
					}
				}
				$products = Products::select('id')->where('parent', '=', $product->id)->get();
				$ps = array();
				$pp = array();
				foreach($products as $p){
					$pp[] = $p->id;
					$ps[$p->id] = $p;
				}
				$productm = Productmetas::where('meta_key', '=', '_size')->where('meta_value', '=', $size)->whereIn('product_id', $pp)->get();
				if(isset($productm[0]) && (isset($ps[$productm[0]->product_id]))){
					$metas = Productmetas::where('product_id', $productm[0]->product_id)->get();
					$pmetas = array();
					if(isset($metas[0])){
						foreach($metas as $kk=>$v){
							$pmetas[$v->meta_key]=$v->meta_value;
						}
						$ps[$productm[0]->product_id]->metas = $pmetas;
					}
					$size_product = $ps[$productm[0]->product_id];
				}

			}
			else{
				$productm = Productmetas::where('meta_key', '=', '_size')->where('meta_value', '=', $size)->where('product_id', '=',$product->id)->get();
				if(isset($productm[0])){
					$metas = Productmetas::where('product_id', $productm[0]->product_id)->get();
					$pmetas = array();
					if(isset($metas[0])){
						foreach($metas as $kk=>$v){
							$pmetas[$v->meta_key]=$v->meta_value;
						}
						$product->metas = $pmetas;
					}
					$size_product = $product;
				}
			}
		}
		echo json_encode(array('product'=>$size_product));die;
	}

	function getListorders(){
		$orders = Checkout::orderBy('id', 'desc')->paginate(20);
		return View::make('webshop/vieworders', array('page'=>'webshop', 'orders'=>$orders));
	}

	function getVieworder($order){
	if(Auth::check()){
		$order = Checkout::where('id', '=', $order)->get();
		if(isset($order[0])){
			$order = $order[0];
			$order->products = Productsessions::where('checkout_id', '=', $order->id)->get();
		}
		else{
			return Redirect::to('/webshop/listorders');
		}

		return View::make('webshop/vieworder', array('page'=>'webshop', 'order'=>$order));
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	function getOrderpdf($order){
	if(Auth::check()){
		$order = Checkout::where('id', '=', $order)->get();
		if(isset($order[0])){
			$order = $order[0];
			$order->products = Productsessions::where('checkout_id', '=', $order->id)->get();
		}
		else{
			return Redirect::to('/webshop/listorders');
		}

		$dompdf = new Dompdf();

		$html = '
			<h2 style="margin-bottom:26px;">
				<span >View Order</span>
				<span style="margin-left:20px;"><a href="'.url('webshop/orderpdf').'/'.$order->id.'"><i class="fa fa-file-pdf-o"></i></a></span>
			</h2>

			<table class="table table-striped table-responsive" style="margin-bottom:50px;width:100%;">
				<tr class="form-group row" style="width:100%;">
					<th style="width:16%;"><b>Product Name</b></th>
					<th style="width:16%;"><b>Quantity</b></th>
					<th style="width:16%;"><b>Price</b></th>
					<th style="width:16%;"><b>Total Price</b></th>
				</tr>';
			foreach($order->products as $product){
				$html .=	'<tr class="form-group row">
					<td class="col-md-6">'.$product->name.'</td>
					<td class="col-md-6">'.$product->quantity.'</td>
					<td class="col-md-6">'.$product->price.'</td>
					<td class="col-md-6">'.$product->quantity * $product->price.'</td>
				</tr>';
			}
		$html .=	'</table>

			<table class="container">
				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Order ID</b></td>
					<td class="col-md-6">'.$order->order_number.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Order Status</b></td>
					<td class="col-md-6">'.ucwords($order->type).'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Buyer Name</b></td>
					<td class="col-md-6">'.$order->name.' '.$order->surname.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Buyer Email</b></td>
					<td class="col-md-6">'.$order->email.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>NIF</b></td>
					<td class="col-md-6">'.$order->nif.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Province</b></td>
					<td class="col-md-6">'.$order->province.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Number</b></td>
					<td class="col-md-6">'.$order->number.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Telephone</b></td>
					<td class="col-md-6">'.$order->telephone.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping Address</b></td>
					<td class="col-md-6">'.$order->shipping_address.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping City</b></td>
					<td class="col-md-6">'.$order->shipping_city.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping State</b></td>
					<td class="col-md-6">'.$order->shipping_state.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping Postal Code</b></td>
					<td class="col-md-6">'.$order->shipping_postalcode.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Billing Address</b></td>
					<td class="col-md-6">'.$order->billing_address.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Billing City</b></td>
					<td class="col-md-6">'.$order->billing_city.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Billing State</b></td>
					<td class="col-md-6">'.$order->billing_state.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Billing Postal Code</b></td>
					<td class="col-md-6">'.$order->billing_postalcode.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping Method</b></td>
					<td class="col-md-6">'.$order->shipping_method.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Shipping Cost</b></td>
					<td class="col-md-6">'.$order->shipping_cost.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Total Cost</b></td>
					<td class="col-md-6">'.$order->total_cost.'</td>
				</tr>

				<tr class="form-group row">
					<td class="col-xs-12 col-md-3" ><b>Order Notes</b></td>
					<td class="col-md-6">'.$order->order_notes.'</td>
				</tr>
			</table>
		';

		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'landscape');

		$dompdf->render();

		$dompdf->stream("order_".$order->order_number.".pdf");

		die;
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getMyAccount(){
		$cat = null;
		$subcat = null;
		$prod = null;
		$data = array();
		$categories = Category::where('parentid', '=', 0)->get();

		foreach($categories as $k=>$sbcat){
			$categories[$k]->subcategories = Category::where('parentid', '=', $sbcat->id)->get();
		}
		$data['categories'] = $categories;
		$data['currentcat'] = $cat;
		$data['currentsubcat'] = $subcat;
		$data['currentproduct'] = $prod;
		return View::make('webshop/my_account', $data);
	}

	public function postEmailsend()
	{
		$category = Input::get('category');
		$name = Input::get('name');
		$surname = Input::get('surname');
		$email = Input::get('email');
		$message1 = Input::get('message1');

	    Mail::send('webshop.mailsend', array('category' => $category,'name' => $name,'surname' => $surname,'email' => $email,'message1' => $message1), function($message)
		{
			$message->to('chandansinghuw@gmail.com', 'Webshop')->subject('Test webshop');
		});
		return Redirect::to('webshop/contact-shopping')->with('success', 'Thanks for Contact Us!');

	}


	public function getGlogin(){
	    $gClient = new Google_Client();
        $gClient->setApplicationName('Login to CodexWorld.com');
        $gClient->setClientId(GOOGLE_CLIENT_ID);
        $gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
        $gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
		$google_oauthV2 = new Google_Service_Oauth2($gClient);
		if(isset($_GET['code'])){
			$gClient->authenticate($_GET['code']);
			// Get user profile data from google
			$gpUserProfile = $google_oauthV2->userinfo->get();
			// Getting user profile info
			$gpUserData = array();
			$gpUserData['oauth_uid']  = !empty($gpUserProfile['id'])?$gpUserProfile['id']:'';
			$gpUserData['first_name'] = !empty($gpUserProfile['given_name'])?$gpUserProfile['given_name']:'';
			$gpUserData['last_name']  = !empty($gpUserProfile['family_name'])?$gpUserProfile['family_name']:'';
			$gpUserData['email']      = !empty($gpUserProfile['email'])?$gpUserProfile['email']:'';
			$gpUserData['gender']     = !empty($gpUserProfile['gender'])?$gpUserProfile['gender']:'';
			$gpUserData['locale']     = !empty($gpUserProfile['locale'])?$gpUserProfile['locale']:'';
			$gpUserData['picture']    = !empty($gpUserProfile['picture'])?$gpUserProfile['picture']:'';
			$gpUserData['link']       = !empty($gpUserProfile['link'])?$gpUserProfile['link']:'';
			$gpUserData['oauth_provider'] = 'google';
			$webusers = UserWeb::where('email', '=', $gpUserData['email'])->get();
			if(isset($webusers[0])){
				$userid = $webusers[0]->id;
				$wusers = UserWeb::find($userid);
				$wusers->email = $gpUserData['email'] ;
				$wusers->firstname = $gpUserData['first_name'];
				$wusers->lastname = $gpUserData['last_name'];
				$wusers->oauth_provider = $gpUserData['oauth_provider'];
				$wusers->oauth_uid = $gpUserData['oauth_uid'];
				$wusers->img = $gpUserData['picture'];
				$wusers->gender = $gpUserData['gender'];
				$wusers->locale = $gpUserData['locale'];
				$wusers->link = $gpUserData['link'];
				if($wusers->save()){
				}
			}
			else{
				$usname=explode("@",$gpUserData['email']);
				$username = $usname[0].rand(1000,9999);
				$email = $gpUserData['email'];
				$sneluserid=$this->getRelations($username,$email);
				$wuser = new UserWeb;
				$wuser->email = $gpUserData['email'] ;
				$wuser->firstname = $gpUserData['first_name'];
				$wuser->lastname = $gpUserData['last_name'];
				$wuser->username =$username ;
				$wuser->oauth_provider = $gpUserData['oauth_provider'];
				$wuser->oauth_uid = $gpUserData['oauth_uid'];
				$wuser->img = $gpUserData['picture'];
				$wuser->gender = $gpUserData['gender'];
				$wuser->locale = $gpUserData['locale'];
				$wuser->link = $gpUserData['link'];
				$wuser->via = 1;
				$wuser->privacy_policy = 0;
				$wuser->receieveemail = 0;
				$wuser->endsession = 0;
				$wuser->snel_id = $sneluserid;
				if($wuser->save()){
					$userid = $wuser->id;
				}
				else{
					return Redirect::to('/webshop');
				}
			}
			$session = Session::getId();
			if(Auth::loginUsingId($userid)){
				if(Auth::user()->status == 1){
					$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>$userid]);
					$products = Productsessions::where('user_id', '=', $userid)->where('checkout_id', '=',0)->get();
					if(isset($products[0])){
						return Redirect::to('/checkout/checkout');
					}
					else{
						return Redirect::to('/webshop');
					}
				}
				else
				{
					return Redirect::to('/webshop');
				}
			}
			else{
				return Redirect::to('/webshop');
			}
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function getFblogin(){
		if(isset($_GET['code'])){
			$fb = new Facebook(array(
				'app_id' => FB_APP_ID,
				'app_secret' => FB_APP_SECRET,
				'default_graph_version' => 'v3.2',
			));
			// Get redirect login helper
			$helper = $fb->getRedirectLoginHelper();
			if (isset($_GET['state'])) {
				$helper->getPersistentDataHandler()->set('state', $_GET['state']);
			}
			$accessToken = $helper->getAccessToken();
			$fb->setDefaultAccessToken($accessToken);

			$graphResponse = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,picture');
			$fbUser = $graphResponse->getGraphUser();
			$gpUserData = array();
			$gpUserData['first_name'] = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
			$gpUserData['last_name']  = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
			$gpUserData['email']      = !empty($fbUser['email'])?$fbUser['email']:'';
			$gpUserData['picture']     = !empty($fbUser['picture']['url'])?$fbUser['picture']['url']:'';
			$gpUserData['username']     = !empty($fbUser['id'])?$fbUser['id']:'';
			$gpUserData['oauth_provider'] = 'facebook';
			$webusers=array();
			if($gpUserData['email']!=""){
				$webusers = UserWeb::where('email', '=', $gpUserData['email'])->get();
			}
			else{
				if($gpUserData['username'] !=""){
					$webusers = UserWeb::where('username', '=', $gpUserData['username'])->get();
				}
			}
			if(isset($webusers[0])){
				$userid = $webusers[0]->id;
				$wusers = UserWeb::find($userid);
				$wusers->email = $gpUserData['email'] ;
				$wusers->firstname = $gpUserData['first_name'];
				$wusers->lastname = $gpUserData['last_name'];
				$wusers->oauth_provider = $gpUserData['oauth_provider'];
				$wusers->oauth_uid = $gpUserData['username'];
				$wusers->img = $gpUserData['picture'];
				if($wusers->save()){

				}
			}
			else{
				$username = $gpUserData['username'];
				$email = $gpUserData['email'];
				$sneluserid=$this->getRelations($username,$email);
				$wuser = new UserWeb;
				$wuser->email = $gpUserData['email'] ;
				$wuser->firstname = $gpUserData['first_name'];
				$wuser->lastname = $gpUserData['last_name'];
				$wuser->username =$gpUserData['username'] ;
				$wuser->oauth_provider = $gpUserData['oauth_provider'];
				$wuser->oauth_uid = $gpUserData['username'];
				$wuser->img = $gpUserData['picture'];
				$wuser->via = 1;
				$wuser->privacy_policy = 0;
				$wuser->receieveemail = 0;
				$wuser->endsession = 0;
				$wuser->snel_id = $sneluserid;
				if($wuser->save()){
					$userid = $wuser->id;
				}
				else{
					return Redirect::to('/webshop');
				}
			}
			$session = Session::getId();
			if(Auth::loginUsingId($userid)){
				if(Auth::user()->status == 1){
					$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>$userid]);
					$products = Productsessions::where('user_id', '=', $userid)->where('checkout_id', '=',0)->get();
					if(isset($products[0])){
						return Redirect::to('/checkout/checkout');
					}
					else{
						return Redirect::to('/webshop');
					}
				}
				else
				{
					return Redirect::to('/webshop');
				}
			}
			else{
				return Redirect::to('/webshop');
			}
		}
		else{
			return Redirect::to('/webshop');
		}
	}

	public function postPrwishlist(){

		$prvariation_id = Input::get('prvariation_id');
		$product_id = Input::get('product_id');
		$wishlists=array();
		$response="";
		if(Auth::check()){
			$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
			foreach($wishlists as $k=>$wish){
				$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
				if($prvariation_id==""){
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->orderBy('id', 'desc')->count();
				}
				else{
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->where('product_variation_id', '=', $prvariation_id)->orderBy('id', 'desc')->count();
				}
				if($is_pr==1){
					$cls="fa fa-heart";
					$cls1="removetowish";
				}
				else{
					$cls="fa fa-heart-o";
					$cls1="addtowish";
				}
				$response.='<li><a wish-list-id="'.$wish->id.'" class="'.$cls1.'" href="#"><span class="wish_nmb">'.$wish->list_name.'</span><span class="item-lenght">('.$ct.')</span><span class="wish_heartselected"><i class="'.$cls.'" aria-hidden="true"></i></span></a></li>';
			}
		}
		echo $response;
	}

	public function postPrwishadd(){
		$prvariation_id = Input::get('prvariation_id');
		$product_id = Input::get('product_id');
		$rules = array(
            'list_name' => 'required'
        );
		$data = [
            'list_name' => trim(Input::get('list_name'))
		];
        $validator = Validator::make($data,$rules);

	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			$response="";
			$name = Input::get('list_name');
			$user_id=Auth::user()->id;
			$wish_list = new Wishlist;
			$wish_list->user_id =$user_id;
			$wish_list->list_name = $name ;
			$wish_list->save();
			$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
			foreach($wishlists as $k=>$wish){
				$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
				if($prvariation_id==""){
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->orderBy('id', 'desc')->count();
				}
				else{
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->where('product_variation_id', '=', $prvariation_id)->orderBy('id', 'desc')->count();
				}
				if($is_pr==1){
					$cls="fa fa-heart";
					$cls1="removetowish";
				}
				else{
					$cls="fa fa-heart-o";
					$cls1="addtowish";
				}
				$response.='<li><a wish-list-id="'.$wish->id.'" class="'.$cls1.'" href="#"><span class="wish_nmb">'.$wish->list_name.'</span><span class="item-lenght">('.$ct.')</span><span class="wish_heartselected"><i class="'.$cls.'" aria-hidden="true"></i></span></a></li>';
			}
			$res['resp']=$response;
			echo json_encode($res);
		}
	}

	public function postAddtowish(){
		$prvariation_id = Input::get('prvariation_id');
		$product_id = Input::get('product_id');
		$list_id = Input::get('list_id');
		$wlist = new Wishlistproduct;
		$wlist->user_id = Auth::user()->id;
		$wlist->wish_list_id = $list_id;
		$wlist->product_id = $product_id;
		if($prvariation_id==""){
			$wlist->product_variation_id =0;
		}
		else{
			$wlist->product_variation_id =$prvariation_id;
		}
		$wlist->quantity = 1;
		$wlist->save();
		$wishlists=array();
		$response="";
		if(Auth::check()){
			$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
			foreach($wishlists as $k=>$wish){
				$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
				if($prvariation_id==""){
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->orderBy('id', 'desc')->count();
				}
				else{
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->where('product_variation_id', '=', $prvariation_id)->orderBy('id', 'desc')->count();
				}
				if($is_pr==1){
					$cls="fa fa-heart";
					$cls1="removetowish";
				}
				else{
					$cls="fa fa-heart-o";
					$cls1="addtowish";
				}
				$response.='<li><a wish-list-id="'.$wish->id.'" class="'.$cls1.'" href="#"><span class="wish_nmb">'.$wish->list_name.'</span><span class="item-lenght">('.$ct.')</span><span class="wish_heartselected"><i class="'.$cls.'" aria-hidden="true"></i></span></a></li>';
			}
		}
		echo $response;
	}

	public function postRemovetowish(){
		$prvariation_id = Input::get('prvariation_id');
		$product_id = Input::get('product_id');
		$list_id = Input::get('list_id');
		if($prvariation_id==""){
			Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('product_id', '=', $product_id)->where('wish_list_id', '=', $list_id)->delete();
		}
		else{
			Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('product_id', '=', $product_id)->where('wish_list_id', '=', $list_id)->where('product_variation_id', '=', $prvariation_id)->delete();
		}
		$wishlists=array();
		$response="";
		if(Auth::check()){
			$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
			foreach($wishlists as $k=>$wish){
				$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
				if($prvariation_id==""){
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->orderBy('id', 'desc')->count();
				}
				else{
					$is_pr=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->where('product_id', '=', $product_id)->where('product_variation_id', '=', $prvariation_id)->orderBy('id', 'desc')->count();
				}
				if($is_pr==1){
					$cls="fa fa-heart";
					$cls1="removetowish";
				}
				else{
					$cls="fa fa-heart-o";
					$cls1="addtowish";
				}
				$response.='<li><a wish-list-id="'.$wish->id.'" class="'.$cls1.'" href="#"><span class="wish_nmb">'.$wish->list_name.'</span><span class="item-lenght">('.$ct.')</span><span class="wish_heartselected"><i class="'.$cls.'" aria-hidden="true"></i></span></a></li>';
			}
		}
		echo $response;
	}

	public function postCheckinwish(){
    if(Auth::check())
    {
      $prvariation_id = Input::get('prvariation_id');
      $product_id = Input::get('product_id');
      $ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('product_id', '=', $product_id)->where('product_variation_id', '=', $prvariation_id)->count();
      if($ct==0){
        echo "no";
      }
      else{
        echo "yes";
      }
    } else echo "no";
	}


	public function getSearch(){
		$search = Input::get('search');
		if($search){
			$prds=BigProduct::select('id')->where('name', 'like', '%'.$search.'%')->where('active', '=', 1)->paginate(20);
			$prds_id=array();
			if(isset($prds[0])){
				foreach ($prds as $k =>$prd){
					$prds_id[]=$prd->id;
				}
			}
			$pro_arr=array();
			if(!empty($prds_id)){
				$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();
				foreach ($subsupprod as $k =>$pr){
					if(isset($pro_arr[$pr->id])){
						$img=$pro_arr[$pr->id]->images;
						$n=count($img);
						$img[$n]=$pr->img_name;
					}
					else{
						$img=array();
						$img[0]=$pr->img_name;
					}
					$pro_arr[$pr->id]=$pr;
					$pro_arr[$pr->id]->images=$img;
				}
			}
			$data['total_products']=BigProduct::select('id')->where('name', 'like', '%'.$search.'%')->where('active', '=', 1)->count();
			$data['products']=$pro_arr;
			return View::make('/webshop/web_categorysearch',$data);
		}
		else{
			return View::make('/webshop/web_categorysearch');
		}
	}

	public function postAllpsearch(){
		$html="";
		$url = Input::get('slug');
		$order = Input::get('order');
		if($url){
			if($order=="highest"){
				$prds=BigProduct::select('id')->where('name', 'like', '%'.$url.'%')->where('active', '=', 1)->orderBy('retailprice', 'desc')->paginate(20);

			}
			else if($order=="lowest"){
				$prds=BigProduct::select('id')->where('name', 'like', '%'.$url.'%')->where('active', '=', 1)->orderBy('retailprice', 'asc')->paginate(20);
			}
			else{
				$prds=BigProduct::select('id')->where('name', 'like', '%'.$url.'%')->where('active', '=', 1)->paginate(20);
			}

			$prds_id=array();
			if(isset($prds[0])){
				foreach ($prds as $k =>$prd){
					$prds_id[]=$prd->id;
				}
			}

			$pro_arr=array();
			if(!empty($prds_id)){
				if($order=="highest"){
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('retailprice', 'desc')->get();
				}
				else if($order=="lowest"){
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('retailprice', 'asc')->get();
				}
				else {
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->get();
				}

				foreach ($subsupprod as $k =>$pr){
					if(isset($pro_arr[$pr->id])){
						$img=$pro_arr[$pr->id]->images;
						$n=count($img);
						$img[$n]=$pr->img_name;
					}
					else{
						$img=array();
						$img[0]=$pr->img_name;
					}
					$pro_arr[$pr->id]=$pr;
					$pro_arr[$pr->id]->images=$img;
				}
			}
			$data['products']=$pro_arr;
			foreach($pro_arr as $k=>$product){
				$html .='<div class="col-md-3 col-sm-3 padd-sec display_2_4"><div class="product_content">
				<div class="carousel slide" id="carousel-example-generic'.$k.'" data-ride="carousel" data-interval="false">
				<ol class="carousel-indicators">';
				foreach($product->images as $i=> $img){
					if($i==0){
						$cls='active';
					}
					else{
						$cls='';
					}
					$html .='<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="'.$cls.'">
					</li>';
				}
				$html .='</ol>
				<div class="carousel-inner" role="listbox">';
					foreach($product->images as $j=> $img){
						if($j==0){
							$clss='active';
						}
						else{
							$clss='';
						}
						$html .='<div class="item '.$clss.'">
							<a href="'.url('/webshop/product').'/'.$product['url'].'">
								<img src="'.url('public/webshop/bigproducts').'/'.$img.'" class="img-responsive" alt="'.$product['name'].'">
							</a>
						</div>';
					}
				$html .='</div>
				<a href="#carousel-example-generic'.$k.'" class="left carousel-control" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-generic'.$k.'" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> </div><h2><a href="'.url('/webshop/product').'/'.$product['url'].'">'.$product['name'].'</a></h2><p>'.Config::get('view.currency').''.number_format($product['retailprice'],2).'</p></div></div>';
			}
			echo json_encode(array('data'=>$data, 'html'=>$html));
		}

		else{
			return View::make('/webshop/web_categorysearch');
		}
	}

	public function postAllphighlowsearch(){
		$html="";
		$url = Input::get('slug');
		$order = Input::get('order');
		if($url){
			if($order=='highest'){
				$prds=BigProduct::select('id')->where('name', 'like', '%'.$url.'%')->where('active', '=', 1)->orderBy('retailprice', 'desc')->paginate(20);
			}
			else{
				$prds=BigProduct::select('id')->where('name', 'like', '%'.$url.'%')->where('active', '=', 1)->orderBy('retailprice', 'asc')->paginate(20);
			}
			$prds_id=array();
			if(isset($prds[0])){
				foreach ($prds as $k =>$prd){
					$prds_id[]=$prd->id;
				}
			}
			$pro_arr=array();
			if(!empty($prds_id)){
				if($order=='highest'){
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('retailprice', 'desc')->get();
				}
				else{
					$subsupprod = BigProduct::select('big_products.id','big_products.name','big_products.retailprice','big_products.url','productsimages.img_name')->leftJoin('productsimages','productsimages.product_id','=','big_products.id')->whereIn('big_products.id', $prds_id)->orderBy('retailprice', 'asc')->get();
				}

				foreach ($subsupprod as $k =>$pr){
					if(isset($pro_arr[$pr->id])){
						$img=$pro_arr[$pr->id]->images;
						$n=count($img);
						$img[$n]=$pr->img_name;
					}
					else{
						$img=array();
						$img[0]=$pr->img_name;
					}
					$pro_arr[$pr->id]=$pr;
					$pro_arr[$pr->id]->images=$img;
				}
			}
			$data['products']=$pro_arr;
			foreach($pro_arr as $k=>$product){
				$html .='<div class="col-md-3 col-sm-3 padd-sec display_2_4"><div class="product_content">
				<div class="carousel slide" id="carousel-example-generic'.$k.'" data-ride="carousel" data-interval="false">
				<ol class="carousel-indicators">';
				foreach($product->images as $i=> $img){
					if($i==0){
						$cls='active';
					}
					else{
						$cls='';
					}
					$html .='<li data-target="#carousel-example-generic" data-slide-to="'.$i.'" class="'.$cls.'">
					</li>';
				}
				$html .='</ol>
				<div class="carousel-inner" role="listbox">';
					foreach($product->images as $j=> $img){
						if($j==0){
							$clss='active';
						}
						else{
							$clss='';
						}
						$html .='<div class="item '.$clss.'">
							<a href="'.url('/webshop/product').'/'.$product['url'].'">
								<img src="'.url('public/webshop/bigproducts').'/'.$img.'" class="img-responsive" alt="'.$product['name'].'">
							</a>
						</div>';
					}
				$html .='</div>
				<a href="#carousel-example-generic'.$k.'" class="left carousel-control" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-generic'.$k.'" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> </div><h2><a href="'.url('/webshop/product').'/'.$product['url'].'">'.$product['name'].'</a></h2><p>'.Config::get('view.currency').''.number_format($product['retailprice'],2).'</p></div></div>';
			}
			echo json_encode(array('data'=>$data, 'html'=>$html));
		}

		else{
			return View::make('/webshop/web_categorysearch');
		}
	}



	public function getTrackorder(){
		$data=array();
		$ordernum = trim(Input::get('ordernumber'));
		if($ordernum){
			$trackcarrier = Checkout::select('order_number','bigbuy_order_id')->where('order_number', '=', $ordernum)->first();
			$ordnum = $trackcarrier['order_number'];
			$orderid = $trackcarrier['bigbuy_order_id'];
			if($orderid !=""){
				$end_point='rest/tracking/order/'.$orderid.'.json';
				if(BIGBUG_MODE=='PRODUCTION'){
					$curl_url=BIGBUY_END_POINT."/".$end_point;
					$api_key=BIGBUY_API_KEY;
				}
				else{
					$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
					$api_key=SANDOX_BIGBUY_API_KEY;
				}
				$cURLConnection = curl_init();
				curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
				curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
					'Authorization: Bearer '.$api_key,'Content-Type: application/json'
				));
				curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($cURLConnection);
				curl_close($cURLConnection);
				$jsonArrayResponse = json_decode($response,TRUE);
				$data=array();
				if(isset($jsonArrayResponse[0]['trackings'][0])){
					$track = $jsonArrayResponse[0]['trackings'][0];
					$data['trackingNumber'] = $track['trackingNumber'];
					$data['statusDescription'] = $track['statusDescription'];
					$data['statusDate'] = $track['statusDate'];
					$car_traid = $track['carrier']['id'];
					$tcarr = TrackCarrier::select('carrier_name')->where('carrier_id', '=', $car_traid)->first();
					$data['carrier'] = $tcarr['carrier_name'];
				}
				else{
					return Redirect::back()->with('message-failure', 'Tracking information does not found');
				}
				return View::make('webshop/track_order',array('data'=>$data));
			}
			else{
				return Redirect::back()->with('message-failure', 'Order Number does not exist');
			}
		}
		else{
			return View::make('webshop/track_order');
		}

	}

	public function getUsercountry(){
		$country="";
		$ip=Request::getClientIp();
		$query = file_get_contents('http://www.geoplugin.net/json.gp?ip='.$ip);
		$query = json_decode($query,true);
		if(isset($query['geoplugin_countryName'])){
			$country=strtolower($query['geoplugin_countryName']);
		}
		return $country;
	}


	public function postCheckproquant(){
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->get();
			$mess = 0;
			$ok = 0;
			foreach($products as $product){
				$bigpro = BigProduct::where('id', '=', $product->product_id)->where('active', '=', 1)->first();
				if($product->product_variation_id==""){
					if(($product->quantity)<=($bigpro->quantity)){
						$ok = 1;
					}
					else{
						$mess = 1;
					}
				}
				else{
					$prvars=ProductVariations::where('id', '=', $product->product_variation_id)->first();
					if(($product->quantity)<=($prvars->quantity)){
						$ok = 1;
					}
					else{
						$mess = 1;
					}
				}
			}
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->get();
			$mess = 0;
			$ok = 0;
				foreach($products as $product){
					$bigpro = BigProduct::where('id', '=', $product->product_id)->where('active', '=', 1)->first();
					if($product->product_variation_id==""){
						if(($product->quantity)<=($bigpro->quantity)){
							$ok = 1;
						}
						else{
							$mess = 1;
						}
					}
					else{
						$prvars=ProductVariations::where('id', '=', $product->product_variation_id)->first();
						if(($product->quantity)<=($prvars->quantity)){
							$ok = 1;
						}
						else{
							$mess = 1;
						}
					}
				}

			}
			echo json_encode(array('ok'=>$ok,'mess'=>$mess));

		}

	// sealstart
	public function getAccesstoken(){
		$url = "https://auth.snelstart.nl/b2b/token";
		$client_key = SNEL_CLIENT_KEY;
		$gt = 'grant_type=clientkey&clientkey='.$client_key;
		$curl_url=$url;
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HEADER, 0);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
		));
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $gt);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$response=json_decode($response,TRUE);
		return $access_token=$response['access_token'];
	}

	//Post Relations( creates new customer )  in  Relationships
	// Klant/Leveranciers are to be changed in relatiesoort
	public function getRelations($username,$email){
		$url = "https://b2bapi.snelstart.nl/v2";
		$end_point = 'relaties';
		$api_key = SNEL_API_KEY;
		$access_token=$this->getAccesstoken();
		$curl_url=$url."/".$end_point;
		$postReq = array();
		$postReq['relatiesoort'][0]="Klant";
		$postReq['naam']=$username;
		$postReq['email']=$email;
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HEADER, 0);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json ',
			'Ocp-Apim-Subscription-Key:'.$api_key,
			'Authorization:Bearer '.$access_token
		));
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postReq));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		$response = json_decode($response, true);
		return $response['id'];
	}
}
