<style>
.md-content {
    color: #1e1e1e;
    display: block;
    position: relative;
    -webkit-overflow-scrolling: touch;
}


.md-content.md-default-theme, md-content {
    color: rgba(0,0,0,0.87);
    background-color: rgb(255,255,255);
}
.append_option {
  position: relative;
  z-index: -12;
}.form{
	margin-bottom:89px;
    width: 85%;
}
.detail-inner .detail-look {
    margin-top: -3%;
    float: left;
    width: 100%;
}

.col-lg-6
{
padding-right: 6px;
padding-left: 37px !important;
}

.md-button{
background: white;
color: #1e1e1e;
white-space: nowrap;
text-transform: uppercase;
font-weight: 500;
font-size: 1.1rem;
font-style: inherit;
font-variant: inherit;
font-family: inherit;
text-decoration: none;
overflow: hidden;
border: 1px solid #1e1e1e;}

a.md-no-style, button.md-no-style {
 font-weight:400;
 background-color:inherit;
 text-align:left;
 border:none;
 padding:0;
 margin:0
}
button, input, select, textarea {
 vertical-align:baseline
}
button, html input[type=button], input[type=reset], input[type=submit] {
 cursor:pointer;
 -webkit-appearance:button
}
button[disabled], html input[type=button][disabled], input[type=reset][disabled], input[type=submit][disabled] {
 cursor:default
}
</style>
@include('webshop.header')

    <section class="side_bar">
       		<div class="container-fluid">
    			<div class="row">
				  @include('webshop.sidebar')
    				<div class="col-sm-9 col-lg-9">
					
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="top: -195px;">
                                        <ol class="carousel-indicators">
                                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner">
										<?php 
										
									$image=explode(',',$img[0]->imagename);
									foreach($image as $key =>$val){
										if($key ==0){
														 $class='active';
													}else{
														$class='';
													}
													?>
													<div class="item <?php echo $class;?>">
														 <img src="{{url('assets/img/')}}/<?php  echo $val; ?>" alt="First-pic" style="width:100%;">
													 </div>
													<?php
													
									}
										?>
											
										
                                        </div>
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="top: 225px;background:transparent;
color: black;">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="top: 225px;background: transparent;
color: black;">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
							<?php //echo"<pre>";print_r($product);echo"</pre>";die; ?>
                                <div class="col-xs-12 col-sm-5" style="margin-top:40px;">
                                    <div class="detail-content">
                                        <h2><?php echo $product->productname;?></h2>
                                        <p>VAT INCLUDED / SHIPPING COSTS NOT INCLUDED<br>REF. 5915/088</p>
                                        <form class="form">
										 <div class="col-lg-12">
										
                                             <div class="col-lg-6">
                                                <div class="selectBox" onclick="showCheckboxes()">
                                                    <select>
													 <option>Choose A Size</option>
													<?php //foreach($product as $key =>$val1){ ?>
                                                       
														<option value="<?php //echo $val1[0]->id;?>"><?php //echo $val1[0]->size;?></option>
                                                        
													<?php //} ?>
                                                    </select>
                                                </div> 
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="selectBox_quantity" onclick="showdropdown()">
                                                    <select>
                                                        <option>Quantity</option>
														<?php 
													for($i=1;$i<=100;$i++){
														
												   echo'<option value='.$i.'>'.$i.'</option>';
													}
														?>
                                                    
                                                    </select> 
                                                </div>                 
                                            </div>
                                        </div>
										 </form>
										 
                                      <!---  <a href="{{url('webshop/addtocart')}}/add/<?php // echo $product->id; ?>" class="button" style="text-decoration: none;width: 85%">ADD TO BASKET</a>--->
									  
									  <form name="addToCartForm">
									<input type="hidden" class="inputid" value="{{$product->id}}"/>
									
									<input type="hidden" class="item_name" id="item_name" value="{{$product->productname}}"/>
									<input type="hidden" class="item_src" id="item_src" value="{{$img[0]->imagename}}"/>
									<input type="hidden" class="item_price"  id="item_price" value="{{$product->price}}"/>
									<input type="hidden" class="item_qty"  id="item_qty" value="{{$product->quantity}}"/>
									<input type="button" class="submitbutton button" style="text-decoration: none;width: 85%"value="ADD TO BASKET" onclick="cart({{$product->id}})"/>
								 </form>
									  
                                    </div>
                                </div>
                            </div>
							<div class="detail-inner" style="padding: 2px 0;">
							<div class="detail-look" style="margin-top:-11%;"> 
						<h3>complete the look</h3>
						<div class="col-xs-12 col-sm-3 col-lg-3">
							<a href="detail.html">
								<img src="{{url('assets/img/')}}/8648013251_1_1_4.jpg" alt="sofa-1" class="img-responsive">
							</a>
							<div class="product-content">
								<h5>- NEW -</h5>
								<h2><a href="#">Ornamental print duvet cover with dark base </a></h2>
								<p>45,99 �  - 69,99 � </p>
							</div>
						</div>
					</div>
					<div class="detail-carousel">
                               <h3>YOU MIGHT BE INTERESTED</h3>
    					<div class="product-inner">
    						<div class="row">
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/8697013710_1_1_4.jpg" alt="sofa-1" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Ornamental print duvet cover with dark base </a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/8641013737_1_1_4.jpg" alt="sofa-2" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Floral print duvet cover with coloured base
        								</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/8416013504_1_1_4.jpg" alt="sofa-3" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
								
									<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/8540013711_1_1_4.jpg" alt="sofa-3" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    						</div>
    					</div>
			<form method="POST" action="{{url('webshop/subscribe')}}">					
				<div class="row">
				<div class="col-lg-12">
					<div class="footer-spot"></div>
					<div class="col-lg-2"> </div>
						<div class="col-lg-8 ">
							<div class="group ">
								<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;">Join our newsletter - Enter your email address*</label>
							</div>
				 <div class="group" style="float: left;width: 100%;">
								<input class="check-content" type="checkbox" style="margin-left:180px;">
								<label style="margin-left: 20px;text-align:center;width:100%;" class="check-button">I have read and accept the Privacy policy.</label>
							</div>
								 <div class="col-sm-8 col-sm-offset-4 col-md-0 col-md-offset-0" style="text-align:center">
				  <input  type="submit" class="button" style="float:right" name="subscribe" value="SUBSCRIBE ME">
						</div></div>
						<div class="col-lg-2"></div>
				</div>
				</div>	   	
			</form>   
					</div>
                      
    				</div>
    			</div>
    		</div>	
	</section>
@include('webshop.footer')
<!---<script>
function showdropdown(){
	$(".selectBox_quantity").append('<div class="append_option"><option value="1">1</option>  <option value="2">2</option><option value="3">3</option></div>');
	}
</script>	 ----> 
  </body>
</html>

		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			 $.ajax({
				 url:'http://inviolateinfotech.com/laravel/webshop/wgbanner',
				 type:'GET',
				 data: 'catid='+id,
				 success: function(result) {
					$('.carousel-inner').html(result);
				 }
				 
				 
			 });
		 }
		 
		 
		  </script>
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
          $(function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 280,
              values: [ 0, 280 ],
              slide: function( event, ui ) {
                $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
              }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
             " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          });
        </script>
		
		
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			
		 }
		 
		 
		  </script>
		  
		  
		    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
		  <script>
	

 $(document).ready(function(){

      $.ajax({
        type:'post',
        url:'http://inviolateinfotech.com/laravel/webshop/addtocart',
        data:{
          total_cart_items:"totalitems"
        },
        success:function(response) {
				$("#total_items").text(response);

        }
      });

    });
	
	 function cart(id)
    {

	 
	  var img_src=document.getElementById("item_src").value;
	  var name=document.getElementById('item_name').value;
	  var price=document.getElementById('item_price').value;
	
	  $.ajax({
        type:'post',
        url:'http://inviolateinfotech.com/laravel/webshop/addtocart',
        data:{
          item_src:img_src,
          item_name:name,
          item_price:price
        },
        success:function(response) {
		alert(response);
			$("#total_items").text(response);
        }
      });
	
    }
	
	   function show_cart()
    {
      $.ajax({
      type:'post',
      url:'http://inviolateinfotech.com/laravel/webshop/addtocart',
      data:{
        showcart:"cart"
      },
      success:function(response) {
        document.getElementById("mycart").innerHTML=response;
        $("#mycart").slideToggle();
      }
     });

    }
		  </script>
		  
		  
		  
	