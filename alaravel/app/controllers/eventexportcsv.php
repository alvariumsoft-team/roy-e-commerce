<?php
/*
= iCal event file export script =

� Copyright 2009-2016 LuxSoft - www.LuxSoft.eu

This file is part of the LuxCal Web Calendar.

The LuxCal Web Calendar is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

The LuxCal Web Calendar is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the LuxCal 
Web Calendar. If not, see: http://www.gnu.org/licenses/.
*/

//sanity check
if (empty($lcV)) { exit('not permitted ('.substr(basename(__FILE__),0,-4).')'); } //launch via script only

//initialize
$adminLang = (file_exists('./lang/ai-'.strtolower($_SESSION['cL']).'.php')) ? $_SESSION['cL'] : "English";
require './lang/ai-'.strtolower($adminLang).'.php';

$fileName = (isset($_POST['fileName'])) ? $_POST['fileName'] : ''; //file name
$fileDes = (isset($_POST['fileDes'])) ? strip_tags($_POST['fileDes']) : ''; //file description
$usrName = (isset($_POST['usrName'])) ? $_POST['usrName'] : ''; //user filter
$catName = (isset($_POST['catName'])) ? $_POST['catName'] : ''; //category filter
$fromDda = (isset($_POST['fromDda'])) ? DDtoID($_POST['fromDda']) : ''; //from event due date
$fromMda = (isset($_POST['fromMda'])) ? DDtoID($_POST['fromMda']) : ''; //from event modified date
$tillDda = (isset($_POST['tillDda'])) ? DDtoID($_POST['tillDda']) : ''; //untill event due date
$tillMda = (isset($_POST['tillMda'])) ? DDtoID($_POST['tillMda']) : ''; //untill event modified date

$meta = array(); //iCal meta data
$calProps = array("PRODID","VERSION"); //iCal properties and components of interest


/* sub-functions */

function catList($selCat) {
	global $ax;
	$stH = dbQuery("SELECT `name`,`color`,`bgColor` FROM `categories` WHERE `status` >= 0 ORDER BY `sequence`");
	
	echo "<option value='*'>{$ax['iex_all_cats']}&nbsp;</option>\n";
	while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
		$selected = ($selCat == $row['name']) ? " selected='selected'" : '';
		$catColor = ($row['color'] ? "color:{$row['color']};" : '').($row['bgColor'] ? "background-color:{$row['bgColor']};" : '');
		echo "<option value=\"{$row['name']}\"".($catColor ? " style='{$catColor}'" : '')."{$selected}>{$row['name']}</option>\n";
	}
}

function userMenu($selUser) {
	global $ax;
	$stH = dbQuery("SELECT `name` FROM `users` ORDER BY `name`");
	echo "<option value='*'>{$ax['iex_all_users']}&nbsp;</option>\n";
	while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
		$selected = ($selUser == $row['name']) ? " selected='selected'" : '';
		echo "<option value=\"{$row['name']}\"{$selected}>{$row['name']}</option>\n";
	}
}


/* main functions */

function selectEvents() {
	global $ax, $set, $msg, $fileName, $fileDes, $usrName, $catName, $fromDda, $fromMda, $tillDda, $tillMda;
	
	if (!$fileName) {
		$fileName = substr(translit($set['calendarTitle'],true),0,60);
	}
	echo "<form action='index.php?lc' method='post' id='eventdata'>";
	if($_GET['rec']=='0'){
		echo"<div class='error_red' style='color:red;margin-bottom:20px;'><h1 style='font-size:15px;'><strong>No Record Found </strong></h1></div>";
	}
	if($_GET['rec']=='-1'){
		echo"<div class='error_red' style='color:red;margin-bottom:20px;'><h1 style='font-size:15px;'><strong>Last Date should be greater than From Date</strong></h1></div>";
	}
	
		echo"<input type='hidden' name='token' value='{$_SESSION['token']}'>
		<fieldset><legend>{$ax['iex_create_event']}</legend>\n
		<table class='list'>\n
		<tr><td class='label'>{$ax['event_file_name']}:</td><td><input type='text' name='fileName' maxlength='60' value=\"{$fileName}\" maxlength='40' size='26'> .csv</td></tr>
		<tr><td colspan='2'><hr>\n</td></tr><tr><td colspan='2'>{$ax['iex_filters']}</td></tr>
		<tr><td class='label'>{$ax['iex_owner']}:</td><td><select name='usrName' >\n";
	userMenu($usrName);
	echo "</select></td></tr>\n<tr><td class='label'>{$ax['iex_category']}:</td><td><select name='catName' >\n";
	catList($catName);
	echo "</select></td></tr>";
		echo"<tr><td class='label'>{$ax['iex_between_dates']}:</td><td>
		<input type='text' name='fromDda' id='fromDda' value='".IDtoDD($fromDda)."' size='8'>
		<button title=\"{$ax['iex_select_start_date']}\" onclick=\"dPicker(1,'nill','fromDda');return false;\">&larr;</button> &#8211;
		<input type='text' name='tillDda' id='tillDda' value='".IDtoDD($tillDda)."' size='8'>
		<button title=\"{$ax['iex_select_end_date']}\" onclick=\"dPicker(1,'nill','tillDda');return false;\">&larr;</button></td></tr>";
	// echo "<tr><td class='label'>{$ax['usr_group']}:</td>";
	// if ($uid == $_SESSION['uid'] or $uid == 2) {
		// $stH = stPrep("SELECT `name`,`color` FROM `groups` WHERE `ID` = ?");
		// stExec($stH,array($user['grpID']));
		// $row = $stH->fetch(PDO::FETCH_ASSOC);
		// $stH = null;
		// $color = $row['color'] ? " style='background-color:{$row['color']};'" : '';
		// echo "<td><input type='hidden' id='grpID' name='grpID' value='{$user['grpID']}'><span{$color}>{$row['name']}</span></td>\n";
	// } else {
		// $stH = stPrep("SELECT `ID`,`name`,`color` FROM `groups` WHERE `status` >= 0 ORDER BY `name`");
		// stExec($stH,null);
		// echo "<td class=''><select name='grpID'>\n";
		// echo"<option value=''>Select Group</option>";
		// while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
			//if ($privs == 3 or $row['ID'] != 2) {
				// $color = $row['color'] ? " style='background-color:{$row['color']};'" : '';
			//	$selected = $row['ID'] == $user['grpID'] ? " selected='selected'" : '';
				// echo "<option value='{$row['ID']}'{$color}>{$row['name']}</option>\n";
			//}
		// }
		// echo "</select></td></tr>\n";
	// }
	// echo"<br>";
	// echo "<tr><td class='label'>{$ax['mymanager']}:</td>";
		// $stH = stPrep("SELECT * FROM `users` WHERE `status` >= 0 ORDER BY `ID`");
		// stExec($stH,null);
		// echo "<td class=''><select name='userID'>\n";
		// echo"<option value=''>Select Manager</option>";
		// while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
			
				// echo "<option value='{$row['email']}'>{$row['email']}</option>\n";
		// }
		// echo "</select></td></tr>\n";
			// echo "<tr><td class='label'>{$ax['mydepartment']}:</td>";
		// $stH = stPrep("SELECT * FROM `users` WHERE `status` >= 0 ORDER BY `name`");
		// stExec($stH,null);
		// echo "<td class=''><select name='grpID1'>\n";
				// echo"<option value=''>Select Department</option>";
		// while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
	
				// echo "<option value='{$row['ID']}'>{$row['name']}</option>\n";
			
		// }
		// echo "</select></td></tr>\n";
		// echo "<tr><td class='label'>{$ax['mycompany']}:</td>";
		// $stH = stPrep("SELECT * FROM `users` WHERE `status` >= 0 ORDER BY `name`");
		// stExec($stH,null);
		// echo "<td class=''><select name='grpID2'>\n";
		// echo"<option value=''>Select Company</option>";
		// while ($row = $stH->fetch(PDO::FETCH_ASSOC)) {
			
				// echo "<option value='{$row['ID']}'>{$row['name']}</option>\n";
		// }
	// echo "</select></td></tr>\n";
	// echo "<tr><td class='label'>{$ax['usr_background']}:</td><td><input type='text' id='color' name='color' value='{$user['color1']}' size='8' maxlength='10'><button class='noPrint' title=\"{$ax['usr_select_color']}\" onclick=\"cPicker('color','uName','t');return false;\">&larr;</button></td></tr>";
	// echo "<tr><td class='label'>Mymanager+1</td><td><input type='text' id='mymanager1' name='mymanager1' value='{$user['mymanager1']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+2</td><td><input type='text' id='mymanager2' name='mymanager2' value='{$user['mymanager2']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+3</td><td><input type='text' id='mymanager3' name='mymanager3' value='{$user['mymanager3']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+4</td><td><input type='text' id='mymanager4' name='mymanager4' value='{$user['mymanager4']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+5</td><td><input type='text' id='mymanager5' name='mymanager5' value='{$user['mymanager5']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+6</td><td><input type='text' id='mymanager6' name='mymanager6' value='{$user['mymanager6']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+7</td><td><input type='text' id='mymanager7' name='mymanager7' value='{$user['mymanager7']}' size='30' maxlength='10'></td></tr>";
	// echo "<tr><td class='label'>Mymanager+8</td><td><input type='text' id='mymanager8' name='mymanager8' value='{$user['mymanager8']}' size='30' maxlength='10'></td></tr>";
	
		echo"</table>\n";
		echo"</fieldset>\n
		<input class='noPrint' type='submit' name='create' value=\"{$ax['iex_create_file']}\">\n";
	if (isset($_POST['create']) and $msg == $ax['iex_file_created']) {
		$icalfName = $fileName ? $fileName : $set['calendarTitle'];
		$icalfName = substr(translit($icalfName,true).'.ics',0,60);
		$rName = str_replace('.','-'.date("Ymd-Hi").'.',$icalfName);
		echo "&nbsp;&nbsp;&nbsp;&nbsp;<button class='noPrint' type='button' onclick=\"location.href='dloader.php?ftd={$icalfName}&amp;rName={$rName}'\">{$ax['iex_download_file']}</button>\n";
	}
	echo "</form>\n<div style='clear:right'></div>\n";
}


function makeFile() {
	global $ax, $evtList, $set, $fileName, $fileDes, $usrName, $catName, $fromDda, $fromMda, $tillDda, $tillMda;

	$icsHead = "BEGIN:VCALENDAR\r\n";
	$icsHead .= "VERSION:2.0\r\n";
	$icsHead .= "METHOD:PUBLISH\r\n";
	$icsHead .= "PRODID:- // LuxCal ".LCV." // {$set['calendarTitle']} // EN\r\n";
	$icsHead .= "X-LC-CONTENT:user: ".(($usrName != '*') ? $usrName : "all");
	$icsHead .= " // cat: ".(($catName != '*') ? $catName : "all");
	$icsHead .= " // due: ".(($fromDda) ? $fromDda : "begin")." - ".(($tillDda) ? $tillDda : "end");
	$icsHead .= " // mod: ".(($fromMda) ? $fromMda : "begin")." - ".(($tillMda) ? $tillMda : "end")."\r\n";
	$icsHead .= "X-WR-CALNAME:".(($fileDes) ? htmlspecialchars_decode($fileDes,ENT_QUOTES) : "Events")."\r\n";
	$icsHead .= "X-WR-TIMEZONE:".date_default_timezone_get()."\r\n";
	$icsHead .= "CALSCALE:GREGORIAN\r\n";

	//set event filter
	$filter = ($usrName != '*') ? " AND u.`name` = '$usrName'" : '';
	if ($catName != '*') { $filter .= " AND c.`name` = '$catName'"; }
	if ($fromMda) { $filter .= " AND SUBSTR(e.`mDateTime`,1,10) >= '$fromMda'"; }
	if ($tillMda) { $filter .= " AND SUBSTR(e.`mDateTime`,1,10) <= '$tillMda'"; }

	//set event date range
	 $sRange = ($fromDda) ? $fromDda : date('Y-m-d',time()-31536000); //-1 year
	 $eRange = ($tillDda) ? $tillDda : date('Y-m-d',time()+31536000); //+1 year
	 ///Start Csv
	if($_POST['create']!=''){
		ob_end_clean();
		$usrName=$_POST['usrName'];
		if($_POST['fileName']==''){
			$fileName='myfile';
		}else{$fileName=$_POST['fileName'];
		}
		$catName=$_POST['catName'];
		if($_POST['fromDda']!=''){
		   $fromdate=date('Y-m-d',strtotime($_POST['fromDda']));
		}else{
			$fromdate='';
		}
		if($_POST['tillDda']!=''){
		  $tillda= date('Y-m-d',strtotime($_POST['tillDda'])) ;
		}else{
			$tillda='';
		}
		// $groupid=$_POST['grpID'];
		// $userid=$_POST['userID'];
		// $grpID1=$_POST['grpID1'];
		// $grpID2=$_POST['grpID2'];
		// $color=$_POST['color'];
		// $mymanager1=$_POST['mymanager1'];
		// $mymanager2=$_POST['mymanager2'];
		// $mymanager3=$_POST['mymanager3'];
		// $mymanager4=$_POST['mymanager4'];
		// $mymanager5=$_POST['mymanager5'];
		// $mymanager6=$_POST['mymanager6'];
		// $mymanager7=$_POST['mymanager7'];
		// $mymanager8=$_POST['mymanager8'];
		function geteventcsv($usrName,$fileName,$catName,$fromdate,$tillda,$groupid,$userid,$grpID1,$grpID2,$color,$mymanager1,$mymanager2,$mymanager3,$mymanager4,$mymanager5,$mymanager6,$mymanager7,$mymanager8){
			$error=0;
			$frm=strtotime($fromdate);
			$till=strtotime($tillda);
			if($frm > $till){
				$error=1;
			}
			if($error==0){
				ob_start();
				ob_clean();
				if($catName=='*'){
						$catid='all';
				}
				else{
						$stH = stPrep("SELECT * FROM `mycal_categories` where name ='$catName'");
						stExec($stH);
						$row = $stH->fetch(PDO::FETCH_ASSOC);
						$catid=$row['ID'];
				}
				if($usrName =='*'){
						$userid='all';
				}else{
						$userstH = stPrep("SELECT * FROM `mycal_users` where name='$usrName'");
						stExec($userstH);
						$usersrow = $userstH->fetch(PDO::FETCH_ASSOC);
						 $userid=$usersrow['ID'];
						 $groupid=$usersrow['groupID'];
						 $mydepartment=$usersrow['mydepartment'];
				}
			
				$evt=[];
				/* if($userid =='all' && $catid!='all' && $fromdate!='' && $tillda!=''){
					 $query = stPrep("SELECT * FROM `mycal_events` where  catID='$catid' and sDate BETWEEN '$fromdate' AND '$tillda'");
				}
				elseif($catid=='all' && $userid!='all' && $fromdate!='' && $tillda!=''){
					$query = stPrep("SELECT * FROM `mycal_events` where userID='$userid' and sDate BETWEEN '$fromdate' AND '$tillda' ");
				}
				elseif($catid=='all' && $userid!='all' && $fromdate=='' && $tillda==''){
					
					$query = stPrep("SELECT * FROM `mycal_events` where userID='$userid'");
					
				}
				elseif($userid =='all' && $catid!='all' && $fromdate=='' && $tillda=='' ){
					
					 $query = stPrep("SELECT * FROM `mycal_events` where  catID='$catid'");
					 
				}
				elseif($userid =='all' && $catid=='all' && $fromdate!='' && $tillda!=''){
					
					 $query = stPrep("SELECT * FROM `mycal_events` where  sDate BETWEEN '$fromdate' AND '$tillda'");
					 
				}
				elseif($userid == 'all' && $catid == 'all' && $fromdate=='' && $tillda==''){
					
					 $query = stPrep("SELECT * FROM `mycal_events");
					 
				}
				elseif($userid!='' && $catid!='' && $fromdate=='' && $tillda==''){
					
					$query = stPrep("SELECT * FROM `mycal_events` where userID='$userid' and catID='$catid'");
				}
				else{
					$query = stPrep("SELECT * FROM `mycal_events` where userID='$userid' and catID='$catid' and sDate BETWEEN '$fromdate' AND '$tillda' ");
				
				} */
				$where='1=1';
			
				if($userid!='' && $userid!='all'){
					$where.=' and evt.userid="'.$userid.'"';
				}
				if($catid!='' && $catid!='all'){
					$where.='and evt.catid="'.$catid.'"';
				}
				if($fromdate!='' && $tillda!=''){
					$where.=' and evt.sDate BETWEEN "'.$fromdate.'" AND "'.$tillda.'"';
				}
				// if($groupid!=''){
					// $where.='and usr.groupID="'.$groupid.'"';
				// }
				// if($grpID1!=''){
					// $where.='and usr.mydepartment="'.$grpID1.'"';
				// }
				// if($grpID2!=''){
					// $where.='and usr.mycompany="'.$grpID2.'"';
				// }
				// if($color!=''){
					// $where.='and usr.color1="'.$color.'"';
					
				// }
				// if($mymanager1!=''){
					// $where.='and usr.`mymanager+1`="'.$mymanager1.'"';
				// }
				// if($mymanager2!=''){
					// $where.='and usr.`mymanager+2`="'.$mymanager2.'"';
				// }
				// if($mymanager3!=''){
					// $where.='and usr.`mymanager+3`="'.$mymanager3.'"';
				// }
				// if($mymanager4!=''){
					// $where.='and usr.`mymanager+4`="'.$mymanager4.'"';
				// }
				// if($mymanager5!=''){
					// $where.='and usr.`mymanager+5`="'.$mymanager5.'"';
				// }
				// if($mymanager6!=''){
					// $where.='and usr.`mymanager+6`="'.$mymanager6.'"';
				// }if($mymanager7!=''){
					// $where.='and usr.`mymanager+7`="'.$mymanager7.'"';
				// }	
				// if($mymanager8!=''){
					// $where.='and usr.`mymanager+8`="'.$mymanager8.'"';
				// }
			$query=stPrep("select evt.ID,evt.name,evt.firstname,evt.lastname,evt.email,evt.type,evt.private,evt.title,evt.venue,evt.text1,evt.text2,evt.text3,evt.attach,evt.catID,evt.userID,evt.editor,
evt.approved,evt.checked,evt.sDate,evt.xDates,evt.sTime,evt.rType,evt.rPeriod,evt.rMonth,evt.rUntil,evt.aDateTime,evt.mDateTime,evt.status,evt.userManager,
usr.groupID,usr.mymanager,usr.mydepartment,usr.mycompany,usr.color1,usr.`mymanager+1`,usr.`mymanager+2`,usr.`mymanager+3`,usr.`mymanager+4`,usr.`mymanager+5`,usr.`mymanager+6`,usr.`mymanager+7`,usr.`mymanager+8`	from mycal_events as evt LEFT join  mycal_users as usr on usr.ID=evt.userID where $where");
				$res=stExec($query);
				  $number_of_rows=$query->rowCount();
				if($number_of_rows >0){
					$filename = $fileName.'.csv';
					header("Content-Type: text/csv;");
					header('Content-Disposition: attachment; filename='.$filename.'');
					header('Pragma: no-cache');
					header("Content-Transfer-Encoding: UTF-8");
					header("Expires: 0");
					$outstream = fopen("php://output", "w");
					
					while ($events = $query->fetch(PDO::FETCH_ASSOC)) {
						$allusers=$events['name'];
						
						$evt['ID']=$events['ID'];
						$evt['type']=$events['type'];
						$evt['private']=$events['private'];
						$evt['title']=$events['title'];
						$evt['venue']=$events['venue'];
						$evt['text1']=$events['text1'];
						$evt['text2']=$events['text2'];
						$evt['text3']=$events['text3'];
						$evt['attach']=$events['attach'];
						$evt['catID']=$events['catID'];
						$evt['userID']=$events['userID'];
						$evt['username']=$events['name'];
						$evt['firstname']=$events['firstname'];
						$evt['lastname']=$events['lastname'];
						$evt['email']=$events['email'];
						$evt['editor']=$events['editor'];
						$evt['approved']=$events['approved'];
						$evt['checked']=$events['checked'];
						$evt['sDate']=$events['sDate'];
						$evt['xDates']=$events['xDates'];
						$evt['sTime']=$events['sTime'];
						$evt['rType']=$events['rType'];
						$evt['rPeriod']=$events['rPeriod'];
						$evt['rMonth']=$events['rMonth'];
						$evt['rUntill']=$events['rUntill'];
						$evt['aDateTime']=$events['aDateTime'];
						$evt['mDateTime']=$events['mDateTime'];
						$evt['status']=$events['status'];
						$evt['userManager']=$events['userManager'];
						$evt['GroupID']=$events['groupID'];
						$evt['Stylish Manager/Parent']=$events['mymanager'];
						$evt['Mydepartment']=$events['mydepartment'];
						$evt['Mydepartmentname']=$allusers;
						$evt['MyCompany']=$events['mycompany'];
						$evt['Background-colour']=$events['color1'];
						$evt['mymanager+1']=$events['mymanager+1'];
						$evt['mymanager+2']=$events['mymanager+2'];
						$evt['mymanager+3']=$events['mymanager+3'];
						$evt['mymanager+4']=$events['mymanager+4'];
						$evt['mymanager+5']=$events['mymanager+5'];
						$evt['mymanager+6']=$events['mymanager+6'];
						$evt['mymanager+7']=$events['mymanager+7'];
						$evt['mymanager+8']=$events['mymanager+8'];
						$evtList[$events['ID']][] = $evt;	
					 } 
					fputs($outstream, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
					foreach($evt as $key11 =>$val){
						 $heading[]=$key11; 
					}
					fputcsv($outstream, $heading);
					foreach($evtList as $key1 =>$val1){
						foreach($val1 as $key=>$val){
							foreach($val as $key12=>$data){
								$values[]=$data;
							}
								fputcsv($outstream, $values);
								$values = [];
								
						}	
						
					}
				
				}
				else{
					header('location:http://visiorx.info/calendar?rec=0');
				
				}
			}
			else{
				header('location:http://visiorx.info/calendar?rec=-1');
			}
			
		}
		
      $data=geteventcsv($usrName,$fileName,$catName,$fromdate,$tillda,$groupid,$userid,$grpID1,$grpID2,$color,$mymanager1,$mymanager2,$mymanager3,$mymanager4,$mymanager5,$mymanager6,$mymanager7,$mymanager8);
		
		
		exit();
	}

	////End Code Csv///
	if (count($evtList) == 0) { return $ax['iex_no_events_found']; }

	$icsBody = '';
	$from = array(',',';','<br>');
	$to = array('\,','\;','\n');
	$eidDone = array(); //events processed
	foreach ($evtList as $evtListDate) {
		foreach ($evtListDate as $evt) {
			if (!in_array($evt['eid'], $eidDone)) { //event not yet processed
				$vDescription = str_replace($from,$to,htmlspecialchars_decode(makeE($evt,$set['evtTemplGen'],'br','\n','345'),ENT_QUOTES));
				$vDescription = chunk_split_unicode($vDescription,72,"\r\n "); //fold to 72 chars line length
				//compile DTSTART and DTEND values
				$dateS = str_replace('-','',$evt['sda']);
				$dateE = ($evt['eda'][0] != '9') ? str_replace('-','',$evt['eda']) : $dateS;
				$timeS = str_replace(':','',$evt['sti']);
				$timeE = str_replace(':','',$evt['eti']);
				if ($timeS == '' and $timeE == '') { //all day
					$allDay = true;
					$dateE = date('Ymd',mktime (12,0,0,substr($dateE,4,2),substr($dateE,6,2)+1,substr($dateE,0,4))); //+1 day
				} else {
					$allDay = false;
					$dateS .= 'T'.$timeS.'00';
					$dateE .= 'T'.(($timeE) ? $timeE.'00' : $timeS.'01');
				}

				//compile RRULE property
				$rrule = '';
				if ($evt['r_t'] == 1) { //every 1|2|3|4 d|w|m|y
					$rrule .= "FREQ=";
					switch ($evt['r_p']) {
						case 1: $rrule .= 'DAILY'; break;
						case 2: $rrule .= 'WEEKLY'; break;
						case 3: $rrule .= 'MONTHLY'; break;
						case 4: $rrule .= 'YEARLY';
					}
					$rrule .= ";INTERVAL=".$evt['r_i'];
				}
				if ($evt['r_t'] == 2) { //every 1|2|3|4|5 m|t|w|t|f|s|s of the month
					$rrule .= $evt['r_m'] ? "FREQ=YEARLY" : "FREQ=MONTHLY";
					$rrule .= ";BYDAY=".(($evt['r_i'] != 5) ? $evt['r_i'] : '-1');
					switch ($evt['r_p']) {
						case 1: $rrule .= 'MO'; break;
						case 2: $rrule .= 'TU'; break;
						case 3: $rrule .= 'WE'; break;
						case 4: $rrule .= 'TH'; break;
						case 5: $rrule .= 'FR'; break;
						case 6: $rrule .= 'SA'; break;
						case 7: $rrule .= 'SU';
					}
					if ($evt['r_m']) {
						$rrule .= ";BYMONTH=".$evt['r_m'];
					}
				}
				if ($evt['r_u'][0] != '9') {
					$rrule .= ";UNTIL=".str_replace('-','',$evt['r_u']).'T235900';
				}
				$tStamp = mktime(substr($timeS,0,2),substr($timeS,2,2),0,substr($dateS,4,2),substr($dateS,6,2),substr($dateS,0,4));
				$icsBody .= "BEGIN:VEVENT\r\n";
				$icsBody .= "DTSTAMP:".gmdate('Ymd\THis\Z')."\r\n";
				if ($evt['adt']) {
					$icsBody .= "CREATED:".gmdate('Ymd\THis\Z', mktime(substr($evt['adt'],11,2),substr($evt['adt'],14,2),0,substr($evt['adt'],5,2),substr($evt['adt'],8,2)+1,substr($evt['adt'],0,4)))."\r\n";
				}
				if ($evt['mdt']) {
					$icsBody .= "LAST-MODIFIED:".gmdate('Ymd\THis\Z', mktime(substr($evt['mdt'],11,2),substr($evt['mdt'],14,2),0,substr($evt['mdt'],5,2),substr($evt['mdt'],8,2)+1,substr($evt['mdt'],0,4)))."\r\n";
				}
				$icsBody .= "UID:".gmdate("Ymd\THis\Z", $tStamp).trim(substr(iconv('UTF-8','ASCII//TRANSLIT//IGNORE',$evt['tit']),0,4))."-LuxCal@{$set['calendarUrl']}\r\n";
				$icsBody .= "SUMMARY:".str_replace(",","\,",htmlspecialchars_decode($evt['tit'],ENT_QUOTES))."\r\n";
				if ($vDescription) { $icsBody .= "DESCRIPTION:{$vDescription}\r\n"; }
				$icsBody .= "CATEGORIES:".str_replace(",","\,",$evt['cnm'])."\r\n";
				if ($evt['pri']) { $icsBody .= "CLASS:PRIVATE\r\n"; }
				if ($evt['ven']) { $icsBody .= "LOCATION:".str_replace(",","\,",htmlspecialchars_decode($evt['ven'],ENT_QUOTES))."\r\n"; }
				if ($rrule) { $icsBody .= "RRULE:{$rrule}\r\n"; }
				$icsBody .= "DTSTART;".($allDay ? "VALUE=DATE" : "TZID=".date_default_timezone_get()).":{$dateS}\r\n";
				$icsBody .= "DTEND;".($allDay ? "VALUE=DATE" : "TZID=".date_default_timezone_get()).":{$dateE}\r\n"; //+1 ?
				$icsBody .= "END:VEVENT\r\n";
				$eidDone[] = $evt['eid']; //mark as processed
			}
		}
	}
	$icsTail = "END:VCALENDAR";
	//save to iCal file
	$icalfName = $fileName ? $fileName : $set['calendarTitle'];
	$icalfName = translit($icalfName,true);
	if (file_put_contents("./files/{$icalfName}.ics", $icsHead.$icsBody.$icsTail, LOCK_EX) !== false) {
		$result = $ax['iex_file_created'];
	} else {
		$result = $ax['iex_write error'];
	}
	return $result;
}

//control logic
$msg = ''; //init
if ($privs == 9) { //admin
	if (isset($_POST['create'])) {
		$msg = makeFile();
	}
	echo "<br><p class='error'>{$msg}</p>
		<div class='scrollBoxAd'>
		<aside class='aside'>{$ax['xpl_event_side']}</aside>
		<div class='centerBox'>\n";
	selectEvents();
	echo "</div>\n</div>\n";
} else {
	echo "<p class='error'>{$ax['no_way']}</p>\n";
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$("#eventdata").submit(function( event ) {
		
var fromdate=$("#fromDda").val();
var tilldate=$("#tillDda").val();
if(fromdate!='' && tilldate ==''){
	alert('Either Fill both Occuring date or leave as empty ');
	 event.preventDefault();
}
if(tilldate!='' &&  fromdate ==''){
		alert('Either Fill both Occuring date or leave as empty');
		 event.preventDefault();
}


	});
});
</script>