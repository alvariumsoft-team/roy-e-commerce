<?php
define('MailChimp_PATHC', base_path().'/vendor/mailchimp');
require_once FACEBOOK_LOGIN_PATHS.'/autoload.php';
require_once MailChimp_PATHC.'/MailChimp.php';
use \DrewM\MailChimp\MailChimp;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
class CheckoutController extends BaseController {
    
	public function __construct(){
		// Call Google API
	
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to CodexWorld.com');
        $gClient->setClientId(GOOGLE_CLIENT_ID);
        $gClient->setClientSecret(GOOGLE_CLIENT_SECRET);
        $gClient->setRedirectUri(GOOGLE_REDIRECT_URL);
        //$gClient->setScopes(Google_Service_Gmail::GMAIL_READONLY);
        $gClient->addScope("https://www.googleapis.com/auth/userinfo.profile");
        $gClient->addScope("https://www.googleapis.com/auth/userinfo.email");
        $gClient->addScope("https://www.googleapis.com/auth/plus.login");
        //$gClient->setAuthConfig('credentials.json');
        
        $google_oauthV2 = new Google_Service_Oauth2($gClient);
        $authUrl = $gClient->createAuthUrl();
		// Call Facebook API
		$fb = new Facebook(array(
			'app_id' => FB_APP_ID,
			'app_secret' => FB_APP_SECRET,
			'default_graph_version' => 'v3.2',
		));

		// Get redirect login helper
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // Optional permissions
		$loginURL = $helper->getLoginUrl(FB_REDIRECT_URL, $permissions);
        View::share('authUrl', $authUrl);
		View::share('fbauthUrl', $loginURL);
	} 
	
	public function getLogin(){
		if(Auth::check()){
			return Redirect::to('/checkout/checkout');
		}
		$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;
			
				 
			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		return View::make('webshop/login')->with('catdata',$_cat);
	}
	
	public function postLogin(){
		$session = Session::getId();
		$username = Input::get('username');
		$password = Input::get('password');
        if((isset($username) && $username!='') && (isset($password) && $password!='')){ 
			if(Auth::attempt(['username' => $username, 'password' => $password]) || Auth::attempt(['email' => $username, 'password' => $password])) {
				if(Auth::user()->status == 1){
					$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
					$checkout = Checkout::where('guest_session', '=', $session)->update(['user_id'=>Auth::user()->id]);
					return Redirect::to('/checkout/checkout');
				}
				else{
					Auth::logout();
					return Redirect::to('/checkout/login');
				}
			}
			else{
				Session::flash('message-error','The username and password combination is not correct.');
				return Redirect::to('/checkout/login');
			}
		}
	    else{
			Session::flash('message-error','The username/email and password fields are required.');
			return Redirect::to('/checkout/login');
		}
	}
    
 
	public function getLogout()
	{
		Auth::logout();
		return Redirect::back();//to('/checkout/login');
	}
	
	public function getRegister(){
		
			$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;
			
				 
			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		return View::make('webshop.register')->with('catdata',$_cat);
	}


	public function postRegister(){
		//dd(Input::all());
		$rules = array(
            'individual' => 'required',
            'email' => 'required|email|unique:users',
            'username' => 'required|max:24|unique:users',
            'password' => 'required',
            'policy' => 'required',
        );

        $data = [
            'individual' => trim(Input::get('individual')),
            'email' => trim(Input::get('email')),
            'username' => trim(Input::get('name')),
            'password' => trim(Input::get('password')),
            'policy' => trim(Input::get('policy')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    //return  response()->json(['errors'=>$validator->errors()]);
	    }else
	    {
			$session = Session::getId();
			$individual = Input::get('individual');
			$email = Input::get('email');
			$name = Input::get('name');
			$password = Input::get('password');
			$privacypolicy=Input::get('policy');
			$session=Input::get('session');
			$receiveeml=Input::get('receiveeml');

				$user = new WebUser;
				$user->username = $name;
				$user->email = $email;
				$user->password =  Hash::make($password);
				$user->privacy_policy=$privacypolicy;
				if(isset($receiveeml)){
				$user->receieveemail=$receiveeml;
				}else{
				$user->receieveemail='';
				}
				$user->endsession=$session;
				if(isset($session)){
				$user->endsession=$session;
				}else{
				$user->endsession='';
				}
				$user->via=1;
				$user->type=0;
				$user->save();	

				if(isset($receiveeml)){

				   $this->sendmail($email, $user->id);
				}	
				
							
				//$products = Productsessions::where('guest_session', '=', $session)->update(['user_id'=>$user->id]);
				//$checkout = Checkout::where('guest_session', '=', $session)->update(['user_id'=>$user->id]);
				//return Redirect::back()->with('message-success', 'Registration Successfully');
				$resultSet['Success'] = 'Registration Successfully';
               // $resultSet['Redirect'] = Redirect::to('/checkout/login');
                echo json_encode($resultSet);
	     }				
				
	}
	

	

    public function postCregister(){

		$rules = array(
            'company' => 'required',
            'email' => 'required|email|unique:users',
            'username' => 'required|max:24|unique:users',
            'surname' => 'required',
            'password' => 'required',
            'policy' => 'required',
            'companyname' => 'required',
            'taxidcode' => 'required',
        );

        $data = [
            'company' => trim(Input::get('company')),
            'email' => trim(Input::get('email')),
            'username' => trim(Input::get('name')),
            'surname' => trim(Input::get('surname')),
            'password' => trim(Input::get('password')),
            'policy' => trim(Input::get('policy')),
            'companyname' => trim(Input::get('companyname')),
            'taxidcode' => trim(Input::get('taxidcode')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    //return  response()->json(['errors'=>$validator->errors()]);
	    }else
	    {
           $session = Session::getId();
			$company = Input::get('company');
	        $cname=Input::get('companyname');
	        $taxidcode=Input::get('taxidcode');
	        $firstname=Input::get('name');
	        $lastname=Input::get('surname');
			$email = Input::get('email');
			$password = Input::get('password');
			$privacypolicy=Input::get('policy');
			$receiveeml=Input::get('receiveeml');
			    $user=new WebUser;
				$user->username=$firstname;
				$user->lastname=$lastname;
				$user->email=$email;
				$user->password=Hash::make($password);
				$user->privacy_policy=$privacypolicy;					
				if(isset($receiveeml)){
				$user->receieveemail=$receiveeml;

				}else{
						$user->receieveemail='';
				}
				$user->endsession=$session;
				if(isset($session)){
				$user->endsession=$session;
				}else{
						$user->endsession='';
				}
				$user->type=1;
				$user->via=1;
				$user->save();
				$userid = $user->id;

				if(isset($receiveeml)){

				   $this->sendmail($email, $userid);
				}
					
					$companydata=new Companydata;
					$companydata->companyname=$cname;
					$companydata->taxidcode=$taxidcode;
					$companydata->userid=$userid;
					$companydata->save();
			    $resultSet['Success'] = 'Registration Successfully';
			   // $resultSet['Redirect'] = Redirect::to('/checkout/login');
                echo json_encode($resultSet);
				
		 }
	}

    private function sendmail($email, $user_id){


 	        $newsletter = Newsletter::where('slug', '=', 'user-subscription')->get();
		
				if(isset($newsletter[0])){
					$newsletter = $newsletter[0];
					$data = new Newsletterlist;
					$data->newsletter_id = $newsletter->id;
					$data->user_id = $user_id;
					$data->email = $email;
					$data->save();
				}
				

			    $MailChimp = new MailChimp('1c6c8a83195c12fe9f42831c9dbb2b78-us20');
				$result = $MailChimp->get('lists');
				//echo "<pre>";
				//print_r($result);

				$list_id = '63cfb27cff';

				$result = $MailChimp->post("lists/$list_id/members", [
								'email_address' => $email,
								'status'        => 'subscribed',
							]);


     }	

	public function getGuestcheckout(){
		//echo "string"; exit();
		

		if(Auth::check()){
			return Redirect::to('/checkout/checkout');
		}
		$checkout = new Checkout;
		
		if(Auth::check()){
			$checkout->user_id = Auth::user()->id;
			$checkout->save();
			return Redirect::to('/checkout/checkout');
		}
		else{
			$checkout->guest_session = Session::getId();
			$checkout->save();
			return Redirect::to('/checkout/checkout');
		}
		return Redirect::to('/checkout/checkout');
	}
	
	public function getCheckout(){
		$categories = Category::where('parentid', '=', 0)->get();
		$europe_county = array('austria','belgium','bulgaria','croatia','cyprus','czechia','denmark','estonia','finland','france','germany','greece','hungary','ireland','italy','latvia','lithuania','luxembourg','malta','netherlands','poland','portugal','romania','slovakia','slovenia','spain','sweden','united kingdom');
		if(Auth::check()){
			$webusers = UserWeb::select('id','country','type')->where('id', '=', Auth::user()->id)->first();
			if(($webusers->type)=='1'){
				if($webusers->country !=""){
					if((strtolower($webusers->country))=='netherlands'){
						$tax=Tax_Neth_Co;
					}
					else if(in_array(strtolower($webusers->country), $europe_county)){
						$tax=Tax_Eu_Co;
					}
					else{
						$tax=Tax_Outside_Eu;	
					}
				}
				else{
					$countr=$this->getUsercountry();
					if($countr==strtolower('Netherlands')){
						$tax=Tax_Neth_Co;
					}
					else if(in_array(strtolower($countr), $europe_county)){
						$tax=Tax_Eu_Co;
					}
					else{
						$tax=Tax_Outside_Eu;
					}	
				}
			}
			else{
				if($webusers->country !=""){
					$cont=strtolower($webusers->country);
					if(($webusers->country)=='netherlands'){
						$tax=Tax_Neth_In;
					}
					else if(in_array($cont,$europe_county)){
						$tax=Tax_Eu_In;
					}
					else{
						$tax=Tax_Outside_Eu;
						
					}
				}
				else{
					$countr=$this->getUsercountry();
					if($countr==strtolower('Netherlands')){
						$tax=Tax_Neth_In;
					}
					else if(in_array(strtolower($countr), $europe_county)){
						$tax=Tax_Eu_In;
					}
					else{
						$tax=Tax_Outside_Eu;
					}	
				}
				
			}
			
			$countries = Shipcountriesnames::orderBy('countries_name', 'asc')->get();
			$allcountrues=array();
			foreach($countries as $k=>$ct){
				$allcountrues[$ct->countries_name]=$ct->countries_name;
			}
			$products = array();
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
			if(!isset($products[0])){
				return Redirect::to('/webshop');
			}
			$delivery_skus=array();
			foreach($products as $k=>$s){
				$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
				$products[$k]->products=$prod_detail;
				$delivery_skus[$k]['sku']=$prod_detail->sku;
				$delivery_skus[$k]['quantity']=$s->quantity;
				if($s->product_variation_id !=0){
					$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
					$products[$k]->prvars=$prvars ;
					$delivery_skus[$k]['sku']=$prvars->sku;
				}
			}
			$auth_user_id = Auth::user()->id;
			$checkout = Checkout::where('user_id',$auth_user_id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			if(!isset($checkout[0])){
				$checkout = new Checkout;
				$checkout->user_id = Auth::user()->id;
				$checkout->save();
				$checkout = Checkout::where('user_id',$auth_user_id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			if(isset($checkout[0])){
				$checkout = $checkout[0];
			}
			else{
				return Redirect::to('/checkout/login');
			}
			$shipping = UserWeb::where('id', '=', Auth::user()->id)->first();
			$shippingaddresses = Shippingaddress::where('user_id', '=', Auth::user()->id)->get();
			$jsonArrayResponse=array();
			if(isset($shippingaddresses[0])){
				$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $shippingaddresses[0]->country)->first();
				if(isset($country_iso->countries_id)){
					$end_point='rest/shipping/orders.json';
					if(BIGBUG_MODE=='PRODUCTION'){
						$curl_url=BIGBUY_END_POINT."/".$end_point;
						$api_key=BIGBUY_API_KEY;
					}
					else{
						$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
						$api_key=SANDOX_BIGBUY_API_KEY;
					}
					$cURLConnection = curl_init();
					
					curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
					curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
						'Authorization: Bearer '.$api_key,'Content-Type: application/json'
					));
					$postRequest = array();
					
					$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
					$postRequest['order']['delivery']['postcode']=$shippingaddresses[0]->postalcode;
					foreach($delivery_skus as $m => $sku){
						$postRequest['order']['products'][$m]['reference']=$sku['sku'];
						$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
					}
					curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
					curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($cURLConnection);
					curl_close($cURLConnection);
					$jsonArrayResponse = json_decode($response);
					if(isset($jsonArrayResponse->shippingOptions[0])){
						$shp_options=array();
						$fshp_options=array();
						foreach($jsonArrayResponse->shippingOptions as $shp){
							$nm=$shp->shippingService->name;
							if(isset($shp_options[$nm])){
								$ct=$shp->cost;
								$cts=$shp_options[$nm]->cost;
								if($ct < $cts){
									$shp_options[$nm]=$shp;
								}
							}
							else{
								$shp_options[$nm]=$shp;
							}
						}
						$i=0;
						foreach($shp_options as $shpop){
							$fshp_options[$i]=$shpop;
							$i=$i+1;
						}
						$jsonArrayResponse->shippingOptions=$fshp_options;
					}
				}
			}
			$chkout = Checkout::where('user_id',$auth_user_id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->first();
			$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
			$chkout->save();
			return View::make('webshop/checkout', array('page'=>'basket', 'products'=>$products, 'categories'=>$categories, 'checkout'=>$checkout,'shipping'=>$shipping,'shippingaddresses'=>$shippingaddresses,'allcountrues'=>$allcountrues,'billing_methods'=>$jsonArrayResponse,'tax'=>$tax));
		}
		else{
			$countries = Shipcountriesnames::orderBy('countries_name', 'asc')->get();
			$allcountrues=array();
			foreach($countries as $k=>$ct){
				$allcountrues[$ct->countries_name]=$ct->countries_name;
			}
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
			if(!isset($products[0])){
				return Redirect::to('/webshop');
			}
			$delivery_skus=array();
			foreach($products as $k=>$s){
				$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
				$products[$k]->products=$prod_detail;
				$delivery_skus[$k]['sku']=$prod_detail->sku;
				$delivery_skus[$k]['quantity']=$s->quantity;
				if($s->product_variation_id !=0){
					$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
					$products[$k]->prvars=$prvars ;
					$delivery_skus[$k]['sku']=$prvars->sku;
				}
			}
			$checkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			if(!isset($checkout[0])){
				$checkout = new Checkout;
				$checkout->guest_session = Session::getId();
				$checkout->save();
				$checkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			if(isset($checkout[0])){
				$checkout = $checkout[0];
			}
			else{
				return Redirect::to('/checkout/login');
			}
			$billing_address = GuestUser::where('guest_session', '=', Session::getId())->first();
			$guestuser=$billing_address;
			if(isset($guestuser->id)){
				if(($guestuser->type)=='1'){
					if($guestuser->country !=""){
						$cont=strtolower(trim($guestuser->country));
						if($cont=='netherlands'){
							$tax=Tax_Neth_Co;
						}
						else if(in_array($cont, $europe_county)){
							$tax=Tax_Eu_Co;
						}
						else{
							$tax=Tax_Outside_Eu;	
						}
					}
					else{
						$countr=$this->getUsercountry();
						if($countr=='netherlands'){
							$tax=Tax_Neth_Co;
						}
						else if(in_array($countr, $europe_county)){
							$tax=Tax_Eu_Co;
						}
						else{
							$tax=Tax_Outside_Eu;
						}	
					}
				}
				else{
					if($guestuser->country !=""){
						$cont=strtolower(trim($guestuser->country));
						if($cont=='netherlands'){
							$tax=Tax_Neth_In;
						}
						else if(in_array($cont,$europe_county)){
							$tax=Tax_Eu_In;
						}
						else{
							$tax=Tax_Outside_Eu;
							
						}
					}
					else{
						$countr=$this->getUsercountry();
						if($countr=='netherlands'){
							$tax=Tax_Neth_In;
						}
						else if(in_array($countr, $europe_county)){
							$tax=Tax_Eu_In;
						}
						else{
							$tax=Tax_Outside_Eu;
						}	
					}
				
				}
			}
			else{
				$countr=$this->getUsercountry();
				if($countr=='netherlands'){
					$tax=Tax_Neth_In;
				}
				else if(in_array($countr, $europe_county)){
					$tax=Tax_Eu_In;
				}
				else{
					$tax=Tax_Outside_Eu;
				}
			}
			$shippingaddresses = Shippingaddress::where('guest_session', '=', Session::getId())->get();
			$jsonArrayResponse=array();
			if(isset($shippingaddresses[0])){
				$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $shippingaddresses[0]->country)->first();
				if(isset($country_iso->countries_id)){
					$end_point='rest/shipping/orders.json';
					if(BIGBUG_MODE=='PRODUCTION'){
						$curl_url=BIGBUY_END_POINT."/".$end_point;
						$api_key=BIGBUY_API_KEY;
					}
					else{
						$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
						$api_key=SANDOX_BIGBUY_API_KEY;
					}
					$cURLConnection = curl_init();
					
					curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
					curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
						'Authorization: Bearer '.$api_key,'Content-Type: application/json'
					));
					$postRequest = array();
					$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
					$postRequest['order']['delivery']['postcode']=$shippingaddresses[0]->postalcode;
					foreach($delivery_skus as $m => $sku){
						$postRequest['order']['products'][$m]['reference']=$sku['sku'];
						$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
					}
					curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
					curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($cURLConnection);
					curl_close($cURLConnection);
					$jsonArrayResponse = json_decode($response);
					if(isset($jsonArrayResponse->shippingOptions[0])){
						$shp_options=array();
						$fshp_options=array();
						foreach($jsonArrayResponse->shippingOptions as $shp){
							$nm=$shp->shippingService->name;
							if(isset($shp_options[$nm])){
								$ct=$shp->cost;
								$cts=$shp_options[$nm]->cost;
								if($ct < $cts){
									$shp_options[$nm]=$shp;
								}
							}
							else{
								$shp_options[$nm]=$shp;
							}
						}
						$i=0;
						foreach($shp_options as $shpop){
							$fshp_options[$i]=$shpop;
							$i=$i+1;
						}
						$jsonArrayResponse->shippingOptions=$fshp_options;
					}
					
				}
			}
			$chkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->first();
			$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
			$chkout->save();
			return View::make('webshop/guest_checkout', array('page'=>'basket', 'products'=>$products, 'categories'=>$categories, 'checkout'=>$checkout,'allcountrues'=>$allcountrues,'shippingaddresses'=>$shippingaddresses,'billing_address'=>$billing_address,'billing_methods'=>$jsonArrayResponse,'tax'=>$tax));
		}
    }



	
	public function postCheckout(){
		$europe_county = array('austria','belgium','bulgaria','croatia','cyprus','czechia','denmark','estonia','finland','france','germany','greece','hungary','ireland','italy','latvia','lithuania','luxembourg','malta','netherlands','poland','portugal','romania','slovakia','slovenia','spain','sweden','united kingdom');
		if(Auth::check()){
			$checkout_id=trim(Input::get('address_id'));
			$ship_add_id=trim(Input::get('ship_add_id'));
			$is_billing = Input::get('use_billing_address');
			$delivery_type=trim(Input::get('delivery_type'));
			$rules = array(
				'address_id'=> 'required',
				'delivery_type'=> 'required',
				'ship_add_id'=>'required'
			);
			$data = [
				'address_id' => $checkout_id,
				'delivery_type' => $delivery_type,
				'ship_add_id' => $ship_add_id,
			];
			$validator = Validator::make($data,$rules);
			if($validator->fails()){
				return Redirect::to('/checkout/checkout')->with("message-failure", "Please provide all the required information!")->withInput();
			}
			else{
				$checkout = Checkout::where('id', '=', $checkout_id)->where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
				if(isset($checkout[0])){
					$shipping_options=json_decode($checkout[0]->all_shipping_options,TRUE);
					if(!isset($is_billing)){
						$ship_address = Shippingaddress::where('id', '=', $ship_add_id)->where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->limit(1)->get();
						if(!isset($ship_address[0])){
							return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
						}
					}
					$products = array();
					$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->get();
					
					$items = 0;
					$total = 0;
					foreach($products as $session){
						$items = $items + $session->quantity;
						$total = $total + ($session->quantity * $session->price);
					}
					$shipping_method="";
					$webusers = UserWeb::select('id','country','type')->where('id', '=', Auth::user()->id)->first();
					if(($webusers->type)=='1'){
						if($webusers->country !=""){

							if((strtolower($webusers->country))=='netherlands'){
								$taxPercent=Tax_Neth_Co;
							}
							else if(in_array(strtolower($webusers->country), $europe_county)){
								$taxPercent=Tax_Eu_Co;
							}
							else{
								$taxPercent=Tax_Outside_Eu;	
							}
						}
						else{
							$countr=$this->getUsercountry();
							if($countr==strtolower('Netherlands')){
								$taxPercent=Tax_Neth_Co;
							}
							else if(in_array(strtolower($countr), $europe_county)){
								$taxPercent=Tax_Eu_Co;
							}
							else{
								$taxPercent=Tax_Outside_Eu;
							}	
						}
					}
					else{
						if($webusers->country !=""){
							$cont=strtolower($webusers->country);
							if(($webusers->country)=='netherlands'){
								$taxPercent=Tax_Neth_In;
							}
							else if(in_array($cont,$europe_county)){
								$taxPercent=Tax_Eu_In;
							}
							else{
								$taxPercent=Tax_Outside_Eu;
								
							}
						}
						else{
							$countr=$this->getUsercountry();
							if($countr==strtolower('Netherlands')){
								$taxPercent=Tax_Neth_In;
							}
							else if(in_array(strtolower($countr), $europe_county)){
								$taxPercent=Tax_Eu_In;
							}
							else{
								$taxPercent=Tax_Outside_Eu;
							}	
						}
						
					}
							
					$ok_shipping=0;
					foreach($shipping_options['shippingOptions'] as $t =>$opt){
						if($opt['shippingService']['id']==$delivery_type){
							$shipping_cost=$opt['cost'];
							$shipping_method=$opt['shippingService']['name'];
							$shipping_delay = $opt['shippingService']['delay'];
							$ok_shipping=1;
							break;
						}
					}
					if($ok_shipping==0){
						return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
					}
					$totalTax = number_format(((($total + $shipping_cost) * $taxPercent)/100),2, '.', '');
					$total_cost = $total + $shipping_cost;
					$finalTotal= number_format(($total_cost + $totalTax),2, '.', '');
					$order_number = '#'.time();
					$userdetails = UserWeb::where('id', '=', Auth::user()->id)->first();
					$checkout = Checkout::find($checkout_id);
					$checkout->name = $userdetails->firstname;
					$checkout->surname = $userdetails->lastname;
					$checkout->email = $userdetails->billing_email;
					$checkout->nif = $userdetails->nr;
					$checkout->country = $userdetails->country;
					$checkout->telephone = $userdetails->phone;
					$checkout->order_number = $order_number;
					$checkout->billing_address = $userdetails->address;
					$checkout->billing_city = $userdetails->city;
					$checkout->billing_postalcode = $userdetails->postcode;
					$checkout->billing_floor = $userdetails->floor;
					$checkout->shipping_method = $shipping_method;
					$checkout->shipping_delay = $shipping_delay;
					$checkout->shipping_cost = $shipping_cost;
					$checkout->taxpercent = number_format($taxPercent,2, '.', '');
					$checkout->total = number_format($total,2, '.', '');
					$checkout->tax = $totalTax ;
					$checkout->total_cost = $finalTotal;
					$checkout->comp_name = $userdetails->comp_name;
					$checkout->vat = $userdetails->vat;
					$checkout->chamberaddress = $userdetails->chamber_commerce;
					$checkout->payment_method = 'Mollie';
					if(!isset($is_billing)){
						$shiiping_details=$ship_address[0];
						$checkout->shipping_name = $shiiping_details->name;
						$checkout->shipping_surname = $shiiping_details->surname;
						$checkout->shipping_nr = $shiiping_details->nr;
						$checkout->shipping_floor = $shiiping_details->floor_door;
						$checkout->shipping_address = $shiiping_details->address;
						$checkout->shipping_city = $shiiping_details->city;
						$checkout->shipping_country = $shiiping_details->country;
						$checkout->shipping_postalcode = $shiiping_details->postalcode;
						$checkout->shipping_phone = $shiiping_details->telephone;
						$checkout->shipping_alias = $shiiping_details->alias;
					}
					else{
						$checkout->shipping_name = $userdetails->firstname;
						$checkout->shipping_surname = $userdetails->lastname;
						$checkout->shipping_nr = $userdetails->nr;
						$checkout->shipping_floor = $userdetails->floor;
						$checkout->shipping_address = $userdetails->address;
						$checkout->shipping_city = $userdetails->city;
						$checkout->shipping_postalcode = $userdetails->postcode;
						$checkout->shipping_phone = $userdetails->phone;
						$checkout->shipping_country = $userdetails->country;
						$checkout->shipping_alias = 'Same as Billing';
					}
					$checkout->save();
					$affectedRows = CheckoutProSessions::where('user_id', '=',  Auth::user()->id)->where('checkout_id', '=',  $checkout_id)->delete();
					foreach($products as $session){
						$checkoutproduct=new CheckoutProSessions;
						$checkoutproduct->user_id = $session->user_id;
						$checkoutproduct->guest_session = $session->guest_session;
						$checkoutproduct->product_id = $session->product_id;
						$checkoutproduct->name = $session->name;
						$checkoutproduct->price = number_format($session->price,2, '.', '');
						$checkoutproduct->img = $session->img;
						$checkoutproduct->product_variation_id = $session->product_variation_id;
						$checkoutproduct->quantity = $session->quantity;
						$checkoutproduct->attributes = $session->attributes;
						$checkoutproduct->checkout_id = $checkout_id;
						$prod=BigProduct::select('taxrate','sku')->where('id', '=', $session->product_id)->where('active', '=', 1)->first();
						if($session->product_variation_id !=0){
							$prvars=ProductVariations::select('sku')->where('id', '=', $session->product_variation_id)->where('product_id', '=', $session->product_id)->first();
							$checkoutproduct->sku = $prvars->sku;
						}
						else{
							$checkoutproduct->sku = $prod->sku;	
						}
						$checkoutproduct->taxrate = number_format($taxPercent,2, '.', '');
						$checkoutproduct->save();
					}
					return Redirect::to('/checkout/getConfirm/'.$checkout_id);
				}
				else{
					return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
				}
			}
		}
		else{
			$session_id=Session::getId();
			$checkout_id=trim(Input::get('address_id'));
			$ship_add_id=trim(Input::get('ship_add_id'));
			$is_billing = Input::get('use_billing_address');
			$delivery_type=trim(Input::get('delivery_type'));
			if(!isset($is_billing)){
				$rules = array(
					'address_id'=> 'required',
					'delivery_type'=> 'required',
					'ship_add_id'=>'required'
				);
				$data = [
					'address_id' => $checkout_id,
					'delivery_type' => $delivery_type,
					'ship_add_id' => $ship_add_id,
				];
			}
			else{
				$rules = array(
					'address_id'=> 'required',
					'delivery_type'=> 'required'
				);
				$data = [
					'address_id' => $checkout_id,
					'delivery_type' => $delivery_type
				];
			}
			
			$validator = Validator::make($data,$rules);
			if($validator->fails()){
				return Redirect::to('/checkout/checkout')->with("message-failure", "Please provide all the required information!")->withInput();
			}
			else{
				$checkout = Checkout::where('id', '=', $checkout_id)->where('guest_session', '=', $session_id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
				if(isset($checkout[0])){
					$shipping_options=json_decode($checkout[0]->all_shipping_options,TRUE);
					if(!isset($is_billing)){
						$ship_address = Shippingaddress::where('id', '=', $ship_add_id)->where('guest_session', '=', $session_id)->orderBy('id', 'desc')->limit(1)->get();
						if(!isset($ship_address[0])){
							return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
						}
					}
					$products = array();
					$products = Productsessions::where('guest_session', '=', $session_id)->where('checkout_id', '=', 0)->get();
					
					$items = 0;
					$total = 0;
					foreach($products as $session){
						$items = $items + $session->quantity;
						$total = $total + ($session->quantity * $session->price);
					}
					$shipping_method="";
					$guestuser = GuestUser::where('guest_session', '=', Session::getId())->first();
					if(isset($guestuser->id)){
						if(($guestuser->type)=='1'){
							if($guestuser->country !=""){
								$cont=strtolower(trim($guestuser->country));
								if($cont=='netherlands'){
									$taxPercent=Tax_Neth_Co;
								}
								else if(in_array($cont, $europe_county)){
									$taxPercent=Tax_Eu_Co;
								}
								else{
									$taxPercent=Tax_Outside_Eu;	
								}
							}
							else{
								$countr=$this->getUsercountry();
								if($countr=='netherlands'){
									$taxPercent=Tax_Neth_Co;
								}
								else if(in_array($countr, $europe_county)){
									$taxPercent=Tax_Eu_Co;
								}
								else{
									$taxPercent=Tax_Outside_Eu;
								}	
							}
						}
						else{
							if($guestuser->country !=""){
								$cont=strtolower(trim($guestuser->country));
								if($cont=='netherlands'){
									$taxPercent=Tax_Neth_In;
								}
								else if(in_array($cont,$europe_county)){
									$taxPercent=Tax_Eu_In;
								}
								else{
									$taxPercent=Tax_Outside_Eu;
									
								}
							}
							else{
								$countr=$this->getUsercountry();
								if($countr=='netherlands'){
									$taxPercent=Tax_Neth_In;
								}
								else if(in_array($countr, $europe_county)){
									$taxPercent=Tax_Eu_In;
								}
								else{
									$taxPercent=Tax_Outside_Eu;
								}	
							}
						
						}
					}
					else{
						$countr=$this->getUsercountry();
						if($countr=='netherlands'){
							$taxPercent=Tax_Neth_In;
						}
						else if(in_array($countr, $europe_county)){
							$taxPercent=Tax_Eu_In;
						}
						else{
							$taxPercent=Tax_Outside_Eu;
						}
					}
					$ok_shipping=0;
					foreach($shipping_options['shippingOptions'] as $t =>$opt){
						if($opt['shippingService']['id']==$delivery_type){
							$shipping_cost=$opt['cost'];
							$shipping_method=$opt['shippingService']['name'];
							$shipping_delay=$opt['shippingService']['delay'];
							$ok_shipping=1;
							break;
						}
					}
					if($ok_shipping==0){
						return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
					}
					$totalTax = number_format(((($total + $shipping_cost) * $taxPercent)/100),2, '.', '');
					$total_cost = $total + $shipping_cost;
					$finalTotal= number_format(($total_cost + $totalTax),2, '.', '');
					$order_number = '#'.time();
					$userdetails = GuestUser::where('guest_session', '=', $session_id)->first();
					$checkout = Checkout::find($checkout_id);
					$checkout->name = $userdetails->firstname;
					$checkout->surname = $userdetails->lastname;
					$checkout->email = $userdetails->email;
					$checkout->nif = $userdetails->nr;
					$checkout->country = $userdetails->country;
					$checkout->telephone = $userdetails->phone;
					$checkout->order_number = $order_number;
					$checkout->billing_address = $userdetails->address;
					$checkout->billing_city = $userdetails->city;
					$checkout->billing_postalcode = $userdetails->postcode;
					$checkout->billing_floor = $userdetails->floor;
					$checkout->shipping_method = $shipping_method;
					$checkout->shipping_delay = $shipping_delay;
					$checkout->shipping_cost = $shipping_cost;
					$checkout->total = number_format($total,2, '.', '');
					$checkout->tax = $totalTax ;
					$checkout->total_cost = $finalTotal;
					$checkout->taxpercent = number_format($taxPercent,2, '.', '');
					$checkout->comp_name = $userdetails->comp_name;
					$checkout->vat = $userdetails->vat;
					$checkout->chamberaddress = $userdetails->chamber_commerce;
					$checkout->payment_method = 'Mollie';
					if(!isset($is_billing)){
						$shiiping_details=$ship_address[0];
						$checkout->shipping_name = $shiiping_details->name;
						$checkout->shipping_surname = $shiiping_details->surname;
						$checkout->shipping_nr = $shiiping_details->nr;
						$checkout->shipping_floor = $shiiping_details->floor_door;
						$checkout->shipping_address = $shiiping_details->address;
						$checkout->shipping_city = $shiiping_details->city;
						$checkout->shipping_country = $shiiping_details->country;
						$checkout->shipping_postalcode = $shiiping_details->postalcode;
						$checkout->shipping_phone = $shiiping_details->telephone;
						$checkout->shipping_alias = $shiiping_details->alias;
					}
					else{
						$checkout->shipping_name = $userdetails->firstname;
						$checkout->shipping_surname = $userdetails->lastname;
						$checkout->shipping_nr = $userdetails->nr;
						$checkout->shipping_floor = $userdetails->floor;
						$checkout->shipping_address = $userdetails->address;
						$checkout->shipping_city = $userdetails->city;
						$checkout->shipping_postalcode = $userdetails->postcode;
						$checkout->shipping_phone = $userdetails->phone;
						$checkout->shipping_country = $userdetails->country;
						$checkout->shipping_alias = 'Same as Billing';
					}
					$checkout->save();
					$affectedRows = CheckoutProSessions::where('guest_session', '=', $session_id)->where('checkout_id', '=',  $checkout_id)->delete();
					foreach($products as $session){
						$checkoutproduct=new CheckoutProSessions;
						$checkoutproduct->user_id = $session->user_id;
						$checkoutproduct->guest_session = $session->guest_session;
						$checkoutproduct->product_id = $session->product_id;
						$checkoutproduct->name = $session->name;
						$checkoutproduct->price = number_format($session->price,2, '.', '');
						$checkoutproduct->img = $session->img;
						$checkoutproduct->product_variation_id = $session->product_variation_id;
						$checkoutproduct->quantity = $session->quantity;
						$checkoutproduct->attributes = $session->attributes;
						$checkoutproduct->checkout_id = $checkout_id;					
						$prod=BigProduct::select('taxrate','sku')->where('id', '=', $session->product_id)->where('active', '=', 1)->first();
						if($session->product_variation_id !=0){
							$prvars=ProductVariations::select('sku')->where('id', '=', $session->product_variation_id)->where('product_id', '=', $session->product_id)->first();
							$checkoutproduct->sku = $prvars->sku;
						}
						else{
							$checkoutproduct->sku = $prod->sku;	
						}
						$checkoutproduct->taxrate = number_format($taxPercent,2, '.', '');
						$checkoutproduct->save();
					}
					return Redirect::to('/checkout/getConfirm/'.$checkout_id);
				}
				else{
					return Redirect::to('/checkout/checkout')->with("message-failure", "Something is going wrong")->withInput();
				}
			}
		}	
	}
	
	public function postShippindelivery(){
		$html="";
		$res='success';
		$shipping_price=0;
		$shipping_options=array();
		if(Auth::check()){
			$add_id = Input::get('add_id');
			$same_billing = Input::get('same_billing');
			if($same_billing=='yes'){
				$userdetails = UserWeb::where('id', '=', Auth::user()->id)->first();
				if(isset($userdetails->id)){
					if(($userdetails->postcode !="") && ($userdetails->country !="")){
						$products = array();
						$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
						if(isset($products[0])){
							$delivery_skus=array();
							foreach($products as $k=>$s){
								$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
								$delivery_skus[$k]['sku']=$prod_detail->sku;
								$delivery_skus[$k]['quantity']=$s->quantity;
								if($s->product_variation_id !=0){
									$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
									$delivery_skus[$k]['sku']=$prvars->sku;
								}
							}	
							$jsonArrayResponse=array();
							$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $userdetails->country)->first();
							if(isset($country_iso->countries_id)){
								$end_point='rest/shipping/orders.json';
								if(BIGBUG_MODE=='PRODUCTION'){
									$curl_url=BIGBUY_END_POINT."/".$end_point;
									$api_key=BIGBUY_API_KEY;
								}
								else{
									$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
									$api_key=SANDOX_BIGBUY_API_KEY;
								}
								$cURLConnection = curl_init();
								curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
								curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
									'Authorization: Bearer '.$api_key,'Content-Type: application/json'
								));
								$postRequest = array();
								$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
								$postRequest['order']['delivery']['postcode']=$userdetails->postcode;
								foreach($delivery_skus as $m => $sku){
									$postRequest['order']['products'][$m]['reference']=$sku['sku'];
									$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
								}
								curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
								curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
								$response = curl_exec($cURLConnection);
								curl_close($cURLConnection);
								$jsonArrayResponse = json_decode($response);
								if(isset($jsonArrayResponse->shippingOptions[0])){
									$shp_options=array();
									$fshp_options=array();
									foreach($jsonArrayResponse->shippingOptions as $shp){
										$nm=$shp->shippingService->name;
										if(isset($shp_options[$nm])){
											$ct=$shp->cost;
											$cts=$shp_options[$nm]->cost;
											if($ct < $cts){
												$shp_options[$nm]=$shp;
											}
										}
										else{
											$shp_options[$nm]=$shp;
										}
									}
									$i=0;
									foreach($shp_options as $shpop){
										$fshp_options[$i]=$shpop;
										$i=$i+1;
									}
									$jsonArrayResponse->shippingOptions=$fshp_options;
								}
								$chkout = Checkout::where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id','desc')->first();
								$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
								$chkout->save();
								$shipping_options=json_decode(json_encode($jsonArrayResponse),TRUE);
								$nwe="";
								if(isset($shipping_options['shippingOptions'][0])){
									foreach($shipping_options['shippingOptions'] as $t =>$opt){
										if ($t==0){
											$shipping_price=number_format($opt['cost'],2);
											$nwe='checked="checked"';
										}
										else{
											$nwe="";
										}
										$html.='<li>
											<div class="radios_sec">	
												<label class="radio-inline center cstm_radio"> <input '.$nwe.'type="radio" name="delivery_type" prc="'.number_format($opt['cost'],2).'" value="'.$opt['shippingService']['id'].'"  class="used drop_points"> <span class="checkmark"></span>'.$opt['shippingService']['name'].'<span>'.$opt['shippingService']['delay'].'</span> </label>
												<span class="price_sec">'.Config::get('view.currency').number_format($opt['cost'],2).'</span>	
											</div>
										</li>';
									}
								}
								else{
									$res='error';
									$html.='<li>
										<div class="radios_sec">	
											<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
										</div>
									</li>';
								}
							}
							else{
								$res='error';
								$html.='<li>
									<div class="radios_sec">	
									<span style="color:red;">Something is going wrong.</span>	
									</div>
								</li>';
							}
						}
						else{
							$res='error';
							$html.='<li>
								<div class="radios_sec">	
								<span style="color:red;">Something is going wrong.</span>	
								</div>
							</li>';
						}
					}
					else{
						$res='error';
						$html.='<li>
							<div class="radios_sec">	
							<span style="color:red;">Please enter the postal code and country of billing address.</span>	
							</div>
						</li>';
					}
				}
				else{
					$res='error';
					$html.='<li>
						<div class="radios_sec">	
						<span style="color:red;">Something is going wrong.</span>	
						</div>
					</li>';
				}
			}
			else{
				if($add_id !=""){
					if($add_id !=0){
						$shippingaddresses = Shippingaddress::where('user_id', '=', Auth::user()->id)->where('id', '=', $add_id)->first();
						if(isset($shippingaddresses->id)){
							$products = array();
							$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
							if(isset($products[0])){
								$delivery_skus=array();
								foreach($products as $k=>$s){
									$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
									$delivery_skus[$k]['sku']=$prod_detail->sku;
									$delivery_skus[$k]['quantity']=$s->quantity;
									if($s->product_variation_id !=0){
										$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
										$delivery_skus[$k]['sku']=$prvars->sku;
									}
								}	
								$jsonArrayResponse=array();
								$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $shippingaddresses->country)->first();
								if(isset($country_iso->countries_id)){
									$end_point='rest/shipping/orders.json';
									if(BIGBUG_MODE=='PRODUCTION'){
										$curl_url=BIGBUY_END_POINT."/".$end_point;
										$api_key=BIGBUY_API_KEY;
									}
									else{
										$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
										$api_key=SANDOX_BIGBUY_API_KEY;
									}
									$cURLConnection = curl_init();
									curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
									curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
										'Authorization: Bearer '.$api_key,'Content-Type: application/json'
									));
									$postRequest = array();
									$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
									$postRequest['order']['delivery']['postcode']=$shippingaddresses->postalcode;
									foreach($delivery_skus as $m => $sku){
										$postRequest['order']['products'][$m]['reference']=$sku['sku'];
										$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
									}
									curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
									curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
									$response = curl_exec($cURLConnection);
									curl_close($cURLConnection);
									$jsonArrayResponse = json_decode($response);
									if(isset($jsonArrayResponse->shippingOptions[0])){
									$shp_options=array();
									$fshp_options=array();
									foreach($jsonArrayResponse->shippingOptions as $shp){
										$nm=$shp->shippingService->name;
										if(isset($shp_options[$nm])){
											$ct=$shp->cost;
											$cts=$shp_options[$nm]->cost;
											if($ct < $cts){
												$shp_options[$nm]=$shp;
											}
										}
										else{
											$shp_options[$nm]=$shp;
										}
									}
									$i=0;
									foreach($shp_options as $shpop){
										$fshp_options[$i]=$shpop;
										$i=$i+1;
									}
									$jsonArrayResponse->shippingOptions=$fshp_options;
								}
									$chkout = Checkout::where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id','desc')->first();
									$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
									$chkout->save();
									$shipping_options=json_decode(json_encode($jsonArrayResponse),TRUE);
									$nwe="";
									if(isset($shipping_options['shippingOptions'][0])){
										foreach($shipping_options['shippingOptions'] as $t =>$opt){
											if ($t==0){
												$shipping_price=number_format($opt['cost'],2);
												$nwe='checked="checked"';
											}
											else{
												$nwe="";
											}
											$html.='<li>
												<div class="radios_sec">	
													<label class="radio-inline center cstm_radio"> <input '.$nwe.'type="radio" name="delivery_type" prc="'.number_format($opt['cost'],2).'" value="'.$opt['shippingService']['id'].'"  class="used drop_points"> <span class="checkmark"></span>'.$opt['shippingService']['name'].'<span>'.$opt['shippingService']['delay'].'</span> </label>
													<span class="price_sec">'.Config::get('view.currency').number_format($opt['cost'],2).'</span>	
												</div>
											</li>';
										}
									}
									else{
										$res='error';
										$html.='<li>
											<div class="radios_sec">	
												<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
											</div>
										</li>';
									}
								}
								else{
									$res='error';
									$html.='<li>
										<div class="radios_sec">	
										<span style="color:red;">Something is going wrong.</span>	
										</div>
									</li>';
								}
							}
							else{
								$res='error';
								$html.='<li>
									<div class="radios_sec">	
									<span style="color:red;">Something is going wrong.</span>	
									</div>
								</li>';
							}
						}
						else{
							$res='error';
							$html.='<li>
								<div class="radios_sec">	
								<span style="color:red;">Something is going wrong.</span>	
								</div>
							</li>';
						}
					}
					else{
						$res='error';
						$html.='<li>
							<div class="radios_sec">	
								<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
							</div>
						</li>';
					}
				}
				else{
					$res='error';
					$html.='<li>
						<div class="radios_sec">	
						<span style="color:red;">Something is going wrong.</span>	
						</div>
					</li>';
				}
			}
		}
		else{
			$res='error';
			$html.='<li>
				<div class="radios_sec">	
				<span style="color:red;">Something is going wrong.</span>	
				</div>
			</li>';
		}
		echo json_encode(array('html'=>$html,'resp'=>$res,'shipping_price'=>$shipping_price));
	}
	
	public function postShipindeliveryguest(){
		$html="";
		$res='success';
		$shipping_price=0;
		$shipping_options=array();
		if(!Auth::check()){
			$add_id = Input::get('add_id');
			$same_billing = Input::get('same_billing');
			if($same_billing=='yes'){
				$userdetails = GuestUser::where('guest_session', '=', Session::getId())->first();
				if(isset($userdetails->id)){
					if(($userdetails->postcode !="") && ($userdetails->country !="")){
						$products = array();
						$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
						if(isset($products[0])){
							$delivery_skus=array();
							foreach($products as $k=>$s){
								$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
								$delivery_skus[$k]['sku']=$prod_detail->sku;
								$delivery_skus[$k]['quantity']=$s->quantity;
								if($s->product_variation_id !=0){
									$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
									$delivery_skus[$k]['sku']=$prvars->sku;
								}
							}	
							$jsonArrayResponse=array();
							$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $userdetails->country)->first();
							if(isset($country_iso->countries_id)){
								$end_point='rest/shipping/orders.json';
								if(BIGBUG_MODE=='PRODUCTION'){
									$curl_url=BIGBUY_END_POINT."/".$end_point;
									$api_key=BIGBUY_API_KEY;
								}
								else{
									$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
									$api_key=SANDOX_BIGBUY_API_KEY;
								}
								$cURLConnection = curl_init();
								curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
								curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
									'Authorization: Bearer '.$api_key,'Content-Type: application/json'
								));
								$postRequest = array();
								$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
								$postRequest['order']['delivery']['postcode']=$userdetails->postcode;
								foreach($delivery_skus as $m => $sku){
									$postRequest['order']['products'][$m]['reference']=$sku['sku'];
									$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
								}
								curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
								curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
								$response = curl_exec($cURLConnection);
								curl_close($cURLConnection);
								$jsonArrayResponse = json_decode($response);
								if(isset($jsonArrayResponse->shippingOptions[0])){
									$shp_options=array();
									$fshp_options=array();
									foreach($jsonArrayResponse->shippingOptions as $shp){
										$nm=$shp->shippingService->name;
										if(isset($shp_options[$nm])){
											$ct=$shp->cost;
											$cts=$shp_options[$nm]->cost;
											if($ct < $cts){
												$shp_options[$nm]=$shp;
											}
										}
										else{
											$shp_options[$nm]=$shp;
										}
									}
									$i=0;
									foreach($shp_options as $shpop){
										$fshp_options[$i]=$shpop;
										$i=$i+1;
									}
									$jsonArrayResponse->shippingOptions=$fshp_options;
								}
								$chkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id','desc')->first();
								$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
								$chkout->save();
								$shipping_options=json_decode(json_encode($jsonArrayResponse),TRUE);
								$nwe="";
								if(isset($shipping_options['shippingOptions'][0])){
									foreach($shipping_options['shippingOptions'] as $t =>$opt){
										if ($t==0){
											$shipping_price=number_format($opt['cost'],2);
											$nwe='checked="checked"';
										}
										else{
											$nwe="";
										}
										$html.='<li>
											<div class="radios_sec">	
												<label class="radio-inline center cstm_radio"> <input '.$nwe.'type="radio" name="delivery_type" prc="'.number_format($opt['cost'],2).'" value="'.$opt['shippingService']['id'].'"  class="used drop_points"> <span class="checkmark"></span>'.$opt['shippingService']['name'].'<span>'.$opt['shippingService']['delay'].'</span> </label>
												<span class="price_sec">'.Config::get('view.currency').number_format($opt['cost'],2).'</span>	
											</div>
										</li>';
									}
								}
								else{
									$res='error';
									$html.='<li>
										<div class="radios_sec">	
											<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
										</div>
									</li>';
								}
							}
							else{
								$res='error';
								$html.='<li>
									<div class="radios_sec">	
									<span style="color:red;">Something is going wrong.</span>	
									</div>
								</li>';
							}
						}
						else{
							$res='error';
							$html.='<li>
								<div class="radios_sec">	
								<span style="color:red;">Something is going wrong.</span>	
								</div>
							</li>';
						}
					}
					else{
						$res='error';
						$html.='<li>
							<div class="radios_sec">	
							<span style="color:red;">Please enter the postal code and country of billing address.</span>	
							</div>
						</li>';
					}
				}
				else{
					$res='erroradd';
					$html.='<li>
						<div class="radios_sec">	
						<span style="color:red;">Please fill your billing address to get the shipping options.</span>	
						</div>
					</li>';
				}
			}
			else{
				if($add_id !=""){
					if($add_id !=0){
						$shippingaddresses = Shippingaddress::where('guest_session', '=', Session::getId())->where('id', '=', $add_id)->first();
						if(isset($shippingaddresses->id)){
							$products = array();
							$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->orderBy('id', 'desc')->get();
							if(isset($products[0])){
								$delivery_skus=array();
								foreach($products as $k=>$s){
									$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
									$delivery_skus[$k]['sku']=$prod_detail->sku;
									$delivery_skus[$k]['quantity']=$s->quantity;
									if($s->product_variation_id !=0){
										$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
										$delivery_skus[$k]['sku']=$prvars->sku;
									}
								}	
								$jsonArrayResponse=array();
								$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $shippingaddresses->country)->first();
								if(isset($country_iso->countries_id)){
									$end_point='rest/shipping/orders.json';
									if(BIGBUG_MODE=='PRODUCTION'){
										$curl_url=BIGBUY_END_POINT."/".$end_point;
										$api_key=BIGBUY_API_KEY;
									}
									else{
										$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
										$api_key=SANDOX_BIGBUY_API_KEY;
									}
									$cURLConnection = curl_init();
									curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
									curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
										'Authorization: Bearer '.$api_key,'Content-Type: application/json'
									));
									$postRequest = array();
									$postRequest['order']['delivery']['isoCountry']=$country_iso->countries_iso_code;
									$postRequest['order']['delivery']['postcode']=$shippingaddresses->postalcode;
									foreach($delivery_skus as $m => $sku){
										$postRequest['order']['products'][$m]['reference']=$sku['sku'];
										$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
									}
									curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
									curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
									$response = curl_exec($cURLConnection);
									curl_close($cURLConnection);
									$jsonArrayResponse = json_decode($response);
									if(isset($jsonArrayResponse->shippingOptions[0])){
										$shp_options=array();
										$fshp_options=array();
										foreach($jsonArrayResponse->shippingOptions as $shp){
											$nm=$shp->shippingService->name;
											if(isset($shp_options[$nm])){
												$ct=$shp->cost;
												$cts=$shp_options[$nm]->cost;
												if($ct < $cts){
													$shp_options[$nm]=$shp;
												}
											}
											else{
												$shp_options[$nm]=$shp;
											}
										}
										$i=0;
										foreach($shp_options as $shpop){
											$fshp_options[$i]=$shpop;
											$i=$i+1;
										}
										$jsonArrayResponse->shippingOptions=$fshp_options;
									}
									$chkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id','desc')->first();
									$chkout->all_shipping_options=json_encode($jsonArrayResponse);	
									$chkout->save();
									$shipping_options=json_decode(json_encode($jsonArrayResponse),TRUE);
									$nwe="";
									if(isset($shipping_options['shippingOptions'][0])){
										foreach($shipping_options['shippingOptions'] as $t =>$opt){
											if ($t==0){
												$shipping_price=number_format($opt['cost'],2);
												$nwe='checked="checked"';
											}
											else{
												$nwe="";
											}
											$html.='<li>
												<div class="radios_sec">	
													<label class="radio-inline center cstm_radio"> <input '.$nwe.'type="radio" name="delivery_type" prc="'.number_format($opt['cost'],2).'" value="'.$opt['shippingService']['id'].'"  class="used drop_points"> <span class="checkmark"></span>'.$opt['shippingService']['name'].'<span>'.$opt['shippingService']['delay'].'</span> </label>
													<span class="price_sec">'.Config::get('view.currency').number_format($opt['cost'],2).'</span>	
												</div>
											</li>';
										}
									}
									else{
										$res='error';
										$html.='<li>
											<div class="radios_sec">	
												<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
											</div>
										</li>';
									}
								}
								else{
									$res='error';
									$html.='<li>
										<div class="radios_sec">	
										<span style="color:red;">Something is going wrong.</span>	
										</div>
									</li>';
								}
							}
							else{
								$res='error';
								$html.='<li>
									<div class="radios_sec">	
									<span style="color:red;">Something is going wrong.</span>	
									</div>
								</li>';
							}
						}
						else{
							$res='error';
							$html.='<li>
								<div class="radios_sec">	
								<span style="color:red;">Something is going wrong.</span>	
								</div>
							</li>';
						}
					}
					else{
						$res='erroradd';
						$html.='<li>
							<div class="radios_sec">	
								<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>	
							</div>
						</li>';
					}
				}
				else{
					$res='error';
					$html.='<li>
						<div class="radios_sec">	
						<span style="color:red;">Something is going wrong.</span>	
						</div>
					</li>';
				}
			}
		}
		else{
			$res='error';
			$html.='<li>
				<div class="radios_sec">	
				<span style="color:red;">Something is going wrong.</span>	
				</div>
			</li>';
		}
		echo json_encode(array('html'=>$html,'resp'=>$res,'shipping_price'=>$shipping_price));
	}
	
	
	public function postBillingupdate(){
		$type = Input::get('type');
		if($type == 0){
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'phone' => 'required',
				'email'=>'required|email'
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'phone' => trim(Input::get('phone')),
				'email' => trim(Input::get('billing_email'))
			];
		}
		else{
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'comp_name' => 'required',
				'email'=>'required|email',
				'vat' => 'required',
				'chamber_commerce' => 'required'
				
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'comp_name' => trim(Input::get('comp_name')),
				'email' => trim(Input::get('billing_email')),
				'vat' => trim(Input::get('vat')),
				'chamber_commerce' => trim(Input::get('chamber_commerce'))
			];
		}
		$validator = Validator::make($data,$rules);
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			$session_id=Session::getId();
			$guest_user = GuestUser::where('guest_session', '=', $session_id)->first();
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$address = Input::get('address');
			$postcode = Input::get('postcode');
			$comp_name=Input::get('comp_name');
			$email=Input::get('billing_email');
			$vat=Input::get('vat');
			$chamber_commerce=Input::get('chamber_commerce');
			$floor=Input::get('floor');
			$country=Input::get('country');
			$city=Input::get('city');
			$phone=Input::get('phone');
			$nr=Input::get('nr');
			$guest_user->firstname = $firstname;
			$guest_user->lastname = $lastname;
			$guest_user->address = $address;
			$guest_user->postcode =  $postcode;
			$guest_user->comp_name=$comp_name;
			$guest_user->email=$email;
			$guest_user->vat=$vat;
			$guest_user->chamber_commerce=$chamber_commerce;
			$guest_user->floor=$floor;
			$guest_user->country=$country;
			$guest_user->city=$city;
			$guest_user->phone=$phone;
			$guest_user->nr=$nr;
			$guest_user->type = $type;
			$guest_user->save();	
			$resultSet['Success'] = 'Billing Address updated Successfully';
			echo json_encode($resultSet);
	    }				
	}
	
	
	public function postBillingadd(){
		$type = Input::get('type');
		if($type == 0){
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'phone' => 'required',
				'email'=>'required|email'
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'phone' => trim(Input::get('phone')),
				'email' => trim(Input::get('email'))
			];
		}
		else{
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'comp_name' => 'required',
				'email'=>'required|email',
				'vat' => 'required',
				'chamber_commerce' => 'required'
				
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'comp_name' => trim(Input::get('comp_name')),
				'email' => trim(Input::get('email')),
				'vat' => trim(Input::get('vat')),
				'chamber_commerce' => trim(Input::get('chamber_commerce'))
			];
		}
		$validator = Validator::make($data,$rules);
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			$session_id=Session::getId();
			$guest_user = New GuestUser;
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$address = Input::get('address');
			$postcode = Input::get('postcode');
			$comp_name=Input::get('comp_name');
			$email=Input::get('email');
			$vat=Input::get('vat');
			$chamber_commerce=Input::get('chamber_commerce');
			$floor=Input::get('floor');
			$country=Input::get('country');
			$city=Input::get('city');
			$phone=Input::get('phone');
			$nr=Input::get('nr');
			$guest_user->firstname = $firstname;
			$guest_user->guest_session = $session_id;
			$guest_user->lastname = $lastname;
			$guest_user->address = $address;
			$guest_user->postcode =  $postcode;
			$guest_user->comp_name=$comp_name;
			$guest_user->email=$email;
			$guest_user->vat=$vat;
			$guest_user->chamber_commerce=$chamber_commerce;
			$guest_user->floor=$floor;
			$guest_user->country=$country;
			$guest_user->city=$city;
			$guest_user->phone=$phone;
			$guest_user->nr=$nr;
			$guest_user->type = $type;
			$guest_user->save();	
			$resultSet['Success'] = 'Billing Address updated Successfully';
			echo json_encode($resultSet);
	    }				
	}
	
	public function postBillupdate(){
		$type = Input::get('type');
		if($type == 0){
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'phone' => 'required',
				'email'=>'required|email'
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'phone' => trim(Input::get('phone')),
				'email' => trim(Input::get('billing_email'))
			];
		}
		else{
			$rules = array(
				'firstname' => 'required',
				'lastname' => 'required',
				'address' => 'required',
				'postcode' => 'required',
				'comp_name' => 'required',
				'email'=>'required|email',
				'vat' => 'required',
				'chamber_commerce' => 'required'
				
			);
			$data = [
				'firstname' => trim(Input::get('firstname')),
				'lastname' => trim(Input::get('lastname')),
				'address' => trim(Input::get('address')),
				'postcode' => trim(Input::get('postcode')),
				'comp_name' => trim(Input::get('comp_name')),
				'email' => trim(Input::get('billing_email')),
				'vat' => trim(Input::get('vat')),
				'chamber_commerce' => trim(Input::get('chamber_commerce'))
			];
		}
		$validator = Validator::make($data,$rules);
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			$user_id=Auth::user()->id;
			$guest_user = UserWeb::where('id', '=', $user_id)->first();
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$address = Input::get('address');
			$postcode = Input::get('postcode');
			$comp_name=Input::get('comp_name');
			$email=Input::get('billing_email');
			$vat=Input::get('vat');
			$chamber_commerce=Input::get('chamber_commerce');
			$floor=Input::get('floor');
			$country=Input::get('country');
			$city=Input::get('city');
			$phone=Input::get('phone');
			$nr=Input::get('nr');
			$guest_user->firstname = $firstname;
			$guest_user->lastname = $lastname;
			$guest_user->address = $address;
			$guest_user->postcode =  $postcode;
			$guest_user->comp_name=$comp_name;
			$guest_user->billing_email=$email;
			$guest_user->vat=$vat;
			$guest_user->chamber_commerce=$chamber_commerce;
			$guest_user->floor=$floor;
			$guest_user->country=$country;
			$guest_user->city=$city;
			$guest_user->phone=$phone;
			$guest_user->nr=$nr;
			$guest_user->type = $type;
			$guest_user->save();	
			$resultSet['Success'] = 'Billing Address updated Successfully';
			echo json_encode($resultSet);
	    }				
	}
	
	
	public function postShippingadd(){
		$rules = array(
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
			'telephone' => 'required',
			'alias' => 'required',
        );

        $data = [
            'name' => trim(Input::get('name')),
            'surname' => trim(Input::get('surname')),
            'address' => trim(Input::get('address')),
            'postalcode' => trim(Input::get('postalcode')),
            'city' => trim(Input::get('city')),
			'telephone' => trim(Input::get('telephone')),
            'alias' => trim(Input::get('alias')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
	    
	    }
		else{
			$name = Input::get('name');
			$surname = Input::get('surname');
			$address = Input::get('address');
			$postalcode = Input::get('postalcode');
			$city=Input::get('city');
			$country=Input::get('country');
			$telephone=Input::get('telephone');
			$alias=Input::get('alias');
			$nr=Input::get('nr');
			$floor_door=Input::get('floor_door');
			$session_id=Session::getId();
			$user = new Shippingaddress;
			$user->name = $name;
			$user->surname = $surname;
			$user->address = $address;
			$user->postalcode =  $postalcode;
			$user->city=$city;
			$user->country=$country;
			$user->telephone=$telephone;
			$user->alias=$alias;
			$user->nr=$nr;
			$user->floor_door=$floor_door;
			$user->guest_session=$session_id;
			$user->save();	
			$resultSet['Success'] = 'Shipping Address added Successfully';
			echo json_encode($resultSet);
	    }				
	}
	
	public function postShippingedit(){
		$rules = array(
			'name' => 'required',
			'surname' => 'required',
			'address' => 'required',
			'postalcode' => 'required',
			'city' => 'required',
			'telephone' => 'required',
			'alias' => 'required',
		);

		$data = [
			'name' => trim(Input::get('name')),
			'surname' => trim(Input::get('surname')),
			'address' => trim(Input::get('address')),
			'postalcode' => trim(Input::get('postalcode')),
			'city' => trim(Input::get('city')),
			'telephone' => trim(Input::get('telephone')),
			'alias' => trim(Input::get('alias')),
		];
		$validator = Validator::make($data,$rules);
	 
		if($validator->fails()){
			echo json_encode(['errors'=>$validator->messages()]);
		
		}
		else{
			$id = Input::get('id');
			$name = Input::get('name');
			$surname = Input::get('surname');
			$address = Input::get('address');
			$postalcode = Input::get('postalcode');
			$city=Input::get('city');
			$country=Input::get('country');
			$telephone=Input::get('telephone');
			$alias=Input::get('alias');
			$nr=Input::get('nr');
			$floor_door=Input::get('floor_door');
			$session_id=Session::getId();
			$user = Shippingaddress::where('id', '=', $id)->where('guest_session', '=', $session_id)->first();
			$user->name = $name;
			$user->surname = $surname;
			$user->address = $address;
			$user->postalcode =  $postalcode;
			$user->city=$city;
			$user->country=$country;
			$user->telephone=$telephone;
			$user->alias=$alias;
			$user->nr=$nr;
			$user->floor_door=$floor_door;
			$user->save();	
			$resultSet['Success'] = 'Shipping Address updated Successfully';
			echo json_encode($resultSet);
		}
	}



    public function getConfirm($id){
		$products = array();
		if(Auth::check()){
			$checkout = Checkout::where('id',$id)->where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->first();
			if(!isset($checkout->id)){
				return Redirect::to('/webshop/');  
			}
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
		}
		else{
			$checkout = Checkout::where('id',$id)->where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->first();
			if(!isset($checkout->id)){
				return Redirect::to('/webshop/');  
			}
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
		}
		$delivery_skus=array();
		foreach($products as $k=>$s){
			$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
			$products[$k]->products=$prod_detail;
			$delivery_skus[$k]['sku']=$prod_detail->sku;
			$delivery_skus[$k]['quantity']=$s->quantity;
			if($s->product_variation_id !=0){
				$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
				$products[$k]->prvars=$prvars ;
				$delivery_skus[$k]['sku']=$prvars->sku;
			}
		}
		if(isset($checkout->id)){
			$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $checkout->shipping_country)->first();
			$end_point='rest/order/check.json';
			if(BIGBUG_MODE=='PRODUCTION'){
				$curl_url=BIGBUY_END_POINT."/".$end_point;
				$api_key=BIGBUY_API_KEY;
			}
			else{
				$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
				$api_key=SANDOX_BIGBUY_API_KEY;
			}
			$cURLConnection = curl_init();
			curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
			curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
				'Authorization: Bearer '.$api_key,'Content-Type: application/json'
			));
			
			$postRequest = array();
			$postRequest['order']['internalReference']=$checkout->order_number;
			$postRequest['order']['language']="en";
			$postRequest['order']['paymentMethod']="paypal";
			$postRequest['order']['carriers']['0']['name']=strtolower($checkout->shipping_method);
			$postRequest['order']['shippingAddress']['firstName']=$checkout->shipping_name;
			$postRequest['order']['shippingAddress']['lastName']=$checkout->shipping_surname;
			$postRequest['order']['shippingAddress']['country']=$country_iso->countries_iso_code;
			$postRequest['order']['shippingAddress']['postcode']=$checkout->shipping_postalcode;
			$postRequest['order']['shippingAddress']['town']=$checkout->shipping_city;
			$ship_address=array();
			$ship_address[0]=$checkout->shipping_address;
			$ship_address[1]=$checkout->shipping_nr;
			$ship_address[2]=$checkout->shipping_floor;
			$postRequest['order']['shippingAddress']['address']=implode(",",$ship_address);
			$postRequest['order']['shippingAddress']['phone']=$checkout->shipping_phone;
			$postRequest['order']['shippingAddress']['email']=$checkout->email;
			foreach($delivery_skus as $m => $sku){
				$postRequest['order']['products'][$m]['reference']=$sku['sku'];
				$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
			}	
			
			curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
			curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($cURLConnection);
			$jsonArra = json_decode($response,true);

			if(isset($jsonArra['code'])){
				Session::flash('message-error','There are some issue to process your order, please try after some time.');
				return Redirect::to('/checkout/checkout');
			}
			else{
				$ttwtaxes=$jsonArra['totalWithoutTaxes'];
				$total_prc=$checkout->total;
				$ship_prc=$checkout->shipping_cost;
				$tall=$total_prc+$ship_prc;
				if($tall < $ttwtaxes){
					//Session::flash('message-error','There are some issue to process your order, please try after some time.');
					//return Redirect::to('/checkout/checkout');
				}
			}
		}
		return View::make('webshop/confirm', array('page'=>'confirm', 'products'=>$products,'checkout'=>$checkout));
    }

    

    public function postConfirm($id){
		if(Auth::check()){
			Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->delete();
		}
		else{
			Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->delete();
		}

		if(Auth::check()){
			$auth_user_id = Auth::user()->id;
			$checkout = Checkout::where('id',$id)->where('user_id',$auth_user_id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->first();
		}
		else{
			$checkout = Checkout::where('id',$id)->where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->first();
		}

		$lastTotalsum = number_format($checkout->total_cost,2) * 100;
		$lastTotal = round($lastTotalsum,0);
		$checkouttype = Checkout::find($id);
		$checkouttype->type = 'pending';
		$checkouttype->save();
		return Redirect::to('postPayment/'.$lastTotal);    			
	}
 
	public function postUpdateaddress(){
	
		$products = array();
		if(Auth::check()){
			$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=', 0)->get();
			
		}
		else{
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=', 0)->get();
		}
		
		$items = 0;
		$total = 0;
		foreach($products as $session){
			$items = $items + $session->quantity;
			$total = $total + ($session->quantity * $session->price);
		}
		
		//echo '';print_r([Input::all(), $total]);die;
		$validator = Validator::make(Input::all(), array(
			'email'=> 'required',
			'name'=> 'required',
			'surname'=> 'required',
			'number'=> 'required',
			/*'nif'=> 'required',*/
			'address'=> 'required',
			/*'province'=> 'required',*/
			'city'=> 'required',
			/*'telephone'=> 'required',*/
			'postal_code'=> 'required',
			//'delivery_type'=> 'required',
			//'total_price'=> 'required',
			'f_d'=> 'required'
			)
		);
		
		
		
		
		//if($validator->passes() && (Input::get('total_price') == $total)){
		if($validator->passes()){
	
			//$use_as_blling = Input::get('use_as_blling');
			$email = Input::get('email');
			$name = Input::get('name');
			$surname = Input::get('surname');
			$nif = Input::get('nif');
			$province = Input::get('province');
			$address = Input::get('address');
			$number = Input::get('number');
			$postal_code = Input::get('postal_code');
			$city = Input::get('city');
			$telephone = Input::get('telephone');
			$shipping_state = Input::get('f_d');
			//$delivery_type = Input::get('delivery_type');
			$id = Input::get('edit_id');
			
			
			$checkout = Checkout::find($id);
		
			
			if(Auth::check()){
				$checkout->user_id = Auth::user()->id;
			}
			
			//$checkout->shipping_method = $delivery_type;
			$checkout->name = $name;
			$checkout->surname = $surname;
			$checkout->email = $email;
			$checkout->nif = $nif;
			$checkout->province = $province;
			$checkout->number = $number;
			$checkout->telephone = $telephone;
			$checkout->order_number = '#'.time();
			
			$checkout->shipping_address = $address;
			$checkout->shipping_city = $city;
			$checkout->shipping_state = $shipping_state;
			$checkout->shipping_postalcode = $postal_code;
			$checkout->shipping_method = '';
			$checkout->shipping_cost = '';
			//$checkout->total_cost = $total;
			//$checkout->order_notes = '';
			$checkout->save();


			if(Auth::check()){
					$shipping = Shipping::where('user_id', '=', Auth::user()->id)->first();
					$shipping->user_id = Auth::user()->id;
					$shipping->name = $name;
					$shipping->email = $email;
					$shipping->number = $number;
					$shipping->surname = $surname;
					$shipping->nif = $nif;
					$shipping->province = $province;
					$shipping->floor_door = $shipping_state;
					$shipping->telephone = $telephone;
					$shipping->order_number = '#'.time();
					$shipping->billing_address = $address;
					$shipping->billing_city = $city;
					$shipping->billing_state = '';
					$shipping->billing_postalcode = $postal_code;
					
					$shipping->shipping_address = $address;
					$shipping->shipping_city = $city;
					$shipping->shipping_state = '';
					$shipping->shipping_postalcode = $postal_code;
					//$shipping->shipping_method = $delivery_type;
					$shipping->shipping_cost = '';
					$shipping->save();
		         }
			
			
			return Redirect::back();
			
		}
		return Redirect::back()->with("message-failure", "Please provide all the required information!")->withInput();
	}
	
	
	public function getCards(){
		
		echo "hello";
		//return View('webshop.inner.cards');
	}
	
	
	public function getPayment(){

		$categories = Category::where('parentid', '=', 0)->get();
		$products = array();
		if(Auth::check()){
            $checkout = Checkout::where('user_id',Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
            if(isset($checkout[0]->id)){

            $products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',$checkout[0]->id)->get();
            }else{

            	$products =[];
            }

		}
		else{
			$checkout = Checkout::where('guest_session',Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			if(isset($checkout[0]->id)){
			$products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',$checkout[0]->id)->get();
		    }else{

            	$products =[];
            }
		}
		foreach($products as $k=>$s){
			$p=Products::where('id','=',$s->product_id)->get();
			$ss = Productmetas::where('product_id', '=', $s->product_id)->get();
			if(isset($ss[0])){
				foreach($ss as $ms){
					$products[$k]->{$ms->meta_key} = $ms->meta_value;
				}
			}
			if(isset($p[0])){$p = $p[0];
				$products[$k]->cat = Category::select('slug')->where('id', '=', $p->catid)->get();
				$products[$k]->parentcat = Category::select('slug')->where('id', '=', $p->parentcatid)->get();
			}
		}
		return View::make('webshop/payment', array('page'=>'basket', 'products'=>$products, 'categories'=>$categories));
	}
	

	public function getPlaceorder(){
		
		$transactionid = $_GET['transactionid'];
		if(isset($transactionid) && $transactionid!=""){
			$products = array();
			if(Auth::check()){
				$checkout = Checkout::where('user_id',Auth::user()->id)->where('type', '=', 'pending')->orderBy('id', 'desc')->limit(1)->get();
				if(count($checkout)>0){
					Checkout::where('id','=',$checkout[0]->id)->where('type', '=', 'pending')->update(array('type'=>'completed','transactionid'=>$transactionid));	
					$products = CheckoutProSessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',$checkout[0]->id)->get();
					$shipping = Checkout::where('user_id','=',Auth::user()->id)->where('id', '=',$checkout[0]->id)->orderBy('id', 'desc')->first();
				}
				else{
					$products =[];
				}	
				
			}
			else{
				$checkout = Checkout::where('guest_session',Session::getId())->where('type', '=', 'pending')->orderBy('id', 'desc')->limit(1)->get();
				if(count($checkout)>0){
					Checkout::where('id', '=', $checkout[0]->id)->where('type', '=', 'pending')->update(array('type'=>'completed','transactionid'=>$transactionid));
					$products = CheckoutProSessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',$checkout[0]->id)->get();
					$shipping = Checkout::where('id',$checkout[0]->id)->first();
				}
				else{
					$products =[];
				}
			}
			if (count($products)>0) {
				$delivery_skus=array();
				foreach($products as $k=>$s){
					$prod_detail  = BigProduct::where('id','=',$s->product_id)->orderBy('id', 'asc')->first();
					$products[$k]->products=$prod_detail;
					$delivery_skus[$k]['sku']=$prod_detail->sku;
					$delivery_skus[$k]['quantity']=$s->quantity;
					if($s->product_variation_id !=0){
						$prvars=ProductVariations::where('id', '=', $s->product_variation_id)->where('product_id', '=', $s->product_id)->first();
						$products[$k]->prvars=$prvars ;
						$delivery_skus[$k]['sku']=$prvars->sku;
					}
				}
				
				if(isset($checkout[0]->id)){
					$country_iso = Shipcountriesnames::orderBy('countries_name', 'asc')->where('countries_name', '=', $checkout[0]->shipping_country)->first();
					$end_point='rest/order/create.json';
					if(BIGBUG_MODE=='PRODUCTION'){
						$curl_url=BIGBUY_END_POINT."/".$end_point;
						$api_key=BIGBUY_API_KEY;
					}
					else{
						$curl_url=SANDOX_BIGBUY_END_POINT."/".$end_point;
						$api_key=SANDOX_BIGBUY_API_KEY;
					}
					/*$cURLConnection = curl_init();
					curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
					curl_setopt($cURLConnection, CURLOPT_HEADER, 1);
					curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
						'Authorization: Bearer '.$api_key,'Content-Type: application/json'
					));
					
					$postRequest = array();
					$postRequest['order']['internalReference']=$checkout[0]->order_number;
					$postRequest['order']['language']="en";
					$postRequest['order']['paymentMethod']="paypal";
					$postRequest['order']['carriers']['0']['name']=strtolower($checkout[0]->shipping_method);
					$postRequest['order']['shippingAddress']['firstName']=$checkout[0]->shipping_name;
					$postRequest['order']['shippingAddress']['lastName']=$checkout[0]->shipping_surname;
					$postRequest['order']['shippingAddress']['country']=$country_iso->countries_iso_code;
					$postRequest['order']['shippingAddress']['postcode']=$checkout[0]->shipping_postalcode;
					$postRequest['order']['shippingAddress']['town']=$checkout[0]->shipping_city;
					$ship_address=array();
					$ship_address[0]=$checkout[0]->shipping_address;
					$ship_address[1]=$checkout[0]->shipping_nr;
					$ship_address[2]=$checkout[0]->shipping_floor;
					$postRequest['order']['shippingAddress']['address']=implode(",",$ship_address);
					$postRequest['order']['shippingAddress']['phone']=$checkout[0]->shipping_phone;
					$postRequest['order']['shippingAddress']['email']=$checkout[0]->email;
					foreach($delivery_skus as $m => $sku){
						$postRequest['order']['products'][$m]['reference']=$sku['sku'];
						$postRequest['order']['products'][$m]['quantity']=$sku['quantity'];
					}					
					curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
					curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($cURLConnection);
					$header_size = curl_getinfo($cURLConnection, CURLINFO_HEADER_SIZE);
					$header = substr($response, 0, $header_size);
					$body = substr($response, $header_size);
					curl_close($cURLConnection);
					
					
					
					$headers = [];
					$response = rtrim($response);
					$data = explode("\n",$response);
					$headers['status'] = $data[0];
					array_shift($data);
					foreach($data as $part){
						$middle = explode(":",$part,2);
						if ( !isset($middle[1]) ) { $middle[1] = null; }
						$headers[trim($middle[0])] = trim($middle[1]);
					}
					$jsonArra=array();
					$jsonArra = json_decode($body,true);
					if(isset($jsonArra['code'])){
						Checkout::where('id', '=', $checkout[0]->id)->where('type', '=', 'completed')->update(array('type'=>'Need to refund'));
						Session::flash('message-error','Sorry, there are some internal issue occurred to process your order, we will refund your payment soon. Thanks');
					}
					else{
						if(isset($headers['Location'])){
							$loct=$headers['Location'];
							$order_id_info=explode("/",$loct);
							$n=count($order_id_info);
							$oid=$order_id_info[$n-1];
							Checkout::where('id', '=', $checkout[0]->id)->where('type', '=', 'completed')->update(array('bigbuy_order_location'=>$loct,'bigbuy_order_id'=>$oid));
						}
					}
					*/
				}
				/*$email = $checkout[0]->email;
				Mail::send('webshop.orderemail',['checkout'=>$checkout, 'products'=>$products], function ($message) use ($email){
					$message->to($email);
					$message->subject('Order Summary');
				});
				*/
				return View::make('webshop/thankyou', array('page'=>'basket','checkout'=>$checkout, 'products'=>$products,'shipping'=>$shipping));
			}
			else{
				return Redirect::to('webshop');
			}
		}
		else{
			if(Auth::check()){
				Checkout::where('user_id',Auth::user()->id)->where('type', '=', 'pending')->update(array('type'=>'failed','transactionid'=>$transactionid));		
			}
			else{
				Checkout::where('guest_session',Session::getId())->where('type', '=', 'pending')->update(array('type'=>'failed','transactionid'=>$transactionid));
			}
			Session::flash('message-error','Sorry, Your order has been failed !!!');
			return View::make('webshop/thankyou', array('page'=>'basket'));
		}
	}	
	
	
	public function postVerifycheckout(){
		$errors=array();
		if(Auth::check()){
			$ship_id = Input::get('ship_id');
			if($ship_id !="0"){
				$shippingaddresses = Shippingaddress::where('user_id', '=', Auth::user()->id)->where('id', '=', $ship_id)->first();
				$rules = array(
					'name' => 'required',
					'surname' => 'required',
					'address' => 'required',
					'postalcode' => 'required',
					'city' => 'required',
					'telephone' => 'required',
					'alias' => 'required',
				);
				$data = [
					'name' => $shippingaddresses->name,
					'surname' => $shippingaddresses->surname,
					'address' => $shippingaddresses->address,
					'postalcode' => $shippingaddresses->postalcode,
					'city' => $shippingaddresses->city,
					'telephone' => $shippingaddresses->telephone,
					'alias' => $shippingaddresses->alias,
				];
				$validator = Validator::make($data,$rules);
				if($validator->fails()){
					$errors['errors']['ship']='Please fill out all required fields of shipping address.';
				
				}
			}
			$billingaddress = UserWeb::where('id', '=', Auth::user()->id)->first();
			$type=$billingaddress->type;
			if($type == 0){
				$rules = array(
					'firstname' => 'required',
					'lastname' => 'required',
					'address' => 'required',
					'postcode' => 'required',
					'phone' => 'required',
					'billing_email'=>'required|email'
				);
				$data = [
					'firstname' => $billingaddress->firstname,
					'lastname' => $billingaddress->lastname,
					'address' => $billingaddress->address,
					'postcode' => $billingaddress->postcode,
					'phone' => $billingaddress->phone,
					'billing_email' => $billingaddress->billing_email
				];
			}
			else{
				$rules = array(
					'firstname' => 'required',
					'lastname' => 'required',
					'address' => 'required',
					'postcode' => 'required',
					'comp_name' => 'required',
					'billing_email'=>'required|email',
					'vat' => 'required',
					'chamber_commerce' => 'required'
					
				);
				$data = [
					'firstname' => $billingaddress->firstname,
					'lastname' => $billingaddress->lastname,
					'address' => $billingaddress->address,
					'postcode' => $billingaddress->postcode,
					'comp_name' => $billingaddress->comp_name,
					'billing_email' => $billingaddress->billing_email,
					'vat' => $billingaddress->vat,
					'chamber_commerce' => $billingaddress->chamber_commerce
				];
			}
			$validator = Validator::make($data,$rules);
			if($validator->fails()){
				echo "<pre>";
				print_r($validator->messages());
				$errors['errors']['bill']='Please fill out all required fields of billing address.';
			}
		}
		else{
			$errors['errors']['common']='Something is wrong';
		}
		echo json_encode($errors);				
	}
	
	
	public function postVerifyguestcheckout(){
		$errors=array();
		$session = Session::getId();
		if(!(Auth::check())){
			$ship_id = Input::get('ship_id');
			if($ship_id !="0"){
				$shippingaddresses = Shippingaddress::where('guest_session', '=', $session)->where('id', '=', $ship_id)->first();
				$rules = array(
					'name' => 'required',
					'surname' => 'required',
					'address' => 'required',
					'postalcode' => 'required',
					'city' => 'required',
					'telephone' => 'required',
					'alias' => 'required',
				);
				$data = [
					'name' => $shippingaddresses->name,
					'surname' => $shippingaddresses->surname,
					'address' => $shippingaddresses->address,
					'postalcode' => $shippingaddresses->postalcode,
					'city' => $shippingaddresses->city,
					'telephone' => $shippingaddresses->telephone,
					'alias' => $shippingaddresses->alias,
				];
				$validator = Validator::make($data,$rules);
				if($validator->fails()){
					$errors['errors']['ship']='Please fill out all required fields of shipping address.';
				
				}
			}
			$billingaddress = GuestUser::where('guest_session', '=', $session)->first();
			$type=$billingaddress->type;
			if($type == 0){
				$rules = array(
					'firstname' => 'required',
					'lastname' => 'required',
					'address' => 'required',
					'postcode' => 'required',
					'phone' => 'required',
					'email'=>'required|email'
				);
				$data = [
					'firstname' => $billingaddress->firstname,
					'lastname' => $billingaddress->lastname,
					'address' => $billingaddress->address,
					'postcode' => $billingaddress->postcode,
					'phone' => $billingaddress->phone,
					'email' => $billingaddress->email
				];
			}
			else{
				$rules = array(
					'firstname' => 'required',
					'lastname' => 'required',
					'address' => 'required',
					'postcode' => 'required',
					'comp_name' => 'required',
					'email'=>'required|email',
					'vat' => 'required',
					'chamber_commerce' => 'required'
					
				);
				$data = [
					'firstname' => $billingaddress->firstname,
					'lastname' => $billingaddress->lastname,
					'address' => $billingaddress->address,
					'postcode' => $billingaddress->postcode,
					'comp_name' => $billingaddress->comp_name,
					'email' => $billingaddress->email,
					'vat' => $billingaddress->vat,
					'chamber_commerce' => $billingaddress->chamber_commerce
				];
			}
			$validator = Validator::make($data,$rules);
			if($validator->fails()){
				$errors['errors']['bill']='Please fill out all required fields of billing address.';
			}
		}
		else{
			$errors['errors']['common']='Something is wrong';
		}
		echo json_encode($errors);				
	}
	
	public function getUsercountry(){
		$country="";
		$ip=Request::getClientIp();
		$query = file_get_contents('http://www.geoplugin.net/json.gp?ip='.$ip);
		$query = json_decode($query,true);
		if(isset($query['geoplugin_countryName'])){
			$country=strtolower($query['geoplugin_countryName']);
		}
		return $country;
	}
	
	public function getOrderemail(){	
		if(Auth::check()){
			$checkout = Checkout::where('user_id',Auth::user()->id)->orderBy('id', 'desc')->limit(1)->get();
			if(count($checkout)>0){
				$products = CheckoutProSessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',$checkout[0]->id)->get();
			}
			else{
				$products =[];

			}	
		}
		else{
			$checkout = Checkout::where('guest_session',Session::getId())->orderBy('id', 'desc')->limit(1)->get();
			if(count($checkout)>0){
				$products = CheckoutProSessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',$checkout[0]->id)->get();
				
			}
			else{
				$products =[];
			
			}
		}
		return View::make('webshop/orderemail', array('checkout'=>$checkout, 'products'=>$products));		
	}	
	
	
	
	
}
