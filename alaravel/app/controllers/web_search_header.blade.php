<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Webshop - Global Property Cowboys</title>

			<!-- Bootstrap -->
			<link href="{{url('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet">
			<!-- Style -->
			<link href="{{url('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet">
			<link rel="stylesheet" href="{{url('assets/frontend/css/demo.css')}}" type="text/css" media="screen" />
			<link rel="stylesheet" href="{{url('assets/frontend/css/flexslider.css')}}" type="text/css" media="screen" />
			<link rel="stylesheet" type="text/css" href="{{url('assets/frontend/css/owl.carousel.min.css')}}">
			<!-- Responsive -->
			<link href="{{url('assets/frontend/css/responsive.css')}}" rel="stylesheet">
			<link rel="stylesheet" href="{{url('assets/frontend/css/font-awesome.min.css')}}">
			<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap" rel="stylesheet">
			<script type="text/javascript">
			window.Trengo = window.Trengo || {};
			window.Trengo.key = 'O4mF0u3h1Zd9qrt';
			(function(d, script, t) {
			    script = d.createElement('script');
			    script.type = 'text/javascript';
			    script.async = true;
			    script.src = 'https://static.widget.trengo.eu/embed.js';
			    d.getElementsByTagName('head')[0].appendChild(script);
			}(document));
			</script>
		</head>
		<body>
			<header class="header header_sec">
				<div class="container-fluid">

					<div class="col-md-3 col-sm-4">
						<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
			<a id="to-top" class="btn btn-lg btn-dark" href="#top">
				<span class="sr-only">Toggle to Top Navigation</span>
				<i class="fa fa-chevron-up"></i>
			</a>
			<nav id="sidebar-wrapper">
				<ul class="sidebar-nav">
					<a id="menu-close" href="#" class="btn btn-danger pull-right hidden-md hidden-lg toggle"><i class="fa fa-times"></i></a>

                    @if(isset($categories))
	                    @foreach($categories as $cat)
							<li>
								<a class="sidebar_heading" href="@if(isset($cat->subcategories[0])) {{url('/webshop/product')}}/{{$cat->slug}}/{{$cat->subcategories[0]->slug}} @else javascript:void(0); @endif">{{$cat->categoryname}}</a>
							</li>
						@endforeach
					@endif



					<p class="cvs">FREE HOME DELIVERY ON <br>PURCHASES OVER £29.99</p>
				</ul>

			</nav>
						<div class="logo">
							<a href="{{URL::to('/')}}"><img src="{{url('assets/frontend/images/logo.png')}}" class="img-responsive" alt="logo"></a>
							<span><a href="{{URL::to('/webshop')}}">Go back</a></span>
						</div>

					</div>


					<div class="col-md-4 col-sm-4 non_sec">
						<form class="form-inline cc">
							<div class="form-group">
								<label for="exampleInputName2">Search</label>
								<input id="searchh" type="text" class="form-control"  placeholder="">
							</div>
						</form>
						<div class="logo_res">
							<a href="{{url('webshop')}}"><img src="{{url('assets/frontend/images/logo.png')}}" class="img-responsive" alt="logo"></a>
						</div>
					</div>
					<div class="col-md-5 col-sm-4">
						<div class="log">
							<ul>
								<li class="account dropdown"><a class="btn btn-main dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">
									<span class="icon flaticon-man"></span>&ensp;
									<img src="{{url('assets/frontend/images/uk.png')}}"> &nbsp;<span class="fa fa-angle-down"></span></a>

									<ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                    <li><a href="#"><img src="{{url('assets/frontend/images/uk.png')}}"><span>English</span></a></li>
                                    <li><a href="#"><img src="{{url('assets/frontend/images/netherlands.jpg')}}"><span>Netherlands</span></a></li>
                                    <li><a href="#"><img src="{{url('assets/frontend/images/france.jpg')}}"><span>France</span></a></li>
                                    <li><a href="#"><img src="{{url('assets/frontend/images/spain.jpg')}}"><span>Spain</span></a></li>
                                </ul>
								</li>

								@if(Auth::check())
								<li class="wish_menu">

								<a href="{{url('customer/address')}}"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><span class="res_sec">{{Auth::user()->username}}</span></a>
								 <ul>
									<li><a href="{{url('checkout/logout')}}"><span class="sign"><i class="fa fa-sign-out" aria-hidden="true"></i></span>SIGN OUT</a>
									</li>
		                         </ul>
								</li>

								<li><a href="{{url('customer/wishlist')}}"><span><i class="fa fa-heart-o" aria-hidden="true"></i></span> <span class="res_sec">wishlist</span></a></li>




                                @else
                                    <?php
									$url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

                                     ?>
                                 @if(($url==url('/checkout/login')) || ($url== url('checkout/checkout')))
                                 @else
                                  @if(($url==url('/webshop/basket')) || ($url==url('/checkout/register')))

								<li>
									<span class="icn">
										<a href="{{url('checkout/login')}}">
										<i class="fa fa-user-o" aria-hidden="true"></i>
									    </a>
									</span>
									<span class="res_sec">
										    <a href="{{url('checkout/login')}}">LOGIN</a>
									</span>
								</li>
								@else
								<li>
									<span class="icn">
										<a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-user-o" aria-hidden="true"></i></a>
									</span>
									<span class="res_sec">
                                         <a href="#" data-toggle="modal" data-target="#myModal">LOGIN</a>
									</span>
								</li>
								@endif
								@endif
								@endif


                                <?php
								$q=0;
                                if(Auth::check())
                                {
                                   $products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
                                 }else{

                                   $products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->get();
                                 }
								 foreach($products as $prd){
								  $q=$q+$prd->quantity ;
							   }

                                  ?>


								<li>
									@if(isset($products[0]))
									<a href="{{url('webshop/basket')}}">
									<span class="icn"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="total_items"><?php if(isset($products)){echo $q;}else{ echo 0; } ?></span></span>
			                        </a>
			                        @else
			                        <a href="{{url('webshop/basket')}}">
									<span class="icn"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="total_items"><?php if(isset($products)){echo $q;}else{ echo 0; } ?></span></span>
			                        </a>
			                        @endif
			                        @if(isset($products[0]))
									<span class="res_sec">
										<a href="{{url('webshop/basket')}}" class="basketshow" id="basketshow" > BASKET (<span id="total_items"><?php if(isset($products)){echo $q;}else{ echo 0; } ?></span>)  </a><!-- <div id="mycart"></div> -->
									</span>
									@else
									<span class="res_sec">
										<a href="{{url('webshop/basket')}}" class="addId"  > BASKET (<span id="total_items"><?php if(isset($products)){echo $q;}else{ echo 0; } ?></span>)  </a><!-- <div id="mycart"></div> -->
									</span>
									 @endif
								</li>
							</ul>
						</div>
					</div>
				</div>
			</header>



     <!-- basket  -->

      <?php
        if(Auth::check())
        {

           $productsss = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
         }else{

           $productsss = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->get();
          // print '<pre>'; print_r($productsss); print '</pre>'; exit();
           //dd($productsss);
         }

      ?>

	   <aside class="slide_basket hideMe re"  id="hideMe">
			<div class="products-container">
				<i class="fa fa-close slide_basket_close"></i>
				<div id="productscroll">
				     @foreach($productsss as $session)
				    <div class="row">
				      	<div class="col-md-4 col-sm-4 basket_right"><img  src="{{url($session->img)}}" />
				      	 </div>

				      	    <div class="col-md-8 col-sm-8 basket_left">
				      	    	<div class="basket_res"><a href="#">{{$session->name}}</a></div>

                            <div class="basket_detailsec"><p>size</p><p>{{$session->quantity}} x {{Config::get('view.currency')}}{{$session->price}}</p></div>

				      </div>
				    </div>
				    @endforeach


                </div>
				<div class="standard_delivery">
					<p><span><i class="fa fa-truck" aria-hidden="true"></i>
                   </span>You will receive free standard delivery!</p>
				</div>

				<a href="{{url('/webshop/basket')}}" class="btn-process-order view-basket">View Basket</a>
			</div>
		</aside>
     <!-- basket end  -->




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	  <h4 class="modal-title" id="myModalLabel"></h4> </div>
      <div class="modal-body">
        <section class="contact-form contact_sec">
	       <div class="log_in df">
			  <div class="col-md-6 col-sm-6">
					<h4>Registered customers</h4>
					<p>We will ask you for the necessary information to speed up the purchase process.</p>
					@if (Session::get('message-success'))
					   <div class="alert alert-success">
			            {{Session::get('message-success')}}
                       </div>
					@endif
					@if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif

					  <div class="alert alert-success" id="success" style="display: none;">

                       </div>

						<div class="alert alert-danger error" style="display: none;">

                        </div>
					<form name="login_form"  id="loginform" class="add-form">
					   <div class="group">
					           <input class="form_input" type="text" name="username" id="username" required /><span class="highlight"></span><span class="bar"></span>
					           <h3 for="" class="loginusername" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Username or E-mail*</label>
					   </div>
					    <div class="group">
					        <input class="form_input" type="password"  name="password" id="password" required /><span class="highlight"></span><span class="bar"></span>
								@if ($errors->has('password'))
								<i class="error">{{ $errors->first('password') }}</i>
							    @endif
							    <h3 for="" class="loginpassword" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>

					   </div>
					   <a href="#" id="forgetModal" style="color:#585757;">Have you forgotten your password?</a>
					   <div class="sub_sec">
						   <div class="checkbox">
							<label class="con">
							   Do not end session
							  <input type="checkbox">
							  <span class="checkmark"></span>
							</label>
							</div>
						</div>
						<div class="submit_sec">
							<input class="buttons" type="submit" value="SUBMIT" id="add-form-btn">
						</div>
					</form>
					<div class="social_btns">
						<a href="#"><span><i class="fa fa-facebook-square" aria-hidden="true"></i></span>log in with facebook</a>
						<a class="fb "href="#"><span><i class="fa fa-google" aria-hidden="true"></i></span>log in with google</a>
					</div>
			  </div>
			  <div class="col-md-6 col-sm-6">
					<h4>new customers</h4>
					<div class="submit_sec">
							<input class="buttons" type="submit"  value="CREATE A NEW ACCOUNT" id="registerModal">
				    </div>
			  </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>


<!-- forget password -->

<div class="modal fade forgetModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	  <h4 class="modal-title" id="myModalLabel"></h4> </div>
      <div class="modal-body">
        <section class="contact-form contact_sec">
	     <div class="log_in df">
			  <div class="col-md-9 col-sm-9">
					<h4 class="center">RETRIEVE PASSWORD </h4>
                    <p>Please enter your email address and we will send you an email with the steps to follow.</p>
                       <div class="alert alert-success" id="success-forgot" style="display: none;">
                       </div>

						<div class="alert alert-danger error-forgot" style="display: none;">
                        </div>
					<form>
					    <div class="group">
								<input class="form_input" type="text" name="email" id="email_forgot"><span class="highlight"></span><span class="bar"></span>
								<label class="labletxt">Email*</label>
								<label for="" class="errorf" style="color: red;"></label>
					   </div>
					  <!--  <div class="group">
								<input class="form_input" type="text"><span class="highlight"></span><span class="bar"></span>
								<label class="labletxt">*Enter the code in the image below*</label>
					   </div>
					   <div class="captcha">
							<img src="images/captcha.png">
					   </div> -->
						<div class="submit_sec fgd">
							<input class="buttons" type="submit" value="SEND" id="add-form-btn-forgot">
						</div>
					</form>
			  </div>

    </div>
      </section>
      </div>
    </div>
  </div>
</div>
<!-- forget password end -->

<!-- registration modal -->
<div class="modal fade" id="myModal33" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	  <h4 class="modal-title" id="myModalLabel"></h4> </div>
      <div class="modal-body">
         <section class="contact-form contact_sec">
	        <div class="log_in df">

                       <div class="alert alert-success success_reg" style="display: none;">
                        </div>
					<?php 	 $sessionvar=	Session::get('data1'); ?>
			        <div class="col-md-8 col-sm-4  col-sm-offset-2">
					<h4 class="text-center"><span class="bck_sec"><a href="#" id="loginModal"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                     </span>CREATE YOUR GPC HOME ACCOUNT </h4>
					  <div class="radio_sec text-center">
					          <label class="radio-inline center cstm_radio"> <input type="radio" name="radio" id="radio-1" class="radio-content"  class="form_input used" checked="checked"  /><span class="checkmark"></span>Individual</label>

						     <label class="radio-inline center cstm_radio"><input type="radio" name="radio" id="radio-2" class="radio-content"  class="form_input used" <?php if(isset($sessionvar)){ echo 'checked=checked';}?>  /><span class="checkmark"></span>Company</label>
					   </div>
						@if (Session::get('name'))
						<?php
						 $sessiondata=	Session::get('name');
						?>
						 @endif
					<form class="create-form"  id="add-form-reg">

					    <div class="group">
								<input class="form_input" type="text" name="name"  id="name" value=""  required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorname" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">User Name*</label>
                                 <div class="md-errors-spacer"><div class="md-char-counter" style="text-align: right;">0 / 128</div></div>
					   </div>

					   <div class="group">
								<input class="form_input" type="text" name="email" id="eml"   value="" required="required">
								<h3 for="" class="erroremail" style="color: red; margin-top: 4px;"></h3>
								<span class="highlight"></span><span class="bar email"></span>
								<label class="labletxt">E-mail*</label>
					   </div>
					    <div class="group">
								<input class="form_input" type="password" name="password"  required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorpassword" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>
					   </div>

					   <div class="sub_sec">
						   <div class="checkbox">
							<label class="con">I have been able to read and understand the information on the use
							of my personal data explained in the <a style="color:#9c9494" href="{{url('assets/pdf/')}}/privacy-policy.pdf">Privacy policy</a>
							  <input type="checkbox" class="check-content form_input" value="1" name="policy">
							  <span class="checkmark"></span>
							   <h3 for="" class="errorpolicy" style="color: red; margin-top: 4px;"></h3>
							</label>
							</div>
							<div class="checkbox">
							<label class="con">I want to receive news and customised commercial communications from of
							GPC HOME via email and other means
							  <input type="checkbox" class="check-content form_input" name="receiveeml" value="1" required="required">
							  <span class="checkmark"></span>

							</label>
							</div>
						</div>
						<input type="hidden" name="individual" value="individual">

						<div class="submit_sec fgd">
							<input type="submit" name="signup" value="CREATE ACCOUNT" class="buttons create-button add-form-reg-btn">
						</div>
					</form>

					<?php
					 $sessionvar=	Session::get('data1');
					?>

					<form class="create-form" method="POST" action="{{url('webshop/cregister')}}" id="add-form-reg-com">

					    <div class="group">
								<input class="form_input" type="text" name="companyname" id="companyname" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<label class="labletxt">Company name*</label>
								<h3 for="" class="errorcompanyname" style="color: red; margin-top: 4px;"></h3>
                                 <div class="md-errors-spacer"><div class="md-char-counter" style="text-align: right;">0 / 128</div></div>
					   </div>
					   <div class="group">
								<input class="form_input" type="text" name="taxidcode" id="taxidcode" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errortaxidcode1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Tax ID Code (RFC)*</label>
					   </div>
					   <div class="group">
								<input class="form_input" type="text" name="name" id="firstname" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorname1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">First name*</label>
					   </div>
					   <div class="group">
								<input class="form_input" type="text" name="surname" id="lastname" value=""><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorsurname1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Surname*</label>
					   </div>
					   <div class="group">
								<input class="form_input" type="text" name="email" id="email" value=""><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="erroremail1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">E-mail*</label>
					   </div>
					    <div class="group">
								<input class="form_input" type="password" name="password"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorpassword1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>
					   </div>

					  <div class="sub_sec">
						   <div class="checkbox">
							<label class="con">I have been able to read and understand the information on the use
							of my personal data explained in the Privacy policy
							  <input type="checkbox" class="check-content form_input" value="1" name="policy">
							  <h3 for="" class="errorpolicy1" style="color: red; margin-top: 4px;"></h3>
							  <span class="checkmark"></span>
							</label>
							</div>
							<div class="checkbox">
							<label class="con">I want to receive news and customised commercial communications from of
							GPC HOME via email and other means
							  <input type="checkbox" class="check-content form_input" name="receiveeml" value="1">
							  <h3 for="" class="errorreceiveeml1" style="color: red; margin-top: 4px;"></h3>
							  <span class="checkmark"></span>
							</label>
							</div>
						</div>
						<input type="hidden" name="company" value="company">

						<div class="submit_sec fgd">
							<input type="submit" name="signup" value="CREATE ACCOUNT" class="buttons create-button add-form-reg-com-btn">
						</div>
					</form>
			  </div>

           </div>
        </section>
      </div>
    </div>
  </div>
</div>
<!-- registration modal end -->

 <!-- feedback

  ==============================-->
 <div class="feedback">
    <div class="feedback_content">
		<div class="feedback_cstm">
		    <a href="#" data-toggle="modal" data-target="#nm">Complaint</a>
		</div>
	</div>
 </div>
 <div class="modal fade bs-example-modal-sm" id="nm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <p>Do you have a complaint about us as an online store? Then we have this complaint
		dealt with by an independent organization you can contact Thuiswinkel.org! </p>
		<p>Click on the link below and follow the 3 steps</p>
		<a href="#">https://www.thuiswinkel.org/consumenten/klacht-indienen</a>
		<p>we still hope to see you back in our online store or website</p>
        <p>Team Global Property Cowboys</p>
    </div>
  </div>
</div>



 <!-- Hotjar Tracking Code for http://devlaravel.inviolateinfotech.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1407283,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>


<script src="{{url('assets/frontend/js/jquery.js')}}"></script>
<script type="text/javascript">
 $('#email_forgot').keyup(function() {
        if($(this).val()=='')
            //alert('checked');

        $('.errorf').html('This field is required');
        else
            //alert('unchecked');
        $('.errorf').html('');
    });


	/* for  */
$('#add-form-btn-forgot').on('click', function(e) {

 // $("#add-form-news").valid();
   e.preventDefault();
   var email_forgot = $('#email_forgot').val();

  // console.log(email_forgot);

      if(email_forgot==''){
            $('.errorf').html('This field is required');
      	     return false;
      }

 $.ajax({

	url:"{{url('/forgotpassword/forgotpassword')}}",
    type:"POST",
	cache:false,
	data:{
      email:email_forgot,
    },
	success: function(response){
		var res = JSON.parse(response);
		console.log(res.ForgotError);
		if(res.ForgotError)
		{
		console.log(res.ForgotError);
		$(".error-forgot").show();
		$(".error-forgot").html(res.ForgotError);
		$(".error-forgot").fadeIn('slow', function(){
        $(".error-forgot").delay(6000).fadeOut();
        $("#success-forgot").hide();
      });
		//$(".password").html(response.errors.password);
		// do what you want with errors,
		}
		else
		{
		$(".error-forgot").hide();
		$('#success-forgot').show();
		$('#success-forgot').html(res.ForgotSuccess);
		$("#success-forgot").fadeIn('slow', function(){
        $("#success-forgot").delay(6000).fadeOut();
        });
		//location.reload();


	    }
	}
  });

});
</script>
<script type="text/javascript">

$( ".basketshow" )
  .mouseover(function(event ) {
  	event.preventDefault();
   //ert('sadasd');
    $('.slide_basket').addClass("ds");
    $('.slide_basket').removeClass("re");
  });

  $( ".basketshow" )
  .mouseup(function(event) {
   event.preventDefault();
    $('.slide_basket').addClass("re");
    $('.slide_basket').removeClass("ds");
  });

 $(document).mouseup(function (e) {
            if ($(e.target).closest(".hideMe").length
                        === 0) {
                $(".hideMe").hide();
                $('.slide_basket').removeClass("ds");
               // $('.slide_basket').addClass("re");
            }
        });


</script>
<script type="text/javascript">
var base_url = '{{url("/")}}';

$( document ).ready(function() {
$('#forgetModal').on('click', function(e) {
 e.preventDefault();

 $('#myModal').modal('hide');
 $('.forgetModal').modal('show');

});

$('#registerModal').on('click', function(e) {
 e.preventDefault();

 $('#myModal').modal('hide');
 $('#myModal33').modal('show');

});

$('#loginModal').on('click', function(e) {
 e.preventDefault();

 $('#myModal33').modal('hide');
 $('#myModal').modal('show');

});

/* for login */
$('#add-form-btn').on('click', function(e) {

 var username = $('#username').val();
 var password = $('#password').val();
 e.preventDefault();


 $.ajax({

	url:base_url+'/webshop/login',
    type:"POST",
	cache:false,
	data:{
      username:username,
      password:password
    },
	success: function(response){
		var res = JSON.parse(response);

	 if(res.errors)
	    {

    	  if(res.errors.password){
           $(".loginpassword").html(res.errors.password);
           $(".loginpassword").fadeIn('slow', function(){
           $(".loginpassword").delay(3000).fadeOut();
	      });
	       }else{

	       	$(".loginpassword").html('');
	       }

	       if(res.errors.username){
           $(".loginusername").html('The username or email field is required.');
           $(".loginusername").fadeIn('slow', function(){
           $(".loginusername").delay(3000).fadeOut();
	      });
	       }else{

	       	$(".loginusername").html('');
	       }

	  }else{

		if(res.Error)
		{
		console.log(res.Error);
		$(".error").html(res.Error);
		$(".error").show();
		$(".error").fadeIn('slow', function(){
        $(".error").delay(6000).fadeOut();
        });
		//$(".password").html(response.errors.password);
		// do what you want with errors,
		}
		else
		{
		$(".error").hide();
		$('#success').html(res.MESSAGE);
		$('#success').show();
		location.reload();


	    }
	  }
	}
  });

});
/* for login end  */


/* for register */
$('.add-form-reg-btn').on('click', function(e) {

 e.preventDefault();
  var data = $('#add-form-reg').serializeArray();

 $.ajax({
	data:data,
	url:base_url+'/webshop/register',
    type:"POST",
	cache:false,

	success: function(response){
		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {

    	  if(res.errors.email){
           $(".erroremail").html(res.errors.email);
           $(".erroremail").fadeIn('slow', function(){
           $(".erroremail").delay(3000).fadeOut();
	      });
	       }else{

	       	$(".erroremail").html('');
	       }

	      if(res.errors.username){
	      $(".errorname").html(res.errors.username);
	      $(".errorname").fadeIn('slow', function(){
	        $(".errorname").delay(3000).fadeOut();
	      });
	      }else{

	       	$(".errorname").html('');
	       }
          if(res.errors.password){
	      $(".errorpassword").html(res.errors.password);
	      $(".errorpassword").fadeIn('slow', function(){
	        $(".errorpassword").delay(3000).fadeOut();
	      });
	      }else{

	       	$(".errorpassword").html('');
	       }
          if(res.errors.policy){
	      $(".errorpolicy").html('You must accept the privacy policy');
	      $(".errorpolicy").fadeIn('slow', function(){
	        $(".errorpolicy").delay(3000).fadeOut();
	      });
	      }else{

	       	$(".errorpolicy").html('');
	       }

	      var success = $(".erroremail");
	      scrollTo(success,-100);
	    }
	    else
	    {

		$('.success_reg').html(res.Success);
		$(".success_reg").fadeIn('slow', function(){
	        $(".success_reg").delay(3000).fadeOut();
	      });
		$('.success_reg').show();
		$('#add-form-reg')[0].reset();
		//location.reload();


	    }
	}
  });

});

/* for register end */

/* for company register */
$('.add-form-reg-com-btn').on('click', function(e) {

 e.preventDefault();
  var data = $('#add-form-reg-com').serializeArray();


 $.ajax({

	data:data,
	url:base_url+'/webshop/cregister',
    type:"POST",
	cache:false,

	success: function(response){
		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {
	      if(res.errors.email){
	        $(".erroremail1").html(res.errors.email);
	        $(".erroremail1").fadeIn('slow', function(){
	        $(".erroremail1").delay(3000).fadeOut();
	      });
	       }else{

	       	$(".erroremail1").html('');
	       }

	       if(res.errors.username){
		      $(".errorname1").html(res.errors.username);
		      $(".errorname1").fadeIn('slow', function(){
		        $(".errorname1").delay(3000).fadeOut();
		      });
	        }else{

	       	 $(".errorname1").html('');
	        }
          if(res.errors.password){
		      $(".errorpassword1").html(res.errors.password);
		      $(".errorpassword1").fadeIn('slow', function(){
		        $(".errorpassword1").delay(3000).fadeOut();
		      });
	      }else{

	       	 $(".errorpassword1").html('');
	        }
          if(res.errors.surname){
	      $(".errorsurname1").html(res.errors.surname);
	      $(".errorsurname1").fadeIn('slow', function(){
	        $(".errorsurname1").delay(3000).fadeOut();
	      });
	      }else{

	       	 $(".errorsurname1").html('');
	        }
          if(res.errors.companyname){
	      $(".errorcompanyname").html(res.errors.companyname);
	      $(".errorcompanyname").fadeIn('slow', function(){
	        $(".errorcompanyname").delay(3000).fadeOut();
	      });
	      }else{

	       	 $(".errorcompanyname").html('');
	        }
           if(res.errors.taxidcode){
	      $(".errortaxidcode1").html(res.errors.taxidcode);
	      $(".errortaxidcode1").fadeIn('slow', function(){
	        $(".errortaxidcode1").delay(3000).fadeOut();
	      });
	      }else{

	       	 $(".errortaxidcode1").html('');
	        }
          if(res.errors.policy){
	      $(".errorpolicy1").html('You must accept the privacy policy');
	      $(".errorpolicy1").fadeIn('slow', function(){
	        $(".errorpolicy1").delay(3000).fadeOut();
	      });
	      }else{

	       	 $(".errorpolicy1").html('');
	        }



	      var success = $(".erroremail1");
	      scrollTo(success,-100);
	    }
	    else
	    {

		$('.success_reg').html(res.Success);
		$(".success_reg").fadeIn('slow', function(){
	        $(".success_reg").delay(3000).fadeOut();
	      });
		$('.success_reg').show();
		$('#add-form-reg-com')[0].reset();
		//location.reload();


	    }
	}
  });

});

/* for company register end */

});
</script>

<script>
$(document).ready(function(){
	var name=$("#name").val();
	if(name!=''){
	$("#name").focus();
	}
	var eml=$("#eml").val();
	if(eml!=''){
	$("#eml").focus();
	}
	var companyname=$("#companyname").val();
	var taxidcode=$("taxidcode").val();
	var firstname=$("#firstname").val();
	var lastname=$("#lastname").val();
	var email=$("#email").val();
	if(email!=''){
		$("#email").focus();
	}
	if(firstname!=''){
		$("#firstname").focus();
	}
	if(lastname!=''){
		$("#lastname").focus();
	}
	if(companyname!=''){

		$("#companyname").focus();
	}
	if(taxidcode!=''){
		$("#taxidcode").focus();
	}
	 $("#radio-1").click(function(){
		 $("#add-form-reg").show();
		 $("#add-form-reg-com").hide();
	 });
	  $("#radio-2").click(function(){
		 $("#add-form-reg-com").show();
		 $("#add-form-reg").hide();
	 });
	if($("input[type='radio']#radio-1").is(':checked')) {
		$("#add-form-reg").show();
	 $("#add-form-reg-com").hide();

	}else{
		 $("#add-form-reg-com").show();
		 $("#add-form-reg").hide();

	}




});

</script>

<script type="text/javascript">
	$('#searchh').on('keyup',function(){
		$value=$(this).val();
		console.log($value);
		$.ajax({
		type : 'post',
		url:base_url+'/webshop/search',
		data:{'search':$value},
		success:function(response){
			var res = JSON.parse(response);
			$('tbody').html(response);
			console.log(res);
			}
		});
	})
</script>
