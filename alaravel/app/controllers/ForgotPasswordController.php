<?php

//define('Mandrill_PATH', base_path().'/vendor/Mandrill');

require_once Mandrill_PATH.'/Mandrill.php';

class ForgotPasswordController extends BaseController
{
    
	

	public function getForgotpassword(){
		
		$category=new Parentcategory;
		$c = Parentcategory::all();
		$_cat =[];
		foreach($c as $parentcategory){
		$parentid=	$parentcategory['id'];
		$category = new Category;
		$catg=Category::select('*')->where('parentid','=',$parentid)->get();
		$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
			foreach($catg as $val){
				 $_cat[$parentcategory['id']]['child'][]=	$val;
			
				 
			}
			$banner=new Banner;
			$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['getbanner']=$getbanners;
		}
		return View::make('webshop/inner/forgotpassword')->with('catdata',$_cat);
	}
	
	public function postForgotpassword()
	{
	  $email = Input::get('email');
	   $user = WebshopUser::where('email',$email)->first();
	  // dd($user);
	   
	   if($user){
		  
            $code = rand(999, 99999);
            $user->code = $code;
            $user->save();



		try {

		$url = url('forgotpassword/resetpassword/'.$user->email.'/'.urlencode($code));	

		echo $html = '<h2>Reset Your Password</h2><p>To change your password,<a href="'.$url.'">click here.</a></p><p>Or point your browser to this address: <br /> '.$url.' </p><p>Thank you!</p>';	
die;
		$mandrill = new Mandrill('8QLJqvtkRO2ZXkGdcEOWwA');
		$message = array(
		    'html' => $html,
		    'subject' => "Hello ".$user->username.", reset your password",
		    'from_email' => 'test@inviolateinfotech.com',
		    'from_name' => 'globalpropertycowboys',
		    'to' => array(
		        array(
		           // 'email' => 'ak.php.develop@gmail.com',
		            'email' => $user->email,
		            //'email' => 'shahnoman1001@gmail.com',
		            'name' =>  $user->username,
		            'type' => 'to'
		        )
		    )
		);
		$result = $mandrill->messages->send($message);
		//print_r($result); exit();
		 $resultSet['ForgotSuccess'] = 'Account password reset successfully. Please check your email account and follow the instruction!';
         echo json_encode($resultSet);

		} catch(Mandrill_Error $e) {
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
		}

			//$this->sendEmail($user,$code);

			//return Redirect::back()->with('message-success', 'Account password reset successfully. Please check your email account and follow the instruction!');
		
	 			
	    }else
	    {
	        $resultSet['ForgotError'] = 'This email address does not exist!';
                     echo json_encode($resultSet); 
	         //return Redirect::back()->with('message-error', 'This email address does not exist!');
	
	     }
	}
	
	
	public function resetpassword($email, $code)
	{  

	  
	   $user = WebshopUser::where('email',$email)->first();
	   
	   
	   if(count($user)==0){

	     abort(404);
		 
		}
		else
		{
//dd($email);
			if($code == $user->code){

		      return View::make('webshop/inner/resetPassword')->with('email',$email)->with('code',$code);
			}
			
			else{
				
			   return 'Invalid or expired password reset code; please request a new link.';
				
			} 
	    }
	
	}
	
	
	
	
	public function postResetpassword()
	{
	   
	   $validator = Validator::make(Input::all(), array(
			'password'=> 'confirmed|required|min:6',
			'password_confirmation'=> 'required|min:6',
		
			)
		);
	   if($validator->passes()){
			  $email = Input::get('email');

			  $code = Input::get('code');
			  $password = Input::get('password');
	          $user = WebshopUser::where('email',$email)->first();
			   if(count($user)>0){
					
					if($code == $user->code){

						$user->password =  Hash::make($password);
						$user->save();	
						return Redirect::to('/checkout/login')->with('message-success', 'Please login with your new password!');

					}else
					{ 
					 return Redirect::to('/checkout/login')->with('message-error', 'Code does not match!');
					
					}
				}else{
						
					return Redirect::to('/checkout/login')->with('message-error', 'E-mail not found!');
						
				} 

		}else{

		  return Redirect::back()->with("message-failure", "Validation error!")->withInput();

		}		
	   
	
	}
	
	
	
	
	
	private function sendEmail($user, $resetcode)
	{
      
      
      //dd($resetcode);
	  Mail::send('emails.forgotPassword', [
	  
	  'user' => $user,
	  'code' => $resetcode
	  ], function($message) use ($user){
		  
		  $message->to($user->email);
		  $message->subject("Hello $user->username, reset your password");
		  
		  });
      	
	
	 
	}
	
	
	
	
}
