<?php
define('MailChimp_PATH', base_path().'/vendor/mailchimp');
require_once MailChimp_PATH.'/MailChimp.php';
use \DrewM\MailChimp\MailChimp;


class CustomerController extends BaseController {
    
	public function getInformation(){
		$countries = Shipcountriesnames::orderBy('countries_name', 'asc')->get();
		$allcountrues=array();
		foreach($countries as $k=>$ct){
			$allcountrues[$ct->countries_name]=$ct->countries_name;
		}
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
				$parentid=	$parentcategory['id'];
				$category = new Category;
				$catg=Category::select('*')->where('parentid','=',$parentid)->get();
				$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
				
					 
				}
				$banner=new Banner;
				$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
				$_cat[$parentcategory['id']]['getbanner']=$getbanners;
			}
			$checkout = null;
			if(Auth::check()){
				$checkout = Checkout::where('user_id', '=', Auth::user()->id)->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			else{
				$checkout = Checkout::where('guest_session', '=', Session::getId())->where('type', '=', 'initiated')->orderBy('id', 'desc')->limit(1)->get();
			}
			if(isset($checkout[0])){
				$checkout = $checkout[0];
			}
			$shipping = UserWeb::where('id', '=', Auth::user()->id)->first();
			return View::make('webshop/inner/information')->with('catdata',$_cat)->with('shipping',$shipping)->with('allcountrues',$allcountrues)->with('checkout',$checkout);
		}
		else{
			return Redirect::to('webshop');
		}	
	}

	public function postChangepassword() 
	{
	    $user = Auth::user();
	    $rules = array(
	        'current_password' => 'required',
	        'password' => 'required|confirmed'
	    );

        $data = [
            'current_password' => trim(Input::get('current_password')),
            'password' => trim(Input::get('password')),
            'password_confirmation' => trim(Input::get('password_confirmation')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    //return  response()->json(['errors'=>$validator->errors()]);
	    }else
	    {
	        if (!Hash::check(Input::get('current_password'), $user->password)) 
	        {
	        	$resultSet['Error'] = 'Your current password does not match';
                echo json_encode($resultSet);
	        }
	        else
	        {
	            $user->password = Hash::make(Input::get('password'));
	            $user->save();
	            $resultSet['Success'] = 'Password have been changed Successfully';
                echo json_encode($resultSet);
	        }
	    }
	}

	public function postChangeemail() 
	{
	    $user = Auth::user();
	    $rules = array(
	        'email' => 'required|confirmed',
	        'password' => 'required'
	    );

        $data = [
            'email' => trim(Input::get('email')),
            'email_confirmation' => trim(Input::get('email_confirmation')),
            'password' => trim(Input::get('password')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails())
	    {
	    	echo json_encode(['errors'=>$validator->messages()]);
	    //return  response()->json(['errors'=>$validator->errors()]);
	    }else
	    {
	        if (!Hash::check(Input::get('password'), $user->password)) 
	        {
	        	$resultSet['Error'] = 'Your password does not match';
                echo json_encode($resultSet);
	        }
	        else
	        {
				//update snel customer email
				$snel_id = $user->snel_id;
				$email = Input::get('email');
				$username = $user->username;
				$this->getRelationupdate($snel_id,$email,$username);
				
	            $user->email = $email;
	            $user->save();
	            $resultSet['Success'] = 'E-mail have been changed Successfully';
                echo json_encode($resultSet);
	        }
	    }
	}


	public function getAddress(){
		$countries = Shipcountriesnames::orderBy('countries_name', 'asc')->get();
		$allcountrues=array();
		foreach($countries as $k=>$ct){
			$allcountrues[$ct->countries_name]=$ct->countries_name;
		}
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
			$parentid=	$parentcategory['id'];
			$category = new Category;
			$catg=Category::select('*')->where('parentid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
				
					 
				}
				$banner=new Banner;
				$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
				$_cat[$parentcategory['id']]['getbanner']=$getbanners;
			}

			$shipping = Shippingaddress::where('user_id', '=', Auth::user()->id)->get();
			return View::make('webshop/inner/address')->with('catdata',$_cat)->with('shipping',$shipping)->with('allcountrues',$allcountrues);

		}else{

             return Redirect::to('webshop');
		}	
	}


	public function postAddress(){
		$validator = Validator::make(Input::all(), array(
			'firstname'=> 'required',
			'lastname'=> 'required',
			'address'=> 'required',
			'billing_email'=> 'required|email',
			'postcode'=> 'required',
			)
		);
		if($validator->passes()){
			$type= Input::get('radio');
			$name = Input::get('firstname');
			$surname = Input::get('lastname');
			$email = Input::get('billing_email');
			$billing_address = Input::get('address');
			$floor_door = Input::get('floor');
			$nr = Input::get('nr');
			$country = Input::get('country');
			$billing_postalcode = Input::get('postcode');
			$billing_city = Input::get('city');
			$telephone = Input::get('phone');
			$vat = Input::get('vat');
			$chamberaddress = Input::get('chamber_commerce');
			$compname = Input::get('comp_name');
			
			
			$shipping = UserWeb::where('id', '=', Auth::user()->id)->first();
			$shipping->type = $type;
			$shipping->firstname = $name;
			$shipping->lastname = $surname;
			$shipping->billing_email = $email;
			$shipping->nr = $nr;
			$shipping->country = $country;
			$shipping->floor = $floor_door;
			$shipping->phone = $telephone;
			$shipping->address = $billing_address;
			$shipping->city = $billing_city;
			$shipping->postcode = $billing_postalcode;
			$shipping->chamber_commerce = $chamberaddress;
			$shipping->vat = $vat;
			$shipping->comp_name = $compname;
			$shipping->save();
			return Redirect::back()->with("message-success", "Data Saved!")->withInput();
		}
		return Redirect::back()->with("message-failure", "Please provide all the required information!")->withInput();
	}




	public function postMyaddress(){
		$rules = array(
			'name' => 'required',
			'surname' => 'required',
			'address' => 'required',
			'postalcode' => 'required',
			'city' => 'required',
			'telephone' => 'required',
			'alias' => 'required',
		);

		$data = [
			'name' => trim(Input::get('name')),
			'surname' => trim(Input::get('surname')),
			'address' => trim(Input::get('address')),
			'postalcode' => trim(Input::get('postalcode')),
			'city' => trim(Input::get('city')),
			'telephone' => trim(Input::get('telephone')),
			'alias' => trim(Input::get('alias')),
		];
		$validator = Validator::make($data,$rules);
	 
		if($validator->fails()){
			echo json_encode(['errors'=>$validator->messages()]);
		
		}
		else{
			$id = Input::get('id');
			$name = Input::get('name');
			$surname = Input::get('surname');
			$address = Input::get('address');
			$postalcode = Input::get('postalcode');
			$city=Input::get('city');
			$country=Input::get('country');
			$telephone=Input::get('telephone');
			$alias=Input::get('alias');
			$nr=Input::get('nr');
			$floor_door=Input::get('floor_door');
			$user_id=Auth::user()->id;
			$user = Shippingaddress::where('id', '=', $id)->where('user_id', '=', Auth::user()->id)->first();
			$user->name = $name;
			$user->surname = $surname;
			$user->address = $address;
			$user->postalcode =  $postalcode;
			$user->city=$city;
			$user->country=$country;
			$user->telephone=$telephone;
			$user->alias=$alias;
			$user->nr=$nr;
			$user->floor_door=$floor_door;
			$user->user_id=$user_id;
			$user->save();	
			$resultSet['Success'] = 'Shipping Address updated Successfully';
			echo json_encode($resultSet);
		}
	}


	public function getNotification(){
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
			$parentid=	$parentcategory['id'];
			$category = new Category;
			$catg=Category::select('*')->where('parentid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
				
				}
				$banner=new Banner;
				$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
				$_cat[$parentcategory['id']]['getbanner']=$getbanners;
			}
			return View::make('webshop/inner/notification')->with('catdata',$_cat);
		}
		else{
			return Redirect::to('webshop');
		}	
	}

	public function getWishlist(){
		if(Auth::check()){
			$sel_wish_id="";
			$wish_selected=array();
			if(isset($_GET['id'])){
				$wish_selected = Wishlist::where('id', '=', trim($_GET['id']))->where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
				if(!isset($wish_selected[0])){
					return Redirect::to('/customer/wishlist');
				}
				else{
					$sel_wish_id=trim($_GET['id']);
				}
			}
			
			$wishlists = Wishlist::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->get();
			if($sel_wish_id==""){
				if(isset($wishlists[0])){
					$sel_wish_id=$wishlists[0]->id;
				}
			}
			
			foreach($wishlists as $k=>$wish){
				$ct=Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $wish->id)->orderBy('id', 'desc')->count();
				$wishlists[$k]->count=$ct;
			}
			$wishl=array();
			$wish_prod_detail=array();
			if($sel_wish_id !=""){
				$wishl = Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('wish_list_id', '=', $sel_wish_id)->orderBy('id', 'desc')->get();
				foreach($wishl as $k=>$wish_id){
					$wish_prod_detail  = BigProduct::where('id','=',$wish_id->product_id)->orderBy('id', 'asc')->first();
					$wishl[$k]->products=$wish_prod_detail;
					$pr_images = Productsimages::select('img_name')->where('product_id', '=', $wish_id->product_id)->orderBy('id', 'asc')->first();
					$wishl[$k]->pr_images=$pr_images ;
					if($wish_id->product_variation_id !=0){
						$prvars=ProductVariations::where('id', '=', $wish_id->product_variation_id)->where('product_id', '=', $wish_id->product_id)->first();
						if(isset($prvars->variation_id)){
							$vproduct=Variations::select('id','attributes_ids')->where('id', '=', $prvars->variation_id)->where('product_id', '=', $wish_id->product_id)->get();
							if(isset($vproduct[0])){
								$atts_name=array();
								$all_attributes=array();
								foreach($vproduct as $pvar){
									$atts=explode(",",$pvar->attributes_ids);
									foreach($atts as $at){
										$all_attributes[$at]=$at;
									}
								}
								foreach($all_attributes as $ats){
									$att_details=Attribute::select('name')->where('sup_attribute_id', $ats)->orderBy('id', 'asc')->first();
									if(isset($att_details->name)){
										$atts_name[]=$att_details->name;
									}
								}
								if(!empty($atts_name)){
									$atts=implode(",",$atts_name);
								}
								$prvars->atts=$atts;
							}
						}
						$wishl[$k]->prvars=$prvars ;
					}
				}
			}
			if($sel_wish_id==""){
				$sel_wish_id=0;
			}
			
			return View::make('webshop/inner/wishlist')->with('wishlists',$wishlists)->with('wish_selected',$wish_selected)->with('wish_prod_detail',$wishl)->with('sel_wish_id',$sel_wish_id);
		}
		else{
			return Redirect::to('webshop');
		}
	}

	public function getOrder(){
		if(Auth::check()){
			$orders = Checkout::where('user_id','=',Auth::user()->id)->where('type', '!=', 'initiated')->orderBy('id','desc')->get();
			return View::make('webshop/inner/order')->with('orders',$orders);
		}
		else{
			return Redirect::to('webshop');
		}	
	}


	public function orderdetail($id){
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
			$parentid=	$parentcategory['id'];
			$category = new Category;
			$catg=Category::select('*')->where('parentid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
				
					 
				}
				$banner=new Banner;
				$getbanners=Banner::select('*')->where('catid','=',$parentid)->get();
				$_cat[$parentcategory['id']]['getbanner']=$getbanners;
			}

			$order = Checkout::where('id','=',$id)->where('user_id', '=', Auth::user()->id)->orderBy('id', 'asc')->first();
			if($order) {
				$products = CheckoutProSessions::where('checkout_id', '=',$order->id)->where('user_id', '=', Auth::user()->id)->get();
			}
			else{
				return Redirect::to('/customer/information');
			}
			
			return View::make('webshop/inner/orderdetail')->with('catdata',$_cat)->with('order',$order)->with('products',$products);

		}else{

             return Redirect::to('webshop');
		}	
	}
	

	public function postSubscribenews(){
        
		// dd(Input::all());
       
       $validator = Validator::make(Input::all(), array(
			'email'=> 'required',
			'privacy_policy'=> 'required',
			)
		);

		if($validator->passes()){
			
				
				$email = Input::get('email');
				$privacy_policy = Input::get('privacy_policy');

				$newsletter = Newsletter::where('slug', '=', 'user-subscription')->get();
		
				if(isset($newsletter[0])){
					$newsletter = $newsletter[0];
					$data = new Newsletterlist;
					$data->newsletter_id = $newsletter->id;
					$data->email = Input::get('email');
					$data->save();
				}
				

			    $MailChimp = new MailChimp('1c6c8a83195c12fe9f42831c9dbb2b78-us20');
				$result = $MailChimp->get('lists');
				//echo "<pre>";
				//print_r($result);

				$list_id = '63cfb27cff';

				$result = $MailChimp->post("lists/$list_id/members", [
								'email_address' => $email,
								'status'        => 'subscribed',
							]);

				

				if((isset($result['title'])) && ($result['title']=='Member Exists')){
					//print_r($result['title']);
					$resultSet['Error'] = 'Email is already a list member.Please use other mail!';
                     echo json_encode($resultSet);
				}else{
				$resultSet['Success'] = 'Thanks for Subscribing.You are Successfully subscribe!';
				echo json_encode($resultSet);
			    }

		}else{

			$resultSet['Error'] = 'Please provide all the required information!';
			echo json_encode($resultSet);

		   //return Redirect::back()->with("message-failure", "Please provide all the required information!")->withInput();

		}

	}
	
	
	

	public function getCards(){
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
			$parentid=	$parentcategory['id'];
			$category = new Category;
			$catg=Category::select('*')->where('parentid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
				
					 
				}
				
			}

			
	       // dd($shipping);
			return View::make('webshop/inner/cards');

		}else{

             return Redirect::to('webshop');
		}	
	}
	
	public function getDeladd(){
		
		
		if(Auth::check()){
			$category=new Parentcategory;
			$c = Parentcategory::all();
			$_cat =[];
			foreach($c as $parentcategory){
			$parentid=	$parentcategory['id'];
			$category = new Category;
			$catg=Category::select('*')->where('parentid','=',$parentid)->get();
			$_cat[$parentcategory['id']]['parentcategory']=	$parentcategory;
				foreach($catg as $val){
					 $_cat[$parentcategory['id']]['child'][]=	$val;
	 
				}
				
				
			}
			 Auth::user()->id;
	       $affectedRows = Shipping::where('user_id', '=',  Auth::user()->id)->delete();
		   return Redirect::back();
			

		}else{

             return Redirect::to('webshop');
		}	
	}
	
	
	public function getNewslett(){
		if(Auth::check()){
			$email = Auth::user()->email;
			$MailChimp = new MailChimp('1c6c8a83195c12fe9f42831c9dbb2b78-us20');
			$list_id = '63cfb27cff';
			$result = $MailChimp->post("lists/$list_id/members", [
				'email_address' => $email,
				'status'        => 'subscribed',
			]);
			if($result){
				$shipping = UserWeb::where('id', '=', Auth::user()->id)->first();
				$shipping->receieveemail = '1';
				$shipping->save();
				return Redirect::back()->with("message-success", "Subscribed Successfully!")->withInput();
			}
			
		}
		else{
			return Redirect::to('webshop');
		}	
	}
	
	public function getNewslettun(){
		if(Auth::check()){
			$shipping = UserWeb::where('id', '=', Auth::user()->id)->first();
			$shipping->receieveemail = '0';
			$shipping->save();
			return Redirect::back()->with("message-success", "Unsubscribed Successfully!")->withInput();
		}
		else{
			return Redirect::to('webshop');
		}	
	}
	
	public function postShippingadd(){
		$rules = array(
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
			'telephone' => 'required',
			'alias' => 'required',
        );

        $data = [
            'name' => trim(Input::get('name')),
            'surname' => trim(Input::get('surname')),
            'address' => trim(Input::get('address')),
            'postalcode' => trim(Input::get('postalcode')),
            'city' => trim(Input::get('city')),
			'telephone' => trim(Input::get('telephone')),
            'alias' => trim(Input::get('alias')),
            
            ];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
	    
	    }
		else{
			$name = Input::get('name');
			$surname = Input::get('surname');
			$address = Input::get('address');
			$postalcode = Input::get('postalcode');
			$city=Input::get('city');
			$country=Input::get('country');
			$telephone=Input::get('telephone');
			$alias=Input::get('alias');
			$nr=Input::get('nr');
			$floor_door=Input::get('floor_door');
			$user_id=Auth::user()->id;
			$user = new Shippingaddress;
			$user->name = $name;
			$user->surname = $surname;
			$user->address = $address;
			$user->postalcode =  $postalcode;
			$user->city=$city;
			$user->country=$country;
			$user->telephone=$telephone;
			$user->alias=$alias;
			$user->nr=$nr;
			$user->floor_door=$floor_door;
			$user->user_id=$user_id;
			$user->save();	
			$resultSet['Success'] = 'Shipping Address added Successfully';
			echo json_encode($resultSet);
	    }				
	}
	
	public function postShippingremove(){
		$id = Input::get('id');
		Shippingaddress::where('user_id', '=',  Auth::user()->id)->where('id', '=',  $id)->delete();
		$resultSet['Success'] = 'Shipping Address deleted Successfully';
		echo json_encode($resultSet);
	}
	
	public function postWishlistremove(){
		$id = Input::get('id');
		Wishlist::where('user_id', '=',  Auth::user()->id)->where('id', '=',  $id)->delete();
		$resultSet['Success'] = 'Wishlist deleted Successfully';
		echo json_encode($resultSet);
	}
	
	public function postPrwishlistremove(){
		$id = Input::get('id');
		Wishlistproduct::where('user_id', '=',  Auth::user()->id)->where('id', '=',  $id)->delete();
		$resultSet['Success'] = 'Wishlist deleted Successfully';
		echo json_encode($resultSet);
	}
	
	public function postWishadd(){
		$rules = array(
            'list_name' => 'required'
        );
		$data = [
            'list_name' => trim(Input::get('list_name'))
		];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			$name = Input::get('list_name');
			$user_id=Auth::user()->id;
			$wish_list = new Wishlist;
			$wish_list->user_id =$user_id;
			$wish_list->list_name = $name ;
			$wish_list->save();	
			$resultSet['Success'] = 'Shipping Address added Successfully';
			echo json_encode($resultSet);
		}				
	}
	
	public function postWishlistupdate(){
		
		$id = Input::get('id');
		$rules = array(
            'list_name' => 'required'
        );
		$data = [
            'list_name' => trim(Input::get('list_name_update'))
		];
        $validator = Validator::make($data,$rules);
     
	    if($validator->fails()){
	    	echo json_encode(['errors'=>$validator->messages()]);
		}
		else{
			
			$name = Input::get('list_name_update');
			$wish_list = Wishlist::where('id', '=', $id)->where('user_id', '=', Auth::user()->id)->first();
			$wish_list->list_name = $name;
			$wish_list->save();	
			$resultSet['Success'] = 'Shipping Address added Successfully';
			echo json_encode($resultSet);
		}				
	}
	
	public function postWishaddtocart(){
		$id = Input::get('id');
		$wishproduct = Wishlistproduct::where('id', '=', $id)->get();
		if(!empty($wishproduct[0])){
			$quantity=$wishproduct[0]->quantity;
			$atts="";
			$prvariation_id = $wishproduct[0]->product_variation_id;
			$product = BigProduct::where('id', '=', $wishproduct[0]->product_id)->where('active', '=', 1)->get();
			if(isset($product[0])){
				if($prvariation_id != 0){
					$prvars=ProductVariations::select('variation_id','retailprice')->where('id', '=', $prvariation_id)->first();
					if(isset($prvars->variation_id)){
						$product[0]->retailprice=$prvars->retailprice;
						$vproduct=Variations::select('id','attributes_ids')->where('id', '=', $prvars->variation_id)->where('product_id', '=', $wishproduct[0]->product_id)->get();
						if(isset($vproduct[0])){
							$atts_name=array();
							$all_attributes=array();
							foreach($vproduct as $pvar){
								$atts=explode(",",$pvar->attributes_ids);
								foreach($atts as $at){
									$all_attributes[$at]=$at;
								}
							}
							foreach($all_attributes as $ats){
								$att_details=Attribute::select('name')->where('sup_attribute_id', $ats)->orderBy('id', 'asc')->first();
								if(isset($att_details->name)){
									$atts_name[]=$att_details->name;
								}
							}
							if(!empty($atts_name)){
								$atts=implode(",",$atts_name);
							}
						}
					}
				}
				$isok=1;
				if($prvariation_id !=0){
					if($atts ==""){
						$isok=0;
					}
				}
				if($isok==1){
					if(Auth::check()){
						if($prvariation_id==""){
							$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$wishproduct[0]->product_id)->first();
						}
						else{
							$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$wishproduct[0]->product_id)->where('product_variation_id', '=',$prvariation_id)->first();
						}
						if(isset($product_check->id)){
							$product_check->quantity = ($product_check->quantity+$quantity);
							$product_check->save();
						}
						else{
							$session = new Productsessions;
							$session->user_id = Auth::user()->id;
							$session->product_id = $wishproduct[0]->product_id;
							$session->price = $product[0]->retailprice;
							$session->name = $product[0]->name;
							$pr_images = Productsimages::select('img_name')->where('product_id', '=', $wishproduct[0]->product_id)->orderBy('id', 'asc')->first();
							if(isset($pr_images->img_name)){
								$session->img = $pr_images->img_name;
							}
							else{
								$session->img = "";
							}
							$session->attributes = $atts;
							if($prvariation_id !=""){
								$session->product_variation_id = $prvariation_id;
							}
							else{
								$session->product_variation_id = 0;
							}
							$session->quantity = $quantity;
							$session->save();
						}
						$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
						$html = '';
						$q = 0;
						foreach($products as $product){
							$img_url=url('public/webshop/bigproducts').'/'.$product->img;
							$html .= '<div class="row">';
							$html .='<div class="col-md-4 col-sm-4 col-xs-4 basket_right"><img  src="'.$img_url.'" /></div>';
							$html .='<div class="col-md-8 col-sm-8 col-xs-8 basket_left"><div class="basket_res"><a href="#">'.$product->name.'</a></div><div class="basket_detailsec"><p>'.$product->attributes.'</p><p>'.$product->quantity.'x'.Config::get('view.currency').number_format($product->price,2).'</p></div></div>';
							$html .= '</div >';
							$q += $product->quantity;
						}
						\Session::put('data', $products->toArray());
						echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html));
						die;
					}
				}
			}
		}
	}
	
	
	public function postWishalladdtocart(){
		$id = Input::get('wish_list_id');
		$todo = Input::get('todo');
		if(Auth::check()){
			$wishproduct = Wishlistproduct::where('wish_list_id', '=', $id)->where('user_id', '=', Auth::user()->id)->get();
			if(!empty($wishproduct[0])){
				foreach($wishproduct as $prd){
					$quantity=$prd->quantity;
					$atts="";
					$prvariation_id = $prd->product_variation_id;
					$product = BigProduct::where('id', '=', $prd->product_id)->where('active', '=', 1)->get();
					if(isset($product[0])){
						if($prvariation_id != 0){
							$prvars=ProductVariations::select('variation_id','retailprice')->where('id', '=', $prvariation_id)->first();
							if(isset($prvars->variation_id)){
								$product[0]->retailprice=$prvars->retailprice;
								$vproduct=Variations::select('id','attributes_ids')->where('id', '=', $prvars->variation_id)->where('product_id', '=', $prd->product_id)->get();
								if(isset($vproduct[0])){
									$atts_name=array();
									$all_attributes=array();
									foreach($vproduct as $pvar){
										$atts=explode(",",$pvar->attributes_ids);
										foreach($atts as $at){
											$all_attributes[$at]=$at;
										}
									}
									foreach($all_attributes as $ats){
										$att_details=Attribute::select('name')->where('sup_attribute_id', $ats)->orderBy('id', 'asc')->first();
										if(isset($att_details->name)){
											$atts_name[]=$att_details->name;
										}
									}
									if(!empty($atts_name)){
										$atts=implode(",",$atts_name);
									}
								}
							}
						}
						$isok=1;
						if($prvariation_id !=0){
							if($atts ==""){
								$isok=0;
							}
						}
						if($isok==1){
							if(Auth::check()){
								if($prvariation_id==""){
									$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$prd->product_id)->first();
								}
								else{
									$product_check = Productsessions::where('user_id', '=', Auth::user()->id)->where('product_id', '=',$prd->product_id)->where('product_variation_id', '=',$prvariation_id)->first();
								}
								if(isset($product_check->id)){
									$product_check->quantity = ($product_check->quantity+$quantity);
									$product_check->save();
								}
								else{
									$session = new Productsessions;
									$session->user_id = Auth::user()->id;
									$session->product_id = $prd->product_id;
									$session->price = $product[0]->retailprice;
									$session->name = $product[0]->name;
									$pr_images = Productsimages::select('img_name')->where('product_id', '=', $prd->product_id)->orderBy('id', 'asc')->first();
									if(isset($pr_images->img_name)){
										$session->img = $pr_images->img_name;
									}
									else{
										$session->img = "";
									}
									$session->attributes = $atts;
									if($prvariation_id !=""){
										$session->product_variation_id = $prvariation_id;
									}
									else{
										$session->product_variation_id = 0;
									}
									$session->quantity = $quantity;
									$session->save();
								}
								
							}
						}
					}
				}
				$products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->orderBy('id', 'desc')->get();
				$html = '';
				$q = 0;
				foreach($products as $product){
					$img_url=url('public/webshop/bigproducts').'/'.$product->img;
					$html .= '<div class="row">';
					$html .='<div class="col-md-4 col-sm-4 col-xs-4 basket_right"><img  src="'.$img_url.'" /></div>';
					$html .='<div class="col-md-8 col-sm-8 col-xs-8 basket_left"><div class="basket_res"><a href="#">'.$product->name.'</a></div><div class="basket_detailsec"><p>'.$product->attributes.'</p><p>'.$product->quantity.'x'.Config::get('view.currency').number_format($product->price,2).'</p></div></div>';
					$html .= '</div >';
					$q += $product->quantity;
				}
				if($todo=='delete'){
					Wishlistproduct::where('user_id', '=',  Auth::user()->id)->where('wish_list_id', '=',  $id)->delete();
				}
				if($todo=='deleteall'){
					Wishlistproduct::where('user_id', '=',  Auth::user()->id)->where('wish_list_id', '=',  $id)->delete();
					Wishlist::where('user_id', '=',  Auth::user()->id)->where('id', '=',  $id)->delete();
				}
				\Session::put('data', $products->toArray());
				echo json_encode(array('products'=>$products, 'len'=>$q, 'html'=>$html));
				die;
			}
		}
	}
	
	public function postUpdatecartwish(){
		$id = Input::get('id');
		$q = Input::get('quantity');
		$wishproduct = Wishlistproduct::where('id', '=', $id)->get();
		if(isset($wishproduct[0])){
			$product = BigProduct::where('id', '=', $wishproduct[0]->product_id)->get();
			if(isset($product[0])){
				if(Auth::check()){
					if($wishproduct[0]->product_variation_id !=0){
						$prvars=ProductVariations::select('retailprice')->where('id', '=', $wishproduct[0]->product_variation_id)->where('product_id', '=', $wishproduct[0]->product_id)->first();
						if(isset($prvars->retailprice)){
							$price=$prvars->retailprice;
						}
					}
					else{
						$price=$product[0]->retailprice;
					}
					$totalprc=number_format(($price*$q),2);
					$totalprc=Config::get('view.currency').$totalprc;
					$qtyprc=$q.'x '.Config::get('view.currency').$price;
					Wishlistproduct::where('user_id', '=', Auth::user()->id)->where('id', '=', $id)->update(['quantity'=>$q]);
					echo json_encode(array('success'=>true,'price'=>$totalprc,'qtyprc'=>$qtyprc));
					die;
				}
			}
			echo json_encode(array('success'=>false));
			die;
		}
		echo json_encode(array('success'=>false));
		die;
	}
	
	// sealstart
	public function getAccesstoken(){
		$url = "https://auth.snelstart.nl/b2b/token";
		$client_key = SNEL_CLIENT_KEY;
		$gt = 'grant_type=clientkey&clientkey='.$client_key;
		$curl_url=$url;
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HEADER, 0);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
		));
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $gt);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
		curl_close($cURLConnection);
		$response=json_decode($response,TRUE);
		return $access_token=$response['access_token'];
	}
	
	//Update Klant/Leveranciers in relatiesoort
	//Note:- The id in the curl_url and postReq is required and is suppose to be same
	public function getRelationupdate($snel_id,$email,$username){
		$url = "https://b2bapi.snelstart.nl/v2";
		$end_point = 'relaties';
		$api_key = SNEL_API_KEY;
		$access_token=$this->getAccesstoken();
		$curl_url=$url."/".$end_point."/".$snel_id;
		$postReq = array();
		$postReq['relatiesoort'][0]="Klant";
		$postReq['naam']=$username;
		$postReq['email']=$email;
		$postReq['id']=$snel_id;
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, $curl_url);
		curl_setopt($cURLConnection, CURLOPT_HEADER, 0);
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
			'Ocp-Apim-Subscription-Key:'.$api_key,
			'Authorization:Bearer '.$access_token
		));
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, http_build_query($postReq));
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($cURLConnection);
	}
	
}