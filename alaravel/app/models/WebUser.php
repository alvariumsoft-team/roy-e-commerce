<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class Webuser extends Eloquent Implements UserInterface, RemindableInterface{

use UserTrait, RemindableTrait;

protected $table = 'users';

protected $hidden = array('password','remember_token');

public static $rules = array(

'username'=> 'required|alpha_num|min:1|max:17',
'first_name'=> 'required|alpha|min:2',
'last_name'=> 'required|alpha|min:2',
'password'=> 'required|between:1,13|confirmed',
'password_confirmation'=> 'required|between:1,13',
'email'=> 'required|email|unique:user',

);


public function getRememberToken(){
return $this->remember_token;
}

public function setRememberToken($value){
$this->remember_token = $value;
}

public function getRememberTokenName(){
return 'remember_token';
}



} 