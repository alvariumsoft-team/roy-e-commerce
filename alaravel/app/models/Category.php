<?php

class Category extends Eloquent
{
    protected $table = 'tblcategory';

	protected $fillable = [
        'id', 'categoryname', 'slug','parent_id','imgurl',
    ];
	
	
    
}
