<?php

class Products extends Eloquent
{
    public function productmeta()
	{
		return $this->hasMany('Productmetas');
	}
	
    public function productimages()
	{
		return $this->hasMany('Productimages');
	}
    
}
