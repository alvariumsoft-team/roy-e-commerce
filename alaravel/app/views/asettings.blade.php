<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css' rel='stylesheet' type='text/css'>
	{{HTML::style('packages/bootstrap/dist/css/bootstrap.min.css')}}

<script  src="{{url('packages/bootstrap/dist/js/jquery.js')}}"></script>

    <!-- Custom styles for this template -->
    <style type="text/css">

		/*
		 * Base structure
		 */

		/* Move down content because we have a fixed navbar that is 50px tall */


.pull-xs-right {
    float: right !important;
}
.navbar-fixed-top {
    border: 0;
}
.bg-inverse {
    color: #fff !important;
    background-color: #373a3c !important;
    padding-right:10%;
}
		/*
		 * Global add-ons
		 */

		.sub-header {
		  padding-bottom: 10px;
		  border-bottom: 1px solid #eee;
		}

		/*
		 * Top navigation
		 * Hide default border to remove 1px line.
		 */
		.navbar-fixed-top {
		  border: 0;
		}

		/*
		 * Sidebar
		 */

		/* Hide for mobile, show later */
		.sidebar {
		  display: none;
		}
		@media (min-width: 768px) {
		  .sidebar {
			position: fixed;
			top: 51px;
			bottom: 0;
			left: 0;
			z-index: 1000;
			display: block;
			padding: 20px;
			overflow-x: hidden;
			overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
			background-color: #f5f5f5;
			border-right: 1px solid #eee;
		  }
		}

		/* Sidebar navigation */
		.nav-sidebar {
		  margin-right: -21px; /* 20px padding + 1px border */
		  margin-bottom: 20px;
		  margin-left: -20px;
		}
		.nav-sidebar > li > a {
		  padding-right: 20px;
		  padding-left: 20px;
		}
		.nav-sidebar > .active > a,
		.nav-sidebar > .active > a:hover,
		.nav-sidebar > .active > a:focus {
		  color: #fff;
		  background-color: #428bca;
		}


		/*
		 * Main content
		 */

		.main {
		  padding: 20px;
		}
		@media (min-width: 768px) {
		  .main {
			padding-right: 40px;
			padding-left: 40px;
		  }
		}


		/*
		 * Placeholder dashboard ideas
		 */

		.placeholders {
		  margin-bottom: 30px;
		  text-align: center;
		}
		.placeholders h4 {
		  margin-bottom: 0;
		}
		.placeholder {
		  margin-bottom: 20px;
		}
		.placeholder img {
		  display: inline-block;
		  border-radius: 50%;
		}

			.search-buttons{height:40px;background:transparent;border:0px;color:#fff;}

			.right{float:right;}
			.left{float:left;}
			.clear{clear:both;}

	</style>
  </head>

  <body>

    <nav class="navbar navbar-dark navbar-fixed-top bg-inverse">
      <button type="button" class="navbar-toggler hidden-sm-up" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL::to('/')}}">

	  <b>G P C</b>

	  </a>
      <div id="navbar">
        <nav class="nav navbar-nav pull-xs-left">
          <a class="nav-item nav-link" href="#">Dashboard</a>
          <a class="nav-item nav-link" href="#">Settings</a>
          <a class="nav-item nav-link" href="#">Profile</a>
          <a class="nav-item nav-link" href="#">Help</a>
        </nav>





        <div class="right" style="margin:0px 20px;">

		<div class="dropdown">
		  <button class="search-buttons dropdown-toggle" style="padding:0px 20px;color:#fff;" type="button" id="user-account" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		   <b> <i class="fa fa-user fa-2x"></i> </b>

		  </button>
		  <ul class="dropdown-menu" aria-labelledby="user-account">
		  <li>{{html_entity_decode(HTML::link('#','<i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;Submit Listing',array()))}}</li>
		  <li role="separator" class="divider"></li>
		  <li>{{html_entity_decode(HTML::link('#','<i class="fa fa-bars"></i>&nbsp;&nbsp;&nbsp;&nbsp;My Listings',array()))}}</li>
		  <li role="separator" class="divider"></li>
		  <li>{{html_entity_decode(HTML::link('asettings','<i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;&nbsp;Account Settings',array()))}}</li>
		  <li role="separator" class="divider"></li>
		  <li>{{html_entity_decode(HTML::link('logout','<i class="fa fa-sign-in"></i>&nbsp;&nbsp;&nbsp;&nbsp;Logout',array()))}}</li>
		  </ul>
		</div>


	</div>
	<div class="clear"></div>

      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#"> hdrsdjhsrdjt<span class="sr-only">(current)</span></a></li>
            <li><a href="#">retert</a></li>
            <li><a href="#">ertert</a></li>
            <li><a href="#">terte</a></li>
          </ul>
        </div>
        <div class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 main">





        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<script async src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
{{HTML::script('packages/js/gpc.js')}}

  </body>
</html>
