<script> 
$(document).ready(function(){
    $(".cat_blocks").click(function(e){
		e.preventDefault();
		var bloclattt=$(this).attr('block');
		var catvalatt=$(this).attr('catval');
		var blocsec=bloclattt+"_section";
		var sec_select=$("."+blocsec).attr('sel_section');
		if(sec_select==catvalatt){
			$("."+blocsec).attr('sel_section',"");
			$(".catsections").css('display','none');
		}
		else{
			$(".catsections").css('display','none');
			$("."+blocsec).css('display','block');
			$("."+blocsec).attr('sel_section',catvalatt);
		}
		var id=$(this).find('.catname' +bloclattt).val();
	
		var datastring='id='+id;
		$.ajax({
			url:'<?php echo base_url();?>welcome/parentcat',
			data:datastring,
			type:'POST',
			success:function(response){
				
				$(".subcatgory").html(response);
			
			}
		});
	});
	
	$('.ditm').on('click', function(){
		$('.all_india_loc').attr('value', $(this).html());
		$('input[name="indexform"]').attr('value', $(this).attr('data-value'));
		console.log($(this).attr('data-value'));
	});
	
});
</script>

<style type="text/css">
	.dcom-dr{display:none;}
	#dcom:hover .dcom-dr{display:block;}
	.ditem{margin:0px;padding:0px;}
	.ditem > a{margin:0px;padding:8px;padding-left:18px;}
	.ditem:hover > a{background:#2c83b6;color:#fff;}
	.dropdown, .dropdown-menu, .dropdown-item, .ditem{padding:0px !important;margin:0px;}
	.dropdown-menu{margin-top:47px;width:100%;}
</style>

<section class="product_selling_in">
   <div class="container">
      <div class="row back_color_search">
		  <form action="welcome/search" method="post">
			  <input type="hidden" name="indexform" value="set" />
	     <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                <div class="input-group stylish-input-group">
                    
					<div class="dropdown" style="width:100%;">
						<input type="text" name="location" class="form-control all_india all_india_loc dropdown-toggle"  placeholder="<?php echo $session_data['al']; ?>"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="">
							<ul class="dropdown" style="margin:0px;padding:0px;width:100%;">
								<li class="dropdown-item ditem"><a href="javascript:void(0);" class="ditm" style="display:block;" data-value="whole">Whole country</a></li>
								<li class="dropdown-item ditem" id="dcom"><a href="javascript:void(0);" class="ditm" style="display:block;" data-value="manama">Manama<span style="float:right;">&raquo;</span></a><div style="clear:both;">
								
								<div class="dcom-dr" style="min-height:300px;min-width:200px;max-width:500px;background:#fff;position:absolute;left:100%;top:0px;overflow-y:scroll;">
									<ul style="margin:0px;padding:0px;">
										<?php foreach($countries as $k=>$v){ ?>
										<li class="ditem"><a href="javascript:void(0);" class="ditm" style="display:block;" data-value="<?php echo $k;?>"><?php echo $v;?></a></li>
										<?php } ?>
									</ul>
								</div>
								
								</li>
							</ul>
						</div>
					</div>
                    
                    <span class="input-group-addon location_form_all">
                        <button type="submit">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </button>  
                    </span>
                </div>
		 </div>
		 	     <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="input-group stylish-input-group">
                   
					<div class="dropdown" style="width:100%;">
						<input type="text" name="search" class="form-control all_india"  placeholder="<?php echo $session_data['near']; ?>"  id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="">
							<ul class="dropdown search-suggest" style="margin:0px;padding:0px;width:100%;">
							</ul>
						</div>
					</div>     
                    
                    
                    <span class="input-group-addon all_india_search">
                        <button type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </button>  
                    </span>
                </div>
		 </div>
		 <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		     <div class="button_sumbit_product">
			     <button type="submit" class="btn btn-default search_olx_all"><?php echo $session_data['search']; ?></button>
			 </div>
		 </div>
	  </div>
	  </form>
   </div>

<?php
	$cn= count($cat);
	$last_in=$cn-1;
	foreach($cat as $key => $val){
		if($key%4==0){
			echo '<div class="container"><div class="row"><div class="col-lg-12 text_center">';
		}
		if($key<4){
			$cls="icon_circle_pr";
			$class="dropdown_bottom_a";
			$sec="cat_block_1";
		}
		else if($key >=4 && $key<8){
			$cls="icon_distribution_maj";
			$class="relative_vehicle_";
			$secclass="dropdown_bottom_a_section";
			$sec="cat_block_2";
		}
		else if($key >=8 && $key<12){
			$cls="icon_markt_ng";
			$class="chevron_true";
			$sec="cat_block_3";
		}
		else{
			$cls="icon_markt_ng";
				$class="dropdown_bottom_a";
				$sec="cat_block_4";
		}
		$sec_class=$sec."_section";
		
		if($val['iconbackground']!=""){
			 $color=$val['iconbackground'];?>
		<?php }
		
		?>
		
		<div class="col-lg-3 col-sm-4 col-md-3 col-xs-12 <?php echo $cls;?> cat_blocks" block="<?php echo $sec;?>" catval="<?php echo $val['id']; ?>">
			   <div class="icon_product">
				   <i class="fa <?php echo $val['caticon']; ?> iconcss" aria-hidden="true" style="background-color:#<?php echo $val['iconbackground']; ?> !important;"></i>
					<a class="markti_ng" href="<?php echo base_url();?><?php echo $val['catname'];?>" ><h4><?php echo $val['catname']; ?></h4></a>
			<input type="hidden" name="catname" class="catname<?php echo $sec;?>"  value="<?php echo $val['id']; ?>">			   
			   </div>
		</div>
		<?php
		if((($key + 1)%4==0) || ($last_in==$key)){
			echo '</div></div></div>';
		?>
			<section class="<?php echo $class;?> <?php echo $sec_class;?> catsections" sel_section="">
   <div class="container">
      <div class="row">
	  <div class="subcatgory"></div>
	  </div>
   </div>
</section>
		<?php }
	}
	?> 
	
</section>
<section class="expert_delhi">
   <div class="container">
       <div class="row">
	      <div class="col-lg-12 recommer">
		      <div class="delhi_res">
			     <h3><?php echo $session_data['latest']; ?></h3>
			  </div>
		  </div>
	   </div>
   </div>
</section>
<section class="experts">
    <div class="container">
	    <div class="row">
		<div class="col-lg-9 col-sm-9 col-md-9 col-xs-12 tap_produc_">
		 <?php
			foreach($add as $key=>$value) { 
			// echo"<pre>";print_r($value);echo"</pre>";
		 ?>
		   <div class="col-lg-12 fvg">
			  <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12 olx_xbd">
			  <?php 
				if(isset($value['img'])){
			  ?>
					<img src="<?php echo base_url();?>/<?php echo $value['img']; ?>">
			  
			  <?php
				}else{?>
					<img src="<?php echo base_url();?>/uploads/no-image.jpg.png">
				<?php } ?>
		
			  </div>
			  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 olx_textd">
		      <div class="commended_Experts">
			  
			  <div class="reviews-left">
			   <a href="<?php echo base_url();?>welcome/cat_detail/<?php echo $value['ad_id']; ?>"><strong class="raja"><?php echo $value['title'];?></strong></a>
			  </div>
                      <p class="thank_pro"><?php  if(isset($value['subcat'])){echo $value['subcat']; ?>>> <?php } ?><?php  if(isset($value['catname'])){ echo $value['catname'];}?> </p>
                     <strong class="sulekha-score"><?php echo $value['city'];?></strong>
					  <span class="sulekha_score"><?php echo $value['createddate'];?>10:46</span>
                  </div>
			 </div>
			 <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 olx_bhd_fl">
			    <p class="price_bnds"><strong><?php  if(!empty($value['fields']['price'])){echo $value['fields']['price'];?> BHD<?php } ?></strong></p>
				<p class="br4_hidden" ><?php echo $session_data['fav'];?>
				<i class="fa fa-star" aria-hidden="true"></i>
</p>
			 </div>
			
			  </div>
			  		   <?php } ?> 
			  		
		  </div> 
		   <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
		      <div class="bike_rase">
			      <img src="<?php echo base_url();?>assets/front/img/bike.png">
			  </div>
           </div>		  
		</div>
	</div>			 </div>
</section>
<section class="sell_phone">
    <div class="container">
	    <div class="row">
		    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			    <div class="make_your" style="text-align:center;">
				    <img src="<?php echo base_url();?>assets/front/img/facebook_mobile_olx.png">
				</div>
			</div>
		</div>
	</div>
</section>


