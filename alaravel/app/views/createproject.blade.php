@extends('dash')

@section('dash_content')

<div >

<ul class="nav nav-tabs" role="tablist">

<li role="presentation" class="active"><a>Edit Information</a></li>
<li role="presentation" ><a >Media</a></li>
<li role="presentation"><a>Contact Information</a></li>

</ul>



</div>

<h1></h1>

<form action="" method="post" enctype="multipart/form-data">

<div class="form-group">{{Form::select('company',$company,'BHK',array('class'=>'form-control','placeholder'=>'bhk'))}}</div>


<div class="form-group">{{Form::text('project_name','',array('class'=>'form-control','placeholder'=>'Project Name'))}}</div>


<div class="form-group">{{Form::textarea('project_description','',array('class'=>'form-control','placeholder'=>'Project Description'))}}</div>



<div id="udetails-db" class="container">
<div class="container">
<h1>ADD UNIT DETAILS</h1>

<div class="row"><!-- row start -->

<div class="col-md-3">
<div class="form-group">{{Form::select('bhk',array('BHK','1','2','3','4','5','6','7','8','9','10'),'BHK',array('class'=>'form-control','placeholder'=>'bhk'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::select('balcony',array('No. Of Balcony','q'=>'1','w'=>'2','e'=>'3'),'null',array('class'=>'form-control'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::select('bathroom',array('No. Of Bathroom','1','2','3','4','5','6','7','8','9','10'),'null',array('class'=>'form-control'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::selectMonth('month','null',array('class'=>'form-control'))}}</div>
</div>


</div> <!-- row end -->

<div class="row"> <!-- row start -->

<div class="col-md-3">
<div class="form-group"> {{Form::text('furnished','',array('class'=>'form-control','placeholder'=>'Furnished'))}}</div>
</div>

<div class="col-md-4">
<div class="form-group">{{Form::text('carpet_area','',array('class'=>'form-control','placeholder'=>'Carpet Area'))}}</div>
</div>


<div class="col-md-5">
<div class="form-group">{{Form::text('price','',array('class'=>'form-control','placeholder'=>'Price'))}}</div>
</div>

</div> <!-- row end -->

<!--
<div class="col-md-1">

<button type="button" id="spec-add" class="spec_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>

<div></div>
-->

<div>
<div>
<div class="form-group">{{Form::textarea('specification','',array('class'=>'form-control ','id'=>'spec','placeholder'=>'Specification'))}}</div>

</div>


</div>

<div>
<div class="form-group">{{Form::textarea('local_advantage','',array('class'=>'form-control','placeholder'=>'Local Advantage'))}}</div>
</div>



<div>
<div class="form-group">{{Form::textarea('amenities','',array('class'=>'form-control','placeholder'=>'Ameneties'))}}</div>
</div>

<div>
<div class="form-group">{{Form::button('Add Image',array('class'=>'btn','placeholder'=>'Image'))}}</div>
</div>

</div>

<hr />

</div>

<div class="form-group" id="udetails">


</div>

<div class="form-group">{{Form::button('Add Unit',array('class'=>'btn ','id'=>'udbtn'))}}</div>

<hr />
<div class="form-group">{{Form::file('image[]',array('class'=>'btn'))}}</div>
<hr />
<div class="form-group">{{Form::button('Add Location',array('class'=>'btn'))}}</div>
<hr />
<div class="form-group"><h2>CONTACT DETAILS</h2></div>
<div>
<div class="form-group">{{Form::text('phone','',array('class'=>'form-control','placeholder'=>'Phone Number'))}}</div>
<div class="form-group">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'E-mail'))}}</div>
<div class="form-group">{{Form::text('website','',array('class'=>'form-control','placeholder'=>'Your Website'))}}</div>
<div class="form-group">{{Form::textarea('address','',array('class'=>'form-control','placeholder'=>'Your Address'))}}</div>
<div class="form-group">{{Form::text('country','',array('class'=>'form-control','placeholder'=>'Your Country'))}}</div>
<div class="form-group">{{Form::text('state','',array('class'=>'form-control','placeholder'=>'Your State'))}}</div>
<div class="form-group">{{Form::text('city','',array('class'=>'form-control','placeholder'=>'Your City'))}}</div>
<div class="form-group">{{Form::button('Add Social Media',array('class'=>'btn'))}}</div>
</div>

<div class="form-group">{{Form::submit('Submit Project',array('class'=>'btn btn-large btn-primary btn-block'))}}</div>

</form>



@stop
