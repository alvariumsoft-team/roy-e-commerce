@extends('adashb')

@section('dash_content')


<style type="text/css">

.gpc-listing-box{border-bottom: 1px solid #ccc;padding:10px;margin:0px 10px;background:#fff;}

.left{float:left;}

.right{float:right;}

.clear{clear:both;}

.feature-tag{background:#63b600;box-shadow: 0px 0px 0px rgb(153, 153, 153); border:1px solid #fff;border-radius:5px;padding:1px 5px;color:#fff;font-size:12px;}

.pfont > p{font-size:14px;}

.pfont-light > p{color:#999;}

.mar-inf{margin:5px 10px;margin-right:5px;}

.pad-inf{padding:10px;}

.aw > a{padding-right:10px;}

.btn-aw{background:#fff !important;margin-top:10px;color:#367fa9;border:0px;font-size:18px;}

.btn-aw:hover{color:#365899;}

</style>

<div class="row">
	<div class="col-md-9">
	<div class="gpc-listing-main">
		<div class="gpc-listing-box">
			<div class="row">
				<div class="col-md-4">
					<p><button class="feature-tag">Featured Listing</button></p>
					<img src="{{URL::to('packages/images/home-img1.jpg')}}" class="img-responsive" />
					<div class="pfont"> 
						<p>
							<button title="Favourite" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-star"></i></button> 
							<button title="Like" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-thumbs-up"></i></button> 
							<button title="Report" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-ban"></i></button> 
							<button title="Map" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-map-marker"></i></button> 
							<div class="clearfix"></div>
						</p>
					</div>
				</div>
				<div class="col-md-8">
					<h3>Apartments</h3>
					<h4><b>for sale</b> in Avenue 125 , Sector-125 </h4>
					<div class="row">
						<section class="col-md-2 pfont pfont-light">
							<p>Builder</p>
							<p>Status</p>
							<p>Detail</p>
							<p>Description</p>
						</section>
						<section class="col-md-10 pfont">
							<p>Ever Rich Buildcon Pvt. Ltd.</p>
							<p>Under Construction (Ready by Mar '18) , Freehold</p>
							<p>3 Bath , Semi-Furnished, 2nd Floor (of 8) dsf sdf dsf..</p>
							<p>Pay 10 %, No EMI & No Interest wqe sdf sdf sdf read more</p>
						</section>
						<div class="pfont"> 
							<p>
								<button class="btn btn-primary pull-left mar-inf"> Contact Info</button> 
								<button class="btn btn-primary pull-left mar-inf"> View Phone Number </button> 
								<div class="clearfix"></div>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="gpc-listing-box">
			<div class="row">
				<div class="col-md-4">
					<p><button class="feature-tag">Featured Listing</button></p>
					<img src="{{URL::to('packages/images/home-img1.jpg')}}" class="img-responsive" />
					<div class="pfont"> 
						<p>
							<button title="Favourite" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-star"></i></button> 
							<button title="Like" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-thumbs-up"></i></button> 
							<button title="Report" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-ban"></i></button> 
							<button title="Map" class="btn btn-primary btn-aw pull-left mar-inf"><i class="fa fa-map-marker"></i></button> 
							<div class="clearfix"></div>
						</p>
					</div>
				</div>
				<div class="col-md-8">
					<h3>Apartments</h3>
					<h4><b>for sale</b> in Avenue 125 , Sector-125 </h4>
					<div class="row">
						<section class="col-md-2 pfont pfont-light">
							<p>Builder</p>
							<p>Status</p>
							<p>Detail</p>
							<p>Description</p>
						</section>
						<section class="col-md-10 pfont">
							<p>Ever Rich Buildcon Pvt. Ltd.</p>
							<p>Under Construction (Ready by Mar '18) , Freehold</p>
							<p>3 Bath , Semi-Furnished, 2nd Floor (of 8) dsf sdf dsf..</p>
							<p>Pay 10 %, No EMI & No Interest wqe sdf sdf sdf read more</p>
						</section>
						<div class="pfont"> 
							<p>
								<button class="btn btn-primary pull-left mar-inf"> Contact Info</button> 
								<button class="btn btn-primary pull-left mar-inf"> View Phone Number </button> 
								<div class="clearfix"></div>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>

	<div class="col-md-2"></div>
</div>





@stop
