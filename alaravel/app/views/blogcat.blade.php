@extends('blog_main')

@section('blog_content')
				
				<?php $count_error = 0; ?>
				
				@foreach($posts as $post)
					@if(count($post) == 0)
						<?php $count_error = $count_error+1; ?>
					@else
					
					<?php $post = $post[0];?>
					
				<div class="post">
					<div class="post-heading"><a href="{{URL::to('blogpost')}}/{{$post->post_name}}"><h2>{{$post->post_title}}</h2></a></div>
					<div class="post-pad author-info">
					<span class="author-image"><img src=""></span>
					<span class="author">by <b>{{$post->post_author}}</b></span>
					<span class="post-creation-date"><i>published on {{$post->created_on}}</i></span>
					</div>
<div class="post-heading img-responsive" style="width:100%;"><img style="height:200px; width:100%;"src="{{URL::to($post->featured_image)}}"></img></div>
					<div class="post-pad post-content">{{$post->post_excerpt}}</div>
					<span><a href="{{URL::to('blogpost')}}/{{$post->post_name}}" class="read-more" target="_blank" >Read More &raquo;</a></span>
					<div class="post-pad blog-info"></div>
				</div>
				
				@endif
				
				@endforeach
				
				@if(count($posts) == 0)
					<div class="post">
						<div class="post-heading"><h2>No results found!!</h2></div>
					</div>
				@elseif(count($posts) == $count_error)
					<div class="post">
						<div class="post-heading"><h2>No results found!!</h2></div>
					</div>
				@endif
				
				
				<!--<span class="bpaginate" style="text-align:center !important;display:block;"></span>-->
				
@stop



