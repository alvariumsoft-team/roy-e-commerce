@extends('adashb')
@section('dash_content')
<h3>Comments</h3>


<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>Comment</th>
<th>Author</th>
<th>On Post</th>
<th>Status</th>
<th>Type</th>
<th>Submitted On</th>
<th>Action</th>
</tr>
</thead>

<tbody>

@foreach($comments as $comment)

<tr>
<td>{{Form::checkbox('listing_action','','',array('id'=>'listing_action'))}}</td>

<td>{{$comment['content']}}</td>
<td>{{$comment['author']}}</td>
<td>{{$comment['post']}}</td>
<td>{{$comment['status']}}</td>
<td>{{$comment['type']}}</td>
<td>{{$comment['date']}}</td>
<td class="clearfix">
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('commentactivate')}}/{{$comment['id']}}" >Open</a>
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('commentdeactivate')}}/{{$comment['id']}}">Close</a>
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('commentdelete')}}/{{$comment['id']}}">Delete</a>
</td>

</tr>

@endforeach

</tbody>

</table>


<span class="bpaginate" style="text-align:center !important;display:block;">{{ $comments_link->links() }}</span>



@stop
