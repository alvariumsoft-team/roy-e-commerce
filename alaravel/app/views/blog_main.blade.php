<!DOCTYPE>
<html>
	<head>
	<title></title>
		{{HTML::style('packages/ad/bootstrap/css/bootstrap.min.css')}}
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		
		{{HTML::script('packages/bootstrap/dist/js/jquery.js')}}
		
		<style type="text/css">
			 body{font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Helvetica Neue",sans-serif;}
			.post-pad{padding:5px 0px;}
			.post{border-bottom:1px solid #eee; margin-bottom:50px;}
			.read-more{padding:10px; background:#424242;color:#fff;font-weight:700;}
			.read-more:hover{text-decoration:none; color:#fff;}
			.recent-post, .categories, .archives{padding:8px 0px;}
			.footer_copyright{text-align:center;padding-top:20px;margin-bottom:-10px;}
			.site-title{margin-top:8px;}
			.pagination > .active > span{background:#424242 !important; border-color:#424242 !important;}
			.pagination > li > a{color:#424242 !important;}
			.post-heading > a:hover{color:#9a9a9a;}
			.post-heading > a{color:#4a4a4a;}
			.post-content{font-weight:400;font-size:18px;margin:20px 0px;}
			.full-post{border-bottom:5px solid #424242;}
			.user-img > img{border: 3px solid:#ccc; border-radius:50px;width:50px;}
			.comment-dt{padding: 16px 20px;}
			.comment{padding:50px 20px;border-bottom: 1px solid #ccc;}
			.comment-childrens{padding-top:30px;padding-bottom:10px;}
			.social{margin:0px;padding:0px;margin-top:8px;padding-top:8px;text-align:right;}
			.social >li{list-style:none;display:inline;padding:6px 12px;}
			
			
			.facebook > a{color:#3a7ea2 !important;}
			.twitter > a{color:#55c5ef !important;}
			.linkedin > a{color:#4875b4 !important;}
			.google-plus > a{color:#c63d2d !important;}
			.youtube > a{color:#c4302b !important;}
			.instagram > a{}
			.facebook:hover{background:#3a7ea2;color:#fff!important;}
			.twitter:hover{background:#55c5ef;color:#fff !important;}
			.linkedin:hover{background:#4875b4;color:#fff !important;}
			.google-plus:hover{background:#c63d2d;color:#fff !important;}
			.youtube:hover{background:#c4302b;color:#fff !important;}
			.instagram:hover{background:#000;color:#fff !important;}
			.facebook:hover > a{background:#3a7ea2;color:#fff!important;}
			.twitter:hover > a{background:#55c5ef;color:#fff !important;}
			.linkedin:hover > a{background:#4875b4;color:#fff !important;}
			.google-plus:hover > a{background:#c63d2d;color:#fff !important;}
			.youtube:hover > a{background:#c4302b;color:#fff !important;}
			.instagram:hover > a{background:#000;color:#fff !important;}
			
			@media(max-width:786px){
				.social{text-align:left;}
			}
			
		</style>
		
	</head>
<body>
<header style="background:#424242;width:100%;color:#fff;padding:3% 8%;">
		<div class="container">
			<header class="row">
				<aside class="logo col-md-1">
					<a href="{{URL::to('/')}}"><img src="{{URL::to('packages/images/logo.png')}}" class="img-responsive" /></a>	
				</aside>
				<aside class="col-md-6">
					<a href="{{URL::to('blog')}}" style="text-decoration:none;color:#fff;outline:none;" title="Blog Site...">
						<h3 class="site-title">Global Property Cowboys</h3>
						<p class="site-description">A Real Estate Portal</p>
					</a>
				</aside>
				<aside class="col-md-4">
					<ul class="social">
						<li class="social-li facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li class="social-li twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li class="social-li youtube"><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
						<li class="social-li instagram"><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</aside>
			</header>
		</div>
	
</header>

<div class="blogs"  style="padding:3% 8%;">
	<div class="container">
		<div class="row">
			<div class="col-md-8">

				@yield('blog_content')

			</div>
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="blog-search">
					<h6>&nbsp;</h6>
					<!--<input class="form-control" name="blog_search" placeholder="Search Blog Post..."/>-->
					
					<script type="text/javascript">
						$(document).ready(function(){
							$('#basic-addon2').on('click', function(){
								$('#blog_search').submit();
							});
						});
					</script>
					
				<form action="{{URL::to('blogsearch')}}" id="blog_search" method="post">	
					<div class="input-group">
					  <input type="text" class="form-control" placeholder="Search Blog Post..." aria-describedby="basic-addon2" name="bquery">
					  <span style="cursor:pointer;background:#424242;color:#fff;border:1px solid #424242;" type="submit" class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
					</div>
				</form>
					
				</div>
				<div class="recent-posts">
					<div class="post-heading"><h3>Recent Posts</h3></div>
					@foreach($recent_posts as $rpost)
						<div class="recent-post"><a href="{{URL::to('blogpost')}}/{{$rpost->post_name}}">{{$rpost->post_title}}</a></div>
					@endforeach
				</div>
				
				<div class="categories">
					<div class="post-heading"><h3>Categories</h3></div>
					@foreach($categories as $category)
						<div class="categories"><a style="color:#000;white-space:nowrap;" href="{{URL::to('category')}}/{{$category->category_slug}}">{{$category->category_name}}</a></div>
					@endforeach
				</div>
				
				<?php $ayear = array();?>
				
				<div class="archives">
					<div class="post-heading"><h3>Archives</h3></div>
					@foreach($archives as $archive)
					<?php if(in_array($archive->year, $ayear)){continue;}else{$ayear[] = $archive->year;}?>
						<div class="archives"><a style="color:#000;white-space:nowrap;" href="{{URL::to('archive')}}/{{$archive->year}}">{{$archive->year}}</a></div>
					@endforeach
				</div>
			
			</div>
		</div>
	</div>
</div>

<footer style="background:#424242;width:100%;color:#fff;padding:3% 8%;">
		<div class="container">
			<footer class="row">
				
				<div class="col-md-3">
					<h3></h3>
					<img src="{{URL::to('packages/images/logo.png')}}" class="img-responsive" />
				</div>
				<div class="col-md-3">
					<h3>About</h3>
					<p>We are a company based in Heiloo, a small town near Amsterdam, The Netherlands , Western Europe.</p>
				</div>
				<div class="col-md-3">
					<h3>Contact</h3>
					<p>101 Front St. San Diego, CA 92101 Netherlands
619-555-9785
support@globalpropertycowboys.com</p>
				</div>
				<div class="col-md-3">
					<h3>Timings</h3>
					<p>OPENING HOUSE
Monday-Saturday 11Am-5Pm
Other times by appointment only
http://www.globalpropertycowboys.com/</p>
				</div>
				
				<span class="footer_copyright col-md-12" >&copy; Global Property Cowboys</span>
				
			</footer>
		</div>
</footer>

</body>
</html>
