@extends('adashb')
@section('dash_content')


<h2> Users </h2>

<p>View all users</p>

<div style="overflow-x:auto;">
<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>UserName</th>
<th>Name</th>
<th>Email</th>

</tr>
</thead>

<tbody>

@foreach($users as $user)

<tr>
<td>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</td>

<td>{{$user['username']}}</td>
<td>{{$user['firstname']}}</td>
<td>{{$user['email']}}</td>
<td></td>
<td></td>
<td>{{$user['country']}}</td>

</tr>

@endforeach

</tbody>

</table>
</div>






{{ $users->links() }}


@stop