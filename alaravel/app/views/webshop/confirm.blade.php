@include('webshop.header')
<section class="contact-form">
	<div class="shipping">
		<div class="container-fluid">		  
		  <div class="col-md-8 col-sm-8">
		      <div class="col-md-12 col-sm-12">
				 <div class="shipping_sec">				
					<h2>Type of delivery </h2>
					@if(isset($checkout))
						<h4>{{$checkout->shipping_method}} - {{$checkout->shipping_delay}}</h4>
					@endif
				 </div>
			   </div>
			
              
			    
              
                <div class="col-md-6 col-sm-6">
				  <div class="billing bill_main">
						<h2>Billing address </h2>						
						<table class="table">													
							<tbody>
                                <tr> 									
									<td class="tab_bold">Name</td> 
									<td>{{$checkout->name}} {{$checkout->surname}}</td> 									
								</tr> 														
								<tr> 									
									<td class="tab_bold">Email</td>
									<td>{{$checkout->email}}</td>								
								</tr>
								
								<tr> 									
									<td class="tab_bold">Telephone</td>
									<td>{{$checkout->telephone}}</td>								
								</tr>
								<tr> 									
									<td class="tab_bold">Postal Code</td>
									<td>{{$checkout->billing_postalcode}}</td>								
								</tr>
                                <tr> 									
									<td class="tab_bold">Address</td>
									<td>{{$checkout->billing_address}}, {{$checkout->nif}}, {{$checkout->billing_floor}} ,{{$checkout->	billing_city}}</td>								
								</tr>
								<tr> 									
									<td class="tab_bold">Country</td>
									<td>{{$checkout->country}}</td>								
								</tr>
                                 								
							</tbody> 
						</table>					
				  </div>
             	</div>
				
				<div class="col-md-6 col-sm-6">
				  <div class="billing bill_main">
						<h2>Shipping Address</h2>						
						<table class="table">													
							<tbody>
                                <tr> 									
									<td class="tab_bold">Name</td> 
									<td>{{$checkout->shipping_name}} {{$checkout->shipping_surname}}</td> 									
								</tr> 															
								<tr> 									
									<td class="tab_bold">Email</td>
									<td>{{$checkout->email}}</td>								
								</tr>
								
								<tr> 									
									<td class="tab_bold">Telephone</td>
									<td>{{$checkout->shipping_phone}}</td>								
								</tr>
								<tr> 									
									<td class="tab_bold">Postal Code</td>
									<td>{{$checkout->shipping_postalcode}}</td>								
								</tr>
                                <tr> 									
									<td class="tab_bold">Address</td>
									<td>{{$checkout->shipping_address}}, {{$checkout->nr}}, {{$checkout->shipping_floor}} ,{{$checkout->shipping_city}}</td>								
								</tr>
								<tr> 									
									<td class="tab_bold">Country</td>
									<td>{{$checkout->shipping_country}}</td>								
								</tr>
                                 								
							</tbody> 
						</table>					
				  </div>
             	</div>
				
             
                     

             

                   <div class="col-md-6 col-sm-6 change_sec">	
						  <div class="change">
								<a href="{{url('/checkout/checkout')}}">
								   change
								</a>
						  </div>
				    </div>

				    <div class="col-md-6 col-sm-6 change_sec">	
					  <div class="change">
							<a href="{{url('/checkout/checkout')}}">
							   change
							</a>
					</div>
				</div>
				<?php  
				$items = 0;
				$total = 0;
				$totaltax=0;
				?>      
				<div class="col-md-12 col-sm-12">

                    @foreach($products as $session)
					<?php
					$items = $items + $session->quantity;
					$total = $total + ($session->quantity * $session->price);
					?>
					<div class="prosec">
						<div class="product_buy">
							<div class="product_leftimg bskt_pruduct_img">
								<img src="{{url('public/webshop/bigproducts')}}/{{$session->img}}" class="img-responsive" alt="product image">
							</div>
							<div class="product_rightsec pro_as">
								<div class="product_rightinfo">
								
									<h5><a href="{{url('/webshop/product')}}/{{$session->products->url}}">{{$session->name}}</a><span class="op">
										<ul>
										  <li>Quantity</li>
										  <li>Excl. Tax</li>
										  <li>Tax</li>
										  <li>Incl. Tax</li>										  
										</ul>
									</span>
									</h5>
									
									
								</div>
								<div class="item_description">
									<div class="item_prop">
										@if($session->product_variation_id==0)
											<span class="reference">ref. {{$session->products->sku}}</span>
										@else
											<span class="reference">ref. {{$session->prvars->sku}}</span>
										@endif
										<div class="props"> <span ng-bind-html="::shopCartItem.sizeToShow">{{$session->attributes}}</span> </div>
									</div>
									<div class="item_quantity">						   
										<ul>								
											<li>{{$session->quantity}}</li>								
										</ul>
										<span class="total">
											{{Config::get('view.currency')}}{{number_format(($session->quantity * $session->price),2)}}
										</span>

										<?php 

				                        $totalPrice = $session->quantity * $session->price;
                                        $tax        = $checkout->taxpercent;
										$finalTotal = ($totalPrice * $tax)/100;
										?>

										<span class="total">
											{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}
										</span>
                                    
										<span class="total">
											{{Config::get('view.currency')}}{{number_format(($finalTotal + $totalPrice),2)}}
										</span>

									</div>
									
								</div>
							</div>
						</div>
					</div>
                        
                     @endforeach
					   <div class="change bnm">
						<a href="{{url('/webshop/basket')}}">
						   change
						</a>
				       </div>
				</div>				
				   <div class="col-md-6 col-sm-6">
					   <div class="sub_sec def xc">
						   <div class="checkbox">						
							<label class="con">
							   I agree with <a href="{{url('assets/pdf/')}}/general-terms-and-conditions.pdf" target="blank">terms and conditions</a>
							  <input type="checkbox" name="term_condition" id="term_condition">
							  <span class="checkmark"></span> 						  
							</label>
							<label for="" class="term_condition" style="color: red;"></label>						
							</div>
						</div>
                    </div>
                    <div class="col-md-6 col-sm-6">					
						<div class="cont_btn">
							<a href="JavaScript:Void(0);" id="continue">continue</a>
						</div>
					</div>					
				 				  
		  </div>
         
          <div class="col-md-3 col-sm-4 order_in">
				<div class="order-summary">
					<h2>ORDER SUMMARY </h2>
					<span class="total-items">({{$items}} items)</span>
				</div>
				<div class="order_sec">
					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Total items:</span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price">{{Config::get('view.currency')}}{{number_format($total,2)}}</span>					
						</div>
					</div>
					
					
					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Delivery charges:</span>						
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price delivery_charges">{{Config::get('view.currency')}}{{number_format($checkout->shipping_cost,2)}}</span>					
						</div>
                    </div>
					
					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Total </span>
							<span class="reference">(Excl Tax):</span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price">{{Config::get('view.currency')}}{{number_format(($checkout->shipping_cost + $total),2)}}</span>					
						</div>
					</div>
					    

					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Tax: </span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price">{{Config::get('view.currency')}}{{number_format($checkout->tax,2)}}</span>					
						</div>
					</div>
	
			    </div>

				<div class="cart-total"> 
						<div class="col-md-8 col-sm-8 total-title"> <span> TOTAL: </span> </div>
						<div class="col-md-4 col-sm-4 total-price final-price final_total">{{Config::get('view.currency')}}{{number_format(($checkout->total_cost),2)}}</div> 
			    </div>
			    <input type="hidden" class="total-items-val" value="{{$items}}" />
		        <input type="hidden" class="product-total" value="{{$total}}" />
				<p class="store"><!-- You still need £26.00 to get free standard delivery! --> </p>
				<div class="free_store">
					<h5><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Free store delivery </h5>
				</div>
				<div class="promote-sec">
					<p>DO YOU HAVE A PROMOTIONAL CODE? Enter it when you reach the payment page </p>
				</div>
            </div>                		  


		</div>
    </div>
</section> 


@include('webshop.nomenu_footer')
<script type="text/javascript">
	 $('#term_condition').click(function() {
        if($(this).is(':checked'))
            //alert('checked');
        $('.term_condition').html('');
        else
            //alert('unchecked');
        $('.term_condition').html('You must accept the term condition.');
        $(".term_condition").fadeIn('slow', function(){
        $(".term_condition").delay(3000).fadeOut();
         });
    });

$('#continue').click(function() {

	if($('#term_condition').is(":checked"))
	{
	  
	} else{

		$('.term_condition').html('You must accept the term condition.');
        $(".term_condition").fadeIn('slow', function(){
        $(".term_condition").delay(3000).fadeOut();
         });
		return false;
	}

    window.location.href = "{{url('checkout/postConfirm/'.$checkout->id)}}";
    return false;
});	 
</script>