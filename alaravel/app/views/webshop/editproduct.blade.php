@extends('adashb')
@section('dash_content')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker1" ).datepicker();
  } );
  </script>
<link rel="stylesheet" href="http://inviolateinfotech.com/webshop/public/assets/css/datepicker.css" />
<h2>Edit Product</h2>
<form action="" method="post" enctype="multipart/form-data">
<div class="form-group row">
	<label class="col-md-3"><b>ParentCategory</b></label>
	<label class="col-md-6">
	<select name="parentcategory">
	<option value="">Select Parentcategory</option>
	<?php foreach($parentcategory as $parentcat) { 
		$select='';
		if($products['parentcatid'] ==$parentcat['id']){
			$select='selected';
		}
	?>
		<option value="<?php echo $parentcat['id']; ?>" <?php echo $select;?>><?php echo $parentcat['name'];?></option>
	<?php
		}
	?>
	</select>
	</label>
</div>
<div class="form-group row">
	<label class="col-md-3"><b>Category</b></label>
	<label class="col-md-6">
	<select name="category">
	<option value="">Select category</option>
	<?php foreach($category as $cat) { 
		$select='';
		if($products['cid'] ==$cat['id']){
			$select='selected';
		}
	?>
		<option value="<?php echo $cat['id']; ?>" <?php echo $select;?>><?php echo $cat['categoryname'];?></option>
	<?php
		}
	?>
	</select>
	</label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Product Name</b></label>
	<label class="col-md-6">	<input type="text" class="form-control" name="product" placeholder="Product Name" required="" value="{{$products['productname']}}"></label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Price</b></label>
	<label class="col-md-6"><input type="text" class="form-control" name="price" placeholder="Price" required="" value="{{$products['price']}}"></label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Size</b></label>
	<label class="col-md-6"><input type="text" class="form-control" name="size" placeholder="Size" required="" value="{{$products['size']}}"></label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Quantity</b></label>
	<label class="col-md-6"><input type="text" class="form-control" name="quantity" placeholder="Quantity" required="" value="{{$products['quantity']}}"></label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Image</b></label>
	<label class="col-xs-12 col-md-6"  for="ifile-id2" >
		<div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd">
		<span id='ifile-labe'  class='ifile-label'>Upload Image</span>
		<input type="file" name="image[]"  class="inputfile inputfile-1 ifile-id"  id="ifile-id2" multiple> 
			<?php 
			if(isset($products['image'])){
				if($products['image']!=''){
					$arr=explode(',',$products['image']);
					foreach($arr as $key =>$v){
						?>
						<img src="{{url('assets/img')}}/{{$v}}" width="100" height="100">
						<?php
					} 
				} 
			}
			?>
			
		</div>
	</label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Start Date</b></label>
	<label class="col-md-6"><input name="startdate" id="datepicker1" class="form-control" type="text" placeholder="m/d/Y" value="{{$products['startdate']}}"></label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>End Date</b></label>
	<label class="col-md-6"><input name="enddate" id="datepicker" class="form-control" type="text" placeholder="m/d/Y" value="{{$products['enddate']}}"></label>
</div>
<input type="hidden" name="id" value="{{$products['id']}}">
<div class="form-group">
<input type="Submit" class="btn btn-large btn-primary" name="submit" value="Update">
</div>
</form>
@stop
