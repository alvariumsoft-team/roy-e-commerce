<!DOCTYPE html>
<html>
<head>
<title>Available payment methods</title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style>
  
  /*---- Reset ----------------------------------*/
.nanorep-faq table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
}
.nanorep-faq a {
  // display: inline-block;
  //color: inherit;
  text-decoration: none;
  outline: none;
}
.article__body a{
  color:#ff6900;
}
// /*.article__body ul:not(.answers__list) li::before{
//   content:"" !important; 
// }*/
.nanorep-faq a:active,
.nanorep-faq a:focus {
  outline: 0;
}
.nanorep-faq ul {
  list-style: none;
}
.nanorep-faq input:not([type="radio"]):not([type="checkbox"]),
.nanorep-faq textarea,
.nanorep-faq button {
  -webkit-appearance: none;
  -moz-appearance: none;
}
.nanorep-faq input,
.nanorep-faq textarea,
.nanorep-faq select,
.nanorep-faq button {
  outline: none;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  border: none;
  box-shadow: none;
  background-color: transparent;
}
.nanorep-faq textarea {
  resize: none;
}
.nanorep-faq ::-ms-expand {
  display: none;
}
.nanorep-faq ::-webkit-search-cancel-button {
  -webkit-appearance: none;
}
.nanorep-faq ::-moz-placeholder {
  opacity: 1;
}
.nanorep-faq *,
.nanorep-faq *:after,
.nanorep-faq *:before {
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
  -webkit-font-smoothing: antialiased;
  font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  -webkit-text-size-adjust: none;
  -moz-text-size-adjust: none;
  -ms-text-size-adjust: none;
  margin: 0;
  padding: 0;
  color: inherit;
  -webkit-box-sizing: border-box !important;
  -moz-box-sizing: border-box !important;
  box-sizing: border-box !important;
  image-rendering: -webkit-optimize-contrast;
}
.nanorep-faq svg path,
.nanorep-faq svg g,
.nanorep-faq svg use {
  fill: inherit !important;
}
/*---- END Reset ------------------------------*/
/*---- Main -----------------------------------*/
.nanorep-faq {
  min-width: 320px;
  font: 400 14px/1 Arial, sans-serif;
  letter-spacing: .5px;
  color: #1a1a1a;
}
.nanorep-faq img {
  display: block;
  width: 100%;
  max-width: 100%;
}
/*---- Main -----------------------------------*/
/*---- Titles ---------------------------------*/
.title--lg {
  font-size: 52px;
  line-height: 1.1;
  letter-spacing: 1px;
}
.title--md {
  font-size: 24px;
  line-height: 1.17;
}
.title--sm {
  font-size: 16px;
  line-height: 1.5;
}
.title--lg,
.title--md,
.title--sm {
  font-weight: 700;
}
.title__image {
  max-width: 96px;
}
.title__text {
  padding: 0 0 0 15px;
  max-width: calc(100% - 100px);
}
.title__image,
.title__text {
  display: inline-block;
  vertical-align: middle;
}
@media only screen and (max-width: 600px) {
  .title--lg {
    font-size: 24px;
    letter-spacing: .5px;
  }
  .title--image {
    margin: 0 -15px 30px !important;
    padding: 0 0 30px;
    border-bottom: 1px solid #d1d1d1;
  }
  .title__image,
  .title__text {
    display: block;
  }
  .title__image {
    margin: 0 auto 10px;
  }
  .title__text {
    max-width: 100%;
    padding: 0;
    text-align: center;
  }
}
/*---- END Titles -----------------------------*/
/*---- Layout ---------------------------------*/
.section {
  padding: 40px 0;
  border-top: 1px solid #d1d1d1;
}
@media only screen and (max-width: 600px) {
  .section {
    padding: 40px 0;
  }
}
.section__frame {
  width: 100%;
  max-width: 630px;
  padding: 0 15px;
  margin: 0 auto;
}
/*---- END Layout -----------------------------*/
/*---- Header ---------------------------------*/
.header {
  padding: 16px 0;
  line-height: 24px;
}
@media only screen and (max-width: 600px) {
  .header {
    padding: 16px 0 16px;
  }
}
.header:after {
  content: '';
  display: table;
  width: 100%;
  clear: both;
}
.logo {
  float: left;
  position: relative;
  font-size: 16px;
  padding: 1px 0 0 30px;
}
.logo:before {
  content: '';
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: 24px;
  height: 24px;
  background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 20 20'%3E%3Cg fill='%231A1A1A'%3E%3Cpath d='M1.2 10c0 4.86 3.94 8.8 8.8 8.8 4.86 0 8.8-3.94 8.8-8.8 0-4.86-3.94-8.8-8.8-8.8-4.86 0-8.8 3.94-8.8 8.8zM0 10C0 4.48 4.48 0 10 0s10 4.48 10 10-4.48 10-10 10S0 15.52 0 10z'/%3E%3Cpath d='M9.25 13.5V15h1.5v-1.5M7.25 8.2h1.08c0-.28.03-.55.1-.8.06-.26.17-.48.32-.67.15-.18.33-.33.55-.44.22-.12.48-.17.78-.17.46 0 .82.13 1.08.4.27.25.42.6.45 1.08.03.32 0 .58-.1.8-.1.22-.22.42-.37.6-.16.2-.33.36-.5.52-.2.17-.36.35-.53.54-.16.2-.3.4-.42.66-.1.25-.17.55-.2.9v.6h1.08v-.5c0-.2.03-.4.1-.58.06-.17.15-.33.25-.48.12-.15.23-.3.36-.42l.4-.38.4-.42c.13-.14.25-.3.35-.47.1-.17.2-.35.26-.56.06-.2.1-.43.1-.7 0-.4-.08-.75-.2-1.07-.14-.3-.32-.57-.56-.8-.23-.2-.5-.37-.84-.48-.32-.1-.68-.17-1.08-.17-.44 0-.83.08-1.2.23-.34.16-.64.37-.88.65-.25.27-.44.6-.57.96-.13.38-.2.78-.18 1.2z'/%3E%3C/g%3E%3C/svg%3E");
  background-size: 22px 22px;
  background-repeat: no-repeat;
  background-position: center;
}
.switcher {
  display: table;
  float: right;
}
.switcher__item {
  display: table-cell;
  font-size: 12px;
  font-weight: 700;
  text-transform: uppercase;
}
.switcher__item--current {
  margin: 0 0 -2px;
  box-shadow: inset 0 -2px 0 0 #1a1a1a;
}
.switcher__item:not(.switcher__item--current) {
  color: #4e4e4e;
}
.switcher__devider {
  display: table-cell;
  padding: 0 .5px;
  background: #d1d1d1;
  border-left: 16px solid #fff;
  border-right: 16px solid #fff;
}
.switcher__devider:first-child {
  display: none;
}
/*---- END Header -----------------------------*/
/*---- Intro ----------------------------------*/
.intro__title {
  margin: 0 0 32px;
}
.intro__link__wrapper {
  display: inline-block;
}
.intro__link__wrapper + .intro__link__wrapper {
  margin: 0 0 0 18%;
}
.intro__link {
  display: inline-block;
  padding: 0 10px 0 58px;
  line-height: 48px;
  position: relative;
}
.intro__link span {
  padding: 3px 0;
  border-bottom: 2px solid #1a1a1a;
}
.intro__link:before,
.intro__link:after {
  content: '';
  display: block;
  position: absolute;
  background: url('//cdn-customers.nanorep.com/customers/zalando/support-center/images/sprite.png') no-repeat;
}
.intro__link:before {
  left: 0;
  background-size: 118px;
}
.intro__link:after {
  width: 5px;
  height: 9px;
  top: 21px;
  right: 0;
  background-size: 104px;
  background-position: -45px -68px;
}
.intro__link--orders:before {
  width: 41px;
  height: 28px;
  top: 10px;
  background-position: -51px -8px;
}
.intro__link--return:before {
  width: 36px;
  height: 32px;
  top: 8px;
  background-position: -5px -6px;
}
.intro__link--address:before {
  width: 36px;
  height: 32px;
  top: 8px;
  background: url('//cdn-customers.nanorep.com/customers/zalando/support-center/images/ic_delivery_orange_48%402x.png') no-repeat;
  background-size: cover;
}
@media only screen and (max-width: 600px) {
  .intro {
    padding: 0;
    text-align: center;
  }
  .intro__title {
    padding: 32px 0;
    margin: 0;
  }
  .intro__links {
    margin: 0 -15px;
  }
  .intro__link__wrapper {
    width: 100%;
    padding: 25px 0;
    border-top: 1px solid #d1d1d1;
  }
  .intro__link__wrapper + .intro__link__wrapper {
    margin: 0;
  }
  .intro__link {
    padding: 40px 10px 0 0;
    line-height: 1;
  }
  .intro__link span {
    display: inline-block;
  }
  .intro__link:after {
    top: auto;
    bottom: 7px;
  }
  .intro__link:before {
    top: 0;
    left: 50%;
  }
  .intro__link--orders:before {
    margin: 0 0 0 -20.5px;
  }
  .intro__link--return:before {
    margin: 0 0 0 -19px;
  }
  .intro__link--address:before {
    margin: 0px 0px 0px -18px;
  }
}
/*---- END Intro ------------------------------*/
/*---- Answers --------------------------------*/
.answers.answers--inline {
  margin: 55px -9999px 0 !important;
  padding-left: 9999px;
  padding-right: 9999px;
  padding-bottom: 0;
}
.answers__title {
  margin: 0 0 28px;
}
.answers__item + .answers__item {
  margin: 20px 0 0;
}
.answers__link {
  position: relative;
  padding: 0 0 0 32px;
  font-size: 16px;
  line-height: 1.5;
}
.answers__link:before {
  content: '';
  display: block;
  width: 21px;
  height: 23px;
  position: absolute;
  left: 0;
  top: 0;
  background: url('//nr-customers.s3.amazonaws.com/customers/zalando/support-center/images/sprite.png') no-repeat;
  background-size: 141px;
  background-position: -33px -59px;
}
.answers__link span {
  padding: 3px 0;
  border-bottom: 2px solid #1a1a1a;
}
.answers__back {
  margin: 25px 0 0;
  padding: 0 0 0 32px;
  font-size: 16px;
  position: relative;
}
.answers__back:before {
  content: '';
  display: block;
  width: 16px;
  height: 20px;
  position: absolute;
  left: 0;
  top: 0;
  background: url('//nr-customers.s3.amazonaws.com/customers/zalando/support-center/images/sprite.png') no-repeat;
  background-size: 115px;
  background-position: -96px -3px;
}
@media only screen and (max-width: 600px) {
  .answers.answers--inline {
    margin-top: 40px !important;
  }
}
/*---- END Answers ----------------------------*/
/*---- Topics ---------------------------------*/
.topics__title {
  margin: 0 0 4px;
}
.topics__list {
  font-size: 0;
  letter-spacing: 0;
  margin: 0 -12px;
}
.topic {
  display: inline-block;
  width: 25%;
  vertical-align: top;
  border: 12px solid transparent;
}
.topic__link {
  display: block;
  padding: 16px 5px 12px;
  font-size: 14px;
  letter-spacing: .4px;
  font-weight: 700;
  text-align: center;
  border: 1px solid #b3b3b3;
}
.topic__image {
  max-width: 96px;
  margin: 0 auto 10px;
}
.topic__text {
  display: inline-block;
  min-height: 29px;
}
@media only screen and (max-width: 600px) {
  .topics__title {
    margin: 0 0 12px;
  }
  .topics__list {
    margin: 0 -4px;
  }
  .topic {
    width: 50%;
    border-width: 4px;
  }
}
/*---- END Topics -----------------------------*/
/*---- Widget ---------------------------------*/
.widget__title {
  margin: 0 0 25px;
  text-align: center;
}
.query-field {
  position: relative;
  border: 1px solid #9e9e9e !important;
}
.query-field:before {
  content: '';
  display: block;
  width: 19px;
  height: 19px;
  position: absolute;
  top: 12px;
  left: 12px;
  background: url('//nr-customers.s3.amazonaws.com/customers/zalando/support-center/images/sprite.png') no-repeat;
  background-size: 121px;
  background-position: -3px -76px;
}
.query-field__input,
.query-field__placeholder {
  height: 43px !important;
  font: 14px/1.5 Arial, sans-serif !important;
  padding: 11px 25px 11px 43px !important;
  -webkit-text-fill-color: #9b9b9b;
  color: #9b9b9b;
}
.query-field__suggestions {
  width: auto !important;
  border-width: 0 1px 1px 1px !important;
  border-style: solid !important;
  border-color: #9e9e9e !important;
  left: -1px;
  right: -1px;
  z-index: 2;
}
.query-field__suggestion {
  position: relative;
  display: block;
  padding: 16px 15px 16px 46px !important;
  font-size: 14px !important;
  border-top: 1px solid #ccc;
  cursor: pointer;
}
@media only screen and (min-width: 601px) { 
  .query-field__suggestion:hover {
    background: #f0f0f0 !important;
  }
}
.query-field__suggestion--active {
  background: #f0f0f0 !important;
}
.query-field__suggestion:before {
  content: '';
  display: block;
  width: 16px;
  height: 20px;
  position: absolute;
  left: 15px;
  top: 14px;
  background: url('//nr-customers.s3.amazonaws.com/customers/zalando/support-center/images/sprite.png') no-repeat;
  background-size: 119px;
  background-position: -28px -49px;
}
.query--no-result {
  display: none;
  padding: 16px 15px;
  text-align: center;
  border-width: 0 1px 1px 1px !important;
  border-style: solid;
  border-color: #9e9e9e;
}
@media only screen and (max-width: 600px) {
      .query-field__icon--reset {
        width: 16px !important;
        height: 16px !important;
    }
}
/*---- END Widget -----------------------------*/
/*---- Contact --------------------------------*/
.contact__title {
  margin: 0 0 25px;
  text-align: center;
}
.contact__button {
  display: block;
  width: 220px;
  padding: 12px;
  margin: 0 auto;
  font-size: 12px;
  line-height: 1.5;
  font-weight: 700;
  text-align: center;
  color: #fff;
  text-transform: uppercase;
  background: #ff6900;
}
.contact__button + .contact__button {
  margin: 20px auto 0;
}
.contact__button--inline {
  margin: 0;
}
@media only screen and (max-width: 600px) {
  .contact__button--inline {
    width: 100%;
  }
}
/*---- END Contact ----------------------------*/
/*---- Breadcrumbs ----------------------------*/
.breadcrumbs {
  padding: 20px 0;
  border-top: 1px solid #d1d1d1;
}
.breadcrumbs__list {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.breadcrumb,
.breadcrumb__link {
  display: inline;
  vertical-align: middle;
}
.breadcrumb__link {
  font-size: 12px;
  line-height: 1.5;
  font-weight: 700;
  text-transform: uppercase;
  border-bottom: 2px solid #1a1a1a;
}
.breadcrumb__icon {
  display: inline-block;
  width: 7px;
  height: 10px;
  margin: 0 3px 3px 3px;
  background: url('//nr-customers.s3.amazonaws.com/customers/zalando/support-center/images/sprite.png') no-repeat !important;
  background-size: 125px !important;
  background-position: -53px -82px !important;
  vertical-align: middle;
}
/*---- END Breadcrumbs ------------------------*/
/*---- Article --------------------------------*/
.article {
  padding: 32px 0 55px;
  overflow: hidden;
}
.article__title {
  margin: 0 0 15px;
}
.article__body {
  line-height: 1.4;
}
.article__body > h2,
.article__body > h3 {
  font-size: 18px;
}
.article__body > h4,
.article__body > h5,
.article__body > h6 {
  font-size: 16px;
}
.article__body ul,
.article__body ol {
  list-style-position: inside;
}

.article__body ul:not(.answers__list) li:before {
  content: '•';
  display: inline-block;
  padding: 0 10px 0 0;
  vertical-align: top;
}
.article__body > ul > li > p {
  display: inline;
}

.article__body > div > ul > li > p {
  display: inline;
}

.article__body ul li div p {
  display: inline;
}

.article__body > ol > li > ol {
  padding-left: 40px;
}

.article__body a {
  margin-top: 0 !important;
}


.article__body strong,
.article__body b {
  font-weight: bold;
}
.article__body em {
  font-style: italic;
}
.article__body img {
  display: inline-block;
  vertical-align: middle;
}
.article__body * + *{
  margin: 20px 0 0 !important;
}
.answer-footer__feedback,
.answer-footer__channeling {
  float: none;
}
@media only screen and (max-width: 600px) {
  .article {
    padding: 10px 0 40px;
  }
}
/*---- END Article ----------------------------*/
/*---- Feedback -------------------------------*/
.feedback {
  padding: 20px 0;
  border-top: 1px solid #d1d1d1;
}
.feedback-survey {
  padding: 0 !important;
}
.feedback-survey__title,
.feedback-survey__thanks {
  margin: 0 10px 0 0;
}
.feedback-survey__thanks {
  font-weight: 700;
}
.feedback-survey__button {
  min-width: 64px;
  height: 32px;
  margin: 0 5px;
  padding: 5px !important;
  font-size: 12px !important;
  line-height: 1.5;
  font-weight: 700;
  color: #1a1a1a !important;
  border: 2px solid #1a1a1a !important;
  text-transform: uppercase;
}
.feedback-survey__button--inactive {
  opacity: .5;
}
.feedback-survey__separator {
  display: none;
}
.feedback-dialog__title,
.phone-dialog__title,
.contact-form__title {
  color: #1a1a1a !important;
}
.contact-form-buttons__button,
.feedback-dialog__button {
  height: auto;
  padding: 5px;
  font-size: 12px;
  line-height: 1.5;
  font-weight: 700;
  text-transform: uppercase;
  border: 2px solid #1a1a1a;
  color: #1a1a1a !important;
  background: none !important;
}
.contact-form {
  letter-spacing: 0;
}
.feedback-dialog {
    padding: 20px 40px !important
}
.arrow-popup {
  margin: 0;
}
.arrow-popup__closer {
    background-size: 15px !important;
    margin: 0 !important;
    padding: 23px !important;
}
@media only screen and (min-width: 601px) {
  .feedback-survey:not(.feedback-survey--disabled) .feedback-survey__button,
  .channeling-bar__text,
  .contact-form-buttons__button,
  .feedback-dialog__button {
    transition: background .3s ease, color .3s ease;
  }
  .feedback-survey:not(.feedback-survey--disabled) .feedback-survey__button:hover,
  .channeling-bar__text:hover,
  .contact-form-buttons__button:hover,
  .feedback-dialog__button:hover {
    background: #1a1a1a !important;
    color: #fff !important;
  }
}
@media only screen and (max-width: 600px) {
  .feedback {
    padding: 25px 0;
  }
  .feedback-survey__title,
  .feedback-survey__thanks {
    display: block;
    margin: 0 0 20px;
    text-align: center;
  }
  .feedback-survey__button {
    width: 48.75%;
    height: 45px;
    margin: 0;
    line-height: 2.6;
  }
  .feedback-survey__button ~ .feedback-survey__button {
    margin: 0 0 0 2.5%;
  }
  .contact-form-buttons__button,
  .feedback-dialog__button {
    line-height: 2.6;
  }
}
/*---- END Feedback ---------------------------*/
/*---- Channeling -----------------------------*/
.channeling {
  text-align: center;
}
.channeling--custom {
  padding: 75px 0;
}
.channeling__title {
  text-align: center;
}
.channeling-bar,
.channeling-bar__list,
.channeling-bar__text {
  display: block;
}
.channeling-bar__item {
  display: inline-block !important;
  width: 50%;
}
.channeling-bar__item--active .channeling-bar__wrap:before {
  display: none;
}
.channeling-bar__button {
  position: relative;
  margin: 130px 0 0 !important;
  overflow: visible;
}
.channeling-bar__icon-wrapper {
  display: table-cell;
  vertical-align: middle;
  height: auto;
  position: absolute;
  top: -68px;
  left: 0;
  right: 0;
  pointer-events: none;
}
.channeling-bar__custom-icon {
  max-width: 48px !important;
  margin: 0 auto !important;
}
.channeling-bar__text {
  min-width: 220px;
  height: 42px;
  margin: 0 !important;
  padding: 10px !important;
  font-size: 12px !important;
  line-height: 1.5;
  color: #1a1a1a !important;
  font-weight: 700;
  text-transform: uppercase;
  border: 2px solid #1a1a1a;
}
.channeling-bar__item--hidden {
  display: none !important;
}
[data-channel-name*="Down"] {
    opacity: .5;
    pointer-events: none;
}
@media only screen and (max-width: 600px) {
  .channeling--custom {
    padding: 10px 0 40px;
  }
  .channeling__title {
    margin: 0 0 25px;
  }
  .channeling-bar__item {
    width: 100%;
  }
  .channeling-bar__button {
    display: table;
    width: 100%;
    margin: 0 0 10px !important;
    border: 2px solid #1a1a1a !important;
  }
  .channeling-bar__icon-wrapper {
    display: table-cell;
    padding: 15px;
    position: static;
  }
  .channeling-bar__text {
    display: table-cell;
    border: none;
    vertical-align: middle;
    text-align: left;
  }
}
/*---- END Channeling -------------------------*/
/*---- Order ----------------------------------*/

.order:after {
    content: '';
    display: table;
    clear: both;
}

.order h3 {
    margin: 0 0 10px;
    font-size: 22px;
}

.order__number {
    margin: 0 0 10px;
    font-size: 16px;
    font-weight: 600;
    color: #c9c9c9;
}

.order__date {
    margin: 0 0 30px;
    font-size: 24px;
    font-weight: 600;
}

.order__button {
    display: block;
    width: 300px;
    margin: 0 auto 30px;
    padding: 10px 20px;
    font-size: 16px;
    line-height: 25px;
    font-weight: 600;
    text-align: center;
    text-transform: uppercase;
    color: #ff6900;
    border: 2px solid #ff6900;
    transition: background .15s linear, color .15s linear;
}

.order__button:hover {
    background: #ff6900;
    color: #fff;
}

.order__links,
.order__summary {
    font-size: 16px;
    font-weight: 600;
    float: left;
    width: 50%;
}

.order__summary {
    text-align: right;
}

.order__link {
    margin: 0 10px 0 0;
    text-decoration: underline;
}

@media only screen and (max-width: 600px) {
    .order__links,
    .order__summary {
        width: 100%;
        margin: 0 0 15px;
        float: none;
    }

    .order__button:hover {
       color: #ff6900;
       background: transparent;
    }

    .order__summary {
        text-align: center;
    }
}

/*---- Order ----------------------------------*/

#faq-order {
    font-size: 16px;
    text-align: left;
}

#faq-order .order__button {
    margin: 20px auto 10px !important;
}

#faq-order a.plain-link {
    padding: 3px 0;
    border-bottom: 2px solid #1a1a1a;
    color: rgb(26, 26, 26)
}

#faq-order .order-date {
    font-weight: bold;
}

#faq-order .margin-top {
    margin-top: 15px;
}

#faq-order .order-header {
    overflow: hidden;
}

#faq-order .float-right {
    float: right;
    margin: 0 !important;
}

@media only screen and (max-width: 600px) {
    #faq-order {
        margin-bottom: 25px;
    }

    #faq-order .faq-price {
        margin-top:10px !important;
        display:block;
        float:none;
    }
}
  
  
  </style>
  
  

</head>
<body>
<div class="nanorep-faq" data-role="wrapper" id="nanorep-faq">
<header class="header">
<div class="section__frame">
<a class="logo" href="/faq">Help</a>
<div class="switcher">
</div>
</div>
</header>
<main>
<section class="breadcrumbs">
<div class="section__frame">
<ul class="breadcrumbs__list">
<li class="breadcrumb">
<a class="breadcrumb__link" href="#" target="_top">Help</a>
</li>
<li class="breadcrumb">
<span class="breadcrumb__icon">&nbsp;</span>
<a class="breadcrumb__link" href="#">@if(!empty($topic)) {{$topic['name']}} @endif</a>
</li>
</ul>
</div>
</section>
<section class="article">
	
<div class="section__frame" data-nr-arrow-popup-viewport="" data-nr-sharing-viewport="">
@if(!empty($qa)) 	
<h1 class="article__title title--md">{{$qa['question']}}</h1>
<div class="article__body">
{{$qa['answer']}}
</div>

@else

<h1 class="article__title title--md">Nothing Found</h1>

@endif

</div>

</section>
<section class="feedback">
<div class="section__frame" id="feedback">
<div nr="" class="feedback-survey"><strong nr="" class="feedback-survey__title">Help us improve, was this answer helpful?</strong>
<button nr="" class="feedback-survey__button feedback-survey__button--yes" type="button" aria-disabled="false" aria-label="Help us improve, was this answer helpful? Yes">Yes</button>
<span nr="" class="feedback-survey__separator">/</span><button nr="" class="feedback-survey__button feedback-survey__button--no" type="button" aria-disabled="false" aria-label="Help us improve, was this answer helpful? No">No</button>
<div nr="" class="arrow-popup" style="display: none;"><div nr="" class="arrow-popup__content">
<button nr="" class="arrow-popup__closer" title=""></button><div nr="" class="scrollable"></div>
</div><div nr="" class="arrow-popup__decoration"></div></div></div></div>
</section>
<section class="channeling section">
<div class="section__frame" id="channeling">
<h3 class="channeling__title title--md">Can't find what you're looking for?</h3>
<div nr="" class="channeling-bar"><div nr="" class="arrow-popup" style="display: none;">
<div nr="" class="arrow-popup__content"><button nr="" class="arrow-popup__closer" title=""></button>
<div nr="" class="scrollable"></div></div><div nr="" class="arrow-popup__decoration"></div></div>
<div nr="" class="channeling-bar__list"><div nr="" class="channeling-bar__item channeling-bar__item--hidden" data-channel-name="Chat" data-channel-type="3"><div nr="" class="channeling-bar__wrap">
<button nr="" class="channeling-bar__button" type="button" aria-label="Instant chat" aria-pressed="false" nr-tooltip=""
<span nr="" class="channeling-bar__icon-wrapper channeling-bar__icon-wrapper--custom">
<img nr="" class="channeling-bar__custom-icon" src="https://nr1.s3.amazonaws.com/channelling/36D8D72/430D9D5/0/1/ic_contacts_chat_multi_48_2x.png"></span>
<span nr="" class="channeling-bar__text">Instant chat</span></button></div></div><div nr="" class="channeling-bar__item" data-channel-name="Facebook Messenger" data-channel-type="3"><div nr="" class="channeling-bar__wrap"><button nr="" class="channeling-bar__button" type="button" aria-label="Messenger" aria-pressed="false" nr-tooltip=""><span nr="" class="channeling-bar__icon-wrapper channeling-bar__icon-wrapper--custom"><img nr="" class="channeling-bar__custom-icon" src="https://nr1.s3.amazonaws.com/channelling/36D8D72/430D9D5/0/2/ic_contacts_fb_messenger_multi_48_2x.png"></span><span nr="" class="channeling-bar__text">Messenger</span></button></div></div><div nr="" class="channeling-bar__item" data-channel-name="email" data-channel-type="1"><div nr="" class="channeling-bar__wrap"><button nr="" class="channeling-bar__button" type="button" aria-label="Email" aria-pressed="false" nr-tooltip=""><span nr="" class="channeling-bar__icon-wrapper channeling-bar__icon-wrapper--custom"><img nr="" class="channeling-bar__custom-icon" src="https://nr1.s3.amazonaws.com/channelling/36D8D72/430D9D5/0/3/ic_contacts_email_multi_48_2x.png"></span><span nr="" class="channeling-bar__text">Email</span></button></div></div><div nr="" class="channeling-bar__item" data-channel-name="ChatDown" data-channel-type="3"><div nr="" class="channeling-bar__wrap"><button nr="" class="channeling-bar__button" type="button" aria-label="Chat currently unavailable" aria-pressed="false" nr-tooltip=""><span nr="" class="channeling-bar__icon-wrapper channeling-bar__icon-wrapper--custom"><img nr="" class="channeling-bar__custom-icon" src="https://nr1.s3.amazonaws.com/channelling/36D8D72/430D9D5/0/1/ic_contacts_chat_multi_48_2x.png"></span><span nr="" class="channeling-bar__text">Chat currently unavailable</span></button></div></div></div></div></div>
</section>
</main>
<div id="nrBranding"></div>
<link href="//eu1-1.nanorep.com/widget/skins/supportCenter/channels.css" rel="stylesheet">
</div>
