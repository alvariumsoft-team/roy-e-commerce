@include('webshop.header')
    <section class="side_bar">
      <div class="row nomrgpad">
	  <div class="container-fluid">
        
         @include('webshop.sidebar')
		 <div class="gap10"></div>
          <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
 <section class="slider">
	  <section class="login-form">
    				<div class="col-lg-7 col-sm-7 col-xs-12">
    					<h2>Registered customersssss</h2>
						
						
    					<p>We will ask you for the information we need to speed up the purchase process.</p>
						  @if (Session::get('message-success'))
						<div class="alert alert-success">
			            {{Session::get('message-success')}}
                      </div>
					@endif	
					 @if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif	
    					<form name="login_form" method="POST" action={{url('webshop/login')}} id="loginform">
						  <div class="group {{ $errors->has('username') ? ' error' : '' }}">
						    <input type="text" name="username" class="form_input"><span class="highlight"></span><span class="bar"></span>
						    <label class="error-label">Username*</label>
						  </div>
						  <div class="group {{ $errors->has('password') ? ' error' : '' }}">
						    <input type="password"  name="password"  class="form_input" ><span class="highlight"></span><span class="bar"></span>
							@if ($errors->has('password'))
								<i class="error">{{ $errors->first('password') }}</i>
							@endif
						    <label class="error-label">Password*</label>
						  </div>
						  <a href="#">Have you forgotten your password?</a>
						  <div class="group" style="float: left;width: 100%;">
						    <input type="checkbox" class="check-content" name="session" class="form_input"></span>
						    <label style="margin-left: 20px;" class="check-button">Do not end session</label>
						  </div>
						  <!-- <label class="checkbox-inline">
							  <input type="checkbox" class="check-button" id="inlineCheckbox1" value="option1"> Do not end session
						  </label> -->
						  <div class="gap10"></div>
						  <input type="submit" name="submit" class="button buttonBlue" Value="Enter ">
						  <!--<button type="button" class="button buttonBlue">Enter
						  </button>-->
						</form>
					</div>
    				<div class="col-lg-4 col-sm-4 col-xs-12 pull-right">
    					<a href="{{url('/webshop/guestcheckout')}}"><button type="button" class="button">Guest account
						  </button></a>
    					<br /><br />
    					<h3 style="text-align:left !important;">OR</h3>
    					<a href="{{url('/webshop/register')}}"><button type="button" class="button">Create a new account
						  </button></a>
    				</div>

    	</section>

		</section>
		</div>
		<div class="gap10"></div>
    
            <!---end class slider here--->
            <div class="col-lg-12">
              <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                <div class="input_rt">
                  <input class="form_input" name="email" type="email"><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;" class="labletxt">Join our newsletter - Enter your email address*</label> </div>
              <div class="gap10"></div>
              <div class="contact_social">
               <div class="col-lg-8 col-md-10 col-sm-10 col-lg-offset-2 col-md-offset-1 col-sm-offset-1">
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="shopping_f">
                    <h6 class="folloe">Follow us on</h6>
                    <ul class="rhv">
                      <li><img src="{{url('assets/img/facebook.png')}}"></li>
                      <li><img src="{{url('assets/img/instagram.png')}}"></li>
                      <li><img src="{{url('assets/img/pinterest.png')}}"></li>
                      <li><img src="{{url('assets/img/twitter.png')}}"></li>
                      <li><img src="{{url('assets/img/youtube.png')}}"></li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-6  col-md-6 col-sm-12">
                  <div class="shopping_fd">
                    <h6 class="folloe">Download our app</h6>
                    <ul class="rhvz">
                      <li><img src="{{url('assets/img/facebook.png')}}"></li>
                      <li><img src="{{url('assets/img/instagram.png')}}"></li>
                    </ul>
                  </div>
                </div>
				</div>
               
              </div>
            </div>
			</div>
			@include('webshop.footer')
          
        </div>
      </div>
</section>
<div id="my-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><b>NEWSLETTER SUBSCRIPTION</b></h4><br />
				<h6 class="modal-title">Subscribe to our newsletter and receive e-mails all the latest Zara Home products and offers</h6>
            </div>
			
            <div class="modal-body">
			<form name=""  method="POST" action="{{url('/')}}">
{{ Form::token() }}

  <div class="group">
						    <input type="email" name="email" class="form_input"><span class="highlight"></span><span class="bar"></span>
						    <label class="error-label">E-mail*</label>
						  </div>
			   <br>
			    <div class="group" style="float: left;width: 100%;">
						    <input type="checkbox" class="check-content" name="session" class="form_input"></span>
						    <label style="margin-left: 20px;" class="check-button">I have read and accept the  <a href="#" class="policy">Privacy Policy</a></label>
						  </div>
	
			  
			  <br><br>
			  <input type="submit" name="submit" class="button" Value="SUBSCRIBE ME ">
			
			   </form>
            </div>
        </div>
    </div> 
</div>

	
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
$(document).ready(function(){

	 var visited = $.cookie("visited");

	  if (visited == null) {

			$('#my-modal').modal('show');
			
			var date = new Date();
			var minutes = 15;
			date.setTime(date.getTime() + (minutes * 60 * 1000));
			
			$.cookie('visited', 'yes', { expires: date, path: '/' });
	  }
	 $("#loginform").validate({
		 rules: {
	
		username: "required",
           password : "required",
   
			
},
messages: {
		   username: "Please enter your Username",
            password: "Please enter Password",
            

			 
		
	},
   submitHandler: function(form) {
            form.submit();
        }
	 });
		  
});

</script>
	
  </body>

</html>
