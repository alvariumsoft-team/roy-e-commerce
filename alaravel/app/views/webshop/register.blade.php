@include('webshop.header')

 <section class="contact-form contact_sec">
	        <div class="log_in df">	
	               @if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif	
					  @if (Session::get('message-success'))
						<div class="alert alert-success">
			            {{Session::get('message-success')}}
                      </div>
					@endif	
					<div class="alert alert-success success_reg" style="display: none;">
                    </div>

					<?php 	 $sessionvar=	Session::get('data1'); ?>  		  
			        <div class="col-md-7 col-sm-7 col-md-offset-3 col-sm-offset-3">
					<h4 class="text-center"><span class="bck_sec"><a href="{{url('checkout/login')}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                     </span>CREATE YOUR GPC HOME ACCOUNT </h4>	
					  <div class="radio_sec text-center">
					          <label class="radio-inline center cstm_radio"> <input type="radio" name="radio" id="radio-3" class="radio-content"  class="form_input used" checked="checked"  /><span class="checkmark"></span>Individual</label>
						   
					   
						     <label class="radio-inline center cstm_radio"><input type="radio" name="radio" id="radio-4" class="radio-content"  class="form_input used" <?php if(isset($sessionvar)){ echo 'checked=checked';}?>  /><span class="checkmark"></span>Company</label>
						    
						   
		
					   </div>
					@if (Session::get('name'))
					<?php 
					 $sessiondata=	Session::get('name');					
					?>
					 @endif   					
					<form class="create-form"  id="add-form-register">
					    
					    <div class="group">											                  
								<input class="form_input" type="text" name="name"  id="name" value=""  required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorname" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Name*</label>
                                 <div class="md-errors-spacer"><div class="md-char-counter" style="text-align: right;">0 / 128</div></div>								
					   </div>
					   
					   <div class="group">											                  
								<input class="form_input" type="text" name="email" id="eml"   value="" required="required">
								<h3 for="" class="erroremail" style="color: red; margin-top: 4px;"></h3>
								<span class="highlight"></span><span class="bar email"></span>
								<label class="labletxt">E-mail*</label>													
					   </div>
					    <div class="group">											                  
								<input class="form_input" type="password" name="password"  required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorpassword" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>													
					   </div>
					  
					   <div class="sub_sec">
						   <div class="checkbox">						
							<label class="con">I have been able to read and understand the information on the use 
							of my personal data explained in the <a target="_blank" style="color:#9c9494" href="{{url('assets/pdf/')}}/privacy-policy.pdf">Privacy policy</a>
							  <input type="checkbox" class="check-content form_input" value="1" name="policy">
							  <span class="checkmark"></span> 
							   <h3 for="" class="errorpolicy" style="color: red; margin-top: 4px;"></h3> 						  
							</label>						
							</div>
							<div class="checkbox">						
							<label class="con">I want to receive news and customised commercial communications from of
							GPC HOME via email and other means 
							  <input type="checkbox" class="check-content form_input" name="receiveeml" value="1" required="required">
							  <span class="checkmark"></span>
							 						  
							</label>						
							</div>
						</div>
						<input type="hidden" name="individual" value="individual">
						  
						<div class="submit_sec fgd">
							<input type="submit" name="signup" value="CREATE ACCOUNT" class="buttons create-button add-form-register-btn">
						</div>
					</form>

					<?php 
					 $sessionvar=	Session::get('data1');					
					?>

					<form class="create-form" method="POST" action="{{url('webshop/cregister')}}" id="add-form-register-com">
					    
					    <div class="group">											                  
								<input class="form_input" type="text" name="companyname" id="companyname" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<label class="labletxt">Company name*</label>
								<h3 for="" class="errorcompanyname" style="color: red; margin-top: 4px;"></h3>
                                 <div class="md-errors-spacer"><div class="md-char-counter" style="text-align: right;">0 / 128</div></div>								
					   </div>
					   <div class="group">											                  
								<input class="form_input" type="text" name="taxidcode" id="taxidcode" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errortaxidcode1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Tax ID Code (RFC)*</label>													
					   </div>
					   <div class="group">											                  
								<input class="form_input" type="text" name="name" id="firstname" value="" required="required"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorname1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">First name*</label>													
					   </div>
					   <div class="group">											                  
								<input class="form_input" type="text" name="surname" id="lastname" value=""><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorsurname1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Surname*</label>													
					   </div>
					   <div class="group">											                  
								<input class="form_input" type="text" name="chamberofcommerce" id="chamberofcommerce" value=""><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorchamberofcommerce1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Chamber of Commerce Number*</label>													
					   </div>
					   <div class="group">											                  
								<input class="form_input" type="text" name="email" id="email" value=""><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="erroremail1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">E-mail*</label>													
					   </div>
					    <div class="group">											                  
								<input class="form_input" type="password" name="password"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorpassword1" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>													
					   </div>
		
					  <div class="sub_sec">
						   <div class="checkbox">						
							<label class="con">I have been able to read and understand the information on the use 
							of my personal data explained in the <a target="_blank" style="color:#9c9494" href="{{url('assets/pdf/')}}/privacy-policy.pdf">Privacy policy</a>
							  <input type="checkbox" class="check-content form_input" value="1" name="policy">
							  <h3 for="" class="errorpolicy1" style="color: red; margin-top: 4px;"></h3>
							  <span class="checkmark"></span> 						  
							</label>						
							</div>
							<div class="checkbox">						
							<label class="con">I want to receive news and customised commercial communications from of
							GPC HOME via email and other means 
							  <input type="checkbox" class="check-content form_input" name="receiveeml" value="1">
							  <h3 for="" class="errorreceiveeml1" style="color: red; margin-top: 4px;"></h3>
							  <span class="checkmark"></span> 						  
							</label>						
							</div>
						</div>
						<input type="hidden" name="company" value="company">
						  
						<div class="submit_sec fgd">
							<input type="submit" name="signup" value="CREATE ACCOUNT" class="buttons create-button add-form-register-com-btn">
						</div>
					</form>					
			  </div>
			 		 				
           </div>
        </section> 
@include('webshop.nomenu_footer') 

<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
/* for register */
$('.add-form-register-btn').on('click', function(e) {

 e.preventDefault();
  var data = $('#add-form-register').serializeArray();


 $.ajax({

	data:data,
	url:base_url+'webshop/register',
    type:"POST",
	cache:false,

	success: function(response){
		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {

    	  if(res.errors.email){
           $(".erroremail").html(res.errors.email);
           $(".erroremail").fadeIn('slow', function(){
           $(".erroremail").delay(3000).fadeOut(); 
	      });
	       }else{

	       	$(".erroremail").html('');
	       }
	      
	      if(res.errors.name){
	      $(".errorname").html(res.errors.name);
	      $(".errorname").fadeIn('slow', function(){
	        $(".errorname").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".errorname").html('');
	       }
          if(res.errors.password){
	      $(".errorpassword").html(res.errors.password);
	      $(".errorpassword").fadeIn('slow', function(){
	        $(".errorpassword").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".errorpassword").html('');
	       }
          if(res.errors.policy){
	      $(".errorpolicy").html(res.errors.policy);
	      $(".errorpolicy").fadeIn('slow', function(){
	        $(".errorpolicy").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".errorpolicy").html('');
	       }

	      
	     
	      var success = $(".erroremail");
	      scrollTo(success,-100);
	    }
	    else
	    {
		
		$('.success_reg').html(res.Success);
		$(".success_reg").fadeIn('slow', function(){
	        $(".success_reg").delay(3000).fadeOut(); 
	      });
		$('.success_reg').show();
		$('#add-form-register')[0].reset();
		 window.setTimeout(function()
		{
		location.href = base_url+'checkout/login';
		}, 2000);
		//location.reload();

							
	    }
	}
  });

});

/* for register end */

/* for company register */
$('.add-form-register-com-btn').on('click', function(e) {

 e.preventDefault();
  var data = $('#add-form-register-com').serializeArray();


 $.ajax({

	data:data,
	url:base_url+'webshop/cregister',
    type:"POST",
	cache:false,

	success: function(response){
		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {
	      if(res.errors.email){
	        $(".erroremail1").html(res.errors.email);
	        $(".erroremail1").fadeIn('slow', function(){
	        $(".erroremail1").delay(3000).fadeOut(); 
	      });
	       }else{

	       	$(".erroremail1").html('');
	       }

	       if(res.errors.firstname){
		      $(".errorname1").html(res.errors.firstname);
		      $(".errorname1").fadeIn('slow', function(){
		        $(".errorname1").delay(3000).fadeOut(); 
		      });
	        }else{

	       	 $(".errorname1").html('');
	        }
          if(res.errors.password){
		      $(".errorpassword1").html(res.errors.password);
		      $(".errorpassword1").fadeIn('slow', function(){
		        $(".errorpassword1").delay(3000).fadeOut(); 
		      });
	      }else{

	       	 $(".errorpassword1").html('');
	        }
          if(res.errors.surname){
	      $(".errorsurname1").html(res.errors.surname);
	      $(".errorsurname1").fadeIn('slow', function(){
	        $(".errorsurname1").delay(3000).fadeOut(); 
	      });
	      }else{

	       	 $(".errorsurname1").html('');
	        }
          if(res.errors.companyname){
	      $(".errorcompanyname").html(res.errors.companyname);
	      $(".errorcompanyname").fadeIn('slow', function(){
	        $(".errorcompanyname").delay(3000).fadeOut(); 
	      });
	      }else{

	       	 $(".errorcompanyname").html('');
	        }
           if(res.errors.taxidcode){
	      $(".errortaxidcode1").html(res.errors.taxidcode);
	      $(".errortaxidcode1").fadeIn('slow', function(){
	        $(".errortaxidcode1").delay(3000).fadeOut(); 
	      });
	      }else{

	       	 $(".errortaxidcode1").html('');
	        }
			
		if(res.errors.chamberofcommerce){
	      $(".errorchamberofcommerce1").html(res.errors.chamberofcommerce);
	      $(".errorchamberofcommerce1").fadeIn('slow', function(){
	        $(".errorchamberofcommerce1").delay(3000).fadeOut(); 
	      });
	      }else{

	       	 $(".errorchamberofcommerce1").html('');
	        }
          if(res.errors.policy){
	      $(".errorpolicy1").html(res.errors.policy);
	      $(".errorpolicy1").fadeIn('slow', function(){
	        $(".errorpolicy1").delay(3000).fadeOut(); 
	      });
	      }else{

	       	 $(".errorpolicy1").html('');
	        }

	      
	     
	      var success = $(".erroremail1");
	      scrollTo(success,-100);
	    }
	    else
	    {
	      var success = $(".erroremail1");
	      scrollTo(success,-100);
		
		$('.success_reg').html(res.Success);
		$(".success_reg").fadeIn('slow', function(){
	        $(".success_reg").delay(3000).fadeOut(); 
	      });
		$('.success_reg').show();
		$('#add-form-register-com')[0].reset();
		 window.setTimeout(function()
		{
		location.href = base_url+'checkout/login';
		}, 2000);

							
	    }
	}
  });

});

/* for company register end */	
</script>
<script>
$(document).ready(function(){
	var name=$("#name").val();
	if(name!=''){
	$("#name").focus();
	}
	var eml=$("#eml").val();
	if(eml!=''){
	$("#eml").focus();
	}
	var companyname=$("#companyname").val();
	var taxidcode=$("taxidcode").val();
	var firstname=$("#firstname").val();
	var lastname=$("#lastname").val();
	var email=$("#email").val();
	if(email!=''){
		$("#email").focus();
	}
	if(firstname!=''){
		$("#firstname").focus();
	}
	if(lastname!=''){
		$("#lastname").focus();
	}
	if(companyname!=''){
		
		$("#companyname").focus();
	}
	if(taxidcode!=''){
		$("#taxidcode").focus();
	}
	 $("#radio-3").click(function(){
		 $("#add-form-register").show();
		 $("#add-form-register-com").hide();
	 });
	  $("#radio-4").click(function(){
		 $("#add-form-register-com").show();
		 $("#add-form-register").hide();
	 });
	if($("input[type='radio']#radio-3").is(':checked')) {
		$("#add-form-register").show(); 
	 $("#add-form-register-com").hide();
		
	}else{
		 $("#add-form-register-com").show();
		 $("#add-form-register").hide();
	
	}
	

	 
});



</script>
<script>
$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
</script>