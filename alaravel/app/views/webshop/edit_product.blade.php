@extends('adashb')

@section('dash_content')

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker1" ).datepicker();
  } );
  </script>
<link rel="stylesheet" href="{{url('public/assets/css/datepicker.css')}}" />

<h2>Add New Webshop Product</h2>

<p>Add new product.</p>

<form action="" method="post" enctype="multipart/form-data">

<div class="form-group row">
	<label class="col-md-2"><b>Parent Category</b></label>
	<label class="col-md-7">
		<select name="parentcategory" class="form-control procat" placeholder="ParentCategory" required >
			<option value="">Select a parent category</option>
			@foreach($parentcat as $k=>$v)
				<option value="{{$k}}" @if($k == $product->parentcatid) selected @endif>{{$v}}</option>
			@endforeach
		</select>
	</label>
</div>
<div class="form-group row">
	<label class="col-md-2"><b>Category</b></label>
	<label class="col-md-7">
		<select name="category" class="form-control ccat" placeholder="Category" required >
			<option value="">Select a Category</option>
			@foreach($cat as $k=>$v)
				<option value="{{$v->id}}" @if($v->id == $product->catid) selected @endif>{{$v->categoryname}}</option>
			@endforeach
		</select>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Product Name</b></label>
	<label class="col-md-7"><input class="form-control" placeholder="Product Name" required="true" value="{{$product->name}}" name="product_name" type="text"></label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Product Description</b></label>
	<label class="col-md-7">
		<textarea class="form-control" name="description" placeholder="Product Description.." required>{{$product->description}}</textarea>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Composition/Care</b></label>
	<label class="col-md-7">
		<textarea class="form-control" name="composition" placeholder="Composition Care Info" required>{{$product->composition}}</textarea>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Delivery/Return</b></label>
	<label class="col-md-7">
		<textarea class="form-control" name="delivery_return" placeholder="Delivery Return Info" required>{{$product->delivery_return}}</textarea>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Payment</b></label>
	<label class="col-md-7">
		<textarea class="form-control" name="payment" placeholder="Payment Info" required>{{$product->payment}}</textarea>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Warranty</b></label>
	<label class="col-md-7">
		<textarea class="form-control" name="warranty" placeholder="Warranty Info" required>{{$product->warranty}}</textarea>
	</label>
</div>


<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Product Type</b></label>
	<label class="col-md-7">
		<select name="product_type" class="ptype form-control" required>
			<option value="simple" @if($product->type == 'simple') selected @endif>Simple</option>
			<option value="variable" @if($product->type == 'variable') selected @endif>Variable</option>
		</select>
	</label>
</div>

<div class="form-group row">
	<label class="col-xs-12 col-md-2" >
		<div class="list-group">
		  <a href="javascript:void(0);" class="list-group-item p_g_item p_gen active">General</a>
		  <!--<a href="javascript::void(0);" class="list-group-item p_g_item p_ship">Shipping</a>-->
		  <!--<a href="javascript:void(0);" style="display:none;" class="list-group-item p_g_item p_attr">Attributes</a>-->
		  <a href="javascript:void(0);" style="display:none;" class="list-group-item p_g_item p_var">variation</a>
		</div>
	</label>
	<div class="col-md-7" style="background:#fff;min-height:180px;padding:16px;">
			<!-- general -->
			<div class="p_general col-md-12">
			
				<div class="col-md-6">
					<h5>Regular Price</h5>
					<input name="property[regular_price]" class="form-control" @if($product->type == 'simple') value="{{$product->_regular_price}}" @endif />
				</div>
				<div class="col-md-6">
					<h5>Sale Price</h5>
					<input name="property[sale_price]" class="form-control" @if($product->type == 'simple') value="{{$product->_sale_price}}" @endif />
				</div>
				
				<div class="col-md-6">
					<h5>Stock</h5>
					<select name="property[stock]" class="form-control" required>
						<option value="in" @if(($product->type == 'simple') && ($product->_stock == 'in')) selected @endif>In Stock</option>
						<option value="out" @if(($product->type == 'simple') && ($product->_stock == 'out')) selected @endif>Out Of Stock</option>
					</select>
				</div>
				<div class="col-md-6">
					<h5>Quantity</h5>
					<input name="property[quantity]" class="form-control" @if($product->type == 'simple') value="{{$product->_quantity}}" @endif />
				</div>
				
				<div class="col-md-6">
					Ref
					<input name="property[ref]" class="form-control" @if($product->type == 'simple') value="{{$product->_ref}}" @endif />
				</div>
				<div class="row"></div>
				<div class="pccat clearfix" >
				
				</div>
			</div>
			
			<!-- attributes 
			<div class="p_attribute col-md-12 " style="display:none;">
				<div class="col-md-12">
					<button type="button" class="btn btn-default add_attribute">Add Attribute</button>
				</div>
				<div class="col-md-4">
					Name
					<input name="name[]" class="form-control attr_name" />
				</div>
				<div class="col-md-8">
					Value(s):
					<textarea name="values[]" class="form-control attr_value" ></textarea>
				</div>
				<div class="col-md-12 save_attr">
					<button id="save_attr" type="button" class="btn btn-primary">Save Attributes</button>
				</div>
			</div>-->
			
			<!-- variation 
			
			regular price
			sale price
			stock
			weight
			dimension
			description
			image
			sku
			
			-->
			<div class="p_variation col-md-12" style="display:none;">
				<div class="col-md-6">
					<button type="button" class="btn btn-default add_variation">Add Variation</button>
				</div>
				<input type="hidden" name="attr_id" value="" id="attr_id" />
				<div id="attr_var_div">
				@if($product->type == 'variable')
					@foreach($product->metas as $k=>$meta)
						<div class="col-md-12" style="margin-top:15px !important;padding:8px; border-top:1px solid #dedede;">
						<div class="col-md-6">
							Regular Price
							<input name="variation_property[{{$k}}][regular_price]" class="form-control" value="{{$meta['_regular_price']}}" required/>
						</div>
						<div class="col-md-6">
							Sale Price
							<input name="variation_property[{{$k}}][sale_price]" class="form-control"  value="{{$meta['_sale_price']}}" required/>
						</div>
						<div class="col-md-6">
							Stock
							<select name="variation_property[{{$k}}][stock]" class="form-control">
								<option value="in" @if(($product->type == 'variable') && ($product->_stock == 'in')) selected @endif>In Stock</option>
								<option value="out" @if(($product->type == 'variable') && ($product->_stock == 'out')) selected @endif>Out Of Stock</option>
							</select>
						</div>
						<div class="col-md-6">
							Quantity
							<input name="variation_property[{{$k}}][quantity]" class="form-control"  value="{{$meta['_quantity']}}" required/>
						</div>
						
						<div class="col-md-6">
							<div style="cursor:pointer;color: #535644;font-size:14px;" class="">
								<span >Upload Image</span>
								<input type="file" name="variation_image[{{$k}}][]"  multiple /> 
							</div>
						</div>
						<div class="col-md-6">
							Ref
							<input name="variation_property[{{$k}}][ref]" class="form-control"  value="{{$meta['_ref']}}" required/>
						</div>
						
						@foreach($prop as $property)
							<div class="col-md-6">{{$property->name}}
							<select name="variation_property[{{$k}}][{{$property->name}}]" class="form-control">
							<?php $vals = ProductPropertyValues::where('property_id', '=', $property->id)->get(); 
							$mstr = '_'.strtolower($property->name);
							?>
							@foreach($vals as $val)
								<option value="{{$val->name}}" @if($val->name == $meta[$mstr]) selected @endif>{{$val->name}}</option>
							@endforeach
							</select>
							</div>
						@endforeach
						
						</div>
					@endforeach
				@endif
				</div>
			</div>
	</div>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-2" ><b>Feature Image:</b></label>
	<label class="col-xs-12 col-md-7">
		<div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="">
			<span>Upload Image</span>
			<input type="file" name="feature_image"> 
		</div>
	</label>
</div>

<div class="form-group row">	
	<div class="col-lg-12 pim">
		<div class="row">
			<label class="col-xs-12 col-md-2" ><b>Product Images:</b></label>
			<label class="col-xs-12 col-md-6">
				<div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="">
					<span>Upload Image</span>
					<input type="file" name="product_images[]"> 
				</div>
			</label>
			<label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-plus products_images" style="margin-top:22px;"></a></label>
		</div>
	</div>
</div>

<div class="form-group">{{Form::submit('Submit Unit',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>

<script type="text/javascript">
	var caturl = "{{url('/gcat')}}";
	var pcaturl = "{{url('/gpcat')}}";
	var baseurl = "{{url('/')}}";
	$(document).ready(function(){
		
		$('.get_property_values').on('change', function(){
			var id = $(this).attr('data-id');
			$.ajax({
				method: "POST",
				dataType: "json",
				data: {id:id},
				url: baseurl+"/products/propertyvalues/"+id,
				success: function (data) {
					try{
						var html = '<option value="">Select Property Values</option>';
						if(data.values){
							for(i in data.values){
								html += '<option value="'+data.values[i].id+'">'+data.values[i].name+'</option>';
							}
							$('.propvals').html(html);
						}
					}catch(err){
					alert('err');
					}
					
				},
				error: function () {
					alert('e');
				}
			});
		});
		
		$(document).on('click', '#save_attr', function(){
			var names = $('.attr_name');
			var values = $('.attr_value');
			$attr_name = [];
			$attr_value = [];
			
			$.each( names, function( key, value ) {
			  $attr_name.push($(value).val());
			});
			$.each( values, function( key, value ) {
			  $attr_value.push($(value).val());
			});

			
			$.ajax({
				method: "POST",
				dataType: "json",
				data: {attr_name:$attr_name, attr_values:$attr_value},
				url: baseurl+"/webshop/addattr",
				success: function (data) {
					$('#attr_id').val(data);
				},
				error: function () {
					
				}
			});
		});
		
		$('.p_g_item').on('click', function(){
			$(this).parents('.list-group').find('.p_g_item').removeClass('active');
			$(this).addClass('active');
		});
		
		
		$('.add_attribute').on('click', function(){
			$('.save_attr').before('<div class="col-md-4">Name<input name="name[]" class="form-control attr_name" /></div><div class="col-md-8">Value(s):<textarea name="value[]" class="form-control attr_value" ></textarea></div>');
		})
		
		$('.ptype').on('change', function(){
			if($(this).val() == 'simple'){
				$('.p_var').css('display', 'none');
				$('.p_variation').css('display', 'none');
				$('.p_attr').css('display', 'none');
				$('.p_attribute').css('display', 'none');
				$('.p_gen').css('display', 'block');
				$('.p_general').css('display', 'block');
			}
			else{
				$('.p_gen').css('display', 'none');
				$('.p_general').css('display', 'none');
				$('.p_var').css('display', 'block');
				$('.p_variation').css('display', 'block');
				$('.p_var').addClass('active');
				//$('.p_attr').css('display', 'block');
				//$('.p_attr').addClass('active');
				//$('.p_attribute').css('display', 'block');
			}
		});
		
		$('.p_gen').on('click', function(){
			$('.p_general').css('display', 'block');
			$('.p_attribute').css('display', 'none');
			$('.p_variation').css('display', 'none');
		});
		
		$('.p_attr').on('click', function(){
			$('.p_general').css('display', 'none');
			$('.p_attribute').css('display', 'block');
			$('.p_variation').css('display', 'none');
		});
		
		$(document).on('click', '.add_variation',function(){
			var cat = $('.ccat').val();
			var type = $('#attr_var_div').children().length;
			if(1){
				$.ajax({
					method: "POST",
					dataType: "json",
					data: {cat:cat, type:type},
					url: baseurl+"/webshop/getvariations",
					success: function (data) {
						$('#attr_var_div').append(data.html);
					},
					error: function(xhr, status, error) {
						
					}
				});
			}
			else{
				$('.p_general').css('display', 'none');
				$('.p_attribute').css('display', 'block');
				$('.p_variation').css('display', 'none');	
			}
		});
		
		$('.p_var').on('click', function(){
			$('.p_general').css('display', 'none');
			$('.p_attribute').css('display', 'none');
			$('.p_variation').css('display', 'block');
		});
		
		$('.procat').on('change', function(){
			var cid = $(this).val();
			$.ajax({
				method: "POST",
				dataType: "json",
				url: caturl+"/?s="+cid,
				success: function (data) {
					var html = '<option value="">Select a Category</option>';
					for(i in data){
						html += '<option value="'+data[i].id+'">'+data[i].categoryname+'</option>';
						console.log(data[i].categoryname);
					}
					$('.ccat').html(html);
				},
				error: function () {
					
				}
			});
		});
		
		$(document).on('change', '.ccat',function(){
			var cid = $(this).val();
			$('#attr_var_div').html('');
			$.ajax({
				method: "POST",
				dataType: "json",
				url: pcaturl+"/?s="+cid,
				success: function (data) {
					if(data.html != ''){
						$('.pccat').html(data.html);
					}
				},
				error: function () {
					
				}
			});
		});
		
		$('.products_images').on('click', function(e){
			e.preventDefault();
			var html = '<div class="row pimr"><label class="col-xs-12 col-md-2" ></label><label class="col-xs-12 col-md-6"><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class=""><span>Upload Image</span><input type="file" name="product_images[]"> </div></label><label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-close products_images_remove" style="margin-top:22px;"></a></label></div>';
			$('.pim').append(html);
		});
		$(document).on('click', '.products_images_remove', function(e){
			e.preventDefault();
			$(this).parents('.pimr').remove();
		});
		@if($product->type == 'variable')
			$('.ptype').val('variable');
			$('.p_gen').css('display', 'none');
			$('.p_general').css('display', 'none');
			$('.p_var').css('display', 'block');
			$('.p_variation').css('display', 'block');
			$('.p_var').addClass('active');
		@endif
	});
</script>

@stop
