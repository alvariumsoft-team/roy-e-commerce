@include('webshop.web_header')

    <section class="side_bar">
      <div class="container-fluid">
        <div class="row">
		
         @include('webshop.web_sidebar')
		
          <div class="col-sm-9 col-lg-9">
          
					<div class="col-sm-9 col-md-9  col-lg-9 nomrgpad">
					<nav class="navbar navbar-default cat_nav">

				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				  </button>
				</div>
				<!--<div class="collapse navbar-collapse nomrgpad"  id="myNavbar">
				  <ul class="nav navbar-nav aligngap">

					<p class="navbar-text text-uppercase">price : <a href="javascript:void(0);" id="product_price_highest">highest</a> | <a href="javascript:void(0);" id="product_price_lowest">lowest</a></p>			
					<button class="btn btn-default navbar-btn text-uppercase" id="clear_search_terms"><a href="javascript:void(0);" >clear</a></button>
				  </ul>
				</div>-->


</nav></div>

	<div class="col-lg-3  col-md-3 col-sm-3 nomrgpad">
    								<div class="filter-right">
	    				<!--<nav aria-label="Page navigation">
                                             <ul class="pagination pagelinks">
                                                <li><a href="javascript:void(0);" id="display_2">2</a></li>
                                                <li><a href="javascript:void(0);" id="display_4">4</a></li>
                                            </ul>
                                        </nav>-->
										<h3>{{$total_products}} items </h3>
									</div>
    							</div>
					
					<div class="gap20"></div>
	
    					<div class="product-inner">
    						<div class="row" id="product_row">
							
							@foreach($products as $product)
							
    							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 maxon420 display_2_4">
    								<a href="{{url('/webshop/product')}}/{{$product->parentcat[0]->slug}}/{{$product->cat[0]->slug}}/{{ $product->id}}">
    									<img style="height: 152px;" src="{{asset($product->img)}}" alt="{{$product->name}}" class="img-responsive">
									</a>
    								<div class="product-content">
    									<h2>
											{{$product->name}}
    									</h2>
    									@if(isset($product->metas['_regular_price']))
    										<p>{{$product->metas['_regular_price']}}</p>
    									@else
    										<p>{{$product->price}}</p>
    									@endif
    								</div>
    							</div>
							@endforeach
							
							<?php if(!isset($products[0])){echo '<h3 style="text-align:center;">No products available.</h3>';}?>
							
							
    						</div>
							
    					</div>
			
           	 <!---end class slider here--->
			<form method="POST" action="{{url('webshop/subscribe')}}">					
				<div class="row">
				<div class="col-lg-12">
					<div class="footer-spot"></div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-1">
							<div class="group">
								<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;" class="labletxt">Join our newsletter - Enter your email address*</label>
							</div>
				 <!--<div class="group" style="float: left;width: 100%; text-align:center;">
								<input class="check-content" type="checkbox" >
								<label style="margin-left: 20px;" class="check-button">I have read and accept the Privacy policy.</label>
							</div>-->
						<div class="col-lg-12  text-center " >
				  <input  type="submit" class="button" style=" float:none;" name="subscribe" value="SUBSCRIBE ME">
						</div>
						</div>
				</div>
				</div>	   	
			</form> 
</div>
</div>
</div></div>
</section>

@include('webshop.web_footer')
