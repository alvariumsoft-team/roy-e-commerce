<ul class="side_nav">
@if(isset($parentcat))
@if(isset($sup_categories))
	@foreach($sup_categories as $cat)
		<li>
			@if($parentcat=="")
				<a class="sidebar_heading @if($currentcat == $cat->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$cat->url}}">{{$cat->name}}</a>
				<ul class="sale_now @if(($currentcat != null) && $currentcat == $cat->url) show @else hide @endif">
			@else
				@if($ppcat=="")
					<a class="sidebar_heading @if($parentcat == $cat->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$cat->url}}">{{$cat->name}}</a>
					<ul class="sale_now @if(($parentcat != null) && $parentcat == $cat->url) show @else hide @endif">
				@else
					<a class="sidebar_heading @if($ppcat == $cat->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$cat->url}}">{{$cat->name}}</a>
					<ul class="sale_now @if(($ppcat != null) && $ppcat == $cat->url) show @else hide @endif">
				@endif
			@endif
				@foreach($cat->subcategories as $subcategory)
					<li>
						
						@if($ppcat=="")
							<a class="@if($currentcat == $subcategory->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$subcategory->url}}">{{$subcategory->name}}</a>
							@if(isset($subcategory->subcategories[0]))
								<ul class="sale_last @if(($currentcat != null) && $currentcat == $subcategory->url) show @else hide @endif">
							@endif
						@else
							<a class="@if($parentcat == $subcategory->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$subcategory->url}}">{{$subcategory->name}}</a>
							@if(isset($subcategory->subcategories[0]))
								<ul class="sale_last @if(($parentcat != null) && $parentcat == $subcategory->url) show @else hide @endif">
							@endif
						@endif
								@foreach($subcategory->subcategories as $subsubcategory)
									<li><a class="@if($currentcat == $subsubcategory->url) sel_cls @endif" href="{{url('/webshop/category')}}/{{$subsubcategory->url}}">{{$subsubcategory->name}}</a>
								@endforeach
							@if(isset($subcategory->subcategories[0]))
								</ul>
							@endif
					</li>
				@endforeach
			</ul>
		</li>
	@endforeach
@endif
@else
	@if(isset($sup_categories))
		@foreach($sup_categories as $cat)
			<li>
				<a class="sidebar_heading" href="{{url('/webshop/category')}}/{{$cat->url}}">{{$cat->name}}</a>
			</li>
		@endforeach
	@endif
@endif	
<p class="cv">FREE HOME DELIVERY ON <br>PURCHASES OVER £29.99</p>
</ul>