@extends('adashb') 
@section('dash_content')

<!-- Child Categories -->
<div class="side-body padding-top">
    <h2>Orders</h2>
    <table class="table">
        <thead>
            <tr>
                <th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>
                <th>Order Number</th>
                <th>Order Status</th>
                <th>Buyer Name</th>
                <th>Buyer Email</th>
                <th>Total Cost</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{Form::checkbox('listing_action','','')}}</td>
                <td>{{$order->order_number}}</td>
                <td>{{ucwords($order->type)}}</td>
                <td>{{$order->name}} {{$order->surname}}</td>
                <td>{{$order->email}}</td>
                <td>{{$order->total_cost}}</td>
                <td>
                	<a href="{{url('webshop/vieworder')}}/{{$order->id}}" style="font-weight:bold;margin-right:5px;" title="View Order"><i class="fa fa-eye"></i></a>
                    <!--<a href="" style="font-weight:bold;margin-right:5px;">Edit</a>
                    <a href="" style="font-weight:bold;margin-right:5px;" onClick="return confirm('Are you sure');">Delete</a>-->
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <?php echo $orders->links(); ?>
</div>
<div class="modal fade" id="cities-modal" tabindex="-1" role="dialog" aria-labelledby="cities-modal-label" aria-hidden="true">
    <div class="modal-dialog modall-dialog">
        <div class="modal-content modall-content">
            <div class="modal-header modall-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title modall-title" id="cities-modal-label"><b>Cities</b></div>
            </div>
            <div class="modal-body modall-body" id="mbody">
                <div id="modal-cities"></div>
            </div>
        </div>
    </div>
</div>
@stop
