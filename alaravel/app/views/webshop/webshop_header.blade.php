<!doctype html>
<html>
	<head>
		<title>Webshop</title>
		<link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=greek" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/base.css')}}">
		<link rel="stylesheet" type="text/css" href="{{url('public/ideal/ideal-image-slider.css')}}">
		<link rel="stylesheet" type="text/css" href="{{url('public/webshop/cart/style.css')}}">
		<link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css' rel='stylesheet' type='text/css'>
		<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
window.Trengo = window.Trengo || {};
window.Trengo.key = 'SEsUuKNGQ4aVoSVKtVpV';
(function(d, script, t) {
    script = d.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = 'https://static.widget.trengo.eu/embed.js';
    d.getElementsByTagName('head')[0].appendChild(script);
}(document));


</script>
	</head>

<body>
<!--------------------------------Header page Start -------------------------->
	<div class="loader" style="display:none;">Loading</div>
	
	<section class="header_main">
	<div class="container-fluid">
		<div class="row">
			<div class="header_zoras">
				<div class="col-lg-1 col-md-2 col-sm-1 col-xs-2 maxon420">
					<div class="logo_zora clearfix ">
						<a title="go back to homepage, press logo" href="{{url('/')}}"><img src="{{url('packages/realestate/site/logo.png')}}"></a>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-8 maxon420"> 
					<a style="margin-top:30% !important;"title="go back to homepage, press logo" href="{{url('/webshop')}}">
						<span >Go back to homepage, press logo</span> 
					</a>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pull-right">
					<div class="log_sign">
						<ul class="lofgo_sr">
							@if(Auth::check())
							<li>
								<a href="{{url('webshop/logout')}}"><img src="{{url('assets/img/account.png')}}">
								My account - {{Auth::user()->username}} Log Out</a>
							</li>
							@else
							<li>
								<a href="{{url('webshop/login')}}"><img src="{{url('assets/img/account.png')}}">Log In</a>
							</li>
							@endif
							<li>
								<a href="{{url('webshop/basket')}}"   ><img src="{{url('assets/img/cart.png')}}">Basket (<span id="total_items"><?php if(isset($_SESSION['name'])){echo count($_SESSION['name']);}else{ echo 0; } ?></span>)  </a><div id="mycart"></div>
							</li>
						</ul>
					</div>
				<div class="input-group search_um">
					<input type="text" class="form-control SearchBar" placeholder="Search for...">
					<span class="input-group-btn"   style="border-radius: 1px;border: 1px solid #ccc;">			
						<button class="btn btn-defaul SearchButton" type="button">				
							<span class=" glyphicon glyphicon-search SearchIcon" ></span> 
						</button>
					</span>
				</div>
						<!--<div class="basket_mini_info">
							<div class="mini_info">
								hello
							</div>
						</div>-->
				</div>
			</div>
		</div>
	</div>
</section>

<div class="feedback">
    <div class="feedback_content">
		<div class="feedback_cstm">
		    <a href="#" data-toggle="modal" data-target="#nm">Complaint</a>
		</div>
	</div>
 </div>
 <div class="modal fade bs-example-modal-sm" id="nm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <p>Do you have a complaint about us as an online store? Then we have this complaint 
		dealt with by an independent organization you can contact Thuiswinkel.org! </p>
		<p>Click on the link below and follow the 3 steps</p>
		<a href="#">https://www.thuiswinkel.org/consumenten/klacht-indienen</a>
		<p>we still hope to see you back in our online store or website</p>
        <p>Team Global Property Cowboys</p>
    </div>
  </div>
</div>

 <!-- feedback
  ==============================-->
 
 <!-- Hotjar Tracking Code for http://devlaravel.inviolateinfotech.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1407283,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>



