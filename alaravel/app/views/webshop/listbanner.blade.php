@extends('adashb')
@section('dash_content')
	<div class="side-body padding-top">
	<h2>List Banner</h2>
		@if (Session::get('message-success'))
			<div class="alert alert-success">
			{{Session::get('message-success')}}
			</div>
		@endif	
		<table  class="table table-bordered table-striped with-check">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($banner as $ban)
				<tr>
				<td>{{$ban['name']}}</td>
				<td>{{$ban['categoryname']}}</td>
				<td>
					<a href="{{url('/webanner/?wbanid=')}}{{$ban['id']}}" style="font-weight:bold;margin-right:5px;">Edit</a>
					<a href="{{url('/wdbanner/?wbanid=')}}{{$ban['id']}}" style="font-weight:bold;margin-right:5px;" onClick="confirm('Do You Want to Delete ?');">Delete</a>
				</td>
				
				</tr>
				@endforeach
				</tbody>
		</table>
	</div>
	
	
<div class="modal fade" id="cities-modal" tabindex ="-1" role="dialog" aria-labelledby="cities-modal-label" aria-hidden="true" >
	<div class="modal-dialog modall-dialog">
		<div class="modal-content modall-content">
			<div class="modal-header modall-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title modall-title" id="cities-modal-label"><b>Cities</b></div>
			</div>
			<div class="modal-body modall-body" id="mbody">
			<div id="modal-cities"></div>
			</div>
		</div>
	</div>
</div>
@stop

