@include('webshop.header')
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		@include('webshop.left_panel')
	  </div>
	  <div class="col-md-10 col-sm-12">
		
		<div class="col-md-12 col-sm-12 questions">
			<h2 class="faq_sec">SHOPPING GUIDE</h2>		
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default" id="faqs">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<i class="more-less glyphicon glyphicon-plus"></i>
									<span class="icon_panel"><i class="fa fa-question-circle" aria-hidden="true"></i></span>FAQS
								</a>
							</h4>							 
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
							     <h5 class="nmd">Are the online shop prices and those in my usual GPC Home shop the same?</h5>
								 <p>The online shop prices are exactly the same as those in your usual shop, except when there is a
								 special online offer with an additional discount. Additionally, the prices include the VAT of 
								 the destination market.</p>
								 <h5>Can I have my order gift-wrapped?</h5>
								 <p>Most products you order can be gift-wrapped. You can choose this option on the shopping cart page
								 (There might be periods when this option will be deactivated for online purchases, such as sale).</p>
								 <h5>Where can I find articles for children?</h5>
								 <p>In the GPC Home Kids section you will find a complete online shop with a wide range of products for the little ones.</p>
								 <h5>Will I receive the same product as I see in the photo?</h5>
								 <p>Photos are taken with the highest quality to reflect every detail of each product. You may find 
								 differences in products in which the format of the raw materials and their manufacture affect the 
								 final results, such as handmade products.</p>
								 <h5>Can I receive periodic information by e-mail on GPC Home's latest products and offers?</h5>
								 <p>To get all the latest news, laravel.inviolateinfotech.com users can subscribe to a periodic newsletter with information on offers and new products.
								 You can subscribe in the Newsletter section.</p>
								 <h5>Can I cancel my subscription to the GPC Home newsletter?</h5>
								 <p>In all the GPC Home e-mails you receive there is a link for you to cancel your subscription immediately.</p>
								 <h5>Does GPC Home have Customer Service?</h5>
								 <p>Yes, you can contact us through the contact form or call our free Customer Service line at 0800 026 0091 (Mon-Fri 8.30 to 19.30).</p>
								 <h5>Is it possible to cancel an order?</h5>
								 <p>Yes, provided the packaging and dispatch process has not yet begun. You can request cancellation by using the contact
								 form or by telephoning the Customer Service department: 080 00 260 091.</p>
								 <h5>What happens if one of the items received is damaged?</h5>
								 <p>The damaged item will be replaced as soon as possible.To request a replacement please use our contact form or call our free 
								 Customer Service line at 0800 026 0091.</p>
								 <h5>What should I do if one of the items I received is different from the ordered one?</h5>
								 <p>We try to replace the wrong item as soon as possible without extra charge. You can request the change through the contact form
								 or call our free Customer Service line at 0800 026 0091.</p>
								 <h5>What is GPC Online's Privacy Policy?</h5>
								 <p class="term">You will find all the information in the link: <a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy Policy.</a></p>
							</div>
						</div>
					</div>

					<div class="panel panel-default" id="purchasing">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<i class="more-less glyphicon glyphicon-plus"></i>
									<span class="icon_panel"><i class="fa fa-briefcase" aria-hidden="true"></i></span>Purchasing process
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
							    <h5>How can I find a specific article?</h5>
								<p>The products are classified in different categories (Bed, Bath, Table, Living room etc.) and each category, in subcategories.</p>
								<p>Access the corresponding subcategory in the left-hand menu to see the products it contains. There are groups of items in which you can choose the sizes and colours before purchasing.
								You may also use the search bar located at the top of the webpage to locate items.</p>
								<h5>How do I add an item to my basket?</h5>
								<p>Once you have chosen an item, its colour (if applicable), size and quantity, you must click the button 
								"Add to cart" to include the item in your order.</p>
								<h5>What steps do I take to place an order?</h5>
								<ol class="gh">
									<li>
										<p>Choose the section (GPC Home or GPC Home Kids) and choose the category within the section you are interested in.</p>
									</li>
									<li>
										<p>Click on the item you wish to buy and you will see various photos of this item, along with its name, reference number, description, price, colour, available sizes, composition and care.</p>
									</li>
									<li>
										<p>Select a size and add the item to your basket. Then you can either choose 'Continue shopping' or 'Process order'.</p>
									</li>
									<li>
										<p>To process your order there are two options available: you can register or make a guest purchase. </p>
									</li>
									<li>
										<p>If you prefer to register, you will need to enter your personal information, billing address, shipping address and select 'Enter'. </p>
									</li>
									<li>
										<p>Choose if you would prefer to collect your order from one of our GPC Home stores or have it delivered to your home address and click 'Finish and Pay'.</p>
									</li>
									<li>
										<p>Select a payment method: VISA, MasterCard, AMEX, InCard, PayPal or GiftCard. Enter your details and click 'Authorise Payment'.</p>
									</li>
									<li>
										<p>You will then receive an email confirming your order.</p>
									</li>
                                </ol>
								<h5>Can I access my invoice?</h5>
								<p>Yes. You can access to it in the section Orders Placed from your account as soon as the package
								is in the hands of the transportation company.</p>
							</div>
						</div>
					</div>
					
					<div class="panel panel-default" id="payment">
						<div class="panel-heading" role="tab" >
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
									<i class="more-less glyphicon glyphicon-plus"></i>
									<span class="icon_panel"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>Payment
								</a>
							</h4>
						</div>
						<div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
							<div class="panel-body">
							     <h5>What payment method can I use for my purchases?</h5>
								 <p>The following payment methods are available: Visa, Visa electron, Mastercard, American Express, 
								 PayPal, Gift Card, Voucher card and In Card.</p>
								 <ul class="card">
								    <li><img src="{{url('assets/frontend/images/card.jpg')}}" alt="card"></li>
									<li><img src="{{url('assets/frontend/images/card_2.jpg')}}" alt="card_2"></li>
									<li><img src="{{url('assets/frontend/images/card_3.jpg')}}" alt="card_3"></li>
									<li><img src="{{url('assets/frontend/images/card_4.jpg')}}" alt="card_4"></li>
									<li><img src="{{url('assets/frontend/images/card_5.jpg')}}" alt="card_5"></li>
									<li><img src="{{url('assets/frontend/images/card_6.jpg')}}" alt="card_6"></li>
									<li><img src="{{url('assets/frontend/images/card_7.jpg')}}" alt="card_7"></li>
								 </ul>
								  <h5>Why could my credit card be rejected?</h5>
								 <p>The details we request from you must be the same as those on your credit card. A simple typing
								 error can cause the operation to be rejected, so we ask you to complete the payment form carefully.
								 Your credit limit may have been exceeded or the card might have expired.</p>
								 <p>Please contact your bank about any queries you may have about your payment credit card.</p>
								  <h5>Can I receive an invoice showing my company name?</h5>
								 <p>Yes. In the shopping basket, you can indicate whether the purchase is made for an individual or a
								 company. In the event of the latter, you will be asked to provide your tax details.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default" id="delivery">
						<div class="panel-heading" role="tab" >
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefourth" aria-expanded="false" aria-controls="collapsefourth">
									<i class="more-less glyphicon glyphicon-plus"></i>
									 <span class="icon_panel"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>DELIVERY
								</a>
							</h4>
							
						</div>
						<div id="collapsefourth" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfourth">
							<div class="panel-body">
								  <h5>Where can I receive my order?</h5>

									<p>It can be sent to an address selected by you (home, work, delivery point, etc. - never a PO Box)
									or to the GPC Home store of your choice.</p>
									<h5>What is the store delivery process?</h5>

									<p>If you have chosen store delivery, we will send you an email/text message when 
									your order has arrived (Please collect your order during the period of 15 days).
									It is necessary to show the order number and the ID of the person who collects the parcel.
									(Different store collection conditions apply during the sales period).</p>
									<h5>What is the home delivery process?</h5>

									<p>If you have chosen home delivery, we will send you an email confirming that your items have been dispatched
									 (when your order leaves the warehouse) and another one with the tracking number (with a link to the courier's web page).</p>
									<h5>What is the delivery process when using a Delivery Point?</h5>

									<p>You’ll receive a shipping confirmation email when the package is dispatched from the 
									distribution centre and another email confirming the order has arrived to the delivery point and is ready for collection.</p>

									<p>Bear in mind that Delivery Point delivery is not available for all items.</p>
									<h5>How much will I pay in delivery charges?</h5>

									<p>It depends on the purchase amount and the shipping location:</p>
									<table cellspacing="0" cellpadding="0">
											<thead>
												<tr>
													<th>Shipping location</th>
													<th>Purchase amount</th>
													<th>Delivery charges</th>
												</tr>
											</thead>
											<tbody>
												   <tr>
														  <td>Store</td>
														  <td>-</td>
														  <td>FREE</td>
														</tr>
														<tr>
														  <td>Next Day</td>
														  <td>0 - £29.99</td>
														  <td>£4.99</td>
														</tr>
														<tr>
														  <td>Next Day</td>
														  <td>Over £29.99</td>
														  <td>FREE</td>
														</tr>
														<tr>
														  <td>Same Day London</td>
														  <td>-</td>
														  <td>£9.99</td>
														</tr>
														<tr>
														  <td>Delivery point</td>
														  <td>0 - £29.99</td>
														  <td>£1.99</td>
														</tr>
														<tr>
														  <td>Delivery point</td>
														  <td>Over £29.99</td>
														  <td>FREE</td>
												   </tr>
											</tbody>
                                    </table>
									 <h5>Can the market of delivery be different from the market where the order was placed?</h5>
									<p>No. Each market has its online shop with its own products, prices and delivery charges that 
									may vary. When you access the web site for the first time, you choose the market where you want
									the order sent. You can change the market of delivery on this page.<p>
                                     <h5>How long will my order take to arrive?</h5>
									<p class="jkl">SHOP PICK-UP: At the GPC HOME of your choice in 4-6 working days.</p>
									<p class="jkl">NEXT DAY: Orders placed before 14:00 will be delivered in 1 working day (+24/48 h on Weekends and remote areas).</p>
									<p class="jkl">SAME DAY LONDON : Orders placed before 13:00 will be delivered on the same day from 18:00 onwards.</p>
									<p class="jkl"> Delivery not available on Sundays and bank holidays.</p>
									<p class="jkl">DELIVERY POINT: You will receive it within an estimated period of 3-5 working days.</p>
									 <h5>Can I track the status of my order?</h5>
                                     <p>Yes, in section Orders Placed of your account you will see the status of your orders 
									 and have a link to track their exact location once they are being processed by the transport
									 company. You will also receive an e-mail with a link for tracking the package as soon as it
									 is ready for dispatch.</p>
							</div>
						</div>
					</div>
					
					<div class="panel panel-default" id="returns">
						<div class="panel-heading" role="tab" >
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefifth" aria-expanded="false" aria-controls="collapsefifth">
									<i class="more-less glyphicon glyphicon-plus"></i>
									<span class="icon_panel"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></span>Return policy
								</a>
							</h4>
						</div>
						<div id="collapsefifth" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfifth">
							<div class="panel-body">
								 
                              <p>Take the time to try on all the items.</p>

<p>If you decide to return item(s), you can fill out the return form in your package and send it back to us together with your item(s).</p>

<p>Please note that the items you wish to return must be returned to us within 30 days from the date of receipt of your package.</p>

<p>The return amount of an order for which you pay afterwards will be deducted from any outstanding costs on your own account. With the other payment methods, the return amount will be refunded via the payment method used by you.</p>

<p>If you have lost the return form, you can print out a new form here.</p>

<p>Each package contains a sticker with the return address. If you no longer have a sticker, you can use the return 
address and answer number below on your parcel.</p>
<p class="jkl">GPC Returnservice</p>
<p class="jkl">Reply number 55555</p>
<p class="jkl">1107 YR AMSTERDAM</p>
<p class="term">Click <a href="{{url('webshop/return-conditions')}}">HERE</a> for our return conditions.</p>


<h5>RETURNING OPTIONS</h5>

<p>Returns via PostNL or DHL ServicePoint (€0.99)/ Free for Members</p>


 

							</div>
						</div>
					</div>
				
														
				   </div>
			</div>	
	        </div>
      </div>		
	</div>

</section> 

@include('webshop.footer')
<script>
 $(".foot_anch").on('click', function (ev) {
      ev.preventDefault();
     
      document.location.href = $(this).attr('href'); 
      document.location.reload();
     
});
</script>