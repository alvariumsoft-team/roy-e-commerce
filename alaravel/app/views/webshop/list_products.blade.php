@extends('adashb')
@section('dash_content')
	<div class="side-body padding-top">
	<h2>List Products</h2>
		<table  class="table table-bordered table-striped with-check">
				<thead>
					<tr>
						<th>Product Name</th>
						<th>Category Name</th>
						<th>Size</th>
						<th>price</th>
						<th>Quantity</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($products as $prod)
				<tr>
				<td>{{$prod['productname']}}</td>
				<td>{{$prod['categoryname']}}</td>
				<td>{{$prod['size']}}</td>
				<td>{{$prod['price']}}</td>
				<td>{{$prod['quantity']}}</td>
				<td>
					<a href="{{url('/products/edit')}}/{{$prod['id']}}" style="font-weight:bold;margin-right:5px;">Edit</a>
					<a href="{{url('/wdproducts/?wprodid=')}}{{$prod['id']}}" style="font-weight:bold;margin-right:5px;" onClick="confirm('Do You wanr to Delete?');">Delete</a>
				</td>
				</tr>
				@endforeach
				</tbody>
		</table>
	</div>
	
	<div style="text-align:center;padding:10px;padding-top:50px;padding-bottom:50px;">{{$prodpage->links()}}</div>

<div class="modal fade" id="cities-modal" tabindex ="-1" role="dialog" aria-labelledby="cities-modal-label" aria-hidden="true" >
	<div class="modal-dialog modall-dialog">
		<div class="modal-content modall-content">
			<div class="modal-header modall-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title modall-title" id="cities-modal-label"><b>Cities</b></div>
			</div>
			<div class="modal-body modall-body" id="mbody">
			<div id="modal-cities"></div>
			</div>
		</div>
	</div>
</div>
@stop

