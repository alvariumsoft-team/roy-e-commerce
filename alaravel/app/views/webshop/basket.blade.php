@include('webshop.header')
<style type="text/css">
.product_quantity
{
background: none;
border: none;
width: 100%;
color: #000;
}
</style>
<section class="contact-form">
	<div class="basket">
		<div class="container-fluid">
		  <div class="col-md-12 col-sm-12">
			 <div class="basket_pop">
				<h2>Your basket </h2>
				<div class="contentBanner">
                  <p class="citaCesta">During the sale period, your order might suffer a slight delay</p>
                  <p class="citaCesta">We inform you that temporarily some stores will not be available for PICK-UP IN STORE.
		            We are sorry for the inconvenience</p>
                </div>
             </div>
		  </div>

           	@if(!isset($products[0]))
           	<div class="col-md-12 col-sm-12">
           	@else
           	<div class="col-md-9 col-sm-9">
           	@endif

		    <?php
			$items = 0;
			$total = 0;
			$totaltax=0;
			?>

		  @if(!isset($products[0]))
		    <div class="col-md-12 col-sm-12 text-center">
					<div class="gap10"></div>
					<img src="{{url('/assets/img/payment/cart-icon.png')}}" class="img-responsive center-block">
					<div class="gap20"></div>
					<h4 class="emp_sec">Your Basket is empty. </h4>
					 <div class="gap_sec">
					<!-- <p>Add products to the basket from. <a href="#">My Wishlist</a></p> -->
					</div>
	        </div>

		  @else


             <!-- <div class="col-md-9 col-sm-9"> -->
            <div class="product_buy basket_parent">
            	@foreach($products as $session)
				    <?php
					$items = $items + $session->quantity;
					$total = $total + ($session->quantity * $session->price);
					$totaltax= $totaltax +($session->quantity * $session->price * $tax / 100);
					?>
				<div class="product_leftimg">
					<a href="">
					   <img src="{{url('public/webshop/bigproducts')}}/{{$session->img}}" style="width:100px" class="img-responsive" alt="{{$session->img}}">
				    </a>
				</div>
				<div class="product_rightsec">
					<div class="product_rightinfo">
						<div class="col-md-6 col-sm-6 col-xs-6">
						     <h5><a href="{{url('/webshop/product')}}/{{$session->products->url}}">{{$session->name}}</a></h5>
						</div>
                         <div class="col-md-6 col-sm-6 col-xs-6">
							<span class="we"><a href="javascript:void(0);" data-id="{{$session->id}}" class="bskt_remove_product"><i class="fa fa-times" aria-hidden="true"></i></a></span>
							<span class="qp" style="margin-right: 10px;"><a>Incl. Tax</a></span>
							<span class="qp" style="margin-right: 33px;"><a>Tax</a></span>
							<span class="qp" style="margin-right: 43px;"><a>Excl. Tax</a></span>
						</div>

					</div>
					<div class="item_description">
						<div class="col-md-3 col-sm-3">
										@if($session->product_variation_id==0)
											<span class="reference">ref. {{$session->products->sku}}</span>
										@else
											<span class="reference">ref. {{$session->prvars->sku}}</span>
										@endif
										<div class="item_prop">
											<div class="props">
												<span ng-bind-html="::shopCartItem.sizeToShow">
													{{$session->attributes}}
												</span>
											</div>
										</div>

						</div>
						<div class="col-md-2 col-sm-2">
						   <div class="io">
							<span class="rebajado">{{Config::get('view.currency')}}{{number_format($session->price,2)}}</span>
								<span class="tachado"></span>
						   </div>
						</div>
						<div class="col-md-2 col-sm-2">
							<ul class="bskt_list">
								<li><a href="javascript://void(0);"><i class="fa fa-minus quantity_remove"  data-id="{{$session->id}}" aria-hidden="true"></i></a></li>
								<li style="width: 16px;">
									<input type="hidden" class="prod_quant" value="@if($session->product_variation_id==0){{$session->products->quantity}}@else{{$session->prvars->quantity}}@endif">
									<input class="product_quantity{{$session->id}} product_quantity" type="text" minlength="1"  max="@if($session->product_variation_id==0){{$session->products->quantity}}@else{{$session->prvars->quantity}}@endif" value="{{$session->quantity}}">
					               <input class="product_price{{$session->id}} product_price" type="hidden" value="{{$session->price}}">
								   <input class="product_tax{{$session->id}} product_tax" type="hidden" value="{{$tax}}">
								</li>
								<li><a href="javascript://void(0);"><i class="fa fa-plus quantity_add" data-id="{{$session->id}}" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4">
							<ul class="pro_price">
									<li class="total final-price-single{{$session->id}}">
										{{Config::get('view.currency')}}{{number_format($session->quantity * $session->price,2)}}
									</li>

										<?php

											$totalPrice = $session->quantity * $session->price;


											$finalTotal = ($totalPrice * $tax)/100;

									   ?>
									<li class="total total-single-tax{{$session->id}}">
										{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}
									</li>
									<li class="total final-price-single-tax{{$session->id}}">


										{{Config::get('view.currency')}}{{number_format($finalTotal + $totalPrice,2)}}
									</li>
                            </ul>
						</div>

				</div>
             </div>

		      <!-- </div> -->
		     @endforeach

		    <div class="col-lg-12 col-md-12 col-sm-12 process_btn">.
				@if(isset($products[0]))
					<a href="javascript:void(0);" class="col-md-2 col-sm-4 col-xs-5 bskt_empty btn-empty " >Empty Basket</a>
			    @endif
				<!-- <a href="{{url('/webshop')}}" style="margin-right: 10" class="col-md-2 btn-empty" >Continue Shopping</a> -->
		    </div>
		  </div>


          </div>
          <div class="col-md-3 col-sm-3">
				<div class="order-summary">
					<h2>ORDER SUMMARY </h2>
					<span class="total-items">({{$items}} item)</span>
				</div>
				<div class="order_sec">
					<div class="row">
						<div class="col-md-8 col-sm-8">
							<span class="item">Total items</span>
						</div>
						<div class="col-md-4 col-sm-4">
							<span class="item_res final-price">{{Config::get('view.currency')}}{{number_format($total,2)}}</span>
						</div>
					</div>


                    <div class="row" style="margin-top: 15px;">
						<div class="col-md-8 col-sm-8">
							<span class="item">Tax</span>
						</div>
						<div class="col-md-4 col-sm-4">
							<span class="item_res total-tax">{{Config::get('view.currency')}}{{number_format($totaltax,2)}}</span>
						</div>
					</div>
			    </div>


				<div class="cart-total">
						<div class="col-md-8 col-sm-8 total-title nm_sec"> <span> TOTAL: </span> </div>
						<div class="col-md-4 col-sm-4 total-price final-price-tax nm_sec2">


						{{Config::get('view.currency')}}{{number_format($totaltax + $total,2)}}</div>
			    </div>
			    <input type="hidden" class="total-items-val" value="{{$items}}" />
		        <input type="hidden" class="product-total" value="{{number_format($total,2, '.', '')}}" />
				<input type="hidden" class="product-tax-total" value="{{number_format($totaltax,2, '.', '')}}" />
				<p class="store"><!-- You still need £26.00 to get free standard delivery! --> </p>
				<div class="free_store">
					<h5><span></span></h5>
				</div>
				<div class="promote-sec">
					<p>DO YOU HAVE A PROMOTIONAL CODE? Enter it when you reach the payment page </p>
				</div>
				<div class="process_btn">
					<a href="{{url('/checkout/login')}}">process order</a>
				</div>
          </div>

          @endif



		</div>

    </div>
</section>
@include('webshop.nomenu_footer')

<script type="text/javascript">
var tax      = "{{Config::get('view.tax')}}";
var currency = "{{Config::get('view.currency')}}";
var baseurl  = "{{url('webshop')}}";
</script>
<script src="{{url('assets/frontend/script/basket.js')}}"></script>
<script>
/*jQuery("#chk_order_quantity").on('click', function(){
		jQuery.ajax({
			method: "POST",
			url: baseurl+'/checkproquant',
			success:function(response) {
				var response = JSON.parse(response);
				console.log(response);
				console.log(response.ok);
				console.log(response.mess);
				if((response.ok==1) && (response.mess==0)){
					alert("hello");
				 window.location.href="{{URL::to('/checkout/login')}}";
				}
				else if((response.ok==0) && (response.mess==1)){
					alert('Maximum available stock for the product has been added in to cart.');
				}
			}
		});
	});
*/
</script>
