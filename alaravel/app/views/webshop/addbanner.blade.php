@extends('adashb')

@section('dash_content')

<h2>Add New Banner</h2>
<form action="" method="post" enctype="multipart/form-data">




<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Name</b></label>
	<label class="col-md-6">{{Form::text('name','',array('class'=>'form-control','placeholder'=>' Name','required'=>'true'))}}</label>
</div>
<div class="form-group row">
	<label class="col-md-3"><b>Parent Category</b></label>
	<label class="col-md-6">{{Form::select('parent',$parents,'BHK',array('class'=>'form-control','placeholder'=>'Parent Category'))}}</label>
</div>
<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Image</b></label>
	<label class="col-xs-12 col-md-6"  for="ifile-id2" >
	<div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd">
	<span id='ifile-labe'  class='ifile-label'>Upload Image</span>
	<input type="file" name="image[]"  class="inputfile inputfile-1 ifile-id"  id="ifile-id2" multiple> 
	</div></label>
</div>

<div class="form-group">{{Form::submit('Submit Unit',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>

@stop
