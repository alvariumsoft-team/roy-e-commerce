<style type="text/css">
	.error{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES  </a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>																				
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-9 col-md-offset-1">		
			<div class="address_sec">
				<h2>delivery addresses</h2>
				<p>Store all the delivery addresses you want (your home, work, family, etc.) to avoid
				having to enter the delivery address each time you place an order.</p>
				<div class="col-md-4 col-sm-4">
				<div class="empty_card">

					@if(isset($shipping))
					<h4>{{$shipping->name}}</h4>
					<h4>{{$shipping->shipping_address}} {{$shipping->floor_door}} {{$shipping->shipping_city}}</h4>
					<h4>{{$shipping->shipping_postalcode}} {{$shipping->province}}</h4>
					<h4>{{$shipping->telephone}}</h4>
					<a href="#" data-toggle="modal" data-target="#myModal34"><i class="fa fa-edit"></i> Change</a>
					@else

					<a href="#" data-toggle="modal" data-target="#myModal34">Add address</a>

					@endif
					<div class="modal fade" id="myModal34" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					      <div class="modal-content">
						  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
						  </div>
					      <div class="modal-body">
					        <section class="contact-form contact_sec">
						    <div class="log_in df">		  		  			 										
										<form method="POST" action="{{url('customer/address')}}">
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<input class="form_input @if(isset($shipping->name))used @endif" type="text" name="name" value="@if(isset($shipping->name)){{$shipping->name}} @endif"><span class="highlight"></span><span class="bar"></span>
													<label class="labletxt">Name*</label>
				                                    <div class="md-errors-spacer"><div class="md-char-counter">0 / 128</div></div>									
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input if(isset($shipping->surname)) used @endif" type="text" name="surname" value="@if(isset($shipping->surname)){{$shipping->surname}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Surname(s)*</label>
														<div class="md-errors-spacer"><div class="md-char-counter">0 / 128</div></div>
											   </div>
											</div>
					                        <div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->billing_address))used @endif" type="text" name="billing_address" value="@if(isset($shipping->billing_address)){{$shipping->billing_address}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Address*</label>
					                           			<div class="md-errors-spacer"><div class="md-char-counter">0 / 30</div></div>						
											   </div>
											</div>							
											<!-- <div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input" type="text"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Complete address*</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter">0 / 30</div></div>									
											   </div>
											</div> -->
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->floor_door)) used @endif" type="text" name="floor_door" value="@if(isset($shipping->floor_door)){{$shipping->floor_door}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Floor/door/etc.*</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter">0 / 30</div></div>									
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->billing_postalcode))used @endif" type="text" name="billing_postalcode" value="@if(isset($shipping->billing_postalcode)){{$shipping->billing_postalcode}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Postcode*</label>                                							
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->billing_city))used @endif" type="text" name="billing_city" value="@if(isset($shipping->billing_city)){{$shipping->billing_city}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">City*</label>                                							
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input" type="text" name="telephone" value="@if(isset($shipping->telephone)){{$shipping->telephone}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Telephone*</label>                                							
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->province)) used @endif" type="text" name="province" value="@if(isset($shipping->province)){{$shipping->province}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Province*</label>                                							
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">											                  
														<input class="form_input @if(isset($shipping->nif))used @endif" type="text" name="nif" value="@if(isset($shipping->nif)){{$shipping->nif}} @endif"><span class="highlight"></span><span class="bar"></span>
														<label class="labletxt">Tax ID number(NIF)*</label>                                							
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="submit_sec fgd">
												    <input class="buttons" type="submit" value="ACCEPT">
											    </div>
											</div>
											
										</form>											 		 				
					        </div>
					        </section> 
					      </div>    
					      </div>
					  </div>
					</div>

				</div>
				</div>
				<div class="billing_main">
					<a href="#">See my billing address </a>
				</div>				
			</div>
           		
	  </div>

</section> 


@include('webshop.inner.inner_footer')
