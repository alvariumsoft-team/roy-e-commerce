@include('webshop.web_header')

<!-- slider
  ==============================-->
<section class="slider">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <img src="{{url('assets/frontend/images/special_prices.jpg')}}"/>
                <div class="RCSlideoverlay">
                    <h1>Special prices</h1>
                    <p>On over 500 selected items</p>
                    <a href="#">Shop now</a>
                </div>
            </li>
            <li>
                <div class="RCSlideoverlay_2">
                    <h1>Welcome baby</h1>
                    <p>Get your home ready for the<br> arrival of a new baby</p>
                    <a href="#">Discover me</a>
                </div>
                <img src="{{url('assets/frontend/images/welcome_baby.jpg')}}"/>

            </li>
            <li>
                <img src="{{url('assets/frontend/images/high_summer.jpg')}}"/>
                <div class="RCSlideoverlay_3">
                    <h1>High<br> Summer</h1>
                    <p>Greece 2019</p>
                    <a href="#">Discover more</a>
                </div>
            </li>
            </li>

        </ul>
    </div>
</section>


<!-- discount
  ==============================-->
<section class="discount">
    <div class="container">
        <div class="col-md-12 col-sm-12">
            <p><span class="news_f_1 bold">10% DISCOUNT</span>SUBSCRIBE TO OUR NEWSLETTER AND GET A 10% DISCOUNT CODE FOR YOUR NEXT ONLINE PURCHASE
                <span class="news_f_1 bold">10% DISCOUNT</span></p>
            <h3><span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span><a href="#">subscribe me</a></h3>
            <h6 class="news_f_3 news_small news_small_tregalo">* This discount cannot be combined with other promotions or applied to articles already on sale or to the Gift Card</h6>
        </div>
    </div>
</section>



<!-- new
 ==============================-->
<section class="new">
    <div class="container-fluid no-padd">
        <div class="col-md-12 col-sm-12 no-padd">
            <a  href="{{url('/webshop/category')}}/{{$categories[6]->url}}">
            <img src="{{url('assets/frontend/images/nueva_collection.jpg')}}" class="img-responsive" alt="nueva-collection">
            <h1><!-- <a href="#">new in</a> -->

                    {{$categories[6]->name}}
            </h1></a>
        </div>
    </div>
</section>




<!-- new
  ==============================-->
<section class="category">
    <div class="container-fluid no-padd">
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[1]->url}}">
                <img src="{{url('assets/frontend/images/cama.jpg')}}" class="img-responsive" alt="cama">
                <h1>{{$categories[1]->name}}</h1></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[0]->url}}">
                <img src="{{url('assets/frontend/images/bano.jpg')}}" class="img-responsive" alt="bano">
                <h1>{{$categories[0]->name}}</h1></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[3]->url}}">
                <img src="{{url('assets/frontend/images/mesa.jpg')}}" class="img-responsive" alt="mesa">
                <h1>{{$categories[3]->name}}</h1></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[8]->url}}">
                <img src="{{url('assets/frontend/images/deco.jpg')}}" class="img-responsive" alt="deco">
                <h1>{{$categories[8]->name}}</h1></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[4]->url}}">
                <img src="{{url('assets/frontend/images/fragancias.jpg')}}" class="img-responsive" alt="fragancias">
                <h1>{{$categories[4]->name}}</h1></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 no-padd">
            <div class="category-content">
                <a  href="{{url('/webshop/category')}}/{{$categories[5]->url}}">
                <img src="{{url('assets/frontend/images/kids.jpg')}}" class="img-responsive" alt="kids">
                <h1>{{$categories[5]->name}}</h1></a>
            </div>
        </div>
    </div>
</section>

<!--feedback
 ==============================-->

<section class="flask" id="feedback">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="flask_cntr">
                    <h3>Our Selection</h3>
                    <div class="owl-carousel" id="testi_slide">

                        @foreach($products as $row)
                        
                        <div>
                            <div class="sec_main">
                              <a href="{{url('/webshop/product')}}/{{$row->url}}">
                                <img src="{{url('public/webshop/bigproducts')}}/{{$row->images[0]}}" alt="product image" class="img-responsive">
                              </a>
                              <a href="{{url('/webshop/product')}}/{{$row->url}}">
                                <p>{{$row->name}}</p>
                              </a>
                              <a href="{{url('/webshop/product')}}/{{$row->url}}">  
                                <p>{{Config::get('view.currency')}}{{number_format($row->retailprice,2)}}</p>
                               </a> 
                            
                            </div>
                        </div>
                        @endforeach
                       
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('webshop.web_footer')