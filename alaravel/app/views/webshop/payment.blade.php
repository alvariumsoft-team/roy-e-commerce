@include('webshop.header')
 <section class="contact-form">
	<div class="shipping">
		<div class="container-fluid">
		  <div class="col-md-8 col-sm-8">
		      <div class="col-md-12 col-sm-12">
				 <div class="shipping_sec">
					<span class="bn">1. SHIPPING</span>
					<span class="bn">></span>
					<span class="bold_sec">2. PAYMENT</span>
					<h2>Payment detail </h2>
					<p>Please select a payment method: </p>
				 </div>
				 <div class="card_payment">
					<ul>
						<li><a href="#"><img src="{{url('/assets/images/visa.jpg')}}" class="img-responsive" alt="visa"><br>VISA</a></li>
						<li><a href="#"><img src="{{url('/assets/images/master.jpg')}}" class="img-responsive" alt="master"><br>MasterCard</a></li>
						<li><a href="#"><img src="{{url('/assets/images/amex.jpg')}}" class="img-responsive" alt="amex"><br>AMEX</a></li>
						<li><a href="#"><img src="{{url('/assets/images/paysec.jpg')}}" class="img-responsive" alt="paysec"><br>PayPal</a></li>
						<li><a href="#"><img src="{{url('/assets/images/zara.jpg')}}" class="img-responsive" alt="zara"><br>GiftCard</a></li>
						<li><a href="#"><img src="{{url('/assets/images/in.jpg')}}" class="img-responsive" alt="in"><br>Discount</a></li>
						<li><a href="#"><img src="{{url('/assets/images/maestro.jpg')}}" class="img-responsive" alt="maestro"><br>Maestro</a></li>
					</ul>
					<div class="pay_bar">
						<p>You have selected the payment method: <span>Discount</span></p>
					</div>
				 </div>
				</div>
			     <div class="col-md-7 col-sm-7">
					<div class="card_detail">
						<form>
							<div class="col-md-12 col-sm-12">
								<div class="group">
									<input class="form_input" type="text"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt">Card number*</label>
								</div>
							</div>
							<div class="col-md-8 col-sm-8">
								<div class="group cust_sec">
									<input class="form_input" type="number"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt">CVV*</label>
                                    <span class="question_sec"><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="group">
									<input class="form_input" type="text"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt">Enter the code in the image below*</label>

								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="group">
									<input class="form_input" type="text"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt">YY*</label>
								</div>
							</div>
						</form>
						<div class="col-md-12 col-sm-12">
								<div class="go_back">
									<a href="#"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>go back</a>
								</div>
						</div>
					</div>
				 </div>

				 <?php
				$items = 0;
				$total = 0;
				?>
				<div class="col-md-12 col-sm-12">

                    @foreach($products as $session)
					<?php
					$items = $items + $session->quantity;
					$total = $total + ($session->quantity * $session->price);
					?>
					 <div class="prosec">
						<div class="product_buy">
							<div class="product_leftimg bskt_pruduct_img">
								<img src="{{url($session->img)}}" class="img-responsive" alt="towel">
							</div>
							<div class="product_rightsec">
								<div class="product_rightinfo">
									<h5><a href="#">{{$session->name}}</a><span class="op">quantity</span></h5>

								</div>
								<div class="item_description">
									<div class="item_prop">
										<span class="reference">ref. 4155101340232</span>
										<div class="props"> <span ng-bind-html="::shopCartItem.sizeToShow">30 x 50 cm</span> </div>
									</div>
									<div class="item_quantity">
										<ul>
											<li>{{$session->quantity}}</li>
										</ul>
										<span class="total">
											${{$session->quantity * $session->price}}
										</span>
									</div>

								</div>
							</div>
						</div>
					  </div>

                     @endforeach
					   <div class="change">
						<a href="{{url('/webshop/basket')}}">
						   change
						</a>
				       </div>
				</div>


		  </div>
          <div class="col-md-4 col-sm-4">
				<div class="order-summary">
					<h2>ORDER SUMMARY </h2>
					<span>({{$items}} item)</span>
				</div>
				<div class="order_sec">
					<div class="col-md-8 col-sm-8">
						<span class="item">Total items</span>
						<span class="reference">including tax </span>
					</div>
					<div class="col-md-4 col-sm-4">
						<span class="item_res">${{$total}}</span>
					</div>
			    </div>
				<div class="cart-total">
						<div class="col-md-8 col-sm-8 total-title"> <span> TOTAL: </span> </div>
						<div class="col-md-4 col-sm-4 total-price">${{$total}}*</div>
			    </div>
				<p class="store">You still need $26.00 to get free standard delivery! </p>
				<div class="free_store">
					<h5><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Free store delivery </h5>
				</div>
				<div class="promote-code">
					<a href="#">Do you have a promotional code? </a>
				</div>
				<div class="process_btn">
					<a href="{{url('checkout/placeorder')}}">order with payment committment</a>
				</div>
          </div>

		</div>
    </div>
</section>
@include('webshop.nomenu_footer')
