<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Webshop - Global Property Cowboys</title>

			<!-- Bootstrap -->
			<link href="{{url('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet">
			<!-- Style -->
				<link href="{{url('assets/frontend/css/style.css')}}" rel="stylesheet">
			<link href="{{url('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet">
			<link rel="stylesheet" href="{{url('assets/frontend/css/demo.css')}}" type="text/css" media="screen" />
			<link rel="stylesheet" href="{{url('assets/frontend/css/flexslider.css')}}" type="text/css" media="screen" />
			<link rel="stylesheet" type="text/css" href="{{url('assets/frontend/css/owl.carousel.min.css')}}">
			<!-- Responsive -->
			<link href="{{url('assets/frontend/css/responsive.css')}}" rel="stylesheet">
			<link rel="stylesheet" href="{{url('assets/frontend/css/font-awesome.min.css')}}">
			<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap" rel="stylesheet">
			
<script type="text/javascript">
window.Trengo = window.Trengo || {};
window.Trengo.key = 'O4mF0u3h1Zd9qrt';
(function(d, script, t) {
    script = d.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = 'https://static.widget.trengo.eu/embed.js';
    d.getElementsByTagName('head')[0].appendChild(script);
}(document));
</script>
			
			</head>
<body>

<!-- header
  ==============================-->
 <header class="header header_sec">
        <div class="container-fluid">
			<div class="col-md-3 col-sm-4 col-xs-4">
			   <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
			    <a id="to-top" class="btn btn-lg btn-dark" href="#top">
			      <span class="sr-only">Toggle to Top Navigation</span>
			      <i class="fa fa-chevron-up"></i>
			    </a>
			    <nav id="sidebar-wrapper">
				<ul class="sidebar-nav">
					<a id="menu-close" href="#" class="btn btn-danger pull-right hidden-md hidden-lg toggle"><i class="fa fa-times"></i></a>
					<li><a href="#">NEW IN</a></li>
								<li class="bn"><a href="#">SPECIAL PRICES</a></li>
								<li><a href="#">BEDROOM</a></li>
								<li><a href="#">BATHROOM</a></li>
								<li><a href="#">TABLEWARE</a></li>
								<li><a href="#">INDOOR FURNITURE</a></li>
								<li><a href="#">DECORATION</a></li>
								<li><a href="#">FRAGRANCES</a></li>
								<li><a href="#">KIDS</a></li>
								<li class="bns"><a href="#">BEACH <span>NEW</span></a></li>
								<li><a href="#">ROYAL COLLECTION</a></li>
								<li class="bns"><a href="#">FATHER'S DAY <span>NEW</span></a></li>
								<li><a href="#">EDITORIALS</a></li>
								<li class="bns"><a href="#">INSPIRATION</a></li>
								<li class="nm"><a href="#">JOIN LIFE</a></li>
								<li class="nm"><a href="#">GIFT CARD</a></li>
								<li class="nm"><a href="#">NEWSLETTER</a></li>
								<p class="cv">FREE HOME DELIVERY ON <br>PURCHASES OVER £29.99</p>
				</ul>	
			    </nav>
			<div class="logo">
				<a href="{{URL::to('/')}}"><img src="{{url('assets/frontend/images/logo.png')}}" class="img-responsive" alt="logo"></a>
			</div>
			</div>
			<div class="col-md-5 col-sm-4 col-xs-4 non_sec">
				<form class="form-inline cc">
				  <div class="form-group">
					<label for="exampleInputName2">Search</label>
					<input type="text" class="form-control"  placeholder="">
				  </div>                    
                </form>
				<div class="logo_res">
					<a href="{{URL::to('/')}}"><img src="{{url('assets/frontend/images/logo.png')}}" class="img-responsive" alt="logo"></a>
					<span><a href="{{URL::to('/webshop')}}">Go back</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="wish_menu">
					<ul>

						<li>

						<a href="{{url('customer/address')}}"><span><i class="fa fa-user-o" aria-hidden="true"></i></span><span class="res_sec">{{Auth::user()->username}}</span></a>
						 <ul>
							<li><a href="{{url('checkout/logout')}}"><span class="sign"><i class="fa fa-sign-out" aria-hidden="true"></i></span>SIGN OUT</a>
							</li>							
                         </ul>
						</li>

						<li><a href="#"><span><i class="fa fa-heart-o" aria-hidden="true"></i></span> <span class="res_sec">wishlist</span></a></li>


						

						 <?php 
	                        if(Auth::check())
	                        {
	                           $products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
	                         }else{

	                           $products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->get();
	                         }

	                    ?>
						<li>
							@if(isset($products[0]))
							<a href="{{url('webshop/basket')}}">
							<span class="icn"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="total_items"><?php if(isset($products)){echo count($products);}else{ echo 0; } ?></span></span>
	                        </a>
	                        @else
	                        <a href="{{url('webshop/basket')}}">
							<span class="icn"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span class="total_items"><?php if(isset($products)){echo count($products);}else{ echo 0; } ?></span></span>
	                        </a>
	                        @endif
	                        @if(isset($products[0]))
							<span class="res_sec">
								<a href="{{url('webshop/basket')}}"  id="basketshow" > BASKET (<span id="total_items"><?php if(isset($products)){echo count($products);}else{ echo 0; } ?></span>)  </a><!-- <div id="mycart"></div> -->
							</span>
							@else
							<span class="res_sec">
								<a href="{{url('webshop/basket')}}" > BASKET (<span id="total_items"><?php if(isset($products)){echo count($products);}else{ echo 0; } ?></span>)  </a><!-- <div id="mycart"></div> -->
							</span>
							@endif
					    </li>
					</ul>
				</div>
			</div>
        </div>
 </header>
 <!-- basket  -->

      <?php 
        if(Auth::check())
        {
           $productsss = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
         }else{

           $productsss = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->get();
         }

      ?>
	  
	   <aside class="slide_basket hideMe re"  id="hideMe">
			<div class="products-container">
				<i class="fa fa-close slide_basket_close" ></i>
				<div id="productscroll" >
				     @foreach($productsss as $session)
				    <div class="row" >
				      	<div class="col-md-4 col-sm-4 basket_right"><img src="{{url($session->img)}}" />
				      	 </div>	

				      	    <div class="col-md-8 col-sm-8 basket_left">
				      	    	<div class="basket_res"><a href="#">{{$session->name}}</a></div>
                            
                            <div class="basket_detailsec"><p>size</p><p>{{$session->quantity}} x {{$session->price}}</p></div>

				      </div>	
				    </div>
				    @endforeach


                </div>
				<div class="standard_delivery">
					<p><span><i class="fa fa-truck" aria-hidden="true"></i>
                   </span>You will receive free standard delivery!</p>
				</div>
				<div class="free_store">
					<h5><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Free store delivery </h5>
				</div>
				<a href="{{url('/webshop/basket')}}" class="btn-process-order view-basket">View Basket</a>
			</div>
		</aside>
     <!-- basket end  -->
	
 
<!-- feedback
  ==============================-->
 <div class="feedback">
    <div class="feedback_content">
		<div class="feedback_cstm">
		    <a href="#" data-toggle="modal" data-target="#nm">Complaint</a>
		</div>
	</div>
 </div>
 <div class="modal fade" id="nm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <p>Do you have a complaint about us as an online store? Then we have this complaint 
		dealt with by an independent organization you can contact Thuiswinkel.org! </p>
		<p>Click on the link below and follow the 3 steps</p>
		<a href="#">https://www.thuiswinkel.org/consumenten/klacht-indienen</a>
		<p>we still hope to see you back in our online store or website</p>
        <p>Team Global Property Cowboys</p>
    </div>
  </div>
</div>
  
 <!-- Hotjar Tracking Code for http://devlaravel.inviolateinfotech.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1407283,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<script src="{{url('assets/frontend/js/jquery.js')}}"></script>
 <script type="text/javascript">
	
$( "#basketshow" )
  .mouseover(function() {
   //alert('sadasd');
    $('.slide_basket').addClass("ds");
    $('.slide_basket').removeClass("re");
  })

  $( "#basketshow" )
  .mouseup(function() {
   //alert('sadasd');
    $('.slide_basket').addClass("re");
    $('.slide_basket').removeClass("ds");
  })

 $(document).mouseup(function (e) { 
            if ($(e.target).closest(".hideMe").length 
                        === 0) { 
                $(".hideMe").hide(); 
                $('.slide_basket').removeClass("ds");
               // $('.slide_basket').addClass("re");
            } 
        });  
</script>