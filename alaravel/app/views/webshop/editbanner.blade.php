@extends('adashb')
@section('dash_content')
<h2>Edit Banner Slider</h2>
<form action="" method="post" enctype="multipart/form-data">

<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b> Name</b></label>
	<label class="col-md-6">
	<input type="text" class="form-control" name="name" placeholder="Name" required="" value="{{$getbanners['bannername']}}"></label>
</div>

<div class="form-group row">
	<label class="col-md-3"><b>Category</b></label>
	<label class="col-md-6">
	<select name="category">
	<option value="">Select category</option>
	<?php

	foreach($category as $cat) { 
		$select='';
		if($getbanners['cid'] ==$cat['id']){
			$select='selected';
		}
	?>
		<option value="<?php echo $cat['id']; ?>" <?php echo $select;?>><?php echo $cat['name'];?></option>
	<?php
		}
	?>
	</select>
	</label>
</div>

<div class="form-group row">	
	<label class="col-xs-12 col-md-3" ><b>Image</b></label>
	<label class="col-xs-12 col-md-6"  for="ifile-id2" >
		<div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd">
		<span id='ifile-labe'  class='ifile-label'>Upload Image</span>
		<input type="file" name="image[]"  class="inputfile inputfile-1 ifile-id"  id="ifile-id2" multiple> 
			<?php 
			if(isset($getbanners['image'])){
				if($getbanners['image']!=''){
					$arr=explode(',',$getbanners['image']);
					foreach($arr as $key =>$v){
						?>
						<img src="{{url('assets/img')}}/{{$v}}" width="100" height="100">
						<?php
					} 
				} 
			}
			?>
			
		</div>
	</label>
</div>
<input type="hidden" name="id" value="{{$getbanners['id']}}">
<div class="form-group">
<input type="Submit" class="btn btn-large btn-primary" name="submit" value="Update">
</div>
</form>
@stop
