@include('webshop.web_header')

<style type="text/css">
	.row p{text-align:left;margin: 0 0 0px;}
	#address-stores{display:none;}
	#orders{display:none;}
	#cards{display:none;}
	#notifications{display:none;}
	#wishlists{display:none;}
	.sidebar_menu {
		line-height: 2;
	}
	.sidebar_menu li a {
		font-size: 13px;
		color: #000;
	}
	.sidebar_menu li a:hover {
		text-decoration: none;
	}
	#customer-information h4 ,#notifications h4 {
		font-size: 14px;
		font-weight: bold;
	}
	#customer-information .row > div {
		margin-bottom: 40px;
	}
	#customer-information .row > div > p:nth-child(2n+1), #customer-information > .row:nth-child(2n) > div:nth-child(2n) > p > span {
		color: #888b8d;
	}
	#customer-information > .row:nth-child(2n) > div:nth-child(2n) > p {
		color: #333;
	}
	#customer-information .row > div:nth-child(2n) p {`
	}
	#notifications p {
		color: #1e1e1e;
		font-size: 12px;
	}
	#notifications p:nth-child(2n+1) {
		margin: 20px 0 40px 0;
	}
	#notifications p:nth-child(4n) {
		position: relative;
		padding-left:20px;
	}
	#notifications p:nth-child(4n) > input {
		position: absolute;
		left: 0;
		top: -3px;
	}
	.info_b {
		background: #f2f2f2;
		border-top: 1px solid #e6e2e7;
	}
	#notifications > .row > div > .row {
		border-bottom: 1px solid #ebebeb;
	}
	#notifications > .row > div div > div:nth-child(2n) > h6:nth-child(2n) > a ,#notifications > .row > div > .row:nth-child(6n) > div:nth-child(2n) > h6 > a:hover {
		color: #000;
	}
	#notifications > .row > div div > div:nth-child(2n) > h6:nth-child(2n) > a:hover {
		color: #888b8d;
	}
	.h_tnetnoc_tcudorp a {
		color: #000;
		text-decoration: none;
	}
	.h_tnetnoc_tcudorp > .row > div:nth-child(2n) {
		padding-left: 0px;
	}
	.pe_tr_parent {
		border: 1px solid #888b8d;
		padding: 8px 10px;
		border-radius: 10px;
	}
	.bskt_list li {
		width: 25%;
		text-align: center;
	}
	.bskt_list li > input {
		padding: 0 2px;
		text-align:center;
	}
	.modal-content {
	border-radius: 0px;
	}



	.z_inpuT\{ input , .z_inpuT\{ select{
		border-bottom: 1px solid #000 !important;
		border: none;
		border-radius: 0px;
		color: #000 !important;
		box-shadow:none ;
	}
	.form-control:focus {
		border-color:none !important;
		box-shadow:none !important;
	}
	.z_inpuT\{ .form-group,.z_inpuT\{ select {
		margin: 20px auto;
	}
	.form-control::placeholder {
		color: #000 !important;
		opacity: 1;
	}
	.modal-header , .nav-tabs {
		border: none !important;
	}
	.nav-tabs li {
		margin-right: 50px;
		margin-left: 16px;
	}
	.nav-tabs li a {
		color: #000;
	}
	 #City , #Number1 , #Telephone{
		margin-top: 38px;
	}
	.z_inpuT\{ small {
		font-size: 10px;
	}
	.nav {
		padding-left: 0;
		margin-bottom: 38px;
		list-style: none;
		width: 100%;
	}
	.nav-tabs > li {
		/* float: left; */
		margin-bottom: -1px;
		width: 100px;
		display: inline-block;
	}
	.modal-dialog {
		height: 100%;
	}
	.pa_mod{
		cursor:pointer;
	}
	.modal-content {
		width: 100%;
	}
	#yourModal .modal-body {
		padding: 0;
	}
	#yourModal h5 {
		font-size: 12px;
		font-weight: 609;
		line-height: 1.3;
	}
	.form-group {
		position: relative;
	}
	#yourModal label {
		font-size: 12px;
	}
	#yourModal label {
		font-size: 12px;
		top: -20px;
	}
	#yourModal .form-group {
		margin: 40px auto;
	}
	#yourModal .modal-dialog {
		width: 360px;
	}
</style>

<section class="side_bar">
	<div class="container-fluid">
		<div class="row">
			<div style="display:none;">
				@include('webshop.web_sidebar')
			</div>
			<div style="display:block;">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<ul class="sidebar_menu">
						<li><a href="javascript:void(0);" class="account-list" data-id="#customer-information" >CUSTOMER INFORMATION</a></li>
						<li><a href="javascript:void(0);" class="account-list" data-id="#address-stores" >MY ADDRESSES AND STORES</a></li>
						<li><a href="javascript:void(0);" class="account-list" data-id="#orders" >ORDERS</a></li>
						<li><a href="javascript:void(0);" class="account-list" data-id="#cards" >CARDS</a></li>
						<li><a href="javascript:void(0);" class="account-list" data-id="#notifications" >Notifications</a></li>
						<li><a href="javascript:void(0);" class="account-list" data-id="#wishlists" >WISHLISTS</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-7 col-lg-7">
				<div id="customer-information" class="account-list-ids">
					<div class="row">
						<div class="col-md-6">
							<h4 class="text-uppercase">Billing details</h4>
							<p>.</p>
							<p  class="pa_mod" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>Details</p>
							<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title" id="myModalLabel">ADD NEW ADDRESS</h4>
										</div>
										<div class="modal-body">
											<ul class="list-unstyled list-inline">
												<li><label class="radio-inline"><input type="radio" name="optradio" checked>Individual</label></li>
												<li><label class="radio-inline"><input type="radio" name="optradio" checked>Browse</label></li>
											</ul>

											<div class="container-fluid">
												<div class="row">
													<div class="col-sm-6 z_inpuT{">
														<div class="form-group">
															<label for="InputEmail1" class="sr-only">Email address</label>
															<input type="email" class="form-control transformer" id="InputEmail1" placeholder="Enter email">
															<div class="text-right"><small class="form-text text-muted">0/128</small></div>
														</div>
														<div class="form-group">
															<label for="Address" class="sr-only">Address</label>
															<input type="Address" class="form-control" id="Address" placeholder="Address">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<div class="form-group">
															<label for="Floor/door/etc." class="sr-only">Floor/door/etc.</label>
															<input type="Floor/door/etc." class="form-control" id="Floor/door/etc." placeholder="Floor/door/etc.">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<select class="form-control">
															<option>DRENTHE</option>
															<option>FLAVOLAND</option>
															<option>FRIESLAND</option>
															<option>GELDERLAND</option>
															<option>GRONINGEN</option>
															<option>LIMBURG</option>
															<option>NOORD-BRABANT</option>
															<option>NOORD-HOLLAND</option>
															<option>OVERIJSSEL</option>
															<option>UTRECHAT</option>
															<option>ZEELAND</option>
															<option>ZUID-HOLLAND</option>
														</select>
														<div class="form-group">
															<label for="Telephone" class="sr-only">Telephone</label>
															<input type="Telephone" class="form-control" id="Telephone" placeholder="Telephone">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
													</div>
													<div class="col-sm-6 z_inpuT{">
														<div class="form-group">
															<label for="Surname(s)" class="sr-only">Surname(s)</label>
															<input type="Surname(s)" class="form-control" id="Surname(s)" placeholder="Surname(s)">
															<div class="text-right"><small class="form-text text-muted">0/128</small></div>
														</div>
														<div class="form-group">
															<label for="Number" class="sr-only">Number</label>
															<input type="text" class="form-control" id="Number" placeholder="Number">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<div class="form-group">
															<label for="Postcode" class="sr-only">Postcode</label>
															<input type="Postcode" class="form-control" id="Postcode" placeholder="Postcode">
														</div>
														<div class="form-group">
															<label for="City" class="sr-only">City</label>
															<input type="City" class="form-control" id="City" placeholder="City">
														</div>
													</div>
												</div>
											</div>
											<div class="container-fluid">
												<div class="row">
													<div class="col-sm-6 z_inpuT{">
														<div class="form-group">
															<label for="InputEmail1" class="sr-only">Email address</label>
															<input type="email" class="form-control" id="InputEmail1" placeholder="Enter email">
															<div class="text-right"><small class="form-text text-muted">0/128</small></div>
														</div>
														<div class="form-group">
															<label for="Company" class="sr-only">Company</label>
															<input type="Company" class="form-control" id="Company" placeholder="Company">
															<div class="text-right"><small class="form-text text-muted">0/128</small></div>
														</div>
														<div class="form-group">
															<label for="Address" class="sr-only">Address</label>
															<input type="Address" class="form-control" id="Address" placeholder="Address">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<div class="form-group">
															<label for="Floor/door/etc." class="sr-only">Floor/door/etc.</label>
															<input type="Floor/door/etc." class="form-control" id="Floor/door/etc." placeholder="Floor/door/etc.">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<select class="form-control">
															<option>DRENTHE</option>
															<option>FLAVOLAND</option>
															<option>FRIESLAND</option>
															<option>GELDERLAND</option>
															<option>GRONINGEN</option>
															<option>LIMBURG</option>
															<option>NOORD-BRABANT</option>
															<option>NOORD-HOLLAND</option>
															<option>OVERIJSSEL</option>
															<option>UTRECHAT</option>
															<option>ZEELAND</option>
															<option>ZUID-HOLLAND</option>
														</select>
														<div class="form-group">
															<label for="Telephone" class="sr-only">Telephone</label>
															<input type="Telephone" class="form-control" id="Telephone" placeholder="Telephone">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
													</div>
													<div class="col-sm-6 z_inpuT{">
														<div class="form-group">
															<label for="Surname(s)" class="sr-only">Surname(s)</label>
															<input type="Surname(s)" class="form-control" id="Surname(s)" placeholder="Surname(s)">
															<div class="text-right"><small class="form-text text-muted">0/128</small></div>
														</div>
														<div class="form-group">
															<label for="Tax ID Code" class="sr-only">Tax ID Code</label>
															<input type="Tax ID Code" class="form-control" id="Tax ID Code" placeholder="Tax ID Code">
														</div>
														<div class="form-group">
															<label for="Number" class="sr-only">Number</label>
															<input type="text" class="form-control" id="Number1" placeholder="Number">
															<div class="text-right"><small class="form-text text-muted">0/30</small></div>
														</div>
														<div class="form-group">
															<label for="Postcode" class="sr-only">Postcode</label>
															<input type="Postcode" class="form-control" id="Postcode" placeholder="Postcode">
														</div>
														<div class="form-group">
															<label for="City" class="sr-only">City</label>
															<input type="City" class="form-control" id="City" placeholder="City">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="button buttonBlue create-buttonsave">Create Account</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="text-uppercase">Main email</h4>
							<p>This email will be used for accessing your private pages as well as for receiving correspondence and newsletters.</p>
							<p  class="pa_mod" data-toggle="modal" data-target="#yourModal"><i class="fa fa-edit"></i>Details</p>





							<!-- Modal -->
							<div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
											</button>
											<h4 class="modal-title" id="myModalLabel">CHANGE EMAIL</h4>
										</div>
										<div class="modal-body">
											<div class="container-fluid">
												<div class="row">
													<div class="col-sm-12 z_inpuT{">
														<h5>Enter a new email address. Remember that you are also changing the username with which you enter "My Account”.</h5>
														<h5>You can change your access email address. This is also the email address where you will receive notices and bulletins. We request your password for security reasons.</h5>
														<form class="horizontal">
															<div class="form-group">
																<label for="InputEmail1" class="sr-only">Email address</label>
																<input type="email" class="form-control transformer" id="InputEmail1" placeholder=" New e-mail address ">
															</div>
															<div class="form-group">
																<label for="InputEmail1" class="text-muted" >Re-enter the New e-mail Address</label>
																<input type="email" class="form-control transformer" id="InputEmail1" placeholder="">
															</div>
															<div class="form-group">
																<label for="InputEmail1" class="text-muted">For security reasons, please enter your password</label>
																<input type="email" class="form-control transformer" id="InputEmail1" placeholder="">
															</div>
															<div class="form-group">
																<label for="InputEmail1" class="sr-only">Enter the code shown in the image </label>
																<input type="email" class="form-control transformer" id="InputEmail1" placeholder=" Enter the code shown in the image ">
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="button buttonBlue create-buttonsave">Save</button>
										</div>
									</div>
								</div>
							</div>





						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<h4 class="text-uppercase">Password</h4>
							<p>**************</p>
							<p><i class="fa fa-edit"></i>Details</p>
						</div>
						<div class="col-md-6">
							<p> At ZARA HOME we take your privacy very seriously and are committed to the protection of your
							 data. Learn more about how we care for and use your data in our <span>Privacy policy</span></p>
						</div>
					</div>
				</div>

				<div class="">
				<div id="address-stores" class="account-list-ids">
					<p>You will need to complete your personal details in order to add a delivery address. </p>
					<input class="button text-uppercase" style="text-decoration: none;font-size:10px;" value="personal details" type="button">
				</div>
				<div id="orders" class="account-list-ids">
					<p>You have yet to place an order.</p>
				</div>
				<div id="cards" class="account-list-ids">
					<p>
						You do not currently have any saved cards.

						To enjoy the benefits of Easy Purchase, activate the option "Save card" in your next online purchase ★

						Remember that your details are completely secure and will only be used to make your purchases easier.
					</p>
				</div>
				<div id="notifications" class="account-list-ids">
					<h4  class="text-uppercase">NEWSLETTER</h4>
					<p>
						If you would like to receive information on what's new and the latest trends, subscribe to our weekly newsletter.
					</p>
					<p class="clearfix">
						<span style="float:left;width:20%;"><b>EMAIL</b></span>
						<span style="float:right;width:80%;"><a href="#" class="text-muted">Change information</a></span>
					</p>
					<p>
					<input type="checkbox" name="pp" />I have read the <a href="#" class="text-muted">Privacy policy</a> and want to receive news and customised commercial communications from of ZARA HOME via email and other means
					</p>
					<p class="clearfix"><button class="button text-uppercase" style="text-decoration: none;font-size:10px;padding:10px 30px;margin: 40px 0 10px 0;" type="button">Subscribe</button></p>
					<div class="row">
						<div class="col-sm-11 col-sm-offset-1">
							<div class="row">
								<div class="col-sm-12 info_b">
									<div class="text-center">
										<h6 class="text-uppercase">Basic information on Data Protection </h6>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<h6>Data controller</h6>
								</div>
								<div class="col-sm-10 col-md-10 col-lg-10">
									<h6>ZARA HOME NEDERLAND B.V. ; ZARA HOME ESPAÑA, S.A </h6>
									<h6><a href="#">More info</a></h6>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<h6>Purposes and legal basis</h6>
								</div>
								<div class="col-sm-10 col-md-10 col-lg-10">
									<h6>If you agree, we will use your data to send you customized commercial communications from ZARA HOME. The reason why we are entitled to use your data is the consent you give us by agreeing to subscribe. </h6>
									<h6><a href="#">More info</a></h6>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<h6>Data recipients</h6>
								</div>
								<div class="col-sm-10 col-md-10 col-lg-10">
									<h6>We share your data with service providers and collaborators within and outside the European Union that support us in sending information, and we do so with appropriate guarantees and while preserving the security of your data. </h6>
									<h6><a href="#">More info</a></h6>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<h6>Rights</h6>
								</div>
								<div class="col-sm-10 col-md-10 col-lg-10">
									<h6>You can unsubscribe at any time and at no cost, in addition to other rights, as detailed in the additional information. </h6>
									<h6><a href="#">More info</a></h6>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<h6>Additional information </h6>
								</div>
								<div class="col-sm-10 col-md-10 col-lg-10">
									<h6>For more information, please refer to our <a href="#" class="text-muted">Privacy policy.</a></h6>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="wishlists" class="account-list-ids">
					<div class="row">
						<div class="col-md-10">
							<h4>You do not have any items in your Wishlist</h4>
							<p>Add them by clicking the heart icon on each item.You will have your favourites to hand and it will make future purchases and gifts easier.</p>
						</div>
						<div class="col-md-2">
							<a href=""> New Wish List  </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-9 col-sm-offset-3">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-2">
							<img src="{{url('public/webshop/products/imgbox4.jpg')}}" class="img-responsive">
						</div>
						<div class="col-sm-8">
							<div class="tnetnoc_tcudorp">
								<div class="h_tnetnoc_tcudorp">
									<div class="row">
										<div class="col-sm-10">
											<strong class="small"><a href="#">COTTON DUVET COVER WITH CONTRASTING LINEN BORDER </a></strong>
										</div>
										<div class="col-sm-2 text-right">
											<img src="{{url('/assets/img/cart.png')}}">
											<i class="fa fa-times" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="d_tnetnoc_tcudorp">
									<div class="row">
										<div class="col-sm-6">
											<small>ref. 9976/088 </small><br>
											<small>220 x 220 cm</small>
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="col-sm-4">
													<small>59,99 € </small>
												</div>
												<div class="col-sm-4">
													<ul class="bskt_list">
														<li><button class="quantity_remove" data-id="20">-</button></li>
														<li>
														<input class="product_quantity" value="1" type="text">
														<input class="product_price" value="23" type="hidden">
														</li>
														<li><button class="quantity_add" data-id="20">+</button></li>
													</ul>
												</div>
												<div class="col-sm-4 text-right">
													<strong class="small">59,99 €</strong>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="pe_tr_parent">
								<em>king</em>
								<span class="pe_tr pull-right">
									<i class="fa fa-pencil" aria-hidden="true"></i>
									<i class="fa fa-trash" aria-hidden="true"></i>
									</div>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>

@include('webshop.web_footer')
