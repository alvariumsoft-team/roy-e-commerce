@include('webshop.header')
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		@include('webshop.left_panel')
	  </div>
	  <div class="col-md-10 col-sm-10">
		
		<div class="col-md-12 col-sm-12 return-con questions term">
			<h2 class="faq_sec">RETURN CONDITIONS</h2>		
			<div class="content">
		
      <p class="title">Is the return free of charge?</p>
      <p>Returns at Global Property Cowboys we charge a return fee of 10 euros for shipments in the Netherlands, 15 euro for shipments within Europe and 25 euro for shipments in the rest of the world.</p>
	  <p>For more information about the right of revocation click <a  href="{{url('webshop/revocation')}}">HERE </a>.</p>
	  <p>For the withdrawal form you can go <a target="_blank" href="{{url('assets/pdf/')}}/withdrawal-from.pdf">HERE </a>.</p>
	  
    </div>
	<div class="content">
		
      <p class="title">What is the return period?</p>
      <p>Items can be returned within 30 days (from shipment), in accordance with the "Purchase and USE TERMS AND CONDITIONS". In addition, there is a statutory period of 14 days from the day you receive all the items in your order. From the moment you notify us within these 14 days that you wish to return the items, you have a further 14 days to return the items.</p>
	  
	  
    </div>
	<div class="content">
		
      <p class="title">How can I return an item?</p>
      <p>Print a return form <a target="_blank" href="{{url('assets/pdf/')}}/return-from.pdf">HERE </a></p>
	  <p>The easiest way to return an item is to take it to one of our GPC stores. It must be a store in the country of purchase. Our shops can be found here. You can also return your order by sending it to our distribution centre. To do this, pack the products and attach the printed electronic receipt. Please send the return shipment at your expense to the following address:</p>
	  <p>Items must be in their original condition. You will only be liable for a reduction in the value of the products if this can be attributed to handling of the products that goes beyond what is necessary to verify the nature, characteristics and functioning of the products. Please note that the right to return does not apply to items delivered in a sealed package or sealed, which are not suitable for return for health or hygiene reasons and where the packaging is opened after delivery or the seal is broken.</p>
	  <p>GPC reserves the right to refuse to accept returns for shipment or communication after the return period has expired or for items that are not in the same condition as when they were delivered.</p>
	  <p>If you have any questions about cancellation or the return process, please contact our Customer Service.</p>
	  
	  
    </div>
	
	<div class="content">
		
      <p class="title">How do I receive my purchase amount back?</p>
      <p>After approval of the returned items, you will receive the corresponding amount in the same way as the one with which you paid your purchase. The items must be in perfect condition and have the attached price tags.</p>
	  <p>If you use your statutory cooling-off period of 14 days you will also receive a refund of the delivery costs you have paid (with the exception of any additional costs resulting from your choice of a method of delivery other than the cheapest standard delivery offered by us).</p>
	 
	  
	  
    </div>
	<div class="content">
		
      <p class="title">When will you receive the amount of your return?</p>
      <p>After approval of the returned items you will receive a confirmation email with the amount that will be paid into your account within a few days. We will refund you with the same means of payment as with which you made the original purchase, unless you have explicitly agreed otherwise. For the refund there will be no charge for you. We may withhold a refund until we have received the items from you, or until you have demonstrated that you have returned the items, whichever comes first. Please note that the period of refund may vary depending on your bank.</p>
	 
	  
	  
    </div>
	
	<div class="content">
		
      <p class="title">What should you do if the return amount is incorrect?</p>
      <p>Please contact our "CUSTOMER SERVICE". We will solve the problem as soon as possible.</p>
	 
	  
	  
    </div>
	<div class="content">
		
      <p class="title">Legal Guarantee</p>
      <p>If an article does not comply with the agreement, then we will comply with the statutory warranty guarantees that apply to it.</p>
	 
	  
	  
    </div>
			</div>	
	        </div>
      </div>		
	</div>

</section> 
@include('webshop.footer')