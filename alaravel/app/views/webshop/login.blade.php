@include('webshop.header')
<section class="contact-form contact_sec">
	<div class="log_in">
		<div class="container">		  
		  <div class="col-md-8 col-sm-8">
			  <div class="col-md-7 col-sm-7">
					<h4>Registered customers</h4>
					<p>We will ask you for the necessary information to speed up the purchase process.</p>
					@if (Session::get('message-success'))
					   <div class="alert alert-success">
			            {{Session::get('message-success')}}
                       </div>
					@endif	
					@if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif	
					<form name="login_form" method="POST" action={{url('checkout/login')}} id="loginformPage">
					   <div class="group {{ $errors->has('username') ? ' error' : '' }}">											                  
								<input class="form_input @if ($errors->has('username')) used @endif" type="text" name="username"><span class="highlight"></span><span class="bar"></span>
								<label class="labletxt">Username or Email*</label>													
					   </div>
					    <div class="group {{ $errors->has('password') ? ' error' : '' }}">											                  
								<input class="form_input @if ($errors->has('password')) used @endif" type="password"  name="password"><span class="highlight"></span><span class="bar"></span>
								@if ($errors->has('password'))
								<i class="error">{{ $errors->first('password') }}</i>
							    @endif
								<label class="labletxt vb">Password*<!-- <a href="#"><span class="eye"><i class="fa fa-eye" aria-hidden="true"></i></span></a> --></label>													
					   </div>
					   <div class="forgot">
					       <a href="{{url('/forgotpassword/forgotpassword')}}">Have you forgotten your password?</a>
					   </div>
					   <div class="sub_sec">
						   <div class="checkbox">						
							<label class="con">
							   Do not end session
							  <input type="checkbox">
							  <span class="checkmark"></span> 						  
							</label>						
							</div>
						</div>
						<div class="submit_sec">
							<input class="buttons" type="submit" name="submit" value="SUBMIT">
						</div>
					</form>
					<div class="social_btns">
						<a href="{{$fbauthUrl}}"><span><i class="fa fa-facebook-square" aria-hidden="true"></i></span>log in with facebook</a>
						<a class="fb" href="{{$authUrl}}"><span><i class="fa fa-google" aria-hidden="true"></i></span>log in with google</a>
					</div>
			  </div>
			   <?php 
                if(Auth::check())
                {
                   $products = Productsessions::where('user_id', '=', Auth::user()->id)->where('checkout_id', '=',0)->get();
                 }else{

                   $products = Productsessions::where('guest_session', '=', Session::getId())->where('checkout_id', '=',0)->get();
                 }

                ?>
			  <div class="col-md-5 col-sm-5">
					<h4>new customers</h4>
					<div class="submit_sec">
                        @if(isset($products[0]))
						<a href="{{url('/webshop/guestcheckout')}}" class="buttons">PURCHASE  AS A GUEST
						 </a>
						 <br>
						 <br>
                        @endif
						
						<a href="{{url('/checkout/register')}}" class="buttons"> CREATE A NEW ACCOUNT </a>
				    </div>
			  </div>
		  </div>		
		</div>
    </div>
</section>
<style type="text/css">
 .submit_sec .buttons {
    display: block;
    width: 100%;
    max-width: 300px;
    padding: 12px 25px;
    font-size: 11px;
    color: #000;
    border: 1px solid #000;
}

.submit_sec .buttons:hover {
    background: #000;
    color: #fff !important;
}
</style>
@include('webshop.nomenu_footer') 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>

<script type="text/javascript">
	$("#loginformPage").validate({
		rules:{
username:"required",
password:"required",

		},
messages:{
username:"Username or Email is required",
password:"Password is required",

		  
},

submitHandler: function(form) {
            form.submit();
        }
});
	
</script>