﻿@include('webshop.header')
<section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		@include('webshop.left_panel')
	  </div>
	  <div class="col-md-10 col-sm-10">
		<section class="contact">
				<div class="contact_inner">
				   <h1>Contact Us</h1>
				</div>
        </section>
		<div class="col-md-12 col-sm-12">
			
			       @if (Session::get('message-success'))
					   <div class="alert alert-success">
			            {{Session::get('message-success')}}
                       </div>
					@endif	
					@if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif	

					@if (Session::get('message-failure'))
						<div class="alert alert-danger">
			             {{Session::get('message-failure')}}
                      </div>
					@endif
		<h2>CONTACT FORM</h2>
			<p>If you need help, call our free Customer Service line at 0800 026 0091.</p>				
		</div>
		<form class="form_sec" method="POST" action="{{url('webshop/contact-shoppings')}}" id="contactform">
			<div class="col-md-6 col-sm-6">
			        <div class="form-group">
					   <label>Agency Information</label>				  
				   </div>
				   <div class="group">					  
					   <select class="form-control" name="category" required="required">
							<option value="">Category</option>
							<option value="Online orders">Online orders</option>
							<option value="Picture Catalogue">Picture Catalogue</option>
							<option value="Technical questions">Technical questions</option>
							<option value="Home Stores">Home Stores</option>
							<option value="Work with us">Work with us</option>
							<option value="Wedding gift anniversary">Wedding gift anniversary</option>
                       </select>
				   </div>
                  <div class="group">											                  
						<input class="form_input" type="text" name="customer_services"><span class="highlight"></span><span class="bar"></span>
						<label class="labletxt">Customer service</label>													
				   </div>					   
			</div>
            <div class="col-md-6 col-sm-6">
			        <div class="form-group">
					   <label>Personal Information</label>				  
				   </div>
			       <div class="group">											                  
							<input class="form_input" type="text" name="name" required="required"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Name*</label>													
				   </div>	
                   <div class="group">											                  
							<input class="form_input" type="text" name="surname" required="required"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Surname(s)*</label>													
				   </div>					  				   
			</div>
			 <div class="col-md-6 col-sm-6 sec">
			        <div class="form-group">
					   <label>Contact Information</label>					   	
				   </div>	
			       <div class="group">											                  
							<input class="form_input" type="text" name="email" required="required"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Email*</label>													
				   </div>	
                   <div class="group">											                  
							<input class="form_input" type="text" name="phone" required="required"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Phone*</label>													
				   </div>		   
			</div>
			 <div class="col-md-6 col-sm-6 sec">
			        <div class="form-group">
					   <label>Address Information</label>					   
				   </div>
                   <div class="group">											                  
							<input class="form_input" type="text" name="street_name"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Street name</label>													
				   </div>
                  <div class="group">											                  
							<input class="form_input" type="text" name="city"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Your Town/City</label>													
				   </div>				   			      						   
			</div>			
           <div class="col-md-6 col-sm-6 sec">
		   <div class="form-group">
					   <label>Services</label>					   
					</div>
				<div class="form-group check_sec">	
					
					<div class="checkbox">						
						<div class="checkbox">
						<label class="con">
						 Construction companies
                          <input type="checkbox" name="service[]" value="Construction companies">
                          <span class="checkmark"></span> 						  
						</label>
                      </div>	
					</div>						
					<div class="checkbox">						
                        <label class="con">
						 Architects
                          <input type="checkbox" name="service[]" value="Architects">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Projectdevelopers
                          <input type="checkbox" name="service[]" value="Projectdevelopers">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Moving companies
                          <input type="checkbox" name="service[]" value="Moving companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>										
					<div class="checkbox">						
                        <label class="con">
						  Investors
                          <input type="checkbox" name="service[]" value="Investors">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Real estate agents
                          <input type="checkbox" name="service[]" value="Real estate agents">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Garden companies
                          <input type="checkbox" name="service[]" value="Garden companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>														
                </div>
				
			</div>
			<div class="col-md-6 col-sm-6 sec">
			<div class="form-group">
					   <label>&nbsp;</label>					   
					</div>
				<div class="form-group check_sec">																																							
					<div class="checkbox">						
                        <label class="con">
						  Home improvement companies
                          <input type="checkbox" name="service[]" value="Home improvement companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Banks
                          <input type="checkbox" name="service[]" value="Banks">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Storage companies
                          <input type="checkbox" name="service[]" value="Storage companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Packaging companies
                          <input type="checkbox" name="service[]" value="Packaging companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Travel agencies
                          <input type="checkbox" name="service[]" value="Travel agencies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
					<div class="checkbox">						
                        <label class="con">
						  Airlines
                          <input type="checkbox" name="service[]" value="Airlines">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
                    <div class="checkbox">						
                        <label class="con">
						  Cleaning companies
                          <input type="checkbox" name="service[]" value="Cleaning companies">
                          <span class="checkmark"></span> 						  
						</label>						
					</div>					
                </div>
				
			</div>
			<div class="col-md-6 col-sm-6 sec">
				   <div class="group">											                  
							<input class="form_input" type="text" name="message"><span class="highlight"></span><span class="bar"></span>
							<label class="labletxt">Message</label>													
				   </div>	               				   
			</div>
			<div class="col-md-12 col-sm-12 sub_sec vxc">
				<div class="checkbox term">						
						<label class="con">
						  I have been able to read and understand the information on the use of my personal data explained in the <a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy Policy</a>
						  <input type="checkbox" name="privacy_policy" id="privacy_policy" required="required">
						  <span class="checkmark"></span> 						  
						</label>
						<label for="" class="errorp" style="color: red;"></label>						
				</div>
				<p><b>Note - </b> If you submit the information entered in the contact form by clicking on the following button, you agree that we may use your information to answer your question or to contact us. The transfer of data to third parties will generally not take place unless justified by the applicable data protection rules or we are legally obliged to do so. You can revoke your consent at any time with effect for the future. In case of revocation, your data will be deleted immediately. You can inquire at any time about the data stored on your person. More information on data protection can be found in the privacy statement on this website. </p>
				<div class="submit_sec">
					<input class="buttons" type="submit" value="SUBMIT" id="btncontactform">
				</div>
            </div>				
        </form>
      </div>		
	</div>

</section> 
@include('webshop.footer')
<script
  src="https://code.jquery.com/jquery-1.11.0.js"
  integrity="sha256-zgND4db0iXaO7v4CLBIYHGoIIudWI5hRMQrPB20j0Qw="
  crossorigin="anonymous"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function() {

$('#btncontactform').click(function() {

    if($('#privacy_policy').is(":checked"))
	{
		return;
	  
	} else{

		$('.errorp').html('This field is required');
		return false;
	}
});   	
	 $('#privacy_policy').click(function() {
        if($(this).is(':checked'))
            //alert('checked');
        $('.errorp').html('');
        else
            //alert('unchecked');
        $('.errorp').html('This field is required');
    });


    $('#btncontactform').click(function() {
        $("#contactform").valid();
    });
});	

$( ".form_input" ).focus(function() {
		//alert( "Handler for .focus() called." );
		if($(this).hasClass('error')){
			console.log("yes");
			$(this).siblings('.bar').addClass('errorbar');
		}
		else{
			$(this).siblings('.bar').removeClass('errorbar');
		}
	});
	$( ".form_input" ).keyup(function() {
		//alert( "Handler for .focus() called." );
		if($(this).hasClass('error')){
			console.log("yes");
			$(this).siblings('.bar').addClass('errorbar');
		}
		else{
			$(this).siblings('.bar').removeClass('errorbar');
		}
	});






</script>