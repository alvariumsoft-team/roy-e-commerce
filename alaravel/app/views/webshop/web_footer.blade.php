<!-- footer
  ==============================-->
<footer class="footer">
	<div class="container">
		<div class="col-md-9 col-sm-9 footer_fg">
			<div class="newsletter">
				       <div class="alert alert-success" id="success-news" style="display: none;">
                       </div>
					
						<div class="alert alert-danger error-news" style="display: none;">
                        </div>
				<form method="POST"  class="add-form-news">
					<div class="group" style="margin-bottom:10px;">											                  
							<input class="form_input" type="text" name="email_sub" id="email_sub">
							<span class="highlight"></span>
							<span class="bar"></span>
							<h3 for="" class="errore" style="color: red; margin-top: 4px;"></h3>
							<label class="labletxt">Join our newsletter - Enter your email address</label>
							
				    </div>
				    
					<!-- <div class="form-group newsletterBtn"> -->												
					<div class="form-group">												
                        <div class="checkbox">
							<label class="con">
							  I agree to receive customised commercial communications from Global Property Cowboys via email and other means
							  <input type="checkbox" id="privacy_policy_sub" name="privacy_policy_sub" value="Yes">
							  <span class="checkmark"></span> 						  
							</label>
							<label for="" class="errorc" style="color: red;"></label>
                        </div>						
                         <div class="submit_sec">
                            <!-- <a href="#">subscribe me</a> -->
                            <input class="buttons" type="submit" value="Subscribe me" id="add-form-btn-news">						 
					     </div>
				    </div>
				</form>
					
				</div>


			
			<div class="footer_social">
				<div class="col-md-3 col-sm-3">
					<h3>Payment methods</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/credit.jpg')}}" alt="credit"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/paypal.jpg')}}" alt="paypal"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/visa_logo.jpg')}}" alt="visa_logo"></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3">
					<h3>Follow us on</h3>
					<ul>
						<li><a href="https://www.facebook.com/globalpropertycowboys/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/globalpropertycowboys/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UCHINpiUff1YdH3jkIjVrksA"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/CowboyGlobal"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 drop_cstm">
					<h3>We ship our goods with</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/dhl.png')}}" alt="dhl"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/upslogo.png')}}" alt="upslogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/tntlogo.png')}}" alt="tntlogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/corrlogo.jpg')}}" alt="corrlogo"></a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2 and_sec">
					<h3>Download our app</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/apple.png')}}" alt="apple"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/android.png')}}" alt="android"></a></li>
					</ul>
				</div>
			</div>
			<div class="footerLinks">
				<div class="col-sm-2 col-md-2 col-xs-12">
					<h4>Shopping guide</h4>
					<div>
						<a class="foot_anch" href="{{url('webshop/shop-guide#faqs')}}">Faqs</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#purchasing')}}">Purchasing process</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#payment')}}" >Payment</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#delivery')}}">Delivery</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#returns')}}">Return policy</a>
					</div>
					<div class="xz">
						<img src="{{url('assets/frontend/images/footer_logo.png')}}" alt="logo" class="img-responsive">
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<h4>Policies</h4>
					<div>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-private-customers-gpc.pdf" target="_blank">		Conditions of Purchase of Private
						</a>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-businesses-gpc.pdf" target="_blank">Conditions of Purchase of Business
						</a>
						<a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy Policy</a>
						<a href="{{url('assets/pdf/')}}/cookies-policy.pdf" target="_blank">Cookies Policy</a>
						<a href="{{url('assets/pdf/')}}/cancellation-form.pdf" target="_blank">Cancellation Form</a>
						<a href="{{url('webshop/contact-shopping')}}">Contact Form</a>
						<a href="{{url('webshop/trackorder')}}">Track Order</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12">
					<h4>Company</h4>
					<div class="jm">
						
						<p><span>Address:</span>
							 schalkwijkpad 29<br>
	                         1107JL Amsterdam, the Netherlands<br>
							<strong>VAT number:</strong> NL860137028B01<br>
							<strong>Chaimber of Commerce: </strong>75087545<br>
							
							<strong>Telefone office hours  </strong>: +31 85 06 07 024<br>
							<strong>Whatsapp 24/7  </strong>:  +31 646824266</p>
						<p><span>Email</span>
						<a>customerservice@globalpropertycowboys.nl</a></p>						                                                                     
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<div class="glob_video">
						<iframe src="https://www.youtube.com/embed/St1oxMR2nsA?rel=0&amp;autoplay=0" 
						allowfullscreen="" width="250" height="250" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>

	</div>
</footer>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- jQuery -->
<script src="{{url('assets/frontend/js/jquery.js"')}}"></script>

<script src="{{url('assets/frontend/js/init.js"')}}"></script>
<!--Main js-->

<script src="{{url('assets/frontend/js/scrollIt.min.js"')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{url('assets/frontend/js/owl.carousel.min.js"')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{url('assets/frontend/js/bootstrap.min.js"')}}"></script>

<script type="text/javascript">

 $('#email_sub').keyup(function() {
		$(".errore").fadeOut();
         });
   

 $('#privacy_policy_sub').click(function() {
        if($(this).is(':checked'))
            //alert('checked');
        $('.errorc').html('');
        else
            //alert('unchecked');
        $('.errorc').html('You must accept the privacy policy in order to subscribe.');
        $(".errorc").fadeIn('slow', function(){
        $(".errorc").delay(3000).fadeOut();
         });
    });

	/* for  */
$('#add-form-btn-news').on('click', function(e) {

 // $("#add-form-news").valid();

   var email_sub = $('#email_sub').val();

   console.log(email_sub);
   
      if(email_sub==''){
            $('.errore').html('This field is required');
            $(".errore").fadeIn('slow');
      	     return false;
      }

	if($('#privacy_policy_sub').is(":checked"))
	{
	  
	} else{

		$('.errorc').html('You must accept the privacy policy in order to subscribe.');
		$(".errorc").fadeIn('slow', function(){
        $(".errorc").delay(3000).fadeOut();
         });
		return false;
	}

var privacy_policy_sub = $('#privacy_policy_sub').val();
 
 e.preventDefault();


 $.ajax({

	url:"{{url('customer/subscribenews')}}",
    type:"POST",
	cache:false,
	data:{
      email:email_sub,
      privacy_policy:privacy_policy_sub
    },
	success: function(response){
		var res = JSON.parse(response);
		console.log(res.Success);
		if(res.Error)
		{
		console.log(res.Error);	
		$(".error-news").show();
		$(".error-news").html(res.Error);
		$(".error-news").fadeIn('slow', function(){
        $(".error-news").delay(6000).fadeOut(); 
      });
		//$(".password").html(response.errors.password);
		// do what you want with errors, 
		}
		else
		{
		$('#email_sub').val('');
		$('#privacy_policy_sub').attr('checked', false); // Unchecks it
		$(".error-news").hide();	
		$('#success-news').show();
		$('#success-news').html(res.Success);
		$("#success-news").fadeIn('slow', function(){
        $("#success-news").delay(6000).fadeOut(); 
      });
		//location.reload();

							
	    }
	}
  });

});
</script>
<script>
	//testimonial slide
	var owl = $('#testi_slide');/*
owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
        dots:true,
        nav:true,
		navigation:true,
		navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
});*/
	$('.play').on('click',function(){
		owl.trigger('play.owl.autoplay',[5000])
	})
	$('.stop').on('click',function(){
		owl.trigger('stop.owl.autoplay')
	})
	owl.owlCarousel({
		items:4,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		dots:true,

		navigation:true,
		navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		loop:true,
		margin:15,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:4
			}
		}
	})
</script>

<script defer src="{{url('assets/frontend/js/jquery.flexslider.js')}}"></script>
<script type="text/javascript">
	var curSlide;
	$(function(){
		//SyntaxHighlighter.all();
	});
	$(window).load(function(){
		$('.flexslider').flexslider({
			animation: "fade",
			start: function(slider){
				$('body').removeClass('loading');
			},
			before: function(slider){
				window.curSlide = slider.currentSlide;
				if(window.curSlide=='0'){
					$("ul.side_nav li a").css('color','#fff');
					$(".cv").css('color','#fff');
					$(".cv").css('border-top','1px solid #fff');
					$(".cv").css('border-bottom','1px solid #fff');
				}
				if(window.curSlide=='1'){
					$("ul.side_nav li a").css('color','#fff');
					$(".cv").css('color','#fff');
					$(".cv").css('border-top','1px solid #fff');
					$(".cv").css('border-bottom','1px solid #fff');
				}
				if(window.curSlide=='2'){
					$("ul.side_nav li a").css('color','#1e1e1e');
					$(".cv").css('color','#1e1e1e');
					$(".cv").css('border-top','1px solid #1e1e1e');
					$(".cv").css('border-bottom','1px solid #1e1e1e');
				}
			}
		});
	});
</script>
<script>

	$(document).ready(function()
	{
		window.setTimeout(function(){
			$(".trengo-vue-iframe").contents().find(".widget-reply-tools .material-icons").css("display","none");

	}, 1000);

		// Closes the sidebar menu on menu-close button click event
		$("#menu-close").click(function(e)
		{
           //alert('dfsdfsd');
			$("#sidebar-wrapper").toggleClass("active");
			e.preventDefault();

			/*!
            ======================= Notes ===============================
            * see: .sidebar-wrapper.active in: style.css
            ==================== END Notes ==============================
            */
		});

		// Open the Sidebar-wrapper on Hover
		$("#menu-toggle").hover(function(e)
		{

			$("#sidebar-wrapper").toggleClass("active",true);
			e.preventDefault();
		});

		$("#menu-toggle").bind('click',function(e)
		{
			
			$("#sidebar-wrapper").toggleClass("active");
			e.preventDefault();
		});
		$('#sidebar-wrapper').mouseleave(function(e)

		{
			//alert('sc');
			/*! .toggleClass( className, state ) */
			$('#sidebar-wrapper').toggleClass('active',false);


			e.stopPropagation();


			e.preventDefault();

		});
	});

	function myFunction() {
		if($("#newsletterId").val() != ""){
			$('.newsletterBtn').css('display','block');
		}
		else{
			$('.newsletterBtn').css('display','none');
		}
	};

	function acceptTerms() {

		$('.newsletterDetail').css('display','block');
	};


</script>
<script>
	$(window, document, undefined).ready(function() {
		$('input').blur(function() {
			var $this = $(this);
			if ($this.val())
				$this.addClass('used');
			else
				$this.removeClass('used');
		});
	});
</script>
</body>
</html>