@include('webshop.header')
<style type="text/css">
 .cont_btn .buttons {
    display: block;
    width: 100%;
    max-width: 300px;
    padding: 12px 25px;
    font-size: 11px;
    color: #000;
    border: 1px solid #000;
}

.cont_btn .buttons:hover {
    background: #000;
    color: #fff !important;
}

.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}
.card-title {
    margin-bottom: .75rem;
}
.h5, h5 {
    font-size: 1.25rem;
}
.text-muted {
    color: #6c757d!important;
}

.mb-2, .my-2 {
    margin-bottom: .5rem!important;
}
.card-subtitle {
    margin-top: -.375rem;
    margin-bottom: 0;
}
.h6, h6 {
    font-size: 1rem;
}
</style>


@if(Session::has('message-failure'))
<section style="font-size:18px;">

<p  style="text-align:center; color:red;position:relative !important;width:100% !important;background:#f9f9f9;padding:20px;">{{Session::get('message-failure')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>

</section>
@endif

<?php
$delivery_charges=0;
$items = 0;
$total = 0;
foreach($products as $session){
	$items = $items + $session->quantity;
	$total = $total + ($session->quantity * $session->price);
}

?>
<section class="contact-form contact_sec">
	<div class="shipping">
		<div class="container-fluid">
		  <div class="col-md-8 col-sm-7">
				<form action="{{url('checkout/checkout')}}" method="POST" class="bskt_section" id="checkoutForm">
					<div class="col-md-12 col-sm-12">

					</div>
					<div class="col-md-12 col-sm-12">
						<div class="billing">
							<h2>DELIVERY ADDRESS </h2>
							<h3 for="" class="shipadderr errserver"></h3>
							@if((isset($shippingaddresses[0])))
								<div class="group">
									<select class="form-control showsh" name="ship_add_id" id="shipids">
										@foreach($shippingaddresses as $ct)
										<option value="{{$ct->id}}">{{$ct->alias}}</option>
										@endforeach
									</select>
								</div>
								@foreach ($shippingaddresses as $k =>$ship)
								<div class="shipallinfo">
								<div class="card @if($k !=0) hidesh @endif" style="width: 18rem;" id="shipidsbl{{$ship->id}}">
									<div class="card-body">
										<h5 class="card-title">{{$ship->name}} {{$ship->surname}}</h5>
										<h6 class="card-subtitle mb-2 text-muted">{{$ship->address}}@if($ship->nr!=""),@endif {{$ship->nr}}@if($ship->floor_door!=""),@endif {{$ship->floor_door}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$ship->postalcode}}@if($ship->city!=""),@endif{{$ship->city}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$ship->country}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$ship->telephone}}</h6>

										<a href="" data-toggle="modal" data-target="#myModal{{$ship->id}}" style="float:right;color:#9a9797"><i class="fa fa-edit">Change</i></a>

									</div>
								</div>
								</div>


								@endforeach

							@endif
							<div class="group" id="shipidsnew">
								<div class="cont_btn">
									<a href="#" data-toggle="modal" data-target="#myModaladdchk">New Address</a>
									<!-- <a href="#">continue</a> -->
								</div>
							</div>
							<div class="sub_sec">
								<div class="checkbox">
									<label class="con">
										Use as billing address
										<input id="shipchkk" type="checkbox" name="use_billing_address" value="1" class="text-capitalize">
										<span class="checkmark"></span>
									</label>
								</div>
							</div>
							<h2>BILLING ADDRESS </h2>
							<h3 for="" class="billadderr errserver"></h3>
							<input name="address_id" type="hidden" value="@if(isset($checkout->id)) {{$checkout->id}} @endif"/>
							@if((isset($billing_address->id)))
								<div class="card" style="width: 18rem;">
									<input id="guestusid" name="guest_user_id" type="hidden" value="@if(isset($billing_address->id)) {{$billing_address->id}} @endif"/>
									<div class="card-body">
										<h5 class="card-title">{{$billing_address->firstname}} {{$billing_address->lastname}}</h5>
										<h6 class="card-subtitle mb-2 text-muted">{{$billing_address->address}}@if($billing_address->nr!=""),@endif {{$billing_address->nr}}@if($billing_address->floor!=""),@endif {{$billing_address->floor}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$billing_address->postcode}}@if($billing_address->city!=""),@endif{{$billing_address->city}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$billing_address->country}}</h6>
										<h6 class="card-subtitle mb-2 text-muted">{{$billing_address->phone}}</h6>

										<a href="" data-toggle="modal" data-target="#editAddress" style="float:right;color:#9a9797"><i class="fa fa-edit">Change</i></a>
									</div>
								</div>
							@else
								<div class="radio_sec">
									<label class="radio-inline center cstm_radio indivisual" > <input type="radio" checked="checked" id="radio-1" name="type" value="0"> <span class="checkmark"></span> Individual </label>
									<label class="radio-inline center cstm_radio comp"> <input type="radio" name="type" value="1" id="radio-2"> <span class="checkmark"></span> Company </label>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<select class="form-control" name="country" required>
											<option value="">Country</option>
											@foreach($allcountrues as $ct)
											<option value="{{$ct}}">{{$ct}}</option>
											@endforeach
										</select>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
								   <div class="group">
										<input id="adds_email" class="form_input" name="email" minlength="2" type="email" required/><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="erroremail errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Email*</label>
								   </div>
								</div>
								<div class="col-md-6 col-sm-6">
								   <div class="group">
										<input id="adds_firstname" class="form_input" name="firstname" minlength="2" type="text" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorfirstname errserver"></h3>
										<label class="labletxt" id="namechange" style="text-align: left;font-size: 13px;">Name*</label>
								   </div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_lastname" class="form_input" name="lastname" type="text" required ><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorlastname errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Surname(s)*</label>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
								   </div>
								</div>
								<div class="col-md-6 col-sm-6 compname" >
									<div class="group">
										<input id="adds_comp_name" class="form_input"  name="comp_name" minlength="2" type="text" value="" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorcomp_name errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Company Name*</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 vat" >
									<div class="group">
										<input id="adds_vat" class="form_input"  name="vat" minlength="2" type="text" value="" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorvat errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">VAT*</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 chamberaddress" >
									<div class="group">
										<input id="adds_chamber_commerce" class="form_input"  name="chamber_commerce" minlength="2" type="text" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorchamber_commerce errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Chamber of commerce number*</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_address" class="form_input" name="address" minlength="2" type="text" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="erroraddress errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Address*</label>
								   </div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_nr" class="form_input" type="text" name="nr" minlength="2" ><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errornr errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_floor" class="form_input" name="floor" minlength="2" type="text" ><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorfloor errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Floor/door/etc.</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
								   <div class="group">
										<input id="adds_postcode" class="form_input" name="postcode" type="text" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorpostcode errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">Postcode*</label>
										 <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_city" class="form_input" name="city" minlength="2" type="text"   ><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorcity errserver"></h3>
										<label class="labletxt" style="text-align: left;font-size: 13px;">City</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="group">
										<input id="adds_phone" class="form_input phnnm" name="phone" minlength="10" maxlength="10" type="text" required><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="errorphone errserver"></h3>
										<label id="phonelabel" class="labletxt" style="text-align: left;font-size: 13px;">Phone Number*</label>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
									</div>
									<input type="hidden" name="total_price" id="total_price" value="{{$total}}"/>
								</div>
							@endif

						</div>
					   </div>
						<div class="shipping_sec">
							<span class="bold_sec">SHIPPING</span>
							<h2>Type of delivery </h2>
							<ul id="ship_delevery_mtd">
								@if(isset($billing_methods->shippingOptions[0]))
									@foreach($billing_methods->shippingOptions as $t=>$opt)
										@if($t==0)
											<?php $delivery_charges=number_format($opt->cost,2);?>
										@endif
										<li>
											<div class="radios_sec">
												<label class="radio-inline center cstm_radio"> <input @if($t==0)checked="checked"@endif type="radio" name="delivery_type" prc="{{number_format($opt->cost,2)}}" value="{{$opt->shippingService->id}}"  class="used drop_points"> <span class="checkmark"></span>{{$opt->shippingService->name}}<span>{{$opt->shippingService->delay}}</span> </label>
												<span class="price_sec">{{Config::get('view.currency')}}{{number_format($opt->cost,2)}}</span>
											</div>
										</li>
									@endforeach
								@else
									<li>
										<div class="radios_sec">
											<span style="color:red;">No Shipping Methods are available for your shipping address, Please choose correct shipping address.</span>
										</div>
									</li>
								@endif
							</ul>
						</div>
						<div class="col-md-12 col-sm-12">
								<div class="cont_btn">
								@if(isset($billing_methods->shippingOptions[0]))
									<button id="order_pays" type="submit" class="btn-md pull-right buttons">order with payment committment</button>
								@else
									@if((isset($billing_address->id)))
										<button disabled="disabled" id="order_pays" type="submit" class="btn-md pull-right buttons">order with payment committment</button>
									@else
										<button id="order_pays" type="submit" class="btn-md pull-right buttons">order with payment committment</button>
									@endif
								@endif
								<!-- <a href="#">continue</a> -->
						</div>
					</form>

               <?php
				$items = 0;
				$total = 0;
				$totaltax=0;
				?>
				<div class="col-md-12 col-sm-12">

                    @foreach($products as $session)
					<?php
					$items = $items + $session->quantity;
					$total = $total + ($session->quantity * $session->price);
					//$totaltax= $totaltax +($session->quantity * $session->price * $session->products->taxrate / 100);
					?>
					<div class="prosec">
						<div class="product_buy">
							<div class="product_leftimg bskt_pruduct_img">
								<img src="{{url('public/webshop/bigproducts')}}/{{$session->img}}" class="img-responsive" alt="product image">
							</div>
							<div class="product_rightsec">
								<div class="product_rightinfo">

									<h5><a href="{{url('/webshop/product')}}/{{$session->products->url}}">{{$session->name}}</a><span class="op">
										<ul>
										  <li>Quantity</li>
										  <li>Excl. Tax</li>
										  <li>Tax</li>
										  <li>Incl. Tax</li>
										</ul>
									</span>
									</h5>


								</div>
								<div class="item_description">
									<div class="item_prop">
										@if($session->product_variation_id==0)
											<span class="reference">ref. {{$session->products->sku}}</span>
										@else
											<span class="reference">ref. {{$session->prvars->sku}}</span>
										@endif
										<div class="props"> <span ng-bind-html="::shopCartItem.sizeToShow">{{$session->attributes}}</span> </div>
									</div>
									<div class="item_quantity">
										<ul>
											<li>{{$session->quantity}}</li>
										</ul>
										<span class="total">
											{{Config::get('view.currency')}}{{number_format(($session->quantity * $session->price),2)}}
										</span>

										<?php

				                        $totalPrice = $session->quantity * $session->price;


                                        $finalTotal = ($totalPrice * $tax)/100;

				                        ?>

										<span class="total">
											{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}
										</span>

										<span class="total">
											{{Config::get('view.currency')}}{{number_format(($finalTotal + $totalPrice),2)}}
										</span>

									</div>

								</div>
							</div>
						</div>
					</div>

                     @endforeach
					   <div class="change">
						<a href="{{url('/webshop/basket')}}">
						   change
						</a>
				       </div>
				</div>
		        </div>


		</div>
            <div class="col-md-3 col-sm-4 order_in">
				<div class="order-summary">
					<h2>ORDER SUMMARY </h2>
					<span class="total-items">({{$items}} items)</span>
				</div>
				<?php $totaltax= ($delivery_charges + $totaltax +$total) * $tax / 100; ?>
				<div class="order_sec">
					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Total items:</span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price">{{Config::get('view.currency')}}{{number_format($total,2)}}</span>
						</div>
					</div>

					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Delivery charges:</span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price delivery_charges">{{Config::get('view.currency')}}{{number_format($delivery_charges,2)}}</span>
						</div>
                    </div>

					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Total </span>
							<span class="reference">(Excl. Tax):</span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price tt_prc">{{Config::get('view.currency')}}{{number_format(($delivery_charges + $total),2)}}</span>
						</div>
					</div>

					<div class="rows">
						<div class="col-md-10 col-sm-10">
							<span class="item">Tax: </span>
						</div>
						<div class="col-md-2 col-sm-2">
							<span class="item_res final-price tt_tax">{{Config::get('view.currency')}}{{number_format($totaltax,2)}}</span>
						</div>
					</div>

				</div>

				<div class="cart-total">
						<div class="col-md-8 col-sm-8 total-title"> <span> TOTAL: </span> </div>
						<div class="col-md-4 col-sm-4 total-price final-price final_total">{{Config::get('view.currency')}}{{number_format(($totaltax + $total + $delivery_charges),2)}}</div>
			    </div>
			    <input type="hidden" class="total-items-val" value="{{$items}}" />
		        <input type="hidden" class="product-total" value="{{$total}}" />
				<p class="store"><!-- You still need £26.00 to get free standard delivery! --> </p>
				<div class="free_store">
					<h5><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Free store delivery </h5>
				</div>
				<div class="promote-sec">
					<p>DO YOU HAVE A PROMOTIONAL CODE? Enter it when you reach the payment page </p>
				</div>
            </div>
		</div>
    </div>
</section>

@if((isset($billing_address->id)))
<!-- edit address modal -->
<div class="modal fade" id="editAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	  <h4 class="modal-title" id="myModalLabel"></h4> </div>
      <div class="modal-body">
         <section class="contact-form contact_sec">
	        <div class="log_in df">

                    <form id="billing_add_modall" method="POST" action="#">
					 <div class="radio_sec">
							<label class="radio-inline center cstm_radio indivisual"> <input type="radio" name="type" id="radio_edit_1" value="0" @if ('0' == $billing_address->type) checked @endif> <span class="checkmark"></span> Individual </label>
							<label class="radio-inline center cstm_radio comp"> <input type="radio" name="type" value="1" id="radio_edit_2" @if ('1' == $billing_address->type) checked @endif> <span class="checkmark"></span> Company </label>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="group">
								<select class="form-control" name="country" required>
									<option value="">Country</option>
									@foreach($allcountrues as $ct)
									<option value="{{$ct}}" @if ($ct === $billing_address->country) selected @endif>{{$ct}}</option>
									@endforeach
								</select>
								<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_email" class="form_input @if(isset($billing_address->email)) used @endif" name="billing_email" minlength="2" type="email" value="@if(isset($billing_address->email)) {{$billing_address->email}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="erroremail errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Email*</label>
						   </div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="group">
									<input id="adds_firstname" class="form_input @if(isset($billing_address->firstname)) used @endif" name="firstname" type="text" value="@if(isset($billing_address->firstname)){{$billing_address->firstname}}@endif" required  ><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorfirstname errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Name*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="group">
									<input id="adds_lastname" class="form_input @if(isset($billing_address->lastname)) used @endif" name="lastname" type="text" value="@if(isset($billing_address->lastname)){{$billing_address->lastname}}@endif" required ><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorlastname errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Surname(s)*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
						</div>

						<div class="col-md-6 col-sm-6 compname">
							<div class="group">
									<input id="adds_comp_name" class="compnamein form_input @if(isset($billing_address->comp_name)) used @endif" name="comp_name" value="@if(isset($billing_address->comp_name)){{$billing_address->comp_name}}@endif" required type="text"><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorcomp_name errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Company Name*</label>
									<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
							</div>
						</div>

						<div class="col-md-6 col-sm-6 vat">
						   <div class="group">
									<input id="adds_vat" class="vatin form_input @if(isset($billing_address->vat)) used @endif" name="vat" minlength="2" type="text"  value="@if(isset($billing_address->vat)){{$billing_address->vat}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorvat errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">VAT*</label>
						   </div>
					   </div>

					   <div class="col-md-6 col-sm-6 chamberaddress">
						   <div class="group">
									<input id="adds_chamber_commerce" class="chamberaddressin form_input @if(isset($billing_address->chamber_commerce)) used @endif" name="chamber_commerce" minlength="2" type="text"  value="@if(isset($billing_address->chamber_commerce)){{$billing_address->chamber_commerce}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorchamber_commerce errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Chamber of commerce no.*</label>
						   </div>
						</div>

						<div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_address" class="form_input @if(isset($billing_address->address)) used @endif" name="address" minlength="2" type="text" value="@if(isset($billing_address->address)){{$billing_address->address}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="erroraddress errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Address*</label>
						   </div>
						</div>

						<div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_nr" class="form_input @if(isset($billing_address->nr)) used @endif" type="text" name="nr" minlength="2" type="text"  value="@if(isset($billing_address->nr)){{$billing_address->nr}}@endif"><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errornr errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
					   </div>

					   <div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_floor" class="form_input @if(isset($billing_address->floor)) used @endif" name="floor" minlength="2" type="text"  value="@if(isset($billing_address->floor)){{$billing_address->floor}}@endif"><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorfloor errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Floor/door/etc.</label>
                                     <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
						</div>
					   <div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_postcode" class="form_input @if(isset($billing_address->postcode)) used @endif" name="postcode" type="text"  value="@if(isset($billing_address->postcode)){{$billing_address->postcode}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorpostcode errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Postcode *</label>
                                     <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
						</div>

						<div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_city" class="form_input @if(isset($billing_address->city)) used @endif" name="city" minlength="2" type="text"  value="@if(isset($billing_address->city)){{$billing_address->city}}@endif"><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorcity errserver"></h3>
									<label class="labletxt" style="text-align: left;font-size: 13px;">City</label>
						   </div>
						</div>
                        <div class="col-md-6 col-sm-6">
						   <div class="group">
									<input id="adds_phone" class="phnnm form_input @if(isset($billing_address->phone)) used @endif" name="phone" minlength="10" maxlength="10" type="text"  value="@if(isset($billing_address->phone)){{$billing_address->phone}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<h3 for="" class="errorphone errserver"></h3>
									<label id="phonelabel" class="labletxt" style="text-align: left;font-size: 13px;">Telephone *</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
					   </div>







						<input name="edit_id" type="hidden"  value="@if(isset($checkout->id)){{$checkout->id}}@endif">

						<div class="cont_btn">


						  <button type="submit" class="btn-md pull-right buttons">ACCEPT</button>

					    </div>
                    </form>
					 <!-- form end -->



           </div>
        </section>
      </div>
    </div>
  </div>
</div>
<!-- edit address modal end-->
@endif
<div class="modal fade" id="myModaladdchk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					      <div class="modal-content">
						  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						  </div>
					      <div class="modal-body">
					        <section class="contact-form contact_sec">
						    <div class="log_in df">
										<form method="POST" action="#" id="addressform_add">
										      <div class="col-md-6 col-sm-6">
												<div class="group">
													<select class="form-control" name="country" required>
														<option value="">Country</option>
														@foreach ($allcountrues as $ct)
														<option value="{{$ct}}">{{$ct}}</option>
														@endforeach
													</select>
												</div>
											 </div>
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_name" class="form_input" type="text" name="name" required><span class="highlight" ></span><span class="bar"></span>
													<h3 for="" class="errorname errserver"></h3>
													<label class="labletxt">Name*</label>
				                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_surname" class="form_input" type="text" name="surname" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorsurname errserver"></h3>
														<label class="labletxt">Surname(s)*</label>
														<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
					                        <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_address" class="form_input" type="text" name="address" required><span class="highlight"></span><span class="bar" ></span>
														<h3 for="" class="erroraddress errserver"></h3>
														<label class="labletxt">Address*</label>
					                           			<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_nr" class="form_input" name="nr" minlength="2" type="text" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errornr errserver"></h3>
													<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
												</div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_floor_door" class="form_input" type="text" name="floor_door"><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorfloor_door errserver"></h3>
														<label class="labletxt">Floor/door/etc.</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_postalcode" class="form_input" type="text" name="postalcode" required><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorpostalcode errserver"></h3>
														<label class="labletxt">Postcode*</label>
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_city" class="form_input" type="text" name="city" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorcity errserver"></h3>
														<label class="labletxt">City*</label>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_telephone" class="form_input" type="text" name="telephone" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errortelephone errserver"></h3>
														<label class="labletxt">Telephone*</label>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_alias" class="form_input" type="text" name="alias" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="erroralias errserver"></h3>
														<label class="labletxt">Alias*</label>
											   </div>
											</div>

										    <div class="col-md-6 col-sm-6">
												<div class="submit_sec fgd">
												    <input class="buttons" type="submit" value="ACCEPT">
											    </div>
											</div>

										</form>
					        </div>
					        </section>
					      </div>
					      </div>
					  </div>
			</div>
@foreach ($shippingaddresses as $k =>$ship)
<div class="modal fade" id="myModal{{$ship->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					      <div class="modal-content">
						  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						  </div>
					      <div class="modal-body">
					        <section class="contact-form contact_sec">
						    <div class="log_in df">
										<form id="myfrm{{$ship->id}}" method="POST" action="#" class="addressformed">
											<input type="hidden" name="id" value="@if(isset($ship->id)){{$ship->id}} @endif">
											<div class="col-md-6 col-sm-6">
												<div class="group">
													<select class="form-control" name="country" required>
														<option value="">Country</option>
														@foreach ($allcountrues as $ct)
														<option value="{{$ct}}"  @if ($ct === $ship->country) selected @endif >{{$ct}}</option>
														@endforeach
													</select>
												</div>
											</div>
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_name" class="form_input @if(isset($ship->name))used @endif" type="text" name="name" value="@if(isset($ship->name)){{$ship->name}}@endif" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errorname errserver"></h3>
													<label class="labletxt">Name*</label>
				                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_surname" class="form_input @if(isset($ship->surname)) used @endif" type="text" name="surname" value="@if(isset($ship->surname)){{$ship->surname}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorsurname errserver"></h3>
														<label class="labletxt">Surname(s)*</label>
														<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
					                        <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_address" class="form_input @if(isset($ship->address))used @endif" type="text" name="address" value="@if(isset($ship->address)){{$ship->address}}@endif" required><span class="highlight"></span><span class="bar" ></span>
														<h3 for="" class="erroraddress errserver"></h3>
														<label class="labletxt">Address*</label>
					                           			<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_nr" class="form_input @if(isset($ship->nr)) used @endif" name="nr" minlength="2" type="text"  value="@if(isset($ship->nr)){{$ship->nr}}@endif" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errornr errserver"></h3>
													<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
												</div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_floor_door" class="form_input @if(isset($ship->floor_door)) used @endif" type="text" name="floor_door" value="@if(isset($ship->floor_door)){{$ship->floor_door}}@endif" ><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorfloor_door errserver"></h3>
														<label class="labletxt">Floor/door/etc.</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_postalcode" class="form_input @if(isset($ship->postalcode))used @endif" type="text" name="postalcode" value="@if(isset($ship->postalcode)){{$ship->postalcode}}@endif" required><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorpostalcode errserver"></h3>
														<label class="labletxt">Postcode*</label>
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_city" class="form_input @if(isset($ship->city))used @endif" type="text" name="city" value="@if(isset($ship->city)){{$ship->city}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorcity errserver"></h3>
														<label class="labletxt">City*</label>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_telephone" class="form_input @if(isset($ship->telephone)) used @endif" type="text" name="telephone" value="@if(isset($ship->telephone)){{$ship->telephone}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errortelephone errserver"></h3>
														<label class="labletxt">Telephone*</label>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_alias" class="form_input @if(isset($ship->alias)) used @endif" type="text" name="alias" value="@if(isset($ship->alias)){{$ship->alias}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="erroralias errserver"></h3>
														<label class="labletxt">Alias*</label>
											   </div>
											</div>

										    <div class="col-md-6 col-sm-6">
												<div class="submit_sec fgd">
												    <input class="buttons" type="submit" value="ACCEPT">
											    </div>
											</div>

										</form>
					        </div>
					        </section>
					      </div>
					      </div>
					  </div>
					</div>
@endforeach
@include('webshop.nomenu_footer')
<script src="{{url('assets/frontend/js/jquery.validate.js')}}"></script>
<script type="text/javascript">
var tax      = "{{$tax}}";
var currency = "{{Config::get('view.currency')}}";
var finalGrandTotal = ('{{$total}}' * tax)/100;

$(document).on( "click", ".drop_points",function() {
	var shipping=$(this).attr('prc');
	var total = (parseFloat(shipping) + parseFloat('{{$total}}') + parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100));
	$('.delivery_charges').html(currency + parseFloat(shipping).toFixed(2));
	$('.final_total').html(currency + parseFloat(total).toFixed(2));
	$('.product-total').val(parseFloat(total).toFixed(2));
	$('#total_price').val(parseFloat(total).toFixed(2));
	$('.tt_prc').html(currency + (parseFloat('{{$total}}')+ parseFloat(shipping)).toFixed(2));
	$('.tt_tax').html(currency + (parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100)).toFixed(2));
});


	$(document).ready(function(){
		$("#radio_edit_1").click(function(){
	 	$("#editnamechange").html('Name *');
		});
	    $("#radio_edit_2").click(function(){
	  	$("#editnamechange").html('Company Name *');
	});
	if($("input[type='radio']#radio_edit_1").is(':checked')) {
		$("#editnamechange").html('Name *');


	}else{
		$("#editnamechange").html('Company Name *');

	}

	var name=$("#name").val();
	if(name!=''){
	$("#name").focus();
	}
	var eml=$("#eml").val();
	if(eml!=''){
	$("#eml").focus();
	}
	var companyname=$("#companyname").val();
	var taxidcode=$("taxidcode").val();
	var firstname=$("#firstname").val();
	var lastname=$("#lastname").val();
	var email=$("#email").val();
	if(email!=''){
		$("#email").focus();
	}
	if(firstname!=''){
		$("#firstname").focus();
	}
	if(lastname!=''){
		$("#lastname").focus();
	}
	if(companyname!=''){

		$("#companyname").focus();
	}
	if(taxidcode!=''){
		$("#taxidcode").focus();
	}
	 $("#radio-1").click(function(){
	 	$("#namechange").html('Name *');


		// $("#form1").show();
		// $("#form2").hide();
	 });
	  $("#radio-2").click(function(){
		  alert("hiiii");
	  	$("#namechange").html('Company Name *');

		 //$("#form2").show();
		 //$("#form1").hide();
	 });
	if($("input[type='radio']#radio-1").is(':checked')) {
		$("#namechange").html('Name *');

		//$("#form1").show();
	 //$("#form2").hide();

	}else{
		$("#namechange").html('Company Name *');
		// $("#form2").show();
		// $("#form1").hide();

	}



// $("#form1").validate({
// rules: {

// 	name: "required",
// 	email : "required",
// 	password : "required",
// 	policy:"required",

// },
// messages: {
//    email: "Please enter your Email",
//    name: "Please enter your name",
//    password: "Please enter Password",
//    policy:"You must accept the privacy policy ",
// },
// submitHandler:function(form){

//         form.submit();
//     }

// });


$("#checkoutForm").validate({

		rules:{
radio:"required",
/*province:"required",*/

name:"required",
surname:"required",
email:"required",
/*nr:"required",*/
postal_code:"required",
/*telephone:"required",*/
nif:"required",
address:"required",
/*f_d:"required",*/
/*city:"required",*/

		},
messages:{
radio:"This field is required",
/*province:"This field is required",*/
name:"This field is required",
surname:"This field is required",
email:"This field is required",
/*nr:"This field is required",*/
postal_code:"This field is required",
nif:"This field is required",
address:"This field is required",
/*address:"This field is required",*/
/*f_d:"This field is required",*/
/*city:"This field is required",*/


},

submitHandler: function(form) {
            form.submit();
        }
});






});



</script>




<script>


$(".indivisual").click(function(){
	$('.compnamein').removeAttr('required');
	$('.vatin').removeAttr('required');
	$('.chamberaddressin').removeAttr('required');
	$(".compname").hide();
	$(".vat").hide();
	$(".chamberaddress").hide();
	$("#phonelabel").html('Phone Number*');
	$('.phnnm').attr('required','required');
});

$(".comp").click(function(){
	$(".compname").show();
	$(".vat").show();
	$(".chamberaddress").show();
	$("#phonelabel").html('Phone Number');
	$('.phnnm').removeAttr('required');
	$('.compnamein').attr('required','required');
	$('.vatin').attr('required','required');
	$('.chamberaddressin').attr('required','required');
});
@if(isset($billing_address->type))
	var typeradio= "{{$billing_address->type}}";
	if(typeradio=="0"){
		$(".compname").hide();
		$(".vat").hide();
		$(".chamberaddress").hide();
	}
	if(typeradio=="1"){
		$("#phonelabel").html('Phone Number');
		$('.phnnm').removeAttr('required');
		$('.compnamein').attr('required','required');
		$('.vatin').attr('required','required');
		$('.chamberaddressin').attr('required','required');
	}
@else
	$(".compname").hide();
	$(".vat").hide();
	$(".chamberaddress").hide();
@endif



</script>

<script>
$("#shipchkk").click(function(){
	var same_billing='no';
	if($(this).prop("checked") == true){
		$("#shipids").removeClass('showsh');
		$("#shipids").parent().removeClass('showsh');
		$("#shipids").addClass('hidesh');
		$("#shipids").parent().addClass('hidesh');
		$(".shipallinfo").removeClass('showsh');
		$(".shipallinfo").addClass('hidesh');
		$("#shipidsnew").removeClass('showsh');
		$("#shipidsnew").addClass('hidesh');
		var add_id=0;
		var same_billing='yes';
	}
	else if($(this).prop("checked") == false){
		$("#shipids").removeClass('hidesh');
		$("#shipids").parent().removeClass('hidesh');
		$("#shipids").addClass('showsh');
		$("#shipids").parent().addClass('showsh');
		$(".shipallinfo").removeClass('hidesh');
		$(".shipallinfo").addClass('showsh');
		$("#shipidsnew").removeClass('hidesh');
		$("#shipidsnew").addClass('showsh');
		var add_id=$("#shipids").val();
		if(add_id==null){
			add_id=0;
		}
		var same_billing='no';
	}
	var shipping=0;
	 var total = (parseFloat(shipping) + parseFloat('{{$total}}') + parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100));
	$('.delivery_charges').html(currency + parseFloat(shipping).toFixed(2));
	$('.final_total').html(currency + parseFloat(total).toFixed(2));
	$('.product-total').val(parseFloat(total).toFixed(2));
	$('#total_price').val(parseFloat(total).toFixed(2));
	$('.tt_prc').html(currency + (parseFloat('{{$total}}')+ parseFloat(shipping)).toFixed(2));
	$('.tt_tax').html(currency + (parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100)).toFixed(2));
	$('#ship_delevery_mtd').html('<p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading...</p>');
	$("#order_pays").attr('disabled','disabled');
	$.ajax({
		data:{'add_id':add_id,'same_billing':same_billing},
		url:base_url+'checkout/shipindeliveryguest',
		type:"POST",
		cache:false,
		success: function(res){
			var res = JSON.parse(res);
			if(res.html != ''){
				$('#ship_delevery_mtd').html(res.html);
			}
			if(res.resp=='error'){
				$("#order_pays").attr('disabled','disabled');
			}
			else{
				$("#order_pays").removeAttr('disabled');
			}
			var shipping=res.shipping_price;
			 var total = (parseFloat(shipping) + parseFloat('{{$total}}') + parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100));
	$('.delivery_charges').html(currency + parseFloat(shipping).toFixed(2));
	$('.final_total').html(currency + parseFloat(total).toFixed(2));
	$('.product-total').val(parseFloat(total).toFixed(2));
	$('#total_price').val(parseFloat(total).toFixed(2));
	$('.tt_prc').html(currency + (parseFloat('{{$total}}')+ parseFloat(shipping)).toFixed(2));
	$('.tt_tax').html(currency + (parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100)).toFixed(2));
		}
	});
});
var base_url = "{{url('/')}}/";
jQuery("#billing_add_modall").validate({
	submitHandler: function(form, event) {
		event.preventDefault();
		$("#billing_add_modall #adds_email").keyup(function(){$("#billing_add_modall .erroremail").fadeOut();});
		$("#billing_add_modall #adds_firstname").keyup(function(){$("#billing_add_modall .errorfirstname").fadeOut();});
		$("#billing_add_modall #adds_lastname").keyup(function(){$("#billing_add_modall .errorlastname").fadeOut();});
		$("#billing_add_modall #adds_address").keyup(function(){$("#billing_add_modall .erroraddress").fadeOut();});
		$("#billing_add_modall #adds_postcode").keyup(function(){$("#billing_add_modall .errorpostcode").fadeOut();});
		$("#billing_add_modall #adds_phone").keyup(function(){$("#billing_add_modall .errorphone").fadeOut();});
		$("#billing_add_modall #adds_comp_name").keyup(function(){$("#billing_add_modall .errorcomp_name").fadeOut();});
		$("#billing_add_modall #adds_vat").keyup(function(){$("#billing_add_modall .errorvat").fadeOut();});
		$("#billing_add_modall #adds_chamber_commerce").keyup(function(){$("#billing_add_modall .errorchamber_commerce").fadeOut();});
		var radio=$("input[name='type']:checked").val();
		var data = $('#billing_add_modall').serializeArray();
		$.ajax({
			data:data,
			url:base_url+'checkout/billingupdate',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.email){
						$("#billing_add_modall .erroremail").html(res.errors.email);
						$("#billing_add_modall .erroremail").fadeIn('slow');
					}
					else{
						$("#billing_add_modall .erroremail").html('');
					}
					if(res.errors.firstname){
						$("#billing_add_modall .errorfirstname").html(res.errors.firstname);
						$("#billing_add_modall .errorfirstname").fadeIn('slow');
					}
					else{
						$("#billing_add_modall .errorfirstname").html('');
					}
					if(res.errors.lastname){
						$("#billing_add_modall .errorlastname").html(res.errors.lastname);
						$("#billing_add_modall .errorlastname").fadeIn('slow');
					}
					else{
						$("#billing_add_modall .errorlastname").html('');
					}

					if(res.errors.address){
						$("#billing_add_modall .erroraddress").html(res.errors.address);
						$("#billing_add_modall .erroraddress").fadeIn('slow');
					}
					else{
						$("#billing_add_modall .erroraddress").html('');
					}
					if(res.errors.postcode){
						$("#billing_add_modall .errorpostcode").html(res.errors.postcode);
						$("#billing_add_modall .errorpostcode").fadeIn('slow');
					}
					else{
						$("#billing_add_modall .errorpostcode").html('');
					}
					if(radio==0){
						if(res.errors.phone){
							$("#billing_add_modall .errorphone").html(res.errors.phone);
							$("#billing_add_modall .errorphone").fadeIn('slow');
						}
						else{
							$("#billing_add_modall .errorphone").html('');
						}
					}
					if(radio==1){
						if(res.errors.comp_name){
							$("#billing_add_modall .errorcomp_name").html(res.errors.comp_name);
							$("#billing_add_modall .errorcomp_name").fadeIn('slow');
						}
						else{
							$("#billing_add_modall .errorcomp_name").html('');
						}
						if(res.errors.vat){
							$("#billing_add_modall .errorvat").html(res.errors.vat);
							$("#billing_add_modall .errorvat").fadeIn('slow');
						}
						else{
							$("#billing_add_modall .errorvat").html('');
						}
						if(res.errors.chamber_commerce){
							$("#billing_add_modall .errorchamber_commerce").html(res.errors.chamber_commerce);
							$("#billing_add_modall .errorchamber_commerce").fadeIn('slow');
						}
						else{
							$("#billing_add_modall .errorchamber_commerce").html('');
						}
					}
				}
				else{
					location.reload();
				}
			}
		});
	}
});
jQuery("#addressform_add").validate({
	submitHandler: function(form, event) {
		event.preventDefault();
		$("#addressform_add #adds_name").keyup(function(){$("#addressform_add .errorname").fadeOut();});
		$("#addressform_add #adds_surname").keyup(function(){$("#addressform_add .errorsurname").fadeOut();});
		$("#addressform_add #adds_address").keyup(function(){$("#addressform_add .erroraddress").fadeOut();});
		$("#addressform_add #adds_city").keyup(function(){$("#addressform_add .errorcity").fadeOut();});
		$("#addressform_add #adds_postalcode").keyup(function(){$("#addressform_add .errorpostalcode").fadeOut();});
		$("#addressform_add #adds_telephone").keyup(function(){$("#addressform_add .errortelephone").fadeOut();});
		$("#addressform_add #adds_alias").keyup(function(){$("#addressform_add .erroralias").fadeOut();});
		var data = $('#addressform_add').serializeArray();
		$.ajax({
			data:data,
			url:base_url+'checkout/shippingadd',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.name){
						$("#addressform_add .errorname").html(res.errors.name);
						$("#addressform_add .errorname").fadeIn('slow');
					}
					else{
						$("#addressform_add .errorename").html('');
					}
					if(res.errors.surname){
						$("#addressform_add .errorsurname").html(res.errors.surname);
						$("#addressform_add .errorsurname").fadeIn('slow');
					}
					else{
						$("#addressform_add .errorsurname").html('');
					}
					if(res.errors.address){
						$("#addressform_add .erroraddress").html(res.errors.address);
						$("#addressform_add .erroraddress").fadeIn('slow');
					}
					else{
						$("#addressform_add .erroraddress").html('');
					}
					if(res.errors.postalcode){
						$("#addressform_add .errorpostalcode").html(res.errors.postalcode);
						$("#addressform_add .errorpostalcode").fadeIn('slow');
					}
					else{
						$("#addressform_add .errorpostalcode").html('');
					}
					if(res.errors.city){
						$("#addressform_add .errorcity").html(res.errors.city);
						$("#addressform_add .errorcity").fadeIn('slow');
					}
					else{
						$("#addressform_add .errorcity").html('');
					}
					if(res.errors.telephone){
						$("#addressform_add .errortelephone").html(res.errors.telephone);
						$("#addressform_add .errortelephone").fadeIn('slow');
					}
					else{
						$("#addressform_add .errortelephone").html('');
					}
					if(res.errors.alias){
						$("#addressform_add .erroralias").html(res.errors.alias);
						$("#addressform_add .erroralias").fadeIn('slow');
					}
					else{
						$("#addressform_add .erroralias").html('');
					}
				}
				else{
					location.reload();
				}
			}
		});
	}
});
@foreach ($shippingaddresses as $k =>$ship)
$("#myfrm{{$ship->id}}").validate({
	submitHandler: function(form,event) {
		event.preventDefault();
		$("#myfrm{{$ship->id}} #adds_name").keyup(function(){$("#myfrm{{$ship->id}} .errorname").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_surname").keyup(function(){$("#myfrm{{$ship->id}} .errorsurname").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_address").keyup(function(){$("#myfrm{{$ship->id}} .erroraddress").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_city").keyup(function(){$("#myfrm{{$ship->id}} .errorcity").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_postalcode").keyup(function(){$("#myfrm{{$ship->id}} .errorpostalcode").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_telephone").keyup(function(){$("#myfrm{{$ship->id}} .errortelephone").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_alias").keyup(function(){$("#myfrm{{$ship->id}} .erroralias").fadeOut();});
		var data = $("#myfrm{{$ship->id}}").serializeArray();
		$.ajax({
			data:data,
			url:base_url+'checkout/shippingedit',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.name){
						$("#myfrm{{$ship->id}} .errorname").html(res.errors.name);
						$("#myfrm{{$ship->id}} .errorname").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorename").html('');
					}
					if(res.errors.surname){
						$("#myfrm{{$ship->id}} .errorsurname").html(res.errors.surname);
						$("#myfrm{{$ship->id}} .errorsurname").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorsurname").html('');
					}
					if(res.errors.address){
						$("#myfrm{{$ship->id}} .erroraddress").html(res.errors.address);
						$("#myfrm{{$ship->id}} .erroraddress").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .erroraddress").html('');
					}
					if(res.errors.postalcode){
						$("#myfrm{{$ship->id}} .errorpostalcode").html(res.errors.postalcode);
						$("#myfrm{{$ship->id}} .errorpostalcode").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorpostalcode").html('');
					}
					if(res.errors.city){
						$("#myfrm{{$ship->id}} .errorcity").html(res.errors.city);
						$("#myfrm{{$ship->id}} .errorcity").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorcity").html('');
					}
					if(res.errors.telephone){
						$("#myfrm{{$ship->id}} .errortelephone").html(res.errors.telephone);
						$("#myfrm{{$ship->id}} .errortelephone").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errortelephone").html('');
					}
					if(res.errors.alias){
						$("#myfrm{{$ship->id}} .erroralias").html(res.errors.alias);
						$("#myfrm{{$ship->id}} .erroralias").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .erroralias").html('');
					}
				}
				else{
					location.reload();
				}
			}
		});
    }
});
@endforeach
$('#shipids').change(function() {
	var addid=$(this).val();
	var cardid="shipidsbl"+addid;
	$(".shipallinfo .card").removeClass('showsh');
	$(".shipallinfo .card").addClass('hidesh');
	$("#"+cardid).removeClass('hidesh');
	$("#"+cardid).addClass('showsh');
	var shipping=0;
	 var total = (parseFloat(shipping) + parseFloat('{{$total}}') + parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100));
	$('.delivery_charges').html(currency + parseFloat(shipping).toFixed(2));
	$('.final_total').html(currency + parseFloat(total).toFixed(2));
	$('.product-total').val(parseFloat(total).toFixed(2));
	$('#total_price').val(parseFloat(total).toFixed(2));
	$('.tt_prc').html(currency + (parseFloat('{{$total}}')+ parseFloat(shipping)).toFixed(2));
	$('.tt_tax').html(currency + (parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100)).toFixed(2));
	$('#ship_delevery_mtd').html('<p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading...</p>');
	$("#order_pays").attr('disabled','disabled');
	$.ajax({
		data:{'add_id':addid,'same_billing':'no'},
		url:base_url+'checkout/shipindeliveryguest',
		type:"POST",
		cache:false,
		success: function(res){
			var res = JSON.parse(res);
			if(res.html != ''){
				$('#ship_delevery_mtd').html(res.html);
			}
			if(res.resp=='error'){
				$("#order_pays").attr('disabled','disabled');
			}
			else{
				$("#order_pays").removeAttr('disabled');
			}
			var shipping=res.shipping_price;
			 var total = (parseFloat(shipping) + parseFloat('{{$total}}') + parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100));
	$('.delivery_charges').html(currency + parseFloat(shipping).toFixed(2));
	$('.final_total').html(currency + parseFloat(total).toFixed(2));
	$('.product-total').val(parseFloat(total).toFixed(2));
	$('#total_price').val(parseFloat(total).toFixed(2));
	$('.tt_prc').html(currency + (parseFloat('{{$total}}')+ parseFloat(shipping)).toFixed(2));
	$('.tt_tax').html(currency + (parseFloat(finalGrandTotal) + parseFloat(shipping*tax/100)).toFixed(2));
		}
	});
});

$('#order_pays').on('click', function(e){
	e.preventDefault();
	if(($("#guestusid").length)){
		$(".shipadderr").html('');
		$(".billadderr").html('');
		if($('#shipchkk').prop("checked") == false){
			if(!($("#shipids").length)){
				$(".shipadderr").html('please add your shipping address');
				return false;
			}
			var shippid=$("#shipids").val();
		}
		else{
			var shippid=0;
		}
		$.ajax({
			data:{'ship_id':shippid},
			url:base_url+'checkout/verifyguestcheckout',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.ship){
					   $(".shipadderr").html(res.errors.ship);
					   $(".shipadderr").fadeIn('slow');
					}
					else{
						$(".shipadderr").html('');
					}
					if(res.errors.bill){
						$(".billadderr").html(res.errors.bill);
						$(".billadderr").fadeIn('slow');
					}
					else{
						$(".billadderr").html('');
					}
					if(res.errors.common){
						window.location.href="/webshop";
					}
				}
				else{
					$("#checkoutForm").submit();
				}
			}
		});
	}
	else{
		if($("#checkoutForm").valid()){
			var radio=$("#checkoutForm input[name='type']:checked").val();
			var data = $("#checkoutForm").serializeArray();
			$.ajax({
				data:data,
				url:base_url+'checkout/billingadd',
				type:"POST",
				cache:false,
				success: function(response){
					var res = JSON.parse(response);
					console.log(res);
					if(res.errors){
						if(res.errors.email){
							$("#checkoutForm .erroremail").html(res.errors.email);
							$("#checkoutForm .erroremail").fadeIn('slow');
						}
						else{
							$("#checkoutForm .erroremail").html('');
						}
						if(res.errors.firstname){
							$("#checkoutForm .errorfirstname").html(res.errors.firstname);
							$("#checkoutForm .errorfirstname").fadeIn('slow');
						}
						else{
							$("#checkoutForm .errorfirstname").html('');
						}
						if(res.errors.lastname){
							$("#checkoutForm .errorlastname").html(res.errors.lastname);
							$("#checkoutForm .errorlastname").fadeIn('slow');
						}
						else{
							$("#checkoutForm .errorlastname").html('');
						}

						if(res.errors.address){
							$("#checkoutForm .erroraddress").html(res.errors.address);
							$("#checkoutForm .erroraddress").fadeIn('slow');
						}
						else{
							$("#checkoutForm .erroraddress").html('');
						}
						if(res.errors.postcode){
							$("#checkoutForm .errorpostcode").html(res.errors.postcode);
							$("#checkoutForm .errorpostcode").fadeIn('slow');
						}
						else{
							$("#checkoutForm .errorpostcode").html('');
						}
						if(radio==0){
							if(res.errors.phone){
								$("#checkoutForm .errorphone").html(res.errors.phone);
								$("#checkoutForm .errorphone").fadeIn('slow');
							}
							else{
								$("#checkoutForm .errorphone").html('');
							}
						}
						if(radio==1){
							if(res.errors.comp_name){
								$("#checkoutForm .errorcomp_name").html(res.errors.comp_name);
								$("#checkoutForm .errorcomp_name").fadeIn('slow');
							}
							else{
								$("#checkoutForm .errorcomp_name").html('');
							}
							if(res.errors.vat){
								$("#checkoutForm .errorvat").html(res.errors.vat);
								$("#checkoutForm .errorvat").fadeIn('slow');
							}
							else{
								$("#checkoutForm .errorvat").html('');
							}
							if(res.errors.chamber_commerce){
								$("#checkoutForm .errorchamber_commerce").html(res.errors.chamber_commerce);
								$("#checkoutForm .errorchamber_commerce").fadeIn('slow');
							}
							else{
								$("#checkoutForm .errorchamber_commerce").html('');
							}
						}
					}
					else{
						$("#adds_email").html('<input id="guestusid" name="guest_user_id" type="hidden" value="0"/>');
						$("#order_pays" ).trigger( "click" );
					}
				}
			});
		}
	}

});

$("#checkoutForm #adds_email").keyup(function(){$("#checkoutForm .erroremail").fadeOut();});
$("#checkoutForm #adds_firstname").keyup(function(){$("#checkoutForm .errorfirstname").fadeOut();});
$("#checkoutForm #adds_lastname").keyup(function(){$("#checkoutForm .errorlastname").fadeOut();});
$("#checkoutForm #adds_address").keyup(function(){$("#checkoutForm .erroraddress").fadeOut();});
$("#checkoutForm #adds_postcode").keyup(function(){$("#checkoutForm .errorpostcode").fadeOut();});
$("#checkoutForm #adds_phone").keyup(function(){$("#checkoutForm .errorphone").fadeOut();});
$("#checkoutForm #adds_comp_name").keyup(function(){$("#checkoutForm .errorcomp_name").fadeOut();});
$("#checkoutForm #adds_vat").keyup(function(){$("#checkoutForm .errorvat").fadeOut();});
$("#checkoutForm #adds_chamber_commerce").keyup(function(){$("#checkoutForm .errorchamber_commerce").fadeOut();});
</script>
