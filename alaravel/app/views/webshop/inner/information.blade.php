
<style type="text/css">
	.errorsss{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif

 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES</a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>																				
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-9 col-md-offset-1 adres_cstm">
	    <div class="row">
	    	<div class="col-md-5 col-sm-5">
		    	<div class="address_sec">
					<h2>BILLING DETAILS</h2>
					<!--<h4>{{Auth::user()->username}}</h4>-->
					<h4>@if(isset($shipping->firstname)){{$shipping->firstname}} @endif @if(isset($shipping->lastname)){{$shipping->lastname}} @endif</h4>
					<h4>@if(isset($shipping->address)){{$shipping->address}} @endif @if(isset($shipping->nr)){{$shipping->nr}}@endif @if(isset($shipping->floor)){{$shipping->floor}}@endif</h4>
					<h4>@if(isset($shipping->postcode)){{$shipping->postcode}} @endif @if(isset($shipping->city)){{$shipping->city}}@endif</h4>
					<h4>@if(isset($shipping->phone)){{$shipping->phone}} @endif</h4>
					<h4>@if(isset($shipping->country)){{$shipping->country}} @endif</h4>
					<h4><a href="#" data-toggle="modal" data-target="#billdetails"><i class="fa fa-edit"></i> Change</a></h4>
					
				</div>
			</div>	
			<div class="col-md-5 col-sm-5">
		    	<div class="address_sec">
					<h2>MAIN EMAIL</h2>
					<h4>{{Auth::user()->email}}</h4>
					<h4>This email will be used for accessing your private pages as well as for receiving correspondence and newsletters.</h4>
					<h4><a href="#" data-toggle="modal" data-target="#changeemail"><i class="fa fa-edit"></i> Change</a></h4>
				</div>
			</div>	
	    </div>

	    <div class="row" style="margin-top:20px">
	    	<div class="col-md-5 col-sm-5">
		    	<div class="address_sec">
					<h2>PASSWORD</h2>
					<h4>**************</h4>
					<h4><a href="#" data-toggle="modal" data-target="#changepassword"><i class="fa fa-edit"></i> Change</a></h4>
				</div>
			</div>	
			<div class="col-md-5 col-sm-5">
		    	<div class="address_sec">
					
					<h4>At GPC HOME we take your privacy very seriously and are committed to the protection of your personal data. Learn more about how we care for and use your data in <br> our <a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy policy</a></h4>
					
				</div>
			</div>	
	    </div>		
			
           	
	  </div>

</section> 


<!-- modal change password-->
<div class="modal fade" id="changepassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	      <div class="modal-content">
		  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
		  </div>
	      <div class="modal-body">
	        <section class="contact-form contact_sec">
		    <div class="log_in df">
		           <div class="alert alert-success success_pass" style="display: none;">
                   </div>
                   <div class="alert alert-danger error_pass" style="display: none;">
                   </div>	
		    <h4>CHANGE PASSWORD</h4>
		    <p>You may change your access password. For security reason keep it in a safe place and do not share it with anyone.</p>	  		  			 										
						<form id="form-change-password">
							<div class="col-md-12 col-sm-12">
								<div class="group">
									<input class="form_input" id="newpass" type="password" name="current_password" value=""><span class="highlight">
									</span><span class="bar"></span>
									<h3 for="" id="currentpass" class="current_password" id="newpasserror" style="color: red; margin-top: 4px;"></h3>
									<label class="labletxt">CURRENT PASSWORD*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
							   </div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="group">											                  
										<input class="form_input" id="currentpass" type="password" name="password" value=""><span class="highlight">
										</span><span class="bar"></span>
										<h3 for="" class="password"  id="currentpasserror" style="color: red; margin-top: 4px;"></h3>	
										<label class="labletxt">NEW PASSWORD*</label>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
							   </div>
							</div>

	                        <div class="col-md-6 col-sm-6">
								<div class="group">											                  
										<input class="form_input" type="password" name="password_confirmation" value=""><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="password_confirmation" style="color: red; margin-top: 4px;"></h3>
										<label class="labletxt">REPEAT PASSWORD*</label>
	                           			<div class="md-errors-spacer"><div class="md-char-counter"></div></div>						
							   </div>
							</div>							
							
						    <div class="col-md-6 col-sm-6">
								<div class="submit_sec fgd">
								    <input class="buttons form-change-password-btn" type="submit" value="SAVE">
							    </div>
							</div>
							
						</form>											 		 				
	        </div>
	        </section> 
	      </div>    
	      </div>
	  </div>
</div>
<!--end modal change password-->


<!-- modal billdetails-->
<div class="modal fade" id="billdetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	      <div class="modal-content">
		  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
		  </div>
	      <div class="modal-body">
	        <section class="contact-form contact_sec">
		    <div class="log_in df">
		           
		      		  			 										
			<form id="loginform" method="POST" action="{{url('customer/address')}}" novalidate>
						
					 <div class="radio_sec">	
							<label class="radio-inline center cstm_radio indivisual"> <input type="radio" name="radio" id="radio_edit_1" value="0" @if ($shipping->type==0) checked @endif> <span class="checkmark"></span> Individual </label>
							<label class="radio-inline center cstm_radio comp"> <input  @if ($shipping->type==1) checked @endif type="radio" name="radio" id="radio_edit_2" value="1"> <span class="checkmark"></span> Company </label>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="group">											                  
									<!--<input class="form_input @if(isset($shipping->province)) used @endif" name="province" value="@if(isset($shipping->province)){{$shipping->province}} @endif" required type="text"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">province(s)*</label>
									<div class="md-errors-spacer"><div class="md-char-counter"></div></div>-->
									 <select class="form-control" name="country" value="@if(isset($shipping->country)) {{$shipping->country}} @endif" required>
										<option value="">Country</option>
										@foreach ($allcountrues as $ct)
										<option value="{{$ct}}"  @if ($ct === $shipping->country) selected @endif >{{$ct}}</option>
										@endforeach
							
									</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->billing_email)) used @endif" name="billing_email" minlength="2" type="email" value="@if(isset($shipping->billing_email)) {{$shipping->billing_email}}@endif" required /><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Email*</label>
									<div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
						   </div>
						</div>
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->firstname)) used @endif" name="firstname" minlength="2" type="text"  value="@if(isset($shipping->firstname)){{$shipping->firstname}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" id="editnamechange" style="text-align: left;font-size: 13px;">Name*</label>													
						   </div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="group">											                  
									<input class="form_input @if(isset($shipping->lastname)) used @endif" name="lastname" type="text" value="@if(isset($shipping->lastname)){{$shipping->lastname}}@endif" required ><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Surname(s)*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
						   </div>
						</div>
						<div class="col-md-6 col-sm-6 compname" >
						   <div class="group">											                  
									<input class="compnamein form_input @if(isset($shipping->comp_name))used @endif"  name="comp_name" minlength="2" type="text" value="@if(isset($shipping->comp_name)){{$shipping->comp_name}}@endif" ><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Company Name*</label>													
						   </div>
						</div>
						
						<div class="col-md-6 col-sm-6 vat" >
						   <div class="group">											                  
									<input class="vatin form_input @if(isset($shipping->vat))used @endif"  name="vat" minlength="2" type="text" value="@if(isset($shipping->vat)){{$shipping->vat}}@endif" ><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">VAT*</label>													
						   </div>
						</div>
						<div class="col-md-6 col-sm-6 chamberaddress" >
						   <div class="group">
									<input class="chamberaddressin form_input @if(isset($shipping->chamber_commerce))used @endif"  name="chamber_commerce" type="text" value="@if(isset($shipping->chamber_commerce)){{$shipping->chamber_commerce}}@endif" ><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Chamber of commerce number*</label>
																						
						   </div>
						</div>
						
						
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->address)) used @endif" name="address" minlength="2" type="text" value="@if(isset($shipping->address)){{$shipping->address}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Address*</label>													
						   </div>
						</div>
						
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->nr)) used @endif" name="nr" minlength="2" type="text"  value="@if(isset($shipping->nr)){{$shipping->nr}}@endif" ><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>													
						   </div>
						</div>
							
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->floor)) used @endif" name="floor" minlength="2" type="text"  value="@if(isset($shipping->floor)){{$shipping->floor}}@endif"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Floor/door/etc.</label>													
						   </div>
						</div>	
						
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->postcode)) used @endif" name="postcode"  type="text"  value="@if(isset($shipping->postcode)){{$shipping->postcode}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Postcode *</label>
                                     <div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
						   </div>
						</div>

						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->city)) used @endif" name="city" minlength="2" type="text"  value="@if(isset($shipping->city)){{$shipping->city}}@endif"><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">City</label>													
						   </div>
						</div>
						
						<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="phnnm form_input @if(isset($shipping->phone)) used @endif" type="text" name="phone" minlength="10" maxlength="10" type="text"  value="@if(isset($shipping->phone)){{$shipping->phone}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label id="phonelabel" class="labletxt" style="text-align: left;font-size: 13px;">Phone Number*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
						   </div>
					   </div>
					   
                        <!--<div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->telephone)) used @endif" name="telephone" minlength="10" maxlength="10" type="text"  value="@if(isset($shipping->telephone)){{$shipping->telephone}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Telephone *</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
						   </div>
					   </div>
					   
                       <div class="col-md-6 col-sm-6">
						   <div class="group">											                  
									<input class="form_input @if(isset($shipping->nif)) used @endif" name="nif" minlength="2" type="text"  value="@if(isset($shipping->nif)){{$shipping->nif}}@endif" required><span class="highlight"></span><span class="bar"></span>
									<label class="labletxt" style="text-align: left;font-size: 13px;">Tax ID number(NIF)*</label>													
						   </div>
					   </div>
					   -->

						
						
						<input name="edit_id" type="hidden"  value="@if(isset($checkout->id)){{$checkout->id}}@endif">

						<div class="cont_btn">


						  <button type="submit" class="btn-md pull-right buttons">ACCEPT</button>
					   
					    </div>
                    </form>										 		 				
	        </div>
	        </section> 
	      </div>    
	      </div>
	  </div>
</div>



<!-- modal change email-->
<div class="modal fade" id="changeemail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	      <div class="modal-content">
		  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
		  </div>
	      <div class="modal-body">
	        <section class="contact-form contact_sec">
		    <div class="log_in df">
		           <div class="alert alert-success success_email" style="display: none;">
                   </div>
                   <div class="alert alert-danger error_email" style="display: none;">
                   </div>	
		    <h4>CHANGE EMAIL</h4>
		    <p>Enter a new email address. Remember that you are also changing the username with which you enter "My Account”.
		    	<br>
		    	<br>
		     You can change your access email address. This is also the email address where you will receive notices and bulletins. We request your password for security reasons.	
		    </p>
		      		  			 										
						<form id="form-change-email">
							<div class="col-md-12 col-sm-12">
								<div class="group">
									<input class="form_input" type="email" name="email" value=""><span class="highlight">
									</span><span class="bar"></span>
									<h3 for="" class="email" style="color: red; margin-top: 4px;"></h3>
									<label class="labletxt">New email address*</label>
                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>									
							   </div>
							</div>

							 <div class="col-md-6 col-sm-6">
								<div class="group">											                  
										<input class="form_input" type="email" name="email_confirmation" value=""><span class="highlight"></span><span class="bar"></span>
										<h3 for="" class="email_confirmation" style="color: red; margin-top: 4px;"></h3>
										<label class="labletxt">Re-enter the New Email Address*</label>
	                           			<div class="md-errors-spacer"><div class="md-char-counter">0 / 30</div></div>						
							   </div>
							</div>	

							<div class="col-md-6 col-sm-6">
								<div class="group">											                  
										<input class="form_input" type="password" name="password" value=""><span class="highlight">
										</span><span class="bar"></span>
										<h3 for="" class="password" style="color: red; margin-top: 4px;"></h3>	
										<label class="labletxt">For security reasons, please enter your password*</label>
										<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
							   </div>
							</div>

	                       						
							
						    <div class="col-md-6 col-sm-6">
								<div class="submit_sec fgd">
								    <input class="buttons form-change-email-btn" type="submit" value="SAVE">
							    </div>
							</div>
							
						</form>											 		 				
	        </div>
	        </section> 
	      </div>    
	      </div>
	  </div>
</div>
<!--end modal change email-->
@include('webshop.inner.inner_footer')
<script type="text/javascript">
var baseurl  = "{{url('/')}}"	
/* for password */
$('.form-change-password-btn').on('click', function(e) {

 e.preventDefault();
 

 
  var data = $('#form-change-password').serializeArray();


 $.ajax({

	data:data,
	url:baseurl+'/customer/changepassword',
    type:"POST",
	cache:false,

	success: function(response){
		

		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {

    	  if(res.errors.current_password){
           $(".current_password").html(res.errors.current_password);
           $(".current_password").fadeIn('slow');
	       }else{

	       	$(".current_password").html('');
	       }
	      
	      if(res.errors.password){
	      $(".password").html(res.errors.password);
	      $(".password").fadeIn('slow');
	      }else{

	       	$(".password").html('');
	       }
          if(res.errors.password_confirmation){
	      $(".password_confirmation").html(res.errors.password_confirmation);
	      $(".password_confirmation").fadeIn('slow', function(){
	        $(".password_confirmation").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".password_confirmation").html('');
	       }
         
	      
	    }
	    else
	    {
		
		if(res.Success){
		$('.success_pass').html(res.Success);
		$(".success_pass").fadeIn('slow', function(){
	        $(".success_pass").delay(3000).fadeOut(); 
	      });
		$('.success_pass').show();
		$('#form-change-password')[0].reset();
		location.reload();
		 }
		 else{

            $('.error_pass').html(res.Error);
		    $(".error_pass").fadeIn('slow', function(){
	        $(".error_pass").delay(3000).fadeOut(); 
	         });
		    $('.error_pass').show();
		 }
		//location.reload();

							
	    }
	}
  });

});




/* for password end */




/* for email */
$('.form-change-email-btn').on('click', function(e) {
 e.preventDefault();
 

 
  var data = $('#form-change-email').serializeArray();
 $.ajax({

	data:data,
	url:baseurl+'/customer/changeemail',
    type:"POST",
	cache:false,

	success: function(response){
		var res = JSON.parse(response);
		console.log(res);
		if(res.errors)
	    {

    	  if(res.errors.email){
           $(".email").html(res.errors.email);
           $(".email").fadeIn('slow', function(){
           $(".email").delay(3000).fadeOut(); 
	      });
	       }else{

	       	$(".email").html('');
	       }
	      
	      if(res.errors.password){
	      $(".password").html(res.errors.password);
	      $(".password").fadeIn('slow', function(){
	        $(".password").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".password").html('');
	       }
          if(res.errors.email_confirmation){
	      $(".email_confirmation").html(res.errors.email_confirmation);
	      $(".email_confirmation").fadeIn('slow', function(){
	        $(".email_confirmation").delay(3000).fadeOut(); 
	      });
	      }else{

	       	$(".email_confirmation").html('');
	       }
         
	      
	    }
	    else
	    {
		
		if(res.Success){
		$('.success_email').html(res.Success);
		$(".success_email").fadeIn('slow', function(){
	        $(".success_email").delay(3000).fadeOut(); 
	      });
		$('.success_email').show();
		$('#form-change-email')[0].reset();
		 location.reload();
		 }
		 else{

            $('.error_email').html(res.Error);
		    $(".error_email").fadeIn('slow', function(){
	        $(".error_email").delay(3000).fadeOut(); 
	         });
		    $('.error_email').show();
		 }
		//

							
	    }
	}
  });

});

/* for email end */
</script>



<script>


$(".indivisual").click(function(){
	$('.compnamein').removeAttr('required');
	$('.vatin').removeAttr('required');
	$('.chamberaddressin').removeAttr('required');	
	$(".compname").hide();
	$(".vat").hide();
	$(".chamberaddress").hide();
	$("#phonelabel").html('Phone Number*');
	$('.phnnm').attr('required','required');
});

$(".comp").click(function(){
	$(".compname").show();
	$(".vat").show();
	$(".chamberaddress").show();
	$("#phonelabel").html('Phone Number');
	$('.phnnm').removeAttr('required');
	$('.compnamein').attr('required','required');
	$('.vatin').attr('required','required');
	$('.chamberaddressin').attr('required','required');
});
var typeradio= "{{$shipping->type}}";

if(typeradio=="0"){
	$(".compname").hide();
	$(".vat").hide();
	$(".chamberaddress").hide();
}

if(typeradio=="1"){
	$("#phonelabel").html('Phone Number');
	$('.phnnm').removeAttr('required');
	$('.compnamein').attr('required','required');
	$('.vatin').attr('required','required');
	$('.chamberaddressin').attr('required','required');
}




</script>


<script>

jQuery("#loginform").validate({
	
	rules:{

email:"required",
name:"required",

		},
messages:{


email:"This field is required",
name:"This field is required",


		  
},

submitHandler: function(form) {
            form.submit();
        }
});

</script>
