<!-- footer
  ==============================-->
<footer class="footer">
	<div class="container">
		<div class="col-md-9 col-sm-9 footer_fg">
			<div class="newsletter">
					<div class="alert alert-success" id="success-news" style="display: none;">
                       </div>
					
						<div class="alert alert-danger error-news" style="display: none;">
                        </div>
				<form method="POST"  class="add-form-news">
					<div class="group" style="margin-bottom:10px;">
							<input class="form_input" type="text" name="email_sub" id="email_sub" placeholder="" ><span class="highlight"></span><span class="bar"></span>
							<h3 for="" class="errore" style="color: red; margin-top: 4px;"></h3>
							<label class="labletxt">Join our newsletter - Enter your email address</label>
							
				    </div>
					<!-- <div class="form-group newsletterBtn"> -->												
					<div class="form-group">												
                        <div class="checkbox">
							<label class="con">
							  I agree to receive customised commercial communications from Global Property Cowboys via email and other means
							  <input type="checkbox" id="privacy_policy_sub" name="privacy_policy_sub" value="Yes">
							  <span class="checkmark"></span> 						  
							</label>
							<label for="" class="errorc" style="color: red;"></label>
                        </div>						
                         <div class="submit_sec">
                            <!-- <a href="#">subscribe me</a> -->
                            <input class="buttons" type="submit" value="Subscribe me" id="add-form-btn-news">						 
					     </div>
				    </div>
				</form>
					
				</div>
			
			<div class="footer_social">
				<div class="col-md-3 col-sm-3">
					<h3>Payment methods</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/credit.jpg')}}" alt="credit"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/paypal.jpg')}}" alt="paypal"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/visa_logo.jpg')}}" alt="visa_logo"></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3">
					<h3>Follow us on</h3>
					<ul>
						<li><a href="https://www.facebook.com/globalpropertycowboys/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/globalpropertycowboys/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UCHINpiUff1YdH3jkIjVrksA"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/CowboyGlobal"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 drop_cstm">
					<h3>We ship our goods with</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/dhl.png')}}" alt="dhl"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/upslogo.png')}}" alt="upslogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/tntlogo.png')}}" alt="tntlogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/corrlogo.jpg')}}" alt="corrlogo"></a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2 and_sec">
					<h3>Download our app</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/apple.png')}}" alt="apple"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/android.png')}}" alt="android"></a></li>
					</ul>
				</div>
			</div>
			<div class="footerLinks">
				<div class="col-sm-2 col-md-2 col-xs-12">
					<h4>Shopping guide</h4>
					<div>
						<a href="{{url('webshop/shop-guide#faqs')}}">Faqs</a>
						<a href="{{url('webshop/shop-guide#purchasing')}}">Purchasing process</a>
						<a href="{{url('webshop/shop-guide#payment')}}" >Payment</a>
						<a href="{{url('webshop/shop-guide#delivery')}}">Delivery</a>
						<a href="{{url('webshop/shop-guide#returns')}}">Return policy</a>
					</div>
					<div class="xz">
						<img src="{{url('assets/frontend/images/footer_logo.png')}}" alt="logo" class="img-responsive">
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<h4>Policies</h4>
					<div>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-private-customers-gpc.pdf" target="_blank">Conditions of Purchase of Private</a>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-businesses-gpc.pdf" target="_blank">Conditions of Purchase of Business</a>
						<a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy Policy</a>
						<a href="{{url('assets/pdf/')}}/cookies-policy.pdf" target="_blank">Cookies Policy</a>
						<a href="{{url('assets/pdf/')}}/cancellation-form.pdf" target="_blank">Cancellation Form</a>
						<a href="{{url('webshop/contact-shopping')}}">Contact Form</a>
						<a href="{{url('webshop/trackorder')}}">Track Order</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12">
					<h4>Company</h4>
					<div class="jm">
						
						<p><span>Address:</span>
							 schalkwijkpad 29<br>
	                         1107JL Amsterdam, the Netherlands<br>
							<strong>VAT number:</strong> NL860137028B01<br>
							<strong>Chaimber of Commerce: </strong>75087545<br>
							
							<strong>Telefone office hours  </strong>: +31 85 06 07 024<br>
							<strong>Whatsapp 24/7  </strong>:  +31 646824266</p>
						<p><span>Email</span>
						<a>customerservice@globalpropertycowboys.nl</a></p>
						
					</div>

				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<div class="glob_video">
						<iframe src="https://www.youtube.com/embed/St1oxMR2nsA?rel=0&amp;autoplay=0" 
						allowfullscreen="" width="250" height="250" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>

	</div>
</footer>


<script src="{{url('assets/frontend/js/jquery.js')}}"></script>
<script src="{{url('assets/frontend/js/scrollIt.min.js')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{url('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js" ></script>
<script type="text/javascript">



 $('#email_sub').keyup(function() {
        if($(this).val()=='')
            //alert('checked');
        
        $('.errore').html('This field is required');
        else
            //alert('unchecked');
        $('.errore').html('');
    });

 $('#privacy_policy_sub').click(function() {
        if($(this).is(':checked'))
            //alert('checked');
        $('.errorc').html('');
        else
            //alert('unchecked');
        $('.errorc').html('Please checked the required field');
    });

	/* for  */
$('#add-form-btn-news').on('click', function(e) {

 // $("#add-form-news").valid();

   var email_sub = $('#email_sub').val();

   console.log(email_sub);
   
      if(email_sub==''){
            $('.errore').html('This field is required');
      	     return false;
      }

	if($('#privacy_policy_sub').is(":checked"))
	{
	  
	} else{

		$('.errorc').html('Please checked the required field');
		return false;
	}

var privacy_policy_sub = $('#privacy_policy_sub').val();
 
 e.preventDefault();


 $.ajax({

	url:"{{url('customer/subscribenews')}}",
    type:"POST",
	cache:false,
	data:{
      email:email_sub,
      privacy_policy:privacy_policy_sub
    },
	success: function(response){
		var res = JSON.parse(response);
		if(res.Error)
		{
		console.log(res.Error);	
		$(".error-news").show();
		$(".error-news").html(res.Error);
		$(".error-news").fadeIn('slow', function(){
        $(".error-news").delay(6000).fadeOut(); 
      });
		//$(".password").html(response.errors.password);
		// do what you want with errors, 
		}
		else
		{

		$('#email_sub').val('');
		$('#privacy_policy_sub').attr('checked', false); // Unchecks it		
		$(".error-news").hide();	
		$('#success-news').show();
		$('#success-news').html(res.Success);
		$("#success-news").fadeIn('slow', function(){
        $("#success-news").delay(6000).fadeOut(); 
      });
		//location.reload();

							
	    }
	}
  });

});
</script>
<script>

$(document).ready(function()
{
	
	// Closes the sidebar menu on menu-close button click event
	$("#menu-close").click(function(e)					
	{
															
		$("#sidebar-wrapper").toggleClass("active");			
		e.preventDefault(); 									
		
		/*!
		======================= Notes ===============================
		* see: .sidebar-wrapper.active in: style.css
		==================== END Notes ==============================
		*/
	});													

	// Open the Sidebar-wrapper on Hover
	$("#menu-toggle").hover(function(e)							
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();									
	});

	$("#menu-toggle").bind('click',function(e)					
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();								
	});														
	$('#sidebar-wrapper').mouseleave(function(e)				
																
	{
		/*! .toggleClass( className, state ) */
		$('#sidebar-wrapper').toggleClass('active',false);		
																
													
		e.stopPropagation();									
																
																
		e.preventDefault();										
																
	});
});
</script>
<script>
$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
</script>
<script>
$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
$( ".form_input" ).focus(function() {
		//alert( "Handler for .focus() called." );
		if($(this).hasClass('error')){
			console.log("yes");
			$(this).siblings('.bar').addClass('errorbar');
		}
		else{
			$(this).siblings('.bar').removeClass('errorbar');
		}
	});
	$( ".form_input" ).keyup(function() {
		//alert( "Handler for .focus() called." );
		if($(this).hasClass('error')){
			console.log("yes");
			$(this).siblings('.bar').addClass('errorbar');
		}
		else{
			$(this).siblings('.bar').removeClass('errorbar');
		}
	});
</script>


<script>
   
     
function toggleIcon(e) {
$(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('glyphicon-minus glyphicon-plus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
$(document).ready(function(){
	var sb=0;
	$('#form').on('submit', function(e){
		if(sb==0){
			e.preventDefault();
			var name = $('#cname').val();
			var email = $('#cemail').val();
			var cphone = $('#cphone').val();
			var ccom = $('#ccom').val();
			var ctitle = $('#ctitle').val();
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
				dd = '0'+dd
			} 

			if(mm<10) {
				mm = '0'+mm
			} 

			today = mm + '/' + dd + '/' + yyyy;
			var date=today;
			$.ajax({
				async :false,
				type: 'POST',
				dataType: 'json',
				url: 'https://hooks.zapier.com/hooks/catch/3598911/qsbebz/',
				data: 'name='+name+'&email='+email+'&phone='+cphone+'&company='+ccom+'&title='+ctitle+'&date='+date,
				success: function(data) {
					if((data.status != undefined) && (data.status == 'success')){
							console.log(data);
							
					}
				}
			});
			sb=1;
			$('#form').submit();
		}
	});	
});
 </script>
 
</body>
</html>