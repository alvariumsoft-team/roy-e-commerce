@include('webshop.inner.inner_header')

<section class="contact-form">
	<div class="container-fluid">
		<div class="col-md-2 col-sm-2">
			<ul class="wish_nav">
				<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
				<li><a href="{{url('customer/address')}}">MY ADDRESSES </a></li>
				<li><a href="{{url('customer/order')}}">ORDERS </a></li>
				<li><a href="{{url('customer/cards')}}">CARDS </a></li>
				<li><a href="{{url('customer/notification')}}">Notifications </a></li>
				<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>
			</ul>
		</div>
	  <div class="col-md-10 col-sm-10 wish_sec">
            @if(isset($wishlists[0]))
			<div class="col-md-9 col-sm-9 wish_info">
				<div class="wish_main">
					<span class="icn_heart"><i class="fa fa-heart" aria-hidden="true"></i></span>
					<span class="text">Wish name @if(isset($wish_selected[0])){{$wish_selected[0]->list_name}}@else{{$wishlists[0]->list_name}}@endif</span>
				</div>
				@if(!isset($wish_prod_detail[0]))
					<h5>You do not have any items in your Wishlist</h5>
					<p>Add them by clicking the <span><i class="fa fa-heart" aria-hidden="true"></i></span>heart icon on each item.You will have your favourites to hand and it will make future purchases and gifts easier.</p>
				@endif
				@foreach($wish_prod_detail  as $k =>$wishpr)
				<div class="wishproduct_info" id="wish_block_{{$wishpr->id}}">
					<div class="wish_img">
						<a href="#">
                          <img src="{{url('public/webshop/bigproducts')}}/{{$wishpr->pr_images->img_name}}" class="img-responsive" alt="cama">
				        </a>
					</div>
					<div class="wish_detail">
						<h2><a href="#">{{$wishpr->products->name}}</a>
						<span class="xc">
							<a href="#" class="addtocartprwish" wish-pr-id="{{$wishpr->id}}"><i class="fa fa-lock" aria-hidden="true"></i></a>
							<a href="#" class="removeprwish" wish-pr-id="{{$wishpr->id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
						</span>
						</h2>
						@if(isset($wishpr->prvars->atts))
						<p>{{$wishpr->prvars->atts}}</p>
						@endif
						@if(isset($wishpr->prvars->sku))
							<p>ref. {{$wishpr->prvars->sku}}</p>
						@else
							<p>ref. {{$wishpr->products->sku}}</p>
						@endif
						@if(isset($wishpr->prvars->height))
							<p>{{$wishpr->prvars->height}} x {{$wishpr->prvars->width}} x {{$wishpr->prvars->depth}} cm</p>
						@else
							<p>{{$wishpr->products->height}} x {{$wishpr->products->width}} x {{$wishpr->products->depth}} cm</p>
						@endif
						<div class="col-md-4 col-sm-4 iow">
							<div class="wish_fg">
								@if(isset($wishpr->prvars->retailprice))
							<p id="wishqtyy_{{$wishpr->id}}">{{$wishpr->quantity}}x {{Config::get('view.currency')}}{{$wishpr->prvars->retailprice}}</p>
						@else
							<p id="wishqtyy_{{$wishpr->id}}">{{$wishpr->quantity}}x {{Config::get('view.currency')}}{{$wishpr->products->retailprice}}</p>
						@endif

							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="multi">
								<ul>
								<li><a href="#" class="chg_qty" wish-pr-id="{{$wishpr->id}}" qtype="minus"><i class="fa fa-minus quantity_remove"></i></a></li>
								<li>
									<input id="wishqty_{{$wishpr->id}}" class="wsh_quantity{{$wishpr->id}} product_quantity used" type="text" minlength="1" value="{{$wishpr->quantity}}">
								</li>
								<li><a href="#" class="chg_qty" wish-pr-id="{{$wishpr->id}}" qtype="plus"><i class="fa fa-plus quantity_add"></i></a></li>
							</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="wish_fg">
							@if(isset($wishpr->prvars->retailprice))
							<p id="wsh_prc_{{$wishpr->id}}">{{Config::get('view.currency')}}{{number_format(($wishpr->prvars->retailprice * $wishpr->quantity),2) }}</p>
						@else
							<p id="wsh_prc_{{$wishpr->id}}">{{Config::get('view.currency')}}{{number_format(($wishpr->products->retailprice * $wishpr->quantity),2) }}</p>
						@endif

							</div>
						</div>
					</div>
				</div>
				@endforeach
				@if(isset($wish_prod_detail[0]))
				<div class="col-md-12 col-sm-12">
					<div class="add_ev">
						<a href="#" data-toggle="modal" data-target="#modaldeletecon"><span><i class="fa fa-lock" aria-hidden="true"></i></span>add everything to basket</a>
					</div>
				</div>
				@endif
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="wish_box">
				@foreach($wishlists  as $k =>$wish)
					<div class="wish_sv">
						<div class="add_wish">
							<a href="{{url('customer/wishlist?id='.$wish->id)}}">{{$wish->list_name}}</a><span class="user-wishlist-number">({{$wish->count}})</span>
						</div>
						<div class="del_wish">
							<ul>
							   <li><a href=""  data-toggle="modal" data-target="#edit_wish{{$wish->list_name}}"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
							   <li><a wish-list-id="{{$wish->id}}" class= "remove_wh_add" href="javscript://void(0);"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
					<div id="edit_wish{{$wish->list_name}}" class="modal fade bs-example-modal-smss" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
					  <div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<form action="{{url('customer/wishlistupdate')}}" id="update_wish_form{{$wish->id}}">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span><div></div></button>
								<div class="group">
									<input type="hidden" name="id" value="{{$wish->id}}">
									<input name="list_name_update" class="listup form_input @if(isset($wish->list_name))used @endif" type="text" value="@if(isset($wish->list_name)){{$wish->list_name}}@endif"><span class="highlight" required></span><span class="bar"></span>
									<h3 for="" class="errorname errserver"></h3>
									<label class="labletxt">Wish List Name*</label>
									<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
								</div>
								<div class="sub_sec"></div>
								<a formsel="{{$wish->id}}" class="wish_upf">accept</a>
							</form>
						</div>
					  </div>
					</div>
				@endforeach
				</div>
				<div class="new_df">

					<a href="#" class="add_wh_list" data-toggle="modal" data-target=".bs-example-modal-sms">
						<span><i class="fa fa-plus" aria-hidden="true"></i></span>New wish list
					</a>

				</div>
			</div>
			@else
				<div class="wishlist">
					<p>You have not yet created a wish list. Add a new one! </p>
					<a href="#" class="add_wh_list" data-toggle="modal" data-target=".bs-example-modal-sms">
						<span><i class="fa fa-plus" aria-hidden="true"></i></span>New wish list
					</a>
				</div>
			@endif

			<div class="modal fade bs-example-modal-sms" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modalwindow">
			  <div class="modal-dialog modal-sm" role="document">
				<form method="POST" action="#" id="wish_add_list">
				<div class="modal-content">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span><div></div></button>
				   <div class="group">
						<input class="form_input" name="list_name" id="wish_add" type="text" value=""><span class="highlight"></span><span class="bar"></span>
						<h3 for="" class="errorname errserver"></h3>
						<label class="labletxt">Wish List Name*</label>
                         <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
				   </div>
				   <div class="sub_sec">

					</div>
					<a href="#" class="#" id="new_wish" >accept</a>
				</div>
				</form>
			  </div>
			</div>


            <div id="modaldeletecon" class="modal fade bs-example-modal-smsss" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="wish_delete">
			  <div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span><div></div></button>
				   <div class="wish_pop">
						<h5>What do you want to do with your wishlist?</h5>
				   </div>
				   <div class="war_sec ">
						<a class="bnv everythng" href="#" todo="keep">Keep items in the wishlist</a>
						<a class="bnv everythng" href="#" todo="delete">Delete items from the wishlist</a>
						<a href="#" todo="deleteall" class="everythng"><span><i class="fa fa-trash" aria-hidden="true"></i></span>Delete wishlist</a>

				   </div>
				</div>
			  </div>
			</div>
	  </div>

</section>


@include('webshop.inner.inner_footer')

<script>
var base_url = "{{url('/')}}/";
$(document).ready(function(){

	$('.remove_wh_add').on('click', function(e){
		if(confirm("Are you sure want to delete this wishlist. ?")){
			$.ajax({
				data:{'id':$(this).attr('wish-list-id')},
				url:base_url+'customer/wishlistremove',
				type:"POST",
				cache:false,
				success: function(response){
					location.reload();
				}
			});
		}
	});

	$('#new_wish').on('click', function(e){
		var data = $('#wish_add_list').serializeArray();
		$.ajax({
			data:data,
			url:base_url+'customer/wishadd',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.list_name){
						$("#wish_add_list .errorname").html(res.errors.list_name);
						$("#wish_add_list .errorname").fadeIn('slow');
					}
					else{
						$("#wish_add_list .errorename").html('');
					}
				}
				else{
					window.location.href="/customer/wishlist";
				}
			}
		});

	});

	$('.wish_upf').on('click', function(e){
		var frmid=$(this).attr('formsel');;
		var data = $('#update_wish_form'+frmid).serializeArray();
		$.ajax({
			data:data,
			url:base_url+'customer/wishlistupdate',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.list_name){
						$("#update_wish_form"+frmid+" .errorname").html(res.errors.list_name);
						$("#update_wish_form"+frmid+" .errorname").fadeIn('slow');
					}
					else{
						$("#update_wish_form"+frmid+" .errorname").html('');
					}
				}
				else{
					window.location.href="/customer/wishlist";
				}
			}
		});

	});

	$("#wish_add").keyup(function(){$(".errorname").fadeOut();});
	$(".listup").keyup(function(){$(".errorname").fadeOut();});

	$('.removeprwish').on('click', function(e){
		if(confirm("Are you sure want to delete this product from wishlist. ?")){
			$.ajax({
				data:{'id':$(this).attr('wish-pr-id')},
				url:base_url+'customer/prwishlistremove',
				type:"POST",
				cache:false,
				success: function(response){
					location.reload();
				}
			});
		}
	});


	$('.addtocartprwish').on('click', function(e){
		$.ajax({
			data:{'id':$(this).attr('wish-pr-id')},
			url:base_url+'customer/wishaddtocart',
			type:"POST",
			cache:false,
			dataType: "json",
			success: function (data) {
				$('.addId').addClass('basketshow');
				$("#total_items").text(data.len);
				$('#productscroll').html(data.html);
				$('.slide_basket').animate({"width":"290px"}, "slow").css('display', 'block');
			},
			error: function (x,s,e) {

			}
		});
	});


	$('.chg_qty').on('click', function(e){
		var qtype=$(this).attr('qtype');
		var wish_id=$(this).attr('wish-pr-id');
		var cr_qty=$("#wishqty_"+wish_id).val();
		var cr_qty = parseInt(cr_qty);
		if(qtype=='plus'){
			var nwqty=cr_qty+1;
		}
		if(qtype=='minus'){
			if(cr_qty > 1){
				var nwqty=cr_qty-1;
			}
			else{
				var nwqty=1;
			}
		}
		$("#wishqty_"+wish_id).val(nwqty);
		$.ajax({
			data:{'id':$(this).attr('wish-pr-id'),'quantity':nwqty},
			url:base_url+'customer/updatecartwish',
			type:"POST",
			cache:false,
			dataType: "json",
			success: function (data) {
				if(data.sucess=true){
					$("#wsh_prc_"+wish_id).html(data.price);
					$("#wishqtyy_"+wish_id).html(data.qtyprc);
				}
			},
			error: function (x,s,e) {

			}
		});
	});


	$('.everythng').on('click', function(e){
		var todo=$(this).attr('todo');
		$.ajax({
			data:{'wish_list_id':{{$sel_wish_id}},'todo':todo},
			url:base_url+'customer/wishalladdtocart',
			type:"POST",
			cache:false,
			dataType: "json",
			success: function (data) {
				$('.addId').addClass('basketshow');
				$("#total_items").text(data.len);
				$('#productscroll').html(data.html);
				$('.slide_basket').animate({"width":"290px"}, "slow").css('display', 'block');
				if(todo!="keep"){
					location.reload();
				}
			},
			error: function (x,s,e) {

			}
		});
	});


 });
</script>
