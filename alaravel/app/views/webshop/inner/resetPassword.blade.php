@include('webshop.header')
<section class="contact-form contact_sec">
	<div class="log_in">
		<div class="container">		  
		  <div class="col-md-8 col-sm-8">
			  <div class="col-md-7 col-sm-7">
					<h4 style="margin-bottom:15px;">Reset Your Password</h4>
					@if (Session::get('message-success'))
					   <div class="alert alert-success">
			            {{Session::get('message-success')}}
                       </div>
					@endif	
					@if (Session::get('message-error'))
						<div class="alert alert-danger">
			             {{Session::get('message-error')}}
                      </div>
					@endif	
					<form name="login_form" method="POST" action={{url('forgotpassword/resetpassword')}}>
					   
					    <div class="group {{ $errors->has('password') ? ' error' : '' }}">											                  
								<input class="form_input" type="password"  name="password"><span class="highlight"></span><span class="bar"></span>
								@if ($errors->has('password'))
								<i class="error">{{ $errors->first('password') }}</i>
							    @endif
								<label class="labletxt vb">Password*<a href="#"></a></label>													
					   </div>
					   <div class="group {{ $errors->has('password_confirmation') ? ' error' : '' }}">											                  
								<input class="form_input" type="password"  name="password_confirmation"><span class="highlight"></span><span class="bar"></span>
								@if ($errors->has('password_confirmation'))
								<i class="error">{{ $errors->first('password_confirmation') }}</i>
							    @endif
								<label class="labletxt vb">Confirm Password*<a href="#"></a></label>													
					   </div>
					     <input type="hidden" name="email" value="{{$email}}">
					     <input type="hidden" name="code" value="{{$code}}">
						<div class="submit_sec">
							<input class="buttons" type="submit" name="submit" value="SUBMIT">
						</div>
					</form>
					
			  </div>
			   
			 
		  </div>		
		</div>
    </div>
</section>
<style type="text/css">
 .submit_sec .buttons {
    display: block;
    width: 100%;
    max-width: 300px;
    padding: 12px 25px;
    font-size: 11px;
    color: #000;
    border: 1px solid #000;
}

.submit_sec .buttons:hover {
    background: #000;
    color: #fff !important;
}
</style>
@include('webshop.nomenu_footer') 