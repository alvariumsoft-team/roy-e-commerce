<style type="text/css">
	.error{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES </a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>																				
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-9 col-md-offset-1">		
			<div class="card_sect">
				<p>You do not currently have any saved cards.</p>
				<p>To enjoy the benefits of Easy Purchase, activate the option "Save card" in your next online purchase<span class="star-fav">★</span></p>
				<p>Remember that your details are completely secure and will only be used to make your purchases easier.</p>
			</div>						
	  </div>

</section> 


@include('webshop.inner.inner_footer')
