@include('webshop.inner.inner_header')

 
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES</a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>										
		</ul>
	  </div>
	  <div class="col-md-9 col-sm-9 col-md-offset-1">		
			<div class="notification">
				<h2>NEWSLETTER</h2>
				@if (Auth::user()->receieveemail == 1)
					<p>You have signed up to receive the our weekly newsletter. Thank you for your interest in GPC</p>
					<div class="clearfix res-newsletter"> 
					<strong>E-mail</strong>
					<div> <span>{{Auth::user()->email}}</span> 
					<p> <a href="{{url('customer/information')}}">Change Details</a> </p>
					</div> 
					</div>
					@else 
					<p>You did not subscribed our weekly newsletter, Please click on subscribe button for subscription.</p>
					<div class="clearfix res-newsletter"> 
						<p> <a href="{{url('customer/information')}}">Change Details</a> </p>
					</div> 
					
				@endif
			</div>
          	<div class="unsub_btn">
			<!--<input class="button" type="submit" value="unsubscribe" id="sub_unsubs_btn">-->
				@if (Auth::user()->receieveemail == 1)
				<a href="{{url('customer/newslettun')}}" >unsubscribe</a>
				@else 
					<a href="{{url('customer/newslett')}}" >subscribe</a>
				@endif
				<!--<input class="hidden" type="text" value="unsubscribe" id="sub_unsubs">-->
            </div>			
	  </div>

</section> 


@include('webshop.inner.inner_footer')

