<style type="text/css">
	.error{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.noorder{color:#000;}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES</a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>																				
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-9 col-md-offset-1">		
			<div class="address_sec">
				<h2>My Orders</h2>
				
            <div class="table-responsive">
				@if(isset($orders[0]))
				<table class="table table-border">
					  <thead>
					  	    <tr>
					  	     <th>Order Number</th>
							 <th>Order Status</th>
					  	     <th>Shipping Method</th>
					  	     <th>Sub Total</th>
					  	     <th>Shipping Cost</th>
					  	     <th>Total (Excl. Tax)</th>
					  	     <th>Tax</th>
					  	     <th>Total Amount</th>
					  	     <th>Date</th>
					  	     <th>Action</th>
							 <th>Track Order</th>
					  	    </tr>
					  </thead>
					  <tbody>
					  	@foreach($orders as $row)
					  	     <tr>
					  	     	<td>{{$row->order_number}}</td>
								<td>{{ucwords($row->type)}}</td>
					  	     	<td>{{$row->shipping_method}} - {{$row->shipping_delay}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format($row->total,2)}}</td>
								<td>{{Config::get('view.currency')}}{{number_format($row->shipping_cost,2)}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format(($row->shipping_cost+$row->total),2)}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format(($row->tax),2)}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format($row->total_cost,2)}}</td>
					  	     	<td>{{date('d/m/Y',strtotime($row->created_at))}}</td>
					  	     	<td><a href="{{url('customer/orderdetail',$row->id)}}" style="color: #000;">Detail</a></td>
								<td><a href="{{url('webshop/trackorder')}}?ordernumber={{str_replace('#','%23',$row->order_number)}}" style="color: #000;">Track</a></td>
					  	     </tr>
					  	@endforeach     
					  </tbody>
				</table>
				@else
					<h3 class="noorder">You did not made any order.</h3>
			@endif
				</div>
			
			</div>
           		
	  </div>

</section> 


@include('webshop.inner.inner_footer')
