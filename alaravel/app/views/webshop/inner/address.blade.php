<style type="text/css">
	.errorsss{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.js" type="text/javascript"></script>
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES  </a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-10 col-md-offset-1">
			<div id="address_sec">
			  <div class="qws">
				<h2>Delivery Addresses</h2>
				<p>Store all the delivery addresses you want (your home, work, family, etc.) to avoid
				having to enter the delivery address each time you place an order.</p>
			  </div>
			<div id="dynamic_field">

			@foreach ($shipping as $ship)
		    <div class="col-md-4 col-sm-4">
				<div class="empt_sec">
					<h2>{{$ship->alias}}</h2>
					<p>{{$ship->name}} {{$ship->surname}}</p>
					<p>{{$ship->floor_door}} {{$ship->nr}}</p>
					<p>{{$ship->address}}</p>
					<p>{{$ship->city}}</p>
					<p>{{$ship->postalcode}}</p>
					<p>{{$ship->telephone}}</p>
					<p>{{$ship->country}}</p>
					<ul>
					   <li><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal{{$ship->id}}"><span><i class="fa fa-pencil" aria-hidden="true"></i></span>Change</a></li>
					   <li><a class="remove_sh_add" href="javascript:void(0);" shipping-add-id="{{$ship->id}}"><span><i class="fa fa-trash" aria-hidden="true"></i></span>Delete</a></li>
					</ul>
				</div>
            </div>
			<div class="modal fade" id="myModal{{$ship->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					      <div class="modal-content">
						  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						  </div>
					      <div class="modal-body">
					        <section class="contact-form contact_sec">
						    <div class="log_in df">
										<form id="myfrm{{$ship->id}}" method="POST" action="#" class="addressformed">
											<input type="hidden" name="id" value="@if(isset($ship->id)){{$ship->id}} @endif">
											<div class="col-md-6 col-sm-6">
												<div class="group">
													<select class="form-control" name="country" value="" required>
														<option value="">Country</option>
														@foreach ($allcountrues as $ct)
														<option value="{{$ct}}"  @if ($ct === $ship->country) selected @endif >{{$ct}}</option>
														@endforeach
													</select>
												</div>
											</div>
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_name" class="form_input @if(isset($ship->name))used @endif" type="text" name="name" value="@if(isset($ship->name)){{$ship->name}}@endif" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errorname errserver"></h3>
													<label class="labletxt">Name*</label>
				                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_surname" class="form_input @if(isset($ship->surname)) used @endif" type="text" name="surname" value="@if(isset($ship->surname)){{$ship->surname}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorsurname errserver"></h3>
														<label class="labletxt">Surname(s)*</label>
														<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
					                        <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_address" class="form_input @if(isset($ship->address))used @endif" type="text" name="address" value="@if(isset($ship->address)){{$ship->address}}@endif" required><span class="highlight"></span><span class="bar" ></span>
														<h3 for="" class="erroraddress errserver"></h3>
														<label class="labletxt">Address*</label>
					                           			<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_nr" class="form_input @if(isset($ship->nr)) used @endif" name="nr" minlength="2" type="text"  value="@if(isset($ship->nr)){{$ship->nr}}@endif" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errornr errserver"></h3>
													<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
												</div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_floor_door" class="form_input @if(isset($ship->floor_door)) used @endif" type="text" name="floor_door" value="@if(isset($ship->floor_door)){{$ship->floor_door}}@endif" ><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorfloor_door errserver"></h3>
														<label class="labletxt">Floor/door/etc.</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_postalcode" class="form_input @if(isset($ship->postalcode))used @endif" type="text" name="postalcode" value="@if(isset($ship->postalcode)){{$ship->postalcode}}@endif" required><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorpostalcode errserver"></h3>
														<label class="labletxt">Postcode*</label>
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_city" class="form_input @if(isset($ship->city))used @endif" type="text" name="city" value="@if(isset($ship->city)){{$ship->city}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorcity errserver"></h3>
														<label class="labletxt">City*</label>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_telephone" class="form_input @if(isset($ship->telephone)) used @endif" type="text" name="telephone" value="@if(isset($ship->telephone)){{$ship->telephone}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errortelephone errserver"></h3>
														<label class="labletxt">Telephone*</label>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_alias" class="form_input @if(isset($ship->alias)) used @endif" type="text" name="alias" value="@if(isset($ship->alias)){{$ship->alias}}@endif" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="erroralias errserver"></h3>
														<label class="labletxt">Alias*</label>
											   </div>
											</div>

										    <div class="col-md-6 col-sm-6">
												<div class="submit_sec fgd">
												    <input class="buttons" type="submit" value="ACCEPT">
											    </div>
											</div>

										</form>
					        </div>
					        </section>
					      </div>
					      </div>
					  </div>
					</div>
			 <script>
jQuery("#myfrm{{$ship->id}}").validate({
	submitHandler: function(form,event) {
		event.preventDefault();
		$("#myfrm{{$ship->id}} #adds_name").keyup(function(){$("#myfrm{{$ship->id}} .errorname").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_surname").keyup(function(){$("#myfrm{{$ship->id}} .errorsurname").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_address").keyup(function(){$("#myfrm{{$ship->id}} .erroraddress").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_city").keyup(function(){$("#myfrm{{$ship->id}} .errorcity").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_postalcode").keyup(function(){$("#myfrm{{$ship->id}} .errorpostalcode").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_telephone").keyup(function(){$("#myfrm{{$ship->id}} .errortelephone").fadeOut();});
		$("#myfrm{{$ship->id}} #adds_alias").keyup(function(){$("#myfrm{{$ship->id}} .erroralias").fadeOut();});
		var data = $("#myfrm{{$ship->id}}").serializeArray();
		$.ajax({
			data:data,
			url:base_url+'customer/myaddress',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.name){
						$("#myfrm{{$ship->id}} .errorname").html(res.errors.name);
						$("#myfrm{{$ship->id}} .errorname").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorename").html('');
					}
					if(res.errors.surname){
						$("#myfrm{{$ship->id}} .errorsurname").html(res.errors.surname);
						$("#myfrm{{$ship->id}} .errorsurname").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorsurname").html('');
					}
					if(res.errors.address){
						$("#myfrm{{$ship->id}} .erroraddress").html(res.errors.address);
						$("#myfrm{{$ship->id}} .erroraddress").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .erroraddress").html('');
					}
					if(res.errors.postalcode){
						$("#myfrm{{$ship->id}} .errorpostalcode").html(res.errors.postalcode);
						$("#myfrm{{$ship->id}} .errorpostalcode").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorpostalcode").html('');
					}
					if(res.errors.city){
						$("#myfrm{{$ship->id}} .errorcity").html(res.errors.city);
						$("#myfrm{{$ship->id}} .errorcity").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errorcity").html('');
					}
					if(res.errors.telephone){
						$("#myfrm{{$ship->id}} .errortelephone").html(res.errors.telephone);
						$("#myfrm{{$ship->id}} .errortelephone").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .errortelephone").html('');
					}
					if(res.errors.alias){
						$("#myfrm{{$ship->id}} .erroralias").html(res.errors.alias);
						$("#myfrm{{$ship->id}} .erroralias").fadeIn('slow');
					}
					else{
						$("#myfrm{{$ship->id}} .erroralias").html('');
					}
				}
				else{
					location.reload();
				}
			}
		});
    }
});

</script>
			 @endforeach

			</div>
			<div class="col-md-12 col-sm-12">
			<div class="empty_cards">
				<a href="#" data-toggle="modal" data-target="#myModaladdsh">Add address</a>
			</div>
			<div class="modal fade" id="myModaladdsh" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					      <div class="modal-content">
						  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						  </div>
					      <div class="modal-body">
					        <section class="contact-form contact_sec">
						    <div class="log_in df">
										<form method="POST" action="#" id="addressform_add">
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<select class="form-control" name="country" value="" required>
														<option value="">Country</option>
														@foreach ($allcountrues as $ct)
														<option value="{{$ct}}">{{$ct}}</option>
														@endforeach
													</select>
												</div>
											</div>
											 <div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_name" class="form_input" type="text" name="name" required><span class="highlight" ></span><span class="bar"></span>
													<h3 for="" class="errorname errserver"></h3>
													<label class="labletxt">Name*</label>
				                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_surname" class="form_input" type="text" name="surname" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorsurname errserver"></h3>
														<label class="labletxt">Surname(s)*</label>
														<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
					                        <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_address" class="form_input" type="text" name="address" required><span class="highlight"></span><span class="bar" ></span>
														<h3 for="" class="erroraddress errserver"></h3>
														<label class="labletxt">Address*</label>
					                           			<div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
													<input id="adds_nr" class="form_input" name="nr" minlength="2" type="text" ><span class="highlight"></span><span class="bar"></span>
													<h3 for="" class="errornr errserver"></h3>
													<label class="labletxt" style="text-align: left;font-size: 13px;">NR</label>
												</div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_floor_door" class="form_input" type="text" name="floor_door"><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorfloor_door errserver"></h3>
														<label class="labletxt">Floor/door/etc.</label>
					                                    <div class="md-errors-spacer"><div class="md-char-counter"></div></div>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_postalcode" class="form_input" type="text" name="postalcode" required><span class="highlight" ></span><span class="bar"></span>
														<h3 for="" class="errorpostalcode errserver"></h3>
														<label class="labletxt">Postcode*</label>
											   </div>
											</div>
										    <div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_city" class="form_input" type="text" name="city" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errorcity errserver"></h3>
														<label class="labletxt">City*</label>
											   </div>
											</div>
											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_telephone" class="form_input" type="text" name="telephone" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="errortelephone errserver"></h3>
														<label class="labletxt">Telephone*</label>
											   </div>
											</div>

											<div class="col-md-6 col-sm-6">
												<div class="group">
														<input id="adds_alias" class="form_input" type="text" name="alias" required><span class="highlight"></span><span class="bar"></span>
														<h3 for="" class="erroralias errserver"></h3>
														<label class="labletxt">Alias*</label>
											   </div>
											</div>

										    <div class="col-md-6 col-sm-6">
												<div class="submit_sec fgd">
												    <input class="buttons" type="submit" value="ACCEPT">
											    </div>
											</div>

										</form>
					        </div>
					        </section>
					      </div>
					      </div>
					  </div>
			</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="billing_main">
					<a href="{{url('customer/information')}}">See my billing address </a>
				</div>
            </div>
			</div>



	  </div>

</section>


@include('webshop.inner.inner_footer')




<script>
var base_url = '{{url('/')}}/';
$(document).ready(function(){
      var i=1;
      $('#add').click(function(){
        // alert();
		i++;
          $('#dynamic_field').append('<div class="empty_card" id="row'+i+'" style="margin-left: 26px;"><a href="#" data-toggle="modal" data-target="#myModal34"><i class="fa fa-edit"></i> Change</a><a type="button" name="remove" id="'+i+'" class="btn_remove"><i class="fa fa-trash"></i> Delete</button></a>');
      });
      $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
     });

	jQuery("#addressform_add").validate({
		submitHandler: function(form, event) {
			event.preventDefault();
			$("#addressform_add #adds_name").keyup(function(){$("#addressform_add .errorname").fadeOut();});
			$("#addressform_add #adds_surname").keyup(function(){$("#addressform_add .errorsurname").fadeOut();});
			$("#addressform_add #adds_address").keyup(function(){$("#addressform_add .erroraddress").fadeOut();});
			$("#addressform_add #adds_city").keyup(function(){$("#addressform_add .errorcity").fadeOut();});
			$("#addressform_add #adds_postalcode").keyup(function(){$("#addressform_add .errorpostalcode").fadeOut();});
			$("#addressform_add #adds_telephone").keyup(function(){$("#addressform_add .errortelephone").fadeOut();});
			$("#addressform_add #adds_alias").keyup(function(){$("#addressform_add .erroralias").fadeOut();});
			var data = $('#addressform_add').serializeArray();
			$.ajax({
				data:data,
				url:base_url+'customer/shippingadd',
				type:"POST",
				cache:false,
				success: function(response){
					var res = JSON.parse(response);
					console.log(res);
					if(res.errors){
						if(res.errors.name){
							$("#addressform_add .errorname").html(res.errors.name);
							$("#addressform_add .errorname").fadeIn('slow');
						}
						else{
							$("#addressform_add .errorename").html('');
						}
						if(res.errors.surname){
							$("#addressform_add .errorsurname").html(res.errors.surname);
							$("#addressform_add .errorsurname").fadeIn('slow');
						}
						else{
							$("#addressform_add .errorsurname").html('');
						}
						if(res.errors.address){
							$("#addressform_add .erroraddress").html(res.errors.address);
							$("#addressform_add .erroraddress").fadeIn('slow');
						}
						else{
							$("#addressform_add .erroraddress").html('');
						}
						if(res.errors.postalcode){
							$("#addressform_add .errorpostalcode").html(res.errors.postalcode);
							$("#addressform_add .errorpostalcode").fadeIn('slow');
						}
						else{
							$("#addressform_add .errorpostalcode").html('');
						}
						if(res.errors.city){
							$("#addressform_add .errorcity").html(res.errors.city);
							$("#addressform_add .errorcity").fadeIn('slow');
						}
						else{
							$("#addressform_add .errorcity").html('');
						}
						if(res.errors.telephone){
							$("#addressform_add .errortelephone").html(res.errors.telephone);
							$("#addressform_add .errortelephone").fadeIn('slow');
						}
						else{
							$("#addressform_add .errortelephone").html('');
						}
						if(res.errors.alias){
							$("#addressform_add .erroralias").html(res.errors.alias);
							$("#addressform_add .erroralias").fadeIn('slow');
						}
						else{
							$("#addressform_add .erroralias").html('');
						}
					}
					else{
						location.reload();
					}
				}
			});
        }
	});
	$('.remove_sh_add').on('click', function(e){
		if(confirm("Are you sure want to delete this shipping address. ?")){
			$.ajax({
				data:{'id':$(this).attr('shipping-add-id')},
				url:base_url+'customer/shippingremove',
				type:"POST",
				cache:false,
				success: function(response){
					location.reload();
				}
			});
		}
	});
});
</script>
