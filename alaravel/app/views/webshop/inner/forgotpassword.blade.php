@include('webshop.header')
<section class="contact-form contact_sec">
	<div class="log_in">
		<div class="container">		  
		  <div class="col-md-8 col-sm-8">
			  <div class="col-md-7 col-sm-7">
					<h4 style="margin-bottom:15px;"> RETRIEVE PASSWORD</h4>
					<p>Please enter your email address and we will send you an email with the steps to follow.</p>
					    <div class="alert alert-success" id="forgot" style="display: none;">
                        </div>
					
						<div class="alert alert-danger error-forgot" style="display: none;">
                        </div>	
					<form>
					   
					    <div class="group">											                  
								<input class="form_input" type="text" name="email" id="emailforgot"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="errorf" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Email*</label>   
					   </div>
					   
			
						<div class="submit_sec">
							<input class="buttons" type="submit" value="SEND" id="btn-forgot">
						</div>
					</form>
					
			  </div>
			   
			 
		  </div>		
		</div>
    </div>
</section>
<style type="text/css">
 .submit_sec .buttons {
    display: block;
    width: 100%;
    max-width: 300px;
    padding: 12px 25px;
    font-size: 11px;
    color: #000;
    border: 1px solid #000;
}

.submit_sec .buttons:hover {
    background: #000;
    color: #fff !important;
}
</style>
@include('webshop.nomenu_footer') 
<script type="text/javascript">
 $('#emailforgot').keyup(function() {
        if($(this).val()==''){
          
        $('.errorf').html('This field is required');
         $(".errorf").fadeIn('slow', function(){
	        $(".errorf").delay(3000).fadeOut(); 
	      });
        }
        else{
            //alert('unchecked');
        $('.errorf').html('');
        }
    });


	/* for  */
$('#btn-forgot').on('click', function(e) {

 // $("#add-form-news").valid();
   e.preventDefault();
   var emailforgot = $('#emailforgot').val();

  // console.log(email_forgot);
   
      if(emailforgot==''){
            $('.errorf').html('This field is required');
            $(".errorf").fadeIn('slow', function(){
	        $(".errorf").delay(3000).fadeOut(); 
	      });
      	     return false;
      }
 
 $.ajax({

	url:"{{url('/forgotpassword/forgotpassword')}}",
    type:"POST",
	cache:false,
	data:{
      email:emailforgot,
    },
	success: function(response){
		var res = JSON.parse(response);
		console.log(res.ForgotError);
		if(res.ForgotError)
		{
		console.log(res.ForgotError);	
		$(".error-forgot").show();
		$(".error-forgot").html(res.ForgotError);
		$(".error-forgot").fadeIn('slow', function(){
        $(".error-forgot").delay(6000).fadeOut(); 
        $("#success-forgot").hide();
      });
		//$(".password").html(response.errors.password);
		// do what you want with errors, 
		}
		else
		{
		$(".error-forgot").hide();	
		$('#forgot').show();
		$('#forgot').html(res.ForgotSuccess);
		$("#forgot").fadeIn('slow', function(){
        $("#forgot").delay(6000).fadeOut(); 
        });
        $('#emailforgot').val('');
		//location.reload();

							
	    }
	}
  });

});
</script>