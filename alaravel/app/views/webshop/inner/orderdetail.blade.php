<style type="text/css">
	.error{
		text-align:center;
		 color:red;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
	.success{
		text-align:center;
		 color:green;
		 position:relative !important;
		 width:100% !important;
		 background:#f9f9f9;
		 padding:20px;
	}
</style>
@include('webshop.inner.inner_header')

 @if(Session::has('message-failure'))
<section style="font-size:18px;">
<p class="error">{{Session::get('message-failure')}}</p>
</section>
@endif
 @if(Session::has('message-success'))
<section style="font-size:18px;">
<p class="success">{{Session::get('message-success')}}</p>
</section>
@endif
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		<ul class="wish_nav">
					<li><a href="{{url('customer/information')}}">CUSTOMER INFORMATION </a></li>
					<li><a href="{{url('customer/address')}}">MY ADDRESSES </a></li>
					<li><a href="{{url('customer/order')}}">ORDERS </a></li>
					<li><a href="{{url('customer/cards')}}">CARDS </a></li>
					<li><a href="{{url('customer/notification')}}">Notifications </a></li>
					<li><a href="{{url('customer/wishlist')}}">WISHLISTS </a></li>																				
		</ul>
	  </div>

	  <div class="col-md-9 col-sm-9 col-md-offset-1">		
			<div class="address_sec">
				<h2>Order Detail</h2>

				<table class="table table-border">
					           
					  	<tbody>					  		
					  	    <tr> 
					  	     <th>Order Number</th>
					  	     <td>{{$order->order_number}}</td>
					  	    </tr>
							<tr> 
					  	     <th>Order Status</th>
					  	     <td>{{ucwords($order->type)}}</td>
					  	    </tr>
							<tr> 
					  	     <th>Billing Name</th>
					  	     <td>{{$order->name}} {{$order->surname}}</td>
					  	    </tr>
							<tr> 
					  	     <th>Shipping Name</th>
					  	     <td>{{$order->shipping_name}} {{$order->shipping_surname}}</td>
					  	    </tr>
							@if($order->comp_name !="")
							<tr> 
								<th>Company Name</th>
								<td>{{$order->comp_name}}</td>
					  	    </tr>
							@endif
							<tr> 
					  	     <th>Email</th>
					  	     <td>{{$order->email}}</td>
					  	    </tr>
							<tr>
								<th>Billing Address</th>
								<td>{{$order->billing_address}}@if($order->nif!=""),@endif {{$order->nif}}@if($order->billing_floor!=""),@endif {{$order->billing_floor}}@if($order->billing_city!=""),@endif {{$order->billing_city}}@if($order->billing_postalcode!=""),@endif {{$order->billing_postalcode}}@if($order->country!=""),@endif {{$order->country}}</td>
					  	    </tr>
							
							<tr>
								<th>Shipping Address</th>
								<td>{{$order->shipping_address}}@if($order->shipping_nr!=""),@endif {{$order->shipping_nr}}@if($order->shipping_floor!=""),@endif {{$order->shipping_floor}}@if($order->shipping_city!=""),@endif {{$order->shipping_city}}@if($order->shipping_postalcode!=""),@endif {{$order->shipping_postalcode}}@if($order->country!=""),@endif {{$order->country}}</td>
					  	    </tr>
					  	    <tr>
					  	     <th>Shipping Method</th>
					  	     <td>{{$order->shipping_method}} - {{$order->shipping_delay}}</td>
					  	    </tr>
					  	     <th>Sub Total</th>
					  	     <td>{{Config::get('view.currency')}}{{number_format(($order->total),2)}}</td>
							 <tr>
					  	     <th>Shipping Cost</th>
					  	     <td>{{Config::get('view.currency')}}{{number_format($order->shipping_cost,2)}}</td>
					  	     </tr>
							 <tr>
					  	     <th>Total &nbsp;<span class="reference">(Excl Tax)</span></th>
					  	     <td>{{Config::get('view.currency')}}{{number_format(($order->shipping_cost + $order->total),2)}}</td>
					  	     </tr>
					  	     <tr>
					  	     <th>TAX</th>
					  	     <td>{{Config::get('view.currency')}}{{number_format($order->tax,2)}}</td>
					  	     </tr>					  	    
					  	    <tr>
					  	     <th>Total Amount</th>
					  	     <td>{{Config::get('view.currency')}}{{number_format($order->total_cost,2)}}</td>
					  	    </tr>
					  	    <tr>
					  	     <th>Transaction Id</th>
					  	     <td>{{$order->transactionid}}</td>
					  	    </tr>
							<tr>
					  	     <th>Payment Method</th>
					  	     <td>{{$order->payment_method}}</td>
					  	    </tr>
							<tr>
								<th>Order Date</th>
								<td>{{date('d/m/Y',strtotime($order->created_at))}}</td>
					  	    </tr>
							
							
							
							
					  	    
					  
					  	    
					  </tbody>
				</table>

				<table class="table table-border">
					  <thead>
					  	    <tr>
					  	     <th>Name</th>
					  	     <th>Image</th>
					  	     <th>Attributes</th>
					  	     <th>Price</th>
					  	     <th>Quantity</th>
					  	     <th>Excl. TAX</th>
					  	     <th>TAX</th>
					  	     <th>Incl. TAX</th>
					  	    </tr>
					  </thead>
					  <tbody>
					  	@foreach($products as $row)
					  	     <tr>
					  	     	<td>{{$row->name}}</td>
					  	     	<td><img src="{{url('public/webshop/bigproducts')}}/{{$row->img}}" style="width: 60px;"></td>
					  	     	<td>{{$row->attributes}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format($row->price,2)}}</td>
					  	     	<td>{{$row->quantity}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format(($row->quantity * $row->price),2)}}</td>
					  	     	<?php 

								$totalPrice = $row->quantity * $row->price;
								$tax        = $row->taxrate;
								$finalTotal = ($totalPrice * $tax)/100;

							    ?>
					  	     	<td>{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}</td>
					  	     	<td>{{Config::get('view.currency')}}{{number_format(($finalTotal + $totalPrice),2)}}</td>
					  	     </tr>
					  	@endforeach     
					  </tbody>
				</table>
							
			</div>
           		
	  </div>

</section> 


@include('webshop.inner.inner_footer')
