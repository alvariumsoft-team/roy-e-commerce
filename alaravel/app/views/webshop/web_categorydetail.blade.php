@include('webshop.header')
<style type="text/css">
	.size .basket_btn {
    color: #000;
    border: 1px solid #000;
    width: 95%;
    display: inline-block;
    text-align: center;
    padding: 13px 0;
    text-transform: uppercase;
    font-size: 11px;
    text-decoration: none;
}

.view-basket{
    border: 1px solid #000;
    width: 100%;
    display: inline-block;
    text-align: center;
    padding: 13px 0;
    text-transform: uppercase;
    font-size: 11px;
    text-decoration: none;
}
</style>
<section class="contact-form zx">
	<div class="product">
	  <div class="container-fluid">
		<div class="col-md-2 col-sm-2">
			 @include('webshop.left_panel')
		</div>
		<?php 
           $url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
           $pieces = explode("/", $url );
           $count= count($pieces);
		?>
		<div class="col-md-6 col-sm-6">
			<div class="more_quilt">
				<p>
					@if(isset($morecat->name))
						<a href="{{url('webshop/category')}}/{{$morecat->url}}"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>MORE {{$morecat->name}}</a>
					@endif
				</p>
			</div>
			<div class="full_img">
				<div class="carousel slide" id="carousel-example-generic" data-ride="carousel" data-interval="false">
					<ol class="carousel-indicators">
					@foreach($product_images as $k=>$pm)
						<li data-target="#carousel-example-generic" data-slide-to="{{$k}}" class="@if($k==0)active @endif"></li>
					@endforeach
					</ol>
					<div class="carousel-inner" role="listbox">
						@foreach($product_images as $j=>$pm)
						<div class="item @if($j==0)active @endif"> <img src="{{url('public/webshop/bigproducts')}}/{{$pm->img_name}}" class="img-responsive" alt="{{$pm->img_name}}"> </div>
						@endforeach
					</div>
					<a href="#carousel-example-generic" class="left carousel-control" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span> </a>
					<a href="#carousel-example-generic" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> 
				</div>
			</div>
			
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="next_info">
				@if(isset($prevpr->id))
					<a href="{{url('/webshop/product')}}/{{$prevpr->url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
				@endif

				@if(isset($nextpr->id))
				<a href="{{url('/webshop/product')}}/{{$nextpr->url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
				@endif
			</div>
			<div class="product_info">		
				<h2>{{ $product['name']}}</h2>
				@if($product['type'] == 'variable')
				<h5 id="regular_price">{{Config::get('view.currency')}}{{number_format($product['price_min'],2)}} - {{Config::get('view.currency')}}{{number_format($product['price_max'],2)}}</h5>
				<!-- <p id="sale_price" style="text-decoration: line-through;margin: 8px 0 8px;color:red;"><b><?php // echo $product['price_min_sale'];?>€</b> - <b><?php // echo $product['price_max_sale'];?>€</b> </p> -->
				@else
				@if(isset($min_pro_price->retailprice))
					<h5 id="prprc">{{Config::get('view.currency')}}{{number_format($min_pro_price->retailprice,2)}}  -  {{Config::get('view.currency')}}{{number_format($max_pro_price->retailprice,2)}}</h5>
				@else
					<h5>{{Config::get('view.currency')}}{{number_format($product['retailprice'],2)}}</h5>
				@endif
				@endif
				<!-- <h5> £89.99 - £199.99</h5> -->
				<p id="prref">ref. {{ $product['sku']}} </p>
				<form class="size form cartform">
					<input type="hidden" name="product" id="product" value="{{$product['id']}}" />
					<input type="hidden" name="product_variation_id" id="prvariation_id" value="" />
					<input type="hidden" name="product_stock" id="prstock_id" value="{{$product['quantity']}}"/>
					<ul class="new_quant">
						@if($prattributes==1)
							@foreach($att_grp_details as $grpdtl)
								<li>							
									<div class="form-group">
										<select class="form-control required pratts" name="att_{{$grpdtl->name}}" required>
													<option value="">Choose a {{$grpdtl->name}}</option>
													@foreach($att_details as $grpdetail)
													@if($grpdetail->attribute_group_id==$grpdtl->id)											<option value="{{$grpdetail->sup_attribute_id}}">{{$grpdetail->name}}</option>
													@endif
													@endforeach
													
												
										</select>
									</div>
									
								</li>
							@endforeach
							
						@endif
						@if(count($att_grp_details)==1)
						<li>
						@else
							<li class="quant_ful">
						@endif
							<div class="form-group">
								<label>QUANTITY<span>*</span></label>
								
								<select class="form-control" name="quantity" id="prquantity">
									<!-- <option value="">Quantity</option> -->
									@if(isset($product['quantity']))
									@if($product['quantity'] > 10)
										@for($i=1;$i<=10;$i++)
											<option value="{{$i}}">{{$i}}</option>
										@endfor
									@else
										@for($i=1;$i<=$product['quantity'];$i++)
											<option value="{{$i}}">{{$i}}</option>
										@endfor
									@endif
									@endif
								</select>
							</div>
						</li>
					</ul>
					<div class="sg">
						<input id="addcartfrm" type="submit" class="basket_btn" value="add to basket">
					</div>
					
				</form>
				<div class="info_plus">
				    <div class="col-md-6 col-sm-6 qd">
					<a href="#" data-toggle="modal" data-target="#myModal66"><span>+</span>info</a>
					</div>
					<div class="col-md-6 col-sm-6 az">
					<a href="#"><span class="heart_as"><i class="fa fa-map-marker" aria-hidden="true"></i></span></a>
					@if(Auth::check())
					<a href="#" data-toggle="modal" id="heart_wish">
						<span class="heart_as"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
					</a>
					@endif
					</div>							
				</div>
			
			<div class="modal fade" id="myModal67" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
					  <h4 class="modal-title" id="myModalLabel"></h4> </div>
					  <div class="modal-body">
						<section class="contact-form contact_sec">
							<div class="log_in df">		  		  
								  <div class="col-md-12 col-sm-12">
										<div class="wish_vb">
											<h2>select a list</h2>
											<ul id="wish_itms">
											@foreach($wishlists as $wish)
											<li><a href="#"><span class="wish_nmb">{{$wish->list_name}}</span><span class="item-lenght">({{$wish->count}})</span><span class="wish_heartselected">
											<i class="fa fa-heart-o" aria-hidden="true"></i></span></a></li>
											@endforeach	
											</ul>
											<div class="create_wish">
											  <a class="#" id="ctr_wish">create wishlist</a>
											</div>
											<form id="wish_add_list">
												<div class="crt_prt group hidesh">							  <input class="form_input" type="text" name="list_name" id="wish_add"><span class="highlight"></span><span class="bar"></span>
												<h3 for="" class="errorname errserver"></h3>
												<label class="labletxt">Wish List Name*</label>
												<div class="md-errors-spacer">
													<div class="md-char-counter"></div>
												</div>			
												</div>				  
											   <div class="crt_prt wish_bt hidesh">
												  <a class="#" id="new_wish">accept</a>
											   </div>
										   </form>
										</div> 													
								  </div>							
							</div>
						</section> 
					  </div>    
					</div>
				  </div>
			</div>
			<div class="modal fade" id="myModal66" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
				  </div>
				  <div class="modal-body">
				   <div class="ship-tab">   
						<div class="col-md-12 col-sm-12">
								<div id="exTab1">	
										<ul class="nav nav-pills">
											<li class="active">
												<a  href="#1a" data-toggle="tab">DESCRIPTION</a>
											</li>
											<li>
												<a href="#2a" data-toggle="tab">DELIVERY / RETURNS</a>
											</li>						
										</ul>
										<div class="tab-content clearfix">
										  <div class="tab-pane active" id="1a">
												<p class="compo-header">DESCRIPTION</p>
												<div class="bundle-compo">
													<p>{{ $product['description']}}</p>
												</div>
												
										  </div>
											<div class="tab-pane" id="2a">
												  <p class="compo-header">DELIVERY</p>
												  <div class="del-info">
														<h5>To which countries can we send orders?</h5>
													<p>Global Property Cowboys is automated and ships to almost every country in the world with express courier. However, shipments to destinations outside the EU may have certain restrictions or special procedures for the management of customs formalities.</p>
													<h5>What are the shopping costs and delivery time?</h5>
													<p>You can estimate the shipping costs and delivery time in the BigBuy shopping cart yourself. An estimate is 1-4 days in the EU and 4-8 days outside the EU.</p>
													<h5>What is included in the shipping charges?</h5>
													<p>The shipping charges include:<br>
													•	Suitable packaging (cardboard boxes, bubble wrap, inner protection, etc.) <br>	•	Transportation <br>•	Insurance <br>•	Customs clearance at origin</p>
											  </div>
											  <p class="compo-header">RETURNS</p>
											  <div class="del-info">
													<h5>You have a month to return any item purchased from us. Returns to store:</h5>
													<p>You may return items to Global Property Cowboys  to the following address</p>
													<h5>Returns through the return centre:</h5>
													<p>First send an email with the order number to the following email address :</p> 
													<p><a href="mailto:returns@globalpropertycowboys.nl">returns@globalpropertycowboys.nl</a><br></p>
													<p>Package the products and attach the printed electronic receipt.</p>
													<p>Send the return at 8€ in the EU and 18€ outside the EU plus your own cost to the following address:<br></p>
													<p><br>Global Property Cowboys,</p>
													<p>schalkwijkpad 29</p>
													<p>1107JL Amsterdam</p>
													<p>Netherlands</p>
													<div class="last_info">
													<a target="_blank" href="{{url('webshop/shop-guide#returns')}}">More info about our returns policy.</a>
													</div>
											  </div>
										  </div>
									 </div>
							    </div>
					 	    </div>	  
					    </div> 
				      </div>    
				  </div>
			   </div>
				<a href="JavaScript:void(0);" class="slide-info"><i class="fa fa-map-marker" aria-hidden="true"></i></a>				
			</div>
			<h4></h4>
			
			<div class="swiper" id="feedback">				
					<div class="flask_cntr">
					<h3>Our selection</h3>
						<div class="owl-carousel" id="testi_slide">
						@if(isset($ourselection))
							@foreach($ourselection as $row)
								<div class="sec_main">									
								   <a href="{{url('/webshop/product')}}/{{$row->url}}"> <img src="{{url('public/webshop/bigproducts')}}/{{$row->images[0]}}" class="img-responsive" alt="{{$row->name}}">
								   </a>
									<p><a href="{{url('/webshop/product')}}/{{$row->url}}" style="color: #000;">{{$row->name}}</a>
									</p>
									@if(isset($row->min_pro_price['retailprice']))
										<p id="os{{$row->id}}prc"> {{Config::get('view.currency')}}{{number_format($row->min_pro_price['retailprice'],2)}}  -  {{Config::get('view.currency')}}{{number_format($row->max_pro_price['retailprice'],2)}}</p>
									@else
										<p id="os{{$row->id}}prc"> {{Config::get('view.currency')}}{{number_format($row->retailprice,2)}}</p>
									@endif
									<!-- <form class="size_slider"> -->
									<form id="osprd{{$row->id}}" class="size_slider size form ourSelectioncartform" prd-id="{{$row->id}}">
										<input type="hidden" name="product" id="product_our{{$row->id}}" value="{{$row->id}}" />
										<input type="hidden" name="product_variation_id" id="pr{{$row->id}}variation_id" value="" />
										<input type="hidden" name="product_stock" id="pr{{$row->id}}stock_id" value="{{$row->quantity}}"/>
										<ul class="new_quant_slide">
											@if(isset($row->att_grp_details[0]))
												@foreach($row->att_grp_details as $ats)
												<li>
													<div class="form-group">
														<select product-ids="{{$row->id}}" class="form-control osatts osat{{$row->id}}">
																	<option value="">{{$ats->name}}</option>
																	@foreach($row->att_details as $grpdetail)
												@if($grpdetail->attribute_group_id==$ats->id)											<option value="{{$grpdetail->sup_attribute_id}}">{{$grpdetail->name}}</option>
													@endif
													@endforeach
														</select>
													</div>
												</li>
												@endforeach
												@if(count($row->att_grp_details)==2)
													<li class="quant_full_sec">
												<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="os{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												@else
													
													<li class="xzd">
													<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="os{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												<li class="quant_full_sec">
													<div class="form-group">
														<select style="visibility:hidden" class="form-control" name="">	
															
														</select>
													</div>
												</li>
												@endif
													
											@else
												<li class="quant_full_sec">
													<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="os{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												<li class="quant_full_sec">
													<div class="form-group">
														<select style="visibility:hidden" class="form-control" name="">	
															
														</select>
													</div>
												</li>
											@endif
										</ul>
										<div class="">
											<input type="submit" class="basket_btn" value="add to basket">
											<!-- <a href="#">add to basket</a> -->
										</div>
									</form>
								</div>
						@endforeach
						@endif  
					</div>
				</div>				
			</div>
			
        
			<div class="swiper" id="feedback">				
					<div class="flask_cntr">
					<h3>YOU MIGHT BE INTERESTED</h3>
						<div class="owl-carousel" id="testi_slide_1">
						@if(isset($mightinterested))
							@foreach($mightinterested as $row)
								<div class="sec_main">
									   <a href="{{url('/webshop/product')}}/{{$row->url}}"> <img src="{{url('public/webshop/bigproducts')}}/{{$row->images[0]}}" class="img-responsive" alt="{{$row->name}}"></a>
									<p><a href="{{url('/webshop/product')}}/{{$row->url}}" style="color: #000;">{{$row->name}}</a></p>								
									@if(isset($row->min_pro_price['retailprice']))
										<p id="mi{{$row->id}}prc"> {{Config::get('view.currency')}}{{number_format($row->min_pro_price['retailprice'],2)}}  -  {{Config::get('view.currency')}}{{number_format($row->max_pro_price['retailprice'],2)}}</p>
									@else
										<p id="mi{{$row->id}}prc"> {{Config::get('view.currency')}}{{number_format($row->retailprice,2)}}</p>
									@endif
									<!-- <form class="size_slider"> -->
									<form id="miprd{{$row->id}}" class="size_slider size form micartform" prd-id="{{$row->id}}">
										
										<input type="hidden" name="product" id="miproduct_our{{$row->id}}" value="{{$row->id}}" />
										<input type="hidden" name="product_variation_id" id="mipr{{$row->id}}variation_id" value="" />
										<input type="hidden" name="product_stock" id="mipr{{$row->id}}stock_id" value="{{$row->quantity}}"/>
										<ul class="new_quant_sec">
											@if(isset($row->att_grp_details[0]))
												@foreach($row->att_grp_details as $ats)
												<li>
													<div class="form-group">
														<select class="form-control miatts miat{{$row->id}}" product-ids="{{$row->id}}">
																	<option value="">{{$ats->name}}</option>
																	@foreach($row->att_details as $grpdetail)
												@if($grpdetail->attribute_group_id==$ats->id)											<option value="{{$grpdetail->sup_attribute_id}}">{{$grpdetail->name}}</option>
													@endif
													@endforeach
														</select>
													</div>
												</li>
												@endforeach
												@if(count($row->att_grp_details)==2)
													<li class="quant_full_sec">
												<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="mi{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												@else
													<li class="xzd">
													<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="mi{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												<li class="quant_full_sec">
													<div class="form-group">
														<select style="visibility:hidden" class="form-control" name="">	
															
														</select>
													</div>
												</li>
												@endif
													
											@else
												<li class="quant_full_sec">
													<div class="form-group">
														<label>QUANTITY<span>*</span></label>
														<select class="form-control" name="quantity" id="mi{{$row->id}}qty">	
															@if(isset($row->quantity))
															@if($row->quantity > 10)
																@for($i=1;$i<=10;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@else
																@for($i=1;$i<=$row->quantity;$i++)
																	<option value="{{$i}}">{{$i}}</option>
																@endfor
															@endif
															@endif
														</select>
													</div>
												</li>
												<li class="quant_full_sec">
													<div class="form-group">
														<select style="visibility:hidden" class="form-control" name="">	
															
														</select>
													</div>
												</li>
											@endif
										</ul>
										<div class="">
											<input type="submit" class="basket_btn" value="add to basket">
											<!-- <a href="#">add to basket</a> -->
										</div>
									</form>
								</div>
							@endforeach
							 @endif   
						</div>
					</div>				
			  </div>
		</div>
	</div>
</div>
</section>


<!-- info popup -->
<!-- info popup end-->
<style type="text/css">
	.infobox{
        float:right;
        overflow: hidden;
        display:none;
    }
</style>
@include('webshop.footer')
<script type="text/javascript">
	var base_url = '{{URL::to("/")}}';
</script>
<script src="{{url('assets/frontend/script/productDetail.js')}}"></script>
<script src="{{url('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	@if(isset($product_vars))
		var product_vars={{$product_vars}};
		console.log(product_vars);
		$(".pratts").on('change', function(){
			$("#addcartfrm").removeAttr('disabled');
			var allatt=1;
			var attids = [];
			$(".pratts").each(function() {
				if($(this).val() !=""){
					attids.push($(this).val());
				}
				else{
					allatt=0;
				}
			});
			if(allatt==1){
				if(attids.length > 1){
					var varin=attids.join();
				}
				else{
					var varin=attids[0];
				}
				$("#prprc").html('{{Config::get('view.currency')}}'+product_vars[varin].retailprice);
				$("#prref").html('Ref.'+product_vars[varin].sku);
				var qty=product_vars[varin].quantity;
				console.log(qty);
				var quant=0;
				if(qty>=10){
					quant=10;
				}
				else{
					quant=qty;
				}
				var qtyoptions="";
				for(var i=1; i<=quant; i++){
					qtyoptions=qtyoptions+'<option value="'+i+'">'+i+'</option>';
				}
				$("#prquantity").html(qtyoptions);
				$("#prvariation_id").val(product_vars[varin].product_variation_id);
				$("#prstock_id").val(qty);
				if(quant==0){
					$("#addcartfrm").attr('disabled','disabled');
					alert("This product is out of stock.")
				}
			}
		});
	@else
		checkinwishlist();
	@endif
	
	
	
	@if(isset($our_sel_product_vars))
		var our_sel_product_vars={{$our_sel_product_vars}};
		console.log(our_sel_product_vars);
		$(".osatts").on('change', function(){
			var pridd=$(this).attr('product-ids');
			var allatt=1;
			var attids = [];
			$(".osat"+pridd).each(function() {
				if($(this).val() !=""){
					attids.push($(this).val());
				}
				else{
					allatt=0;
				}
			});
			if(allatt==1){
				if(attids.length > 1){
					var varin=attids.join();
				}
				else{
					var varin=attids[0];
				}
				$("#os"+pridd+"prc").html('{{Config::get('view.currency')}}'+our_sel_product_vars[pridd][varin].retailprice);
				var qty=our_sel_product_vars[pridd][varin].quantity;
				console.log(qty);
				var quant=0;
				if(qty>=10){
					quant=10;
				}
				else{
					quant=qty;
				}
				var qtyoptions="";
				for(var i=1; i<=quant; i++){
					qtyoptions=qtyoptions+'<option value="'+i+'">'+i+'</option>';
				}
				$("#os"+pridd+"qty").html(qtyoptions);
				$("#pr"+pridd+"variation_id").val(our_sel_product_vars[pridd][varin].product_variation_id);
				if(quant==0){
					alert("This product is out of stock.")
				}
			}
		});
	@endif
	
	
	@if(isset($might_int_product_vars))
		var might_int_product_vars={{$might_int_product_vars}};
		console.log(might_int_product_vars);
		$(".miatts").on('change', function(){
			var pridd=$(this).attr('product-ids');
			var allatt=1;
			var attids = [];
			$(".miat"+pridd).each(function() {
				if($(this).val() !=""){
					attids.push($(this).val());
				}
				else{
					allatt=0;
				}
			});
			if(allatt==1){
				if(attids.length > 1){
					var varin=attids.join();
				}
				else{
					var varin=attids[0];
				}
				$("#mi"+pridd+"prc").html('{{Config::get('view.currency')}}'+might_int_product_vars[pridd][varin].retailprice);
				var qty=might_int_product_vars[pridd][varin].quantity;
				console.log(qty);
				var quant=0;
				if(qty>=10){
					quant=10;
				}
				else{
					quant=qty;
				}
				var qtyoptions="";
				for(var i=1; i<=quant; i++){
					qtyoptions=qtyoptions+'<option value="'+i+'">'+i+'</option>';
				}
				$("#mi"+pridd+"qty").html(qtyoptions);
				$("#mipr"+pridd+"variation_id").val(might_int_product_vars[pridd][varin].product_variation_id);
				if(quant==0){
					alert("This product is out of stock.")
				}
			}
		});
	@endif
	
	$(".slide-info").click(function(){
   	    console.log('sdsd');
      $('.infobox').animate({"width":"100%"}, "slow").css('display', 'contents');
            $(".infobox").slideToggle();
             $(".infobox").animate({
                  width: "toggle"
              });
        });
   

        $(".slide-info").click(function(){
             $(".infobox").animate({
                 width: "toggle"
             });
         });
	$("#heart_wish").click(function(){
		var prvariation_id="";
		var isatt = $(".pratts").length;
		if(isatt){
			var allatt=1;
			var attids = [];
			$(".pratts").each(function() {
				if($(this).val() !=""){
					attids.push($(this).val());
				}
				else{
					allatt=0;
				}
			});
			if(allatt==0){
				alert("Please choose the attributes");
				return false;
			}
			else{
				prvariation_id=$("#prvariation_id").val();
			}
		}
		$.ajax({
			data:{'prvariation_id':prvariation_id,'product_id':{{$product->id}}},
			url:base_url+'/webshop/prwishlist',
			type:"POST",
			cache:false,
			success: function(response){
				$("#wish_itms").html(response);
				$("#myModal67").modal('show');
			}
		});
	});
	$(document).on( "click", ".addtowish", function() {
		$.ajax({
			data:{'list_id':$(this).attr('wish-list-id'),'prvariation_id':$("#prvariation_id").val(),'product_id':{{$product->id}}},
			url:base_url+'/webshop/addtowish',
			type:"POST",
			cache:false,
			success: function(response){
				$("#wish_itms").html(response);
				checkinwishlist();
			}
		});
	});
	$(document).on( "click", ".removetowish", function() {
		$.ajax({
			data:{'list_id':$(this).attr('wish-list-id'),'prvariation_id':$("#prvariation_id").val(),'product_id':{{$product->id}}},
			url:base_url+'/webshop/removetowish',
			type:"POST",
			cache:false,
			success: function(response){
				$("#wish_itms").html(response);
				checkinwishlist();
			}
		});
	});
	$("#ctr_wish").click(function(){
		$(this).parent().addClass('hidesh');
		$(".crt_prt").removeClass('hidesh');
	});
	$(".pratts").change(function(){
		var allatt=1;
		var attids = [];
		$(".pratts").each(function() {
			if($(this).val() !=""){
				attids.push($(this).val());
			}
			else{
				allatt=0;
			}
		});
		if(allatt==1){
			$.ajax({
				data:{'prvariation_id':$("#prvariation_id").val(),'product_id':{{$product->id}}},
				url:base_url+'/webshop/checkinwish',
				type:"POST",
				cache:false,
				success: function(response){
					if(response=='yes'){
						$("#heart_wish i").removeClass('fa fa-heart-o');
						$("#heart_wish i").addClass('fa fa-heart');
					}
					else{
						$("#heart_wish i").removeClass('fa fa-heart');
						$("#heart_wish i").addClass('fa fa-heart-o');
					}
				}
			});	
		}
	});
	$('#new_wish').on('click', function(e){
		$.ajax({
			data:{'list_name':$("#wish_add").val(),'prvariation_id':$("#prvariation_id").val(),'product_id':{{$product->id}}},
			url:base_url+'/webshop/prwishadd',
			type:"POST",
			cache:false,
			success: function(response){
				var res = JSON.parse(response);
				console.log(res);
				if(res.errors){
					if(res.errors.list_name){
						$("#wish_add_list .errorname").html(res.errors.list_name);
						$("#wish_add_list .errorname").fadeIn('slow');
					}
					else{
						$("#wish_add_list .errorename").html('');
					}
				}
				else{
					$("#wish_itms").html(res.resp);
					$("#wish_add").val('');
					$("#ctr_wish").parent().removeClass('hidesh');
					$(".crt_prt").addClass('hidesh');
				}
				checkinwishlist();
			}
		});
  
	});
	$("#wish_add").keyup(function(){$(".errorname").fadeOut();});
	function checkinwishlist(){
		$.ajax({
			data:{'prvariation_id':$("#prvariation_id").val(),'product_id':{{$product->id}}},
			url:base_url+'/webshop/checkinwish',
			type:"POST",
			cache:false,
			success: function(response){
				if(response=='yes'){
					$("#heart_wish i").removeClass('fa fa-heart-o');
					$("#heart_wish i").addClass('fa fa-heart');
				}
				else{
					$("#heart_wish i").removeClass('fa fa-heart');
					$("#heart_wish i").addClass('fa fa-heart-o');
				}
			}
		});
	}
});
</script>

<script type="text/javascript">

	var baseurl = "{{url('webshop')}}";


	 //testimonial slide
var owl = $('#testi_slide');

$('#testi_slide .play').on('click',function(){
    owl.trigger('play.owl.autoplay',[50000])
})
$('#testi_slide .stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
owl.owlCarousel({
    items:4,
	autoplay:true,
	autoplayTimeout:50000,
	autoplayHoverPause:true,
	dots:true,

	navigation:true,
	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    loop:false,
    margin:15,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
})

var owls = $('#testi_slide_1');

$('#testi_slide_1 .play').on('click',function(){
    owls.trigger('play.owl.autoplay',[50000])
})
$('#testi_slide_1 .stop').on('click',function(){
    owls.trigger('stop.owl.autoplay')
})

owls.owlCarousel({
    items:4,
	autoplay:false,
	autoplayTimeout:50000,
	autoplayHoverPause:true,
	dots:true,

	navigation:true,
	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    loop:false,
    margin:15,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
})


$(function(){
  //SyntaxHighlighter.all();
});
// $(window).load(function(){
//   $('.flexslider').flexslider({
//     animation: "fade",
//     start: function(slider){
//       $('body').removeClass('loading');
//     }
//   });
// });


$(document).ready(function()
{
	
	// Closes the sidebar menu on menu-close button click event
	$("#menu-close").click(function(e)					
	{
															
		$("#sidebar-wrapper").toggleClass("active");			
		e.preventDefault(); 									
		
		/*!
		======================= Notes ===============================
		* see: .sidebar-wrapper.active in: style.css
		==================== END Notes ==============================
		*/
	});													

	// Open the Sidebar-wrapper on Hover
	$("#menu-toggle").hover(function(e)							
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();									
	});

	$("#menu-toggle").bind('click',function(e)					
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();								
	});														
	$('#sidebar-wrapper').mouseleave(function(e)				
																
	{
		/*! .toggleClass( className, state ) */
		$('#sidebar-wrapper').toggleClass('active',false);		
																
													
		e.stopPropagation();									
																
																
		e.preventDefault();										
																
	});
});


  $(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
//});


$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});


$(function () {

	"use strict";
	var wind = $(window);

	/*==================================
	ScrollIt Setup
	====================================*/

	

	/*==================================
	Navbar Scrolling Setup
	====================================*/
	
	wind.on("scroll", function () { 
		var swidth = (screen.width);
		var bodyScroll = wind.scrollTop(),
		header = $(".header")
		if(swidth >1024){
			if (bodyScroll > 0) {
				
				header.addClass("fixed-top");
						
				
				
			} 
			else {
				header.removeClass("fixed-top");
								
			}
		}
	});
	
	

});



</script>
