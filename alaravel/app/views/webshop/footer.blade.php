<div class="border">
</div>
<footer class="footer footer_sec">
	<div class="container">
		<div class="col-md-9 col-sm-9 col-md-offset-2 col-sm-offset-2">
			<div class="newsletter">
				       <div class="alert alert-success" id="success-news" style="display: none;">
                       </div>
					
						<div class="alert alert-danger error-news" style="display: none;">
                        </div>
				<form method="POST"  class="add-form-news">
					<div class="group" style="margin-bottom:10px;">											                  
							<input class="form_input" type="text" name="email_sub" id="email_sub" placeholder="" ><span class="highlight"></span><span class="bar"></span>
							<h3 for="" class="errore" style="color: red; margin-top: 4px;"></h3>
							<label class="labletxt">Join our newsletter - Enter your email address</label>
							
				    </div>
					<!-- <div class="form-group newsletterBtn"> -->												
					<div class="form-group">												
                        <div class="checkbox">
							<label class="con">
							  I agree to receive customised commercial communications from Global Property Cowboys via email and other means
							  <input type="checkbox" id="privacy_policy_sub" name="privacy_policy_sub" value="Yes">
							  <span class="checkmark"></span> 						  
							</label>
							<label for="" class="errorc" style="color: red;"></label>
                        </div>						
                         <div class="submit_sec">
                            <!-- <a href="#">subscribe me</a> -->
                            <input class="buttons" type="submit" value="Subscribe me" id="add-form-btn-news">						 
					     </div>
				    </div>
				</form>

			</div>
			<div class="footer_social">
				<div class="col-md-3 col-sm-3">
					<h3>Payment methods</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/credit.jpg')}}" alt="credit"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/paypal.jpg')}}" alt="paypal"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/visa_logo.jpg')}}" alt="visa_logo"></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3">
					<h3>Follow us on</h3>
					<ul>
						<li>
							<a href="https://www.facebook.com/globalpropertycowboys/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://www.instagram.com/globalpropertycowboys/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://www.youtube.com/channel/UCHINpiUff1YdH3jkIjVrksA">
								<i class="fa fa-youtube-play" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/CowboyGlobal"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 drop_cstm">
					<h3>We ship our goods with</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/dhl.png')}}" alt="dhl"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/upslogo.png')}}" alt="upslogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/tntlogo.png')}}" alt="tntlogo"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/corrlogo.jpg')}}" alt="corrlogo"></a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-2 and_sec">
					<h3>Download our app</h3>
					<ul>
						<li><a href="#"><img src="{{url('assets/frontend/images/apple.png')}}" alt="apple"></a></li>
						<li><a href="#"><img src="{{url('assets/frontend/images/android.png')}}" alt="android"></a></li>
					</ul>
				</div>
			</div>
			<div class="footerLinks">
				<div class="col-sm-2 col-md-2 col-xs-12">
					<h4>Shopping guide</h4>
					<div>
						<a class="foot_anch" href="{{url('webshop/shop-guide#faqs')}}">Faqs</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#purchasing')}}">Purchasing process</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#payment')}}" >Payment</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#delivery')}}">Delivery</a>
						<a class="foot_anch" href="{{url('webshop/shop-guide#returns')}}">Return policy</a>
					</div>
					<div class="xz">
						<img src="{{url('assets/frontend/images/footer_logo.png')}}" alt="logo" class="img-responsive">
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<h4>Policies</h4>
					<div>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-private-customers-gpc.pdf" target="_blank">		Conditions of Purchase of Private
						</a>
						<a href="{{url('assets/pdf/')}}/general-terms-and-conditions-for-businesses-gpc.pdf" target="_blank">Conditions of Purchase of Business
						</a>
						<a href="{{url('assets/pdf/')}}/privacy-policy.pdf" target="_blank">Privacy Policy</a>
						<a href="{{url('assets/pdf/')}}/cookies-policy.pdf" target="_blank">Cookies Policy</a>
						<a href="{{url('assets/pdf/')}}/cancellation-form.pdf" target="_blank">Cancellation Form</a>
						<a href="{{url('webshop/contact-shopping')}}">Contact Form</a>
						<a href="{{url('webshop/trackorder')}}">Track Order</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12">
					<h4>Company</h4>
					<div class="jm">
						
						<p><span>Address:</span>
							 schalkwijkpad 29<br>
	                         1107JL Amsterdam, the Netherlands<br>
							<strong>VAT number:</strong> NL860137028B01<br>
							<strong>Chaimber of Commerce: </strong>75087545<br>
							
							<strong>Telefone office hours  </strong>: +31 85 06 07 024<br>
							<strong>Whatsapp 24/7  </strong>:  +31 646824266</p>
						<p><span>Email</span>
						<a>customerservice@globalpropertycowboys.nl</a></p>						                                                                     
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-12">
					<div class="glob_video">
						<iframe src="https://www.youtube.com/embed/St1oxMR2nsA?rel=0&amp;autoplay=0" 
						allowfullscreen="" width="250" height="250" frameborder="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<script src="{{url('assets/frontend/js/jquery.js')}}"></script>
<script src="{{url('assets/frontend/js/scrollIt.min.js')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{url('assets/frontend/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">

 $('#email_sub').keyup(function() {
    if($(this).val()==''){
          
        $('.errore').html('This field is required');
        $(".errore").fadeIn('slow', function(){
        $(".errore").delay(3000).fadeOut();
         });
    }  
      else{
            //alert('unchecked');
        $('.errore').html('');
        }
    });

 $('#privacy_policy_sub').click(function() {
        if($(this).is(':checked'))
            //alert('checked');
        $('.errorc').html('');
        else
            //alert('unchecked');
        $('.errorc').html('You must accept the privacy policy in order to subscribe.');
        $(".errorc").fadeIn('slow', function(){
        $(".errorc").delay(3000).fadeOut();
         });
    });

	/* for  */
$('#add-form-btn-news').on('click', function(e) {

 // $("#add-form-news").valid();

   var email_sub = $('#email_sub').val();

   console.log(email_sub);
   
      if(email_sub==''){
            $('.errore').html('This field is required');
            $(".errore").fadeIn('slow', function(){
            $(".errore").delay(3000).fadeOut();
            });
      	     return false;
      }

	if($('#privacy_policy_sub').is(":checked"))
	{
	  
	} else{

		$('.errorc').html('You must accept the privacy policy in order to subscribe.');
		$(".errorc").fadeIn('slow', function(){
        $(".errorc").delay(3000).fadeOut();
         });
		return false;
	}

var privacy_policy_sub = $('#privacy_policy_sub').val();
 
 e.preventDefault();


 $.ajax({

	url:"{{url('customer/subscribenews')}}",
    type:"POST",
	cache:false,
	data:{
      email:email_sub,
      privacy_policy:privacy_policy_sub
    },
	success: function(response){
		var res = JSON.parse(response);
		if(res.Error)
		{
		console.log(res.Error);	
		$(".error-news").show();
		$(".error-news").html(res.Error);
		$(".error-news").fadeIn('slow', function(){
        $(".error-news").delay(6000).fadeOut(); 
      });
		//$(".password").html(response.errors.password);
		// do what you want with errors, 
		}
		else
		{
		$('#email_sub').val('');
		$('#privacy_policy_sub').attr('checked', false); // Unchecks it	
		$(".error-news").hide();	
		$('#success-news').show();
		$('#success-news').html(res.Success);
		$("#success-news").fadeIn('slow', function(){
        $("#success-news").delay(6000).fadeOut(); 
      });
		//location.reload();

							
	    }
	}
  });

});

$(document).ready(function(){
  $("#menu-toggle").click(function(){
    $("#sidebar-wrapper").toggle();
  });
});	
</script>

<script>
	$(document).ready(function(){
		var hash = location.hash.substr(1);
		$("#"+hash).find('.panel-collapse').addClass('in');
		$("#"+hash).find('.more-less').removeClass('glyphicon-plus');
		$("#"+hash).find('.more-less').addClass('glyphicon-minus');
		window.setTimeout(function(){
			$(".trengo-vue-iframe").contents().find(".material-icons").css("display","none");
		}, 1000);
		// Closes the sidebar menu on menu-close button click event
		$("#menu-close").click(function(e){
			$("#sidebar-wrapper").toggleClass("active");			
			e.preventDefault(); 									
		});													
		// Open the Sidebar-wrapper on Hover
		$("#menu-toggle").hover(function(e){
			$("#sidebar-wrapper").toggleClass("active",true);		
			e.preventDefault();									
		});
		$("#menu-toggle").bind('click',function(e){
			$("#sidebar-wrapper").toggleClass("active",true);		
			e.preventDefault();								
		});														
		$('#sidebar-wrapper').mouseleave(function(e){
			$('#sidebar-wrapper').toggleClass('active',false);		
			e.stopPropagation();									
			e.preventDefault();										
		});
	});
</script>
<script>
	$(window, document, undefined).ready(function() {
		$('input').blur(function() {
			var $this = $(this);
			if ($this.val())
				$this.addClass('used');
			else
				$this.removeClass('used');
		});
	});
</script>
<script>
	$(function () {
		var wind = $(window);
		wind.on("scroll", function () { 
			var swidth = (screen.width);
			var bodyScroll = wind.scrollTop(),
			header = $(".header")
			if(swidth >1024){
				if (bodyScroll > 0) {
					header.addClass("fixed-top");
					$('.contact-form ul.side_nav').css('margin-top','110px');
				} 
				else {
					header.removeClass("fixed-top");
					$('.contact-form ul.side_nav').css('margin-top','20px');
				}
			}
		});
	});
	
	function myFunction(){
		if($("#newsletterId").val() != ""){
			$('.newsletterBtn').css('display','block');
		}
		else{
			$('.newsletterBtn').css('display','none');
		}
	};

	function acceptTerms(){
		$('.newsletterDetail').css('display','block');
	};

</script>
<script>
   
     
	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

 </script>
 
</body>
</html>