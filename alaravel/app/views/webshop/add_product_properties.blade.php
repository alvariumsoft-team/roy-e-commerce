@extends('adashb')

@section('dash_content')

@if(Session::has('message'))
<div style="padding:20px; padding-bottom:0px;">
<div class="alert alert-info">
<p>{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker1" ).datepicker();
  } );
  </script>
<link rel="stylesheet" href="http://inviolateinfotech.com/webshop/public/assets/css/datepicker.css" />

<h2>Add New Webshop Product Properties</h2>

<p>Add Product Properties</p>

<div class="row">
	<div class="col-md-6">
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="submit" value="add" />
		<div style="border:1px solid #dedede;background:#fff;padding:12px;padding-top:6px;margin:12px;">
			<h4>Add New Property</h4>
			
			<div class="form-group row">	
				<label class="col-xs-12 col-md-2" ><b>Property</b></label>
				<label class="col-md-7">
					<input type="text" name="product_property" placeholder="" class="form-control" required />
				</label>
			</div>
			
			<div class="form-group row">
				<label class="col-md-2"><b>Category</b></label>
				<label class="col-md-7">
					<select name="category" class="form-control cpid" required >
						<option value="">Select a category</option>
						@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->categoryname}}</option>
						@endforeach
					</select>
				</label>
			</div>
			
			<div class="form-group row">	
				<div class="col-lg-12 pim">
					<div class="row">
						<label class="col-xs-12 col-md-2" ><b>Values:</b></label>
						<label class="col-xs-12 col-md-7">
							<input type="text" name="product_property_values[]" placeholder="" class="form-control" required />
						</label>
						<label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-plus products_p" style="margin-top:12px;"></a></label>
					</div>
					<div class="row ">
						<div class="col-xs-12 rpmir"></div>
					</div>
				</div>
			</div>
			
			<div class="form-group">{{Form::submit('Add',array('class'=>'btn btn-large btn-primary '))}}</div>
		</div>
		</form>
	</div>
	<div class="col-md-6">
		<form action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="submit" value="update" />
		<div style="border:1px solid #dedede;background:#fff;padding:12px;padding-top:6px;margin:12px;">
			<h4>Product Properties</h4>
			<div class="upr">
				<div class="form-group row">
					<label class="col-md-2"><b>Properties</b></label>
					<label class="col-md-7">
						<select name="pid" class="form-control pid" placeholder="Properties" required >
							<option value="">Select a property</option>
							@foreach($properties as $p)
							<option value="{{$p->id}}">{{$p->name}}</option>
							@endforeach
						</select>
					</label>
				</div>
				
				<div class="form-group row">
					<label class="col-md-2"><b>Category</b></label>
					<label class="col-md-7">
						<select name="category" class="form-control cpid" required >
							<option value="">Select a category</option>
							@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->categoryname}}</option>
							@endforeach
						</select>
					</label>
				</div>
				
				<div class="form-group row">	
					<label class="col-xs-12 col-md-2" ><b>Name</b></label>
					<label class="col-md-7">
						<input type="text" name="product_property" placeholder="" class="form-control upro" required/>
					</label>
				</div>
				
				<div class="form-group row">	
					<div class="col-lg-12 pim upim">
						<div class="row">
							<label class="col-xs-12 col-md-2" ><b>Values:</b></label>
							<label class="col-xs-12 col-md-7">
								<input type="text" name="product_property_values[]" placeholder="" class="form-control uv0" required/>
							</label>
							<label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-plus products_p" style="margin-top:12px;"></a></label>
						</div>
						<div class="row ">
							<div class="col-xs-12 rpmir"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<div class="form-group pull-left">{{Form::submit('Update',array('class'=>'btn btn-large btn-primary '))}}</div>
				<div class="form-group pull-right"><a href="javascript:void(0);" class="btn btn-primary del_prop" data-id="">Delete Property</a></div>
			</div>
		</div>
		</form>
	</div>
</div>

</form>

<script type="text/javascript">
	var caturl = "{{url('/gcat')}}";
	var baseurl = "{{url('/')}}";
	$(document).ready(function(){
		
		$(document).on('click', '.products_p', function(e){
			e.preventDefault();
			var html = '<div class="row pimr"><label class="col-xs-12 col-md-2" ></label><label class="col-xs-12 col-md-7"><input type="text" name="product_property_values[]" placeholder="" class="form-control"/></label><label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-close products_p_remove" style="margin-top:12px;"></a></label></div>';
			$(this).parents('.pim').find('.rpmir').append(html);
		});
		$(document).on('click', '.products_p_remove', function(e){
			e.preventDefault();
			$(this).parents('.pimr').remove();
		});
		$('.del_prop').on('click', function(){
			if(confirm('Are you sure you want to delete?')){
				var id = parseInt($(this).attr('data-id'));
				if(isNaN(id)){alert('Please select a property first!');return;}
				window.location = "{{url('products/propertydelete')}}/"+id;
			}
		});
		$(document).on('click', '.pid', function(){
			var cid = parseInt($(this).val());
			$pim = $(this).parents('.upr').find('.pim').find('.rpmir');
			$cpid = $(this).parents('.upr').find('.cpid');
			$pim.html('');
			if(isNaN(cid)){
				$('.uv0').val('');
				$('.upro').val('');
			return;}
			if(cid < 1){cid = 0;}
			$('.del_prop').attr('data-id', cid);
			$.ajax({
				method: "GET",
				dataType: "json",
				url: "/products/propertyvalues/"+cid,
				success: function (data) {
					if(data.prop != undefined){
						$('.upro').val(data.prop);
						$cpid.val(data.category);
						for(i in data.val){
							if(i == 0){
								$('.uv0').val(data.val[i]);
							}
							else{
								var html = '<div class="row pimr"><label class="col-xs-12 col-md-2" ></label><label class="col-xs-12 col-md-7"><input type="text" name="product_property_values[]" placeholder="" class="form-control" value="'+data.val[i]+'"/></label><label class="col-xs-12 col-md-1" ><a href="#" class="fa fa-close products_p_remove" style="margin-top:12px;"></a></label></div>';
								$pim.append(html);
							}
						}
					}
				},
				error: function () {
					
				}
			});
		});
		
	});
</script>

@stop
