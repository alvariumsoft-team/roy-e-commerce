@include('webshop.header')
<section class="contact-form contact_sec">
	<div class="log_in">
		<div class="container">		  
		  <div class="col-md-8 col-sm-8">
		  @if(Session::has('message-failure'))
<section style="font-size:18px;">

<p  style="text-align:center; color:red;position:relative !important;width:100% !important;background:#f9f9f9;padding:20px;">{{Session::get('message-failure')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>

</section>
<br>
@endif
			  <div class="col-md-10 col-sm-10">
				
					<h4>Track your order</h4>
					
				
					@if(isset($data['trackingNumber']))
						<p>Your order traking information are as below:</p>
					
                     <div class="order_result">
						<table class="table">													
							<tbody>
								<tr> 									
									<td class="tab_bold">Tracking Number</td> 
									<td>{{$data['trackingNumber']}}</td> 									
								</tr> 
                                <tr> 									
									<td class="tab_bold">Status</td> 
									<td>{{$data['statusDescription']}}</td> 									
								</tr> 														
								<tr> 									
									<td class="tab_bold">Order Date</td>
									<td>{{$data['statusDate']}}</td>								
								</tr>
								
								<tr> 									
									<td class="tab_bold">Carrier</td>
									<td>{{$data['carrier']}}</td>								
								</tr>
								
                                 								
							</tbody> 
						</table>
                     </div>	
					 @else
						 <p>Enter your order number to know the status of your order</p>
					@endif
					
	<!-- <form name="login_form" method="POST" action={{url('checkout/login')}} id="loginformPage">-->					 
					<form name="track" method="GET" action={{url('webshop/trackorder')}}>
					   <div class="group {{ $errors->has('username') ? ' error' : '' }}">											                  
								<input id="ordnum" class="form_input @if ($errors->has('username')) used @endif" type="text" name="ordernumber"><span class="highlight"></span><span class="bar"></span>
								<h3 for="" class="erroord" style="color: red; margin-top: 4px;"></h3>
								<label class="labletxt">Order number*</label>										
					   </div>					   					  					 
						<div class="submit_sec">
							<input class="buttons" type="submit" id="tracksub">
						</div>
					</form>					
			  </div>			  
			 
		  </div>		
		</div>
    </div>
</section>
<style type="text/css">
 .submit_sec .buttons {
    display: block;
    width: 100%;
    max-width: 300px;
    padding: 12px 25px;
    font-size: 11px;
    color: #000;
    border: 1px solid #000;
}

.submit_sec .buttons:hover {
    background: #000;
    color: #fff !important;
}
</style>
@include('webshop.nomenu_footer') 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>

<script type="text/javascript">

$('#tracksub').on('click', function(e) {
   var ordnum = $('#ordnum').val();   
      if(ordnum==''){
            $('.erroord').html('This field is required');
      	     return false;
      }

});
$('#ordnum').keyup( function(e) {
   var ordnum = $('#ordnum').val();   
      if(ordnum!=''){
            $('.erroord').html('');
      	     
      }

});

</script>