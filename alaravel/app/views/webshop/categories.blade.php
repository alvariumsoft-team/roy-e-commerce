<style>

.product-inner{
	
    padding: -18px 0 20px 0 !important;
}


</style>
@include('webshop.header')
    <section class="side_bar">
       	<div class="container-fluid">
    		<div class="row">
			  @include('webshop.sidebar')
				<div class="col-sm-9 col-md-9  col-lg-9">
					<div class="col-sm-9 col-md-9  col-lg-9 nomrgpad">
						<nav class="navbar navbar-default cat_nav">
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>                        
							  </button>
							</div>
							<div class="collapse navbar-collapse nomrgpad"  id="myNavbar">
							  <ul class="nav navbar-nav aligngap">
								@foreach($attrs as $attr)
								<li class="dropdown text-uppercase">
								  <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{$attr->name}}<span class="caret"></span></a>
								  <ul class="dropdown-menu droponmob">
									<form  class="scroll">
									@foreach($attr->attr_values as $aval)
										<div class="checkbox">
										  <label class="lbl_set"><input class="patt" name="{{$attr->name}}" type="checkbox" value="{{$aval->name}}"><span class="checkmark"></span>{{$aval->name}}</label>
										</div>
									@endforeach
									 </form>
								  </ul>
								</li>  
								@endforeach	
								<p class="navbar-text text-uppercase">price : <a href="javascript:void(0);" id="product_price_highest">highest</a> | <a href="javascript:void(0);" id="product_price_lowest">lowest</a></p>			
								<button class="btn btn-default navbar-btn text-uppercase" id="clear_search_terms"><a href="javascript:void(0);" >clear</a></button>
							  </ul>
							</div>
						</nav>
					</div>

					<div class="col-lg-3  col-md-3 col-sm-3 nomrgpad">
						<div class="filter-right">
							<nav aria-label="Page navigation">
								 <ul class="pagination pagelinks">
									<li><a href="javascript:void(0);" id="display_2">2</a></li>
									<li><a href="javascript:void(0);" id="display_4">4</a></li>
								</ul>
							</nav>
							<h3>{{$total_products}} items <span>|</span> display</h3>
						</div>
					</div>
					
					<div class="gap20"></div>
					<div class="product-inner">
						<div class="row" id="product_row">
						@foreach($products as $product)
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 maxon420 display_2_4">
								<a href="{{url('webshop/product')}}/{{$slug}}/{{ $product->id}}">
									<img src="{{asset($product->img)}}" alt="{{$product->name}}" class="img-responsive">
								</a>
								<div class="product-content">
									<h2><a href="{{url('webshop/categorydetail')}}/{{ $product->id}}">
										{{$product->name}}
									</a></h2>
									@if(isset($product->metas['_regular_price']))
										<p>{{$product->metas['_regular_price']}}</p>
									@else
										<p>{{$product->price}}</p>
									@endif
								</div>
							</div>
						@endforeach
						<?php if(empty($products)){echo 'No products available.';}?>
						</div>	
					</div>
					<form method="POST" action="{{url('webshop/subscribe')}}">					
						<div class="row">
							<div class="col-lg-12">
								<div class="footer-spot"></div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-1">
									<div class="group">
										<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
										<label style="text-align:center;width:100%;" class="labletxt">Join our newsletter - Enter your email address*</label>
									</div>
								<!--<div class="group" style="float: left;width: 100%; text-align:center;">
											<input class="check-content" type="checkbox" >
											<label style="margin-left: 20px;" class="check-button">I have read and accept the Privacy policy.</label>
									</div>-->
									<div class="col-lg-12  text-center " >
										<input  type="submit" class="button" style=" float:none;" name="subscribe" value="SUBSCRIBE ME">
									</div>
								</div>
							</div>
						</div>	   	
					</form> 
               </div>
		    </div>
        </div>
	</section>
@include('webshop.footer')


	
	
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			 $.ajax({
				 url:'http://inviolateinfotech.com/laravel/webshop/wgbanner',
				 type:'GET',
				 data: 'catid='+id,
				 success: function(result) {
					$('.carousel-inner').html(result);
				 }
				 
				 
			 });
		 }
		 
		 
		  </script>
		  
		  
		  
		  
		  
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>  -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>

        <!--<script type="text/javascript">
          $(function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 280,
              values: [ 0, 280 ],
              slide: function( event, ui ) {
                $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
              }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
             " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          });
        </script>-->
		
		
	 
	 <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
		 }
		 
		 $(document).ready(function(){
		 	var product_order = 'default';
		 	var category = '{{$slug}}';
		 	$('#product_price_highest').on('click', function(){
		 		product_order = 'highest';
				 $.ajax({
					url:"{{url('/webshop/psearch')}}",
					type:"POST",
					cache:false,
					data:{
						order:product_order,
						slug:category
					},
					success: function (res) {
						var res = JSON.parse(res);
						if(res.html != ''){
							$('#product_row').html(res.html);
						}
					},
					error: function (jqXHR, exception) {
						
					},
		
				}).done(function(res){
				});
		 	});
		 	$('#product_price_lowest').on('click', function(){
		 		product_order = 'lowest';
				 $.ajax({
					url:"{{url('/webshop/psearch')}}",
					type:"POST",
					cache:false,
					data:{
						order:product_order,
						slug:category
					},
					success: function (res) {
						var res = JSON.parse(res);
						if(res.html != ''){
							$('#product_row').html(res.html);
						}
					},
					error: function (jqXHR, exception) {
						
					},
		
				}).done(function(res){
				});
		 	});
		 	$('#clear_search_terms').on('click', function(){
		 		product_order = 'default';
				 $.ajax({
					url:"{{url('/webshop/psearch')}}",
					type:"POST",
					cache:false,
					data:{
						order:product_order,
						slug:category
					},
					success: function (res) {
						var res = JSON.parse(res);
						if(res.html != ''){
							$('#product_row').html(res.html);
						}
					},
					error: function (jqXHR, exception) {
						
					},
		
				}).done(function(res){
				});
		 	});
		 	$('#display_2').on('click', function(){
		 		$('.display_2_4').attr('class', 'col-lg-6 col-md-6 col-sm-6 col-xs-6 maxon420 display_2_4');
		 	});
		 	$('#display_4').on('click', function(){
		 		$('.display_2_4').attr('class', 'col-lg-3 col-md-3 col-sm-3 col-xs-3 maxon420 display_2_4');
		 	});
		 	var scroll = 1;
		 	
			$(window).on("scroll", function() {
				var scrollHeight = $(document).height();
				var scrollPosition = $(window).height() + $(window).scrollTop();
				if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
				var page = 2;
					 $.ajax({
						url:"{{url('/webshop/pload')}}/?page="+page,
						type:"POST",
						cache:false,
						data:{
							order:product_order,
							slug:category
						},
						success: function (res) {
							var res = JSON.parse(res);
							if(res.html != ''){
								//$('#product_row').append(res.html);
								scroll = 1;
							}
						},
						error: function (jqXHR, exception) {
							
						},
			
					}).done(function(res){
					});
				}
			});
		 	
		 	
		 	/*
		 	$(window).on('scroll', function(e){
		 	if(scroll == 1){
		 		var h = $(window).height() - 850;
		 		if($(window).scrollTop() == $(window).height()) {
		 		$(window).scrollTop($(window).height() - 1000);
		 		//scroll =0;
		 		var page = 2;
					 $.ajax({
						url:"{{url('/webshop/pload')}}/?page="+page,
						type:"POST",
						cache:false,
						data:{
							order:product_order,
							slug:category
						},
						success: function (res) {
							var res = JSON.parse(res);
							if(res.html != ''){
								$('#product_row').append(res.html);
								scroll = 1;
							}
						},
						error: function (jqXHR, exception) {
							
						},
			
					}).done(function(res){
					});
		 		}
		 	}
		 	});*/
		 	
		 	$('.patt').on('change', function(e){
		 		var check = $('.patt:checked');
		 		var attrs = {};
		 		var alen = 0;
		 		for(i in check){
		 			if(i < check.length){
			 			var name = $(check[i]).attr('name');
			 			var val = $(check[i]).val();
			 			if(attrs[name] == undefined){
			 				attrs[name] = [];
			 			}
			 			attrs[name].push(val);
			 		} 
		 		}
		 		for(i in attrs){alen = alen+1;}
		 		if(alen > 0){
					 $.ajax({
						url:"{{url('/webshop/psearch')}}",
						type:"POST",
						cache:false,
						data:{
							order:product_order,
							slug:category,
							attrs:attrs
						},
						success: function (res) {
							var res = JSON.parse(res);
							if(res.html != ''){
								$('#product_row').html(res.html);
							}
							else{
								$('#product_row').html('<p style="text-align:center;">No results!</p>');
							}
						},
						error: function (jqXHR, exception) {
							
						},
			
					}).done(function(res){
					});
				}
				else{
					 $.ajax({
						url:"{{url('/webshop/psearch')}}",
						type:"POST",
						cache:false,
						data:{
							order:product_order,
							slug:category
						},
						success: function (res) {
							var res = JSON.parse(res);
							if(res.html != ''){
								$('#product_row').html(res.html);
							}
							else{
								$('#product_row').html('<p style="text-align:center;">No results!</p>');
							}
						},
						error: function (jqXHR, exception) {
							
						},
			
					}).done(function(res){
					});
				}
		 	});
		 	
		 });
		 
	 </script>
		  
		  
		  
		  
		  
		  
		  
		  
  </body>

</html>

		  
		  
		  
		  
		  
		  
	
