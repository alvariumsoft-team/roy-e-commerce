<?php die() ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Global property Cowboys | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {{HTML::style('packages/ad/bootstrap/css/bootstrap.min.css')}}
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  {{HTML::style('packages/ad/dist/css/AdminLTE.min.css')}}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  {{HTML::style('packages/ad/dist/css/skins/_all-skins.min.css')}}
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  {{HTML::style('packages/ad/plugins/iCheck/flat/blue.css')}}

{{HTML::script('packages/bootstrap/dist/js/jquery.js')}}

{{HTML::script('packages/js/profile.js')}}

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style type="text/css">

	  .navbar-nav > .dropdown > ul{box-shadow: 0px 0px 2px rgb(153, 153, 153);}
	  
	  @media(max-width: 786px){
	  
	  .skin-blue .main-header .navbar{box-shadow: 0px 0px 0px rgb(153, 153, 153);border-top:1px solid #2a2a2a;}
	  
	  }
  
  
  </style>
  

{{HTML::script('packages/ad/plugins/jQuery/jquery-2.2.3.min.js')}}

<script type="text/javascript">
  
$(document).ready(function(){

$('.treeview').on('click',function(){



});

});  
  
</script>
  
  
 	<style type="text/css">
		
		.ifile-id{display:none !important;}
		
		.logo-img{width:50px;}
		
	</style>
	
	<script type="text/javascript">
		
		/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

$(document).ready(function(){
	
		$(document).on( 'change', 'input[type="file"]', function( e )
		{	
				//console.log($(this).siblings('.ifile-label'));
				label = $(this).parent().find('.ifile-label');
				
				var fileName = '';
				if( this.files && this.files.length > 1 ){
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				}
				else{
					fileName = e.target.value.split( '\\' ).pop();
				}

				if( fileName )
					label.html(fileName);
			
			
		});

});
	
	</script>
  
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
   
    <a href="{{URL::to('/')}}" class="logo" >
		<img src="{{URL::to('packages/images/logo.png')}}"  class="logo-img" ></img>
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!--  <span class="logo-lg " style="text-align:center;"><b>G</b>PC</span>
      <span class="clearfix"></span>
     logo for regular state and mobile devices
      <span class="logo-lg"><b>Global</b>Property Cowboys</span> -->
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 1 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{URL::to('packages/images/auser.png')}}" class="img-circle" alt="User Image">
                       </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Dummy Content?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger"></span>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{URL::to(Auth::user()->img)}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->username}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{URL::to(Auth::user()->img)}}" class="img-circle" alt="User Image">

                <p>
                 
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  
                </div>
                <div class="pull-right">
                  <a href="{{URL::to('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gearsssss"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
           <img src="{{URL::to(Auth::user()->img)}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->username}}</p>
         <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
        </div>
      </div>
      

      
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
               
        <li class=" treeview">
          <a href="{{URL::to('profile')}}">
            <i class="fa fa-dashboard"></i> <span>Profile</span>
          </a>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span> Listing</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'listing')) style="display:block;" @endif >
            <li><a href="{{URL::to('addlisting')}}"><i class="fa fa-plus"></i> Add Listing</a></li>
            <li><a href="{{URL::to('showlisting')}}"><i class="fa fa-eye"></i> View Listing</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span> Project</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"  @if((isset($page)) && ($page == 'project')) style="display:block;" @endif >
            <li><a href="{{URL::to('addcompanylisting')}}"><i class="fa fa-plus"></i> Add Project</a></li>
            <li><a href="{{URL::to('showcompanylisting')}}"><i class="fa fa-eye"></i> View Projects</a></li>
            <li><a href="{{URL::to('addunit')}}"><i class="fa fa-plus"></i> Add Unit</a></li>
            <li><a href="{{URL::to('showunits')}}"><i class="fa fa-eye"></i> View Units</a></li>
            <li><a href="{{URL::to('add360')}}"><i class="fa fa-plus"></i> Add 360 &deg; Images</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span> User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'user')) style="display:block;" @endif >
            <li><a href="{{URL::to('user')}}"><i class="fa fa-plus"></i> Add User</a></li>
            <li><a href="{{URL::to('users')}}"><i class="fa fa-eye"></i> View User</a></li>
            <li><a href="{{URL::to('uprofile')}}"><i class="fa fa-circle-o"></i> Update User</a></li>
            <li><a href="{{URL::to('chpassword')}}"><i class="fa fa-circle-o"></i> Change Password</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span> Plans</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'plans')) style="display:block;" @endif >
            <li><a href="{{URL::to('plans')}}"><i class="fa fa-circle-o"></i>Dashboard</a></li>
            <li><a href="{{URL::to('showplans')}}"><i class="fa fa-eye"></i>View Plans</a></li>
            <li><a href="#"><i class="fa fa-eye"></i>View Users</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Add Plans</a></li>
          </ul>
        </li>
        
        @if(Auth::user()->is_admin == 1)
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span> Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'blog')) style="display:block;" @endif >
            <li><a href="{{URL::to('newpost')}}"><i class="fa fa-plus"></i>New Post</a></li>
            <li><a href="{{URL::to('blogs')}}"><i class="fa fa-eye"></i>View Blog</a></li>
            <li><a href="{{URL::to('categories')}}"><i class="fa fa-eye"></i>Categories</a></li>
            <li><a href="{{URL::to('comments')}}"><i class="fa fa-eye"></i>Comments</a></li>
          </ul>
        </li>        
		
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Countries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'countries')) style="display:block;" @endif >
            <li><a href="{{URL::to('addcountry')}}"><i class="fa fa-plus"></i>Add Countries</a></li>
            <li><a href="{{URL::to('listcountry')}}"><i class="fa fa-eye"></i>List Countries</a></li>
          </ul>
        </li>  
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>WebShop</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" @if((isset($page)) && ($page == 'webshop')) style="display:block;" @endif >
            <li><a href="#"><i class="fa fa-plus"></i>Add Category</a></li>
            <li><a href="#"><i class="fa fa-eye"></i>List Category</a></li>
            <li><a href="#"><i class="fa fa-plus"></i>Add Category</a></li>
            <li><a href="#"><i class="fa fa-eye"></i>List Category</a></li>
          </ul>
        </li>  
		
		@endif      

        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i> <span> Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          </ul>
        </li>
        <li class="header">Sub Navigation</li>
       <!-- <li><a href="#" id="gpc-chat-open"><i class="fa fa-envelope text-red"></i> <span> Chat</span></a></li> -->
		<li><a href="{{URL::to('chat')}}" ><i class="fa fa-envelope text-red"></i> <span> Chat</span></a></li>
        <li><a href="{{URL::to('media')}}"><i class="fa fa-circle-o text-yellow"></i> <span> Media</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span> Dummy Content</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<div class="content-wrapper">

<?php print_r(Session::all()); die();?>

<div style="padding:20px;">

@if(Session::has('message-success'))
<div style="padding:20px; padding-bottom:0px;">
<div class="alert alert-info">
<p>{{Session::get('message-success')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
</div>
@endif

@yield('dash_content')

</div>

</div>


  <footer class="main-footer">

    <strong>Copyright &copy; 2016 <a href="http://laravel.inviolateinfotech.com">Global Property Cowboys</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>


<style type='text/css'>
	
	.chat-box{position:fixed;width:250px;height:40px;right:0px;background:#8c8c8c;bottom:0px;border:1px solid #dedede;}
	.chat-header{padding:10px;background:#365899;height:40px;cursor:pointer;color:#fff;}
	.chat-body{padding:10px;background:#f9f9f9;height:190px;display:block;}
	.chat-footer{background:#f9f9f9;height:50px;display:block;}
	.fa-close{cursor:pointer;}
	.show{height:270px; !important;}
	
	.list-group-share > li > a{text-align:center;font-weight:700;margin:0px;padding:0px;width:100%;display:block;}
	.list-group-item > a{color:#fff !important;}
	.list-group-item{margin-bottom:21px !important;}
	.modal-dialog {width: 475px !important;}
	#mbody{padding:3% 7% !important;padding-top:0% !important;}
	
	.facebook	{background:#3a7ea2 !important;}
	.twitter	{background:#55c5ef !important;}
	.linkedin	{background:#4875b4 !important;}
	.google-plus{background:#c63d2d !important;}
	.youtube	{background:#c4302b !important;}
	.instagram	{background:#000 !important;}
	
	#modal-title{text-align:center;font-weight:700;font-size:20px;}
	
</style>




<script type='text/javascript'>

$(document).ready(function(){

	$('.share').on('click',function(){
		
		var social = ['twitter','facebook','instagram','youtube'];
		
		var share = $('.share-like-item');
		
		$('#modal-title').html('SHARE');
		
		share.each(function(i){
		
		$(this).attr('href','share/'+social[i]);
		$(this).html(social[i].toUpperCase());
		$(this).parent().addClass(social[i]);	
			
		});
		
		$('#gpc-modal').modal('show');
		
	});
	
	$('.like').on('click',function(){
		
		var social = ['twitter','facebook','instagram','youtube'];
		
		var share = $('.share-like-item');
		
		$('#modal-title').html('LIKE');
		
		share.each(function(i){
		
		$(this).attr('href','like/'+social[i]);
		$(this).html(social[i].toUpperCase());
		$(this).parent().addClass(social[i]);	
			
		});
		
		$('#gpc-modal').modal('show');
		
	});
	
	$('.share-like-item').on('click',function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		alert(href);
	
	});

$.support.cors = true;

 var source = new EventSource('http://laravel.inviolateinfotech.com/listingevent');
  source.onmessage = function(e) {
	  console.log(e.data);
  }




});
	
</script>

<style type="text/css">

.gpc-chat-box{position:fixed;display:none;bottom:0px;right:0px;height:300px;width:300px;background:#fff;}

.gpc-chat-header{background:#29333d;padding:10px;color:#f9f9f9;height:40px;}

.gpc-chat-header > a{color:#fff;}

.gpc-header-widgets{padding-right:10px;}

.gpc-chat-body{height:200px;overflow-y:scroll;padding:10px;}

.gpc-chat-message{width:80%;}

.gpc-chat-guest-message{padding:5px 10px;font-size:12px;background:#eee;color:#000;margin:5px;text-align:left;border-radius:12px;line-height: 1.28;overflow-wrap: break-word;}

.gpc-chat-user-message{padding:5px 10px;font-size:12px;background:#6b86cb;color:#fff;margin:5px;text-align:right;border-radius:12px;line-height: 1.28;overflow-wrap: break-word;}

.gpc-chat-footer{border:1px solid #fff;border-top:1px solid #72afd2;height:60px;}

.gpc-chat-textarea{height:100% !important;border-radius:5px;border:0px !important;resize:none;}

</style>

<script type="text/javascript">

$(document).ready(function(){

$('#gpc-chat-open').on('click',function(){ $('.gpc-chat-box').css('display','block'); });

$('#gpc-chat-close').on('click',function(){ $('.gpc-chat-box').css('display','none'); });

$('.chat-message-link').on('click',function(){ 
	$('#gpc-chat-username').html($('#profile-username').html());
	$('.gpc-chat-box').css('display','block'); 
});


$('#gpc-chat-minimize').on('click',function(){ 
	
	console.log($('#gpc-chat-ce').attr('class'));
	
	if($('#gpc-chat-ce').attr('class') == "fa fa-compress"){
	$('.gpc-chat-box').css('height','40px');
	$('#gpc-chat-ce').removeClass('fa fa-compress');
	$('#gpc-chat-ce').addClass('fa fa-expand');
	
	}
	else{
	$('.gpc-chat-box').css('height','300px');
	$('#gpc-chat-ce').removeClass('fa fa-expand');
	$('#gpc-chat-ce').addClass('fa fa-compress');
		
	}
	
});

$('.gpc-chat-textarea').on('keyup',function(e){ 
	if(e.which == 13){
	var val = $(this).val(); 
	var html = '<section class="gpc-chat-message pull-right"><aside class="gpc-chat-user-message ">'+val+'</section></section>';
	$('.gpc-chat-body').append(html);
	var wtf    = $('.gpc-chat-body');
	var height = wtf[0].scrollHeight;
	wtf.scrollTop(height);
	$(this).val('');
		send_ajax_message($('#url').val(), val, $('#to').val(), $('#from').val());
	}
});


});

</script>

<div class="gpc-chat-box">

<header class="gpc-chat-header">
<a href="#" class="pull-left" id="gpc-chat-username"></a>
<a href="#" class="pull-right gpc-header-widgets" id="gpc-chat-close"><i class="fa fa-close"></i></a>
<a href="#" class="pull-right gpc-header-widgets" id="gpc-chat-minimize"><i class="fa fa-compress" id="gpc-chat-ce"></i></a>
<div class="clearfix"></div>
</header>

<section class="gpc-chat-body">

<section class="gpc-chat-message-box">

<section class="gpc-chat-message pull-left">

<aside class="gpc-chat-guest-message ">hello!!</aside>

</section>

<section class="gpc-chat-message pull-right">

<aside class="gpc-chat-user-message ">
	hello back!!!
</aside>

</section>

</section>

<section class="clearfix"></section>

</section>

<footer class="gpc-chat-footer">
<textarea class="form-control gpc-chat-textarea"></textarea>

@if(isset($user))
<!--
<input type="hidden" id="from" value="{{Auth::user()->id}}" />
<input type="hidden" id="to"   value="{{$user->id}}" />
<input type="hidden" id="url"  value="{{URL::to('chat')}}" />
-->
@endif

</footer>

</div>







<script type='text/javascript'>

$(document).ready(function(){
	
	$('.btn-modal').on('click', function(){
		
		$('#gpc-modal').modal('show');
		
	});
	
});

</script>




<div class="modal fade" id="gpc-modal" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><div class="modal-title" id="gpc-modal-label"></div>
			<h4 class="modal-title" id="modal-title"></h4>
			</div>
			<div class="modal-body" id="mbody">
			
			

			</div>
		</div>
			<!--<div class="modal-footer"><button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Close</button></div>-->
	</div>
</div>












<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
{{HTML::script('packages/ad/bootstrap/js/bootstrap.min.js')}}
<!-- Slimscroll -->
{{HTML::script('packages/ad/plugins/slimScroll/jquery.slimscroll.min.js')}}
<!-- FastClick -->
{{HTML::script('packages/ad/plugins/fastclick/fastclick.js')}}
<!-- AdminLTE App -->
{{HTML::script('packages/ad/dist/js/app.min.js')}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src=""></script>
{{HTML::script('packages/ad/dist/js/demo.js')}}

</body>
</html>
