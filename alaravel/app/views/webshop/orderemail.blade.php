<!-- Bootstrap -->
<link href="{{url('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Style -->
<link href="{{url('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{url('/assets/frontend/css/demo.css')}}" type="text/css" media="screen" />

<!-- Responsive -->
<link href="{{url('/assets/frontend/css/responsive.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{url('/assets/frontend/css/font-awesome.min.css')}}">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap" rel="stylesheet">
<section class="contact-form">
	<div class="thankyou">
		<div class="container">
           <div class="col-md-12 col-sm-12">
				<h1>Thank you! Your order has been placed..</h1>
           </div>
          <div class="col-md-4 col-sm-4">
          </div>
                <?php
				$items = 0;
				$total = 0;
				?>
          <div class="col-md-12 col-sm-12">
					 <div class="prosec">
						<div class="product_buy product_bill">
						@if(isset($products))
							@foreach($products as $session)
							<?php
							$items = $items + $session->quantity;
							$total = $total + ($session->quantity * $session->price);
							?>
							<div class="product_leftimg vbb">
								<img src="{{url('public/webshop/bigproducts')}}/{{$session->img}}" class="img-responsive" alt="product">
							</div>
							<div class="product_rightsec vbb">
								<div class="item_description item_df">
									<div class="table-responsive">
									   <table class="table">
											<tr>
												<td class="grey_sec">{{$session->name}}</td>
												<td>Price</td>
												<td>Quantity</td>
												<td>Excl. TAX</td>
												<td>Tax</td>
												<td class="bold-sec">Incl. TAX</td>
											</tr>
											<tr>
												<td class="grey_sec">ref. {{$session->sku}}<br>{{$session->attributes}}</td>
												<td>{{Config::get('view.currency')}}{{number_format($session->price,2)}}</td>
												 <?php

											$totalPrice = $session->quantity * $session->price;
											$tax        = $session->taxrate;

											$finalTotal = ($totalPrice * $tax)/100;

											?>
												<td>{{$session->quantity}}</td>
												<td>{{Config::get('view.currency')}}{{number_format(($session->quantity * $session->price),2)}}</td>
												<td>{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}</td>
												<td class="bold-sec">{{Config::get('view.currency')}}{{number_format(($finalTotal + $totalPrice),2)}}</td>
											</tr>
									   </table>
									 </div>

								</div>
								<div class="item_description item_df">


								</div>
							</div>
							@endforeach
						@endif

						</div>
						<div class="total_bill">
							<table class="table">
								<tbody>
									<tr>
										<td>Subtotal:</td>
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format($checkout[0]->total,2)}}@endif</td>
									</tr>
									<tr>
										<td>Shipping & Handling:</td>

										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->shipping_cost),2)}}@endif</td>
									</tr>
									<tr>
										<td>Total <span class="reference">(Excl. Tax)</span>:</td>
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->shipping_cost + $checkout[0]->total),2)}}@endif</td>
									</tr>
									<tr>
										<td>Tax:</td>
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->tax),2)}}@endif</td>
									</tr>
                                    <tr style="border-top:1px solid #ccc;">
										<td style="font-weight:700;">Grand Total:</td>
										<td style="float:right; font-weight:700;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->total_cost),2)}}@endif</td>
									</tr>
								</tbody>
							</table>
						</div>
					  </div>
				  </div>
		</div>
    </div>
</section>
