@include('webshop.web_search_header')
<style>
/*.maxon420:hover .swatches{z-index: 987875;margin-top: -35px;position: relative;margin-bottom: 34px;display:block;}
.swatches{display:none;}
.swatches ul li{display:inline;padding-right:6px;}
.swatches ul li a span{border:1px #000 solid;padding: 8px 16px;}
*/
/*.scroll {
    max-height: 256px;
    min-height: 48px;
    overflow-y: scroll;
}*/


</style>
<section class="contact-form">
	<div class="product">
  <div class="container-fluid">
    <div class="col-md-2 col-sm-2">
     @include('webshop.left_panel')
    </div>
    <div class="col-md-10 col-sm-10">
    	
    	<div class="row">
		
    	  <div class="col-md-9 col-sm-9">      
    	    <nav class="navbar navbar-default cat_nav">				
    	    	<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				  </button>
				</div>
				<p class="navbar-text text-uppercase">price : <a href="javascript:void(0);" id="product_price_highest">highest</a> | <a href="javascript:void(0);" id="product_price_lowest">lowest</a></p>			
				<button class="btn btn-default navbar-btn text-uppercase" id="clear_search_terms"><a href="javascript:void(0);" >clear</a></button>
            </nav>
          </div>  
             
		    <div class="col-lg-3  col-md-3 col-sm-3 nomrgpad displaytype">
				<div class="filter-right">
					<nav aria-label="Page navigation">
		                 <ul class="pagination pagelinks">
							<li><a href="javascript:void(0);" id="tt_items">{{$total_products}} ITEMS &nbsp;| &nbsp;  VIEW</a></li>
		                    <li><a href="javascript:void(0);" id="display_2" class="dis_lnk">2</a></li>
		                    <li><a href="javascript:void(0);" id="display_4" class="dis_sel">4</a></li>
		                </ul>
		            </nav>
				</div>
		    </div>
    	</div>			
    <div class="gap20"></div>
      <div class="product_inner" id="product_row">
		<div class="row row_sec"  id="out">
			@if(!empty($products))
				@foreach($products as $k=>$product)	
					<div class="col-md-3 col-sm-3 padd-sec display_2_4">
						<div class="product_content">
						  <div class="carousel slide" id="carousel-example-generic{{$k}}" data-ride="carousel" data-interval="false">
							<ol class="carousel-indicators">
							@foreach($product->images as $i=> $img)
								<li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="@if($i==0)active @endif"></li>
							@endforeach
							</ol>
							<div class="carousel-inner" role="listbox">
								@foreach($product->images as $j=> $img)
								<div class="item @if($j==0)active @endif">
									<a href="{{url('/webshop/product')}}/{{$product->url}}"><img src="{{url('public/webshop/bigproducts')}}/{{$img}}" class="img-responsive" alt="{{$product->name}}">
									</a>
								</div>
								@endforeach
							</div>
							<a href="#carousel-example-generic{{$k}}" class="left carousel-control" role="button" data-slide="prev"> 
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left" aria-hidden="true"></i>
								</span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-generic{{$k}}" class="right carousel-control" 
								role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right" 
								aria-hidden="true"></i></span> <span class="sr-only">Next</span> </a> </div>
							<h2>
							<a href="{{url('/webshop/product')}}/{{$product->url}}">
								{{$product->name}}</a>
							</h2>
							<p>{{Config::get('view.currency')}}{{number_format($product->retailprice,2)}}</p>
						</div>
					</div>
				@endforeach 
			@else
			<div>
				<h2 id="no_prd">No products are available in this category.</h2>
			</div>
			@endif
		</div>
	  </div>
 </div>
<div class="ajax-load text-center" style="display:none">
  <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More </p>
</div> 
</section>
@include('webshop.footer')

<script src="{{url('assets/frontend/script/product_cat_search.js')}}"></script>




