@extends('adashb') 
@section('dash_content')

<!-- Child Categories -->
<div class="side-body padding-top">
    <h2>Category</h2>
    <table class="table">
        <thead>
            <tr>
                <th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>
                <th>Category Name</th>
                <th>Parent Category Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{Form::checkbox('listing_action','','')}}</td>
                <td>{{$category['categoryname']}}</td>
                <td>{{$category['parencategoryname']}}</td>
                <td>
                    <a href="{{url('/wecategory/?wcatid=')}}{{$category['id']}}" style="font-weight:bold;margin-right:5px;">Edit</a>
                    <a href="{{url('/wdcategory/?wcatid=')}}{{$category['id']}}" style="font-weight:bold;margin-right:5px;" onClick="return confirm('Are you sure');">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <?php echo $categories->links(); ?>
</div>
<div class="modal fade" id="cities-modal" tabindex="-1" role="dialog" aria-labelledby="cities-modal-label" aria-hidden="true">
    <div class="modal-dialog modall-dialog">
        <div class="modal-content modall-content">
            <div class="modal-header modall-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title modall-title" id="cities-modal-label"><b>Cities</b></div>
            </div>
            <div class="modal-body modall-body" id="mbody">
                <div id="modal-cities"></div>
            </div>
        </div>
    </div>
</div>
@stop
