@include('webshop.header')
<section class="contact-form">
	<div class="thankyou">
		<div class="container">
			@if (Session::get('message-error'))
				<div class="col-md-12 col-sm-12">
					<div class="alert alert-danger">
						{{Session::get('message-error')}}
					</div>
				</div>
			@else
				<div class="col-md-12 col-sm-12">
					<h1>Thank you! Your order has been placed..</h1>
					<p class="down-padd">
						An email receipt including details of your order has been sent to
						customerservice@globalpropertycowboys.nl
					</p>
				</div>		   
				<div class="col-md-4 col-sm-4">				
					 <h2>This order will be shipped to:</h2>
					 @if(isset($shipping))
					 <div class="nj">
						 <p>{{$shipping->shipping_name}} {{$shipping->shipping_surname}}</p>
						 <p>{{$shipping->shipping_floor}} {{$shipping->shipping_address}} </p>
						 <p>{{$shipping->shipping_city}},{{$shipping->shipping_postalcode}}</p>
						  <p>{{$shipping->shipping_country}}</p>
					 </div>
					  <h2>Shipping method</h2>
					 <p>{{$shipping->shipping_method}} - {{$shipping->shipping_delay}}</p>
					 <h2>Payment Method</h2>
					  <p>{{$shipping->payment_method}}</p>
					  @endif				 
				</div>
                <?php  
				$items = 0;
				$total = 0;
				?>       
				<div class="col-md-8 col-sm-8">
					 <div class="prosec">
						<div class="product_buy product_bill">
							@foreach($products as $session)	
							<?php
							$items = $items + $session->quantity;
							$total = $total + ($session->quantity * $session->price);
							?>
							<div class="product_leftimg vbb">
								<img src="{{url('public/webshop/bigproducts')}}/{{$session->img}}" class="img-responsive" alt="product">
							</div>
							<div class="product_rightsec vbb">
								<div class="item_description item_df">
									<div class="table-responsive">
									   <table class="table">
											<tr>
												<td class="grey_sec">{{$session->name}}</td>
												<td>Price</td>
												<td>Quantity</td>
												<td>Excl. TAX</td>
												<td>Tax</td>
												<td class="bold-sec">Incl. TAX</td>
											</tr>
											<tr>
												<td class="grey_sec">ref. {{$session->sku}}<br>{{$session->attributes}}</td>
												<td>{{Config::get('view.currency')}}{{number_format($session->price,2)}}</td>
												 <?php 

											$totalPrice = $session->quantity * $session->price;
											$tax        = $session->taxrate;

											$finalTotal = ($totalPrice * $tax)/100;

											?>
												<td>{{$session->quantity}}</td>
												<td>{{Config::get('view.currency')}}{{number_format(($session->quantity * $session->price),2)}}</td>
												<td>{{Config::get('view.currency')}}{{number_format($finalTotal,2)}}</td>
												<td class="bold-sec">{{Config::get('view.currency')}}{{number_format(($finalTotal + $totalPrice),2)}}</td>
											</tr>
									   </table>
									 </div>
								   
								</div>
								<div class="item_description item_df">
									
									
								</div>
							</div>
							@endforeach
							
							
							
						</div>
						<div class="total_bill">
							<table class="table"> 							
								<tbody> 
									<tr> 
										<td>Subtotal:</td> 
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format($checkout[0]->total,2)}}@endif</td> 										
									</tr> 
									<tr> 									
										<td>Shipping & Handling:</td> 

										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->shipping_cost),2)}}@endif</td>										
									</tr> 
									<tr> 									
										<td>Total <span class="reference">(Excl. Tax)</span>:</td> 
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->shipping_cost + $checkout[0]->total),2)}}@endif</td> 										
									</tr>
									<tr> 									
										<td>Tax:</td> 
										<td style="float:right;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->tax),2)}}@endif</td> 										
									</tr>
                                    <tr style="border-top:1px solid #ccc;"> 									
										<td style="font-weight:700;">Grand Total:</td> 
										<td style="float:right; font-weight:700;">{{Config::get('view.currency')}}@if(isset($checkout[0])){{number_format(($checkout[0]->total_cost),2)}}@endif</td> 										
									</tr>									
								</tbody> 
							</table>
						</div>
					  </div>
					   
				</div>
			@endif
		</div>
	</div>
</section> 
@include('webshop.nomenu_footer')
