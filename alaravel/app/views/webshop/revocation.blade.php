@include('webshop.header')
 <section class="contact-form">
	<div class="container-fluid">
	  <div class="col-md-2 col-sm-2">
		@include('webshop.left_panel')
	  </div>
	  <div class="col-md-10 col-sm-10">
		
		<div class="col-md-12 col-sm-12 return-con questions term">
			<h2 class="faq_sec">REVOCATION</h2>		
			<div class="content">
		
      <p class="title">Within what period of time can you revoke the agreement? </p>
	  <p>You have a cooling-off period of 14 days (your statutory right of withdrawal), after the day on which
you have received all articles of your order. This means that you can revoke the contract without
giving any reason within this period. You can also use a reflection period of 30 days (your contractual
right of withdrawal). You do not have to wait until you have received your order in full.
</p>
	  
    </div>
	<div class="content">
		
      <p class="title">How do you make use of your right of withdrawal? </p>
      <p>If you want to return an article, please let us know by means of a clear statement that you want to
revoke the agreement. We must have received your notice of withdrawal before the withdrawal
period (of 14 or 30 days) has expired. There are several ways of notifying us of your withdrawal. The
easiest way is to bring the items to one of our Zara Home stores in the Netherlands. You can also
return the items to the distribution centre. If you prefer to send the products to our distribution
centre, please pack them and add the electronic receipt. Please send the return to the following
address: Global Property Cowboys, Hoogoordweg 53 1101BB Amsterdam, The Netherlands</p>
<p>You can also notify us of your cancellation by telephone (toll-free) on 0800 209 4949 or by sending us
an e-mail at info@globalpropertycowboys.nl Finally, you can always use the Model Revocation Form.
It is not mandatory to use the Model Form. The Model Revocation Form can be found here. </p>
	  
	  
    </div>
	<div class="content">
		
      <p class="title">Within what period of time should you return the items? </p>
     
	  <p>If you have exercised your statutory right of withdrawal, you must return the items no later than 14
days after the day you notified us of the withdrawal from the contract. This can be done by delivering
the items to a Zara Home shop in the Netherlands. You can also return the items by post to our
distribution centre. The direct costs for return are for your account. You are on time if you hand over
or return the items before the period of 14 days for return has expired.
</p>
	 
	  
	  
    </div>
	
	<div class="content">
		
      <p class="title">When and how will you receive the amount of your return? </p>
      <p>At the latest within 14 days after you have informed us - within 14 or 30 days reflection period - of
your decision to withdraw from the contract, you will receive your payment back from us. If you use
your statutory cooling-off period of 14 days, you will also receive a refund of the delivery costs paid
by you, up to a maximum equal to the amount of the cheapest standard delivery offered by us. We
will refund you with the same means of payment as with which you made the original purchase,
unless you have explicitly agreed otherwise. No costs will be charged for the refund. We may
withhold a refund until we have received the items from you, or until you have shown that you have
returned the items, whichever comes first. You are only liable for the depreciation of the articles 
resulting from the use of the articles, which goes beyond what is necessary to determine the nature,
characteristics and operation of the articles.</p>
	  
	 
	  
	  
    </div>
	<div class="content">
		
      <p class="title">Contractual right of withdrawal (cooling-off period) of 30 days</p>
      <p>You can also return the items to us within 30 days from the date of dispatch of your order. This
means that, even after your statutory withdrawal period has expired, you can notify us within the
period of 30 days that you want to return the items. In order to be able to make use of this, you must
also return the items to us together with the invoice for your order within 30 days of the date on
which your order was shipped. If you wish to withdraw from the contract after your statutory
withdrawal period has expired, you will be refunded the amount of your return shipment, excluding
the delivery costs. The direct costs for return are for your account, unless you hand over the articles
in one of our Zara Home stores in the Netherlands with the invoice of your order.</p>
	 
	  
	  
    </div>
	
	<div class="content">
		
      <p class="title">What should you do if the return amount is not correct? </p>
      <p>Please contact our customer service. We will solve the problem as soon as possible.</p>
	 
	  
	  
    </div>
	<div class="content">
		
      <p class="title">When can you not exercise the right of withdrawal? </p>
      <p>The right of withdrawal does not apply to items which, for health and hygiene reasons, are not
suitable for return and whose seal is broken after delivery.
</p>
	 
	  
	  
    </div>
	<div class="content">
		
      <p class="title">Your statutory warranty</p>
      <p>If an article does not comply with the agreement, then we will comply with the legal guarantee
guarantees that apply to it.
</p>
	 
	  
	  
    </div>
			</div>	
	        </div>
      </div>		
	</div>

</section> 
@include('webshop.footer')