@include('webshop.header')
	<style type="text/css">
		.md-content {
			color: #1e1e1e;
			display: block;
			position: relative;
			-webkit-overflow-scrolling: touch;
		}


		.md-content.md-default-theme, md-content {
			color: rgba(0,0,0,0.87);
			background-color: rgb(255,255,255);
		}
		.append_option {
		  position: relative;
		  z-index: -12;
		}.form{
			margin-bottom:89px;
			width: 85%;
		}
		.detail-inner .detail-look {
			margin-top: -3%;
			float: left;
			width: 100%;
		}

		.col-lg-6
		{
		padding-right: 6px;
		padding-left: 37px !important;
		}

		.md-button{
		background: white;
		color: #1e1e1e;
		white-space: nowrap;
		text-transform: uppercase;
		font-weight: 500;
		font-size: 1.1rem;
		font-style: inherit;
		font-variant: inherit;
		font-family: inherit;
		text-decoration: none;
		overflow: hidden;
		border: 1px solid #1e1e1e;}

		a.md-no-style, button.md-no-style {
		 font-weight:400;
		 background-color:inherit;
		 text-align:left;
		 border:none;
		 padding:0;
		 margin:0
		}
		button, input, select, textarea {
		 vertical-align:baseline
		}
		button, html input[type=button], input[type=reset], input[type=submit] {
		 cursor:pointer;
		 -webkit-appearance:button
		}
		button[disabled], html input[type=button][disabled], input[type=reset][disabled], input[type=submit][disabled] {
		 cursor:default
		}
		
		a{outline:none;}
		.prop-nav > .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
		.prop-nav > .nav-tabs > li > a { border: none; color: #666; }
		.prop-nav > .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #29333d !important; background: transparent; }
		.prop-nav > .nav-tabs > li > a::after { content: ""; background: #29333d; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
		.prop-nav > .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
		.prop-nav > .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
				
		.tab-pane{margin-top:10px;padding:5px;}
		
	</style>

    <section class="side_bar">
       		<div class="container-fluid">
    			<div class="row">
				  @include('webshop.sidebar')
    				<div class="col-sm-9 col-lg-9">
					
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="">
                                        <ol class="carousel-indicators">
                                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					  @foreach($product_images as $k=>$pm)
                                          <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}"></li>
					  @endforeach  
                                        </ol>
                                        <div class="carousel-inner">

						<div class="item active">
							 <img src="{{url($product['img'])}}" alt="First-pic" style="width:100%;">
						 </div>
						 @foreach($product_images as $pm)
						<div class="item">
							 <img src="{{url($pm->imagename)}}" alt="No-image" style="width:100%;">
						 </div>
						 @endforeach
										
                                        </div>
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="top: 225px;background:transparent;
color: black;">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="top: 225px;background: transparent;
color: black;">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
							<?php //echo"<pre>";print_r($product);echo"</pre>";die; ?>
                                <div class="col-xs-12 col-sm-5" style="margin-top:40px;">
                                    <div class="detail-content">
                                        <h2><?php echo $product['name'];?></h2>
                                        @if($product['type'] == 'variable')
                                        	<p id="regular_price" style="margin: 8px 0 8px;"><b><?php echo $product['price_min'];?>€</b> - <b><?php echo $product['price_max'];?>€</b> </p>
                                        	<p id="sale_price" style="text-decoration: line-through;margin: 8px 0 8px;color:red;"><b><?php echo $product['price_min_sale'];?>€</b> - <b><?php echo $product['price_max_sale'];?>€</b> </p>
                                        @else
	                                       	 <p style="margin: 8px 0 8px;"><b><?php echo $product['price'];?>€</b></p>
                                        @endif
                                        
                                        <p style="margin: 8px 0 8px;">VAT INCLUDED / SHIPPING COSTS NOT INCLUDED</p>
                                        <form class="form cartform">
                                        <input type="hidden" name="product" id="product" value="{{$product['product_id']}}" />
					<div class="">
					@if(isset($attrs['size']))				
                                             <div class="col-lg-6">
                                                <div class="selectBox">
                                                    <select name="size" id="size" class="form-control required" required>
							 <option value="">Size</option>
								@foreach($attrs['size']->attr_values as $size)
									<option value="{{$size->name}}">{{ucwords($size->name)}}</option>
								@endforeach
                                                    </select>
                                                </div> 
                                            </div>
                                       @endif
                                            <div class="col-lg-6">
                                                <div class="selectBox_quantity" onclick="showdropdown()">
                                                    <select name="quantity" id="quantity" class="form-control required" required>
                                                        <option value="">Quantity</option>
								@if(isset($product['quantity']))
								@if($product['quantity'] > 10)
									@for($i=1;$i<=10;$i++)
										<option value="{{$i}}">{{$i}}</option>
									@endfor
								@else
									@for($i=1;$i<=$product['quantity'];$i++)
										<option value="{{$i}}">{{$i}}</option>
									@endfor
								@endif
								@endif

                                                    </select> 
                                                </div>                 
                                            </div>
                                        </div>
										 </div>
										 
                                      <!---  <a href="{{url('webshop/addtocart')}}/add/<?php // echo $product->id; ?>" class="button" style="text-decoration: none;width: 85%">ADD TO BASKET</a>--->
									  
									<div name="addToCartForm">
										<input type="hidden" class="inputid" value=""/>

										<input type="hidden" class="item_name" id="item_name" value=""/>
										<input type="hidden" class="item_src" id="item_src" value=""/>
										<input type="hidden" class="item_price"  id="item_price" value=""/>
										<input type="hidden" class="item_qty"  id="item_qty" value=""/>
										<input type="submit" class="submitbutton button" style="text-decoration: none;width: 85%"value="ADD TO BASKET" />
									</div>
									</form>
									  
									  <span id="detailinfo" style="margin-left:20px;cursor:pointer;"><b style="margin-right:5px;font-size:20px;">&plus;</b>INFO</span>
									  
                                    </div>
                                </div>
                            </div>
							<div class="detail-inner" style="padding: 2px 0;">
							<div class="detail-look" style="padding-top:10%;"> 
						<h3>complete the look</h3>
						@foreach($complete_look as $cl)
							<div class="col-xs-12 col-sm-3 col-lg-3">
								<a href="{{url('/webshop/product')}}/{{$cl->category_name}}/{{$cl->id}}">
									<img src="{{url($cl->img)}}" alt="sofa-1" class="img-responsive">
								</a>
								<div class="product-content">
									<h5>- NEW -</h5>
									<h2><a href="#">{{$cl->description}} </a></h2>
									<p>{{$cl->price}}&#8364; </p>
								</div>
							</div>
						@endforeach
					</div>
					<div class="detail-carousel">
                               <h3>YOU MIGHT BE INTERESTED</h3>
    					<div class="product-inner">
    						<div class="row">
			                       @foreach($might_be_interested as $mbin)
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('/webshop/product')}}/{{$mbin->category_name}}/{{$mbin->id}}">
    									<img src="{{url($mbin->img)}}" alt="sofa-1" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="">{{$mbin->description}}</a></h2>
    									<p>{{$mbin->price}}&#8364; </p>
    								</div>
    							</div>
			                       @endforeach


    						</div>
    					</div>
			<form method="POST" action="{{url('webshop/subscribe')}}">					
				<div class="row">
				<div class="col-lg-12">
					<div class="footer-spot"></div>
					<div class="col-lg-2"> </div>
						<div class="col-lg-8 ">
							<div class="group ">
								<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;">Join our newsletter - Enter your email address*</label>
							</div>
				 <div class="group" style="float: left;width: 100%;">
								<input class="check-content" type="checkbox" style="margin-left:180px;">
								<label style="margin-left: 20px;text-align:center;width:100%;" class="check-button">I have read and accept the Privacy policy.</label>
							</div>
								 <div class="col-sm-8 col-sm-offset-4 col-md-0 col-md-offset-0" style="text-align:center">
				  <input  type="submit" class="button" style="float:right" name="subscribe" value="SUBSCRIBE ME">
						</div></div>
						<div class="col-lg-2"></div>
				</div>
				</div>	   	
			</form>   
					</div>
                      
    				</div>
    			</div>
    		</div>	
	</section>
	
	
	<!-- Modal -->
	<div class="modal fade" id="dinfo" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="width: 60%!important;">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body prop-nav">
			<ul class="nav nav-tabs tabgd" role="tablist">
				<li role="presentation" class="active"><a class="tal" href="#description" aria-controls="profile" role="tab" data-toggle="tab">DESCRIPTION</a></li>
				<li role="presentation"><a class="tal" href="#composition" aria-controls="messages" role="tab"data-toggle="tab">COMPOSITION/CARE</a></li>
				<li role="presentation"><a class="tal" href="#delivery_return" aria-controls="messages" role="tab" data-toggle="tab">DELIVERY/Returns</a></li>
				<li role="presentation"><a class="tal" href="#payments" aria-controls="messages" role="tab" data-toggle="tab">Payments</a></li>
				<li role="presentation"><a class="tal" href="#warranty" aria-controls="messages" role="tab" data-toggle="tab">Warranty</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="description">
					{{$product['description']}}
				</div>
				<div role="tabpanel" class="tab-pane " id="composition">
					{{$product['composition']}}
				</div>
				<div role="tabpanel" class="tab-pane " id="delivery_return">
					{{$product['delivery_return']}}
				</div>
				<div role="tabpanel" class="tab-pane " id="payments">
					{{$product['payments']}}
				</div>
				<div role="tabpanel" class="tab-pane " id="warranty">
					{{$product['warranty']}}
				</div>
			</div>
		  </div>
		</div>
	  </div>
	</div>


	<aside class="slide_basket" style="position:absolute;top:100px;display:none;right:0px;background:#fff;height:400px;padding:12px;z-index:125965545;">
		<div class="products-container">
			<i class="fa fa-close slide_basket_close" style="margin:6px;right:0px;cursor:pointer;"></i>
			<div id="productscroll" style="overflow-y:scroll;height:320px;">
			
			</div>
			<a href="{{url('/webshop/basket')}}" class="btn-process-order view-basket">View Basket</a>
		</div>
	</aside>
	
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<script>  
				$('#size').change(function(e) {
					var value = $(this).val();
					var q = $(this).attr("data-size");
					console.log(q)
					console.log('hhh')
				});

			 function click_func(id){
				 $(".sale_now").hide();
				 $(".test-"+id).show(); 
				 $.ajax({
					 url:'http://inviolateinfotech.com/laravel/webshop/wgbanner',
					 type:'GET',
					 data: 'catid='+id,
					 success: function(result) {
						$('.carousel-inner').html(result);
					 }
				 });
			 }
			</script>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
			<script type="text/javascript">
			  /*$(function() {
				$( "#slider-range" ).slider({
				  range: true,
				  min: 0,
				  max: 280,
				  values: [ 0, 280 ],
				  slide: function( event, ui ) {
					$( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
				$( "#amount1" ).val(ui.values[ 0 ]);
				$( "#amount2" ).val(ui.values[ 1 ]);
				  }
				});
				$( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
				 " - $" + $( "#slider-range" ).slider( "values", 1 ) );
			  });*/
			</script>
		
		
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<script>  
				function click_func(id){
				 $(".sale_now").hide();
				 $(".test-"+id).show(); 

				}
			</script>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
			
		  <script>
	

		 $(document).ready(function(){
			
			$('.slide_basket_close').on('click', function(){
				$('.slide_basket').animate({"width":"0px"}, "slow").css('display', 'none');
			});
			
			$('.cartform').on('submit', function(e){
				e.preventDefault();
				cart();
			});
			
			var baseurl = "{{url('webshop')}}";
			
			$('#detailinfo').on('click', function(){
				$('#dinfo').modal('show');
			});



			});
	
			$('#size').on('change', function(){
				var size = $(this).val();
				var id = $('#product').val();
				jQuery.ajax({
					method: "POST",
					data:{
						id:id,
						size:size,
					},
					dataType: "json",
					url: '{{url('/webshop/variablesize')}}',
					success: function (data) {
						if(data.product != ''){
							$('#product').val(data.product.id);
							$('#regular_price').html('<b>'+data.product.metas._regular_price+'€</b>');
							$('#sale_price').html('<b>'+data.product.metas._sale_price+'€</b>');
						}
						else{
							alert('No products available!');
							$('#size').val('');
						}
					},
					error: function (x,s,e) {
						
					}
				});
			});
	
			function cart(){
				
				var id = $('#product').val();
				var size = $('#size').val();
				var quantity = $('#quantity').val();
				jQuery.ajax({
					method: "POST",
					data:{
						id:id,
						size:size,
						quantity:quantity
					},
					dataType: "json",
					url: '{{url('/webshop/addtocart')}}',
					success: function (data) {
						$("#total_items").text(data.len);
						$('#productscroll').html(data.html);
						$('.slide_basket').animate({"width":"290px"}, "slow").css('display', 'block');
					},
					error: function (x,s,e) {
						
					}
				});
			}
	
	   function show_cart()
    {
      $.ajax({
      type:'post',
      url:'http://inviolateinfotech.com/laravel/webshop/addtocart',
      data:{
        showcart:"cart"
      },
      success:function(response) {
        document.getElementById("mycart").innerHTML=response;
        $("#mycart").slideToggle();
      }
     });

    }
		  </script>
	
	
@include('webshop.footer')
<!---<script>
function showdropdown(){
	$(".selectBox_quantity").append('<div class="append_option"><option value="1">1</option>  <option value="2">2</option><option value="3">3</option></div>');
	}
</script>	 ----> 
  </body>
</html>


