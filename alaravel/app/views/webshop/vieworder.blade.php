@extends('adashb')

@section('dash_content')

<h2 style="margin-bottom:26px;">
	<span >View Order</span>
	<span style="margin-left:20px;"><a href="{{url('webshop/orderpdf')}}/{{$order->id}}"><i class="fa fa-file-pdf-o"></i></a></span>
</h2>
<table class="table table-striped table-responsive">
	<tr class="form-group row">	
		<th><b>Product Name</b></th>
		<th><b>Quantity</b></th>
		<th><b>Price</b></th>
		<th><b>Total Price</b></th>
	</tr>
@foreach($order->products as $product)
	<tr class="form-group row">	
		<td >{{$product->name}}</td>
		<td >{{$product->quantity}}</td>
		<td >{{$product->price}}</td>
		<td >{{$product->quantity * $product->price}}</td>
	</tr>
@endforeach
</table>
<div class="container">

	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Order ID</b></label>
		<label class="col-md-6">{{$order->order_number}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Order Status</b></label>
		<label class="col-md-6">{{ucwords($order->type)}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Buyer Name</b></label>
		<label class="col-md-6">{{$order->name}} {{$order->surname}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Buyer Email</b></label>
		<label class="col-md-6">{{$order->email}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>NIF</b></label>
		<label class="col-md-6">{{$order->nif}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Province</b></label>
		<label class="col-md-6">{{$order->province}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Number</b></label>
		<label class="col-md-6">{{$order->number}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Telephone</b></label>
		<label class="col-md-6">{{$order->telephone}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping Address</b></label>
		<label class="col-md-6">{{$order->shipping_address}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping City</b></label>
		<label class="col-md-6">{{$order->shipping_city}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping State</b></label>
		<label class="col-md-6">{{$order->shipping_state}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping Postal Code</b></label>
		<label class="col-md-6">{{$order->shipping_postalcode}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Billing Address</b></label>
		<label class="col-md-6">{{$order->billing_address}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Billing City</b></label>
		<label class="col-md-6">{{$order->billing_city}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Billing State</b></label>
		<label class="col-md-6">{{$order->billing_state}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Billing Postal Code</b></label>
		<label class="col-md-6">{{$order->billing_postalcode}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping Method</b></label>
		<label class="col-md-6">{{$order->shipping_method}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Shipping Cost</b></label>
		<label class="col-md-6">{{$order->shipping_cost}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Total Cost</b></label>
		<label class="col-md-6">{{$order->total_cost}}</label>
	</div>
	
	<div class="form-group row">	
		<label class="col-xs-12 col-md-3" ><b>Order Notes</b></label>
		<label class="col-md-6">{{$order->order_notes}}</label>
	</div>
</div>
@stop
