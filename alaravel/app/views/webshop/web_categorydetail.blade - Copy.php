@include('webshop.header')
<style type="text/css">
	.size .basket_btn {
    color: #000;
    border: 1px solid #000;
    width: 100%;
    display: inline-block;
    text-align: center;
    padding: 13px 0;
    text-transform: uppercase;
    font-size: 11px;
    text-decoration: none;
}

.view-basket{
	    color: #000;
    border: 1px solid #000;
    width: 100%;
    display: inline-block;
    text-align: center;
    padding: 13px 0;
    text-transform: uppercase;
    font-size: 11px;
    text-decoration: none;
}
</style>

<section class="contact-form">
	<div class="product">
	  <div class="container-fluid">
		<div class="col-md-2 col-sm-2">
			 @include('webshop.left_panel')
		</div>

		<?php 
           $url = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
           $pieces = explode("/", $url );
           $count= count($pieces);
          
		?>
		<div class="col-md-6 col-sm-6">
			<div class="more_quilt">
				<p><a href="<?php echo $pieces[0].'/'.$pieces[1].'/'.$pieces[2].'/'.$pieces[3].'/'.$pieces[4].'/'.$pieces[5].'/'.$pieces[6]; ?>"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span>MORE <?php echo $pieces[$count-2]; ?></a></p>
			</div>
			<div class="full_img">
				 <img src="{{url($product['img'])}}" class="img-responsive" alt="{{url($product['name'])}}">
			</div>

			@foreach($product_images as $pm)
			<div class="full_img">
				 <img src="{{url($pm->imagename)}}" class="img-responsive" alt="{{$pm->name}}">
			</div>
			@endforeach
			
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="next_info">
					<a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			</div>
			<div class="product_info">		
				<h2>{{ $product['name']}}</h2>
				@if($product['type'] == 'variable')
				<h5 id="regular_price">${{number_format($product['price_min'],2)}} - ${{number_format($product['price_max'],2)}}</h5>
				<!-- <p id="sale_price" style="text-decoration: line-through;margin: 8px 0 8px;color:red;"><b><?php // echo $product['price_min_sale'];?>€</b> - <b><?php // echo $product['price_max_sale'];?>€</b> </p> -->
				@else
				 <h5>${{number_format($product['price'],2)}}</h5>
				@endif
				<!-- <h5> £89.99 - £199.99</h5> -->
				<p>ref. {{$product['meta']['_ref']}} </p>
				<form class="size form cartform">
					 <input type="hidden" name="product" id="product" value="{{$product['product_id']}}" />
					<ul>
						<li>
							@if(isset($attrs['size']))	
							<div class="form-group">
								<select class="form-control required" name="size" id="size" required>
											<option value="">choose a size</option>
									   @foreach($attrs['size']->attr_values as $size)
											<option value="{{$size->name}}">{{ucwords($size->name)}}</option>
										@endforeach
								</select>
							</div>
							@endif
						</li>
						<li>
							<div class="form-group">
								<label>QUANTITY<span>*</span></label>
								<select class="form-control" name="quantity" id="quantity">
											<option value="">Quantity</option>
											@if(isset($product['quantity']))
											@if($product['quantity'] > 10)
												@for($i=1;$i<=10;$i++)
													<option value="{{$i}}">{{$i}}</option>
												@endfor
											@else
												@for($i=1;$i<=$product['quantity'];$i++)
													<option value="{{$i}}">{{$i}}</option>
												@endfor
											@endif
											@endif
								</select>
							</div>
						</li>
					</ul>
					<div class="">
						<input type="submit" class="basket_btn" value="add to basket">
					</div>
				</form>
				<div class="info_plus">
					<a href="#"><span>+</span>info</a>
					<!-- <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a>	 -->			
				</div>
				<h4>Washed-effect linen quilt. Featuring a plain design with ruffled lace trims along the edges.</h4>

	 
	    <div class="swiper" id="feedback">				
				<div class="flask_cntr">
				<h3>Our selection</h3>
					<div class="owl-carousel" id="testi_slide">
						@foreach($allProduct as $row)
							<div class="sec_main">
								
				                   <img src="{{url($row['product']['img'])}}" class="img-responsive" alt="{{$row['product']['name']}}">
			                    
								<p>{{$row['product']['name']}}</p>							
								<p> ${{$row['product']['price']}}</p>
								<!-- <form class="size_slider"> -->
                                <form class="size_slider size form ourSelectioncartform" id="{{$row['product']['product_id']}}">
                                    <ul>
										<li>
				                            @if(isset($row['attrs']['size']))	
											<div class="form-group">
												<select class="form-control required" name="size" id="size_our{{$row['product']['product_id']}}" required>
															<option value="">choose a size</option>
													   @foreach($row['attrs']['size']->attr_values as $size)
															<option value="{{$size->name}}">{{ucwords($size->name)}}</option>
														@endforeach
												</select>
											</div>
											@endif
										</li>
										<li>
											<div class="form-group">
												<label>QUANTITY<span>*</span></label>
												<select class="form-control" name="quantity" id="quantity_our{{$row['product']['product_id']}}">	
													@if(isset($row['product']['quantity']))
													@if($row['product']['quantity'] > 10)
														@for($i=1;$i<=10;$i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													@else
														@for($i=1;$i<=$row['product']['quantity'];$i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													@endif
													@endif
												</select>
											</div>
										</li>
									</ul>
									<input type="hidden" name="product" id="product_our{{$row['product']['product_id']}}" value="{{$row['product']['product_id']}}" />
									<div class="">
										<input type="submit" class="basket_btn" value="add to basket">
										<!-- <a href="#">add to basket</a> -->
									</div>
								</form>
							</div>
                        @endforeach
					
					</div>
				</div>				
	    </div>


	    <div class="swiper" id="feedback">				
				<div class="flask_cntr">
				<h3>YOU MIGHT BE INTERESTED</h3>
					<div class="owl-carousel" id="testi_slide_1">
						@foreach($allProduct as $row)
							<div class="sec_main">
								
				                   <img src="{{url($row['product']['img'])}}" class="img-responsive" alt="{{$row['product']['name']}}">
			                    
								<p>{{$row['product']['name']}}</p>							
								<p> ${{$row['product']['price']}}</p>
								<!-- <form class="size_slider"> -->
                                <form class="size_slider size form ourSelectioncartform" id="{{$row['product']['product_id']}}">
                                    <ul>
										<li>
				                            @if(isset($row['attrs']['size']))	
											<div class="form-group">
												<select class="form-control required" name="size" id="size_our{{$row['product']['product_id']}}" required>
															<option value="">choose a size</option>
													   @foreach($row['attrs']['size']->attr_values as $size)
															<option value="{{$size->name}}">{{ucwords($size->name)}}</option>
														@endforeach
												</select>
											</div>
											@endif
										</li>
										<li>
											<div class="form-group">
												<label>QUANTITY<span>*</span></label>
												<select class="form-control" name="quantity" id="quantity_our{{$row['product']['product_id']}}">	
													@if(isset($row['product']['quantity']))
													@if($row['product']['quantity'] > 10)
														@for($i=1;$i<=10;$i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													@else
														@for($i=1;$i<=$row['product']['quantity'];$i++)
															<option value="{{$i}}">{{$i}}</option>
														@endfor
													@endif
													@endif
												</select>
											</div>
										</li>
									</ul>
									<input type="hidden" name="product" id="product_our{{$row['product']['product_id']}}" value="{{$row['product']['product_id']}}" />
									<div class="">
										<input type="submit" class="basket_btn" value="add to basket">
										<!-- <a href="#">add to basket</a> -->
									</div>
								</form>
							</div>
                        @endforeach
					
					</div>
				</div>				
	    </div>
        
        
	  
	   <aside class="slide_basket" style="position:absolute;display:none;right:0px;background:#fff;height:400px;padding:12px;z-index:125965545;">
			<div class="products-container">
				<i class="fa fa-close slide_basket_close" style="margin:6px;right:0px;cursor:pointer;"></i>
				<div id="productscroll" style="overflow-y:scroll;height:320px;">
				
				</div>
				<a href="{{url('/webshop/basket')}}" class="btn-process-order view-basket">View Basket</a>
			</div>
		</aside>


	</div>
	</div>
	</div>
</section>
@include('webshop.footer')
<script src="{{url('assets/frontend/script/productDetail.js')}}"></script>
<script src="{{url('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
	var baseurl = "{{url('webshop')}}";

	 //testimonial slide
var owl = $('#testi_slide');

$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[5000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})

owl.owlCarousel({
    items:4,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	dots:true,

	navigation:true,
	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    loop:true,
    margin:15,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
})

var owls = $('#testi_slide_1');

$('.play').on('click',function(){
    owls.trigger('play.owl.autoplay',[5000])
})
$('.stop').on('click',function(){
    owls.trigger('stop.owl.autoplay')
})

owls.owlCarousel({
    items:4,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	dots:true,

	navigation:true,
	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    loop:true,
    margin:15,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
})

$(function(){
  //SyntaxHighlighter.all();
});
$(window).load(function(){
  $('.flexslider').flexslider({
    animation: "fade",
    start: function(slider){
      $('body').removeClass('loading');
    }
  });
});


$(document).ready(function()
{
	
	// Closes the sidebar menu on menu-close button click event
	$("#menu-close").click(function(e)					
	{
															
		$("#sidebar-wrapper").toggleClass("active");			
		e.preventDefault(); 									
		
		/*!
		======================= Notes ===============================
		* see: .sidebar-wrapper.active in: style.css
		==================== END Notes ==============================
		*/
	});													

	// Open the Sidebar-wrapper on Hover
	$("#menu-toggle").hover(function(e)							
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();									
	});

	$("#menu-toggle").bind('click',function(e)					
	{
		$("#sidebar-wrapper").toggleClass("active",true);		
		e.preventDefault();								
	});														
	$('#sidebar-wrapper').mouseleave(function(e)				
																
	{
		/*! .toggleClass( className, state ) */
		$('#sidebar-wrapper').toggleClass('active',false);		
																
													
		e.stopPropagation();									
																
																
		e.preventDefault();										
																
	});
});


  $(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});
//});


$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });

});


$(function () {

	"use strict";
	var wind = $(window);

	/*==================================
	ScrollIt Setup
	====================================*/

	

	/*==================================
	Navbar Scrolling Setup
	====================================*/
	
	wind.on("scroll", function () { 
		var swidth = (screen.width);
		var bodyScroll = wind.scrollTop(),
		header = $(".header")
		if(swidth >1024){
			if (bodyScroll > 0) {
				
				header.addClass("fixed-top");
						
				
				
			} 
			else {
				header.removeClass("fixed-top");
								
			}
		}
	});
	
	

});


</script>
