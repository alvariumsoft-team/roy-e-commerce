@include('webshop.header')

<style type="text/css">
.sale_nowfg{
	cursor:pointer;
}
.error{
	color:red;
}
.modal-header{
	border-bottom:none!important;
	padding:10px !important;
}
.btn.btn-primary.black-background.white {
  background: #ffff none repeat scroll 0 0;
  border: 1px solid #000;
  border-radius: 3px;
  color: #000;
}
.control-group.check {
  font-size: 14px !important;
  font-weight: initial;
  text-decoration: none;
}
.control-group.check >a{
	color:#ccc;
}
.text-line {
    background-color: transparent !important;
    color: #eeeeee !important;
    outline: none !important;
    outline-style: none !important;
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #000 !important;
    padding: 3px 10px!important;
	border-radius:0px !important;
	box-shadow:none !important;
	width:47%!important;
	
}
@media only screen  and (max-width: 360px){
label{
	display:flex;
}
}
</style>

    <section class="side_bar">
      <div class="container-fluid">
        <div class="row">
		
         @include('webshop.sidebar')
		
          <div class="col-sm-9 col-lg-9">
            <section class="slider">
              <div class="">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">

					<div class="item active"> 
						<img src="{{url('assets/img/')}}/1089013.jpg" alt="Los Angeles" style="width:100%;" class="img_acr"> 
					</div>

                 </div>
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a>
                </div>
              </div>
            </section>
			
            <!---end class slider here--->
 <form method="POST" action="{{url('webshop/subscribe')}}">					
				<div class="row">
				<div class="col-lg-12">
					<div class="footer-spot"></div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-1">
							<div class="group">
								<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;" class="labletxt">Join our newsletter - Enter your email address*</label>
							</div>
				 <!--<div class="group" style="float: left;width: 100%; text-align:center;">
								<input class="check-content" type="checkbox" >
								<label style="margin-left: 20px;" class="check-button">I have read and accept the Privacy policy.</label>
							</div>-->
						<div class="col-lg-12  text-center " >
				  <input  type="submit" class="button" style=" float:none;" name="subscribe" value="SUBSCRIBE ME">
						</div>
						</div>
				</div>
				</div>	   	
			</form> 
</div>
</div>
</div></div>
</section>



@include('webshop.footer')



<div class="container">
  <div class="row">
<div id="my-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><b>NEWSLETTER SUBSCRIPTION</b></h4><br />
				<h6 class="modal-title">Subscribe to our newsletter and receive e-mails all the latest Zara Home products and offers</h6>
            </div>
			
            <div class="modal-body">
			<form name=""  method="POST">
			<div class="control-group">
			<label for ="control-label">E-mail<span class="error_req error">*</span></label>
			
              <div class="controls"> <input type="text" name="email"  id="eml_fld" class="form-control text-line" ></div>
			   </div>
			   <br>
		<div class="control-group check">
               <input type="checkbox" name="checkbox"  value="1" > I have read and accept the <a href="#" class="policy">Privacy Policy</a>
			  </div> 
			  
			  <br><br>
			  <div class="control-group">
			  <input type="submit" name="submit" class="btn btn-primary black-background white" Value="SUBSCRIBE ME">
			  </div>
			   </form>
            </div>
        </div>
    </div> 
</div>

	    </div> 
</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
jQuery(document).ready(function(){
	 var visited = jQuery.cookie("visited");
	  if (visited == null) {
			jQuery('#my-modal').modal('show');
			var date = new Date();
			var minutes = 15;
			date.setTime(date.getTime() + (minutes * 60 * 1000));
			jQuery.cookie('visited', 'yes', { expires: date, path: '/' });
	  }
});

</script>

  </body>

</html>
