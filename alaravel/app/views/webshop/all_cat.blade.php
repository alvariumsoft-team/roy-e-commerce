@include('webshop.header')
<section class="contact-form">
  <div class="product">
  <div class="container-fluid">
    <div class="col-md-2 col-sm-2">
      @include('webshop.left_panel')
    </div>
    <div class="col-md-10 col-sm-10">
      <div class="product_inner">
		<div class="row">
           @foreach($subsupcats as $subcat)
			<div class="col-md-3 col-sm-3 no-padd">
				<div class="product_main">
				  <a href="{{url('/webshop/category')}}/{{$subcat->url}}">
					@if(isset($subcat->big_img))
						<img src="{{url('public/webshop/categories/big/')}}/{{$subcat->big_img}}" class="img-responsive">				
						<h2>{{$subcat->name}}</h2>
					@elseif(isset($subcat->small_img))
						<img src="{{url('public/webshop/categories/small/')}}/{{$subcat->small_img}}" class="img-responsive">				
						<h2>{{$subcat->name}}</h2>
					@elseif(isset($subcat->mini_img))
						<img src="{{url('public/webshop/categories/mini/')}}/{{$subcat->mini_img}}" class="img-responsive">				
						<h2>{{$subcat->name}}</h2>
					@elseif(isset($subcat->menu_img))
						<img src="{{url('public/webshop/categories/menu/')}}/{{$subcat->menu_img}}" class="img-responsive">				
						<h2>{{$subcat->name}}</h2>
					@elseif(isset($subcat->header_img))
						<img src="{{url('public/webshop/categories/header/')}}/{{$subcat->header_img}}" class="img-responsive">				
						<h2>{{$subcat->name}}</h2>
					@else
						<img src="{{url('public/webshop/categories/sample_demo.jpg')}}" class="img-responsive">	<h2>{{$subcat->name}}</h2>
					@endif
                  </a>					 
		        </div>
			</div>
		   @endforeach
		</div>
	</div>
  </div>
  </div>
</section>
@include('webshop.footer')