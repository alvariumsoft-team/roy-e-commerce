@extends('adashb')
@section('dash_content')



<h2>Listings</h2>

<p>View all listings.</p>
<h2></h2>

<form action="" method="post">


<!--<div class="row">

<div class="col-md-2">

<div class="form-group">{{Form::select('Bulk action',array('Bulk Actions','edit'=>'Edit','delete'=>'Move To Trash'),'',array('class'=>'form-control','id'=>'bulk_action'))}}</div>

</div>

<div class="col-md-1">

<div class="form-group">{{Form::button('apply',array('class'=>'btn','id'=>'bulk_action_button'))}}</div>


</div>

</div> -->

<div class="row">

<div class="col-md-9" style="overflow-x:auto;">

<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>Image</th>
<th>Name</th>
<th>Location</th>
<th>Price</th>
<th>Beds</th>
<th>Baths</th>
<th>Agent</th>
<th>Status</th>
<th>Listed</th>

</tr>
</thead>

<tbody>

@foreach($lists as $list)

<tr> 

<td>{{Form::checkbox('listing_action','','',array('class'=>'listing_action'))}}</td>
<td><a href="{{URL::to('listingsite/'.$list['id'])}}" target="_blank"><img src="{{$list['img']}}" height="100px" width="120px" ></img></a></td>
<td>{{$list['name']}}</td>
<td>{{$list['location']}}</td>
<td>{{$list['price']}}</td>
<td>{{$list['beds']}}</td>
<td>{{$list['baths']}}</td>
<td>{{$list['agent']}}</td>
<td>{{$list['status']}}</td>
<td>Published on:{{$list['created_at']}}</td>

</tr>

@endforeach

</tbody>

</table>

</div>

</div>


</form>





{{ $lists->links() }}











<div class="hidden">

<form action="" method="post">

<div class="form-group">{{Form::select('list_type',array('Listing Type','apartment'=>'Apartment','land'=>'Land','home'=>'Home'),'',array('class'=>'form-control'))}}</div>


<div class="form-group">{{Form::text('list_name','',array('class'=>'form-control','placeholder'=>'Listing Name'))}}</div>


<div class="form-group">{{Form::textarea('list_description','',array('class'=>'form-control','placeholder'=>'Listing Description'))}}</div>

<div class="form-group">{{Form::text('list_area','',array('class'=>'form-control','placeholder'=>'Listing Area (in sq.ft.)'))}}</div>

<div class="form-group">{{Form::text('list_price','',array('class'=>'form-control','placeholder'=>'Listing Price'))}}</div>

<div class="form-group">{{Form::select('list_status',array('sale'=>'For Sale','rent'=>'For Rent'),'',array('class'=>'form-control'))}}</div>

<div class="form-group">{{Form::text('list_beds','',array('class'=>'form-control','placeholder'=>'Beds'))}}</div>

<div class="form-group">{{Form::text('list_baths','',array('class'=>'form-control','placeholder'=>'Baths'))}}</div>


<div>
<div class="form-group">{{Form::text('list_phone','',array('class'=>'form-control','placeholder'=>'Phone Number'))}}</div>
<div class="form-group">{{Form::text('list_email','',array('class'=>'form-control','placeholder'=>'E-mail'))}}</div>
<div class="form-group">{{Form::text('list_website','',array('class'=>'form-control','placeholder'=>'Your Website'))}}</div>
<div class="form-group">{{Form::textarea('list_address','',array('class'=>'form-control','placeholder'=>'Your Address'))}}</div>
<div class="form-group">{{Form::text('country','',array('class'=>'form-control','placeholder'=>'Your Country'))}}</div>
<div class="form-group">{{Form::text('state','',array('class'=>'form-control','placeholder'=>'Your State'))}}</div>
<div class="form-group">{{Form::text('city','',array('class'=>'form-control','placeholder'=>'Your City'))}}</div>
</div>

<div class="form-group">{{Form::submit('Submit Project',array('class'=>'btn btn-large btn-primary btn-block'))}}</div>

</form>

</div>



@stop
