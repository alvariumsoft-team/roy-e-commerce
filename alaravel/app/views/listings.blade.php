@extends('main')
@section('content')

<div class="list-header">

<div class="container"><div><h4>All Listing</h4></div></div>

</div>

<div class="about">

<div class="container">

		@foreach($listings as $listing)
		 
		 <div class="list-all">
		 
             <div class="col-lg-4 col-sm-6">
			 <div class="list-main">
				<a href="listingsite/{{$listing['id']}}">{{HTML::image($listing['img'],'alt',array('height'=>'250px','width'=>'100%'))}}</a>
                 <div class=" center" style="padding:10px;">
                    <a class="list-link" href="listingsite/{{$listing['id']}}">{{ucwords($listing['name'])}}</a>
					<p class="list-desc">{{$listing['location'] !== "" ?  $listing['location'] : 'Location Not Available'}}</p>

                 </div>
					@if(Auth::check())
					<div class="list-price center"><h4 style="margin:10px">${{$listing['price']}}</h4></div>
					<div class="list-head">

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Beds</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['beds']}}</div>

					</div>
					</div>

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Baths</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['baths']}}</div>

					</div>
					</div>

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Sq. Ft.</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['area']}}</div>

					</div>
					</div>


					</div>
					@endif
			  </div>
             </div>
			 
			 </div>
			 
		@endforeach


</div>

<div style="text-align:center;padding:10px;padding-top:50px;padding-bottom:50px;">{{$listings->links()}}</div>

</div>


@stop
