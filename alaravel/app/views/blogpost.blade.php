@extends('blog_main')

@section('blog_content')
				
				<div class="post full-post">
					<div class="post-heading"><h2>{{$post->post_title}}</h2></div>
					<div class="post-pad author-info">
					<span class="author-image"><img src=""></span>
					<span class="author">by <b>{{$post->post_author}}</b></span>
					<span class="post-creation-date"><i>published on {{$post->created_at}}</i></span>
					</div>
<div class="post-heading img-responsive"><img style="height:200px; width:100%;"src="{{URL::to($post->featured_image)}}"></img></div>
					<div class="post-pad post-content">{{$post->post_content}}</div>
					<div class="post-pad blog-info"></div>
				</div>
				
				<div class="post-comment" style="width:100%;margin:auto;background:#fff;padding:1% 2%;">
					<div class="row">
						<div class="col-md-12">
							<form action="{{URL::to('comment')}}" method="post">
								<div class="form-group" id="comment-form-group">
									
									<?php $comment_count = count($comments); /*echo 'pre'; print_r($comments);echo '</div>';*/?>
									
									<label><b>Comments &nbsp;( {{$comment_count}} )</b></label>
									<textarea class="form-control" name="comment" rows="5" placeholder="Comment here..." ></textarea>
									<input type="hidden" name="post_id" value="{{$post->post_id}}" />
									<span class="clearfix" style="padding:20px;"><input class="pull-right btn btn-primary" type="submit" name="submit" value="Post Comment..." /></span>
								</div>
							</form>
						</div>
					</div>
					
					<div class="comments">
						
					<style type="text/css">
						.blog-comment-reply{padding:6px 8px;border-radius:20px;background:#ccc;color:#f9f9f9;font-size:12px;}
						.blog-comment-reply:hover{color:#424242;cursor:pointer;}
						.comment-child{display:none;}
					</style>
					
					<script type="text/javascript">
						$(document).ready(function(){
							$('.blog-comment-reply').on('click', function(){
								var id = $(this).parents('.comment').attr('data-id');
								$('.comment-child').css('display', 'none');
								$(this).parents('.comment').find('.comment-child').css('display', 'block');
								 //$('#comment-form-group').append('<input type="hidden" name="comment_parent" value="'+id+'" />');
								//$('textarea[name="comment"]').attr('placeholder', 'Reply to the comment here...');
								//window.location.hash = "comment-form-group";
							});
						});
					</script>
					
						@foreach($comments as $comment)
							
							<div class="comment" data-id="{{$comment['uid']}}">
								<div class="row">
									<div class="col-md-1 user-img"><img src="{{URL::to($comment['comment']->comment_author_img)}}"></img></div>
									<div class="col-md-5 comment-dt"><b>{{$comment['comment']->created_on}}</b></div>
									<div class="col-md-6 comment-dt" style="text-align:right;">
									
										<span style="padding-left:4px;">
											<span class="blog-comment-reply" title="Flag comment" ><i class="fa fa-flag"></i></span>
										</span>
										<span style="padding-left:4px;">
											<span class="blog-comment-reply" title="Reply" ><i class="fa fa-reply"></i></span>
										</span>
										
									</div>
									<div class="col-md-1"></div>
									<div class="col-md-11 comment-content">{{$comment['comment']->comment_content}}</div>
									
									<div class="col-md-12">
										@foreach($comment['child'] as $childrens)
											@if(!empty($childrens))
											<div class="comment-childrens" style="margin-left:5%">										
												<div class="row">
													
													<div class="col-md-1 user-img"><img src="{{URL::to($comment['comment']->comment_author_img)}}"></img></div>
													<div class="col-md-5 comment-dt"><b>{{$comment['comment']->created_on}}</b></div>
													<div class="col-md-6 comment-dt" style="text-align:right;">
													
														<span style="padding-left:4px;">
															<span class="blog-comment-reply" title="Flag comment" ><i class="fa fa-flag"></i></span>
														</span>
														
													</div>
													<div class="col-md-1"></div>
													<div class="col-md-11 comment-content">{{$childrens->comment_content}}</div>
					
												</div>
											</div>
											@endif	
										@endforeach					
									</div>
									
									<div class="col-md-12 comment-child" style="padding-top:50px;">
										<form action="{{URL::to('comment')}}" method="post">
											<div class="form-group">
												<textarea class="form-control" name="comment" rows="5" placeholder="Reply here..." ></textarea>
												<input type="hidden" name="post_id" value="{{$post->post_id}}" />
												<input type="hidden" name="comment_parent" value="{{$comment['uid']}}" />
												<span class="clearfix" style="padding:20px;"><input class="pull-right btn btn-primary" type="submit" name="submit" value="Reply" /></span>
											</div>
										</form>
									</div>
									
								</div>
							</div>
							
						@endforeach
						
					</div>
					
				</div>
				
				
@stop



