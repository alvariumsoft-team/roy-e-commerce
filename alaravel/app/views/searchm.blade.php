<!DOCTYPE HTML>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Global Property Cowboys &#8211; We are a company based in Heiloo, a small town near Amsterdam, The Netherlands , Western Europe.</title>

	
	
{{HTML::style('packages/realestate/css/site.css')}}
{{HTML::style('packages/realestate/css/media.css')}}
{{HTML::style('packages/realestate/css/bootstrap.css')}}
{{HTML::style('packages/realestate/css/bootstrap-theme.css')}}
{{HTML::style('packages/realestate/css/animate.min.css')}}

{{HTML::script('packages/realestate/js/jquery.js')}}


<style type="text/css">

.header-top{background:#29333d; color:#dedede;padding:15px;}
.left{float:left;}
.right{float:right;}
fluid{width:100%;}
.dashboard-left{background:#29333d; color:#dedede;padding:15px;}

a{text-decoration:none; color:#dedede;text-size:20px;}

.project-admin{margin-top:20px;}

#udetails-db{display:none;}

.udetails{padding:20px;}


</style>

</head>

<body>

<header class="header-top">

<div class="left"><a href="/"><img src="/packages/realestate/images/logo.png" /></a></div>
@if(Auth::check())<div class="right"><a href="/logout">LOGOUT</a></div>@endif
<div class="clearfix"></div>

</header>

@if(Session::has('message'))
<div class="container">

<div class="container">

<p class="well">{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>

</div>
@endif

<div style="margin-top:40px;margin-bottom:40px;"></div>

<div class="container-fluid">

@yield('search_content')

</div>

</body>

</html>