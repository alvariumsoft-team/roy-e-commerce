@extends('adashb')
@section('dash_content')

<h2>Add New Listing</h2>

<p>Create a brand new listing and add them to this site.</p>

<form action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-md-3"><b>Listing Type</b></label>
<label class="col-md-6">{{Form::select('list_type',array('Listing Type','apartment'=>'Apartment','land'=>'Land','home'=>'Home'),'',array('class'=>'form-control','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Name</b></label>
<label class="col-md-6">{{Form::text('list_name','',array('class'=>'form-control','placeholder'=>'Listing Name','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Description</b></label>
<label class="col-md-6">{{Form::textarea('list_description','',array('class'=>'form-control','placeholder'=>'Listing Description','required'=>'true'))}}</label>
</div>

<div class="form-group row">	
<label class="col-xs-12 col-md-3" ><b>Listing Image:</b></label>
<label class="col-xs-12 col-md-6"  for="ifile-id2" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;"><span id='ifile-labe' class='ifile-label'>Upload Image</span>{{Form::file('file',array('class'=>'inputfile inputfile-1 ifile-id','required'=>'true','id'=>'ifile-id2','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Area</b></label>
<label class="col-md-6">{{Form::text('list_area','',array('class'=>'form-control','placeholder'=>'Listing Area (in sq.ft.)','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Price</b></label>
<label class="col-md-6">{{Form::text('list_price','',array('class'=>'form-control','placeholder'=>'Listing Price','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Price Per sq. ft.</b></label>
<label class="col-md-6">{{Form::text('price_per_sqft','',array('class'=>'form-control','placeholder'=>'Price Per sq. ft.','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Status</b></label>
<label class="col-md-6">{{Form::select('list_status',array('sale'=>'For Sale','rent'=>'For Rent'),'',array('class'=>'form-control','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Status Type</b></label>
<label class="col-md-6">{{Form::select('status_type',array('sale'=>'Resale','rent'=>'New'),'',array('class'=>'form-control','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Age Of Property</b></label>
<label class="col-md-6"><input type="date" class="form-control" name="age_of_property" placeholder="Launch Date"/></label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Beds</b></label>
<label class="col-md-6">{{Form::text('list_beds','',array('class'=>'form-control','placeholder'=>'Beds','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Baths</b></label>
<label class="col-md-6">{{Form::text('list_baths','',array('class'=>'form-control','placeholder'=>'Baths','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Presence Of</b></label>
<label class="col-md-6">
<div class="row">
	
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="balcony" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Balcony</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="bathtub" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Bathtub</span></label>
	<!--<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="commercial_property" /></span><span class="asrch-checkbox-info">Commercial property</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="monumental_building" /></span><span class="asrch-checkbox-info">Monumental building</span></label>
	--><label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="ch_builder" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;CH boiler</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fire_place" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Fire place</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="roof_terrace" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Roof terrace</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="sauna" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Sauna</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="renewable_energy" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Renewable energy</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="shed_storage" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Shed / storage</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garage" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Garage</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="steam_cabin" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Steam cabin</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="jacuzzi" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Jacuzzi</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garden" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Garden</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fixer_upper" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Fixer-upper</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="swimming_pool" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Swimming pool</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="elevator" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Elevator</span></label>

</div>
</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Map Location</b></label>
<label class="col-md-3">{{Form::text('latitude','',array('class'=>'form-control','placeholder'=>'Latitude','required'=>'true'))}}</label>
<label class="col-md-3">{{Form::text('longitude','',array('class'=>'form-control','placeholder'=>'Longitude','required'=>'true'))}}</label>
</div>

<div>
<div class="form-group row">
<label class="col-md-3"><b>Phone</b></label>
<label class="col-md-6">{{Form::text('list_phone','',array('class'=>'form-control','placeholder'=>'Phone Number','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>Email</b></label>
<label class="col-md-6">{{Form::text('list_email','',array('class'=>'form-control','placeholder'=>'E-mail','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>Website</b></label>
<label class="col-md-6">{{Form::text('list_website','',array('class'=>'form-control','placeholder'=>'Your Website','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>Address</b></label>
<label class="col-md-6">{{Form::textarea('list_address','',array('class'=>'form-control','placeholder'=>'Your Address','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>Country</b></label>
<label class="col-md-6">{{Form::text('list_country','',array('class'=>'form-control','placeholder'=>'Your Country','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>State</b></label>
<label class="col-md-6">{{Form::text('list_state','',array('class'=>'form-control','placeholder'=>'Your State','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-md-3"><b>City</b></label>
<label class="col-md-6">{{Form::text('list_city','',array('class'=>'form-control','placeholder'=>'Your City','required'=>'true'))}}</label>
</div>

</div>

<div class="form-group">{{Form::submit('Add Listing',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
  $( function() {
    $( 'input[type="date"]' ).datepicker();
  } );
  </script>

@stop
