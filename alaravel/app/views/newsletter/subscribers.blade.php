@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>List Subscribers</h2>


<form class="row" action="{{url('/newsletter/send')}}" method="GET">
	<div class="col-md-3">
		<select name="newsletter" class="form-control newsletter">
			<option value="">Select Newsletter</option>
			@foreach($newsletters as $newsletter)
			<option value="{{$newsletter->id}}" @if((isset($curr_newsletter) && ($curr_newsletter == $newsletter->id))) selected @endif>{{$newsletter->name}}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-3">
		<button class="btn btn-default" type="submit">Send</button> 
	</div>
	<div class="col-md-3">
	
	</div>
	<div class="col-md-3">
	
	</div>
</form>


<table class="table">

<thead>


<!--<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>-->
<th>Newsletter</th>
<th>Subscriber</th>
<!--<th>Actions</th>-->

</tr>
</thead>

<tbody id="subscribers">

@if(isset($subscribers[0]))
@foreach($subscribers as $subscriber)

<tr>
<!--<td>{{Form::checkbox('listing_action','','')}}</td>-->

<td>{{$subscriber->newsletter}}</td>
<td>{{$subscriber->email}}</td>
<td>
	<!--<a href="{{url('/subscriber/edit')}}/{{$subscriber->id}}" style="font-weight:bold;margin-right:5px;" title="Edit"><i class="fa fa-edit"></i></a>
	<a href="{{url('/subscriber/view')}}/{{$subscriber->id}}" style="font-weight:bold;margin-right:5px;" title="View"><i class="fa fa-eye"></i></a>-->
	<a href="{{url('/newsletter/subscriber/delete')}}/{{$subscriber->id}}" style="font-weight:bold;margin-right:5px;" title="Delete" onClick="return confirm('Are you sure you want to delete this subscriber?');"><i class="fa fa-close"></i></a>
</td>
</tr>
 
@endforeach
@else
<tr style="text-align:center;"><td colspan="2">No Records Found.<td></tr>
@endif
</tbody>

</table>


</div>


<script type="text/javascript">

var sub_url = "{{url('newsletter/get_newsletter_subscribers')}}";

jQuery(document).ready(function(){
	jQuery('.newsletter').on('change', function(){
		var n = jQuery(this).val(); 
		if(n != ''){
			var url = sub_url+'/'+n;
			jQuery.ajax({
				method: "GET",
				dataType: "json",
				url: url,
				success:function(response) {
					if((response.success != undefined) && (response.success == true)){
						var html = '';
						var i = -1;
						for(i in response.subscribers){
							html += '<tr>';
							html += '<td>'+response.subscribers[i].newsletter+'</td>';
							html += '<td>'+response.subscribers[i].email+'</td>';
							html += '<td><a href="'+"{{url('/newsletter/subscriber/delete')}}"+'/'+response.subscribers[i].id+'" style="font-weight:bold;margin-right:5px;" title="Delete" onClick="'+"return confirm('Are you sure you want to delete this subscriber?')"+';"><i class="fa fa-close"></i></a>';
							html += '</tr>';
						}
						if(i<0){
							$('#subscribers').html('<tr style="text-align:center;"><td colspan="2">'+'No Records Found.'+'<td></tr>');
						}
						else{
							$('#subscribers').html(html);
						}
					}
					else{
						$('#subscribers').html('<tr style="text-align:center;"><td colspan="2">'+'No Records Found.'+'<td></tr>');
					}
				}
			  });
		  }
	});
});

</script>


@stop

