@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>View Newsletter</h2>


<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Name :</b></label>
<label class="col-xs-12 col-md-8">
	<input name="name" type="text" class="form-control" placeholder="Email Template Name" value="@if(isset($name)) {{$name}} @endif" required/>
</label>
</div>

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Newsletter :</b></label>
<label class="col-xs-12 col-md-8">
	@if(isset($content)){{$content}}@else Unable to find email template.@endif
</label>
</div>

<div class="form-group">



</div>




</div>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector : ".mceEditor", height:270,   plugins: "link"});</script>

@stop
