@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>List Newsletter</h2>


<table class="table">

<thead>
<tr>

<!--<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>-->

<th>Name</th>
<th>Actions</th>

</tr>
</thead>

<tbody>

@foreach($templates as $template)

<tr>
<!--<td>{{Form::checkbox('listing_action','','')}}</td>-->

<td>{{$template->name}}</td>
<td>
	<a href="{{url('/newsletter/edit')}}/{{$template->id}}" style="font-weight:bold;margin-right:5px;" title="Edit"><i class="fa fa-edit"></i></a>
	<a href="{{url('/newsletter/view')}}/{{$template->id}}" style="font-weight:bold;margin-right:5px;" title="View"><i class="fa fa-eye"></i></a>
	<a href="{{url('/newsletter/delete')}}/{{$template->id}}" style="font-weight:bold;margin-right:5px;" title="Delete" onClick="return confirm('Are you sure you want to delete this newsletter?');"><i class="fa fa-close"></i></a>
</td>
</tr>
 
@endforeach

</tbody>

</table>


</div>



@stop

