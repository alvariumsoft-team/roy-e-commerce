@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add Privacy Policy</h2>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Privacy Policy:</b></label>
<label class="col-xs-12 col-md-8">
	<textarea name="policy" class="form-control mceEditor">{{$privacy}}</textarea>
</label>
</div>

<div class="form-group">

{{Form::submit('Submit',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector : ".mceEditor", height:270,   plugins: "link"});</script>

@stop
