@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>List Pages</h2>


<table class="table">

<thead>
<tr>

<!--<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>-->

<th>Name</th>
<th>Actions</th>

</tr>
</thead>

<tbody>

@foreach($pages as $page)

<tr>
<!--<td>{{Form::checkbox('listing_action','','')}}</td>-->

<td>{{$page->name}}</td>
<td>
	<a href="{{url('settings/editpage')}}/{{$page->id}}" style="font-weight:bold;margin-right:5px;" title="Edit"><i class="fa fa-edit"></i></a>
	<a href="{{url('settings/deletepage')}}/{{$page->id}}" style="font-weight:bold;margin-right:5px;" title="Delete" onClick="return confirm('Are you sure you want to delete this page?');"><i class="fa fa-close"></i></a>
</td>
</tr>
 
@endforeach

</tbody>

</table>


</div>



@stop

