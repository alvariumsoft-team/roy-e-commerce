@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add Page</h2>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Name :</b></label>
<label class="col-xs-12 col-md-8">
	<input name="name" type="text" class="form-control" placeholder="Page Name" value="@if(isset($cmspage['name'])) {{$cmspage['name']}} @endif" required/>
</label>
</div>

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Page :</b></label>
<label class="col-xs-12 col-md-8">
	<textarea name="value" class="form-control mceEditor">@if(isset($cmspage['value'])){{$cmspage['value']}}@endif</textarea>
</label>
</div>

<div class="form-group">

{{Form::submit('Submit',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector : ".mceEditor", height:270,   plugins: "link"});</script>

@stop
