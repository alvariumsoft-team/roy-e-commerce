@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>List Countries</h2>

<p>View countries.</p>

<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>Location</th>
<th>Country</th>
<th>Actions</th>

</tr>
</thead>

<tbody>

@foreach($countries as $country)

<tr>
<td>{{Form::checkbox('listing_action','','')}}</td>

<td>@if($country['location'] == 'eu') Europe @else Rest of the world @endif</td>
<td>{{$country['country']}}</td>
<td>
	<a href="{{URL::to('viewcities/'.$country['country'])}}" class="viewcities" style="font-weight:bold;margin-right:5px;">View Cities</a>
	<a href="{{URL::to('editcountry/'.$country['id'])}}" style="font-weight:bold;margin-right:5px;">Edit</a>
	<a href="{{URL::to('deletecountry/'.$country['id'])}}" style="font-weight:bold;margin-right:5px;">Delete</a>
</td>
</tr>
 
@endforeach

</tbody>

</table>


</div>


<div class="modal fade" id="cities-modal" tabindex ="-1" role="dialog" aria-labelledby="cities-modal-label" aria-hidden="true" >
	<div class="modal-dialog modall-dialog">
		<div class="modal-content modall-content">
			<div class="modal-header modall-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title modall-title" id="cities-modal-label"><b>Cities</b></div>
			</div>
			<div class="modal-body modall-body" id="mbody">
		
			<div id="modal-cities"></div>
		
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
		$('.viewcities').on('click', function(e){
			e.preventDefault();
			
			var url = $(this).attr('href');
			
			$.get(url).done(function(d){
				
				var html = '<ul style="">';
				
				var j = JSON.parse(d);
				
				for(i in j){
					html += '<li>'+j[i].toUpperCase()+'</li>';
				}
				
				html += '</ul>';
				
				$('#modal-cities').html(html);
			});
			
			
			$('#cities-modal').modal('show');
		});
		
		
	});
</script>

@stop

