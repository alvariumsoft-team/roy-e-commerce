@extends('main')
@section('content')

<style type="text/css">
	.advanced-search-container{background:#fff;margin:2% 5%;}
	.asrch-ul{padding:0px;margin:0px;border:0px;}
	.asrch-li{padding:0px;margin:0px;border:0px;list-style:none;float:left;}
	.asrch-li-a{padding:14px 18px;border:1px dotted #ccc;display:block;}
	.asrch-li-a:hover{background:#dedede;}
	
	.asrch-block{padding:10px;margin:10px;border-bottom:1px solid #dedede;}
	
	.asrch-search-data{border-right:1px dotted #dedede;}
	
	.asrch-search-data, .asrch-presence-data{width:100%;padding:10px;}
	
	.asrch-p-p{font-size:14px;margin-bottom:25px;padding-right:20px;}
	
	.asrch-p{font-size:14px;padding-right:20px;}
	
	.asrch-checkbox{}
	.asrch-checkbox-info{padding:10px 16px;color:#424242;font-size:14px;}
	
</style>


<script type="text/javascript">
	$(document).ready(function(){
		$('#add-location').on('click', function(){
			var html = '<div class="form-group"><input class="form-control" name="location[]"/></div>';
			$('#addloc').prepend(html);
		});
	});
</script>

	<div class="container">
		
		<form action="" method="post" >
		
		<div class="advanced-search-container">
		
		<div class="row">
		
		<div class="col-md-12 clearfix">
			
			<!--<ul class="asrch-ul">
				<li class="asrch-li"><a class="asrch-li-a" href="#">Homes for sale</a></li>
				<li class="asrch-li"><a class="asrch-li-a" href="#">Homes for rent</a></li>
				<li class="asrch-li"><a class="asrch-li-a" href="#">Newly built housing</a></li>
				<li class="asrch-li"><a class="asrch-li-a" href="#">Recreation</a></li>
				<li class="asrch-li"><a class="asrch-li-a" href="#">Europe</a></li>
			</ul>-->
			
			<h3 style="text-align:center;">Advanced Search</h3>
			
		</div>
			
			<div style="clear:both;">
			
			<div class="col-md-12">
				<section class="asrch-block location">
					<div class="row">
						<div class="col-md-2">
							<h4>Location</h3>
						</div>
						<div class="col-md-6">
							<h5 style="text-align:center;">Search below by town/city or zip code etc.</h5>
						</div>
						<div class="col-md-4">
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-6">
							<div class="form-group">
								<input class="form-control" name="location[]"/>
							</div>
							
							<div class="form-group" id="addloc">
								<span><a href="#" id="add-location">Add another location</a></span>
							</div>
							
						</div>
						<div class="col-md-2"></div>
					</div>
				</section>
				
				<section class="asrch-block budget">
					<div class="row">
						<div class="col-md-2"><h4>Budget</h4></div>
						<div class="col-md-2">
							<select class="form-control" name="budget_min">
								<?php for($i=0;$i<2050000; $i = $i+50000){ ?>
								<option value="<?php echo "$i"; ?>"><?php echo '$'.$i; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-2"><h4 style="text-align:center;">up to</h4></div>
						<div class="col-md-2">
							<select class="form-control" name="budget_max">
								<?php for($i=0;$i<200000000; $i = $i+50000){ ?>
								<option value="<?php echo "$i"; ?>"><?php echo '$'.$i; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-2"></div>
					</div>
				</section>
				
				<section class="asrch-block search-presence">
					<div class="row">
						<div class="col-md-6">
							
							<div class="asrch-search-data">
								<h4>Search criteria</h4>
								<div class="row">
									<div class="col-md-6">
										<p class="asrch-p-p">Type of property</p>
										<p class="asrch-p-p">Rent/Sale</p>
										<p class="asrch-p-p">Status type of property</p>
										<p class="asrch-p-p">Number of rooms</p>
										<p class="asrch-p-p">Floor area</p>
										<p class="asrch-p-p">Age of property</p>
										<!--<p class="asrch-p-p">Near By</p>
										<p class="asrch-p-p">Accessibility</p>
										<p class="asrch-p-p">Energy label</p>-->
										<p class="asrch-p-p">Show listing from</p>
										<!--<p class="asrch-p-p">Open House</p>
										<p class="asrch-p-p">Auction</p>
										<p class="asrch-p-p">Extras</p>-->
									</div>
									<div class="col-md-6">
										<p class="asrch-p">
											<select class="form-control" name="property_type">
												<option value="">No preference</option>
												<option value="house">House</option>
												<option value="apartment">Apartment</option>
												<option value="plot">Building Plot</option>
												<option value="parking">Parking</option>
												<option value="other_properties">Other Properties</option>
											</select>
										</p>
										
										<p class="asrch-p">
											<select class="form-control" name="status">
												<option value="">No preference</option>
												<option value="rent">Rent</option>
												<option value="sale">Sale</option>
											</select>
										</p>
										
										<p class="asrch-p">
											<select class="form-control" name="property_status_type">
												<option value="">No preference</option>
												<option value="resale">Resale Property</option>
												<option value="new">New Property</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="no_of_rooms">
												<option value="">No preference</option>
												<option value="1">1 Room</option>
												<option value="2">2 Rooms</option>
												<option value="3">3 Rooms</option>
												<option value="4">4 Rooms</option>
												<option value="5">5 Rooms</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="floor_area">
												<option value="">No preference</option>
												<option value="50+">50+ m2</option>
												<option value="75+">75+ m2</option>
												<option value="100+">100+ m2</option>
												<option value="150+">150+ m2</option>
												<option value="200+">200+ m2</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="age_of_property">
												<option value="">No preference</option>
												<option value="0-1906">before 1906</option>
												<option value="1906-30">1906-1930</option>
												<option value="1931-1944">1931-1944</option>
												<option value="1945-1959">1945-1959</option>
												<option value="1960-1970">1960-1970</option>
												<option value="1971-1980">1971-1980</option>
												<option value="1981-1990">1981-1990</option>
												<option value="1991-2000">1991-2000</option>
												<option value="2001-2010">2001-2010</option>
												<option value="2010-2020">after 2010</option>
											</select>
										</p>
										<!--<p class="asrch-p">
											<select class="form-control" name="near_by">
												<option value="">No preference</option>
												<option value="BladerRustigeWeg">Alongside a quiet road</option>
												<option value="BladerAanVaarwater">Alongside a waterway</option>
												<option value="BladerDrukkeWeg">Alongside busy road</option>
												<option value="BladerPark">Alongside park</option>
												<option value="BladerWater">Alongside water</option>
												<option value="BladerOpenligging">Freestanding location</option>
												<option value="BladerWoonwijk">In a residential neighborhood</option>
												<option value="BladerCentrum">In center</option>
												<option value="BladerInRecreatiepark">In recreational park</option>
												<option value="BladerBosrijkeOmgeving">In wooded surroundings</option>
												<option value="BladerBosrand">On the edge of a forest</option>
												<option value="BladerBuitenBebouwdeKom">Outside the built-up area</option>
												<option value="BladerBeschutteLigging">Sheltered location</option>
												<option value="BladerVrijUitzicht">Unobstructed surrounding view</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="accessibility">
												<option value="">No preference</option>
												<option value="aangepastewoning">Adapted home</option>
												<option value="gehandicaptenwoning">Disabled home</option>
												<option selected="selected" value="seniorenwoning">Elderly home</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="energy_label">
												<option selected="selected" value="">No preference</option>
												<option value="A">A-label</option>
												<option value="B">B-label</option>
												<option value="C">C-label</option>
												<option value="D">D-label</option>
												<option value="E">E-label</option>
												<option value="F">F-label</option>
												<option value="G">G-label</option>
											</select>
										</p>-->
										<p class="asrch-p">
											<select class="form-control" name="show_listing_from">
												<option value="">No preference</option>
												<option value="1">Last 24 hours</option>
												<option value="3">Last 3 days</option>
												<option value="5">Last 5 days</option>
												<option value="10">Last 10 days</option>
												<option value="30">Last 30 days</option>
											</select>
										</p>
										<!--<p class="asrch-p">
											<select class="form-control" name="open_house">
												<option selected="selected" value="">No preference</option>
												<option value="1">All open houses</option>
												<option value="2">Today (open house)</option>
												<option value="3">This weekend (open house)</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="auction">
												<option value="">No preference</option>
												<option selected="selected" value="1">All auctions</option>
												<option value="2">Today</option>
												<option value="3">The next week</option>
												<option value="4">The next month</option>
											</select>
										</p>
										<p class="asrch-p">
											<select class="form-control" name="extras">
												<option selected="selected" value="">No preference</option>
												<option value="Brochure">Brochure</option>
												<option value="Overbruggingsgarantie">Bridging guarantee</option>
												<option value="360-fotos">Virtual Tour</option>
												<option value="Plattegrond">Floor plan</option>
												<option value="Video">Video</option>
											</select>
										</p>-->
									</div>
									

									
								</div>
							</div>
							
						</div>
						<div class="col-md-6">
							
							<div class="asrch-presence-data">
								<h4>Presence of</h4>
								<div class="row">
										
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="balcony" /></span><span class="asrch-checkbox-info">Balcony</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="bathtub" /></span><span class="asrch-checkbox-info">Bathtub</span></label>
										<!--<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="commercial_property" /></span><span class="asrch-checkbox-info">Commercial property</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="monumental_building" /></span><span class="asrch-checkbox-info">Monumental building</span></label>
										--><label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="ch_builder" /></span><span class="asrch-checkbox-info">CH boiler</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fire_place" /></span><span class="asrch-checkbox-info">Fire place</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="roof_terrace" /></span><span class="asrch-checkbox-info">Roof terrace</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="sauna" /></span><span class="asrch-checkbox-info">Sauna</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="renewable_energy" /></span><span class="asrch-checkbox-info">Renewable energy</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="shed_storage" /></span><span class="asrch-checkbox-info">Shed / storage</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garage" /></span><span class="asrch-checkbox-info">Garage</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="steam_cabin" /></span><span class="asrch-checkbox-info">Steam cabin</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="jacuzzi" /></span><span class="asrch-checkbox-info">Jacuzzi</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garden" /></span><span class="asrch-checkbox-info">Garden</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fixer_upper" /></span><span class="asrch-checkbox-info">Fixer-upper</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="swimming_pool" /></span><span class="asrch-checkbox-info">Swimming pool</span></label>
										<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="elevator" /></span><span class="asrch-checkbox-info">Elevator</span></label>
	<!--									
										<div class="col-md-12 clearfix">
											<div style="margin:10px 0px;">
												<span class="pull-left">Type of commercial property</span><span class="pull-right"><select name="type_of_commercial_property" style="width:200px;" class="form-control">

													<option selected="selected" value="">No preference</option>
													<option value="winkelruimte">Retail space</option>
													<option value="kantoorruimte">Office space</option>
													<option value="praktijkruimte">Surgery</option>
													<option value="bedrijfsruimte">Commercial space</option>


													</select></span>
											</div>
										</div>
										
										<div class="col-md-12 clearfix">
											<div style="margin:10px 0px;">
												<span class="pull-left">Garden placement</span><span class="pull-right"><select name="garden_placement" style="width:200px;" class="form-control">

													<option selected="selected" value="">No preference</option>
													<option value="noord">North</option>
													<option value="west">West</option>
													<option value="zuid">South</option>
													<option value="oost">East</option>


													</select></span>
											</div>
										</div>
									
										<div class="col-md-12 clearfix">
											<div style="margin:10px 0px;">
												<span class="pull-left">Surface area of garden</span><span class="pull-right"><select name="surface_area_of_garden" style="width:200px;" class="form-control">

													<option selected="selected" value="">No preference</option>
													<option value="25">25+ m²</option>
													<option value="50">50+ m²</option>
													<option value="100">100+ m²</option>
													<option value="250">250+ m²</option>
													<option value="500">500+ m²</option>
													<option value="Other...">Other...</option>


													</select></span>
											</div>
										</div>
										
										<div class="col-md-12 clearfix">
											<div style="margin:10px 0px;">
												<span class="pull-left">Type of garage</span><span class="pull-right"><select name="type_of_garage" style="width:200px;" class="form-control">

													<option selected="selected" value="">No preference</option>
													<option value="[* TO *]">Alle soorten garages</option>
													<option value="aangebouwd">Lean-to</option>
													<option value="vrijstaand">Detached</option>
													<option value="garagebox">Lock-up garage</option>
													<option value="garagemetcarport">Garage + Carport</option>
													<option value="inpandig">built-in</option>
													<option value="parkeerkelder">underground car park</option>
													<option value="souterrain">basement</option>


													</select></span>
											</div>
										</div>
										
										<div class="col-md-12 clearfix">
											<div style="margin:10px 0px;">
												<span class="pull-left">Capacity</span><span class="pull-right"><select name="capacity" style="width:200px;" class="form-control">

													<option selected="selected" value="">No preference</option>
													<option value="1">1 car</option>
													<option value="2">2+ cars</option>
													<option value="3">3+ cars</option>
													<option value="4">4+ cars</option>
													<option value="Other...">Other...</option>

													</select></span>
											</div>
										</div>
				-->					
								</div>
								
							</div>
							
						</div>
					</div>
				</section>
				<div class="clearfix" style="position:relative;bottom:0px;"><button class="btn btn-default btn-info pull-right" type="submit">Search For Property</button></div>
				
			</div>
			
		</div>
		
	</div>
</div>

</form>

</div>

@stop
