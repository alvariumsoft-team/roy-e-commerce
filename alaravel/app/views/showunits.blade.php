@extends('adashb')
@section('dash_content')

<h2>Units</h2>

<p>View units.</p>

<h2></h2>

<form action="" method="post">


<!--<div class="row">

<div class="col-md-2">

<div class="form-group">{{Form::select('Bulk action',array('Bulk Actions','edit'=>'Edit','delete'=>'Move To Trash'),'',array('class'=>'form-control','id'=>'bulk_action'))}}</div>

</div>

<div class="col-md-1">

<div class="form-group">{{Form::button('apply',array('class'=>'btn','id'=>'bulk_action_button'))}}</div>


</div>

</div> -->

<div class="row">

<div class="col-md-9" style="overflow-x:auto;">

<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>


<th>Name</th>
<th>Type</th>
<th>Furnished</th>
<th>Area</th>
<th>Price</th>

</tr>
</thead>

<tbody>

@foreach($lists as $list)

<tr> 

<td>{{Form::checkbox('listing_action','','',array('class'=>'listing_action'))}}</td>
<td>{{$list['name']}}</td>
<td>{{$list['type']}}</td>
<td>{{$list['furnished']}}</td>
<td>{{$list['area']}}</td>
<td>{{$list['price']}}</td>

</tr>

@endforeach

</tbody>

</table>

</div>

</div>


</form>





{{ $lists->links() }}






@stop
