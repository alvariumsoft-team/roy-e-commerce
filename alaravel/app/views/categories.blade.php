	@include('webshop.header')
	<section class="side_bar">
      <div class="container-fluid">
        <div class="row">
	    @include('webshop.sidebar')
	<div class="col-sm-9 col-lg-9">	
																											<section class="product">
    		<div class="container-fluid">
    			<div class="row">
    			
    				<div class="col-sm-9 col-lg-9">
    					<div class="filter-cab">
    						<div class="row">
    							<div class="col-cs-12 col-sm-8 col-lg-8">
    								<div class="filter-left">
    									<a href="#" class="filter-anchor">
	    									<img src="{{url('assets/img/')}}/ico-filtrar.png" alt="filter" class="img-responsive">
	    									<span>filter & sort</span>
	    									<img src="{{url('assets/img/')}}/ico-ordenar.png" alt="filter" class="img-responsive">
    									</a>
    								</div>
    							</div>
    							<div class="col-cs-12 col-sm-4 col-lg-4">
    								<div class="filter-right">
	    								<h3>142 items <span>|</span> display</h3>
	    								<nav aria-label="Page navigation">
                                             <ul class="pagination">
                                                <li><a href="index2.html">6</a></li>
                                                <li><a href="index.html">4</a></li>
                                                <li><a href="index1.html">3</a></li>
                                            </ul>
                                        </nav>
									</div>
    							</div>
    							<div class="filter-bar">
    								<div class="col-md-3 col-sm-6 col-xs-12">
    									<button class="col-md-6 col-sm-6 col-xs-6 md-button" type="button">
    										<span>- Price</span>
    									</button>
    									<button class="col-md-6 col-sm-6 col-xs-6 md-button" type="button">
    										<span>+ Price</span>
    									</button>
    								</div>
    								<div class="col-md-3 col-sm-6 col-xs-12">
    									<div class="row">
    										<div class="col-md-5 col-sm-5 col-xs-3 price-range">
    											<span>Price range:</span>
    										</div>
                                            <div class="col-md-7 col-sm-7 col-xs-7 price-range">
                                                <div id="slider-range"></div>
                                                <form method="post" action="get_items.php">
                                                    <input type="hidden" id="amount1">
                                                    <input type="hidden" id="amount2">
                                                </form>
                                            </div>
    									</div>
    								</div>
    								<div class="col-md-2 col-sm-6 col-xs-12">
                                        <form>
                                            <div class="multiselect">
                                                <div class="selectBox" onclick="showCheckboxes()">
                                                    <select>
                                                        <option>Color</option>
                                                    </select>
                                                    <div class="overSelect"></div>
                                                </div>
                                                <div id="checkboxes">
                                                    <label for="one">
                                                        <input type="checkbox" id="one" />Yellow
                                                    </label>
                                                    <label for="two">
                                                        <input type="checkbox" id="two" />Blue
                                                    </label>
                                                    <label for="three">
                                                        <input type="checkbox" id="three" />White
                                                    </label>
                                                    <label for="three">
                                                        <input type="checkbox" id="four" />Grey
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                        <form>
                                            <div class="multiselect">
                                                <div class="selectBox" onclick="showCheckboxes1()">
                                                    <select>
                                                        <option>Size</option>
                                                    </select>
                                                    <div class="overSelect"></div>
                                                </div>
                                                <div id="checkboxes1">
                                                    <label for="one">
                                                        <input type="checkbox" id="one" />Single
                                                    </label>
                                                    <label for="two">
                                                        <input type="checkbox" id="two" />Double
                                                    </label>
                                                    <label for="three">
                                                        <input type="checkbox" id="three" />King
                                                    </label>
                                                    <label for="three">
                                                        <input type="checkbox" id="four" />Super King
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
    							</div>
    						</div>
    					</div>
    					<div class="product-inner">
    						<div class="row">
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/5915088999_1_1_4.jpg" alt="sofa-1" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Ornamental print duvet cover with dark base </a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/5916088999_1_1_4.jpg" alt="sofa-2" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Floral print duvet cover with coloured base
        								</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    							<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
								
									<div class="co-xs-12 col-sm-3">
    								<a href="{{url('webshop/categorydetail')}}">
    									<img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
    								</a>
    								<div class="product-content">
    									<h5>- NEW -</h5>
    									<h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
    									<p>45,99 �  - 69,99 � </p>
    								</div>
    							</div>
    						</div>
    					</div>
    					<!--<div class="product-inner">
                            <div class="row">
                                <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/5915088999_1_1_4.jpg" alt="sofa-1" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Ornamental print duvet cover with dark base </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/5916088999_1_1_4.jpg" alt="sofa-2" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Floral print duvet cover with coloured base
                                        </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
								 <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                            </div>
                        </div>
    					<div class="product-inner">
                            <div class="row">
                                <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/5915088999_1_1_4.jpg" alt="sofa-1" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}{{url('webshop/categorydetail')}}">Ornamental print duvet cover with dark base </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/5916088999_1_1_4.jpg" alt="sofa-2" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Floral print duvet cover with coloured base
                                        </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="#">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="#">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
								 <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                            </div>
                        </div>
    					<div class="product-inner">
                            <div class="row">
                                <div class="co-xs-12 col-sm-3">
                                    <a href="#">
                                        <img src="{{url('assets/img/')}}/5915088999_1_1_4.jpg" alt="sofa-1" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="#">Ornamental print duvet cover with dark base </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="#">
                                        <img src="{{url('assets/img/')}}/5916088999_1_1_4.jpg" alt="sofa-2" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="#">Floral print duvet cover with coloured base
                                        </a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                                <div class="co-xs-12 col-sm-3">
                                    <a href="#">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="#">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
								 <div class="co-xs-12 col-sm-3">
                                    <a href="{{url('webshop/categorydetail')}}">
                                        <img src="{{url('assets/img/')}}/1683088250_1_1_4.jpg" alt="sofa-3" class="img-responsive">
                                    </a>
                                    <div class="product-content">
                                        <h5>- NEW -</h5>
                                        <h2><a href="{{url('webshop/categorydetail')}}">Reversible two-tone washed percale duvet cover</a></h2>
                                        <p>45,99 �  - 69,99 � </p>
                                    </div>
                                </div>
                            </div>
                        </div>-->
    				</div>
    			<!--</div>
    		</div>-->
    	
		@include('webshop.footer')
		</div>
</div>
</div>
</section>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
          $(function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 280,
              values: [ 0, 280 ],
              slide: function( event, ui ) {
                $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
              }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
             " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          });
        </script>
		
		
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			
		 }
		 
		 
		  </script>