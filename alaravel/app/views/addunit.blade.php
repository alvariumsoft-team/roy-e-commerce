@extends('adashb')

@section('dash_content')

<h2>Add New Unit</h2>

<p>Add new unit to a project.</p>

<form action="" method="post" enctype="multipart/form-data">


<div class="form-group row">
<label class="col-md-3"><b>Project Name</b></label>
<label class="col-md-6">{{Form::select('project',$projects,'BHK',array('class'=>'form-control','placeholder'=>'Project Name'))}}</label>
</div>

<div class="form-group row">	
<label class="col-xs-12 col-md-3" ><b>Image:</b></label>
<label class="col-xs-12 col-md-6"  for="ifile-id2" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd"><span id='ifile-labe'  class='ifile-label'>Upload Image</span>{{Form::file('file',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'ifile-id2','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">	
<label class="col-xs-12 col-md-3" ><b>Floor Plan:</b></label>
<label class="col-xs-12 col-md-6"   ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd"><span id='ifile-lab'  class='ifile-label'>Upload Floor Plan</span>{{Form::file('floor_plan',array('class'=>'inputfile inputfile-1 ifile-id','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">	
<label class="col-xs-12 col-md-3" ><b>Floor Plan 3D:</b></label>
<label class="col-xs-12 col-md-6"   ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd"><span id='ifile-lab'  class='ifile-label'>Upload Floor Plan 3D</span>{{Form::file('floor_plan_3d',array('class'=>'inputfile inputfile-1 ifile-id','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Unit Type</b></label>
<label class="col-md-6">{{Form::select('unit_type',array('apartment'=>'Apartment','land'=>'Land','home'=>'Home'),'',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Construction Status</b></label>
<label class="col-md-6">{{Form::select('build_status',array('started'=>'Just Started','under'=>'Under Construction','completed'=>'Completed'),'',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Rooms</b></label>
<label class="col-md-6">{{Form::select('bedroom',array('Room','1','2','3','4','5','6','7','8','9','10'),'Room',array('class'=>'form-control'))}}</label>
</div>


<div class="form-group row">
<label class="col-md-3"><b>Balconys</b></label>
<label class="col-md-6">{{Form::select('balcony',array('Balcony','1','2','3','4','5','6','7','8','9','10'),'Balcony',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Bathrooms</b></label>
<label class="col-md-6">{{Form::select('bathroom',array('Bathroom','1','2','3','4','5','6','7','8','9','10'),'Bathroom',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Furnished</b></label>
<label class="col-md-6">{{Form::select('furnished',array('semi furnished'=>'Semi Furnished','fully furnished'=>'Fully Furnished'),'',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Carpet Area</b></label>
<label class="col-md-6">{{Form::text('area','',array('class'=>'form-control','placeholder'=>'Area','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Price</b></label>
<label class="col-md-6">{{Form::text('price','',array('class'=>'form-control','placeholder'=>'Price','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Price Per sq. ft.</b></label>
<label class="col-md-6">{{Form::text('price_per_sqft','',array('class'=>'form-control','placeholder'=>'Price Per sq. ft.','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Status Type</b></label>
<label class="col-md-6">{{Form::select('status_type',array('sale'=>'Resale','rent'=>'New'),'',array('class'=>'form-control','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Age Of Property</b></label>
<label class="col-md-6"><input type="date" class="form-control" name="age_of_property" placeholder="Launch Date"/></label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Listing Status</b></label>
<label class="col-md-6">{{Form::select('list_status',array('sale'=>'For Sale','rent'=>'For Rent'),'',array('class'=>'form-control','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Map Location</b></label>
<label class="col-md-3">{{Form::text('latitude','',array('class'=>'form-control','placeholder'=>'Latitude','required'=>'true'))}}</label>
<label class="col-md-3">{{Form::text('longitude','',array('class'=>'form-control','placeholder'=>'Longitude','required'=>'true'))}}</label>
</div>

<div class="form-group row">

<label class="col-md-3"><b>Images</b></label>	

<label class="col-xs-12 col-md-6" >
	<div style="position:relative;height:100px;width:100%;border:2px dotted #ccc;background:#fff;" class="asd">
	<span style="margin-top:30px;margin-left:36%;" class="btn btn-default ifile-label">Select Files</span>
	{{Form::file('files[]',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'select-files','multiple'=>'multiple', 'data-multiple-caption'=>'{count} files selected'))}}
	</div>
	</label>
</div>

<div class="form-group row">
<label class="col-md-3"><b>Presence Of</b></label>
<label class="col-md-6">
<div class="row">
	
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="balcony" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Balcony</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="bathtub" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Bathtub</span></label>
	<!--<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="commercial_property" /></span><span class="asrch-checkbox-info">Commercial property</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="monumental_building" /></span><span class="asrch-checkbox-info">Monumental building</span></label>
	--><label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="ch_builder" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;CH boiler</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fire_place" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Fire place</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="roof_terrace" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Roof terrace</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="sauna" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Sauna</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="renewable_energy" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Renewable energy</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="shed_storage" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Shed / storage</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garage" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Garage</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="steam_cabin" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Steam cabin</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="jacuzzi" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Jacuzzi</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="garden" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Garden</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="fixer_upper" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Fixer-upper</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="swimming_pool" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Swimming pool</span></label>
	<label class="col-md-6"><span class="asrch-checkbox"><input type="checkbox" name="elevator" /></span><span class="asrch-checkbox-info">&nbsp;&nbsp;Elevator</span></label>

</div>
</label>
</div>

<div class="form-group">{{Form::submit('Submit Unit',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>

@stop
