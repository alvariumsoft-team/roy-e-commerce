@extends('main')

@section('content')

<style type="text/css">
/*
body{background:url('http://static.pexels.com/photos/24499/pexels-photo-24499.jpg') !important;
    background-repeat: no-repeat !important;
    background-attachment: fixed !important;
    background-position: center !important; 

}
*/
.putpul_main {
  width: 231px;
}

.srcShadow:hover {
    border: 1px solid #424242;
}

</style>

<style type="text/css">
	
.panel-heading{background:#fff !important;border:0px solid #dedede !important;}

.panel-body{padding: 0px !important;}

.panel-primary{background:#fff !important;border:0px solid #fff !important;}

.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px !important;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none !important;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px !important;
}
/********************************************************************/

/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	/*border-color: transparent;*/
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
#tab1primary h3 {color: #666;}
.panel a { color: #666 !important;}
.active.projects  a { border-top: 4px solid #DA251C !important;color: #333 !important;}
.with-nav-tabs.panel-primary .nav-tabs > .open > a, .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover, .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus, .with-nav-tabs.panel-primary .nav-tabs > li > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
  background-color: #fff!important;
  border-color: transparent;
  color: #fff;
}
.house_main {padding-top: 10px;}

.bhk h4 {margin-bottom: 6px;}
.show-on-hover:hover > ul.dropdown-menu {display: block;}
.toggle_main{background-color:none;color:#999; border: none;}
.open_haver {margin-top: 17px;margin-bottom: 18px;}
.btn .caret {margin-left: 6px;}
.blid.pull-right > p {margin-bottom: 1px;}
.blid.pull-left > p {margin-bottom: 1px;}
.dleft img { width: 100%;}
.dolar {margin-bottom: 11px;}
.button_img  button {background: #fff;border: 2px solid #C0392B; padding: 10px; font-size:12px; color: #C0392B;}
.button_imgb  button {background: #C0392B;border: 2px solid #C0392B; padding: 10px; font-size:12px; color: #fff;margin-left: -18px!important;}
.maing {margin-bottom: 70px;}

.button_imgbh_img > p {margin-bottom: 0;text-align: right; font-size: 13px!important; margin-left: -42px!important;}
.button_imgbh_img h6{text-align: right;}
.sqyrd { float: right;margin-bottom: 10px;}
.button_imgb {margin-top: 20px;margin-left: 0px!important;}
.button_img {margin-top: 20px;margin-left: -17px;}
.left_img img {width: 100%;}
.pull_map:hover {font-size: 15px;!important;margin-top:-5px;margin-left:-5px;cursor:pointer;}

.clearfix_new {margin-left: -13px!important;}
.button_imgbh_img.pull-right {margin-right: -60px!important;}
.sqyrd.pull-right {margin-right: -35px;!important}
//.pull-left {margin-left: -16px;}


.panel{font-size:13px;font-weight:500;}

.listing-heading{color:#000 !important;}
.listing-heading:hover{color:#C0392B !important; font-weight:900 !important;}
.slider-photo{height:200px;width:100%}
     @media screen and (max-width: 1024px) {
		 
		 .slider-photo{height:270px;}
	 }
	 @media screen and (max-width: 1024px) {
.slider-photo {
    height: 200px
}
 @media screen and (max-width: 767px) {
 #contact-agent .modal-dialog{margin-top:2% !important;} 
 #contact-agent{margin-top:-19%;}
#view-phone {top:0% important;}
.search-ani .ani-search{padding: 5px 16px !important;
margin-top: -50%;
margin-right: 25%;}
.modal-dialog{margin-left:auto;margin-right:auto;}
 }
</style>

<style type="text/css">
	.ani-search:hover{
		background:#c4302b;
		color:#fff;
		box-shadow:0px 0px 1px #c4302b;
	}
	.ani-search{outline:none !important;border:0px;background:#a1a1a1;margin-top:-40px;}
	.search-bar-top{}
	.ani-show{display:block;}
	.search-bar-top{display:none;}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$('.ani-search').on('click', function(){
			var x = $('.search-bar-top').css('display');
			if(x == 'block'){$('.search-bar-top').css('display','none');}
			else{$('.search-bar-top').css('display','block');}
		});
	});
</script>


<!-- background: #E2E2E2; -->
<section class="fixed-search-bar" style="width: 100%;background:#2a2a2a;position:relative;transition: all ease 0.5s;">
	<div class="container">
		<div class="search-ani clearfix" style="position:absolute;right:0px;">
			<button class="btn btn-default pull-right ani-search" style="padding:10px 24px;border-radius:0px !important;"><i class="fa fa-search"></i></button>
		</div>
		<div style="font-size:14px;color:#fff;" class="search-bar-top">
			<div class="row">
				<div class="col-md-2">
					<div class="dropdown">
					  <button class="search-buttons search-buttonss dropdown-toggle" style="color:#a1a1a1 !important;" type="button" id="buy-sale" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Buy/Rent 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="buy-sale">
						<li><a href="{{URL::to('search')}}/all">All</a></li>
						<li><a href="{{URL::to('search')}}/buy_rent/sale">Buy</a></li>
						<li><a href="{{URL::to('search')}}/buy_rent/rent">Rent</a></li>
						<!--<li role="separator" class="divider"></li>
						<li><a href="#">Separated link</a></li>-->
					  </ul>
					</div>
				</div>
				<div class="col-md-2">
					<div class="dropdown">
					  <button class="search-buttons search-buttonss dropdown-toggle" style="color:#a1a1a1 !important;" type="button" id="construction-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Construction Status 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="construction-type">
						<li><a href="{{URL::to('search')}}/construction/ready">Ready To Move</a></li>
						<li><a href="{{URL::to('search')}}/construction/underconstruction">Under Construction</a></li>
					  </ul>
					</div>
				</div>
				<div class="col-md-2">
					<div class="dropdown">
					  <button class="search-buttons search-buttonss dropdown-toggle" style="color:#a1a1a1 !important;" type="button" id="property-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					   Property Type 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="property-type">
						<li><a href="{{URL::to('search')}}/property_type/house">House</a></li>
						<li><a href="{{URL::to('search')}}/property_type/apartment">Apartment</a></li>
						<li><a href="{{URL::to('search')}}/property_type/land">Land</a></li>
						<li><a href="{{URL::to('search')}}/property_type/storage">Storage</a></li>
						<li><a href="{{URL::to('search')}}/property_type/berth">Berth</a></li>
						<li><a href="{{URL::to('search')}}/property_type/substructure">Substructure</a></li>
					  </ul>
					</div>
				</div>
				<div class="col-md-2">
					<div class="dropdown">
					  <button class="search-buttons search-buttonss dropdown-toggle" style="color:#a1a1a1 !important;" type="button" id="bedroom-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Bedroom 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="bedroom-type">
						  <div class="row">
						  <form action="{{URL::to('search')}}/beds/" method="get">
						  <div class="col-md-6"><div style="padding:3px 5px;"><input name="bed_from" type="text" class="form-control" placeholder="From" /></div></div>
						  <div class="col-md-6"><div style="padding:3px 5px;"><input name="bed_to" type="text" class="form-control" placeholder="To" /></div></div>
						  <div class="col-md-12"><div style="padding:3px 5px;"><input name="bed_search" type="submit" class="form-control" value="Search" /></div></div>
						  </form>
						  </div>
					  </ul>
					</div>
				</div>
				<div class="col-md-2">
					<div class="dropdown">
					  <button class="search-buttons search-buttonss dropdown-toggle" style="color:#a1a1a1 !important;" type="button" id="area-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Area 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="area-type">
						  <div class="row">
						  <form action="{{URL::to('search')}}/area/" method="get">
						  <div class="col-md-6"><div style="padding:3px 5px;"><input name="area_from" type="text" class="form-control" placeholder="From" /></div></div>
						  <div class="col-md-6"><div style="padding:3px 5px;"><input name="area_to" type="text" class="form-control" placeholder="To" /></div></div>
						  <div class="col-md-12"><div style="padding:3px 5px;"><input name="area_search" type="submit" class="form-control" value="Search" /></div></div>
						  </form>
						  </div>
					  </ul>
					</div>
				</div>

				<div class="col-md-2">
				</div>
			</div>
		</div>
	</div>
</section>


<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet"> 

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style type="text/css">


.prop-nav > .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
.prop-nav > .nav-tabs > li > a { border: none; color: #666; }
.prop-nav > .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #29333d !important; background: transparent; }
.prop-nav > .nav-tabs > li > a::after { content: ""; background: #29333d; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
.prop-nav > .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.prop-nav > .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }




#imaginar_{
    background: #252525;
	text-align: center;
    border-radius: 4px;
}
.search_ {
    padding-left: 18px;
}
.link_{color:#fff;text-decoration: none;}
.link_:hover{color:#fff;text-decoration: none;}
.card {background: #FFF none repeat scroll 0% 0%;margin-bottom: 15px; }

.choose_pro {
    /*margin: 11px 0px;*/
    padding-left:15px;
    font-family: 'Roboto Slab', serif;
}
.choose_pro span {
    color:#29333d;
}
.tabgd{border:none;}
.main_volo {
height: 38px;
line-height: 38px;
border-radius: 4px;
border: solid 1px #e5e5e5;
padding: 0 5px;
width: 100%;
}
.slectin_option {
    margin: 30px 0;
	width: 100%;
}

.slectin_option .form-group select, input{margin-bottom:30px !important;color:#000;}

#imaginary_container{
    margin-top:20%; /* Don't copy this */
}
.stylish-input-group .input-group-addon{
    background:#000 !important; 
	    padding-right: 22px;
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
.icon_ft{background:#000;text-align: right;}
	
.s_div{width:100%;background:#f9f9f9;}
	
.prop-nav{margin-top:30px;margin-bottom:50px;}
	
	
.ui-widget{font-size:10px;height: 5px;}

.ui-slider-horizontal .ui-slider-handle {
    top: -0.4em;
}

.ui-state-active{
	border: 1px solid rgba(0,0,0,0.83) !important;
	background: rgba(0,0,0,0.83) !important;
}

/*

.ui-slider .ui-slider-range {
    background: #29333d !important;
}

.ui-state-default{
	border: 1px solid #29333d !important;
	background: #29333d !important;
}

*/



.country-nav > .active > a {
   border-color:none!important;
   background: #494949 !important;
    color:#fff!important;
    border-radius: 0;
    padding: 10px 22px!important;
    display:block;
}
.country-nav > .active > a:hover{
   color:#fff!important;
   background: #494949 !important;
}

.country-nav > li{
  margin-bottom: 0!important;
  border:0 none!important;
    margin-right: 20px;
}
.country-nav > li a{
  border:0 none!important;
       font-size: 14px !important;
}
.country_bootom{
	border-bottom:1px solid #DDD!important;
}
.country-nav{
padding-bottom:10px!important;	
	padding:5px 15px 12px !important;
}
.loc-loc.cleardix > li {
    padding: 15px !important;
}
		
#search_rent{
  transition: all 2s ease-out;
}
/**************************add css 6-01-2017*****************/
.srcShadow {
	cursor: pointer;
	margin-bottom: 10px;
	background: #fff;
	box-shadow: 0 1px 2px 0 rgba(0,0,0,0.2);
	border: solid 1px transparent;
	float: left;
	padding:5px;
}
.certif_ p {
	position: absolute;
	color: #fff;
	font-size: 13px;
	padding-left:7px;
	line-height:0px;
	margin-top: -12px;
}
.certif_ img {
    width: 100%;
    height: 150px;
}
.certif_ img:hover  {
    transform: scale(1.1);
}
.certdif_ strong {
    font-size: 14px;
    padding-left: 0;
}
.col_53 {
    color: #333;
    display: inline-block;
    font-size: 14px;
	font-weight: bold;
    margin-left: 10px;
}
.maxPro_DesWrap {
    font-size: 14px;
    margin-left: 5px;
}
.bathroom_ghj {
	margin: 20px 0;
}
.detail_ {
    padding-left: 0px;
}
.detail_ a {
    font-size: 13px;
}
.Details_d {
    margin-right: 20px;
	color: #999;
}
.bath_room{
	color: #333;
	margin-right:5px;
}
.View_detail {
    color: #333;
    text-decoration: underline;
	font-weight: bold;
}
.detail_ li {
    display: inline;
	margin-right: 5px;
}
.danger_d {
	background-color:#b71c1c;
	color: #fff;
	width: 150px;
	font-size: 14px;
	line-height: 18px;
	font-weight: bold;
}
.Contact_agent {
	background:transparent;
	color: #b71c1c;
	width: 150px;
	font-size: 14px;
	line-height: 18px;
	margin: 0 10px;
	font-weight: bold;
}
.Phone_agent{
    background:transparent;
	color: #b71c1c;
	width: 90px;
	font-size: 14px;
	line-height: 18px;
	font-weight: bold;
}
.refg{float:left;margin-right: 10px;}
.vetficat_ p{margin:0px;}
.fa.fa-comments.comm {
	margin-right: 5px;
	font-size: 17px;
}
.vetficat_ {
	margin: 12px 0;
}
.icon_g {
	padding-right: 5px;
}
.map_location a{
	color:#333;
	font-size:13px;
	text-decoration: underline;
}
.star_{margin-left:10px;}
.dollar_ h5 {
	font-size: 18px;
	color: #b71c1c;
	font-weight:bold;
}
.dollanr_ p{
	color: #333;
	text-decoration: underline;
}
.group_dollar {
	text-align: right;
}
.dollanxr_ {
	margin-top: 20px;
		color: #333;
	text-decoration: underline;
}
.posted_t {
	margin: 100px 0 0;
	color: #999;
    font-size: 10px;
}


@media only screen and (max-width: 375px){
.danger_d {
	width: 111px;
	font-size: 13px;
}
.Contact_agent {
width: 111px;
font-size: 13px;
margin: 0 5px;
}
.Phone_agent{
	width: 73px;
	font-size: 13px;
}	
.star_ {
	margin-left: 2px;
}
	
}


@media only screen and (max-width: 360px){
.danger_d {
	width: 100px;
	font-size: 11px;
}
.Contact_agent {
width: 100px;
font-size: 11px;
margin: 0 5px;
}
.Phone_agent{
	width: 73px;
	font-size: 11px;
}	
}


@media only screen and (max-width: 320px){
.danger_d {
	width: 90px;
	font-size: 10px;
}
.Contact_agent {
width: 90px;
font-size: 10px;
margin: 0 1px;
}
.Phone_agent{
	width: 68px;
	font-size: 11px;
}	
}

	
	
</style>


<form action="" method="post" class="s_div">

<div class="container">
		<!-- Nav tabs -->
		<div class="card prop-nav">
			<ul class="nav nav-tabs tabgd" role="tablist">
				<li role="presentation" class="active"><a class="tal" href="#search_rent" aria-controls="profile" role="tab" data-toggle="tab">FOR RENT</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">BUY</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">PROJECT</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">AGENT</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">NEWLY BUILT</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">RECREATION</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">COMMERCIAL</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">AGRICULTURAL</a></li>
				<li role="presentation"><a class="tal" href="#search_rent" aria-controls="messages" role="tab" data-toggle="tab">LUXURY PROJECTS</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="search_rent">
					
					<div class="srchpnl">
						<div class="row">
							<div class="slectin_option ">
							<div class="form-group col-lg-3">
								<select class="main_volo">
									<option value="">Property Type</option>
									<option value="">House</option>
									<option value="">Apartment</option>
									<option value="">Land</option>
									<option value="">Storage</option>
									<option value="">Berth</option>
									<option value="">Substructure</option>
								</select>
							</div>
							
							<div class="form-group col-lg-3">
								<select class="main_volo">
									<option value="">Construction Status</option>
									<option value="">Ready to move</option>
									<option value="">Under construction</option>
								</select>
							</div>
							
							<div class="form-group col-lg-3">
								<select class="main_volo">
									<option value="">Furnished Status</option>
									<option value="">Fully Furnished</option>
									<option value="">Semi Furnished</option>
								</select>
							</div>
							
							<div class="form-group col-lg-3">
								<input name="location" id="location" class="main_volo" value="@if(Session::has('country')) {{Session::get('country')}}@endif" placeholder="Location" type="text">
							</div>
							
							<div class="form-group col-lg-3">
								<div class="search_slider">
									
									<p class="clearfix" style="position:relative;">
										<span style="float:left; color:#29333d !important; font-size:12px;">Price range:</span>
										<span class="amount" style="float:right; color:#29333d !important; font-size:12px;font-weight:bold;"></span>
										<input type="hidden" name="min" value="" />
										<input type="hidden" name="max" value="" />	
									</p>
									
									<div class="slider-range"></div>
									
								</div>
							</div>
							
							<div class="form-group col-lg-3">
								<div class="search_slider">
									
									<p class="clearfix" style="position:relative;">
										<span style="float:left; color:#29333d !important; font-size:12px;">Area(Sqft.):</span>
										<span class="sqft" style="float:right; color:#29333d !important; font-size:12px;font-weight:bold;"></span>
										<input type="hidden" name="min" value="" />
										<input type="hidden" name="max" value="" />	
									</p>
									
									<div id="area-sqft"></div>
									
								</div>
							</div>
							
							<div class="form-group col-lg-3">
								<div class="search_slider">
									
									<p class="clearfix" style="position:relative;">
										<span style="float:left; color:#29333d !important; font-size:12px;">Rooms:</span>
										<span class="sqft" style="float:right; color:#29333d !important; font-size:12px;font-weight:bold;"></span>
										<input type="hidden" name="min" value="" />
										<input type="hidden" name="max" value="" />	
									</p>
									
									<div id="rooms"></div>
									
								</div>
							</div>
							
							<div class="form-group col-lg-3">
								<div id="imaginar_"> 
									<button type="submit" class="btn btn-link link_">SEARCH<i class="fa fa-search search_" aria-hidden="true"></i></button>
								</div>
							</div>
							</div>
						</div>
					</div>
					
				</div>

				<div role="tabpanel" class="tab-pane" id="search_commercial"></div>
				<div role="tabpanel" class="tab-pane" id="search_agricultural"></div>
				<div role="tabpanel" class="tab-pane" id="search_luxury"></div>			
			</div>
		</div>
</div>


</form>

  <script>
	  
	  
	  
  $( function() {
	  
	$('.tal').on('click', function(){
		$('#search_rent').hide();
		setTimeout(function () {
			$('#search_rent').show();
		},160);
	});
	  
    $( "#area-sqft" ).slider({
		
      range: true,
      min: 10,
      max: 10000,
      values: [ 1000, 3000 ],
      slide: function( event, ui ) {
        $(this).parents('.search_slider').find('p').find( ".sqft" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        $('input[name="min"]').val(ui.values[ 0 ]);$('input[name="max"]').val(ui.values[ 1 ]);
      }
    });
    $('.search_slider').find('p').find( ".sqft" ).html( "$" + $( "#area-sqft" ).slider( "values", 0 ) +
      " - $" + $( "#area-sqft" ).slider( "values", 1 ) );
  } );
	  
  $( function() {
    $( "#rooms" ).slider({
      range: true,
      min: 1,
      max: 15,
      values: [ 3, 10 ],
      slide: function( event, ui ) {
        $(this).parents('.search_slider').find('p').find( ".sqft" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        $('input[name="min"]').val(ui.values[ 0 ]);$('input[name="max"]').val(ui.values[ 1 ]);
      }
    });
    $('.search_slider').find('p').find( ".sqft" ).html( "$" + $( "#rooms" ).slider( "values", 0 ) + " - $" + $( "#rooms" ).slider( "values", 1 ) );
  } );
	  
	  
  $( function() {
    $( ".slider-range" ).slider({
		
      range: true,
      min: 0,
      max: 3000000,
      values: [ 900000, 2000000 ],
      slide: function( event, ui ) {
        $(this).parents('.search_slider').find('p').find( ".amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        $('input[name="min"]').val(ui.values[ 0 ]);$('input[name="max"]').val(ui.values[ 1 ]);
      }
    });
    $('.search_slider').find('p').find( ".amount" ).html( "$" + $( ".slider-range" ).slider( "values", 0 ) +
      " - $" + $( ".slider-range" ).slider( "values", 1 ) );
  } );
  </script>	

<div class="container">

<div style="">

    <div class="row">
        <div class="col-md-10">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                        <ul class="nav nav-tabs project">
                            <li class="active projects tab-li"><a href="#tab1primary" data-toggle="tab"> Properties</a></li>
                            <li class="projects tab-li"><a href="#tab2primary" data-toggle="tab"> New Projects</a></li>
                            <li class="projects tab-li"><a href="#tab3primary" data-toggle="tab"> Agents</a></li>
                            <li class="projects tab-li"><a href="#tab3primary" data-toggle="tab">  Localities</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						

                        
                        <div class="tab-pane fade in active" id="tab1primary">
							
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="residentali">
                                   <!--<h3>4249 Residential Properties for Sale</h3>-->
                               </div>
                            </div>
                        </div>	

					@if(empty($listings)) <h4>Sorry! No records found!</h4> @endif
					
@foreach($listings as $list)



<div class="rohit" style="border-bottom:1px solid #dedede; float:left; padding:10px 0px;">
<div class="srcShadow">
		   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
		      <div class="certif_">
			      <img class="img-responsive slider-photo" src="{{$list->img}}" style=""></img>
				  <p><!--15 Photos--></p>
			  </div>
			  <div class="vetficat_">
			  </div>
		   </div>
		   <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
		      <div class="certdif_">
			  <?php 
				$project_id = $list->project_id;
				$pl_site = '';
				if($project_id != 0){$pl_site = 'project/'.$project_id;}
				else{$pl_site = 'listing/'.$list->id;}
			?>		
			    <h5>
				<a class="listing-heading" target="_blank" href="{{URL::to('microsite/'.$pl_site )}}"><strong>@if($list->beds > 0){{$list->beds}} BHK, @endif {{ucwords($list->list_type)}}</strong> &nbsp;<span class="maxPro_DesWrap"> for {{$list->status}} in {{ucwords($list->name)}} , {{ucwords($list->location)}}</span><span class="col_53">{{$list->area}}&nbsp; sq.ft.</span> </a>
				
				</h5>
			  </div>
			  <div class="bathroom_ghj">
			  <ul class="detail_">
			    <li><a class="Details_d" href="#">Details:</a></li>
                <li><a class="bath_room" href="#">Ubber Group</a></li>
				<li><a class="View_detail" href="#">View Details</a></li>
			  </ul>
			  <ul class="detail_">
			    <li><a class="Details_d" href="#">Status:</a></li>
                <li><a class="bath_room" href="#">{{ucwords($list->status)}}</a></li>
			  </ul>
			  <ul class="detail_">
			    <li><a class="Details_d" href="#">Description:</a></li>
                <li><a class="bath_room" href="#">"1BHK - Whitefield - ZERO Brokerage 1 BHK semi fur...</a></li>
				<li><a class="View_detail"  target="_blank" href="{{URL::to('microsite/'.$pl_site )}}">read more</a></li>
			  </ul>
			  </div>
			  <button type="button" class="btn btn-danger danger_d phone-view">View Phone No.</button>
			   <button type="button" class="btn btn-danger Contact_agent agent-contact">Contact Agent</button>
			    <button type="button" class="btn btn-danger Phone_agent"><i class="fa fa-comments comm" aria-hidden="true"></i>Chat</button>
				
		   </div>
		   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
		      <h5 class="map_location">
				<a href="javascript:void(0);" data-lat="{{$list->map_lat}}" data-long="{{$list->map_long}}" class="map-show"><i class="fa fa-map-marker" style="padding:0px 10px;"></i>Map</a>
			  </h5>
		   </div>
		   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
		   <div class="group_dollar">
		      <div class="dollar_"><h5>${{$list->price}}</h5></div>
			  <div class="dollanr_"></div>
			  <div class="dollanxr_"></div>
			  <!--<p class="posted_t" style="bottom:0px;">Posted Today</p>-->
			  </div>
			   </div>
			   
		   </div>

		

</div>
          
@endforeach 
                </div>

                        <div class="tab-pane fade" id="tab2primary">Primary 2</div>
                        <div class="tab-pane fade" id="tab3primary">Primary 3</div>
                        <div class="tab-pane fade" id="tab4primary">Primary 4</div>
                        <div class="tab-pane fade" id="tab5primary">Primary 5</div>
                    </div>
                </div>
            </div>
        </div>
   
        <div class="col-md-2">
			<!--<a class="twitter-timeline" href="https://twitter.com/CowboyGlobal">Tweets by CowboyGlobal</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->
			
			<img src="{{URL::to('packages/images/ad.png')}}" style="width:240px;padding-top:50px;">
			<img src="{{URL::to('packages/images/ad.png')}}" style="width:240px;padding-top:50px;">
			
		</div>
    </div>
</div>
</div>

<style type="text/css">
	
	#show-map > .modal-dialog{width:90% !important;height:90% !important;}
	
	#photo-slider > .modal-dialog{width:90% !important;height:90% !important;}
	
	#photo-slider{top:0px !important;}
	
	.slider-photo{cursor:pointer;}
	
	.vp{padding:5px;}
</style>


  <!-- FlexSlider -->
  {{HTML::style('packages/css/flexslider.css')}}

  {{HTML::script('packages/js/jquery.flexslider.js')}}

  <script type="text/javascript">
   
    $(window).ready(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>

<script type="text/javascript">
	
	$ = jQuery.noConflict();
	
	$(document).ready(function(){
		var latitude;
		var longitude;
	$('.map-show').on('click',function(){
		
		latitude = $(this).attr('data-lat');
		longitude = $(this).attr('data-long');
		
		$('#show-map').modal('show');
		
		if((latitude == '') || (latitude == undefined)){
			latitude	= 28.7041;
			longitude	= 77.1025;
		}
		
		setTimeout(function() {
			$('.re-map').trigger('resize');
		
		latitude = parseFloat(latitude);
		longitude= parseFloat(longitude);

			var uluru = {lat: latitude, lng: longitude};
			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			  center: uluru
			});
			var marker = new google.maps.Marker({
			  position: uluru,
			  map: map
			});
			
		}, 500);
		
	});	
	
	
	$('.phone-view').on('click',function(){
		$('#view-phone').modal('show');
	});	

	$('.agent-contact').on('click',function(){
		$('#contact-agent').modal('show');
	});	
	
	$('.slider-photo').on('click',function(){
		
		$('#photo-slider').modal('show');
		
		setTimeout(function() {
			$('.slider').trigger('resize');
		}, 500);
		
	});	

	});
	
</script>

<div class="modal fade" id="view-phone" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="gpc-modal-label"><h2 style="text-align:center;">View Phone Number</h2></div>
			</div>
			<div class="modal-body" id="mbody">
				<div class="row">
					<div class="col-md-12">
						<span class="vp"> I am </span>
						<span class="vp"><input type="radio" name="contact_type" value="individual" /></span>
						<span class="vp"> Individual </span>
						<span class="vp"><input type="radio" name="contact_type" value="agent" /></span>
						<span class="vp"> Agent </span>
						<br style="margin:5px;"/>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Name"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Mobile"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Email"/>
						</div>
						<div class="form-group">
						<input type="submit" class="btn btn-primary" name="View Phone" value="View Phone"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<style type="tet/css">

.slider {display: block; margin: 50px 0 10px!important;}
#carousel li {margin-right: 5px;}
#carousel img {display: block; opacity: .5; cursor: pointer;}
#carousel img:hover {opacity: 1;}
#carousel .flex-active-slide img {opacity: 1; cursor: default;}

.photo-slider-body{padding:0px !important;}

</style>

<div  class="modal fade" id="photo-slider" tabindex ="-1" role="dialog" aria-labelledby="photo-view-label" aria-hidden="true" style="height:100% !important">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="photo-view-label"><h2 style="text-align:center;"></h2></div>
			</div>
			<div   class="modal-body photo-slider-body" style="padding:0px !important">
				
				<figure>
				
			  <section class="slider">
				<div class="flexslider">
				  <ul class="slides" >
					<li data-thumb="{{URL::to('packages/images/1.jpg')}}">
						<img src="{{URL::to('packages/images/1.jpg')}}" />
						</li>
						<li data-thumb="{{URL::to('packages/images/2.jpg')}}">
						<img src="{{URL::to('packages/images/2.jpg')}}" />
						</li>
						<li data-thumb="{{URL::to('packages/images/3.jpg')}}">
						<img src="{{URL::to('packages/images/3.jpg')}}" />
						</li>
						<li data-thumb="{{URL::to('packages/images/4.jpg')}}">
						<img src="{{URL::to('packages/images/4.jpg')}}" />
						</li>
				  </ul>
				</div>
			  </section>
			  </figure>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="contact-agent" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="gpc-modal-label"><h2 style="text-align:center;">Contact Agent</h2></div>
			</div>
			<div class="modal-body" id="mbody">
				<div class="row">
					
					<div class="col-md-12">
						<span class="vp"> I am </span>
						<span class="vp"><input type="radio" name="contact_type" value="individual" /></span>
						<span class="vp"> Individual </span>
						<span class="vp"><input type="radio" name="contact_type" value="agent" /></span>
						<span class="vp"> Agent </span>
						<br style="margin:5px;"/>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Name"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Mobile"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Email"/>
						</div>
						<div class="form-group">
						<input type="submit" class="btn btn-primary" name="Contact Agent" value="Contact Agent"/>
						</div>
						
						</div>
				
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="show-map" tabindex ="-1" role="dialog" aria-labelledby="zzz" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="zzz"></div>
			</div>
			<div class="modal-body" id="mbody">
				<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
					<div id="map" style="height:500px;width:100%"></div>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>

</div>




@stop
