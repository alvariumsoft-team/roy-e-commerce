@extends('adashb')

@section('dash_content')

<style type="text/css">

.box{margin:80px; padding:50px 30px;border: 1px solid #dedede;}

.plan-box{box-shadow:0px 0px 5px #dedede;padding:10px 0px;width:220px;margin:0px 10px;}

.plan-header{font-size:25px;font-weight:500;padding:1px;text-align:center;}

.plan-main{padding:50px 10px;margin:0px 0px;text-align:center;background:#f9f9f9;}

.plan-register{
background:#699d34;width:50px;height:50px;text-align:center;
text-decoration:none !important;
text-align: center;
box-sizing: border-box;
margin: 10px auto;
color: #fff !important;
border: none;
border-radius: 4px;
text-transform: capitalize;
padding: 10px 20px;
font-size: 13px;
background-color: #699d34;
background: -webkit-gradient(linear,left bottom,left top,color-stop(0,#699d34),color-stop(1,#83c143));}

.plan-footer{text-align:center;padding-top:20px;}

.bnr{height:15px;width:100%;background: -moz-linear-gradient(top,#4f97cd,#3b8bc7)}

</style>


<style type="text/css">

.stylr_cm{

text-align:center;
padding-top:20px;padding-bottom:12px;
border-right:1px solid #fff;

}

.stylr_cm > a{
	color:#fff;
	display: block;
}

.stylr_cm:hover{

background:rgba(0,0,0,0.3);
	
}

.m_head{
	margin-top: -20px;
	margin-right: -7px;
	margin-left: -6px;
}
 @media screen and (max-width: 980px) {
.Blue{padding-top:22px !important;}
 }
  @media screen and (max-width: 480px) {
 .plan-box{width:94% !important;}
  }
  @media screen and (max-width: 360px) {
	  
	.Blue{font-size:25px !important;padding-top: 31px !important;}
  }
</style>

   <section class="m_head">
           <div class="row" style="background:#444;">
               <div class="Blue col-lg-2 col-sm-6 col-xs-6" style="background:#14b192;text-align:center;padding-top:25px;padding-bottom:25px;font-size:30px;color:#fff;">
                       Membership
               </div>
				   
			   <div class="col-lg-2 col-sm-6 col-xs-6 stylr_cm" >
				<a href="#"><i style="font-size:30px;color:#fff;"class="fa fa-users"></i><p>Users</p></a>
			   </div>
			   
				<div class="col-lg-2 col-sm-6 col-xs-6 stylr_cm">
				<a href="#"><i style="font-size:30px;color:#fff;"class="fa fa-star"></i><p>Levels</p></a>
			   </div>
			   
				<div class="col-lg-2 col-sm-6 col-xs-6 stylr_cm">
				<a href="#"><i style="font-size:30px;color:#fff;"class="fa fa-money"></i><p>Transactions</p></a>
			   </div>
			   
				<div class="col-lg-2 col-sm-6 col-xs-6 stylr_cm">
				<a href="#"><i style="font-size:30px;color:#fff;"class="fa fa-gear"></i><p>Settings</p></a>
			   </div>
			   
			   <div class="col-lg-2 col-sm-6 col-xs-6 stylr_cm">
				<a href="#"><i style="font-size:30px;color:#fff;"class="fa fa-question-circle"></i><p>Help</p></a>
			   </div>
					
                <!--<img src="{{URL::to('packages/images/dashboard-underline.jpg')}}" style="width:100%;height:5px;">-->
           

              
</div>
</section>
 






<h2>Plans</h2>



<div class="row">

<div class="plan-box col-md-4 col-sm-6 col-xs-6">

<header class="plan-header">2 weeks trial</header>
<div class="bnr"></div>
<div class="plan-main">FREE</div>


<p class="plan-footer">{{HTML::link('register/?pid=1','Sign Up',array('class'=>'plan-register'))}}</p>


</div>


 
<div class="plan-box col-md-4 col-sm-6 col-xs-6">

<header class="plan-header">One Month</header>
<div class="bnr"></div>
<div class="plan-main">$9.99</div>


<p class="plan-footer">{{HTML::link('register/?pid=2','Sign Up',array('class'=>'plan-register'))}}</p>

</div>


</div>

	
@stop
