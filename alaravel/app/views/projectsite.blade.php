@extends('m')

@section('content')

<div class="container">
<div>
<img src="/packages/realestate/img/full/2.jpg" style="width:100%; height:auto;"/>
</div >


<div>

<div><h1>Name</h1></div>
<div>

{{$p->name}}

</div>
</div>


<div>

<div><h1>Description</h1></div>
<div>

{{$p->description}}

</div>
</div>

<div>

<div><h1>Unit Details</h1></div>


@foreach($u as $unit)

<div>

<div><h3>Unit Name</h3></div>
<div>{{$unit->name}}</div>

<div><h3>Unit Price</h3></div>
<div>{{$unit->price}}</div>

<div><h3>Unit Specifications</h3></div>
<div>{{$unit->specification}}</div>

<hr />

</div>

@endforeach


</div>

<div>

<div><h1>Image Gallery</h1></div>

<div class="property-listing">

<div class="heading"><h1></h1></div>

<div class="container">
@foreach($Index['property-list'] as $property)

<div class="property-block">
<div style="height:80%;width:100%;"><img src="{{$property['img']}}" class="property-image"/></div>
<div style="padding:5px;text-align:center;"><a href="#"><b>{{$property['name']}}</b></a></div>
<div style="padding:5px;text-align:center;">{{$property['name']}}</div>
</div>

@endforeach
</div>
<div class="clearfix"></div>
</div>

</div>

<div>

<div><h1>Map</h1></div>

</div>

<div>

<h1>Contact Us</h1>

<h3>Phone</h3>
<div>{{$p->phone}}</div>

<h3>E-Mail</h3>
<div>{{$p->email}}</div>

<h3>Website</h3>
<div>{{$p->website}}</div>

<h3>Address</h3>
<div>{{$p->address}}</div>

</div>
<h1></h1>
</div>

@stop