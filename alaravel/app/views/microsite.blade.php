<!doctype html>
<html>
<head>
<title>{{$project->name}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
{{HTML::style('packages/ad/bootstrap/css/bootstrap.min.css')}}
{{HTML::style('packages/css/flexslider.css')}}
{{HTML::style('packages/style.css')}}
<style>
.navbar-toggle{background-color: #15336e;}
.navbar-toggle .icon-bar{background-color:white;}
.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
	border-color: white;
}
.navbar-inverse .navbar-toggle{border-color: #15336e;}
.navbar-inverse{background-color:white;border-color:white;}
  @media screen and (max-width: 1024px) {
	  .back_ground{height:auto;}
  }
   @media screen and (max-width: 767px) { 
.floor-plan-im{
min-width: 100% !important ;
max-height: auto !important;
width: 100%;}
   .photos-modal{height:auto !important;}}
</style>
</head>
<body>


<?php $arr = array(); $arrr = array(); $prc = array(); $ara = array();?>

@foreach($units as $u)
@if(!in_array($u->beds, $arr))
<?php $arr[] = $u->beds; ?>
<?php $prc[] = $u->price; ?>
<?php $ara[] = $u->area; ?>
@endif
@endforeach

<style type="text/css">
	
	.nli{color:#6a6a6a !important;}
	.nli:hover{background:#fff !important;color:#1a1a1a !important;}
	
	.slidePanel{cursor:pointer;}
	.slide-minus{display:none;}
	.panel-body{display:none;}
	
	.image-ul{border-bottom: 2px solid #EF8600;}
	
	.image-types{
		text-align:center;
		display:block;
		color:#2a2a2a;
		font-size:18px;
		font-weight:700;
		padding-top:24px;
		outline:none !important;
	}
	
	.image-types > .fa{
		margin:0px 14px;
	}
	
	.image-types  {padding-left:25%;}
	
	.image-types{color:#4ba7e4;}
	.image-types:hover{color:#00a65a;}
	
	.bx-prev{background: rgba(0,0,0,0.2) !important;}
	.bx-next{background: rgba(0,0,0,0.2) !important;}
	.flexslider .slides img {height: 500px !important;}
	.slider{display:block;}
	.floor-plan-detail{display:block;}
	
	.degnava{padding:16px 24px;text-decoration:none !important;color:#4ba7e4;font-weight:700;}
	.degnava:hover{color:#00a65a;}
	
	
</style>

<section class="main-nav" style="background:#fff;padding:5px;position:fixed;z-index:100000;top:0px;width:100%;box-shadow:0 1px 5px rgba(0,0,0,1);">
	<div class="container">
		<div class="row">
		 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     @if(isset($project->logo))<img src="{{URL::to($project->logo)}}" height="55" width="140">@endif
	@if(isset($listing->logo))<img src="{{URL::to($listing->logo)}}" height="55" width="140">@endif
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" style="padding-top:10px 0;">
					<li><a style="color:#2a2a2a;" class="nli" href="#">Properties Available</a></li>
					<li><a style="color:#2a2a2a;" class="nli" href="#unit-details">Unit Details</a></li>
					<li><a style="color:#2a2a2a;" class="nli" href="#map-view">Map View</a></li>
					<li><a style="color:#2a2a2a;" class="nli" href="#image-gallery">Image Gallery</a></li>
					<li><a style="color:#2a2a2a;" class="nli" href="#about-us">About Us</a></li>
				</ul>
      <ul class="nav navbar-nav navbar-right">
        <a href="#contact-us"style="margin-top:10px;color:#fff !important;padding:7px !important;"class="btn btn-primary btn-default btn-block btn-danger"><i class="fa fa-envelope"></i>&nbsp;Click here to contact us</a>
      </ul>
    </div>
  </div>
</nav>
			
	  
		</div>
	</div>
</section>

<section style="margin-top:60px;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" >
    <!-- Indicators -->
    <ol class="carousel-indicators">
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox" style="margin-top:10px;">
      <div class="item active">
        @if(isset($project->header))<img style="height:100%;" src="{{URL::to($project->header)}}" alt=""> @endif
        @if(isset($listing->img))<img style="height:100%;" src="{{URL::to($listing->img)}}" alt=""> @endif
      </div>
    </div>
  </div>
</section>

<section class="back_ground">
	<div class="container">
		
		<div class="row clearfix">
		
		<div class="col-md-3 col-sm-3 col-xs-6">
			<div class="address-info">
			   <h2>Address</h2>
				<h3>{{$project->address}}</h3>
			 </div>
		</div>

		<div class="col-lg-2 col-sm-2 col-xs-6">
			<div class="address-info">
				<h2>Area Range</h2>
                <h3>{{min($ara)}} to {{max($ara)}} sqft</h3>
            </div>  
		</div>

		<div class="col-lg-2 col-sm-2 col-xs-6">
			<div class="address-info">
				<h2>Possession</h2>
				<h3>{{ucwords($project->possession_month)}} {{$project->possession_year}}</h3>
			</div>  
		</div>

		<div class="col-lg-2 col-sm-2 col-xs-6">
			<div class="address-info">
				<h2>Price</h2>
				<h3>${{min($prc)}} onwards</h3>
			</div>    
		</div>

		<div class="col-lg-3 col-sm-3 col-xs-6">
			<div class="address-info">
				<h2>Bedroom</h2>
				<h3><?php $arlen = count($arr);for($i=0;$i<$arlen;$i++){echo $arr[$i];if($i != ($arlen-1)){echo ',';}}?> BHK</h3>
			</div>  
		</div>

		</div>
		
		
	</div>
</section>


<div class="container">
<h2 style="text-align:center;">{{ucwords($project->name)}}</h2>
<section style="padding:20px 10px;">
	
	{{$project->description}}
	
</section>

<section>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
                Project Area
                 <span>{{$project->project_area}}</span>
            </p>	  
			</div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
              Total No. of Tower
                 <span>{{$project->towers}}</span>
            </p>  
			</div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
              Total No. of Floors
                 <span>{{$project->floors}}</span>
            </p>  
			</div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
               Total units
                 <span>{{$project->units}}</span>
            </p>  
			</div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
              Launch Date
                <span>{{ucwords($project->launch_month)}} {{$project->launch_year}}</span>
                  </p>	  
			</div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
			<p class="happy">
              Open Space in Project
                <span>{{$project->open_space}}%</span>
                   </p>	  
			</div>
		</div>
</section>

<section>
	<div class="" id="project-video">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span >Project Video</span></h2>
			</div>
		</div>
	</div>
</section>

<section>
    <div>
      <p>
		<iframe style="width:100%; height:500px;" src="{{$project->vlink}}" frameborder="0" allowfullscreen></iframe>
      </p>
    </div>
</section>

<section>
	<div class="" id="unit-details">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span >Unit Details</span></h2>

			</div>
		</div>
	</div>
</section>

@foreach($units as $u)

@if(!in_array($u->beds, $arrr))

<?php /*$arrr[] = $u->beds;*/ ?>

<div class="panel panel-default">
<div class="panel-heading clearfix">
<span class="pull-left"><b>{{$u->beds}} BHK {{ucwords($u->list_type)}}({{$u->area}} sqft) - ${{$u->price}}</b></span>
	
<i class="fa fa-plus slidePanel slide-plus pull-right"></i><i class="fa fa-minus slidePanel slide-minus pull-right"></i>
</div>
  <div class="panel-body">
	<section>
		<div class="row">
			<div class="col-md-3">
			<div class="bd-example bd-example-tabs" role="tabpanel">
			  <ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item active">
				  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;"><b>Floor Plan</b></a>
				</li>
			  </ul>
			  <div class="tab-content" id="myTabContent">
				<div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab" aria-expanded="false">
				  <p><img class="floor-plan-img" class="img-responsive" src="{{URL::to($u->floor_plan)}}" style="cursor:pointer;width:100%;height:auto;" ></p>
				</div>
			  </div>
			</div>
				<!--<img class="img-responsive" src="{{URL::to($u->floor_plan)}}" >-->
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-amenities" >Amenities</button>
				</div>
				<div class="form-group">
				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-spec">Specifications</button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4">
					<h4>Carpet Area</h4>
					<b>{{$u->area}}sqft</b>
					<br /><br />
					<h4>Furnished</h4>
					<b>{{ucwords($u->furnished)}}</b>
					</div>
					<div class="col-md-4">
					<h4>Price</h4>
					<b>${{$u->price}}</b>
					<br /><br />
					<h4>No. Of Balconys</h4>
					<b>{{$u->balconys}}</b>
					</div>
					<div class="col-md-4">
					<h4>Construction Status</h4>
					<b>@if($u->build_status == 'under') Under Construction @else {{ucwords($u->build_status)}} @endif</b>
					<br /><br />
					<h4>No. Of Bathrooms</h4>
					<b>{{$u->baths}}</b>
					</div>
				</div>
			</div>
		</div>
	</section>
  </div>
</div>

@endif
		
@endforeach	

<section>
	<div class="" id="image-gallery">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span >Image Gallery</span></h2>

			</div>
		</div>
	</div>
</section>

<section>
	<div class="">
		
		<div class="row">
			<div>
				
				@foreach($images as $image)
				<div class="four">
					<div class="col-lg-3 col-sm-3 col-xs-6" >
						<div class="five" style="margin-bottom:20px;">
							<p style="height:200px;"><img class="floor-plan-img img-responsive" src="{{URL::to($image->image)}}" style="width:100%;height:100%;cursor:pointer;"></p>
						</div>
					</div>
				@endforeach
				</div>
				
			</div>
		
	<div class="image-ul col-lg-12 col-sm-12 col-xs-12" style="padding:24px 12px;">
		<div class="row">
			<div class="col-lg-3 col-sm-3 col-xs-6">
			<a href="#" class="image-types image-photos" ><i class="material-icons pull-left">perm_media</i><span style="margin-left:8px;" class="pull-left">Images</span></a>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6">
			<a href="#" class="image-types image-plan" ><i class="material-icons pull-left">view_module</i><span style="margin-left:8px;" class="pull-left">Floor Plan</span></a>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6">
			<a href="#" class="image-types image-degree" ><i class="material-icons pull-left">loop</i><span style="margin-left:8px;" class="pull-left">360&deg; Images</span></a>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-6">
			<a href="#project-video" class="image-types clearfix"><i class="material-icons pull-left">video_library</i><span style="margin-left:8px;" class="pull-left">Video</span></a>
			</div>
		</div>
	</div>	
		
		</div>
	

	</div>
</section>

<section>
	<div class="" id="map-view">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span >Map View</span></h2>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
		<div id="map" style="height:500px;width:100%">
			
		</div>
	</div>
</section>

<section>
	<div class="" id="about-us">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span >About {{$project->parent_name}}</span></h2>
				<p class="line">{{$project->about}}</p>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="" id="contact-us">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="lined"><span>Contact with Advertiser</span></h2>
					<div class="row">
							<div class="col-lg-8">
								<div class="form">
									<form action="" method="post">
										<div class="form-group row">
										<label class="col-xs-12 col-md-3"><b>Name:</b></label>
										<label class="col-xs-12 col-md-9">{{Form::text('name','',array('class'=>'form-control','placeholder'=>'Name'))}}</label>
										</div>
										<input type="hidden" name="pid" value="{{$project->id}}" />
										<div class="form-group row">
										<label class="col-xs-12 col-md-3"><b>City:</b></label>
										<label class="col-xs-12 col-md-9">{{Form::text('city','',array('class'=>'form-control','placeholder'=>'City'))}}</label>
										</div>
										<div class="form-group row">
										<label class="col-xs-12 col-md-3"><b>Email:</b></label>
										<label class="col-xs-12 col-md-9">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'Email'))}}</label>
										</div>
										<div class="form-group row">
										<label class="col-xs-12 col-md-3"><b>City:</b></label>
										<label class="col-xs-12 col-md-9">{{Form::submit('Submit',array('class'=>'btn bt-large btn-primary','id'=>'contact-form-btn'))}}</label>
										</div>
									</form>
								</div>
							</div>
							<div class="col-lg-4">
							<div><img class="img-responsive" src="{{URL::to($project->logo)}}" ></div>
							<div>{{$project->address}}</div>
							</div>
					</div>
			</div>
		</div>
	</div>
</section>

</div>

<footer style="background:#333;color:#fff;height:40px;padding:10px;">
	
	<div style="text-align:center;"> &copy; @if(isset($project->name))
												{{$project->name}} 
											@endif  
											@if(isset($project->name)) 
												{{$project->name}} 
											@endif
	</div>
	
</footer>

<div class="modal fade" id="unit-extras-modal" tabindex ="-1" role="dialog" aria-labelledby="unit-extras-heading" aria-hidden="true" style="top:17%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="unit-extras-heading"></div>
			</div>
			<div class="modal-body" id="unit-extras-body"></div>
			
			<div class="modal-footer"></div>
		</div>
	</div>
</div>



<div class="modal fade" id="floor-plan-modal" tabindex ="-1" role="dialog" aria-labelledby="floor-plan-heading" aria-hidden="true" style="top:10%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="floor-plan-heading"></div>
			</div>
			<div class="modal-body" id="floor-plan-body">
				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="contact-form-modal" tabindex ="-1" role="dialog" aria-labelledby="floor-plan-heading" aria-hidden="true" style="top:10%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="contact-form-heading"><h4 style="text-align:center;color:#449d44;">Message Sent Successfully</h4></div>
			</div>
			<div class="modal-body" id="contact-form-body">
				<h3 style="text-align:center;color:#449d44;">Advertiser will contact you soon.</h3>
			</div>
			
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="photos-modal" style="position:fixed;transition:0.5s;top:0px;height:96%;width:98%;margin:1%;background:#fff;z-index:10000000;box-shadow: 1px 1px 5px #2a2a2a;display:none;">
	<div class="row">
		<div class="col-md-12 clearfix" >
			
			<div class="pull-left">
				<nav class="degnav clearfix" style="display:none;">
				  <a class="degnava pull-left" href="#">Front Yard</a>
				  <a class="degnava pull-left" href="#">Bedroom</a>
				  <a class="degnava pull-left" href="#">Storage</a>
				  <a class="degnava pull-left" href="#">Backyard</a>
				  <a class="degnava pull-left" href="#">Hall</a>
				  <a class="degnava pull-left" href="#">Living Room</a>
				  <a class="degnava pull-left" href="#">Kitchen</a>
				  <a class="degnava pull-left" href="#">Overflow</a>
				  <a class="degnava pull-left" href="#">Bathroom</a>
				</nav>
			</div>
			
			<div class="pull-right" style="padding:10px;">
				<a href="" class="sad">
					<img src="{{URL::to('packages/images/cancel.png')}}" style="width:25px;"/>
				</a>
			</div>
		</div>
		<div class="col-md-12" >
			
			<section class="slider" style="display:none;">
				<div class="flexslider">
				  <ul class="slides" id="slides-html">
					@foreach($images as $image)
					<li data-thumb="{{URL::to($image->image)}}"><img src="{{URL::to($image->image)}}" /></li>
					@endforeach
				  </ul>
				</div>
			</section>
			<section class="floor-plan-detail" style="display:none;">
				<div class="bd-example bd-example-tabs" role="tabpanel">
				  <ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item active">
					  <a class="nav-link" id="2d-tab" data-toggle="tab" href="#2d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>2D</b></a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" id="3d-tab" data-toggle="tab" href="#3d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>3D</b></a>
					</li>
				  </ul>
				  <div class="tab-content" id="myTabContent">
					<div role="tabpanel" class="tab-pane fade active in" id="2d" aria-labelledby="2d-tab" aria-expanded="false">
					  <p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floor_plan)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>
					</div>
					<div role="tabpanel" class="tab-pane fade  in" id="3d" aria-labelledby="3d-tab" aria-expanded="false">
						<p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floorplan3d)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>
					</div>
				  </div>
				</div>
			</section>
			<section class="degree" id="gdegree" style="display:none;">

			</section>
		</div>
	</div>
</div>
	
{{HTML::script('packages/realestate/js/jquery.js')}}
{{HTML::script('packages/js/jquery.flexslider.js')}}
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/three.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/CanvasRenderer.js')}}"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/Projector.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('.slidePanel').on('click', function(){
		
		$(this).siblings().css('display','block');
		$(this).css('display','none');
		
		if($(this).hasClass('slide-plus')){
			$('.panel-body').css('display','none');
			$('.slide-minus').css('display','none');
			$('.slide-plus').css('display','block');
		$(this).siblings().css('display','block');
		$(this).css('display','none');
			var panelBody = $(this).parent().next()[0];
			$(panelBody).css('display','block');
		}
		
		if($(this).hasClass('slide-minus')){var panelBody = $(this).parent().next()[0];$(panelBody).css('display','none');}
		
			
		});	

		$('.unit-extras').on('click', function(){

		var id = $(this).attr('id');
		var heading = '';
		var content = '';
		$('#unit-extras-heading').html(heading);
		$('#unit-extras-body').html(content);
		if(id == 'unit-amenities'){
			heading = 'Amenities';
			$.post("{{URL::to('projectspec')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});
		}
		if(id == 'unit-ladvantages'){
			heading = 'Local Advantages';
			
		}
		if(id == 'unit-usp'){
			heading = 'Project USP';
		}
		if(id == 'unit-spec'){
			heading = 'Specifications';
			$.post("{{URL::to('projectamenities')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});
		}

		$('#unit-extras-heading').html('<h4>'+heading+'</h4>');


		$('#unit-extras-modal').modal('show');	
			
		});

		$('.floor-plan-img').on('click',function(){
			
			$('#floor-plan-body').html($(this).parent().html());
			$('#floor-plan-modal').modal('show');	
			
		});

		latitude = <?php echo "'$project->map_lat'" ?>;
		longitude = <?php echo "'$project->map_long'" ?>;
		
		
		if((latitude == '') || (latitude == undefined)){
			latitude	= 28.7041;
			longitude	= 77.1025;
		}
		
		setTimeout(function() {
			
		
		latitude = parseFloat(latitude);
		longitude= parseFloat(longitude);

			var uluru = {lat: latitude, lng: longitude};
			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			  scrollwheel: false,
			  center: uluru
			});
			var marker = new google.maps.Marker({
			  position: uluru,
			  map: map
			});
			
		}, 50);
		
		$('#contact-form-btn').on('click', function(e){
			e.preventDefault();
			var url = '';
			//$.post(url, {}).done(function(data){
			
			$('#contact-form-modal').modal('show');	
				
			//}
			
			
		});

		$('.image-photos').on('click', function(e){
			e.preventDefault();
				$('.slider').css('opacity', '0');
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				});
				$('.photos-modal').css('display', 'block');
				$('.slider').css('display', 'block');
				setTimeout(function(){$('.slider').css('opacity', '1');}, 300);
				$('.image-photos').focus();
		});
		
		$('.image-plan').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'block');
			$('.floor-plan-detail').css('display', 'block');
			$('.image-photos').focus();	
		});
		
		$('.image-degree').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'block');
			$('.degree').css('display', 'block');	
			$('.degnav').css('display', 'block');
			$('.gcanvas').focus();
		});
		
		$('.sad').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'none');
			$('.slider').css('display', 'none');
			$('.floor-plan-detail').css('display', 'none');
			$('.degree').css('display', 'none');
			$('.degnav').css('display', 'none');
		});

	});
	
</script>

<script>

	var cimagedir = "{{URL::to('public/canvas')}}";

	var camera, scene, renderer;

	var texture_placeholder,
	isUserInteracting = false,
	onMouseDownMouseX = 0, onMouseDownMouseY = 0,
	lon = 90, onMouseDownLon = 0,
	lat = 0, onMouseDownLat = 0,
	phi = 0, theta = 0,
	target = new THREE.Vector3();
	
	var materials = [

		loadTexture( cimagedir+'/skybox/px.jpg' ), // right
		loadTexture( cimagedir+'/skybox/nx.jpg' ), // left
		loadTexture( cimagedir+'/skybox/py.jpg' ), // top
		loadTexture( cimagedir+'/skybox/ny.jpg' ), // bottom
		loadTexture( cimagedir+'/skybox/pz.jpg' ), // back
		loadTexture( cimagedir+'/skybox/nz.jpg' )  // front

	];
	
	init(materials);
	animate();

	function init() {

		var container, mesh;

		container = document.getElementById( 'gdegree' );

		camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1100 );

		scene = new THREE.Scene();

		texture_placeholder = document.createElement( 'canvas' );
		texture_placeholder.width = 128;
		texture_placeholder.height = 128;

		var context = texture_placeholder.getContext( '2d' );
		context.fillStyle = 'rgb( 200, 200, 200 )';
		context.fillRect( 0, 0, texture_placeholder.width, texture_placeholder.height );



		mesh = new THREE.Mesh( new THREE.BoxGeometry( 300, 300, 300, 7, 7, 7 ), new THREE.MultiMaterial( materials ) );
		mesh.scale.x = - 1;
		scene.add( mesh );

		renderer = new THREE.CanvasRenderer();
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( (window.innerWidth * 0.968), 500);
		container.appendChild( renderer.domElement );

		document.addEventListener( 'mousedown', onDocumentMouseDown, false );
		document.addEventListener( 'mousemove', onDocumentMouseMove, false );
		document.addEventListener( 'mouseup', onDocumentMouseUp, false );
		document.addEventListener( 'wheel', onDocumentMouseWheel, false );

		document.addEventListener( 'touchstart', onDocumentTouchStart, false );
		document.addEventListener( 'touchmove', onDocumentTouchMove, false );

		//

		window.addEventListener( 'resize', onWindowResize, false );

	}

	function onWindowResize() {

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( (window.innerWidth * 0.968), 500);

	}

	function loadTexture( path ) {

		var texture = new THREE.Texture( texture_placeholder );
		var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );

		var image = new Image();
		image.onload = function () {

			texture.image = this;
			texture.needsUpdate = true;

		};
		image.src = path;

		return material;

	}

	function onDocumentMouseDown( event ) {

		event.preventDefault();

		isUserInteracting = true;

		onPointerDownPointerX = event.clientX;
		onPointerDownPointerY = event.clientY;

		onPointerDownLon = lon;
		onPointerDownLat = lat;

	}

	function onDocumentMouseMove( event ) {

		if ( isUserInteracting === true ) {

			lon = ( onPointerDownPointerX - event.clientX ) * 0.1 + onPointerDownLon;
			lat = ( event.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;

		}

	}

	function onDocumentMouseUp( event ) {

		isUserInteracting = false;

	}

	function onDocumentMouseWheel( event ) {

		camera.fov += event.deltaY * 0.05;
		camera.updateProjectionMatrix();

	}

	function onDocumentTouchStart( event ) {

		if ( event.touches.length == 1 ) {

			event.preventDefault();

			onPointerDownPointerX = event.touches[ 0 ].pageX;
			onPointerDownPointerY = event.touches[ 0 ].pageY;

			onPointerDownLon = lon;
			onPointerDownLat = lat;

		}

	}

	function onDocumentTouchMove( event ) {

		if ( event.touches.length == 1 ) {

			event.preventDefault();

			lon = ( onPointerDownPointerX - event.touches[0].pageX ) * 0.1 + onPointerDownLon;
			lat = ( event.touches[0].pageY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;

		}

	}

	function animate() {

		requestAnimationFrame( animate );
		update();

	}

	function update() {

		if ( isUserInteracting === false ) {

			lon += 0.1;

		}

		lat = Math.max( - 85, Math.min( 85, lat ) );
		phi = THREE.Math.degToRad( 90 - lat );
		theta = THREE.Math.degToRad( lon );

		target.x = 500 * Math.sin( phi ) * Math.cos( theta );
		target.y = 500 * Math.cos( phi );
		target.z = 500 * Math.sin( phi ) * Math.sin( theta );

		camera.lookAt( target );

		renderer.render( scene, camera );

	}

</script>

</body>
</html>


