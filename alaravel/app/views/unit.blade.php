@extends('adashb')

@section('dash_content')

<form action="" method="post" enctype="multipart/form-data">

<div class="form-group">{{Form::select('project',[],'BHK',array('class'=>'form-control','placeholder'=>'Project Name'))}}</div>

<div class="row"><!-- row start -->

<div class="col-md-3">
<div class="form-group">{{Form::select('bhk',array('BHK','1','2','3','4','5','6','7','8','9','10'),'BHK',array('class'=>'form-control','placeholder'=>'bhk'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::select('balconys',array('No. Of Balcony',1,2,3),0,array('class'=>'form-control'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::select('bathrooms',array('No. Of Bathroom','1','2','3','4','5','6','7','8','9','10'),'null',array('class'=>'form-control'))}}</div>
</div>

<div class="col-md-3">
<div class="form-group">{{Form::selectMonth('month','null',array('class'=>'form-control'))}}</div>
</div>


</div> <!-- row end -->

<div class="row"> <!-- row start -->

<div class="col-md-3">
<div class="form-group"> {{Form::text('furnished','',array('class'=>'form-control','placeholder'=>'Furnished'))}}</div>
</div>

<div class="col-md-4">
<div class="form-group">{{Form::text('carpet_area','',array('class'=>'form-control','placeholder'=>'Carpet Area'))}}</div>
</div>


<div class="col-md-5">
<div class="form-group">{{Form::text('price','',array('class'=>'form-control','placeholder'=>'Price'))}}</div>
</div>

</div> <!-- row end -->

<!--
<div class="col-md-1">

<button type="button" id="spec-add" class="spec_add"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>

<div></div>
-->

<div>
<div>
<div class="form-group">{{Form::textarea('specification','',array('class'=>'form-control ','id'=>'spec','placeholder'=>'Specification'))}}</div>

</div>


</div>

<div>
<div class="form-group">{{Form::textarea('local_advantages','',array('class'=>'form-control','placeholder'=>'Local Advantage'))}}</div>
</div>



<div>
<div class="form-group">{{Form::textarea('amenities','',array('class'=>'form-control','placeholder'=>'Ameneties'))}}</div>
</div>

<div>
<div class="form-group">{{Form::button('Add Image',array('class'=>'btn','placeholder'=>'Image'))}}</div>
</div>

<div class="form-group">{{Form::submit('Submit Unit',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>

@stop
