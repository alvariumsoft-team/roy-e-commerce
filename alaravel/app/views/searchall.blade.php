@extends('searchm')
@section('search_content')

<form action="" method="post">

<div class="row">

<div class="col-md-4">
{{Form::select('search',array('','company'=>'Company','builder'=>'Builder','project'=>'Project'),'',array('class'=>'form-control'))}}
</div>

<div class="col-md-4">
{{Form::select('unit',array('','1 BHK','2 BHK','3 BHK','4 BHK','5 BHK'),'',array('class'=>'form-control'))}}
</div>

<div class="col-md-4">
{{Form::submit('search',array('class'=>'btn btn-block btn-primary'))}}
</div>

</div>

</form>

@if(isset($vars))
<h1></h1>
<div class="list-group">

@foreach($vars as $var)

<div class="list-group-item">
@if(isset($var->name))
{{$var->name}}
@else
{{$var->username}}
@endif


@foreach($units[$var->name] as $unit)

@foreach($unit as $u)

<div class="list-group-item"> {{$u->name}}</div>

@endforeach

@endforeach

</div>

@endforeach

</div>
@endif

@stop