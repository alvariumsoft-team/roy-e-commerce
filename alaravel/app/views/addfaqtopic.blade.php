@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add Frequently Asked Question Topic</h2>

<p>Add a topic.</p>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Topic:</b></label>
<label class="col-xs-12 col-md-6">{{Form::text('topic','',array('class'=>'form-control','placeholder'=>'FAQ Topic','required'=>'true'))}}</label>
</div>

<div class="form-group">

{{Form::submit('ADD',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>


@stop
