@extends('main')

@section('content')

<style type="text/css">

/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
.carousel {
  height: 600px;
  margin-bottom: 0px;
}
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
  z-index: 10;
}

/* Declare heights because of positioning of img element */
.carousel .item {
  height: 600px;
  background-color: #777;
}
.carousel-inner > .item > img {
  position: absolute;
  top: 0;
  left: 0;
  min-width: 100%;
  height: 600px;
}

/* RESPONSIVE CSS
-------------------------------------------------- */

@media (min-width: 768px) {
  /* Navbar positioning foo */
  .navbar-wrapper {
    margin-top: 20px;
  }
  .navbar-wrapper .container {
    padding-right: 15px;
    padding-left: 15px;
  }
  .navbar-wrapper .navbar {
    padding-right: 0;
    padding-left: 0;
  }

  /* The navbar becomes detached from the top, so we round the corners */
  .navbar-wrapper .navbar {
    border-radius: 4px;
  }

  /* Bump up size of carousel content */
  .carousel-caption p {
    margin-bottom: 20px;
    font-size: 21px;
    line-height: 1.4;
  }

  .featurette-heading {
    font-size: 50px;
  }
}

@media (min-width: 992px) {
  .featurette-heading {
    margin-top: 120px;
  }
}


.carousel-fade .carousel-inner .item{

opacity: 0;
-moz-transition-property:opacity;
transition-property:opacity;
transition : opacity 1s;
}

.carousel-fade .carousel-inner .active{
opacity:1;
}

.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right{
left:0;
opacity:0;
z-index:1;
}

.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right{
opacity:1;
}

.carousel-fade .carousel-control{
z-index:2;
display: none;
}

.ssearch{
position:absolute;

width:76%;
padding:1%;
top:23%;
left:12%;
//background:rgba(0,0,0,0.4);
z-index:10000;
}

.search-nav-tab-li > a{
padding:9px !important;	
}

.ssearch-main{


background:#fff;
width:100%;

}

.ssearch-li{
float:left;
height:40px;width:25%;
border:1px solid #1a1a1a;
  display: inline-block;
  position: relative;
  z-index: 100;
  line-height:30px;
  padding:5px;
}

.ssearch-select{width:15%;}

.ssearch-button{width:10%;text-align:center;background:rgba(255,0,0,0.8);line-height:1.75em;cursor:pointer;}

.ssearch-menu{display:block;}

.ssearch-ul {
  visibility: hidden;
  opacity: 0;
  margin: 0;
  padding: 0;
  width:100%;
  position: absolute;
  left: 0px;
  background: #fff;
  z-index: 99;
  padding:10px;
}



.cpro-head{margin:0px;padding:0px;line-height:1em !important;font-weight:500;}

.cproc{float:left;padding:5px;}

.ssji{width:98%;}

.ssearch-li-li{border-bottom:1px solid #dedede;}

@media(max-width:786px){

.ssearch{
position:absolute;
z-index:100000000;
width:100%;
padding:1%;
top:1%;
left:0%;
background:rgba(0,0,0,0.4);
}
.ssearch-ul{width:150px;}
.header{background: #29333d !important;position:relative !important;left:0% !important;}
.contact-phone-m{display:none;}
.login{float:right;}

}

.sss{width:10%;}
.ssi{width:29.6%;margin-right:0.4%;color:#2d2d2d !important;}
.ssb{width:10%;background:#fff;}

.sss > select{width:100%;height:35px;}
.ssi > input{width:100%;height:40px;border:0px;padding-left:10px;}
//.ssb > button{width:100%;height:40px;background:rgba(255,0,0,0.7);border:1px solid rgba(255,0,0,0.4);color:rgba(0,0,255,0.8);}
.ssb > button{width:100%;height:40px;
border:0px;
background: #4ba7e4;
border-color: #4ba7e4;
border-bottom-color: #2980b9;

}

.ssi:focus{box-shadow:none;}
.ui-menu-item{position:absolute;z-index:10000000;}
.ui-autocomplete{position:absolute;z-index:10000000 !important;}
.pul{position:absolute;width:270px;background:#fff;z-index:1000;transition:2s;display:none;}
.ssi-pro > .pul{padding:10px;width:320px;}
.ssi:hover > .pul{display:block;}

a{outline:none !important;}
.tab-content{background:rgba(0,0,0,0.4);}
.nav-tabs{border-bottom:0px;}
.nav-tabs > li{background:rgba(255,255,255,0.9) !important;margin:0px 5px !important;padding-right:5px !important;padding-left:8px !important;}
.nav-tabs > .active{background:rgba(0,0,0,0.4)!important;color:#ccc;}
.nav-tabs > .active{background:rgba(0,0,0,0.7)  !important;color:#ccc;}
.nav-tabs > li > a{border:0px solid #292929!important;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{background:transparent !important;border-radius:0px !important;color:#ccc !important;}
//.nav-tabss > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{background:rgba(0,0,0,0.4)  !important;border:0px!important; color:#ccc;border-bottom:1px solid rgba(0,0,0,0.7);}
.pul{color:#292929;}

</style>




    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
	

@if(Auth::check())


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />

<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function(){
/*
$('#location').autocomplete({

source:['abcd','accd','addd']

});
*/
$('#propertyy').click(function(){

$('div.pul').toggleClass('hidden');

});

$("#results").insertAfter('.ui-autocomplete');


});

function init(){

var location = $('#property');

var autocomplete =  new google.maps.places.Autocomplete(location);

}

google.maps.event.addDomListener(window,'load',init);

</script>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3"></script>


<div class="ssearch left">


<div class="head_text_to_search" style="text-align: center;color: #fff;font-size: 35px;margin-bottom: 5%;font-family: lato;">
		
		Worlds No.1 All In One Property Site
		
	</div>



	<form class="ssform" action="search" method="post">
		<input type="hidden" name="country" />
		<input type="hidden" name="city" />
	<input type="hidden" name="own_type" id="own_type" value="" />
	<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs" > 
	<ul class="nav nav-tabs" id="myTabs" role="tablist"> 
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Buy</a></li> 
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Rent</a></li> 
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Project</a></li>
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Agent</a></li>  
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Newly Built</a></li>  
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Recreation</a></li>  
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Luxury Projects</a></li>  
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Commercial</a></li>  
	<li role="presentation" class="search-nav-tab-li"><a class="search-nav-tab" href="#home" role="tab" id="profile-tab" data-toggle="tab" aria-controls="home">Agricultural</a></li>  
	</ul> 
	<div class="tab-content" id="myTabContent"> 
	<div class="tab-pane fade in active" role="tabpanel" id="home" aria-labelledby="home-tab"> 
	<div style="padding:10px;">

	<div class="ssi left">
	<input type="text" name="location" id="location" placeholder="Location" />
	</div>
	<div class="ssi left ssi-pro">
	<input type="text" name="property" id="property" disabled="true" value="Property Type"/>

	<div class="pul">


	<div id="ssearch-li-li-pro" >
	<div class="cpro-head"><b>Residential</b></div>
	<div class="cpro">
	<div class="cproc"><input type="checkbox" class="ssj" name="ptype[]" value="Apartment" />&nbsp;Flat</div>
	<div class="cproc"><input type="checkbox" class="ssj" name="ptype[]" value="house"/>&nbsp;House/Villa</div>
	<div class="cproc"><input type="checkbox" class="ssj" name="ptype[]" value="land"/>&nbsp;Plot/Land</div>
	<div class="clear"></div></div>
	</div>

	<div id="ssearch-li-li-pro" >
	<div class="cpro-head"><b>Commercial</b></div>
	<div class="cpro">
	<div class="cproc"><input type="checkbox" class="ssj"  name="office"/>&nbsp;Office Space</div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="shop"/>&nbsp;Shop/Showroom</div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="commercial-land"/>&nbsp;Commercial Land </div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="warehouse"/>&nbsp;Warehouse/Godown</div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="industrial-building"/>&nbsp;Industrial Building</div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="industrial-shed"/>&nbsp;Industrial Shed</div>
	<div class="clear"></div>
	</div>
	</div>


	<div id="ssearch-li-li-pro" >
	<div class="cpro-head"><b>Others</b></div>
	<div class="cpro">
	<div class="cproc"><input type="checkbox" class="ssj"  name="agriculture-land"/>&nbsp;Agriculture Land</div>
	<div class="cproc"><input type="checkbox" class="ssj"  name="farm-house"/>&nbsp;Farm House </div>
	<div class="clear"></div>
	</div>
	</div>


	</div>

	</div>

	<div class="ssi left">
	<input type="text" name="budget" disabled="true" value="Budget"/>



	<div class="pul">

	<div >

	<div class="left" style="width:50%;">

	<input type="text" style="width:100%;" name="bmin" placeholder="Min"/>

	<div class="budget-suggestions"><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>

	</div>

	<div class="left" style="width:50%;">
	<input type="text" style="width:100%;" name="bmax" placeholder="Max"/>

	<div class="budget-suggestions"><br /><br /><br /><br /><br /><br /><br /><br /><br /></div>

	</div>

	</div>

	<div class="clear"></div>

	</div>

	</div>

	<div class="ssb left">
	<button type="submit" id="search-button"><i class="fa fa-search"></i></button>
	</div>
	<div class="clear"></div>

	</div> 
	</div> 
	<div class="tab-pane fade" role="tabpanel" id="profile" aria-labelledby="home-tab"> 
	<p>

	</p> 
	</div> 
	</div> 
	</div>

	</form>

<div class="" style="text-align:center;margin-top:10px;color:#ccc;background:rgba(0,0,0,0.356);margin:auto;width:400px;">
	or search by<a href="{{URL::to('mapsearch')}}" target="_blank" style="padding:0px 10px;color:#f9f9f9;">Map Search</a>|<a href="{{URL::to('travelsearch')}}" target="_blank" style="padding:0px 10px;color:#f9f9f9;">Travel Time Search</a>
</div>

</div>


@endif
	
	
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
	  

	  
        <div class="item active">
		{{HTML::image("packages/realestate/images/1.jpg",'First slide',array('class'=>'first-slide'))}}
          <div class="container">
            <div class="carousel-caption">
             <!-- <h1>Example headline.</h1>
              <p>Dummy Dummy</p>
              <p><a class="btn btn-lg btn-primary" href="register" role="button">Sign up today</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          {{HTML::image("packages/realestate/images/2.jpg",'Second slide',array('class'=>'second-slide'))}}
          <div class="container">
            <div class="carousel-caption">
            <!--  <h1>Another example headline.</h1>
              <p>Dummy Dummy</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>-->
            </div>
          </div>
        </div>
        <div class="item">
          {{HTML::image("packages/realestate/images/3.jpg",'Second slide',array('class'=>'second-slide'))}}
          <div class="container">
            <div class="carousel-caption">
              <!--<h1>One more for good measure.</h1>
              <p>Dummy Dummy</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>-->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
</div><!-- /.carousel -->


<section class="imagesd">
<div class="imagesd-cover">
     <div class="container">
         <div class="row">
             <div class="col-lg-4">
                 <div class="portal_main">
                     <h3>MOST POPULAR</h3>
                     <h4>New Projects</h4>
                     <p> Upcoming projects in your budget</p>
                     <h4>Luxury Living</h4>
                     <p> Assortment of iconic lifestyle addresses</p>
                     <h4>Investment Hotspots</h4>
                     <p> Know the areas that promise best returns</p>
                 </div>
              </div>
                <div class="col-lg-4">
                 <div class="portal_main">
                     <h3>TOOLS</h3>
                     <h4>Calculators</h4>
                     <p>Know the loan amount you are eligible for</p>
                     <h4>Home Worth</h4>
                     <p>Know the value of any property on the go</p>
                     <h4>Smart Search</h4>
                     <p>Localities that offer home in your budget</p>
                 </div>
              </div>
                <div class="col-lg-4">
                 <div class="portal_main">
                     <h3>RESEARCH</h3>
                     <h4>Home Buying Guide</h4>
                     <p>India's first e-book on How to Buy a House</p>
                     <h4>Rates & Trends</h4>
                     <p>Track & compare property prices</p>
                     <h4>Finance/Legal</h4>
                     <p>Get Advice from Experts</p>
                 </div>
              </div>
         </div>
     </div>
     </div>
</section>







<section class="decide" style="background:#fff;">
     <div class="container">
         <div class="row">
          <div class="propetyd">
             <div class="col-lg-12">
             <h1>Get property advice before you decide</h1>
            </div>
          </div>
             <div class="col-lg-3">
                  <div class="searchibon">
                 <img src="packages/images/search.png">
                 <h4>Smart Search</h4>
                  <p>Explore your options, find here what your money can buy </p>
                  <div class="proAdviceLink">Compare</div>
             </div>
            </div>
                         <div class="col-lg-3">
                  <div class="searchibon">
                 <img src="packages/images/lagan.png">
                 <h4>Smart Search</h4>
                  <p>Explore your options, find here what your money can buy </p>
                  <div class="proAdviceLink">Compare</div>
             </div>
            </div>
                         <div class="col-lg-3">
                  <div class="searchibon">
                 <img src="packages/images/compare.png">
                 <h4>Smart Search</h4>
                  <p>Explore your options, find here what your money can buy </p>
                  <div class="proAdviceLink">Compare</div>
             </div>
            </div>
                         <div class="col-lg-3">
                  <div class="searchibon">
                 <img src="packages/images/calculate.jpg">
                 <h4>Smart Search</h4>
                  <p>Explore your options, find here what your money can buy </p>
                  <div class="proAdviceLink">Compare</div>
             </div>
            </div>
         </div>
     </div>
</section>



<section id="rate" class="rate" >
     <div class="container">
         <div class="row">
		 
		@foreach($Index['listings'] as $listing)
		 
             <div class="col-md-4">
			 <div class="list-main">
			 
				<a href="listingsite/{{$listing['id']}}">{{HTML::image($listing['img'],'alt',array('height'=>'250px','width'=>'100%'))}}</a>
                
                
                 <div class=" center" style="padding-top:10px;">
                    <a class="list-link" href="listingsite/{{$listing['id']}}">{{ucwords($listing['name'])}}</a>
                    <p class="list-desc">{{$listing['location'] !== "" ?  $listing['location'] : 'Location Not Available'}}</p>
                 </div>
				@if(Auth::check())
					<div class="list-price center" style="padding:2px!important;"><h4 style="margin:10px">${{$listing['price']}}</h4></div>
					<div class="list-head">

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Beds</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['beds']}}</div>

					</div>
					</div>

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Baths</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['baths']}}</div>

					</div>
					</div>

					<div class="list-info">
					<div class="row">

					<div class="col-xs-6 col-sm-6 col-md-6 list-left">Sq. Ft.</div>
					<div class="col-xs-6 col-sm-6 col-md-6 list-right">{{$listing['area']}}</div>

					</div>
					</div>


					</div>
					@endif 
				
			  </div>
             </div>
			 
		@endforeach
		
			 
             <div class="col-lg-12">
              <div class="property">
                 <div class="proAdviceLinks">{{HTML::link('listings','view all properties',array('class'=>'list-link','style'=>'color:#fff;'))}}</div>
               </div>
             </div>
         </div>
     </div>
</section>
<section class="group" >
    <div class="container">
         <div class="row">
          <div class="propetyds">
             <div class="col-lg-12">
                <h1>What makes us the preferred choice</h1>
            </div>
          </div>
          <div class="col-lg-3">
               <div class="groupimgs">
                   <img src="packages/images/group1.png">
                   <h3>Trusted Times Group</h3>
                   <p>We are a trusted Times Group company, an institution that speaks of quality and reliability</p>
               </div>
          </div>
                    <div class="col-lg-3">
               <div class="groupimgs">
                   <img src="packages/images/group2.png">
                   <h3>Sellers Prefer us</h3>
                   <p>27,000 new properties posted daily, making us the biggest platform to sell & rent properties</p>
               </div>
          </div>
                    <div class="col-lg-3">
               <div class="groupimgs">
                   <img src="packages/images/group3.png">
                   <h3>Buyers trust us</h3>
                   <p>12 million users visit us every month for their buying and renting needs</p>
               </div>
          </div>
                    <div class="col-lg-3">
               <div class="groupimgs">
                   <img src="packages/images/group4.png">
                   <h3>Expert Guidance</h3>
                   <p>Advice from the largest panel of Industry experts to make smart property decisions</p>
               </div>
          </div>
         </div>
    </div>
</section>
<section class="online" >
     <div class="container">
          <div class="row">
               <div class="col-lg-12">
                   <div class="online_main">
                        <h1>Connect with Industry Experts Online</h1>
                   </div>
               </div>
               <div class="col-lg-4">
                   <div class="india_cost">
                       <h4>Will GST reduce cost of property in India</h4>
                       <img src="packages/realestate/img/expert.jpg">
                       <div class="expOnlineName">Expert</div>
                       <div class="expOnlineGroup">Former CMD, Company</div>
                       <div class="expOnlineDate">August 1, 2016</div>
                       <div class="expOnlineLink">Know More</div>
                   </div>
               </div>
                              <div class="col-lg-4">
                   <div class="india_cost">
                       <h4>What Ghaziabad residents can expect...</h4>
                       <img src="packages/realestate/img/expert.jpg">
                       <div class="expOnlineName">Expert</div>
                       <div class="expOnlineGroup">Vice Chairman, Company</div>
                       <div class="expOnlineDate">February 1, 2016</div>
                       <div class="expOnlineLink">Read Transcript</div>
                   </div>
               </div>
                              <div class="col-lg-4">
                   <div class="india_cost">
                       <h4>Will GST reduce cost of property in India</h4>
                       <img src="packages/realestate/img/expert.jpg">
                       <div class="expOnlineName">Expert</div>
                       <div class="expOnlineGroup">Co-Founder</div>
                       <div class="expOnlineDate">June 1, 2016</div>
                       <div class="expOnlineLink">Know More</div>
                   </div>
               </div>
          </div>
     </div>
</section>





<section class="tabsnavigat" >
<div class="container">
    <div class="row">
    <div class="col-md-12">
      <div class="calculator_main">
           <h3>Home Loan Calculators</h3>
          </div>
      <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">
            <li class="active activd_main">
              <a href="#tab_default_1" data-toggle="tab">Loan Eligibility </a>
              <p>How much loan you are eligible for?</p>
            </li>
            <li class="activd_main">
              <a href="#tab_default_2" data-toggle="tab">EMI</a>
              <p>Find out how much EMI you have to pay</p>
            </li>
            <li class="activd_main">
              <a href="#tab_default_3" data-toggle="tab">Prepayment </a>
              <p>Should you prepay your loan?</p>
            </li>
            <li class="activd_main">
            <a href="#tab_default_4" data-toggle="tab">Rate Change</a>
            <p>Effect of Change in Rate of Interest</p>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
                <div class="col-lg-6">
                     <div class="form_row">
                         <div class="formlabel">
                             <p>Home Loan Required</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Net income per month</p>
                             <span>Excluding LTA and Medical allowances</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                        <div class="form_row">
                         <div class="formlabel">
                             <p>Existing loan commitments</p>
                             <span>(per month)</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                          <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Tenure</p>
                             <span>(in years)</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Rate of Interest</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                             <span class="modularIcon">%</span>
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                         </div>
                         <div class="buttom_search">
                             <input type="button" onclick="submitLoanEligibilityCalculator(true)" value="Check Eligibility" class="btnAdvFinance">
                             <span class="resetAll"><a href="javascript: resetEligibilityCalculator()">Reset All</a></span>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-6">
                      <div id="iconId" class="resultHLEligibLeft">
                      <span class="rightIcon"></span>
                         <div class="loan_main">
                             <div class="loan_main_classr">
                                 <p>You are Eligible for this loan</p>
                             </div>
                             <div class="emis">
                                 <span class="rupeeIcon4"></span>
                                 <span id="loanAmtResultDiv">15.0 Lac at EMI </span>
                                 <span class="rupeeIcon4 marginL15"></span>
                                 <span id="emiResultDiv">19,822</span>
                             </div>
                             <div class="emiss">
                                 <div id="maxEligbLoanDiv">You are Eligible for a maximum loan of</div>
                                 <span class="rupeeIcon3"></span>
                                 <span id="maxLoanAmtResultDiv">15.1 Lac at EMI </span>
                                 <span class="rupeeIcon3 marginL7"></span>
                                 <span id="maxAmtEmiResultDiv">20,000</span>
                             </div>
                             <div class="buttom_apply">
                                 <a class="btnAdvFinance2" target="_blank" href="http://www.magicbricks.com/apply-for-loan">Apply for a loan</a>
                             </div>
                         </div>
                    </div>
                  </div>
                </div>
            <div class="tab-pane" id="tab_default_2">
                <div class="col-lg-6">
                     <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Amount</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Tenure</p>
                             <span>(in years)</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                        <div class="form_row">
                         <div class="formlabel">
                             <p>Rate of Interest</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                            <span class="modularIcon">%</span>
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                         </div>
                         <div class="buttom_search">
                         <input type="button" onclick="submitEMICalculator(true)" value="Calculate EMI" class="btnAdvFinance">
                             <span class="resetAll"><a href="javascript: resetEligibilityCalculator()">Reset All</a></span>
                         </div>
                     </div>
                 </div>
                  <div class="col-lg-6">
                       <div class="rupess">
                         <div class="loan_mains">
                              <div class="emiTitle">Monthly EMI</div>
                              <div class="month">
                                  <span class="rupeeIcon5"></span>
                                  <span id="emiSpan">33,038</span>
                              </div>
                             </div>
                             <div class="buttom_applys">
                                 <a class="btnAdvFinance2" target="_blank" href="http://www.magicbricks.com/apply-for-loan">Apply for a loan</a>
                             </div>
                         </div>
                       </div>
            </div>
            <div class="tab-pane" id="tab_default_3">
                <div class="col-lg-6">
                     <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Amount</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Tenure</p>
                             <span>(in years)</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                        <div class="form_row">
                         <div class="formlabel">
                             <p>Rate of Interest</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                             <span class="modularIcon">%</span>
                         </div>
                     </div>
                          <div class="form_row">
                         <div class="formlabel">
                             <p>Installments Paid</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Prepayment Amount</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                     <div class="form_rowlf">
                         <div class="formlabel">
                         </div>
                         <div class="buttom_searchhv">
                             <span class="customCheckBox">
                             <input type="checkbox" onclick="customCheckBox(this);togglePartPayment();" id="foreclosureId" name="foreclosure" style="opacity: 0;"></span>
                          <label for="foreclosureId">Foreclosure</label>
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                         </div>
                         <div class="buttom_search_rest">
                            <input type="button" onclick="submitPrepaymentCalculator(true)" value="Calculate" class="btnAdvFinance">
                             <span class="resetAll"><a href="javascript: resetEligibilityCalculator()">Reset All</a></span>
                         </div>
                     </div>
                 </div>
                <div class="col-lg-6">
                       <div class="rupessd">
                         <div class="loan_mains">
                            <div id="interestSavedDiv" class="resultHeading">You will save Interest of <span class="rupeeIcon5"></span> 92,576</div>
                             <div id="emiTenureReductionDiv" class="resultHeading_2">EMI Tenure will be reduced by 5 Months</div>
                             <div class="perybox">
                                 <h4>Current</h4>
                                 <div class="boxrow">
                                     <div class="labelDiv">Interest</div>
                                     <div id="currInterestPayableDiv" class="valueDiv"><span class="rupeeIcon7"></span> 14,64,522</div>
                                 </div>
                                    <div class="boxrow">
                                     <div class="labelDiv">EMI Tenure</div>
                                     <div id="currInterestPayableDiv" class="valueDiv"><span class="rupeeIcon7"></span>10 Years</div>
                                 </div>
                             </div>
                             <div class="perybox">
                                  <h4>New</h4>
                                  <div class="boxRow">
                                  <div class="labelDiv">Interest </div>
                                  <div id="newInterestDiv" class="valueDiv"><span class="rupeeIcon7"></span> 13,71,946</div>
                                   </div>
                                    <div class="boxRow">
                                 <div class="labelDiv">EMI Tenure</div>
                               <div id="newEmiTenure" class="valueDiv">9 Years 7 Months</div>
                                 </div>
                              </div>
                              <div class="perybox_main">
                                  <div class="boxRow">
                                  <div class="labelDivs">Outstanding Principal</div>
                                  <div id="outstandPrincDivS" class=""><span class="rupeeIcon7"></span> 19,23,438</div>
                                   </div>
                              </div>
                              <div class="outRow">
                                   <div class="labelDivm">EMI</div>
                                <div id="emiDivS" class=""><span class="rupeeIcon7"></span> 33,038</div>
                                  </div>
                                  <div class="rbiNotify">As per RBI notification all prepayment charges on floating rate loans have been waived</div>
                         </div>
                       </div>
                     </div>
            </div>
            <div class="tab-pane" id="tab_default_4">
                <div class="col-lg-6">
                     <div class="form_row">
                         <div class="formlabel">
                             <p>Loan Amount</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>Tenure</p>
                             <span>(in years)</span>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                        <div class="form_row">
                         <div class="formlabel">
                             <p>Current Rate of Interest</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                            <span class="modularIcon">%</span>
                         </div>
                     </div>
                          <div class="form_row">
                         <div class="formlabel">
                             <p>Loan taken on</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                             <p>New Rate of Interest</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                            <span class="modularIcon">%</span>
                         </div>
                     </div>
                     <div class="form_row">
                         <div class="formlabel">
                             <p>Rate of Interest changes on</p>
                         </div>
                         <div class="buttom_search">
                             <span class="rupeeIcon"></span>
                             <input type="text" maxlength="15" oninput="reFormate(this)" onblur="fromNumberToWords(this,'lalec_in_rs');" id="lalec" class="inputAdvFinance">
                         </div>
                     </div>
                      <div class="form_row">
                         <div class="formlabel">
                         </div>
                         <div class="buttom_search_mainl">
                             <input type="button" onclick="submitRateChangeCalculator(true)" value="Compare" class="btnAdvFinance">
                             <span class="resetAll"><a href="javascript: resetEligibilityCalculator()">Reset All</a></span>
                         </div>
                     </div>
                 </div>
                <div class="col-lg-6">
                     <div class="resultTentureBox">
<div id="iconRateChangeId" class="resultHLRateLeft"><span class="rightIcon"></span></div>
<div class="resultHLRateRight">
<div id="sameTenureDiv" class="resultIfYou">If you keep the Tenure same (120 months)</div>
<div id="sameTenureSavingDiv" class="resultHeading">You will save <span class="rupeeIcon5"></span>45,600</div>
<div class="rateRow">
<div class="labelDiv">Current EMI</div>
<div id="currentEmiDiv" class="valueDiv"><span class="rupeeIcon7"></span> 33,038</div>
</div>
<div class="rateRow">
<div class="labelDiv">New EMI</div>
<div id="newEmiDiv" class="valueDiv"><span class="rupeeIcon7"></span> 32,278</div>
</div>
</div>
<div id="iconRateChangeId" class="resultHLRateLeft"><span class="rightIcon"></span></div>
<div class="resultHLRateRight">
<div id="sameTenureDiv" class="resultIfYou">If you keep the Tenure same (120 months)</div>
<div id="sameTenureSavingDiv" class="resultHeading">You will save <span class="rupeeIcon5"></span>45,600</div>
<div class="rateRow">
<div class="labelDiv">Current EMI</div>
<div id="currentEmiDiv" class="valueDiv"><span class="rupeeIcon7"></span> 33,038</div>
</div>
<div class="rateRow">
<div class="labelDiv">New EMI</div>
<div id="newEmiDiv" class="valueDiv"><span class="rupeeIcon7"></span> 32,278</div>
</div>
</div>
</div>

                  </div>
            </div>
          </div>
        </div>
      </div>    
    </div>
  </div>
</div>
<br>
<br>
</section>



<section class="mobile_background_images" >
    <div class="container">
         <div class="row">
             <div class="col-lg-6">
                 <div class="mobileimages_images">
                     <img src="packages/images/mobileNew.png">
                 </div>
             </div>
              <div class="col-lg-6">
                 <div class="mobileimages_images_google">
                     <h2>Refreshingly New, Refreshingly Different</h2>
                     <p>Now search properties on the go with our new Mobile Apps.</p>
                     <img src="packages/images/appStoreBtn.png">
                     <img src="packages/images/googlePlayBtn.png">
                     <img src="packages/images/windowsStoreBtn.png">
                 </div>
             </div>
         </div>
    </div>
</section>



<section class="trends">
     <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="rates_trend">
                   <div class="blockWidth">
<div class="blockHeading"><h2>Rates &amp; Trends</h2></div>
<div class="ratesNtrends">
<ul>
<li class="noMargin rtDelhi">
<a href="trends/New-Delhi"><span>Delhi</span></a>
</li>
<li class="rtGurgaon">
<a href="trends/Gurgaon"><span>Gurgaon</span></a>
</li>
<li class="rtNoida">
<a href="trends/Noida"><span>Noida</span></a>
</li>
<li class="rtGrNoida">
<a href="trends/Greater-Noida"><span>Gr. Noida</span></a>
</li>
<li class="rtGhaziabad">
<a href="trends/Ghaziabad"><span>Ghaziabad</span></a>
</li>
<li class="rtFaridabad">
<a href="trends/Faridabad"><span>Faridabad</span></a>
</li>
<li class="noMargin rtChandigarh">
<a href="trends/Chandigarh"><span>Chandigarh</span></a>
</li>
<li class="rtKochi">
<a href="trends/Kochi"><span>Kochi</span></a>
</li>
<li class="rtCoimbatore">
<a href="trends/Coimbatore"><span>Coimbatore</span></a>
</li>
<li class="rtLucknow">
<a href="trends/Lucknow"><span>Lucknow</span></a>
</li>
<li class="rtHyderabad">
<a href="trends/Hyderabad"><span>Hyderabad</span></a>
</li>
<li class="rtMumbai">
<a href="trends/Mumbai"><span>Mumbai</span></a>
</li>
<li class="noMargin noMarginBot rtPune">
<a href="trends/Pune"><span>Pune</span></a>
</li>
<li class="noMarginBot rtBangalore">
<a href="trends/Bangalore"><span>Bangalore</span></a>
</li>
<li class="noMarginBot rtChennai">
<a href="trends/Chennai"><span>Chennai</span></a>
</li>
<li class="noMarginBot rtKolkata">
<a href="trends/Kolkata"><span>Kolkata</span></a>
</li>
<li class="noMarginBot rtJaipur">
<a href="trends/Jaipur"><span>Jaipur</span></a>
</li>
<li class="noMarginBot rtAhmedabad">
<a href="trends/Ahmedabad"><span>Ahmedabad</span></a>
</li>
</ul>
<div class="clearAll"></div>
</div>
</div>
</div>
              </div>
          </div>
     </div>
</section>




@stop
