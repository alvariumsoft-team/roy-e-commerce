@extends('adashb')

@section('dash_content')


<style type="text/css">

.gpc-listing-box{border-bottom: 1px solid #ccc;padding:10px;margin:0px 10px;background:#fff;}

.left{float:left;}

.right{float:right;}

.clear{clear:both;}

.feature-tag{background:#63b600;box-shadow: 0px 0px 0px rgb(153, 153, 153); border:1px solid #fff;border-radius:5px;padding:1px 5px;color:#fff;font-size:12px;}

.pfont > p{font-size:14px;}

.pfont-light > p{color:#999;}

.mar-inf{margin:5px 10px;margin-right:4px;}

.pad-inf{padding:10px;}

.aw > a{padding-right:10px;}

.btn-aw{background:#fff !important;margin-top:10px;color:#367fa9;border:0px;font-size:18px;}

.btn-aw:hover{color:#365899;}

.btn-aw:active{color:#365899 !important;}

.btn-aw:focus{color:#365899 !important;}
.modal-dialog{margin-top:9%;}
 @media screen and (max-width: 768px) {
	 .modal-dialog{margin-left:auto;margin-right:auto;}
	 
 }
 @media screen and (max-width: 568px) {
	#contact-agent .modal-dialog{width: 91% !important;
margin-top: 27%;}
 #view-phone .modal-dialog{width: 91% !important;
margin-top: 27%;} 
	 }
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="container-fluid ">
    <!-- Content Header (Page header) -->
    <section class="content-header">
 
    </section>

    <!-- Main content -->
    <section class="content">

<style type="text/css">
	
.panel-heading{background:#e9ebee !important;border:0px solid #dedede !important;}

.panel-body{padding: 0px !important;}

.panel-primary{background:#fff !important;border:0px solid #fff !important;}

.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px !important;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none !important;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px !important;
}
/********************************************************************/

/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff; height: 42px !important;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	/*border-color: transparent;*/
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
#tab1primary h3 {color: #666;}
.panel a { color: #666 !important;}
.active.projects  a { border-top: 4px solid #DA251C !important;color: #333 !important;}
.with-nav-tabs.panel-primary .nav-tabs > .open > a, .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover, .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus, .with-nav-tabs.panel-primary .nav-tabs > li > a:hover, .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
  background-color: #fff!important;
  border-color: transparent;
  color: #fff;
}
.house_main {padding-top: 10px;}

.bhk h4 {margin-bottom: 6px;}
.show-on-hover:hover > ul.dropdown-menu {display: block;}
.toggle_main{background-color:none;color:#999; border: none;}
.open_haver {margin-top: 17px;margin-bottom: 18px;}
.btn .caret {margin-left: 6px;}
.blid.pull-right > p {margin-bottom: 1px;}
.blid.pull-left > p {margin-bottom: 1px;}
.dleft img { width: 100%;}
.dolar {margin-bottom: 11px;}
.button_img  button {background: #fff;border: 2px solid #C0392B; padding: 10px; font-size:12px; color: #C0392B;}
.button_imgb  button {background: #C0392B;border: 2px solid #C0392B; padding: 10px; font-size:12px; color: #fff;margin-left: -18px!important;}
.maing {margin-bottom: 70px;}

.button_imgbh_img > p {margin-bottom: 0; font-size: 13px!important;}
.button_imgbh_img h6{}
.sqyrd { float: right;margin-bottom: 10px;}
.button_imgb {margin-top: 20px;margin-left: 0px!important;}
.button_img {margin-top: 20px;margin-left: -17px;}
.left_img img {width: 100%;}
.pull_map:hover {font-size: 15px;!important;margin-top:-5px;margin-left:-5px;cursor:pointer;}
.button_img > button {margin-left: 4px!important;}
.clearfix {margin-left: -13px!important;}
.button_imgbh_img.pull-right {margin-right: -60px!important;}
.sqyrd.pull-right {margin-right: -35px;!important}
//.pull-left {margin-left: -16px;}


.panel{font-size:13px;font-weight:500;}
. putpul_main {
  width: 231px;
}

.listing-heading{color:#000 !important;}
.listing-heading:hover{color:#C0392B !important; font-weight:900 !important;}
.slider-photo{height:200px;width:100%}
     @media screen and (max-width: 1024px) {
		 
		 .slider-photo{height:270px;}
	 }
	 @media screen and (max-width: 1024px) {
.slider-photo {
    height: 200px
}
</style>



<div style="margin-right: -10px;margin-left: -30px;">

    <div class="row">
        <div class="col-lg-9 col-sm-9 col-xs-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                        <ul class="nav nav-tabs project">
                            <li class="active projects tab-li"><a href="#tab1primary" data-toggle="tab"> Properties (4248)</a></li>
                            <li class="projects tab-li"><a href="#tab2primary" data-toggle="tab"> New Projects (35)</a></li>
                            <li class="projects tab-li"><a href="#tab3primary" data-toggle="tab"> Agents (91)</a></li>
                            <li class="projects tab-li"><a href="#tab3primary" data-toggle="tab">  Localities (96)</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
						

                        
                        <div class="tab-pane fade in active" id="tab1primary">
							
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="residentali">
                                   <h3>4249 Residential Properties for Sale</h3>
                               </div>
                            </div>
                        </div>	
					
@foreach($listings as $list)



<div class="rohit" style="border-bottom:1px solid #dedede; float:left; padding:10px 0px;">
<div class="col-lg-12 col-sm-12 col-xs-12 clearfix">
						<div style="padding-bottom: 13px; margin-left: 3%;" class="button_imgbh_img text-left;">	
							<h3>{{ucwords($list->name)}}</h3>
							<!--<h6>{{$list->created_at}}</h6>-->
						</div>
					 </div>
		<div class="col-lg-4 col-sm-12 col-xs-12 pull-right ">
			<img id="rohit_sl" class="img-responsive slider-photo" src="{{$list->img}}"  data-pid="{{$list->project_id}}" data-id="{{$list->id}}"></img>
			
			<p class="text-center pull_map" style="padding-top:8px;padding-bottom:8px;" ><a href="javascript:void(0);" data-lat="{{$list->map_lat}}" data-long="{{$list->map_long}}" class="map-show"><i class="fa fa-map-marker" style="padding:0px 10px;"></i>Map</a></p>
			
				<div class="button_imgbh text-center">
							<p><u> 4 Similar Properties</u></p>
						</div>
						
			
		</div>

		<div class="col-lg-8 col-sm-12 col-xs-12">
						   <div class="col-lg-7 col-sm-7 col-xs-6">
							  <div class="house">
							  
			<?php 
				$project_id = $list->project_id;
				$pl_site = '';
				if($project_id != 0){$pl_site = 'project/'.$project_id;}
				else{$pl_site = 'listing/'.$list->id;}
			?>			  
								  
								<a class="listing-heading" target="_blank" href="{{URL::to('microsite/'.$pl_site )}}"><h4>@if($list->beds > 0){{$list->beds}} BHK, @endif {{ucwords($list->list_type)}} &nbsp; <span style="font-size:12px;">{{$list->area}}&nbsp; sq.ft.</span></h4></a>
								
								<p class="pull-left putpul_main"><b>for {{$list->status}}</b> in {{ucwords($list->name)}} , {{ucwords($list->location)}}</p>
								
							  </div>
						   </div>
						   <div class="col-lg-5 col-sm-5 col-xs-6">
						   
						 
							 <div class="pull-right" style="margin-right:25px;">
								<h4 style="text-align:center;"><b>&#8377;{{$list->price}} </b></h4>
								<p style="text-align:center;">{{round($list->price_sqft)}}&nbsp; per sq.ft.</p>
							 </div>
							</div>
							<div class="col-lg-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-sm-12 col-xs-12 clearfix">
							 <div class="blid pull-left"><p>Builder</p></div>
							 <div class="blid pull-right"><p>Ubber Group</p></div>
						   </div>
							<div class="col-lg-12 col-sm-12 col-xs-12 clearfix">
							 <div class="blid pull-left"><p>Status</p></div>
							  <div class="blid pull-right"><p>{{ucwords($list->status)}}</p></div>
						   </div>
						   
							<div class="col-lg-12 col-sm-12 col-xs-12  clearfix">
							 <div class="blid pull-left"><p>Details</p></div>
							  <div class="blid pull-right"><p><a class="listing-heading" target="_blank" href="{{URL::to('microsite/'.$pl_site )}}">View Details</a></p></div>
						   </div>
						   
						   </div>
						   <div class="col-lg-12 col-sm-12 col-xs-12">
							<div class="col-lg-6 col-sm-6 col-xs-6">
								<div class="button_img pull-left"><button type="button button_phone" class="phone-view" >View Phone No</button></div>
							</div>
					   <div class="col-lg-6 col-sm-6 col-xs-6">
						<div class="button_imgb pull-right"> <button type="button button_phone" class="agent-contact">Contact Agent</button></div>
					 </div>
											 
					
					 
						   </div>
						</div>
						

		

</div>
          
@endforeach 
                </div>

                        <div class="tab-pane fade" id="tab2primary">Primary 2</div>
                        <div class="tab-pane fade" id="tab3primary">Primary 3</div>
                        <div class="tab-pane fade" id="tab4primary">Primary 4</div>
                        <div class="tab-pane fade" id="tab5primary">Primary 5</div>
                    </div>
                </div>
            </div>
        </div>
   
        <div class="col-lg-2 col-sm-2 col-xs-12">
			
			<!--<a class="twitter-timeline" href="https://twitter.com/CowboyGlobal">Tweets by CowboyGlobal</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->
			
			<img src="{{URL::to('packages/images/ad.png')}}" style="width:240px;padding-top:50px;">
			<img src="{{URL::to('packages/images/ad.png')}}" style="width:240px;padding-top:50px;">
			
		</div>
   
    </div>

</div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<style type="text/css">
	
	#show-map > .modal-dialog{width:90% !important;height:90% !important;}
	
	#photo-slider > .modal-dialog{width:90% !important;height:90% !important;}
	
	.slider-photo{cursor:pointer;}
	
	.vp{padding:5px;}
</style>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>

<script type="text/javascript">
	
	$ = jQuery.noConflict();
	
	$(document).ready(function(){
		var latitude;
		var longitude;
	$('.map-show').on('click',function(){
		
		latitude = $(this).attr('data-lat');
		longitude = $(this).attr('data-long');
		
		$('#show-map').modal('show');
		
		if((latitude == '') || (latitude == undefined)){
			latitude	= 28.7041;
			longitude	= 77.1025;
		}
		
		setTimeout(function() {
			$('.re-map').trigger('resize');
		
		latitude = parseFloat(latitude);
		longitude= parseFloat(longitude);

			var uluru = {lat: latitude, lng: longitude};
			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			  center: uluru
			});
			var marker = new google.maps.Marker({
			  position: uluru,
			  map: map
			});
			
		}, 500);
		
	});	
	
	
	$('.phone-view').on('click',function(){
		$('#view-phone').modal('show');
	});	

	$('.agent-contact').on('click',function(){
		$('#contact-agent').modal('show');
	});	
	
	$('.slider-photo').on('click',function(){
		
		var pid	= $(this).attr('data-pid');
		var id	= $(this).attr('data-id');
		var url	= "{{URL::to('/limages')}}";
		
		$.post(url, {pid:pid, id:id}).done(function(data){
			
			var im = JSON.parse(data);
			var html = '';
			for(i in im){
				html += '<li data-thumb="'+im[i]+'"><img src="'+im[i]+'" /></li>';
			}
			
			//$('#slides-html').html(html);
			
			//console.log(html);
			
				$('.flexslider').flexslider({
					animation: "slide",
					controlNav: "thumbnails",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				});
			
			setTimeout(function() {
				$('#photo-slider').modal('show');
				setTimeout(function() {$('.slider').trigger('resize');}, 1000);
			}, 700);
			
		});
		

		
		
		
		
	});	

	});
	
</script>


<div class="modal fade" id="view-phone" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="gpc-modal-label"><h2 style="text-align:center;">View Phone Number</h2></div>
			</div>
			<div class="modal-body" id="mbody">
				
				<div class="row">
					
					<div class="col-md-12">
						<span class="vp"> I am </span>
						<span class="vp"><input type="radio" name="contact_type" value="individual" /></span>
						<span class="vp"> Individual </span>
						<span class="vp"><input type="radio" name="contact_type" value="agent" /></span>
						<span class="vp"> Agent </span>
						<br style="margin:5px;"/>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Name"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Mobile"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Email"/>
						</div>
						<div class="form-group">
						<input type="submit" class="btn btn-primary" name="View Phone" value="View Phone"/>
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="modal-footer">
				
			</div>
		</div>
	</div>

</div>

<style type="tet/css">

.slider {display: block; margin: 50px 0 10px!important;}
#carousel li {margin-right: 5px;}
#carousel img {display: block; opacity: .5; cursor: pointer;}
#carousel img:hover {opacity: 1;}
#carousel .flex-active-slide img {opacity: 1; cursor: default;}

.photo-slider-body{padding:0px !important;}



</style>

<div  class="modal fade" id="photo-slider" tabindex ="-1" role="dialog" aria-labelledby="photo-view-label" aria-hidden="true" style="height:100% !important">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="photo-view-label"><h2 style="text-align:center;"></h2></div>
			</div>
			<div   class="modal-body photo-slider-body" style="padding:0px !important">

			  <section class="slider">
				<div class="flexslider">
				  <ul class="slides" id="slides-html">
					
					<li data-thumb="{{URL::to('public/listing_img/aa1.png')}}"><img src="{{URL::to('public/listing_img/aa1.png')}}" /></li>
					<li data-thumb="{{URL::to('public/listing_img/aa2.png')}}"><img src="{{URL::to('public/listing_img/aa2.png')}}" /></li>
					<li data-thumb="{{URL::to('public/listing_img/aa4.jpg')}}"><img src="{{URL::to('public/listing_img/aa4.jpg')}}" /></li>
					<li data-thumb="{{URL::to('public/listing_img/aa5.jpg')}}"><img src="{{URL::to('public/listing_img/aa5.jpg')}}" /></li>
					
				  </ul>
				</div>
			  </section>


			</div>
		</div>
	</div>

</div>
  <!-- FlexSlider -->
  {{HTML::style('packages/css/flexslider.css')}}

   {{HTML::script('packages/js/jquery.flexslider.js')}}

  <script type="text/javascript">
   
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<div class="modal fade" id="contact-agent" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="gpc-modal-label"><h2 style="text-align:center;">Contact Agent</h2></div>
			</div>
			<div class="modal-body" id="mbody">
				
				<div class="row">
					
					<div class="col-md-12">
						<span class="vp"> I am </span>
						<span class="vp"><input type="radio" name="contact_type" value="individual" /></span>
						<span class="vp"> Individual </span>
						<span class="vp"><input type="radio" name="contact_type" value="agent" /></span>
						<span class="vp"> Agent </span>
						<br style="margin:5px;"/>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Name"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Mobile"/>
						</div>
						<div class="form-group">
						<input type="text" class="form-control" placeholder="Email"/>
						</div>
						<div class="form-group">
						<input type="submit" class="btn btn-primary" name="Contact Agent" value="Contact Agent"/>
						</div>
						
						</div>
						
					</div>
					
				
			</div>
			
			<div class="modal-footer">
				
			</div>
		</div>
	</div>

</div>

<div class="modal fade" id="show-map" tabindex ="-1" role="dialog" aria-labelledby="zzz" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="zzz"></div>
			</div>
			<div class="modal-body" id="mbody">


<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
	<div id="map" style="height:500px;width:100%">
		
	</div>
</div>



			
				
			</div>
			
			<div class="modal-footer">
				
			</div>
		</div>
	</div>

</div>

<script type="text/javascript"> 
	/*function init_map(){
		var myOptions = {zoom:15,center:new google.maps.LatLng(40.805478,-73.96522499999998),mapTypeId: google.maps.MapTypeId.ROADMAP};
		map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
		marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(40.805478, -73.96522499999998)});
		infowindow = new google.maps.InfoWindow({content:"<b>la</b><br/>2880 Broadway<br/> New York" });
		google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});
		infowindow.open(map,marker);}
		google.maps.event.addDomListener(window, 'load', init_map);
		*/
		

		
</script>

@stop

