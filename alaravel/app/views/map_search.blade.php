@extends('main')
@section('content')

<style type="text/css">
	.map_bar_ul{padding:0px;margin:0px;}
	.map_bar_ul::after{clear:both;}
	.map_bar_li{list-style:none;float:left; padding:5px 12px;}
	.gm-style-iw + div {display: none;}
	.mlabels{background:#000;padding:10px;margin:10px;}
	.mapdp{margin:0px;padding:0px;margin-bottom:3px;}
	
.gm-style-iw {
   width: 150px;
   top: 25px !important; 
   background-color: #fff;
   box-shadow: 0 1px 6px rgba(0, 0, 0, 0.3);
   border: 1px solid rgba(0, 0, 0, 0.3);
   border-radius: 2px 2px 0 0;
   padding:1px 10px;
}
.fl{float:left;}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-2"><h3><span style="color:#55c5ef;margin-right:10px;">Map</span><span style="color:#27ae60;">Search</span></h3></div>
		<div class="col-md-10">
		<div style="clear:both;">
			<div class="fl clearfix"><h4 style="padding:5px;">
				<input type="text" class="form-control map-fr" placeholder="Enter City.." name="city" value="delhi"/>
			</h4></div>
			<div class="fl clearfix"><h4 style="padding:5px;">
				<select class="form-control map-fr"  name="buy_rent">
					<option value="sale">Buy</option>
					<option value="rent">Rent</option>
				</select>
			</h4></div>
			<div class="fl clearfix"><h4 style="padding:5px;">
			<select class="form-control map-fr" name="list_type">
				<option value="apartment">Apartment</option>
				<option value="house">House</option>
				<option value="land">Land</option>
			</select>
			</h4></div>
		</div>
		</div>
		<div class="col-md-12">
			<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
				<div id="map" style="height:500px;width:100%">
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="mapload" style="  display:none;position: absolute;
  width: 250px;
  z-index: 15;
  top: 30%;
  padding:0px 20px;
  left: 40%;
  background: #f9f9f9;">
	<h4><img height="40px" width="40px"src="{{URL::to('packages/images/loading.gif')}}"><span style="padding-left:20px;color:red;font-size:14px;">Loading Results...</span></h4>
	
</div>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>-->
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
<script src="http://inviolateinfotech.com/laravel/packages/bootstrap/dist/js/jquery.js"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>


<script type="text/javascript">
	var map = undefined;
	function map_setup(jsn){console.log(jsn);
		map = new google.maps.Map(document.getElementById('map'), { center: jsn.center, zoom: 5 });	
		  var markers = [];
		  var marker = '';
		  var infowindow = '';
			for(i in jsn.list){
				marker = new google.maps.Marker({ position: jsn.list[i].loc, label: '', title: ''});
				infowindow = new google.maps.InfoWindow({content: jsn.list[i].content});
				marker.setVisible(false);
				infowindow.open(map,marker);
				google.maps.event.addListener(infowindow, 'domready', function() {
					var iwOuter = $('.gm-style-iw');
					var iwBackground = iwOuter.prev();
					iwBackground.children(':nth-child(2)').css({'display' : 'none'});
					iwBackground.children(':nth-child(4)').css({'display' : 'none'});
					iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 1px 6px'});
					iwBackground.children(':nth-child(3)').find('div').children().css({'z-index': '1'});
				});
				markers.push(marker);
			}
			var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
	}
	$(document).ready(function(){
		var jsn = {{$jsn}};
		map_setup(jsn);
		
		$(document).on('change, click', '.map-fr', function(){
			$vals = [];
			$('.mapload').css('display', 'block');
			var term = $('input[name="city"]').val();
			var buy_rent = $('select[name=buy_rent]').val();
			var list_type = $('select[name=list_type]').val();
			//setTimeout(function() {}, 700);
			$.post("{{URL::to('amapsearch')}}", {term:term, buy_rent:buy_rent, list_type:list_type}).done(function(data){
				data = JSON.parse(data);
				map_setup(data);
				setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
			})
			
		});
		
			
		
	});
</script>

@stop
