<!doctype html><html>
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<meta http-equiv="Cache-control" content="public">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Global Property Cowboys</title>
<link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css' rel='stylesheet' type='text/css'>
{{HTML::style('packages/css/gpc.css')}}
{{HTML::script('packages/bootstrap/dist/js/jquery.js')}}


 	<style type="text/css">#ifile-id{display:none;}</style>
	
	<script type="text/javascript">
		
/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
$(document).ready(function(){
	var input = $('#ifile-id');
	var label = $('#ifile-label');
		input.on( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.html(fileName);
		});

		// Firefox bug fix
		input.on( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.on( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});

	
	</script>

<style type="text/css" >
	
	.loc-loc{margin:0px;padding:0px;}
	.loc-loc > li{list-style:none;width:25%;margin:0px;padding:5px;float:left;font-weight:500;}
	.locs{width:720px;position: absolute;left: -56%!important;top: 30px;z-index:56368844658;padding:0px ;}
	.with-nav-tabs{margin-bottom:0px;}
	.tab_menu{background:#fff;}
	
</style>

</head>
<body>
	
<header class="header-top " style="z-index:10000000;">
<div class="container-fluid">

<div class="row" style="border-bottom:1px solid #3b4d5d;">

<div class="col-xs-0 col-md-2 contact-phone-m">
	<ul class="social-media cphone"><li><i class="fa fa-mobile-phone"> </i>Call Us Today: 1-888-999-5454 </li></ul>

</div>

                 <div class="col-md-4">
					 
					 @if(Auth::check())
					 
					 <div style="color:#fff;padding: 8px 6px;">
					<div class="dropdown ">
					  <a style="color:#fff;cursor:pointer;" class="dropdown-toggle" type="button" id="loc" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<span class="ftext country-location">@if(Session::has('country')) {{ucwords(Session::get('country'))}} @else Select Location @endif</span>
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu locs pull-right" aria-labelledby="buy_rent">
				  
						  
						  <li>
							  
									<div class="row">
										<div class="col-md-12">
											<div class="panel with-nav-tabs panel-default">
												<div class="panel-heading country_bootom">
														<ul class="nav nav-tabs country-nav">
															<li class="active"><a class="seloc" data-loc="eu" href="#tab1default" data-toggle="tab">Europe</a></li>
															<li><a class="seloc" href="#tab2default" data-loc="world" data-toggle="tab">Rest of the world</a></li>
														</ul>
												</div>
												<div class="panel-body">
													<div class="tab-content tab_menu">
														<div class="tab-pane fade in active" id="tab1default">
														
								<ul class="loc-loc cleardix eu">
								
								@if(isset($countries['eu']))
									@foreach($countries['eu'] as $country)
										<li><a class="country-class" href="javascript:void(0);" data-country="{{strtolower($country['country'])}}">{{$country['country']}}</a></li>
									@endforeach
								@endif
								<!--
								<li>Germany</li><li>Italy</li><li>France</li><li>United Kingdom</li><li>Germany</li><li>Switzerland</li>
								<li>Poland</li><li>Netherlands</li><li>Ukraine</li><li>Greece</li><li>Austria</li><li>Sweden</li>
								<li>Norway</li><li>Malta</li><li>Czech Republic</li><li>Belgium</li><li>Iceland</li><li>Finland</li>
								<li>Croatia</li><li>Cyprus</li><li>Romania</li><li>Hungary</li><li>Denmark</li><li>Bulgaria</li>
								<li>Luxembourg</li><li>Monaco</li><li>Slovenia</li><li>Serbia</li><li>Vatican City</li><li>Albania</li>
								<li>Lithuania</li><li>Belarus</li><li>Montenegro</li><li>Estonia</li><li>Moldova</li><li>Slovakia</li>
								<li>Bosnia And Herzegovina</li><li>Kosovo</li><li>Latvia</li><li>Republic Of Ireland</li><li>San Marino</li><li>Macedonia</li>
								<li>Liechtenstein</li><li>Gibraltar</li><li>Faroe Islands</li><li>Isle Of Man</li><li>Northern Island</li><li>Jersey</li>
								<li>Svalbard And Jan Mayen</li><li>Aland Islands</li>
								-->
								</ul>
														
								</div>
								<div class="tab-pane fade" id="tab2default">
														
								<ul class="loc-loc cleardix world">
	
								@if(isset($countries['world']))
									@foreach($countries['world'] as $country)
										<li><a class="country-class" href="javascript:void(0);" data-country="{{strtolower($country['country'])}}">{{$country['country']}}</a></li>
									@endforeach
								@endif
	<!--
								<li>America</li><li>India</li><li>Russia</li><li>Mongolia</li><li>Africa</li><li>Japan</li>
								<li>China</li><li>Australia</li><li>Indonasia</li><li>Srilanka</li><li>malayasia</li>
								-->
								</ul>
														
														</div>
													</div>
												</div>
											</div>
										</div>
									  </div>
								
							  
						  </li>
						  
						
					  </ul>
					</div>
					</div>
					@endif
                 </div> 


<div class="col-xs-12 col-md-6">

<ul class="social-media">

@if(isset($Index['social-media']) && $Index['social-media']!="")
@foreach($Index['social-media'] as $menu)

<li class="{{$menu['li-class']}}"><a href="{{$menu['url']}}" target="_blank"><i class="{{$menu['class']}}"></i></a></li>

@endforeach
@endif

<li>
<div class="lng" style="background:rgba(255,255,255,0.1);">
<ul class="lang left" >
<li><a href="{{URL::to('/lang/en')}}" class="lang_sel_sel icl-en">
<img  class="iclflag" src="{{URL::to('packages/flags/gb.png')}}" style="height:14px;" alt="en"  title="English" />
&nbsp;</a> 

<ul class="llang">
<a href="{{URL::to('/lang/nl')}}" class="lang_sel_sel icl-en"><li><img  class="iclflag" src="{{URL::to('packages/flags/nl.png')}}" style="height:14px;margin-bottom:5px;" alt="nl" title="Nederlands" /></a></li>
<a href="{{URL::to('/lang/fr')}}" class="lang_sel_sel icl-en"><li><img  class="iclflag" src="{{URL::to('packages/flags/fr.png')}}" style="height:14px;margin-bottom:5px;" alt="fr" title="Fran�ais" /></a></li>
<a href="{{URL::to('/lang/es')}}" class="lang_sel_sel icl-en"><li><img  class="iclflag" src="{{URL::to('packages/flags/es.png')}}" style="height:14px;margin-bottom:5px;" alt="es" title="Espa�ol" /></a></li>
<a href="{{URL::to('/lang/ar')}}" class="lang_sel_sel icl-en"><li><img  class="iclflag" src="{{URL::to('packages/flags/ar.png')}}" style="height:14px;margin-bottom:5px;" alt="ar" title="???????" /></a></li>
<a href="{{URL::to('/lang/zh-CN')}}" class="lang_sel_sel icl-en"><li><img  class="iclflag" src="{{URL::to('packages/flags/cn.png')}}" style="height:14px;margin-bottom:5px;" alt="zh-hant" title="????" /></a></li>

</ul>
</li>
</ul>

<span class="left langd"><i class="fa fa-sort-down lng" aria-hidden="true"></i></span>

</div>

</li>

<style type="text/css">
	.dropdown_m_app{width:530px;padding:0px;}
	.dropdown_m_app > li > div > header > form > span{padding:0px 3px !important; }
	.dropdown_m_app > li{margin:0px !important;}
	.dropdown_m_app > li > div > header{padding: 10px 30px 10px 20px; border-bottom:1px solid #dedede;}
	.dropdown_m_app > li > section{margin-bottom:20px;}
	.appline{line-height:0.8em;}
	.m_app_back{background:#dedede;}
</style>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-mobile"></i>&nbsp;<span class="caret"></span></a>
	<ul class="dropdown-menu animated fadeInDown pull-right dropdown_m_app">
		<li class="">
			<div class="m_app_back">
				<header class="clearfix">
					<h3>Get Our App Via Sms</h3>
					<form class="row" action="" method="post">
						<span class="col-md-4 form-group">
							<select name="app" class="form-control">
								<option>Property Search App</option>
								<option>Prop Worth App</option>
								<option>Agent Smart Dairy App</option>
								<option>Broker Connect App</option>
							</select>
						</span>
						<span class="col-md-2 form-group">
							<select name="code" class="form-control">
								<option>+91</option>
							</select>
						</span>
						<span class="col-md-4 form-group"><input type="text" class="form-control" name="number" placeholder="Phone Number"></span>
						<span class="col-md-2 form-group"><button class="btn btn-default">GET APP</button></span>
					</form>
				</header>
			</div>
				<section>
					<div class="row">
						<div class="col-md-2" style="text-align:center;">
							<i class="fa fa-mobile" style="font-size:40px; color:#dedede;text-align:center;margin-top:20px;"></i>
						</div>
						<div class="col-md-10">
							<h4>Property Search App</h4>
							<p class="appline">Experience real estate like never before</p>
							<p class="appline">Android | IOS</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2" style="text-align:center;">
							<i class="fa fa-mobile" style="font-size:40px; color:#dedede;text-align:center;margin-top:20px;"></i>
						</div>
						<div class="col-md-10">
							<h4>Prop Worth App</h4>
							<p class="appline">Right Price Of Any Property Now On Your Fingertips</p>
							<p class="appline">Android</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2" style="text-align:center;">
							<i class="fa fa-mobile" style="font-size:40px; color:#dedede;text-align:center;margin-top:20px;"></i>
						</div>
						<div class="col-md-10">
							<h4>Agent Smart Dairy App</h4>
							<p class="appline">Exclusively For Agents! Now, run your buiseness from your app</p>
							<p class="appline">Android | IOS</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2" style="text-align:center;">
							<i class="fa fa-mobile" style="font-size:40px; color:#dedede;text-align:center;margin-top:20px;"></i>
						</div>
						<div class="col-md-10">
							<h4>Broker Connect App</h4>
							<p class="appline">Join largest broker network</p>
							<p class="appline">Android</p>
						</div>
					</div>
					
				</section>
		</li>
	</ul>
</li>


@if(!Auth::check())
<li>{{html_entity_decode(HTML::link('login','<i class="fa fa-sign-in"></i>&nbsp;Login',array('class'=>'login','id'=>'login')))}}</li>
<li>{{html_entity_decode(HTML::link('plans','<i class="fa fa-sign-in"></i>&nbsp;Register',array('class'=>'login','id'=>'plans')))}}</li>
@else 
    <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                            <ul class="dropdown-menu danger  animated fadeInDown pull-right notification-title">
                                <li class="title">
                                   <i class="fa fa-exclamation-circle icon"></i>  Notification <span class="badge pull-right">4</span>
                                </li>
                                <li>
                                    <ul class="list-group notifications">
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> messages
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item message">
                                                view all
                                            </li>
                                        </a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
    <li class="dropdown ">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown pull-right ">
                                <li class="profile-img">
                                    <img src="{{URL::to('packages/images/user.png')}}" class="profile-img" style="width:100%;height:120px;">
                                </li>
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username">{{ucwords(Auth::user()->username)}}</h4>
                                        <p>{{Auth::user()->email}}</p>
                                        <div class="btn-group margin-bottom-2x profile-info-button" role="group">
                                            <a href="{{URL::to('profile')}}" class="btn btn-default"><i class="fa fa-user"></i> Profile</a>
                                            <a href="{{URL::to('logout')}}" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
    </li>
@endif

</ul>
@if(Auth::check())
<div class="right"><a href="javascript:void(0);" class="side-menu-link"><i class="fa fa-bars" style="font-size:22px;color:#ccc;"></i></a></div>
@endif
<div class="clear"></div>
</div>
</div>

</div>

</header>

<?php

$blog_site = URL::to('blog');

$webshop = URL::to('webshop');

$side_menu = <<<Q


<aside class="side-menu unhighlight">

<header class="side-menu-head">
<div class="right"><a href="javascript:void(0);" class="side-menu-close"><i class="fa fa-times fa-4" style="font-size:22px;color:#ccc;"></i></a></div>
<div class="clear"></div>
</header>

<nav>
                               <ul class="side-menu-nav" >
                               <li><a href="$webshop" class="menu-head">Webshop</a></li>
									<li><a href="#" class="menu-head">Property Tours</a></li>
									<li><a href="#" class="menu-head">Property Donations</a></li>
									<li><a href="#" class="menu-head">Borrow Tools</a></li>
									<li><a href="#" class="menu-head">Home Loan</a></li>
									<li><a href="#" class="menu-head">Advertise with us</a></li>
									<li><a href="#" class="menu-head">Customer Care</a></li>
									<li><a href="#" class="menu-head">Download Mobile App</a></li>
									<li><a href="" class="menu-head">Blog</a></li>
									<li><a href="#" class="menu-head">Property Alerts</a></li>
                              </ul>

</nav>

</aside>


Q;

?>

@if(Auth::check())
{{$side_menu}}
@endif

@if(isset($Index['index']) && $Index['index']!="")
@if(isset($Index['index']) && ($Index['index'] == 'index'))

<header class="header" style="position:absolute;background:transparent;z-index:100000;left:6.7%;">
    <div class="container">
         <div class="row" >
             <div class="col-md-1">
                 <div class="logo">
                      {{html_entity_decode(HTML::link('/',HTML::image("packages/realestate/site/logo.png")))}}
                 </div>
             </div>
             
                 <div class="col-md-3">
					 
					 @if(Auth::check())
	<!--				 
					 <div style="padding-top:40px;color:#fff;" id="locsel">
					<div class="dropdown ">
					  <a  style="color:#fff;cursor:pointer;" class="dropdown-toggle" type="button" id="buy_rent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<span class="ftext">Select Location</span>
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu locs pull-right" aria-labelledby="buy_rent">
				  
						  
						  <li>
							  
									<div class="row">
										<div class="col-md-12">
											<div class="panel with-nav-tabs panel-default">
												<div class="panel-heading">
														<ul class="nav nav-tabs">
															<li class="active"><a class="seloc" href="#tab1default" data-toggle="tab">Europe</a></li>
															<li><a class="seloc" href="#tab2default" data-toggle="tab">Rest of the world</a></li>
														</ul>
												</div>
												<div class="panel-body">
													<div class="tab-content tab_menu">
														<div class="tab-pane fade in active" id="tab1default">
														
								<ul class="loc-loc cleardix">
								
								<li>Germany</li><li>Italy</li><li>France</li><li>United Kingdom</li><li>Germany</li><li>Switzerland</li>
								<li>Poland</li><li>Netherlands</li><li>Ukraine</li><li>Greece</li><li>Austria</li><li>Sweden</li>
								<li>Norway</li><li>Malta</li><li>Czech Republic</li><li>Belgium</li><li>Iceland</li><li>Finland</li>
								<li>Croatia</li><li>Cyprus</li><li>Romania</li><li>Hungary</li><li>Denmark</li><li>Bulgaria</li>
								<li>Luxembourg</li><li>Monaco</li><li>Slovenia</li><li>Serbia</li><li>Vatican City</li><li>Albania</li>
								<li>Lithuania</li><li>Belarus</li><li>Montenegro</li><li>Estonia</li><li>Moldova</li><li>Slovakia</li>
								<li>Bosnia And Herzegovina</li><li>Kosovo</li><li>Latvia</li><li>Republic Of Ireland</li><li>San Marino</li><li>Macedonia</li>
								<li>Liechtenstein</li><li>Gibraltar</li><li>Faroe Islands</li><li>Isle Of Man</li><li>Northern Island</li><li>Jersey</li>
								<li>Svalbard And Jan Mayen</li><li>Aland Islands</li>
								
								</ul>
														
														</div>
														<div class="tab-pane fade" id="tab2default">
														
								<ul class="loc-loc cleardix">
								
								<li>America</li><li>India</li><li>Russia</li><li>Mongolia</li><li>Africa</li><li>Japan</li>
								<li>China</li><li>Australia</li><li>Indonasia</li><li>Srilanka</li><li>malayasia</li>
								
								</ul>
														
														</div>
													</div>
												</div>
											</div>
										</div>
									  </div>
								
							  
						  </li>
						  
						  
						  
			  
						  
					  </ul>
					</div>
					</div>
					-->
					@endif
					
                 </div>             
             
             <div class="col-md-8">
                        <div class="navigation">
                        <div class="container-fluid">
                           <div class="navbar-header">
                               <span class="sr-only">Toggle navigation</span>
                               <span class="icon-bar"></span>
                               <span class="icon-bar"></span>
                               <span class="icon-bar"></span>
                           </div>
                           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
														   
								<ul class="nav navbar-nav header-nav">
									<li><a href="{{URL::to('/webshop')}}" target="_blank">Webshop</a></li>
									<li><a href="#">Two Weeks Trail</a></li>
									<li>{{HTML::link('about','AboutUs')}}</li>
									<li>{{HTML::link('contact','ContactUs')}}</li>
									<li><a href="#">Interior</a></li>
									<li><a href="#">Expert</a></li>
								</ul>
						   

                          </div>
                      </div>
                 </div>
             </div>
         </div>
    </div>
</header>

@else
 
<header class="header">
    <div class="container">
         <div class="row">
             <div class="col-md-4">
                 <div class="logo">
		{{html_entity_decode(HTML::link('/',HTML::image("packages/realestate/site/logo.png")))}}
                 </div>
                 

                 
                 
             </div>
             <div class="col-md-8">
                        <div class="navigation">
                        <div class="container-fluid">
                           <div class="navbar-header">
                               <span class="sr-only">Toggle navigation</span>
                               <span class="icon-bar"></span>
                               <span class="icon-bar"></span>
                               <span class="icon-bar"></span>
                           </div>
                           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav header-nav">
									<li><a href="#">Webshop</a></li>
									<li><a href="#">Two Weeks Trail</a></li>
									<li>{{HTML::link('about','AboutUs')}}</li>
									<li>{{HTML::link('contact','ContactUs')}}</li>
									<li><a href="#">Interior</a></li>
									<li><a href="#">Expert</a></li>
								</ul>
						   
                          </div>
                      </div>
                 </div>
             </div>
         </div>
    </div>
</header>

@endif
@endif
<!--

<section class="container"><section class="container">

@if(Session::has('message'))
<p class="well" style="text-align:center; color:red;">{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
@endif
</section></section>
-->
@yield('content')

<footer class="footera">
  <div class="container">
       <div class="row">
           <div class="col-lg-3">
                <div class="logo_main_logo">
                     {{HTML::image("packages/realestate/site/logo.png")}}
                     <p>We are a company based in Heiloo, a small town near Amsterdam, The Netherlands , Western Europe.</p>
                     <h4>Follow Us</h4>
					 <ul class="social-media" style="border:0px;padding:10px;padding-left:0px;">
@if(isset($Index['social-media']) && $Index['social-media']!="")					 	
@foreach($Index['social-media'] as $menu)

<li class="{{$menu['li-class']}}" style="float:left;padding:0px 10px;padding-left:0px;"><a href="{{$menu['url']}}" target="_blank" style="border:0px;"><i class="{{$menu['class']}}"></i></a></li>

@endforeach
@endif
<div class="clear"></div>
</ul>       
	   </div>
           </div>
              <div class="col-lg-3">
                <div class="house_main_timming">
                     <h4>Timming</h4>
                     <h5>OPENING HOUSE</h5>
                     <h5>Monday-Saturday 11Am-5Pm</h5>
                     <h5>Other times by appointment only</h5>
                     <h5>http://www.globalpropertycowboys.com/</h5>
                </div>
              </div>
                <div class="col-lg-3">
                <div class="contact_main_contact">
                     <h4>Contact Info</h4>
                     <h5>101 Front St.
San Diego, CA 92101
Netherlands </h5>
                     <h5>619-555-9785</h5>
                     <h5>support@globalpropertycowboys.com</h5>
                </div>
              </div>
			  
			 <div class="col-md-3">
			 <div class="contact_main_contact">
				<aside id="text-6" class="widget col span_3 widget_text">			
					<div class="textwidget">
					  <iframe width="250" height="250" src="https://www.youtube.com/embed/St1oxMR2nsA?rel=0&autoplay=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</aside>
			 </div>
			 </div>
			  
       </div>
       </div>
  
</footer>
<section class="copyright" style="background:#fff;">
    <div class="container">
          <div class="row">
               <div class="copy_right" >
                   <p>&copy; Copyright 2016 Global Property Cowboys, All Rights Reserved.</p>
               </div>
          </div>
    </div>
</section>

<div class="modal fade" id="gpc-modal" tabindex ="-1" role="dialog" aria-labelledby="gpc-modal-label" aria-hidden="true" style="z-index: 8751155!important;">
	<div class="modal-dialog modall-dialog">
		<div class="modal-content modall-content">
			<div class="modal-header modall-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><div class="modal-title modall-title" id="gpc-modal-label"></div>
			</div>
			<div class="modal-body modall-body" id="mbody">
			
			<div id="gpc-modal-message"></div>
			
			<div class="modal-main-plan" id="modal-main-plan">
				<div class="">
					<div class="plan-box left">
						<header class="plan-header" style="font-size:18px;padding-bottom:27px;">2 weeks trial<br /></header>
						<div class="bnr"></div>
						<div class="plan-main">FREE</div>
						<p class="plan-footer">{{HTML::link('register/?pid=1','Sign Up',array('class'=>'plan-register', 'id'=>'register'))}}</p>
					</div>

					<div class="plan-box left">
						<header class="plan-header" style="font-size:18px;">Packages/Membership GPC</header>
						<div class="bnr"></div>
						<div class="plan-main">$9.99</div>
						<p class="plan-footer">{{HTML::link('register/?pid=2','Sign Up',array('class'=>'plan-register', 'id'=>'register'))}}</p>
					</div>

					<div class="clear"></div>
			
				</div>
			</div>
			
			<div class="modal-main-register" id="modal-main-register">
			
				{{Form::open(array('action'=>'PropertyController@postRegister','class'=>'form','method'=>'post','id'=>'form-register'))}}

				<div class="form-group">

				<label>Username:</label>
				{{Form::text('username','',array('class'=>'form-control jform-control','placeholder'=>'User name','required'=>'true','id'=>'register-username'))}}

				</div>

				<div class="form-group">

				<label>First Name:</label>
				{{Form::text('first_name','',array('class'=>'form-control jform-control','placeholder'=>'First name','required'=>'true','id'=>'register-firstname'))}}

				</div>

				<div class="form-group">

				<label>Last Name:</label>
				{{Form::text('last_name','',array('class'=>'form-control jform-control','placeholder'=>'Last name','required'=>'true','id'=>'register-lastname'))}}

				</div>

				<div class="form-group">

				<label>Password:</label>
				{{Form::password('password',array('class'=>'form-control jform-control','placeholder'=>'password','required'=>'true','id'=>'register-password'))}}

				</div>

				<div class="form-group">

				<label>Confirm Password:</label>
				{{Form::password('password_confirmation',array('class'=>'form-control jform-control','placeholder'=>'confirm password','required'=>'true','id'=>'register-cpassword'))}}

				</div>
				
				<div class="form-group">	
					<label>Profile Image:</label>
					<br />
					<label style="width:100%;" for="ifile-id" >
						<div class="form-control jform-control" style="cursor:pointer;">
							<span id='ifile-label' >Upload Image</span>
							{{Form::file('file',array('class'=>'form-control jform-control inputfile inputfile-1','id'=>'ifile-id','data-multiple-caption'=>'{count} files selected'))}}
						</div>
					</label>
				</div>

				<div class="form-group">

				<label>Email:</label>
				{{Form::text('email','',array('class'=>'form-control jform-control','placeholder'=>'E-Mail','required'=>'true','id'=>'register-email'))}}

				</div>
 

				<div class="form-group">

				Owner:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control register-user-type'))}}

				Seller:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control register-user-type'))}}

				Renter:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control register-user-type'))}}

				</div>

				<div class="form-group">

				{{Form::submit('Register',array('class'=>'btn bt-large btn-primary ','style'=>'background:#29333d;','id'=>'link-register'))}}

				</div>

				{{Form::close()}}

			</div>
			
			<div class="modal-main-confirm" id="modal-main-confirm">
				{{Form::open(array('action'=>'PropertyController@postConfirmuser','class'=>'form','method'=>'post','id'=>'form-confirm'))}}

				<div class="form-group">
				<label>Username:</label>
				{{Form::text('user','',array('class'=>'form-control jform-control','placeholder'=>'Your Username','required'=>'true','id'=>'confirm-user'))}}
				</div>

				<div class="form-group">
				<label>Password:</label>
				{{Form::password('password',array('class'=>'form-control jform-control','placeholder'=>'Your Password','required'=>'true','id'=>'confirm-password'))}}
				<!--$2y$10$o3/TOsQeAzJWo8g3lOmRSet9j3/YWnaL8NIsZlnSEwNtWbUiFBF2.-->
				</div>

				<div class="form-group">
				<label>Confirmation Code:</label>
				{{Form::text('ccode','',array('class'=>'form-control jform-control','placeholder'=>'Your Confirmation Code Here','required'=>'true','id'=>'confirm-code'))}}

				</div>

				<div class="form-group">

				{{Form::submit('Countinue',array('class'=>'btn bt-large btn-primary ','style'=>'background:#29333d;','id'=>'link-confirm'))}}

				</div>

				{{Form::close()}}
			</div>
			
			<div class="modal-main-login" id="modal-main-login">
				{{Form::open(array('action'=>'PropertyController@postLogin','class'=>'form','method'=>'post','id'=>'form-login','required'=>'true'))}}
				
					<div class="form-group">
					<label>Username:</label>
					{{Form::text('user','',array('class'=>'form-control jform-control','placeholder'=>'builder','required'=>'true','id'=>'login-user','required'=>'true'))}}
					</div>

					<div class="form-group">
					<label>Password:</label>
					{{Form::password('password',array('class'=>'form-control jform-control','placeholder'=>'qwe','required'=>'true','id'=>'login-password','required'=>'true'))}}
					<!--$2y$10$o3/TOsQeAzJWo8g3lOmRSet9j3/YWnaL8NIsZlnSEwNtWbUiFBF2.-->
					</div>

					<div class="form-group">{{Form::checkbox('remember_me','remember',null,array('id'=>'login-remember'))}}&nbsp;Remember Me</div>

					<div class="form-group">{{HTML::link('lostpassword','Lost Your Password?',array('id'=>'login-lost-password'))}}</div>

					<div class="form-group">

					{{Form::submit('Log In',array('class'=>'btn bt-large btn-primary ','style'=>'background:#29333d;','id'=>'link-login'))}}

					</div>

				{{Form::close()}}
			</div>
			
			<div class="modal-main-lost-password" id="modal-main-lost-password">
			
				{{Form::open(array('action'=>'PropertyController@postLostpassword','class'=>'form','method'=>'post','id'=>'form-lost-password','required'=>'true'))}}
				
				<div class="form-group">
				<label>Username:</label>
				{{Form::text('user','',array('class'=>'form-control jform-control','placeholder'=>'Your User Name','required'=>'true','id'=>'lost-password-user'))}}
				
				</div>
				
				<div class="form-group">
				<label>Email:</label>
				{{Form::text('email','',array('class'=>'form-control jform-control','placeholder'=>'Your Email','required'=>'true','id'=>'lost-password-email'))}}
				
				</div>
				<br />
				<div class="form-group">
				
				{{Form::submit('Continue',array('class'=>'btn bt-large btn-primary ','style'=>'background:#29333d;','id'=>'link-lost-password'))}}
				
				</div>
				
				{{Form::close()}}
				</div>
			
			<div class="modal-main-confirm-password" id="modal-main-confirm-password">
			
				{{Form::open(array('url'=>URL::to('/confirmpassword',array(),false),'class'=>'form','method'=>'post','id'=>'form-confirm-password','required'=>'true'))}}
				
				<div class="form-group">
				<label>Username:</label>
				{{Form::text('user','',array('class'=>'form-control jform-control','placeholder'=>'Your Username','required'=>'true','id'=>'confirm-password-user'))}}
				
				</div>
				
				<div class="form-group">
				<label>Email:</label>
				{{Form::text('email','',array('class'=>'form-control jform-control','placeholder'=>'Your Email','required'=>'true','id'=>'confirm-password-email'))}}
				
				</div>
				
				<div class="form-group">
				<label>Confirmation Code:</label>
				{{Form::text('ccode','',array('class'=>'form-control jform-control','placeholder'=>'Your Confirmation Code Here','required'=>'true','id'=>'confirm-password-ccode'))}}
				
				</div>
				
				<div class="form-group">
				<label>Your New Password:</label>
				{{Form::password('password',array('class'=>'form-control jform-control','placeholder'=>'New Password','required'=>'true','id'=>'confirm-password-password'))}}
				
				</div>
				
				<div class="form-group">
				<label>Confirm Password:</label>
				{{Form::password('confirmation_password',array('class'=>'form-control jform-control','placeholder'=>'Confirm Password','required'=>'true','id'=>'confirm-password-cpassword'))}}
				
				</div>
				
				<div class="form-group">
				
				{{Form::submit('Change Password',array('class'=>'btn bt-large btn-primary ','style'=>'background:#29333d;','id'=>'link-confirm-password'))}}
				
				</div>
				
				{{Form::close()}}
			
			</div>
			</div>
		</div>
	</div>
</div>


<div class="gpc-chat-box">

<header class="gpc-chat-header">
<a href="#" class="pull-left" id="gpc-chat-username"></a>
<a href="#" class="pull-right gpc-header-widgets" id="gpc-chat-close"><i class="fa fa-close"></i></a>
<a href="#" class="pull-right gpc-header-widgets" id="gpc-chat-minimize"><i class="fa fa-compress" id="gpc-chat-ce"></i></a>
<div class="clearfix"></div>
</header>

<section class="gpc-chat-body">

<section class="gpc-chat-message-box">

<section class="gpc-chat-message pull-left">

<aside class="gpc-chat-guest-message ">hello!!</aside>

</section>

<section class="gpc-chat-message pull-right">

<aside class="gpc-chat-user-message ">
	hello back!!!
	efsdgsfghfdgfdgfdgfdgfdgfdgfdsssssssssssssssssssssssssdsfgdfgfdgfdgfdgfdgfdfdg fg fdg fd g fdg fd g
</aside>

</section>

</section>

<section class="clearfix"></section>

</section>

<footer class="gpc-chat-footer">
<textarea class="form-control gpc-chat-textarea"></textarea>

@if(isset($user))
<!--
<input type="hidden" id="from" value="{{Auth::user()->id}}" />
<input type="hidden" id="to"   value="{{$user->id}}" />
<input type="hidden" id="url"  value="{{URL::to('chat')}}" />
-->
@endif

</footer>

</div>



<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<script async src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
{{HTML::script('packages/js/gpc.js')}}

<style type="text/css">
	.city-class{display:block;cursor:pointer;}
</style>

<script type="text/javascript">	
	$('document').ready(function() {
		var url = "{{URL::to('setcloc')}}";
		$('.dropdown-menu a[data-toggle="tab"]').click(function (e) {
			e.stopPropagation();        
			$(this).tab('show');
		});
		$(document).on('click', '.country-class',function(){
			$('#location').val('');
			$('.country-location').html($(this).html());
			$('.ssform').find('input[name="country"]').val($(this).attr('data-country'));
			//$('.ssform').prepend('<input type="hidden" name="country" value="'+$(this).attr('data-country')+'" />');
			$('#location').val($(this).attr('data-country').toUpperCase());
			$.get(url, {country:$(this).attr('data-country')}).done(function(d){});
			
		});
		
		$(document).on('click', '.city-class', function(){
			if(!$(this).hasClass('has-cities')){
				$('.country-location').html($(this).html());
				$('.ssform').find('input[name="city"]').val($(this).attr('data-country'));
				//$('.ssform').prepend('<input type="hidden" name="city" value="'+$(this).attr('data-city')+'" />');
				var loc = $('#location').val();
				$('#location').val(loc+','+$(this).attr('data-city').toUpperCase());
				$.get("{{URL::to('setcity')}}", {city:$(this).attr('data-city')}).done(function(d){});
			}
		});
		
		$('.seloc').on('click', function(){
			$loc = $(this).attr('data-loc');
			$.get("{{URL::to('countryloc')}}", {loc:$loc}).done(function(d){
				if($loc == 'eu'){$('.eu').html(d);}else if($loc == 'world'){$('.world').html(d);}
			});
		});
		
		$(document).on('click', '.has-cities',function(e){
			e.stopPropagation(); 
			$('#location').val('');
			$('.country-location').html($(this).html());
			$('.ssform').find('input[name="country"]').val($(this).attr('data-country'));
			//$('.ssform').prepend('<input type="hidden" name="country" value="'+$(this).attr('data-country')+'" />');
			$('#location').val($(this).attr('data-country').toUpperCase());
			$.get(url, {country:$(this).attr('data-country')}).done(function(d){});
			$loc = $(this).attr('data-loc');
			$country = $(this).attr('data-country');
			$.get("{{URL::to('cityloc')}}", {loc:$country}).done(function(d){
				if($loc == 'eu'){$('.eu').html(d);}else if($loc == 'world'){$('.world').html(d);}
			});
		});
		
	});
</script>

</body>
</html>
