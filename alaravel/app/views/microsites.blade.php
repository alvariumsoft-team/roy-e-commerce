
@extends('main')
@section('content')

<style type="text/css">

.list-header{
margin:0px;
padding:10px;
background:#dedede;
}

.list-head{padding:30px;}

.listing{
box-sizing:border-box;
margin:10px;
box-shadow:0px 0px 1px #1d1d1d;

}

.status{float:right;background:#0097d6;color:#fefefe;padding:10px;border-radius:10px;font-size:16px;}


.list-price{width:100%;background:#27ae60;padding:5px 15px;padding-top:10px;color:#fefefe;box-shadow:0px 0px 2px #27ae60;}

.list-info{border-bottom:1px solid #d5d9dd;padding:10px;}

.list-left{text-align:left;}
.list-right{text-align:right;}

.timg{margin:20px 0px;}

.list-features{width:100%;background:#29333d;color:#b9bfc6;font-family:"Lato";box-shadow:0px 0px 2px #29333d;margin:0px;padding:0px;padding-top:10px;padding-bottom:10px;}

.list-features > ul{margin:0px;padding:0px;}

.list-features > ul > li{float:left;display:inline;list-style:none;padding-top:5px;padding-bottom:5px;padding-left:16px;padding-right:16px;background:transparent;}

.list-fmenu{border-right:1px solid #3b4d5d;}

body{font-family:"Lato";}

.list-headd{padding:10px 20px;}

	.nli{color:#6a6a6a !important;}
	.nli:hover{background:#fff !important;color:#1a1a1a !important;}
	
	.slidePanel{cursor:pointer;}
	.slide-minus{display:none;}
	.panel-body{display:none;}
	
	.image-ul{border-bottom: 2px solid #EF8600;}
	
	.image-types{
		text-align:center;
		display:block;
		color:#2a2a2a;
		font-size:18px;
		font-weight:700;
		padding-top:24px;
		outline:none !important;
	}
	
	.image-types > .fa{
		margin:0px 14px;
	}
	
	.image-types  {padding-left:25%;}
	
	.image-types{color:#4ba7e4;}
	.image-types:hover{color:#00a65a;}
	
	.bx-prev{background: rgba(0,0,0,0.2) !important;}
	.bx-next{background: rgba(0,0,0,0.2) !important;}
	.flexslider .slides img {height: 500px !important;}
	.slider{display:block;}
	.floor-plan-detail{display:block;}
	
	.degnava{padding:16px 24px;text-decoration:none !important;color:#4ba7e4;font-weight:700;}
	.degnava:hover{color:#00a65a;}

</style>

<?php $arr = array(); $arrr = array(); $prc = array(); $ara = array();?>

@foreach($units as $u)
@if(!in_array($u->beds, $arr))
<?php $arr[] = $u->beds; ?>
<?php $arrb[] = $u->baths; ?>
<?php $prc[] = $u->price; ?>
<?php $ara[] = $u->area; ?>
@endif
@endforeach

<div class="list-header">

<div class="container"><div><h4>Listing</h4></div></div>

</div>

<div class="container">

<div class="row">

<div class="col-md-10">

<div class="about">

<div class="listing">

<div class="list-head">

<div class="status">For&nbsp; Sale</div>
<div class="clear"></div>

<div><h1 style="margin-top:0px !important;">{{ucwords($project['name'])}}</h1></div>

<div>{{$project['address']}}</div>

</div>

<div class="list-price" style="padding:2px!important;padding-left:10px!important;"><h3>${{min($prc)}} onwards</h3></div>

<div class="list-head">

<div class="list-info">
<div class="row">

<div class="col-md-6 list-left">Beds</div>
<div class="col-md-6 list-right"><?php $arlen = count($arr);for($i=0;$i<$arlen;$i++){echo $arr[$i];if($i != ($arlen-1)){echo ',';}}?> BHK</div>

</div>
</div>

<div class="list-info">
<div class="row">

<div class="col-md-6 list-left">Baths</div>
<div class="col-md-6 list-right"><?php $arlen = count($arrb);for($i=0;$i<$arlen;$i++){echo $arrb[$i];if($i != ($arlen-1)){echo ',';}}?></div>

</div>
</div>

<div class="list-info">
<div class="row">

<div class="col-md-6 list-left">Sq. Ft.</div>
<div class="col-md-6 list-right">{{min($ara)}} to {{max($ara)}} sq.ft.</div>

</div>
</div>


</div>

<div class="timg">{{HTML::image($project->header,'alt',array('height'=>'500px','width'=>'100%'))}}</div>

<div class="list-features">
<ul>
	<li class="list-fmenu"><i class="fa fa-bars"></i></li>
	<li><a href="#" style="color:#dedede!important;">About</a></li>
	<li><a href="#listing_features" style="color:#dedede!important;">Features</a></li>
	<li><a href="#location" style="color:#dedede!important;">Location</a></li>
	<li><a href="#" style="color:#dedede!important;">Images</a></li>
	<li><a href="#" style="color:#dedede!important;">Floor Plan</a></li>
	<li><a href="#" style="color:#dedede!important;"class="image-degree" >360&deg; Images</a></li>
	<li><a href="#" style="color:#dedede!important;">Video</a></li>
	<li><a href="#" style="color:#dedede!important;">Map</a></li>
	<li><a href="#contact" style="color:#dedede!important;">Contact</a></li>
</ul>
<div class="clear"></div>
</div>

<div class="list-headd" id="listing_features">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">About</h3>
<div >
{{$project['about']}}
</div>
</div>

<div class="list-headd" id="listing_features">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Features</h3>
<div >

</div>
</div>

<div class="list-headd" id="location">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Location</h3>
<div>
{{$project['address']}}
</div>
</div>


<div class="list-headd" id="location">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Floor Plan</h3>
<div>


@foreach($units as $u)

@if(!in_array($u->beds, $arrr))

<?php /*$arrr[] = $u->beds;*/ ?>

<div class="panel panel-default">
<div class="panel-heading clearfix">
<span class="pull-left"><b>{{$u->beds}} BHK {{ucwords($u->list_type)}}({{$u->area}} sqft) - ${{$u->price}}</b></span>
	
<i class="fa fa-plus slidePanel slide-plus pull-right"></i><i class="fa fa-minus slidePanel slide-minus pull-right"></i>
</div>
  <div class="panel-body">
	<section>
		<div class="row">
			<div class="col-md-3">
			<div class="bd-example bd-example-tabs" role="tabpanel">
			  <ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item active">
				  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;"><b>Floor Plan</b></a>
				</li>
			  </ul>
			  <div class="tab-content" id="myTabContent">
				<div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab" aria-expanded="false">
				  <p><img class="floor-plan-img" class="img-responsive" src="{{URL::to($u->floor_plan)}}" style="cursor:pointer;width:100%;height:auto;" ></p>
				</div>
			  </div>
			</div>
				<!--<img class="img-responsive" src="{{URL::to($u->floor_plan)}}" >-->
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-amenities" >Amenities</button>
				</div>
				<div class="form-group">
				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-spec">Specifications</button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4">
					<h4>Carpet Area</h4>
					<b>{{$u->area}}sqft</b>
					<br /><br />
					<h4>Furnished</h4>
					<b>{{ucwords($u->furnished)}}</b>
					</div>
					<div class="col-md-4">
					<h4>Price</h4>
					<b>${{$u->price}}</b>
					<br /><br />
					<h4>No. Of Balconys</h4>
					<b>{{$u->balconys}}</b>
					</div>
					<div class="col-md-4">
					<h4>Construction Status</h4>
					<b>@if($u->build_status == 'under') Under Construction @else {{ucwords($u->build_status)}} @endif</b>
					<br /><br />
					<h4>No. Of Bathrooms</h4>
					<b>{{$u->baths}}</b>
					</div>
				</div>
			</div>
		</div>
	</section>
  </div>
</div>

@endif
		
@endforeach	



</div>
</div>


<div class="list-headd">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Images</h3>
<div >

	<div class="row">
		<div>
			@foreach($images as $image)
				<div class="col-lg-4" >
					<div class="five" style="margin-bottom:20px;">
						<p style="height:200px;"><img class="floor-plan-img img-responsive" src="{{URL::to($image->image)}}" style="width:100%;height:100%;cursor:pointer;"></p>
					</div>
				</div>
			@endforeach
		</div>
	</div>

</div>
</div>


<div class="list-headd">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Video</h3>
<div >

      <p>
		<iframe style="width:100%; height:500px;" src="{{$project->vlink}}" frameborder="0" allowfullscreen></iframe>
      </p>

</div>
</div>


<div class="list-headd">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Map</h3>
<div >

	<section>
		<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
			<div id="map" style="height:500px;width:100%">
				
			</div>
		</div>
	</section>

</div>
</div>

<div class="list-header">

<div class="row">

<div class="col-md-1"></div>
<div class="col-md-10">

<div class="about" id="contact">
<h3>Request More Information</h3>
<div class="form-group">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'Your E-mail'))}}</div>
<div class="form-group">{{Form::textarea('comment','',array('class'=>'form-control','placeholder'=>'Your Comment'))}}</div>
<div class="form-group">{{Form::submit('submit',array('class'=>'btn btn-primary btn-default btn-large btn-block'))}}</div>
</div></div>
<div class="col-md-1"></div>

</div>

</div>


</div>



</div>

</div>

<div class="col-md-2">
			<a class="twitter-timeline" href="https://twitter.com/CowboyGlobal">Tweets by CowboyGlobal</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>	
</div>

</div>

</div>


<div class="modal fade" id="unit-extras-modal" tabindex ="-1" role="dialog" aria-labelledby="unit-extras-heading" aria-hidden="true" style="top:17%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="unit-extras-heading"></div>
			</div>
			<div class="modal-body" id="unit-extras-body"></div>
			
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="floor-plan-modal" tabindex ="-1" role="dialog" aria-labelledby="floor-plan-heading" aria-hidden="true" style="top:10%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="floor-plan-heading"></div>
			</div>
			<div class="modal-body" id="floor-plan-body">
				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="contact-form-modal" tabindex ="-1" role="dialog" aria-labelledby="floor-plan-heading" aria-hidden="true" style="top:10%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="modal-title" id="contact-form-heading"><h4 style="text-align:center;color:#449d44;">Message Sent Successfully</h4></div>
			</div>
			<div class="modal-body" id="contact-form-body">
				<h3 style="text-align:center;color:#449d44;">Advertiser will contact you soon.</h3>
			</div>
			
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="photos-modal" style="position:fixed;transition:0.5s;top:0px;height:96%;width:98%;margin:1%;background:#fff;z-index:10000000;box-shadow: 1px 1px 5px #2a2a2a;display:none;">
	<div class="row">
		<div class="col-md-12 clearfix" >
			
			<div class="pull-left">
				<nav class="degnav clearfix" style="display:none;">
				  <a class="degnava pull-left deg-front" href="javascript:void();">Front Yard</a>
				  <a class="degnava pull-left deg-bedroom" href="javascript:void();">Bedroom</a>
				  <a class="degnava pull-left deg-storage" href="javascript:void();">Storage</a>
				  <a class="degnava pull-left deg-backyard" href="javascript:void();">Backyard</a>
				  <a class="degnava pull-left deg-hall" href="javascript:void();">Hall</a>
				  <a class="degnava pull-left deg-living" href="javascript:void();">Living Room</a>
				  <a class="degnava pull-left deg-kitchen" href="javascript:void();">Kitchen</a>
				  <a class="degnava pull-left deg-overflow" href="javascript:void();">Overflow</a>
				  <a class="degnava pull-left deg-bathroom" href="javascript:void();">Bathroom</a>
				</nav>
			</div>
			
			<div class="pull-right" style="padding:10px;">
				<a href="" class="sad">
					<img src="{{URL::to('packages/images/cancel.png')}}" style="width:25px;"/>
				</a>
			</div>
		</div>
		<div class="col-md-12" >
			
			<section class="slider" style="display:none;">
				<div class="flexslider">
				  <ul class="slides" id="slides-html">
					@foreach($images as $image)
					<li data-thumb="{{URL::to($image->image)}}"><img src="{{URL::to($image->image)}}" /></li>
					@endforeach
				  </ul>
				</div>
			</section>
			<section class="floor-plan-detail" style="display:none;">
				<div class="bd-example bd-example-tabs" role="tabpanel">
				  <ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item active">
					  <a class="nav-link" id="2d-tab" data-toggle="tab" href="#2d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>2D</b></a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" id="3d-tab" data-toggle="tab" href="#3d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>3D</b></a>
					</li>
				  </ul>
				  <div class="tab-content" id="myTabContent">
					<div role="tabpanel" class="tab-pane fade active in" id="2d" aria-labelledby="2d-tab" aria-expanded="false">
					  <p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floor_plan)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>
					</div>
					<div role="tabpanel" class="tab-pane fade  in" id="3d" aria-labelledby="3d-tab" aria-expanded="false">
						<p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floorplan3d)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>
					</div>
				  </div>
				</div>
			</section>
			<section class="degree" id="gdegree" style="display:none;">

			</section>
		</div>
	</div>
</div>
	

{{HTML::script('packages/js/jquery.flexslider.js')}}
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/three.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/CanvasRenderer.js')}}"></script>
<script type="text/javascript" src="{{URL::to('public/canvas/Projector.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('.slidePanel').on('click', function(){
		
		$(this).siblings().css('display','block');
		$(this).css('display','none');
		
		if($(this).hasClass('slide-plus')){
			$('.panel-body').css('display','none');
			$('.slide-minus').css('display','none');
			$('.slide-plus').css('display','block');
		$(this).siblings().css('display','block');
		$(this).css('display','none');
			var panelBody = $(this).parent().next()[0];
			$(panelBody).css('display','block');
		}
		
		if($(this).hasClass('slide-minus')){var panelBody = $(this).parent().next()[0];$(panelBody).css('display','none');}
		
			
		});	

		$('.unit-extras').on('click', function(){

		var id = $(this).attr('id');
		var heading = '';
		var content = '';
		$('#unit-extras-heading').html(heading);
		$('#unit-extras-body').html(content);
		if(id == 'unit-amenities'){
			heading = 'Amenities';
			$.post("{{URL::to('projectspec')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});
		}
		if(id == 'unit-ladvantages'){
			heading = 'Local Advantages';
			
		}
		if(id == 'unit-usp'){
			heading = 'Project USP';
		}
		if(id == 'unit-spec'){
			heading = 'Specifications';
			$.post("{{URL::to('projectamenities')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});
		}

		$('#unit-extras-heading').html('<h4>'+heading+'</h4>');


		$('#unit-extras-modal').modal('show');	
			
		});

		$('.floor-plan-img').on('click',function(){
			
			$('#floor-plan-body').html($(this).parent().html());
			$('#floor-plan-modal').modal('show');	
			
		});

		latitude = <?php echo "'$project->map_lat'" ?>;
		longitude = <?php echo "'$project->map_long'" ?>;
		
		
		if((latitude == '') || (latitude == undefined)){
			latitude	= 28.7041;
			longitude	= 77.1025;
		}
		
		setTimeout(function() {
			
		
		latitude = parseFloat(latitude);
		longitude= parseFloat(longitude);

			var uluru = {lat: latitude, lng: longitude};
			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 10,
			  scrollwheel: false,
			  center: uluru
			});
			var marker = new google.maps.Marker({
			  position: uluru,
			  map: map
			});
			
		}, 50);
		
		$('#contact-form-btn').on('click', function(e){
			e.preventDefault();
			var url = '';
			//$.post(url, {}).done(function(data){
			
			$('#contact-form-modal').modal('show');	
				
			//}
			
			
		});

		$('.image-photos').on('click', function(e){
			e.preventDefault();
				$('.slider').css('opacity', '0');
				$('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				});
				$('.photos-modal').css('display', 'block');
				$('.slider').css('display', 'block');
				setTimeout(function(){$('.slider').css('opacity', '1');}, 300);
				$('.image-photos').focus();
		});
		
		$('.image-plan').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'block');
			$('.floor-plan-detail').css('display', 'block');
			$('.image-photos').focus();	
		});
		
		$('.image-degree').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'block');
			$('.degree').css('display', 'block');	
			$('.degnav').css('display', 'block');
			$('.gcanvas').focus();
			panaroma(front);
			
		});

		$('.sad').on('click', function(e){
			e.preventDefault();
			$('.photos-modal').css('display', 'none');
			$('.slider').css('display', 'none');
			$('.floor-plan-detail').css('display', 'none');
			$('.degree').css('display', 'none');
			$('.degnav').css('display', 'none');
		});

	});
	
</script>

<script>

<?php
	$s = [];
	foreach($f360 as $f){
		$t = URL::to($f->image);
		$s[$f->type][] = "'".$t."'";
	}
	
	foreach($s as $k=>$v){
		$v = implode(', ', $v);
		echo "var $k = [ $v ];\n";
		echo "$('.deg-$k').on('click', function(){
			$('canvas').remove();
			panaroma($k);
		});\n";
	}
?>

	var camera, scene, renderer;
	var texture_placeholder,
	isUserInteracting = false,
	onMouseDownMouseX = 0, onMouseDownMouseY = 0,
	lon = 90, onMouseDownLon = 0,
	lat = 0, onMouseDownLat = 0,
	phi = 0, theta = 0,
	target = new THREE.Vector3();	

	function panaroma(img){
		if(img.length > 0){
			init(img);
			animate();
		}
	}

	function init(img) {
		var materials = [];
		
		var imglen = img.length;
		var i = 0;
		for(i=0;i<imglen;i++){
			
			console.log(img[i]);
			
			materials.push(loadTexture( img[i] ));
		}
		
		

		var container, mesh;

		container = document.getElementById( 'gdegree' );

		camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1100 );

		scene = new THREE.Scene();

		texture_placeholder = document.createElement( 'canvas' );
		texture_placeholder.width = 128;
		texture_placeholder.height = 128;

		var context = texture_placeholder.getContext( '2d' );
		context.fillStyle = 'rgb( 200, 200, 200 )';
		context.fillRect( 0, 0, texture_placeholder.width, texture_placeholder.height );



		mesh = new THREE.Mesh( new THREE.BoxGeometry( 300, 300, 300, 7, 7, 7 ), new THREE.MultiMaterial( materials ) );
		mesh.scale.x = - 1;
		scene.add( mesh );

		renderer = new THREE.CanvasRenderer();
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( (window.innerWidth * 0.968), 500);
		container.appendChild( renderer.domElement );

		document.addEventListener( 'mousedown', onDocumentMouseDown, false );
		document.addEventListener( 'mousemove', onDocumentMouseMove, false );
		document.addEventListener( 'mouseup', onDocumentMouseUp, false );
		document.addEventListener( 'wheel', onDocumentMouseWheel, false );

		document.addEventListener( 'touchstart', onDocumentTouchStart, false );
		document.addEventListener( 'touchmove', onDocumentTouchMove, false );

		//

		window.addEventListener( 'resize', onWindowResize, false );

	}

	function onWindowResize() {

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( (window.innerWidth * 0.968), 500);

	}

	function loadTexture( path ) {

		var texture = new THREE.Texture( texture_placeholder );
		var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );

		var image = new Image();
		image.onload = function () {

			texture.image = this;
			texture.needsUpdate = true;

		};
		image.src = path;

		return material;

	}

	function onDocumentMouseDown( event ) {

		event.preventDefault();

		isUserInteracting = true;

		onPointerDownPointerX = event.clientX;
		onPointerDownPointerY = event.clientY;

		onPointerDownLon = lon;
		onPointerDownLat = lat;

	}

	function onDocumentMouseMove( event ) {

		if ( isUserInteracting === true ) {

			lon = ( onPointerDownPointerX - event.clientX ) * 0.1 + onPointerDownLon;
			lat = ( event.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;

		}

	}

	function onDocumentMouseUp( event ) {

		isUserInteracting = false;

	}

	function onDocumentMouseWheel( event ) {

		camera.fov += event.deltaY * 0.05;
		camera.updateProjectionMatrix();

	}

	function onDocumentTouchStart( event ) {

		if ( event.touches.length == 1 ) {

			event.preventDefault();

			onPointerDownPointerX = event.touches[ 0 ].pageX;
			onPointerDownPointerY = event.touches[ 0 ].pageY;

			onPointerDownLon = lon;
			onPointerDownLat = lat;

		}

	}

	function onDocumentTouchMove( event ) {

		if ( event.touches.length == 1 ) {

			event.preventDefault();

			lon = ( onPointerDownPointerX - event.touches[0].pageX ) * 0.1 + onPointerDownLon;
			lat = ( event.touches[0].pageY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;

		}

	}

	function animate() {

		requestAnimationFrame( animate );
		update();

	}

	function update() {

		if ( isUserInteracting === false ) {

			lon += 0.07;

		}

		lat = Math.max( - 85, Math.min( 85, lat ) );
		phi = THREE.Math.degToRad( 90 - lat );
		theta = THREE.Math.degToRad( lon );

		target.x = 500 * Math.sin( phi ) * Math.cos( theta );
		target.y = 500 * Math.cos( phi );
		target.z = 500 * Math.sin( phi ) * Math.sin( theta );

		camera.lookAt( target );

		renderer.render( scene, camera );

	}

</script>



@stop
