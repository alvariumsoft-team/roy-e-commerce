@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add New User</h2>

<p>Create a brand new user and add them to this site.</p>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Username:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('username','',array('class'=>'form-control','placeholder'=>'User name','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Firstname:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('firstname','',array('class'=>'form-control','placeholder'=>'First name','required'=>'true'))}}</label>
</div>


<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Lastname:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('lastname','',array('class'=>'form-control','placeholder'=>'Last name','required'=>'true'))}}</label>
</div>

<div class="form-group row">	
<label class="col-lg-3 col-sm-3 col-xs-12" ><b>Profile Image:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12"  for="ifile-id" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;"><span id='ifile-label' >Upload Image</span>{{Form::file('file',array('class'=>'inputfile inputfile-1','id'=>'ifile-id','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Password:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::password('password',array('class'=>'form-control','placeholder'=>'password','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Confirm Password:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'confirm password','required'=>'true'))}}</label>
</div>



<div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Phone:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('list_phone','',array('class'=>'form-control','placeholder'=>'Phone Number'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Email:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'E-mail'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Website:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('list_website','',array('class'=>'form-control','placeholder'=>'Your Website'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Address:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::textarea('list_address','',array('class'=>'form-control','placeholder'=>'Your Address'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Country:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('country','',array('class'=>'form-control','placeholder'=>'Your Country'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>State:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('state','',array('class'=>'form-control','placeholder'=>'Your State'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>City:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('city','',array('class'=>'form-control','placeholder'=>'Your City'))}}</label>
</div>

</div>

<div class="form-group">

{{Form::submit('ADD',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>

@stop
