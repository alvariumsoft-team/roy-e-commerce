<!DOCTYPE HTML>

<html lang="en">
 
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>..:: Real Estate ::.</title>
 
{{HTML::style('packages/realestate/css/site.css')}}
{{HTML::style('packages/realestate/css/media.css')}}
{{HTML::style('packages/realestate/css/bootstrap.css')}}
{{HTML::style('packages/realestate/css/bootstrap-theme.css')}}
{{HTML::style('packages/realestate/css/animate.min.css')}}

{{HTML::script('packages/realestate/js/jquery.js')}}
 
 <script>
function sticky_relocate() {
  var window_top = $(window).scrollTop();
  var div_top = $('#sticky-anchor').offset().top;
  if (window_top > div_top) {
    $('#sticky').addClass('stick');
	$(".navbar-default .navbar-nav > li > a ").css("color", "#3a3a3a");
       
  } else {
    $('#sticky').removeClass('stick');
	$(".navbar-default .navbar-nav > li > a ").css("color", "#333");

  }
}

$(function() {
  $(window).scroll(sticky_relocate);
  sticky_relocate();
});
</script>


<style type="text/css">		

		/* general style */
		.preview {
			width: 90px;
			height:90px;
			position: absolute;
			top:0;
			left:90px;
			z-index:100;
			-webkit-transition:  all 0.3s ease-out; 
			-moz-transition:  all 0.3s ease-out; 
			transition:  all 0.3s ease-out; 	
		}
		
		.preview img {
			position: absolute;
			left:0;
			top:0;
			width: 90px;
		}
		
		.preview .alt {
			display:none;
		}
		
		
		/* prev button */				
		.flex-prev {
			-webkit-perspective-origin: 100% 50%;
			perspective-origin: 100% 50%;
			-webkit-perspective: 1000px;
			perspective: 1000px;			
		}
		
		.flex-prev .preview {
			-webkit-transform: rotateY(90deg);
			transform: rotateY(90deg);
			-webkit-transform-origin: 0% 50%;
			transform-origin: 0% 50%;
		}
		
		
		/* next button */				
		.flex-next {
			-webkit-perspective-origin: 0% 50%;
			perspective-origin: 0% 50%;			
			-webkit-perspective: 1000px;
			perspective: 1000px;				
		}
		
		.flex-next .preview {
			right:90px;
			left:auto;	
			-webkit-transform: rotateY(-90deg);
			transform: rotateY(-90deg);
			-webkit-transform-origin: 100% 100%;
			transform-origin: 100% 100%;
		}
		
		.flex-next .preview img {
			position: absolute;
			left:0;
			top:0;
			width: 90px;
		}
		
		
		/* hover style */				
		.flex-direction-nav a:hover .preview {
			opacity:1;
			-webkit-transform: rotateY(0deg);
			transform: rotateY(0deg);			
		}
	
	
		/* different hover style for flexslider nav */
		.flex-direction-nav a {
			-webkit-transition:  none; 
			-moz-transition: none; 
			transition:  none;		
		}	
	
	</style>
 
 <style type="text/css">
 
 .header-top{
 
 background:#1a1a1a;
 
 }
 
 r{float:right;}
 
 </style>
 
 </head>
 
 
<body>
 
<header class="top_header ">

<div class="container">

<div class="row">

<div class="col-md-6"></div>

<div class="col-md-6">

<ul class="social_media r">

<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class=" fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus"></i></a></li>

<li><i class="fa fa-phone"> </i>+ 0522 26 565522 </li>
<li><i class="fa fa-envelope-o"> </i><a href="mailto:info@yourdomain.com">info@yourdomain.com</a></li>

</ul>

</div>
</div>

</div>

</header>

 
 
<div class="cont_box1">


<div id="sticky-anchor"></div>
<div id="sticky">
<div class="menu_container">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="logo"><img src="packages/realestate/images/logo.png"></div><!--logo-->



<nav role="navigation" class="navbar navbar-default"> 

        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
<div id="navbarCollapse" class="collapse navbar-collapse">
  <ul class="menu nav navbar-nav">
    <li><a class="homer" href="#"> HOME</a>
      <ul class="sub-menu">
        <li><a href="#">Sub-Menu 1</a></li>
        <li><a href="#">Sub-Menu 2</a></li>
        <li><a href="#">Sub-Menu 3</a></li>
        <li><a href="#">Sub-Menu 4</a></li>
        <li><a href="#">Sub-Menu 5</a></li>
      </ul>
    </li>
    <li><a  href="#"> CATEGORIES</a>
      <ul class="sub-menu">
        <li><a href="#">Sub-Menu 1</a></li>
        <li><a href="#">Sub-Menu 2</a>
          <ul>
            <li><a href="#">Sub Sub-Menu 1</a></li>
            <li><a href="#">Sub Sub-Menu 2</a></li>
            <li><a href="#">Sub Sub-Menu 3</a></li>
            <li><a href="#">Sub Sub-Menu 4</a></li>
            <li><a href="#">Sub Sub-Menu 5</a></li>
          </ul>
        </li>
        <li><a href="#">Sub-Menu 3</a>
          <ul>
            <li><a href="#">Sub Sub-Menu 1</a></li>
            <li><a href="#">Sub Sub-Menu 2</a></li>
            <li><a href="#">Sub Sub-Menu 3</a></li>
            <li><a href="#">Sub Sub-Menu 4</a></li>
            <li><a href="#">Sub Sub-Menu 5</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a  href="#">PORTFOLIO</a>
      <ul class="sub-menu">
        <li><a href="#">Sub-Menu 1</a></li>
        <li><a href="#">Sub-Menu 2</a>
          <ul>
            <li><a href="#">Sub Sub-Menu 1</a></li>
            <li><a href="#">Sub Sub-Menu 2</a></li>
            <li><a href="#">Sub Sub-Menu 3</a></li>
            <li><a href="#">Sub Sub-Menu 4</a></li>
            <li><a href="#">Sub Sub-Menu 5</a></li>
          </ul>
        </li>
        <li><a href="#">Sub-Menu 3</a>
          <ul>
            <li><a href="#">Sub Sub-Menu 1</a></li>
            <li><a href="#">Sub Sub-Menu 2</a></li>
            <li><a href="#">Sub Sub-Menu 3</a></li>
            <li><a href="#">Sub Sub-Menu 4</a></li>
            <li><a href="#">Sub Sub-Menu 5</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a  href="#"> ABOUT</a></li>
    <li><a  href="#"> BLOG</a></li>
    <li><a  href="#"> CONTACT</a></li>
  </ul>
  </div>
  
</nav>

</div><!--col-md-12-->




</div><!--sticky-->
</div><!--row-->
</div><!--container-->
</div>
<!--menu_container-->

<div class="container-fluid">
<div id="hero">
		
		
		<div class="flexslider">
		  <ul class="slides">
		    <li>
					<img src="packages/realestate/img/full/1.jpg" alt="icy Mountain" data-thumbnail="img/thumbnail/1.jpg" />			
					<div class="banner_text_box">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
					</div><!--banner_text_box-->
					
						</li>		
		 		<li>
					<img src="packages/realestate/img/full/2.jpg" alt="Stoney Creek" data-thumbnail="img/thumbnail/2.jpg" />				</li>
		    <li>
					<img src="packages/realestate/img/full/1.jpg" alt="Narrow Road" data-thumbnail="img/thumbnail/1.jpg" />		
					<div class="banner_text_box">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
<button class="btn_slider">Book Now</button> 
					</div><!--banner_text_box-->
					
							</li>						 		
		    <li>
					<img src="packages/realestate/img/full/3.jpg" alt="Grey Sky" data-thumbnail="img/thumbnail/3.jpg"/>				</li>		
		    <li>
					<img src="packages/realestate/img/full/1.jpg" alt="Wood Pattern" data-thumbnail="img/thumbnail/1.jpg" />			
					
					<div class="banner_text_box2">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
<button class="btn_slider">Book Now</button> 
					</div><!--banner_text_box-->
						</li>
		  </ul>
		</div>		
	</div>
</div><!--contianer_fluid-->
</div>
<!--cont_box1-->
 
 
<div class="map_cont">

<div class="container">

    <section class="section  general-row">
		<div class="wpb_column col-md-12">
		


<!--Properties Section-->

    <section class="properties-section">

    	<div class="auto-container">

        

            <!--Section Title-->

            <div class="sec-title">

                <h2>About <span class="theme_color">Property</span></h2>

                <div class="separator small-separator"></div>

                <div class="text"><p>Real Estate agents are Property consisting of land and the buildings on it, along with its seds naturals resources such seds as crops, minerals, or water; immovable property of this nature.Since this was a limited seds unit auction, we had approached by Developers to release incremental units to fulfill the demands at a stepped up cut-off price.</p></div>

            </div>

            

            <!--Full Image Box-->

            
            

            <div class="five-col-theme">

                <div class="row clearfix">

                    


<!--Column-->

<article class="column">

    <div class="inner-box">

         <div class="icon"><img src="packages/realestate/images/i1.jpg"></div>

        <h4 class="title">Bedrooms</h4>

        <h3 class="count">3</h3>

    </div>

</article>






<!--Column-->

<article class="column">

    <div class="inner-box">

          <div class="icon"><img src="packages/realestate/images/i2.jpg"></div>

        <h4 class="title">Square Feet</h4>

        <h3 class="count">2530</h3>

    </div>

</article>






<!--Column-->

<article class="column">

    <div class="inner-box">

         <div class="icon"><img src="packages/realestate/images/i3.jpg"></div>

        <h4 class="title">Baths</h4>

        <h3 class="count">2</h3>

    </div>

</article>






<!--Column-->

<article class="column">

    <div class="inner-box">

         <div class="icon"><img src="packages/realestate/images/i4.jpg"></div>

        <h4 class="title">Year Build</h4>

        <h3 class="count">2010</h3>

    </div>

</article>






<!--Column-->

<article class="column">

    <div class="inner-box">

        <div class="icon"><img src="packages/realestate/images/i5.jpg"></div>

        <h4 class="title">Car Parking</h4>

        <h3 class="count">5</h3>

    </div>

</article>




                </div>

            </div>

            

        </div>

    </section>




	</div>

	<div class="clearfix"></div>
	</section>

</div><!--container-->

</div><!--map_cont-->
 
 
 
 
 
 

<div class="cont_box3">

<div class="container">

    <section class="section  general-row">
		<div class="wpb_column col-md-12">
		   

<!--Tabs Section-->

<div class="sec-title">

                <h2>Property  <span class="theme_color">Details</span></h2>

                <div class="separator small-separator"></div>

                <div class="text"><p>Real Estate agents are Property consisting of land and the buildings on it, along with its seds naturals resources such seds as crops, minerals, or water</p></div>

            </div>

<section style="background-image:url('packages/realestate/images/image-21.jpg');" class="property-details">

    	<div class="auto-container">

        	<div class="row clearfix">

            	

                <!--Left Column-->

                <div class="col-md-7 col-sm-12 col-xs-12 left-column">

                   
                    <div class="Popular col-md-6 col-xs-12">
                        
                        <img src="packages/realestate/images/jardin-4-370x270.jpg">
                        <div class="Popular_title_next">Lucknow</div><!--Popular_title_next-->
                        <div class="Popular_title">For sale Rs.9000000/</div><!--Popular_title-->
                    </div><!--Popular-->
                     <div class="Popular col-md-6 col-xs-12">
                        
                         <img src="packages/realestate/images/product-4.jpg">
                        <div class="Popular_title_next">Lucknow</div><!--Popular_title_next-->
                        <div class="Popular_title">For sale Rs.9000000/</div><!--Popular_title-->
                    </div><!--Popular-->
                        
                        
                </div>

            </div>

        </div>

    </section>






<section class="properties-section">

    	<div class="auto-container">

        

            <!--Section Title-->

            <div class="sec-title">

                <h2>Popular House  <span class="theme_color">Details</span></h2>

                <div class="separator small-separator"></div>

                <div class="text"><p>Real Estate agents are Property consisting of land and the buildings on it, along with its seds naturals resources such seds as crops, minerals, or water</p></div>

            </div>

                       

        </div>

    </section>
	</div>

	<div class="clearfix"></div>
	</section>
    
    
    
</div><!--container-->
</div><!--cont_box3-->

<div class="cont_box_4">

<div class="container">
<div class="row">
<div class="col-md-4">

<div class="worl_w_box">
<div class="worl_w_left_top">
    <i class="fa fa-home home_icon"></i>
</div><!--worl_w_left_top-->
<div class="worl_w_right_top">
<h3><a href="">World wide Property</a></h3>
<h4><a href="">Any destination you need</a></h4>
</div><!--worl_w_left_top-->

<div class="wc_para">
<p>In accordance with your needs we organise domestic and international deliveries of collective, complete and partial shipments. </p>
</div><!--wc_para-->

</div><!--worl_w_box-->
<div class="worl_w_box">
<div class="worl_w_left_top">
     <i class="fa fa-home home_icon"></i>
</div><!--worl_w_left_top-->
<div class="worl_w_right_top">
<h3><a href="">World wide Property</a></h3>
<h4><a href="">Any destination you need</a></h4>
</div><!--worl_w_left_top-->

<div class="wc_para">
<p>In accordance with your needs we organise domestic and international deliveries of collective, complete and partial shipments. </p>
</div><!--wc_para-->

</div><!--worl_w_box-->

</div><!--col-md-4-->

<div class="col-md-4">

<div class="worl_w_box">
<div class="worl_w_left_top">
 <i class="fa fa-home home_icon"></i>
</div><!--worl_w_left_top-->
<div class="worl_w_right_top">
<h3><a href="">World wide Property</a></h3>
<h4><a href="">Any destination you need</a></h4>
</div><!--worl_w_left_top-->

<div class="wc_para">
<p>In accordance with your needs we organise domestic and international deliveries of collective, complete and partial shipments. </p>
</div><!--wc_para-->

</div><!--worl_w_box-->
<div class="worl_w_box">
<div class="worl_w_left_top">
 <i class="fa fa-home home_icon"></i>
</div><!--worl_w_left_top-->
<div class="worl_w_right_top">
<h3><a href="">World wide Property</a></h3>
<h4><a href="">Any destination you need</a></h4>
</div><!--worl_w_left_top-->

<div class="wc_para">
<p>In accordance with your needs we organise domestic and international deliveries of collective, complete and partial shipments. </p>
</div><!--wc_para-->

</div><!--worl_w_box-->

</div><!--col-md-4-->


<div class="col-md-4">

<div class="vc_thumb_box">
<figure class="wpb_single_image vc_figure">
    <img width="347" height="166" sizes="(max-width: 347px) 100vw, 347px" src="packages/realestate/images/11.jpg">		</figure>
<h5 class="style_3">
<span>YOUR CARGO OUR CONCERN</span>
</h5>
<div class="wpb_wrapper">
			<p><span>We offer a big storage space, heated and with air condition, to store your good?s safe and organized even for longer period of time.</span></p>



		</div>
</div><!--vc_thumb_box-->

</div><!--col-md-4-->

</div><!--row-->

</div><!--container-->
</div><!--cont_box_3-->



<div class="cont_box_5 hidden-xs">

    <section class="section  general-row">
		<div class="wpb_column col-md-12">
		


<!--Intro Section-->

<section class="intro-section">

    <div class="auto-container">

        <div class="outer-box clearfix">

            <span data-wow-duration="1000ms" data-wow-delay="0ms" class="anim-image wow fadeInUp animated" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;"><img alt="" src="http://wp1.themexlab.com/wp/dreamland/wp-content/uploads/2015/12/logo-2.png"></span>

            <div class="col-md-9 col-sm-7 col-xs-12">

                <p>Real Estate agents are Property consisting of land and the buildings on it, along with its seds naturals resources such seds as crops, minerals, or water.</p>

            </div>

            <div class="col-md-3 col-sm-5 col-xs-12 text-right">

                <a class="theme-btn btn-style-two" href="#">Contact Now </a>

            </div>

        </div>

    </div>

</section>




	</div>

	<div class="clearfix"></div>
	</section>

</div><!--cont_box_5-->
 
 
 
 
<div class="footer_site">

<div class="container">
<div class="row">
<div class="col-md-3">
<div class="footer_ul">
<ul>

<li><img src="packages/realestate/images/logo.png" width="160px"></li>
<li><div>For more then 30 years,Real Estate agents are Property consisting  in the field of logistics and cargo forwarding.</div></li>

</ul>

</div><!--footer_ul-->

</div><!--col-md-3-->

<div class="col-md-3">

<div class="footer_link_ul">

<ul>
<li><h3>Navigation</h3></li>
<li><a href="" class="activ">Home</a></li><li><a href="">Terms &AMP; Conditions</a></li>
<li><a href="">Services</a></li><li><a href="">About Us</a></li>
<li><a href="">Contact Us</a></li>
</ul>
</div><!--footer_link_ul-->

</div><!--col-md-3-->


<div class="col-md-3">
<div class="footer_ul">
<ul>
<li><h3 class="widget-title">Our office</h3></li>

<li><div class="locati"><i class="fa fa-map-marker"></i></div><div class="locati_add">For more then 30 years, Real Estate agents are Property consisting  in the field of logistics and cargo forwarding.</div></li>


<li><div class="locati"><i class="fa fa-phone"></i></div><div class="locati_add">Telephone: +38640 222 455</div></li>
<li><div class="locati"><i class="fa fa-mobile"></i></div><div class="locati_add">Mobile phone: +38640 222 455</div></li>
<li><div class="locati"><i class="fa fa-file"></i></div><div class="locati_add">Fax: +38640 222 455</div></li>
</ul>

</div><!--footer_ul-->

</div><!--col-md-3-->

<div class="col-md-3">
<div class="footer_ul">
<ul>
<li><h3 class="widget-title">Newsletter subscribe</h3></li>

<li><div>We Work 7 Day a week, every day including major holiday. Contact us for any info.</div></li>
<li>&nbsp;</li>

</ul>

</div><!--footer_ul-->

</div><!--col-md-3-->
</div><!--row-->

</div><!--container-->

</div><!--footer_site-->
 
 
 
 
 
<div class="cpoy_right">Real Estate agents are Property consisting  | &copy; 2016 Real Estate  , All rights reserved</div><!--cpoy_right-->

 
{{HTML::script('packages/realestate/js/bootstrap.js')}}
{{HTML::script('packages/realestate/js/jquery.flexslider.js')}}
{{HTML::script('packages/realestate/js/demo.js')}}
 
</body>
 
 </html>