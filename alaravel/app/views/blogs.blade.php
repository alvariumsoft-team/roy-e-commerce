@extends('adashb')

@section('dash_content')

<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>Title</th>
<th>Author</th>
<th>Date</th>
<th>Status</th>
<th>Action</th>
</tr>
</thead>

<tbody>

@foreach($posts as $post)

<tr>
<td>{{Form::checkbox('listing_action','','',array('id'=>'listing_action'))}}</td>

<td>{{$post['post_title']}}</td>
<td>{{$post['post_author']}}</td>
<td>{{$post['created_at']}}</td>
<td>{{$post['post_status']}}</td>
<td class="clearfix">
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('blogactivate')}}/{{$post['post_name']}}" >Open</a>
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('blogdeactivate')}}/{{$post['post_name']}}">Close</a>
	<a class="pull-left btn btn-primary" style="margin-right:10px;" href="{{URL::to('blogdelete')}}/{{$post['post_name']}}">Delete</a>
</td>

</tr>

@endforeach

</tbody>

</table>


<span class="bpaginate" style="text-align:center !important;display:block;">{{ $posts->links() }}</span>

@stop
