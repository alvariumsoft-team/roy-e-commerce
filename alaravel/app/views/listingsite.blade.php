@extends('main')
@section('content')

<style type="text/css">

.list-header{
margin:0px;
padding:10px;
background:#dedede;
}

.list-head{padding:30px;}

.listing{
box-sizing:border-box;
margin:10px;
box-shadow:0px 0px 1px #1d1d1d;

}

.status{float:right;background:#0097d6;color:#fefefe;padding:10px;border-radius:10px;font-size:16px;}


.list-price{width:100%;background:#27ae60;padding:5px 15px;padding-top:10px;color:#fefefe;box-shadow:0px 0px 2px #27ae60;}

.list-info{border-bottom:1px solid #d5d9dd;padding:10px;}

.list-left{text-align:left;}
.list-right{text-align:right;}

.timg{margin:20px 0px;}

.list-features{width:100%;background:#29333d;color:#b9bfc6;font-family:"Lato";box-shadow:0px 0px 2px #29333d;margin:0px;padding:0px;padding-top:10px;padding-bottom:10px;}

.list-features > ul{margin:0px;padding:0px;}

.list-features > ul > li{float:left;display:inline;list-style:none;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;background:transparent;}

.list-fmenu{border-right:1px solid #3b4d5d;}

body{font-family:"Lato";}

</style>

<div class="list-header">

<div class="container"><div><h4>Listing</h4></div></div>

</div>

<div class="container">

<div class="row">

<div class="col-lg-12 col-sm-12 col-xs-12">

<div class="about">

<div class="listing">

<div class="list-head">

<div class="status col-lg-1 col-sm-2 col-xs-12">For&nbsp;{{ucwords($listing['status'])}}</div>

<div class="col-lg-11 col-sm-10 col-xs-12 name_sq"><h1 style="margin-top:0px !important;">{{ucwords($listing['name'])}}</h1></div>

<div class="pull-right">{{$listing['address']}}</div>

</div>
</br>
<div class="list-price col-lg-12 col-sm-12 col-xs-12 " style="padding:2px!important;padding-left:10px!important;"><h3>${{$listing['price']}}</h3></div>

<div class="list-head">

<div class="list-info">
<div class="row">

<div class="col-lg-6 col-sm-6 col-xs-6 list-left">Beds</div>
<div class="col-lg-6 col-sm-6 col-xs-6 list-right">{{$listing['beds']}}</div>

</div>
</div>

<div class="list-info">
<div class="row">

<div class="col-lg-6 col-sm-6 col-xs-6 list-left">Baths</div>
<div class="col-lg-6 col-sm-6 col-xs-6 list-right">{{$listing['baths']}}</div>

</div>
</div>

<div class="list-info">
<div class="row">

<div class="col-lg-6 col-sm-6 col-xs-6 list-left">Sq. Ft.</div>
<div class="col-lg-6 col-sm-6 col-xs-6 list-right">{{$listing['area']}}sq.ft.</div>

</div>
</div>


</div>

<div class="timg">{{HTML::image($listing['img'],'alt',array('height'=>'500px','width'=>'100%'))}}</div>

<div class="list-features">
<ul><li class="list-fmenu"><i class="fa fa-bars"></i></li><li><a href="#listing_features" style="color:#dedede!important;">Listing Features</a></li><li><a href="#location" style="color:#dedede!important;">Location</a></li><li><a href="#contact" style="color:#dedede!important;">Contact</a></li></ul>
<div class="clear"></div>
</div>

<div class="list-head" id="listing_features">
<div >
{{$listing['description']}}
</div>
</div>


<div class="list-head" id="location">
<h3 style="border-bottom:1px solid #dedede;padding:4px;">Location</h3>
<div class="list-info" >
{{$listing['address']}}
</div>
</div>


<div class="list-header">

<div class="row">

<div class="col-md-1"></div>
<div class="col-md-10">

<div class="about" id="contact">
<h3>Request More Information</h3>
<div class="form-group">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'Your E-mail'))}}</div>
<div class="form-group">{{Form::textarea('comment','',array('class'=>'form-control','placeholder'=>'Your Comment'))}}</div>
<div class="form-group">{{Form::submit('submit',array('class'=>'btn btn-primary btn-default btn-large btn-block'))}}</div>
</div></div>
<div class="col-md-1"></div>

</div>

</div>


</div>



</div>

</div>

<div class="col-lg-12 col-sm-12 col-xs-12 tw_time">
			<a class="twitter-timeline" href="https://twitter.com/CowboyGlobal">Tweets by CowboyGlobal</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>	
</div>

</div>

</div>

@stop
