@extends('adashb')
@section('dash_content')

<h2>Chage Password</h2>

<p>Want to change your password, you are on a right place.</p>

<form action="" method="post">

<div class="form-group">

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Password:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::password('password',array('class'=>'form-control','placeholder'=>'password','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Confirm Password:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'confirm password','required'=>'true'))}}</label>
</div>

</div>

{{Form::submit('Change Password',array('class'=>'btn bt-large btn-primary'))}}

</form>

@stop