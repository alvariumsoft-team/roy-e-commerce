@extends('main')
@section('content')

<div style="padding:50px;background:#000;color:#fff;font-size:24px;font-weight:500;">
<h1 class="center" style="font-family:Montserrat !important;">Change Password</h1></div>


<div class="container ">

<div style="border:1px solid #dedede;margin:80px;">

<div class="about">

@if(Session::has('message'))
<section style="font-size:18px;">

<p  style="text-align:center; color:red;position:relative !important;width:100% !important;background:#f9f9f9;padding:20px;">{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>

</section>
@endif

<div class="row"><div class="col-md-3"></div>

<div class="col-md-6">

<form class="form" action="" method="post">

<div class="form-group">

{{Form::password('password',array('class'=>'form-control','placeholder'=>'New Password','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::password('confirm_password',array('class'=>'form-control','placeholder'=>'Confirm Password','required'=>'true'))}}
<!--$2y$10$o3/TOsQeAzJWo8g3lOmRSet9j3/YWnaL8NIsZlnSEwNtWbUiFBF2.-->
</div>




<div class="form-group">

{{Form::submit('Change Password',array('class'=>'btn bt-large btn-primary btn-block','style'=>'background:#29333d;'))}}

</div>

</form>

</div>

<div class="col-md-3"></div></div>

</div>

</div>

</div>

@stop