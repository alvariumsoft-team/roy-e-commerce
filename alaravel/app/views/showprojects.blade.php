@extends('adashb')
@section('dash_content')

<h2>Projects</h2>

<p>View all projects.</p>

<h2></h2>

<form action="" method="post">


<!--<div class="row">

<div class="col-md-2">

<div class="form-group">{{Form::select('Bulk action',array('Bulk Actions','edit'=>'Edit','delete'=>'Move To Trash'),'',array('class'=>'form-control','id'=>'bulk_action'))}}</div>

</div>

<div class="col-md-1">

<div class="form-group">{{Form::button('apply',array('class'=>'btn','id'=>'bulk_action_button'))}}</div>


</div>

</div> -->

<div class="row">

<div class="col-md-9">

<table class="table">

<thead>
<tr>

<th>{{Form::checkbox('listing_action_all','','',array('id'=>'listing_action_all'))}}</th>

<th>Company Logo</th>
<th>Name</th>
<th>Description</th>
<th>Project Area</th>

</tr>
</thead>

<tbody>

@foreach($lists as $list)

<tr> 

<td>{{Form::checkbox('listing_action','','',array('class'=>'listing_action'))}}</td>
<td><a href="{{URL::to('microsite'.'/'.$list['id'])}}" target="_blank"><img src="{{$list['logo']}}" height="60px" width="80px" ></img></a></td>
<td>{{$list['name']}}</td>
<td>{{$list['description']}}</td>
<td>{{$list['project_area']}}</td>

</tr>

@endforeach

</tbody>

</table>

</div>

</div>


</form>





{{ $lists->links() }}






@stop
