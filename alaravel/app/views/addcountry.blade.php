@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add Country</h2>

<p>Add a country.</p>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Location:</b></label>
<label class="col-xs-12 col-md-6">

<select class="form-control" name="location">

	<option value="eu">Europe</option>
	<option value="world">Rest of the world</option>

</select>

</label>
</div>

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Country:</b></label>
<label class="col-xs-12 col-md-6">{{Form::text('country','',array('class'=>'form-control','placeholder'=>'Country','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>City:</b></label>
<label class="col-xs-8 col-md-5">{{Form::text('city[]','',array('class'=>'form-control','placeholder'=>'City'))}}</label>
<label class="col-xs-4 col-md-4"><i class="fa fa-plus add-city" style="margin-top:10px;cursor:pointer;"></i></label>
</div>

<div class="form-group">

{{Form::submit('ADD',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.add-city', function(){
			var html = '<div class="row"><label class="col-xs-12 col-md-3"><b></b></label><label class="col-xs-8 col-md-5"><input class="form-control" placeholder="City" type="text" name="city[]" value="" /></label><label class="col-xs-4 col-md-4"><i class="fa fa-plus add-city" style="cursor:pointer;margin-top:10px;"></i></label></div>';
			$(this).parents('.row').after(html);
			$(this).remove();
		});
	});
</script>

@stop
