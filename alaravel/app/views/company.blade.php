@extends('dash')

@section('dash_content')
<div>
<form action="" method="post" enctype="multipart/form-data">


<div class="form-group">{{Form::text('company_name','',array('class'=>'form-control','placeholder'=>'Company Name'))}}</div>
<div class="form-group">{{Form::file('company_logo')}}</div>

<div>
<div class="form-group">{{Form::text('phone','',array('class'=>'form-control','placeholder'=>'Phone Number'))}}</div>
<div class="form-group">{{Form::text('email','',array('class'=>'form-control','placeholder'=>'E-mail'))}}</div>
<div class="form-group">{{Form::text('website','',array('class'=>'form-control','placeholder'=>'Your Website'))}}</div>
<div class="form-group">{{Form::textarea('address','',array('class'=>'form-control','placeholder'=>'Your Address'))}}</div>
<div class="form-group">{{Form::text('country','',array('class'=>'form-control','placeholder'=>'Your Country'))}}</div>
<div class="form-group">{{Form::text('state','',array('class'=>'form-control','placeholder'=>'Your State'))}}</div>
<div class="form-group">{{Form::text('city','',array('class'=>'form-control','placeholder'=>'Your City'))}}</div>
<div class="form-group">{{Form::button('Add Social Media',array('class'=>'btn'))}}</div>
</div>

</div>


<div class="form-group">{{Form::submit('Update Profile',array('class'=>'btn bt-large btn-primary btn-block'))}}</div>
</div>
</form>

@stop