<!DOCTYPE>
<html>
	<head>
	<title></title>
		{{HTML::style('packages/ad/bootstrap/css/bootstrap.min.css')}}
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
		<!--{{HTML::script('packages/bootstrap/dist/js/jquery.js')}}-->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		
		{{HTML::style('packages/magicsuggest/magicsuggest-min.css')}}
		{{HTML::script('packages/magicsuggest/magicsuggest-min.js')}}
		
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc&libraries=places"></script>
		<script type="text/javascript"  src="http://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
		<script type="text/javascript"  src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobubble/src/infobubble.js"></script>	
		
		
		<style type="text/css">
			 html, body{font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Helvetica Neue",sans-serif;margin:0px;padding:0px;border:0px;box-sizing:border-box;}
			 .mapheader{border:1px solid #efefef;box-shadow:0px 3px 5px #ccc;}
			 .mapheaderul{margin:0px;padding:0px;border:0px;clear:both;}
			 .mapheaderli{padding:6px;float:left;list-style:none;}
			 .mapcontainer{position:fixed;width:100%;height:90%;right:0px;top:10%;clear:both;}
			 .listsection{width:75%;height:100%;background:#fff;float:left;}
			 .listaside{width:25%;height:100%;right:0px;background:#fffff0;border-left:1px solid #ccc;overflow-y:scroll;float:left;}
			 .asideswitch{background:#f9f9f9;position:absolute;top:0px;right:25%;z-index:100000;font-size:27px;padding:0px 12px;box-shadow: -4px 4px 5px #dedede;}
			 .asideswitch > a{padding:0px; margin:0px; text-decoration:none;outline:none;display:block;}
			 .bsideswitch{background:#f9f9f9;position:absolute;top:0%;left:10%;z-index:100000;padding:0px 6px;box-shadow: -4px 4px 5px #dedede;}
			 .bsideswitch > span > a{padding:0px; margin:0px; text-decoration:none;outline:none;font-size:20px!important;}
			 
			 .bsideswitch > ul{padding:0px;margin:0px;border:0px;clear:both;}
			 .bsideswitch > ul > li{float:left;padding:6px 4px;margin:0px;list-style:none;}
			 .bbtn{border:none !important;background:#fff;padding:6px 0px;font-weight:700;}
			 .bbtn:hover{border:none !important;background:#fff;padding:6px 0px;}
			 
			 .fullwidth{width:100%;}
			 .right0{right:0%!important;}
			 .scrabble{width:30%;}
			 .slide{width:0%!important;}
			 .list_a:hover{border:1px solid #ff0000;}
			 .list_a{border:1px solid #fff;padding:10px 0px;display:block;border-bottom:1px solid #dedede;}
			 .mfield_cus{padding-left:6px;font-size:12px;color:#424242;text-decoration: none !important;}
			 .fcontainer > label{display:block;}
			 
			.map_bar_ul{padding:0px;margin:0px;}
			.map_bar_ul::after{clear:both;}
			.map_bar_li{list-style:none;float:left; padding:5px 12px;}
			.gm-style-iw + div {display: none;}
			.mlabels{background:#000;padding:10px;margin:10px;}
			.mapdp{margin:0px;padding:0px;margin-bottom:3px;}
			
			.gm-style-iw {
			   width: 150px;
			   top: 20px !important; 
			   background-color: #fff;
			   box-shadow: 0 1px 6px rgba(0, 0, 0, 0.3);
			   border: 1px solid rgba(0, 0, 0, 0.3);
			   border-radius: 2px 2px 0 0;
			   padding:1px 10px;
			}
			.fl{float:left;}
			.fcontainer{padding:10px;}
			 
			 .pac-container{width:470px!important;}
			 
			 .tpad{padding-right:10px;}
			 .tpad > label{font-weight:200;cursor:pointer;}
			 .bdistance{display:none;padding:20px 5px 10px 5px;}
			 
			 .infobubble{padding:1px; padding-bottom:5px;}
			 
		</style>
		
		<script type="text/javascript">
			$location = [];
			$(document).ready(function(){
				var ms = $('#ms-scrabble').magicSuggest({
					placeholder: 'Enter location here...',
					data: "{{URL::to('locations')}}",
					valueField: 'idCountry',
					displayField: 'nameCountry'
				});
/*					
				$(ms).on('selectionchange', function(event, combo, selection){alert('wqe');
					alert('wqe');
					$location = selection;
					alert('sfd');
					alert(selection.lat());
					var lat = selection.lat();
					var lng = selection.lng();
					$('#asrch').prepend('<input type="hidden" name="lat" value="'+lat+'" />');
					$('#asrch').prepend('<input type="hidden" name="lng" value="'+lng+'" />');
					var buy_rent = $('input[name="buy_rent"]').val();
					var bed_type = $('input[name="bedtype"]').val();
					var posted_by = $('input[name="postedby"]').val();
					var beds = $('input[name="bedtype"]').val();
					$prop_type = [];
					$('input[name="prop_type[]"]').each(function(i, o){
						if($(o).prop('checked') == true){
							$prop_type.push($(o).val());
						}
					});
					$('.mapload').css('display', 'block');
					$('.errgif').html('Loading...');
					$.post("{{URL::to('traveltime')}}", {location:$location, buy_rent:buy_rent, prop_type:$prop_type, beds:beds, posted_by:posted_by}).done(function(data){
						if(data) {
							try {
								data = JSON.parse(data);
								map_setup(data);
								$('#properties').html(data.listings.list);
								setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
							} catch(e) {
								setTimeout(function() {$('.errgif').html('No results found.');}, 400);
								setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
							}
						}
					});
				});
			
				$('.mfield_cus').on('click', function(e){
					//e.preventDefault();
					var par = $(this).parent().find('input[type="checkbox"]');
					if(par.prop('checked') == true){
						par.prop('checked', false);
					}else{
						par.prop('checked', true);
						var buy_rent = $('input[name="buy_rent"]').val();
						var bed_type = $('input[name="bedtype"]').val();
						var posted_by = $('input[name="postedby"]').val();
						var beds = $('input[name="bedtype"]').val();
						$prop_type = [];
						$('input[name="prop_type[]"]').each(function(i, o){
							if($(o).prop('checked') == true){
								$prop_type.push($(o).val());
							}
						});
						$('.mapload').css('display', 'block');
						$('.errgif').html('Loading...');
						$.post("{{URL::to('traveltime')}}", {location:$location, buy_rent:buy_rent, prop_type:$prop_type, beds:beds, posted_by:posted_by}).done(function(data){
							if(data) {
								try {
									data = JSON.parse(data);
									map_setup(data);
									$('#properties').html(data.listings.list);
									setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
								} catch(e) {
									setTimeout(function() {$('.errgif').html('No results found.');}, 400);
									setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
								}
							}
						});
						
					}
				});
*/				

				$('.toggleField').on('click', function(){
					$(this).parents('.dropdown').find('input[type="hidden"]').val($(this).attr('data-val'));
					$(this).parents('.dropdown').find('button > .ftext').html($(this).html());
				});

				$('.asideswitch').on('click', function(){
					$('.listaside').toggleClass('slide');
					$('.asideswitch').toggleClass('right0');
					$('.listsection').toggleClass('fullwidth');
					google.maps.event.trigger(map, 'resize');
				});
				
				$('.ttimer > span').click(function(e) {
					e.stopPropagation();
				});
				
				$('.bedtype').click(function(e) {
					$('input[name="bedtype"]').val($(this).attr('data-val'));
				});
				
				$('.dropdown-cus a,input').click(function(e) {
					//e.stopPropagation();
				});
				
				$('.bttime').on('click', function(){
					$('.btime').css('display', 'block');
					$('.bdistance').css('display', 'none');
					$('input[name="tdistance"]').prop('checked', false);
				});
				
				$('.btdistance').on('click', function(){
					$('.bdistance').css('display', 'block');
					$('.btime').css('display', 'none');
					$('input[name="ttime"]').prop('checked', false);
					$('input[name="tmode"]').prop('checked', false);
				});
				
				$(document).on('click', '.ms-res-item', function(){
					var data = $(this).attr('data-json');
					data = JSON.parse(data);
					$da = $('.ms-sel-item ');
				});
				
				$('.tfield').on('click', function(){
					ttsearch();
				});
				
				$('.shownear').on('click', function(){
					if($(this).hasClass('heat')){
						setheat($(this).attr('data-val'));
					}else{
						setnear($(this).attr('data-val'));
					}
				});
				
			});
			
			$(document).ready(function(){
				var jsn = {{$jsn}};
				map_setup(jsn);
				initAutocomplete();

				
			});
			
		</script>
		
	</head>
	<body>
		<form method="post" id="asrch" onsubmit="event.preventDefault();ttsearch();">
			<header class="mapheader container-fluid">
				<div class="row">
					<div class="col-lg-1"><a href="{{URL::to('/')}}"><img style="margin:0px 20px;height: 60px;" src="{{URL::to('packages/realestate/site/logo.png')}}"></img></a></div>
					<div class="col-lg-11" style="height:50px;padding:5px;">
						<ul class="mapheaderul">
							<li class="mapheaderli scrabble">
								<input id="pac-input" class="controls form-control" type="text" placeholder="Place" required/>
							</li>
							<li class="mapheaderli">
								<div class="dropdown">
									<input type="hidden" name="buy_rent" value="buy" />
								  <button class="btn btn-default dropdown-toggle" type="button" id="buy_rent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Buy</span>
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" aria-labelledby="buy_rent">
									<li><a class="mfield tfield toggleField" href="javascript:void(0);" data-val="buy">Buy</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield toggleField" href="javascript:void(0);" data-val="rent">Rent</a></li>
								  </ul>
								</div>
							</li>
							
							<li class="mapheaderli">
								<div class="dropdown">
								  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Apartment</span>
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu dropdown-cus" aria-labelledby="prop_type" style="width:450px;max-width:700px;">
									  <li>
										<div class="row">
											<div class="col-md-4" style="margin-right:0px!important;padding-right:0px!important;">
												<div class="fcontainer">
													<h5 style="margin-top:0px;"><b>Residential</b></h5>
													
													<label class="tfield"><input type="checkbox" name="prop_type[]" value="apartment"/><a class="mfield_cus mfield" href="javascript:void(0);">Apartment</a></label>
													<label class="tfield"><input type="checkbox" name="prop_type[]" value="house"/><a class="mfield_cus mfield" href="javascript:void(0);">House</a></label>
													<label class="tfield"><input type="checkbox" name="prop_type[]" value="land"/><a class="mfield_cus mfield" href="javascript:void(0);">Land</a></label>
												</div>
											</div>
											<div class="col-md-4" style="margin-right:0px!important;padding-right:0px!important;">
												<div class="fcontainer">
													<h5 style="margin-top:0px;"><b>Commercial</b></h5>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="office"/><a class="mfield_cus mfield" href="javascript:void(0);">Office Space</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="shop"/><a class="mfield_cus mfield" href="javascript:void(0);">Shop</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="land_commercial"/><a class="mfield_cus mfield" href="javascript:void(0);">Commercial land</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="warehouse"/><a class="mfield_cus mfield" href="javascript:void(0);">Warehouse</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="indus_building"/><a class="mfield_cus mfield" href="javascript:void(0);">Industrial building</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="indus_shed"/><a class="mfield_cus mfield" href="javascript:void(0);">Industrial Shed</a></label>
												</div>
											</div>
											<div class="col-md-4" style="margin-right:0px!important;padding-right:0px!important;">
												<div class="fcontainer">
													<h5 style="margin-top:0px;"><b>Others</b></h5>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="land_agri"/><a class="mfield_cus mfield" href="javascript:void(0);">Agricultural land</a></label>
													<label class="tfield"><input type="checkbox"  name="prop_type[]" value="farm_house"/><a class="mfield_cus mfield" href="javascript:void(0);">Farm House</a></label>
												</div>
											</div>
										</div>
									  </li>
									  
								  </ul>
								</div>
							</li>					
							
							<li class="mapheaderli">
								<div class="dropdown">
									<input type="hidden" name="bedtype" value="1"/>
								  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Bedroom</span>
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" aria-labelledby="bedtype">
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="1">1 BHK</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="2">2 BHK</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="3">3 BHK</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="4">4 BHK</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="5">5 BHK</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield bedtype toggleField" href="javascript:void(0);" data-val="6">>5 BHK</a></li>
								  </ul>
								</div>
							</li>
							<li class="mapheaderli">
								<div class="dropdown">
									<input type="hidden" name="postedby" value="owner" />
								  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Posted by</span>
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" aria-labelledby="prop_type">
									<li><a class="mfield tfield toggleField" href="javascript:void(0);" data-val="owner">Owners</a></li>
									<li role="separator" class="divider"></li>
									<li><a class="mfield tfield toggleField" href="javascript:void(0);" data-val="broker">Brokers</a></li>
									<li role="separator " class="divider"></li>
									<li><a class="mfield tfield toggleField" href="javascript:void(0);" data-val="builder">Builders</a></li>
								  </ul>
								</div>
							</li>
							<!--
							<li class="mapheaderli">
								<div class="dropdown">
								  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Filters</span>
									<span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu pull-right dropdown-cus" aria-labelledby="prop_type"  style="width:450px;max-width:700px;">
									  <li>
										<div class="row">
											
										</div>
									  </li>
								  </ul>
								</div>
							</li>	
							-->
							
							<li class="mapheaderli">
								<div class="dropdown">
								  <button class="btn btn-default dropdown-toggle" type="button" id="travel_time" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="ftext">Travel Time Search</span>
									<span class="caret"></span>
								  </button>
								  <div class="dropdown-menu pull-right ttimer" aria-labelledby="travel_time" style="width:470px;padding:10px;">
									  <span>
										<a style="text-decoration:none;" class="btn btn-default bttime" href="javascript:void(0);">Travel Time</a>
										<a style="text-decoration:none;" class="btn btn-default btdistance" href="javascript:void(0);">Distance</a>
									  </span>
									  
									<div class="btime">
										<h5>Travel Time</h5>
										<div>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="10" checked/>&nbsp;10 min</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="20" />&nbsp;20 min</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="30" />&nbsp;30 min</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="40" />&nbsp;40 min</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="50" />&nbsp;50 min</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="ttime" value="60" />&nbsp;60 min</label></span>
										</div>
										<h5>Mode Of Transport</h5>
										<div>
											<span class="tpad"><label class="tfield"><input type="radio" name="tmode" value="driving" checked/>&nbsp;Driving</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="tmode" value="walking" />&nbsp;Walking</label></span>
											<span class="tpad"><label class="tfield"><input type="radio" name="tmode" value="public_transport" />&nbsp;Public Transport</label></span>
										</div>
									</div>
									<div class="bdistance">
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="1" />&nbsp;1 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="2" />&nbsp;2 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="3" />&nbsp;3 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="4" />&nbsp;4 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="5" />&nbsp;5 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="6" />&nbsp;6 Km</label></span>
										<span class="tpad"><label class="tfield"><input type="radio" name="tdistance" value="7" />&nbsp;7 Km</label></span>
									</div>
								  </div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</header>
		</form>
		<section class="mapcontainer">
			<section class="listsection">
				<div class="bsideswitch"  style="clear:both;">
				
				
				<ul>
					<!--<li><button class="btn bbtn">&laquo;</button></li>-->
					<li><button class="btn bbtn">SHOW:</button></li>
					<li><button class="btn btn-default shownear" data-val="commute" >Commute</button></li>
					<li><button class="btn btn-default shownear" data-val="markets" >Markets</button></li>
					<li><button class="btn btn-default shownear" data-val="schools" >Schools</button></li>
					<li>
					
						<div class="dropdown">
							<input type="hidden" name="postedby" value="owner" />
						  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="ftext">More</span>
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="prop_type">
							<li><a class="shownear" href="javascript:void(0);" data-val="airports">Airports</a></li>
							<li role="separator" class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="bank_atm">Bank/ATM's</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="parks">Parks</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="restaurants">Restaurants</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="gym">Gym</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="petrol_pump">Petrol Pumps</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="worship">Worship</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="pharmacy">Pharmacy</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear" href="javascript:void(0);" data-val="movies">Movies</a></li>
						  </ul>
						</div>
					
					</li>
					<li><button class="btn bbtn">Heat Map:</button></li>
					<li>
					
						<div class="dropdown">
							<input type="hidden" name="postedby" value="owner" />
						  <button class="btn btn-default dropdown-toggle" type="button" id="prop_type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="ftext">Please Select</span>
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="prop_type">
							<li><a class="shownear heat" href="javascript:void(0);" data-val="price_per_sqft">Price Per Sqft</a></li>
							<li role="separator" class="divider"></li>
							<li><a class="shownear heat" href="javascript:void(0);" data-val="demand">Demand</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear heat" href="javascript:void(0);" data-val="supply">Supply</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear heat" href="javascript:void(0);" data-val="rental_yield">Rental Yield</a></li>
							<li role="separator " class="divider"></li>
							<li><a class="shownear heat" href="javascript:void(0);" data-val="remove">Remove Heat Maps</a></li>
						  </ul>
						</div>
					
					</li>
				</ul>
				
				
				</div>
				<div class="re-map" style="overflow:hidden;height:100%;width:100%;">
					<div class="re-map" id="map" style="height:100%;width:100%"></div>
				</div>
				<span class="asideswitch"><a href="javascript:void(0);">&raquo;</a></span>
			</section>
			<aside class="listaside">
				<div class="panel with-nav-tabs panel-default">
					<div class="panel-heading">
						<ul class="nav nav-tabs">
						  <li role="presentation" class="active"><a href="#properties"  data-toggle="tab">Properties</a></li>
						  <li role="presentation"><a href="#projects"  data-toggle="tab">Projects</a></li>
						  <li role="presentation"><a href="#localities"  data-toggle="tab">Localities</a></li>
						</ul>
					</div>
				<div class="panel-body">
					 <div class="tab-content">
						<div class="tab-pane fade active in" id="properties">
						  <?php foreach($prop as $p){?>
							  <a target="_blank" href="{{URL::to('microsite/listing/'.$p->id)}}" class="list_a">
								  <div class="row">
								  <div class="col-md-6"><img style="height:80px;" class="img-responsive" src="{{URL::to($p->img)}}"></div>
								  <div class="col-md-6">
									  <div class="">
										  <div style="float:left;padding:0px;padding-right:5px;"><b><?php if($p->beds != 0){echo $p->beds.' BHK';} ?></b></div>
										  <div style="float:left;padding:0px;padding-right:5px;"><i>{{$p->area}} sqft.</i></div>
									  </div>
									  <div style="clear:both;"></div>
									  <div>{{$p->location}}</div>
								  </div>
								  </div>
							  </a>
						  <?php } ?>
						</div>
						<div class="tab-pane fade" id="projects">
						  <?php foreach($proj as $p){?>
							  <a target="_blank" href="{{URL::to('microsite/project/'.$p->id)}}" class="list_a">
								  <div class="row">
								  <div class="col-md-6"><img style="height:80px;" class="img-responsive" src="{{URL::to($p->header)}}"></div>
								  <div class="col-md-6">
									  <div class="">
										  <div style="float:left;padding:0px;padding-right:5px;"><b>{{$p->name;}}</b></div>
										  <div style="float:left;padding:0px;padding-right:5px;"><i>{{$p->project_area}} sqft.</i></div>
									  </div>
									  <div style="clear:both;"></div>
									  <div>{{$p->location}}</div>
								  </div>
								  </div>
							  </a>
						  <?php } ?>
						</div>
						<div class="tab-pane fade" id="localities">
							<?php foreach($localities as $locality){ ?>
								
								<?php 
									$locp	= preg_replace('/\P{Xan}+/u', '-', $locality);
								?>
								
							  <div class="list_a" style="padding:5px;">
								  <div class="row">
								  <div class="col-md-6">
									  <h4>{{$locality}}</h4>
									  <h6>1321321 sq.ft.</h6>
								  </div>
								  <div class="col-md-6"><div></div> </div>
								  
									  <div class="col-md-12 clearfix">
										  <div class="col-md-6" style="float:left;padding:5px 0px;padding-right:17px;">
											<a target="_blank" href="{{URL::to('locality-properties')}}/{{$locp}}">See all properties</a>
										  </div>
										  <div class="col-md-6" style="float:left;padding:5px 0px;padding-left:17px;">
											<a target="_blank" href="{{URL::to('locality-projects')}}/{{$locp}}">See all projects</a>
										  </div>
										  <div style="clear:both;"></div>
									  </div>
								  
								  </div>
							  </div>
							<?php } ?>
						</div>
					</div>
				</div>
				</div>
			</aside>
		</section>
		
<script type="text/javascript">
	var map = undefined;
	var circle = undefined;
	$mmap = undefined;
	$marknear = [];
	
	function setheat(params){
		
	}
	
	function setnear(near){
		
		var type = [];
		
		if(near == 'commute'){type.push('taxi_stand');type.push('bus_station');
			type.push('subway_station');}
		if(near == 'schools'){type.push('school');}
		if(near == 'airports'){type.push('airport');}
		if(near == 'bank_atm'){type.push('bank');type.push('atm');}
		if(near == 'parks'){type.push('park');}
		if(near == 'restaurants'){type.push('restaurant');}
		if(near == 'gym'){type.push('gym');}
		if(near == 'petrol_pump'){type.push('gas_station');}
		if(near == 'worship'){type.push('hindu_temple');type.push('mosque');type.push('church');}
		if(near == 'pharmacy'){type.push('pharmacy');}
		if(near == 'movies'){type.push('movie_theater');type.push('movie_rental');}
		
		for( i in $marknear) {
		  $marknear[i].setMap(null);
		}
		
		var lat = parseFloat($('input[name="lat"]').val());
		var lng = parseFloat($('input[name="lng"]').val());
        var pyrmont = {lat: lat, lng: lng};
        inwindow =  new InfoBubble({
          maxWidth: 300,
          minHeight: 50,
        });
        var service = new google.maps.places.PlacesService($mmap);
        service.nearbySearch({
          location: pyrmont,
          radius: 500,
          type: type
        }, callback);
	}
	
	function callback(results, status) {
		if (status === google.maps.places.PlacesServiceStatus.OK) {
		  for (var i = 0; i < results.length; i++) {
			createNearMarker(results[i]);
		  }
		}
	}
	
	function createNearMarker(place) {
		var placeLoc = place.geometry.location;
		var marker = new google.maps.Marker({
		  map: $mmap,
		  position: place.geometry.location,
		});
		$marknear.push(marker);
		google.maps.event.addListener(marker, 'click', function() {
		  inwindow.setContent('<div class="infobubble">'+place.name+'</div>');
		  inwindow.open(map, this);
		});
	}
	$latlng = [];
	function map_setup(jsn){
		if(circle != undefined)circle.setMap(null);
		map = new google.maps.Map(document.getElementById('map'), { center: jsn.center, zoom: 7 });
		$mmap = map;	
		  var markers = [];
		  var marker = '';
		  var infowindow = '';
			for(i in jsn.list){
				marker = new google.maps.Marker({ position: jsn.list[i].loc, label: '', title: '', icon: {path: google.maps.SymbolPath.CIRCLE,scale: 2}});
				infowindow = new google.maps.InfoWindow({content: jsn.list[i].content});
				//marker.setVisible(false);
				infowindow.open(map,marker);
				google.maps.event.addListener(infowindow, 'domready', function() {
					var iwOuter = $('.gm-style-iw');
					var iwBackground = iwOuter.prev();
					iwBackground.children(':nth-child(2)').css({'display' : 'none'});
					iwBackground.children(':nth-child(4)').css({'display' : 'none'});
					iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 1px 6px'});
					iwBackground.children(':nth-child(3)').find('div').children().css({'z-index': '1'});
				});
				markers.push(marker);
			}
			var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'http://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
	}
	
	function ttsearch(){
		$(document).ready(function(){
			$frm = $('#asrch').serialize();
			if(($('#pac-input').val() == undefined) || ( $('#pac-input').val() == '')){
				alert('Please enter a location.');
			}else{
				$('.mapload').css('display', 'block');
				$('.errgif').html('Loading...');
				$.post("{{URL::to('traveltime')}}", $frm).done(function(data){
					if(data) {
						try {
							data = JSON.parse(data);
							map_setup(data);
							$('#properties').html(data.listings.list);
							$('#projects').html(data.listings.proj);
							$('#localities').html(data.listings.local);
							setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
						} catch(e) {
							setTimeout(function() {$('.errgif').html('No results found.');}, 400);
							setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
						}
					}
				});
			}
		});
	}

      function initAutocomplete() {
		
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        //$mmap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        $mmap.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
			if(circle != undefined)circle.setMap(null);
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

			var mrk = new google.maps.Marker({
						  map: $mmap,
						  //icon: icon,
						  title: place.name,
						  position: place.geometry.location
						});

			//set_timetravel(place.geometry.location.lat(), place.geometry.location.lng());
			
				$('input[name="lat"]').remove();
				$('input[name="lng"]').remove();
			
				$('#asrch').append('<input type="hidden" name="lat" value="'+place.geometry.location.lat()+'" />');
				$('#asrch').append('<input type="hidden" name="lng" value="'+place.geometry.location.lng()+'" />');
				ttsearch();

			//console.log(place.geometry.location);

			// Add circle overlay and bind to marker
			 circle = new google.maps.Circle({
			  map: map,
			  radius: 16093,    // 10 miles in metres
			  fillColor: '#AA0000',
			  strokeOpacity:0.02
			});
			circle.bindTo('center', mrk, 'position');

            // Create a marker for each place.
            markers.push(mrk);

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          
			google.maps.event.addListener(map, 'zoom_changed', function() {
				zoomChangeBoundsListener = 
					google.maps.event.addListener(map, 'bounds_changed', function(event) {
						if (this.getZoom() > 9 && this.initialZoom == true) {
							// Change max/min zoom here
							this.setZoom(9);
							this.initialZoom = false;
						}
					google.maps.event.removeListener(zoomChangeBoundsListener);
				});
			});
			map.initialZoom = true;
          
          $mmap.fitBounds(bounds);
        });
      }

	function set_timetravel(lat, lng){
		$(document).ready(function(){
			var dist = 50;
			$.post("{{URL::to('traveltime')}}", {lat:lat, lng:lng, dist:dist}).done(function(data){
				if(data) {
					try {
						data = JSON.parse(data);
						//map_setup(data);
						//$('#properties').html(data.listings.list);
						setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
					} catch(e) {
						setTimeout(function() {$('.errgif').html('No results found.');}, 400);
						setTimeout(function() {$('.mapload').css('display', 'none');}, 1500);
					}
				}
			});
		});
	}


</script>



<div class="mapload" style="  display:none;position: absolute;width: 250px;z-index: 15;top: 30%;padding:0px 20px;left: 40%;background: #f9f9f9;">
	<h4><img height="40px" width="40px"src="{{URL::to('packages/images/loading.gif')}}"><span class="errgif" style="padding-left:20px;color:red;font-size:14px;">Loading Results...</span></h4>
</div>



	</body>
</html>


<!--

<select>
  <optgroup label="Swedish Cars">
    <option value="volvo">Volvo</option>
    <option value="saab">Saab</option>
  </optgroup>
  <optgroup label="German Cars">
    <option value="mercedes">Mercedes</option>
    <option value="audi">Audi</option>
  </optgroup>
</select>
https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&mode=driving&origins=28.58000042523,77.16382820000001&destinations=28.7023,77.102|28.7041,77.1025&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc
-->
