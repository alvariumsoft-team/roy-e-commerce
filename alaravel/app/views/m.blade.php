<!DOCTYPE HTML>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Global Property Cowboys &#8211; We are a company based in Heiloo, a small town near Amsterdam, The Netherlands , Western Europe.</title>

	
	
{{HTML::style('packages/realestate/css/site.css')}}
{{HTML::style('packages/realestate/css/media.css')}}
{{HTML::style('packages/realestate/css/bootstrap.css')}}
{{HTML::style('packages/realestate/css/bootstrap-theme.css')}}
{{HTML::style('packages/realestate/css/animate.min.css')}}

{{HTML::script('packages/realestate/js/jquery.js')}}
	

<style type="text/css" rel="stylesheet">


.header{background:#29333d;}

.header-top{background:#29333d;border-top:1px solid #3b4d5d;}

.social-media{background:#29333d;}

ul.social-media  > li{
list-style:none;
display:inline-block;
float:left;
border-left:1px solid #3b4d5d;
line-height:1.75em;
padding:4px;
font-size:12px;
color:#d9d9d9;

}

ul.social_media li a{
font-weight:normal;
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
color:#d9d9d9;
text-decoration:none;
}

ul.social-media li i{
color:#fff;
font-size:14px;
margin:0px 10px;
}


ul.social-media-phone  > li{
list-style:none;
display:inline-block;
float:left;
line-height:1.75em;
padding:4px;
font-size:12px;
color:#d9d9d9;

}

ul.social-media-phone li i{
color:#fff;
font-size:14px;
margin:0px 10px;
}

.nav-list{

float:left;
padding: 0px 20px;
margin:15px 2px;
color:#fff;

}

.nav-list > a{color:#fff;text-decoration:none;}

.nav-container{margin-left:10%;margin-right:10%;padding-bottom:10px;padding-top:20px;}

.clm{padding: 20px 0px;}

.property-listing{}

.property-block{width:33.3%;height:300px;float:left; }

.heading{text-align:center;margin:20px;padding:20px;}

.footer{background:#29333d;color:#fff;border-top:1px solid #3b4d5d;}

.footer-top{background:#29333d;padding-top:20px;}

.footer-header-child{float:left;width:25%;}

.footer-nav-list{

float:left;
padding: 0px 5px;
color:#999;background:transparent;

}

.footer-nav-list > a{color:#fff;text-decoration:none;}


.wlogo{border-radius:10px 10px 10px 10px;}

.footer-child-logo-desc{padding:10px;}

.contact{padding-top: 20px;font-size:12px;margin:0px 10px; color:#999;background:transparent;}

.contactc{padding: 10px 50px;font-size:12px;margin:0px 10px; color:#999;background:transparent;}

.left{float:left;}

.right{float:right;}

.follow{padding:20px 20px 20px 0px; font-size:14px;}

.facebook{background:#3b5998;}

img{display:block;}

.property-image{width:100%;height:100%;}

.property-block{padding:20px;}

.about{text-size:30px;padding:20px;margin:20px;line-height:2.5em;}

.center{text-align:center;}

.pad{padding:20px;margin:20px;}

.form{padding:50px;margin:20px;box-shadow:0px 0px 1px #1a1a1a;}

</style>
	
</head>


<body>



<header class="header-top">

<div class="container">

<div class="row">

<div class="col-md-6">
<ul class="social-media-phone"><li><i class="fa fa-mobile-phone"> </i>Call Us Today: {{$Index['phone']}} </li></ul>
</div>

<div class="col-md-6">


<ul class="social-media ">


@foreach($Index['social-media'] as $menu)

<li><a href="{{$menu['url']}}"><i class="{{$menu['class']}}"></i></a></li>

@endforeach

<li><a href="login"><i class="fa fa-sign-in"></i>Login</a></li>
<li><a href="register"><i class="fa fa-sign-in"></i>Register</a></li>

</ul>

</div>

</div>

</div>

</header>





<div class="header-top">

<div class="left nav-container">

<a href="/"><img src="http://www.inviolateinfotech.com/roy/wp-content/uploads/2016/08/Untitled-1-1.png" /></a>

</div>


<header class="header right">

<div class=" nav-container">

<nav role="navigation" class="navbar navbar-default">

@foreach($Index['top-menu'] as $menu)

<div class="nav-list"><a href="{{$menu['url']}}"> {{$menu['name']}} </a></div>


@endforeach



</nav>

</div>

</header>


<div class="clearfix"></div>

</div>







@if(Session::has('message'))
<div class="container">

<div class="container">

<p class="well">{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>

</div>

</div>
@endif

@yield('content')




<footer class="footer ">

<div class="container-fluid">

<header class="footer-header contactc">

<div class="footer-header-child">

<div class="footer-child-logo"><img class="wlogo" src="/packages/realestate/img/logo.jpg" /></div>

<div class="footer-child-logo-desc">We are a company based in Heiloo, a small town near Amsterdam, The Netherlands , Western Europe.</div>

</div>
<div class="footer-header-child"> 
Contact Info: 
<br />
<div><div class="contact left"><i class="fa fa-home"></i></div><div class="contact left">Address:{{$Index['contact-us']['address']}}</div></div><div class="clearfix"></div>
<div><div class="contact left"><i class="fa fa-phone"></i></div><div class="contact left">Phone:{{$Index['contact-us']['phone']}}</div></div><div class="clearfix"></div>
<div><div class="contact left"><i class="fa fa-envelope-o"></i></div><div class="contact left">E-mail:{{$Index['contact-us']['email']}}</div></div><div class="clearfix"></div>

</div>

<div class="footer-header-child"> 
Follow Us:
<div>
<div class="left follow "><i class="fa fa-facebook"></i></div>
<div class="left follow"><i class="fa fa-twitter"></i></div>
<div class="left follow"><i class="fa fa-instagram"></i></div>
<div class="left follow"><i class="fa fa-linkedin"></i></div>
<div class="left follow"><i class="fa fa-google-plus"></i></div>
</div>
<div class="clearfix"></div>

</div>

<div class="footer-header-child"> 
Something: 

</div>

</header>

<div class="clearfix"></div>

</div>

<footer class="container footer">

<header class="footer-top left contact">

<nav role="navigation" class="navbar navbar-default  ">

@foreach($Index['top-menu'] as $menu)

<div class="footer-nav-list"><a href="{{$menu['url']}}"> {{$menu['name']}} </a></div>

@endforeach


</nav>

</header>

<footer class=" footer-top right contact">
&copy; 2016 {{$Index['copyright']}}, All Rights Reserved. <a href="#">Back to top</a> 
</footer>

</footer>

<div class="clearfix"></div>

{{HTML::script('packages/realestate/js/bootstrap.js')}}
{{HTML::script('packages/realestate/js/jquery.flexslider.js')}}
{{HTML::script('packages/realestate/js/demo.js')}}

</body>
</html>