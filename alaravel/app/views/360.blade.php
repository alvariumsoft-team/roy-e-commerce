@extends('adashb')

@section('dash_content')

<h2>Add 360&deg; Images</h2>

<form class="form360" id="f360" action="{{URL::to('add360')}}" method="post" enctype="multipart/form-data">

<div class="form-group row">

<label class="col-lg-4 col-sm-4 col-xs-6">
	<div class="form-group row">
	<label class="col-lg-12 col-sm-12 col-xs-12">
		<select name="project" class="form-control sproject">
			<option value="">Project</option>
			@foreach($projects as $k=>$v)
				<option value="{{$k}}">{{ucwords($v)}}</option>
			@endforeach
		</select>
	</label>
	</div>
</label>

<label class="col-lg-4 col-sm-4 col-xs-6">
	<div class="form-group row">
	<label class="col-lg-12 col-sm-12 col-xs-12">
		<select name="type" class="form-control stype" >
			<option value="">Type</option>
				  <option value="front">Front Yard</option>
				  <option value="bedroom">Bedroom</option>
				  <option value="storage">Storage</option>
				  <option value="backyard">Backyard</option>
				  <option value="hall">Hall</option>
				  <option value="living">Living Room</option>
				  <option value="kitchen">Kitchen</option>
				  <option value="overflow">Overflow</option>
				  <option value="bathroom">Bathroom</option>
				</nav>
		</select>
	</label>
	</div>
</label>

<label class="col-lg-4 col-sm-4 col-xs-6">
	<div class="form-group row">
	<label class="col-lg-12 col-sm-12 col-xs-12">
		<select name="unit" class="form-control sunit" style="display:none;">
			<option value="">Unit</option>
		</select>
	</label>
	</div>
</label>

<label class="col-lg-12 col-sm-12 col-xs-12">
	<div style="position:relative;height:190px;width:100%;border:2px dotted #ccc;background:#fff;" class="asd">
	<span style="margin-top:70px;margin-left:40%;" class="btn btn-default ifile-label">Select Image</span>
	{{Form::file('file',array('class'=>'inputfile inputfile-1 ifile-id file360','id'=>'select-files','data-multiple-caption'=>'{count} files selected'))}}
	</div>
	</label>
</div>

</form>

<div class="img-container row">
	
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('.sproject').on('change', function(){
			var url = "{{URL::to('projunit')}}";
			$.post(url, {data:$(this).val()}).done(function(data){
				data = JSON.parse(data);
				$('.sunit').css('display', 'block');
				$('.sunit').append(data.data);
			});
		});
		
		$('.file360').on('change', function(){
			$('.form360').submit();
		});
		
		$('.form360').on('submit', function(e){
			e.preventDefault();
				var data = new FormData(this);
				$.ajax({
					url : $('.form360').attr('action'),
					type: "POST",
					data : data,
					processData: false,
					contentType: false,
					success:function(data, textStatus, jqXHR){
						data = JSON.parse(data);
						if(data.status == '1'){
							$('.ifile-label').html('Select Image');
							$('.img-container').append('<div class="col-md-3"><img src="'+data.img+'" class="img-responsive" style="padding:10px;height:190px;" /></div>');
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						//if fails     
					}
				});
		});
		
	});
</script>

@stop
