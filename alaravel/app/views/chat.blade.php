@extends('adashb')

@section('dash_content')

<style type="text/css">
	
	.main-chat{width:100%;}
	.body-chat{width:100%;background:#fff;min-height:400px;border:1px solid #f9f9f9;box-shadow: 0 1px 3px rgba(0,0,0,.13);overflow-y: scroll;}
	.header-chat{background:#333;color:#ccc;padding:2px 10px;box-shadow: 0 1px 3px rgba(0,0,0,.13);}
	.footer-chat{width:100%;height:80px;border:1px solid #f9f9f9;box-shadow: 0 1px 3px rgba(0,0,0,.13);border-top:1px solid #72afd2;}
	.textarea-chat{height:100% !important;border-radius:5px;border:0px !important;resize:none;}
	.people-chat{background:#fff;height:500px;overflow-y:scroll;}
	.user-box{padding:10px;font-size:12px;}
	.user-info{padding:3px 5px;margin:0px;font-size:14px;}
	.circle-img{background:#f9f9f9;border-radius:40px;font-size:20px;}
	
</style>



<div class="main-chat">

<div class="row" >
	
	<div class="col-md-9">

		<header class="header-chat">
			<h2>Conversations</h2>
		</header>

		<section class="body-chat">

		</section>

		<footer class="footer-chat">
			<textarea class="form-control textarea-chat"></textarea>
		</footer>

	</div>
	
	<div class="col-md-3">
		<aside class="people-chat">
				@foreach($users as $user)
				@if($user->id == Auth::user()->id)
				<a href="#" style="display:none;" id="this-user">
				@else
				<a href="#" style="display:block;">
				@endif
				<div class="user-box">
						<div class="row" style="margin:0px !important;">
						<div class="col-md-3">
							<div class="">
								<img class="img-circle" alt="User Image" width="50px"; src="{{URL::to($user->img)}}"></img>
							</div>
						</div>
						<div class="col-md-9">
							<p class="user-info">{{$user->username}}</p>
							@if($user->online == 1)
							<span><i class="fa fa-circle" style="color:#0e8734;margin:0px 5px;"></i>Online</span> 
							@else
							<span><i class="fa fa-circle" style="color:#bbbfbc;margin:0px 5px;"></i>Offline</span> 
							@endif
						</div>
					</div>
				</div>
				</a>
				@endforeach
		</aside>
	</div>
	
</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea-chat').on('keyup',function(e){ 
			if(e.which == 13){
			var val = $(this).val(); 
			
			var img = $('#this-user').find('img').attr('src');
			
			var html = '<div class="row" style="padding:10px 20px;"><div class="col-md-2" ><img class="img-circle" width="50px" src="'+img+'" /></div><div class="col-md-10" style="padding:10px 0px;">'+val+'</div></div>';
			$('.body-chat').append(html);
			var wtf    = $('.body-chat');
			var height = wtf[0].scrollHeight;
			wtf.scrollTop(height);
			$(this).val('');
				send_ajax_message($('#url').val(), val, $('#to').val(), $('#from').val());
			}
		});
	});
</script>

@stop
