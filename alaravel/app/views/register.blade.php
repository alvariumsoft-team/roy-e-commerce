@extends('main')
@section('content')

<div style="padding:50px;background:#000;color:#fff;font-size:24px;font-weight:500;">
<h1 class="center" style="font-family:Montserrat !important;">REGISTER</h1></div>


<div class="container ">
 
<div class="Reg_div_form" style="border:1px solid #dedede;">

<div class="about">

@if(Session::has('message'))
<section style="font-size:18px;">

<p  style="text-align:center; color:red;position:relative !important;width:100% !important;background:#f9f9f9;padding:20px;">{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>

</section>
@endif

<form class="form" action="" method="post">

<div class="form-group">

{{Form::text('username','',array('class'=>'form-control','placeholder'=>'User name','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::text('firstname','',array('class'=>'form-control','placeholder'=>'First name','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::text('lastname','',array('class'=>'form-control','placeholder'=>'Last name','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::password('password',array('class'=>'form-control','placeholder'=>'password','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'confirm password','required'=>'true'))}}

</div>

<div class="form-group">

{{Form::text('email','',array('class'=>'form-control','placeholder'=>'E-Mail','required'=>'true'))}}

</div>


<div class="form-group">

Owner:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control'))}}

Seller:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control'))}}

Renter:&nbsp;{{Form::radio('user_type','',array('class'=>'form-control'))}}

</div>

<div class="form-group">

{{Form::submit('Register',array('class'=>'btn bt-large btn-primary btn-block','style'=>'background:#29333d;'))}}

</div>

</form>

</div>

</div>

</div>

</div>

@stop