@extends('adashb')
@section('dash_content')
<h3>Add New Post</h3>

{{HTML::script('packages/tinymce/js/tinymce.min.js')}}

  <script>
  tinymce.init({
    selector: '#mytextarea',
    height : "350",
    language: "{{App::getLocale()}}",
   // plugins: "paste",
   // paste_as_text: true,

  });
  </script>

<style type="text/css">
	.categories{width:100%;background:#fff;padding:16px;position:relative;}
</style>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="row">

	<div class="col-md-8">

		<div class="form-group row">
		<label class="col-xs-12 col-md-12">{{Form::text('title','',array('class'=>'form-control','placeholder'=>'Enter Title here','required'=>'true'))}}</label>
		</div>
		<div class="form-group">
			 <textarea id="mytextarea" name="post" ></textarea>
		</div>
		<div class="form-group">
		{{Form::submit('ADD POST',array('class'=>'btn bt-large btn-primary'))}}
		</div>

	</div>

	<div class="col-md-4">
		<div class="categories">
			<p style="font-size:20px;"><b>Categories</b></p>
			@foreach($categories as $category)
			<p>
				<label style="cursor:pointer;">
					<span style="padding:0px 4px;"><input type="checkbox" name="cat[{{$category->category_slug}}]" /></span>
					<span style="padding:0px 4px;">{{$category->category_name}}</span>
				</label>
			</p>
			@endforeach
		</div>
		
		<div class="categories" style="margin-top:20px;">
			<p style="font-size:20px;"><b>Featured Image</b></p>
			
			<label for="ifile-id2" style="margin-top:5px;margin-bottom:20px;">
				<div style="cursor:pointer;color: rgba(75,192,192,1);font-size:14px;" class="asd">
					<span id='ifile-labe'  class='ifile-label'>Set featured image</span>
					{{Form::file('featured_image',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'ifile-id2','data-multiple-caption'=>'{count} files selected'))}}
				</div>
			</label>
			
			<div class="alert alert-warning" role="alert" style="font-size:12px;padding:4px!important;">
			  Image height should be <strong>200px</strong>.
			</div>
			
			
		</div>
		
	</div>

</div>

</form>



@stop
