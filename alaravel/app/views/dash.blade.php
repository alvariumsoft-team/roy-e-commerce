<!DOCTYPE html>
<html>

<head>
    <title>Global Property Cowboys</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
	<link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css' rel='stylesheet' type='text/css'>
    <!-- Javascript Libs -->
	{{HTML::script('packages/bootstrap/dist/js/jquery.js')}}
	{{HTML::script('packages/bootstrap/dist/js/bootstrap.min.js')}}

<style type="text/css">

.left{float:left;} .right{float:right;} .clear{clear:both;} .hidden{display:none;}


.l-dashboard{
	box-sizing:border-box !important;
	margin:0px;
	padding:0px;
	border:0px;
	background:#fff;
	
	}
.l-header{
	position:fixed;
	padding:10px;
	background:#29333d;
	color:#fff;
	z-index:10000;
	width:100%;
	height:45px;
}

.l-main{position:relative;top:45px;}

.l-side-nav{
	position:fixed;
	top:45px;
	height:100%;
	width:16%;
	background:#222;
	overflow-y:scroll;
}

.l-panel{
	position:relative;
	left:16%;
	width:84%;
	padding:10px;
}

.l-side-nav > ul > li {width:100%;}

.l-side-nav > ul > li > a{padding:10px;display:block;}

.panel-body > ul > li {width:100%;}

.panel-body  > ul > li > a{padding:10px;display:block;}

.panel-body> ul > li > a:hover{background:transparent;}
.panel-body> ul > li > a:active{background:transparent;}
.panel-body> ul > li > a:focus{background:transparent;}

.title{
	
	padding-left:20px;
	
}

.nav-right-dashboard > li{display:inline;padding:6px 16px;}

a{outline:none !important;}

.dropdown-menu{color:#23527c !important;width:200px;}

.dropdown-menu > ul > a > li {border:0px; border-bottom:1px solid #dedede;}

.profile{width:220px;}

.profile-info{text-align:center;}

.profile-info-button{padding:10px;}

.notification-title .title{padding:10px 16px;}


</style>
	
</head>

<body class="l-dashboard">

<header class="l-header">
<div class="container-fluid">
<nav class="left">

<a href="{{URL::to('/')}}" style="color:#fff;">G P C</a>

</nav>


<nav class="right">


<ul class="nav-right-dashboard">
    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs"> <i class="fa fa-times icon"></i></button>

    <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                            <ul class="dropdown-menu danger  animated fadeInDown pull-right notification-title">
                                <li class="title">
                                   <i class="fa fa-exclamation-circle icon"></i>  Notification <span class="badge pull-right">4</span>
                                </li>
                                <li>
                                    <ul class="list-group notifications">
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> messages
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item message">
                                                view all
                                            </li>
                                        </a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
    <li class="dropdown profile">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown pull-right ">
                                <li class="profile-img">
                                    <img src="{{Auth::user()->img}}" class="profile-img">
                                </li>
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username">{{ucwords(Auth::user()->username)}}</h4>
                                        <p>{{Auth::user()->email}}</p>
                                        <div class="btn-group margin-bottom-2x profile-info-button" role="group">
                                            <a href="{{URL::to('/')}}" class="btn btn-default"><i class="fa fa-user"></i> Home</a>
                                            <a href="logout" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
    </li>
                    
					</ul>
            
 
</nav>

<div class="clear"></div>

</div>

</header>

<div class="l-main">



<aside class="l-side-nav">
<ul class="nav navbar-nav">
                            <li class="active">
                                <a href="{{URL::to('dashboard')}}">
                                    <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                                </a>
                            </li>
							
							@if(Auth::user()->type == 1)
							
                            <!-- Dropdown-->
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#component-example">
                                    <span class="icon fa fa-user"></span><span class="title">User</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="component-example" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="user">Add User</a>
                                            </li>
                                            <li><a href="users">View Users</a>
                                            </li>
                                            </li>
                                            <li><a href="#">Additional Features</a>
                                        </ul>
                                    </div>
                                </div>
                            </li>
							
							@endif
							
                            <!-- Dropdown-->
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#profile">
                                    <span class="icon fa fa-th-large"></span><span class="title">Profile</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="profile" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="uprofile">Update Info</a>
											<li><a href="chpassword">Change Password</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class=" dropdown">
                                <a data-toggle="collapse" href="#dropdown-element">
                                    <span class="icon fa fa-list"></span><span class="title">Listing</span>
                                </a>

                                <!-- Dropdown level 1 -->
                                <div id="dropdown-element" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="addlisting">Add Listing</a>
                                            </li>
                                            <li><a href="showlisting">View Listing</a>
                                            </li>
                                            <li><a href="#">Additional Features</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#dropdown-table">
                                    <span class="icon fa fa-table"></span><span class="title">Brokerage</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-table" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="#">Add Brokerage</a>
                                            </li>
                                            <li><a href="#">View Brokerage</a>
                                            </li>
                                            </li>
                                            <li><a href="#">Additional Features</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#dropdown-form">
                                    <span class="icon fa fa-money"></span><span class="title">Membership Plans</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-form" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="#">Add Plan</a>
                                            </li>
                                            <li><a href="#">View Plans</a>
                                            </li>
                                            </li>
                                            <li><a href="#">Additional Features</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
							


                            <!-- Dropdown-->
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#dropdown-example">
                                    <span class="icon fa fa-gears"></span><span class="title">Settings</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-example" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <!-- Dropdown-->
                            <li class="dropdown">
                                <a data-toggle="collapse" href="#dropdown-icon">
                                    <span class="icon fa fa-archive"></span><span class="title">Trash</span>
                                </a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-icon" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                        </ul>
                                    </div>
                                </div>
                            </li>
</aside>

<article class="l-panel">

@if(Session::has('message'))
<div class="alert alert-info">
<p>{{Session::get('message')}}</p>
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif


<div style="padding:10px 20px;">
@yield('dash_content')
</div>

</article>

<div class="clear"></div>

</div>


</body>

</html>















