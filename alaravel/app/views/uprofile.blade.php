@extends('adashb')

@section('dash_content')




<div class="side-body padding-top">

<h2>Update User Info</h2>

<p>Update your info.</p>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Username:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('username',$user['username'],array('class'=>'form-control','placeholder'=>'User name','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Firstname:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('firstname',$user['firstname'],array('class'=>'form-control','placeholder'=>'First name','required'=>'true'))}}</label>
</div>


<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Lastname:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('lastname',$user['lastname'],array('class'=>'form-control','placeholder'=>'Last name','required'=>'true'))}}</label>
</div>

<div class="form-group row">	
<label class="col-lg-3 col-sm-3 col-xs-12" ><b>Profile Image:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12"  for="ifile-id" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;"><span id='ifile-label' >Upload Image</span>{{Form::file('file',array('class'=>'inputfile inputfile-1','required'=>'true','id'=>'ifile-id','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<input type='hidden' name='_token' value='{{csrf_token()}}' />

<div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Phone:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('phone',$user['phone'],array('class'=>'form-control','placeholder'=>'Phone Number'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Email:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('email',$user['email'],array('class'=>'form-control','placeholder'=>'E-mail'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Website:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('website',$user['website'],array('class'=>'form-control','placeholder'=>'Your Website'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Address:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::textarea('address',$user['address'],array('class'=>'form-control','placeholder'=>'Your Address'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Country:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('country',$user['country'],array('class'=>'form-control','placeholder'=>'Your Country'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>State:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('state',$user['state'],array('class'=>'form-control','placeholder'=>'Your State'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>City:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('city',$user['city'],array('class'=>'form-control','placeholder'=>'Your City'))}}</label>
</div>

</div>

<div class="form-group">

{{Form::submit('UPDATE',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>

@stop
