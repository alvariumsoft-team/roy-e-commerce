@extends('adashb')
@section('dash_content')

<h2>Add New Project</h2>

<p>Create a brand new project.</p>

<form action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">	
<label class="col-lg-3 col-sm-3 col-xs-12" ><b>Company Logo:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12"  for="ifile-id1" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="zxc"><span id='ifile-label' class='ifile-label' >Upload Image</span>{{Form::file('logo',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'ifile-id1','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Company Name</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('cname','',array('class'=>'form-control','placeholder'=>'Company/Builder Name','required'=>'true'))}}</label>
</div>

<div class="form-group row">	
<label class="col-lg-3 col-sm-3 col-xs-12" ><b>Header Image:</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12"  for="ifile-id2" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd"><span id='ifile-labe'  class='ifile-label'>Upload Image</span>{{Form::file('header',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'ifile-id2','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Project Name</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('name','',array('class'=>'form-control','placeholder'=>'Listing Name','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Project Description</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::textarea('description','',array('class'=>'form-control','placeholder'=>'Listing Description','required'=>'true'))}}</label>
</div>

<!--

<div class="form-group row">	
<label class="col-xs-12 col-md-3" ><b>Project Video</b></label>
<label class="col-xs-12 col-md-6" ><div style="border:1px solid #dedede;cursor:pointer;color: #535644;padding: 6px 12px;font-size:14px;" class="asd"><span id='ifile-labe'  class='ifile-label'>Upload Project video</span>{{Form::file('video',array('class'=>'inputfile inputfile-1 ifile-id','id'=>'ifile-id2','data-multiple-caption'=>'{count} files selected'))}}</div></label>
</div>

-->

<div class="form-group row">	
<label class="col-lg-3 col-sm-3 col-xs-12" ><b>Project Video Link</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12" >{{Form::text('video_link','',array('class'=>'form-control','placeholder'=>'Youtube URL'))}}</label>
</div>



<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Project Area</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('area','',array('class'=>'form-control','placeholder'=>'Listing Area (in sq.ft.)','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Construction Status</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::select('cstat',array('under_construction'=>'Under Construction','completed'=>'Completed'),'',array('class'=>'form-control'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>No. Of Towers </b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('towers','',array('class'=>'form-control','placeholder'=>'Total No. Of Towers','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>No. Of Floors </b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('floors','',array('class'=>'form-control','placeholder'=>'Total No. Of Floors','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Total No. Of Units </b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('units','',array('class'=>'form-control','placeholder'=>'Total Total No. Of Units','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Percentage Of Open Space </b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text('open_space','',array('class'=>'form-control','placeholder'=>'Percentage Of Open Space','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Possession Date</b></label>
<label class="col-lg-2 col-sm-2 col-xs-4">{{Form::text('possession_day','',array('class'=>'form-control','placeholder'=>'Day','required'=>'true'))}}</label>
<label class="col-lg-2 col-sm-2 col-xs-4">{{Form::text('possession_month','',array('class'=>'form-control','placeholder'=>'Month','required'=>'true'))}}</label>
<label class="col-lg-2 col-sm-2 col-xs-4">{{Form::text('possession_year','',array('class'=>'form-control','placeholder'=>'Year','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Launch Date</b></label>
<label class="col-lg-2 col-sm-2 col-xs-4 ">{{Form::text('launch_day','',array('class'=>'form-control','placeholder'=>'Day','required'=>'true'))}}</label>
<label class="col-lg-2 col-sm-2 col-xs-4">{{Form::text('launch_month','',array('class'=>'form-control','placeholder'=>'Month','required'=>'true'))}}</label>
<label class="col-lg-2 col-sm-2 col-xs-4">{{Form::text('launch_year','',array('class'=>'form-control','placeholder'=>'Year','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Map Location</b></label>
<label class="col-lg-3 col-sm-3 col-xs-12">{{Form::text('latitude','',array('class'=>'form-control','placeholder'=>'Latitude','required'=>'true'))}}</label>
<label class="col-lg-3 col-sm-3 col-xs-12">{{Form::text('longitude','',array('class'=>'form-control','placeholder'=>'Longitude','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Amenities</b></label>
<label class="col-lg-6 col-sm-6 col-xs-8">{{Form::text('amenities[]','',array('class'=>'form-control','placeholder'=>'Amenities'))}}</label>
<label class="col-lg-3 col-sm-3 col-xs-4"><span style="color:#92bd1d;"><i style="padding:10px;padding-left:0px;cursor:pointer;" class="fa fa-plus plus-amen"></i></span></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"></label>
<label class="col-lg-6 col-sm-6 col-xs-12 amen-div" ></label>
<label class="col-lg-3 col-sm-3 col-xs-12"></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Specification</b></label>
<label class="col-lg-6 col-sm-6 col-xs-8">{{Form::text('spec[]','',array('class'=>'form-control','placeholder'=>'Specification'))}}</label>
<label class="col-lg-3 col-sm-3 col-xs-4"><span style="color:#92bd1d;padding-top:10px;"><i style="padding:10px;padding-left:0px;cursor:pointer;" class="fa fa-plus plus-spec"></i></span></label>
</div>

<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"></label>
<label class="col-lg-6 col-sm-6 col-xs-12 spec-div" ></label>
<label class="col-lg-3 col-sm-3 col-xs-12"></label>
</div>

<div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Phone</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' phone','',array('class'=>'form-control','placeholder'=>'Phone Number','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Email</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' email','',array('class'=>'form-control','placeholder'=>'E-mail','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Website</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' website','',array('class'=>'form-control','placeholder'=>'Your Website','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Address</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::textarea(' address','',array('class'=>'form-control','placeholder'=>'Your Address','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>Country</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' country','',array('class'=>'form-control','placeholder'=>'Your Country','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>State</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' state','',array('class'=>'form-control','placeholder'=>'Your State','required'=>'true'))}}</label>
</div>
<div class="form-group row">
<label class="col-lg-3 col-sm-3 col-xs-12"><b>City</b></label>
<label class="col-lg-6 col-sm-6 col-xs-12">{{Form::text(' city','',array('class'=>'form-control','placeholder'=>'Your City','required'=>'true'))}}</label>
</div>

</div>

<div class="form-group">{{Form::submit('Add Project',array('class'=>'btn btn-large btn-primary '))}}</div>

</form>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('.plus-amen').on('click', function(){
			$('.amen-div').append('<input style="margin-bottom:10px;" class="form-control" placeholder="Amenities" name="amenities[]" value="" type="text">');	
		});

		$('.plus-spec').on('click', function(){
			$('.spec-div').append('<input style="margin-bottom:10px;" class="form-control" placeholder="Specification" name="spec[]" value="" type="text">');	
		});

	});
	
</script>

@stop
