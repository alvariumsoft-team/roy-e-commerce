@extends('adashb')

@section('dash_content')

<div class="side-body padding-top">

<h2>Add Frequently Asked Question</h2>

<p>Add a question.</p>

<form class="form" action="" method="post"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Topic:</b></label>
<label class="col-xs-12 col-md-6">
<select name="topic" class="form-control">

@foreach($topics as $topic):

<option value="{{$topic['id']}}">{{$topic['name']}}</option>

@endforeach

</select>

</label>
</div>


<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Question:</b></label>
<label class="col-xs-12 col-md-6">{{Form::text('q','',array('class'=>'form-control','placeholder'=>'Question','required'=>'true'))}}</label>
</div>

<div class="form-group row">
<label class="col-xs-12 col-md-3"><b>Answer:</b></label>
<label class="col-xs-12 col-md-6">{{Form::textarea('a','',array('class'=>'form-control','placeholder'=>'Answer','required'=>'true'))}}</label>
</div>

<div class="form-group">

{{Form::submit('ADD',array('class'=>'btn bt-large btn-primary'))}}

</div>

</form>


</div>


@stop
