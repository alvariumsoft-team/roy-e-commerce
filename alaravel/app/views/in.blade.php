@extends('m')

@section('content')





<div class="slider">


<div id="hero">
		
		
		<div class="flexslider">
		  <ul class="slides">
		    <li>
					<img src="/packages/realestate/img/full/1.jpg" alt="icy Mountain" data-thumbnail="img/thumbnail/1.jpg" />			
					<div class="banner_text_box">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
					</div><!--banner_text_box-->
					
						</li>		
		 		<li>
					<img src="/packages/realestate/img/full/2.jpg" alt="Stoney Creek" data-thumbnail="img/thumbnail/2.jpg" />				</li>
		    <li>
					<img src="/packages/realestate/img/full/1.jpg" alt="Narrow Road" data-thumbnail="img/thumbnail/1.jpg" />		
					<div class="banner_text_box">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
<button class="btn_slider">Book Now</button> 
					</div><!--banner_text_box-->
					
							</li>						 		
		    <li>
					<img src="/packages/realestate/img/full/3.jpg" alt="Grey Sky" data-thumbnail="img/thumbnail/3.jpg"/>				</li>		
		    <li>
					<img src="/packages/realestate/img/full/1.jpg" alt="Wood Pattern" data-thumbnail="img/thumbnail/1.jpg" />			
					
					<div class="banner_text_box2">
					<h3>Real Estate agents are Property consisting</h3>
					<p>Air conditioned, movie stocked library, <br> fine dinning experience and with
</p>
<button class="btn_slider">Book Now</button> 
					</div><!--banner_text_box-->
						</li>
		  </ul>
		</div>		
	</div>
</div>

<div class="clm"><hr /></div>

<div class="clm">
<div class="property-listing">

<div class="heading"><h1>Property Listing</h1></div>

<div class="container">
@foreach($Index['property-list'] as $property)

<div class="property-block">
<div style="height:80%;width:100%;">
<a href="{{$property['url']}}"><img src="{{$property['img']}}" class="property-image"/></a>
</div>
<div style="padding:5px;text-align:center;"><a href="#"><b>{{$property['name']}}</b></a></div>
<div style="padding:5px;text-align:center;">{{$property['name']}}</div>
</div>

@endforeach
</div>
<div class="clearfix"></div>
</div>


<div class="property-listing">

<div class="heading"><h1>Connect With Our Experts</h1></div>

<div class="container">
@foreach($Index['experts'] as $expert)

<div class="property-block"><img src="{{$expert['img']}}" /></div>

@endforeach
</div>
<div class="clearfix"></div>
</div>
</div>

@stop