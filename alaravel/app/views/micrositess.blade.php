@extends('main')
@section('content')

<style type="text/css">
	
	/*****************************Navigation Start***************************************/
	.logo_center{
		text-align:center;
	}
	.admin_back {
		background: #fff;
		box-shadow: 0 2px 4px #ddd;
		position: fixed;
		z-index: 999;
		width: 100%;
	}
	.navbar, .dropdown-menu{
	background:rgba(255,255,255,0.25);
	border: none;

	}

	.nav>li>a, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover, .dropdown-menu>li>a, .dropdown-menu>li{
	  border-bottom: 3px solid transparent;
	}
	.nav>li>a:focus, .nav>li>a:hover,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
	  border-bottom: 3px solid transparent;
	  background: none;
	}
	.navbar a, .dropdown-menu>li>a, .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover, .navbar-toggle{
	 color: #fff;
	}
	.dropdown-menu{
		  -webkit-box-shadow: none;
		box-shadow:none;
	}

	.navbar-toggle .icon-bar{
		color: #fff;
		background: #000;
	}
	.menu_togga_d a {
		color: #7a7a7a;
	}
	.menu_togga_d li:hover{
		border-bottom: #5cd468 3px solid;
	}
	.menu_togga_d > li > a {
		position: relative;
		display: block;
		padding: 10px 12px;
	}
	.topcontact {
		background-color: #5cd468;
		margin-top: 3px;
		border-radius: 0;
	}
	.topcontact a {
		color: #fff;
		padding: 0px 9px;
	}
	.navigation {
		margin-top: 13px;
		margin-bottom: 0;
	}
	.slider_bar_d {
		z-index: 999;
		padding: 64px 0;
	}
	.logo_2.cdf {
		display: none;
	}
	/*****************************Navigation end***************************************/

	/*****************************Apartments Start***************************************/
	.over_view {
		margin: 50px 0;
	}
	.greenBg {
		background-color: #5cd468;
		padding: 10px 20px;
	}
	.cols1 h1 {
		color: #fff;
		font-weight: bold;
		margin: 0;
	}
	.cols1 h2 {
		margin: 0;
		padding: 0;
		color: #fff;
		font-size: 24px;
		line-height: 34px;
		padding-bottom: 8px;
		font-weight: 600;
	}
	.cols1 h3 {
		color: #fff;
		font-size: 23px;
		font-weight: 600;
		margin: 0;
	}
	.cols1 h3 span {
		color: #ffff00;
	}
	.cols2 h1 {
		font-size: 30px;
		margin: 0;
		color: #2c2d31;
	}
	.cols2 p {
		margin: 0;
		padding: 0;
		font-size:16px;
		color: #7a7a7a;
		padding-top: 20px;
		padding-bottom: 10px;
		text-align: left;
	}
	.download {
		margin: 20px 0 0;
	}
	.download li a {
		color: #7a7a7a;
		text-decoration: none;
		font-size: 16px;
	}
	.download i {
		margin-right: 10px;
			color:#5CD468;
	}
	.download li {
		list-style: none;
	}
	.download ul {
		padding-left:0px;
	}
	/*****************************Apartments End***************************************/


	/*****************************description  Start***************************************/
	.description {
		clear: both;
		margin-top: 20px;
		border: 1px solid #ececec;
		content: " ";
		overflow: hidden;
	}
	.description .col-md-3 {
		width: 25%;
		float: left;
		box-sizing: border-box;
	}
	.description .sep {
		border-right: 1px solid #ececec;
	}
	.description ul {
		padding: 20px 0 20px 0;
	}
	.description ul li {
		font: normal 14px/40px 'Open Sans', sans-serif;
		background: url({{URL::to('packages/images/arrow.png')}}) no-repeat 0 15px;
		padding-left: 15px;
		list-style: none;
	}
	.sep a strong {
		font-weight: 600;
		color: #7A7A7A;
	}
	.sep li a{color:#7A7A7A;text-decoration:none;}

	.blockPadding.overviewDetail {
		display: none;
	}
	.overviewRow:first-child {
		border-top: 0;
		padding-top: 0;
	}
	.overviewRow {
		border-top: 1px solid #f0f0f0;
		padding: 7px 0 7px;
	}
	.overviewRow .overviewLabel {
		color: #999;
		float: left;
		width: 50%;
	}
	.overviewRow .labelDetail {
		color: #333;
		float: left;
		width: 50%;
	}
	/*****************************description  End***************************************/

	/*****************************animated  Start***************************************/
	.animated {
		background: #F7F7F7;
		padding: 60px 0;
	}
	.animated_Up h1 {
		color: #2c2d31;
		font-size: 30px;
		padding-bottom: 30px;
		box-sizing: border-box;
		font-weight: 400;
	}
	.animated_Up li {
		width: 50%;
		font: 14px/30px 'Open Sans', sans-serif;
		color: #666666;
		padding: 4px 10px 0 15px;
		box-sizing: border-box;
		float: left;
		background: url({{URL::to('packages/images/arrow.png')}}) no-repeat 0 15px;
		list-style: none;
	}
	.animated_Up li a{
		color: #666666;
	}
	.club_house{
		padding-left:0px;
	}
	.fadeIn_DownBig img {
		width: 100%;
	}
	/*****************************animated  End***************************************/


	/*****************************Tabs  Start***************************************/

	.location_ {
		width: 100%;
		font-size: 14px;
		line-height: 30px;
		padding: 10px 30px 10px 20px;
		text-align: left;
		overflow: hidden;
	}
	.tabd_D {
		border: 1px solid #DDDDDD;
	}
	.nav.nav-tabs.map_d {
		margin-bottom: 0;
		background: #DDDDDD;
		border-radius:0;
		padding: 10px 10px 0px 30px;
	}
	.nav-tabs > li {
		margin-bottom: -3px;
	}
	.map_d a {
		color: #7a7a7a ;
	}
	.map_d > li > a:hover {
		border-color: #DDDDDD;
		border-bottom: 0px solid transparent;
	}
	.map_gall img {
		width: 100%;
	}
	.tabs_botton {
		margin: 50px 0;
	}
	/*****************************Tabs  End***************************************/


	/*****************************Slider_bar botton Start***************************************/
	.carousel-control {
	  padding-top:10%;
	  width:5%;
	}
	.gallery {
		color: #333;
		font-size: 30px;
		padding-top: 100px;
		padding-bottom: 40px;
		width: 100%;
		box-sizing: border-box;
	}
	.well_well {
		padding: 5px;
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 0px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
	}
	/*****************************Slider_bar botton End***************************************/



	/*****************************Projects  Start***************************************/
	.Projects {
		margin: 40px 0;
	}
	.Malwa {
		margin: 40px 0;
	}
	.project_Gh {
		float: left;
		border: 1px solid #d4dcd5;
		padding: 45px 22px;
	}
	.project_Gh h4 {
		font-weight: bold;
		font-size: 21px;
		line-height: 30px;
	}
	.highway  h4{
		color: #7a7a7a;
		line-height: 30px;
		font-size:14px;
	}


	/*****************************Projects  End***************************************/

	/*****************************Footer Start***************************************/
	.footer_copy {
		text-align: center;
	}
	.footer_back {
		background: #000;
		padding: 20px 0;
	}
	.mb-logo-footer {
		color: #7a7a7a;
		font-size: 12px;
		margin-bottom: 10px;
	}
	.copyRightInfo{
		color: #7a7a7a;
		font-size: 12px;
	}

	/*****************************Footer  End***************************************/
	.home_d {
		width: 50%;
		float: left;
		color: #2c2d31;
		font-size: 14px;
	}
	#amount-words {
		color: #2c2d31;
		float: right;
		font-size: 12px;
		margin: 10px 0;
	}
	.icon-ruppee {
		margin-left: 58px;
		color:#5CD468;
		font-size: 18px;
		font-weight: bold;
	}
	#homeLoanAmountInput {
		border: 1px solid #cdcbcb;
		float: right;
	}
	.price_ {
		background: #F7F7F7;
		margin: 40px 0;
	}
	.price_list {
		background: #fff;
		float: left;
		padding: 33px 50px;
	}
	.cal h1 {
		color: #333;
		font: 30px 'Open Sans', sans-serif;
		padding-top: 50px;
		padding-bottom: 10px;
		width: 100%;
		box-sizing: border-box;
	}
	.cal h2 {
		color: #333;
		font: 14px 'Open Sans', sans-serif;
		padding-top: 0px;
		font-weight: 700;
	}
	.cal h3 {
		color: #5b5b5b;
		font: 14px 'Open Sans', sans-serif;
		margin-top: 0;
		font-weight: 500;
		padding-bottom: 10px;
		line-height: 24px;
	}
	.month_ {
		width: 50%;
		float: left;
	}
	.monxdth_ {
		width: 50%;
		float: left;
	}
	#emiMonthly {
		width: 100%;
		float: left;
		color: #5CD468;
		font-weight: bold;
	}
	#monthly_emiText {
		font-weight: normal;
		color: #5b5b5b;
	}
	.total{
	color: #5b5b5b;
	font-weight: 500;
	font-size: 14px;
	}



	/*****************************Resposive  Start***************************************/

	@media only screen and (max-width: 1024px){
		
	.menu_togga_d > li > a {
		padding: 9px 6px;
		font-size: 12px;
	}
	.project_Gh h4 {
		font-size: 17px;
	}	
	}
	@media only screen and (max-width: 991px){

	.cols1 h1 {
		font-size: 30px;
	}
	.cols1 h2 {
		font-size: 18px;

	}
	.anmm{
		font-size:18px;
	}
	.cols1 h3 {
		font-size: 18px;
	}
	.sep li a {
		font-size: 10px;
	}
	.project_Gh h4 {
		font-size: 12px;
	}
	.home_d {
		font-size: 12px;
	}
	.icon-ruppee {
		margin-left: 0;
	}
	#homeLoanAmountInput {
		width: 125px;
	}
	}

	@media only screen and (max-width: 736px){
	.logo_center {
		display: none;
	}
	.blockPadding.overviewDetail {
		display: block;
	}
	.description {
		display: none;
	}
	.navigation {
		margin-top: 0;
	}
	.slider_bar_d {
		z-index: 999;
		padding: 61px 0 0;
	}
	.logo_2.cdf {
		display: block;
	}
	}

	@media only screen and (max-width: 568px){
	.Malwa h2 {
		font-size: 20px;
	}
	.gallery {
		font-size: 18px;
		padding-top: 0px;
		padding-bottom: 6px;
	}
	.animated_Up h1 {
		font-size: 23px;
		padding-bottom: 9px;
	}
	}

	@media only screen and (max-width: 360px){
	}
	
	
	.floor-plan-pan > .panel-body{display:none;}
	
	

	.slidePanel{cursor:pointer;}

	.slide-minus{display:none;}

	.panel-body{display:none;}

	

	.image-ul{border-bottom: 2px solid #EF8600;}

	

	.image-types{

		text-align:center;

		display:block;

		color:#2a2a2a;

		font-size:18px;

		font-weight:700;

		padding-top:24px;

		outline:none !important;

	}

	

	.image-types > .fa{

		margin:0px 14px;

	}

	

	.image-types  {padding-left:25%;}

	

	.image-types{color:#4ba7e4;}

	.image-types:hover{color:#00a65a;}

	

	.bx-prev{background: rgba(0,0,0,0.2) !important;}

	.bx-next{background: rgba(0,0,0,0.2) !important;}

	.flexslider .slides img {height: 500px !important;}

	.slider{display:block;}

	.floor-plan-detail{display:block;}

	

	.degnava{padding:16px 24px;text-decoration:none !important;color:#4ba7e4;font-weight:700;}

	.degnava:hover{color:#00a65a;}

	
	
	
	
	
</style>


<?php $arr = array(); $arrr = array(); $prc = array(); $ara = array();?>



@foreach($units as $u)

@if(!in_array($u->beds, $arr))

<?php $arr[] = $u->beds; ?>

<?php $arrb[] = $u->baths; ?>

<?php $prc[] = $u->price; ?>

<?php $ara[] = $u->area; ?>

@endif

@endforeach

    <!---------------------- over_view Start------------------------------>	
<section class="over_view">
   <div class="container">
      <div class="row">
	     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
			<div class="cols1">
			<section class="greenBg">
			<h2>{{ucwords($project['name'])}}</h2>
			<h2><?php $arlen = count($arr);for($i=0;$i<$arlen;$i++){echo $arr[$i];if($i != ($arlen-1)){echo ',';}}?> BHK Apartments </h2>
			<h2><span class="anmm" style="font-size:23px;">{{$project['address']}}</span></h2>
			<h3 class="reset">{{min($ara)}} to {{max($ara)}} sq.ft. | <span> <i class="fa fa-usd"></i> {{min($prc)}} onwards</span></h3>
			</section>
			<div class="video-img"><a href="#imagegallery"><img src="{{URL::to('packages/images/video-img.jpg')}}" alt="Escon Arena" class="img-responsive"></a></div>
			</div>
		 </div>
		 <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			<div class="cols2" data-animation="animated zoomInUp">
			<h1>{{ucwords($project['name'])}}</h1>
			<!--<p style="font-weight:bold;">Offer: Free Car Parking, Development Charges &amp; Pre Approved Loan from SBI</p><br>-->
			<br>
			
			{{HTML::image($project['header'],'alt',array('height'=>'300px','width'=>'100%'))}}
			
			<p>
			{{$project['about']}}	
			</p>		
			
			<div class="download">
			<ul>
			<!--<li><a href="#"><i class="fa fa-download" aria-hidden="true"></i>Brochure</a></li>-->
			</ul>
			</div>
			</div>
		 </div>
	  </div>
   </div>
</section>
<section class="over_view">
   <div class="container">
      <div class="row">
			<div class="col-lg-12">
			<div class="description middleWrapper">
			<div class="col-md-3 sep" style="height: 160px;">
			<ul>
			<li><a href="#">Project Area: <strong>{{$project->project_area}}</strong></a></li>
			<li><a href="#">Total No. of Tower: <strong>{{$project->towers}}</strong></a></li>
			</ul>
			</div>
			<div class="col-md-3 sep" style="height: 160px;">
			<ul>
			<li><a href="#">Total No. of Floors: <strong>{{$project->floors}}</strong></a></li>
			<li><a href="#">Total &amp; Available Units: <strong>{{$project->units}}</strong></a></li>
			</ul>
			</div>
			<div class="col-md-3 sep" style="height: 160px;">
			<ul>
			<li><a href="#">Launch Date: <strong>{{$project->launch_month}} {{ $project->launch_year}}</strong></a></li>
			<li><a href="#">Authority Approved: <strong>Development Authority </strong></a></li>
			</ul>
			</div>
			<div class="col-md-3 sep" style="height: 160px;">
			<ul>
			<li><a href="#">Occupancy Certificate: <strong>Yes</strong></a></li>
			<li><a href="#">Commencement Certificate: <strong>Yes</strong></a></li>
			</ul>
			</div>
			</div>
			
			
			<div class="blockPadding overviewDetail">
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Rate</div>
			<div class="labelDetail">7 - 13&nbsp;per&nbsp;sqft</div>
			</div>
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Covered Area</div>
			<div class="labelDetail">610 - 5700 sqft</div>
			</div>
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Property Types</div>
			<div class="labelDetail">Flat, House, Showroom</div>
			</div>
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Launch Date</div>
			<div class="labelDetail">Dec 2012</div>
			</div>
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Status</div>
			<div class="labelDetail">Ongoing</div>
			</div>
			<div class="overviewRow clearfix">
			<div class="overviewLabel">Units</div>
			<div class="labelDetail">23&nbsp;Towers,&nbsp;1211&nbsp;Units</div>
			</div>
			</div>



			</div>
			
	  </div>
   </div>
</section>
    <!---------------------- over_view End------------------------------>
	

<div class=" container list-headd">
	<h3 style="border-bottom:1px solid #dedede;padding:4px;">{{ucwords($project['name'])}} Video</h3>
	<div >
		  <p>
			<iframe style="width:100%; height:500px;" src="{{$project->vlink}}" frameborder="0" allowfullscreen></iframe>
		  </p>
	</div>

</div>



    <!---------------------- animated Start------------------------------>	
<section class="animated">
	<div class="container" >
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="animated_Up">
				<h1>{{ucwords($project['name'])}} Amenities</h1>
				<ul class="club_house">
					
				@foreach($amenities as $amen)
				
				<li><a href="#">{{$amen->amenities}}</a></li>
				
				@endforeach
					
				<!--
					
				<li><a href="#">Huge 15000 sq.ft clubhouse</a></li>
				<li><a href="#">Banquet/Party Hall</a></li>
				<li><a href="#">Yoga &amp; Meditation Park</a></li>
				<li><a href="#">Jogging /Cycling Pathways</a></li>
				<li><a href="#">24 hours CCTV</a></li>
				<li><a href="#">2 lifts in each tower</a></li>
				<li><a href="#"> 100% power backup</a></li>
				<li><a href="#">Landscaped Garden with Gazebo</a></li>
				<li><a href="#">Cafe &amp; restaurant with wifi</a></li>
				<li><a href="#">Advanced fire safety systems</a></li>
				<li><a href="#">Well equipped gymnasium</a></li>
				<li><a href="#">Indoor games</a></li>
				<li><a href="#">Tennis - badminton and Basket court</a></li>
				<li><a href="#">In close proximity to shopping mall, hotels, schools and medical centers</a></li>
				<li><a href="#">Advanced Fire Safety Spelling</a></li>
				<li><a href="#">Court Spelling</a></li>
				<li><a href="#">Close Space Proximity</a></li>
				<li><a href="#">Hotels, Space</a></li>
				<li><a href="#">Swimming pool with separate kids pool</a></li>
				
				-->
				
				</ul>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			   <div class="fadeIn_DownBig">
			      <img src="{{URL::to('packages/images/amenities.jpg')}}">
			   </div>
			</div>
		</div>
	</div>
</section>

    <!---------------------- animated End------------------------------>	
	
	
<!--floor plan start-->

<div class="list-headd container" id="location">

	<h3 style="border-bottom:1px solid #dedede;padding:4px;">Floor Plan</h3>

<div>





@foreach($units as $u)



@if(!in_array($u->beds, $arrr))



<?php /*$arrr[] = $u->beds;*/ ?>



<div class="panel panel-default floor-plan-pan">

<div class="panel-heading clearfix">

<span class="pull-left"><b>{{$u->beds}} BHK {{ucwords($u->list_type)}}({{$u->area}} sqft) - ${{$u->price}}</b></span>

	

<i class="fa fa-plus slidePanel slide-plus pull-right"></i><i class="fa fa-minus slidePanel slide-minus pull-right" style="display:none;"></i>

</div>

  <div class="panel-body">

	<section>

		<div class="row">

			<div class="col-md-3">

			<div class="bd-example bd-example-tabs" role="tabpanel">

			  <ul class="nav nav-tabs" id="myTab" role="tablist">

				<li class="nav-item active">

				  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;"><b>Floor Plan</b></a>

				</li>

			  </ul>

			  <div class="tab-content" id="myTabContent">

				<div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab" aria-expanded="false">

				  <p><img class="floor-plan-img" class="img-responsive" src="{{URL::to($u->floor_plan)}}" alt="No Image Available" style="cursor:pointer;width:100%;height:auto;" ></p>

				</div>

			  </div>

			</div>

				<!--<img class="img-responsive" src="{{URL::to($u->floor_plan)}}" >-->

			</div>

			

			<div class="col-md-3">

				<div class="form-group">

				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-amenities" >Amenities</button>

				</div>

				<div class="form-group">

				<button class="form-control btn btn-default unit-extras" style="outline:none;"  id="unit-spec">Specifications</button>

				</div>

			</div>

			<div class="col-md-6">

				<div class="row">

					<div class="col-md-4">

					<h4>Carpet Area</h4>

					<b>{{$u->area}}sqft</b>

					<br /><br />

					<h4>Furnished</h4>

					<b>{{ucwords($u->furnished)}}</b>

					</div>

					<div class="col-md-4">

					<h4>Price</h4>

					<b>${{$u->price}}</b>

					<br /><br />

					<h4>No. Of Balconys</h4>

					<b>{{$u->balconys}}</b>

					</div>

					<div class="col-md-4">

					<h4>Construction Status</h4>

					<b>@if($u->build_status == 'under') Under Construction @else {{ucwords($u->build_status)}} @endif</b>

					<br /><br />

					<h4>No. Of Bathrooms</h4>

					<b>{{$u->baths}}</b>

					</div>

				</div>

			</div>

		</div>

	</section>

  </div>

</div>



@endif

		

@endforeach	







</div>

</div>


<!--floor plan end-->
	
	
	
	
	
<!--map start-->

	<section class="container">

		<div class="list-headd">

			<h3 style="border-bottom:1px solid #dedede;padding:4px;">{{ucwords($project['name'])}} Map View</h3>

		<div >

		<div class="re-map" style="overflow:hidden;height:100%;width:100%;">

			<div id="map" style="height:500px;width:100%">

				

			</div>

		</div>

	</section>



<!--map end -->
	
	
	
	    <!---------------------- Tabs Start------------------------------>	
<!--
	<section class="tabs_botton">
		<div class="container">
		<div class="row">
		<div class="col-lg-12">
		<div class="tabd_D">
-->
	  <!-- Nav tabs -->
<!--
	  <ul class="nav nav-tabs map_d" role="tablist">
		<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Location Map</a></li>
		<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Site Map</a></li>
		<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Location Advantages</a></li>
	  </ul>
-->
	  <!-- Tab panes -->
<!--
	  <div class="tab-content map_gall">
		<div role="tabpanel" class="tab-pane active" id="home">
		<img src="{{URL::to('packages/images/locationmap.jpg')}}" alt="Location Map">
		</div>
		<div role="tabpanel" class="tab-pane" id="profile">
		<img src="{{URL::to('packages/images/locationmap.jpg')}}" alt="Location Map">
		</div>
		<div role="tabpanel" class="tab-pane" id="messages">
			<ul class="location_">
			<li><strong>5 Min drive from Airport</strong> </li>
			</ul>
		</div>
	  </div>

	</div>
		</div>
		</div>
	</div>
	</section>
-->
	
	    <!---------------------- Tabs Start------------------------------>	
	    
<!--
	    	
<section class="price_">
   <div class="container">
      <div class="row">
	     <div class="col-lg-12">
		     <div class="cal">
			   <h1>EMI Calculator</h1>
			   <h2>How much do i need to pay as EMI?</h2>
			   <h3>Here we are to help you calculate the amount you need to pay each month for a amount taken as loan</h3>
			 </div>
		 </div>
		 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
		   <div class="price_rate">
			<label class="home_d">Home Loan Amount:</label>
			<div class="formField"><span class="icon-ruppee">&#x20B9;</span>
			<input id="homeLoanAmountInput" value="" type="text">
			</div>
			<div id="amount-words"> Twenty Eight Lakh Twenty Four Thousand Two Hundred Sixty</div>
		   </div>
		 </div>
		 <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
		 <div class="price_list">
				<div class="monxdth_">
				   <p class="monxth_"><label class="total">Monthly EMI:</label></p>
				</div>
				<div class="month_">
				   <p class="rupee_"><span id="emiMonthly">21126 </span>
				   <span id="monthly_emiText">twenty one thousand one hundred and twenty six </span></p>
				 </div>
				 <div class="monxdth_">
				   <p class="monxth_"><label class="total">Total Interest Payable:</label></p>
				</div>
				<div class="month_">
				   <p class="rupee_"><span id="emiMonthly">2335723 </span>
				   <span id="monthly_emiText">twenty three lakh thirty five thousand seven hundred and twenty three</span></p>
				 </div>

				 				 <div class="monxdth_">
				   <p class="monxth_"><label class="total">Total Payment: <br>(Principal + Interest) </label></p>
				</div>
				<div class="month_">
				   <p class="rupee_"><span id="emiMonthly">4835723 </span>
				   <span id="monthly_emiText">twenty one thousand one hundred and twenty six </span></p>
				 </div>
		 </div>
		  </div>
	  </div>
   </div>
</section>


-->


<section id="imagegallery" class="slider_bar_botton">
	<div class="container">
    <div class="col-md-12">
         <h1 class="gallery">{{ucwords($project['name'])}} Image Gallery</h1>

	<div class="row">

		<div>

			@foreach($images as $image)

				<div class="col-lg-4" >

					<div class="five" style="margin-bottom:20px;">

						<p style="height:200px;"><img class="floor-plan-img img-responsive" src="{{URL::to($image->image)}}" style="width:100%;height:100%;cursor:pointer;"></p>

					</div>

				</div>

			@endforeach
			
			
				<div class="col-lg-4" >

					<div class="five" style="margin-bottom:20px;">

						<p style="height:200px;" class=""><img class="image-degree img-responsive" src="{{URL::to('packages/images/video-img.jpg')}}" style="width:100%;height:100%;cursor:pointer;"></p>

					</div>

				</div>
			
			

		</div>

	</div>        <!--/well-->
    </div>
</div>
</section>


<!---------------------- Projects Start------------------------------>

 
<section class="Projects">
	   <div class="container">
	      <div class="row">
		      <div class="col-lg-12">
				  <div class="Malwa">
				     <h2>About {{ucwords($project['name'])}}</h2>
				  </div>
			  </div>
			  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			     <div class="project_Gh">
				    <h4>{{ucwords($project['name'])}}</h4>
				 </div>
			 </div>
			 <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				   <div class="highway">
						<h4>{{$project['about']}}</h4>
						<h4>{{ucwords($project['address'])}}</h4>
				   </div>
				</div>
		  </div>
	   </div>
</section>

<!---------------------- Projects End------------------------------>


<!--modal-->


<div class="modal fade" id="floor-plan-modal" tabindex ="-1" role="dialog" aria-labelledby="floor-plan-heading" aria-hidden="true" style="top:10%;">

	<div class="modal-dialog ">

		<div class="modal-content">

			<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

			<div class="modal-title" id="floor-plan-heading"></div>

			</div>

			<div class="modal-body" id="floor-plan-body">

				

			</div>

			<div class="modal-footer"></div>

		</div>

	</div>

</div>

<div class="modal fade" id="unit-extras-modal" tabindex ="-1" role="dialog" aria-labelledby="unit-extras-heading" aria-hidden="true" style="top:17%;">

	<div class="modal-dialog ">

		<div class="modal-content">

			<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

			<div class="modal-title" id="unit-extras-heading"></div>

			</div>

			<div class="modal-body" id="unit-extras-body"></div>

			

			<div class="modal-footer"></div>

		</div>

	</div>

</div>



<div class="photos-modal" style="position:fixed;transition:0.5s;top:0px;height:96%;width:98%;margin:1%;background:#fff;z-index:10000000;box-shadow: 1px 1px 5px #2a2a2a;display:none;">

	<div class="row">

		<div class="col-md-12 clearfix" >

			

			<div class="pull-left">

				<nav class="degnav clearfix" style="display:none;">

				  <a class="degnava pull-left deg-front" href="javascript:void();">Front Yard</a>

				  <a class="degnava pull-left deg-bedroom" href="javascript:void();">Bedroom</a>

				  <a class="degnava pull-left deg-storage" href="javascript:void();">Storage</a>

				  <a class="degnava pull-left deg-backyard" href="javascript:void();">Backyard</a>

				  <a class="degnava pull-left deg-hall" href="javascript:void();">Hall</a>

				  <a class="degnava pull-left deg-living" href="javascript:void();">Living Room</a>

				  <a class="degnava pull-left deg-kitchen" href="javascript:void();">Kitchen</a>

				  <a class="degnava pull-left deg-overflow" href="javascript:void();">Overflow</a>

				  <a class="degnava pull-left deg-bathroom" href="javascript:void();">Bathroom</a>

				</nav>

			</div>

			

			<div class="pull-right" style="padding:10px;">

				<a href="" class="sad">

					<img src="{{URL::to('packages/images/cancel.png')}}" style="width:25px;"/>

				</a>

			</div>

		</div>

		<div class="col-md-12" >

			

			<section class="slider" style="display:none;">

				<div class="flexslider">

				  <ul class="slides" id="slides-html">

					@foreach($images as $image)

					<li data-thumb="{{URL::to($image->image)}}"><img src="{{URL::to($image->image)}}" /></li>

					@endforeach

				  </ul>

				</div>

			</section>

			<section class="floor-plan-detail" style="display:none;">

				<div class="bd-example bd-example-tabs" role="tabpanel">

				  <ul class="nav nav-tabs" id="myTab" role="tablist">

					<li class="nav-item active">

					  <a class="nav-link" id="2d-tab" data-toggle="tab" href="#2d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>2D</b></a>

					</li>

					<li class="nav-item">

					  <a class="nav-link" id="3d-tab" data-toggle="tab" href="#3d" role="tab" aria-controls="home" aria-expanded="false" style="color:#2a2a2a;outline:none;padding: 16px 34px;"><b>3D</b></a>

					</li>

				  </ul>

				  <div class="tab-content" id="myTabContent">

					<div role="tabpanel" class="tab-pane fade active in" id="2d" aria-labelledby="2d-tab" aria-expanded="false">

					  <p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floor_plan)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>

					</div>

					<div role="tabpanel" class="tab-pane fade  in" id="3d" aria-labelledby="3d-tab" aria-expanded="false">

						<p><img class="floor-plan-im" class="img-responsive" src="{{URL::to($u->floorplan3d)}}" style="display: flex;margin:auto;min-width:500px;max-height:500px;" ></p>

					</div>

				  </div>

				</div>

			</section>

			<section class="degree" id="gdegree" style="display:none;"></section>

		</div>

	</div>

</div>



<!--modal-->



{{HTML::script('packages/js/jquery.flexslider.js')}}

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyClvxFM9LBGu6Gimzeuir9amgjuFD38SHc"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{{URL::to('public/canvas/three.min.js')}}"></script>

<script type="text/javascript" src="{{URL::to('public/canvas/CanvasRenderer.js')}}"></script>

<script type="text/javascript" src="{{URL::to('public/canvas/Projector.js')}}"></script>



<script type="text/javascript">

	

	$(document).ready(function(){

		

		$('.slidePanel').on('click', function(){

		

		$(this).siblings().css('display','block');

		$(this).css('display','none');

		

		if($(this).hasClass('slide-plus')){

			$('.panel-body').css('display','none');

			$('.slide-minus').css('display','none');

			$('.slide-plus').css('display','block');

		$(this).siblings().css('display','block');

		$(this).css('display','none');

			var panelBody = $(this).parent().next()[0];

			$(panelBody).css('display','block');

		}

		

		if($(this).hasClass('slide-minus')){var panelBody = $(this).parent().next()[0];$(panelBody).css('display','none');}

		

			

		});	



		$('.unit-extras').on('click', function(){



		var id = $(this).attr('id');

		var heading = '';

		var content = '';

		$('#unit-extras-heading').html(heading);

		$('#unit-extras-body').html(content);

		if(id == 'unit-amenities'){

			heading = 'Amenities';

			$.post("{{URL::to('projectspec')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});

		}

		if(id == 'unit-ladvantages'){

			heading = 'Local Advantages';

			

		}

		if(id == 'unit-usp'){

			heading = 'Project USP';

		}

		if(id == 'unit-spec'){

			heading = 'Specifications';

			$.post("{{URL::to('projectamenities')}}", {project_id:<?php echo $project->id; ?>}).done(function(data){$('#unit-extras-body').html(data);});

		}



		$('#unit-extras-heading').html('<h4>'+heading+'</h4>');





		$('#unit-extras-modal').modal('show');	

			

		});



		$('.floor-plan-img').on('click',function(){

			$('#floor-plan-body').html($(this).parent().html());

			$('#floor-plan-modal').modal('show');	

			

		});



		latitude = <?php echo "'$project->map_lat'" ?>;

		longitude = <?php echo "'$project->map_long'" ?>;

		

		

		if((latitude == '') || (latitude == undefined)){

			latitude	= 28.7041;

			longitude	= 77.1025;

		}

		

		setTimeout(function() {

			

		

		latitude = parseFloat(latitude);

		longitude= parseFloat(longitude);



			var uluru = {lat: latitude, lng: longitude};

			var map = new google.maps.Map(document.getElementById('map'), {

			  zoom: 10,

			  scrollwheel: false,

			  center: uluru

			});

			var marker = new google.maps.Marker({

			  position: uluru,

			  map: map

			});

			

		}, 50);

		

		$('#contact-form-btn').on('click', function(e){

			e.preventDefault();

			var url = '';

			//$.post(url, {}).done(function(data){

			

			$('#contact-form-modal').modal('show');	

				

			//}

			

			

		});



		$('.image-photos').on('click', function(e){

			e.preventDefault();

				$('.slider').css('opacity', '0');

				$('.flexslider').flexslider({

					animation: "slide",

					start: function(slider){

					  $('body').removeClass('loading');

					}

				});

				$('.photos-modal').css('display', 'block');

				$('.slider').css('display', 'block');

				setTimeout(function(){$('.slider').css('opacity', '1');}, 300);

				$('.image-photos').focus();

		});

		

		$('.image-plan').on('click', function(e){

			e.preventDefault();

			$('.photos-modal').css('display', 'block');

			$('.floor-plan-detail').css('display', 'block');

			$('.image-photos').focus();	

		});

		

		$('.image-degree').on('click', function(e){

			e.preventDefault();

			$('.degree').html('');

			$('.photos-modal').css('display', 'block');

			$('.degree').css('display', 'block');	

			$('.degnav').css('display', 'block');

			$('.gcanvas').focus();

			panaroma(front);

			

		});



		$('.sad').on('click', function(e){

			e.preventDefault();

			$('.photos-modal').css('display', 'none');

			$('.slider').css('display', 'none');

			$('.floor-plan-detail').css('display', 'none');

			$('.degree').css('display', 'none');

			$('.degnav').css('display', 'none');

		});



	});

	

</script>



<script>



<?php

	$s = [];

	foreach($f360 as $f){

		$t = URL::to($f->image);

		$s[$f->type][] = "'".$t."'";

	}

	

	foreach($s as $k=>$v){

		$v = implode(', ', $v);

		echo "var $k = [ $v ];\n";

		echo "$('.deg-$k').on('click', function(){

			$('canvas').remove();

			panaroma($k);

		});\n";

	}

?>



	var camera, scene, renderer;
	var texture_placeholder,
	isUserInteracting = false,
	onMouseDownMouseX = 0, onMouseDownMouseY = 0,
	lon = 90, onMouseDownLon = 0,
	lat = 0, onMouseDownLat = 0,
	phi = 0, theta = 0,
	target = new THREE.Vector3();	
	
	function panaroma(img){
		if(img.length > 0){
			init(img);
			animate();
		}
	}

	function init(img) {
		var materials = [];
		var imglen = img.length;
		var i = 0;
		for(i=0;i<imglen;i++){
			console.log(img[i]);
			materials.push(loadTexture( img[i] ));
		}
		var container, mesh;
		container = document.getElementById( 'gdegree' );
		camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1100 );
		scene = new THREE.Scene();
		texture_placeholder = document.createElement( 'canvas' );
		texture_placeholder.width = 128;
		texture_placeholder.height = 128;
		var context = texture_placeholder.getContext( '2d' );
		context.fillStyle = 'rgb( 200, 200, 200 )';
		context.fillRect( 0, 0, texture_placeholder.width, texture_placeholder.height );
		mesh = new THREE.Mesh( new THREE.BoxGeometry( 300, 300, 300, 7, 7, 7 ), new THREE.MultiMaterial( materials ) );
		mesh.scale.x = - 1;
		scene.add( mesh );
		renderer = new THREE.CanvasRenderer();
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( (window.innerWidth * 0.968), 500);
		container.appendChild( renderer.domElement );
		document.addEventListener( 'mousedown', onDocumentMouseDown, false );
		document.addEventListener( 'mousemove', onDocumentMouseMove, false );
		document.addEventListener( 'mouseup', onDocumentMouseUp, false );
		document.addEventListener( 'wheel', onDocumentMouseWheel, false );
		document.addEventListener( 'touchstart', onDocumentTouchStart, false );
		document.addEventListener( 'touchmove', onDocumentTouchMove, false );
		//
		window.addEventListener( 'resize', onWindowResize, false );
	}

	function onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( (window.innerWidth * 0.968), 500);
	}

	function loadTexture( path ) {
		var texture = new THREE.Texture( texture_placeholder );
		var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );
		var image = new Image();
		image.onload = function () {
			texture.image = this;
			texture.needsUpdate = true;
		};
		image.src = path;
		return material;
	}

	function onDocumentMouseDown( event ) {
		event.preventDefault();
		isUserInteracting = true;
		onPointerDownPointerX = event.clientX;
		onPointerDownPointerY = event.clientY;
		onPointerDownLon = lon;
		onPointerDownLat = lat;
	}

	function onDocumentMouseMove( event ) {
		if ( isUserInteracting === true ) {
			lon = ( onPointerDownPointerX - event.clientX ) * 0.1 + onPointerDownLon;
			lat = ( event.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
		}
	}

	function onDocumentMouseUp( event ) {
		isUserInteracting = false;
	}

	function onDocumentMouseWheel( event ) {
		camera.fov += event.deltaY * 0.05;
		camera.updateProjectionMatrix();
	}

	function onDocumentTouchStart( event ) {
		if ( event.touches.length == 1 ) {
			event.preventDefault();
			onPointerDownPointerX = event.touches[ 0 ].pageX;
			onPointerDownPointerY = event.touches[ 0 ].pageY;
			onPointerDownLon = lon;
			onPointerDownLat = lat;
		}
	}

	function onDocumentTouchMove( event ) {
		if ( event.touches.length == 1 ) {
			event.preventDefault();
			lon = ( onPointerDownPointerX - event.touches[0].pageX ) * 0.1 + onPointerDownLon;
			lat = ( event.touches[0].pageY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
		}
	}

	function animate() {
		requestAnimationFrame( animate );
		update();
	}

	function update() {
		if ( isUserInteracting === false ) {
			lon += 0.07;
		}
		lat = Math.max( - 85, Math.min( 85, lat ) );
		phi = THREE.Math.degToRad( 90 - lat );
		theta = THREE.Math.degToRad( lon );
		target.x = 500 * Math.sin( phi ) * Math.cos( theta );
		target.y = 500 * Math.cos( phi );
		target.z = 500 * Math.sin( phi ) * Math.sin( theta );
		camera.lookAt( target );
		renderer.render( scene, camera );
	}
</script>



@stop
