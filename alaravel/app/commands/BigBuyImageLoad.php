<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BigBuyImageLoad extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bigbuy:load_images';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Loads images from Big Buy.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		App::make('BigbuyController')->getCrnupdateproductimages();
	}

}
