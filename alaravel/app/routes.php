<?php

Route::get('/privacy', 'SettingsController@getPrivacy');

Route::get('webshop/privacy', 'SettingsController@getPrivacy');

Route::get('/settings/addprivacy', 'SettingsController@getaddprivacy');

Route::post('/settings/addprivacy', 'SettingsController@postaddprivacy');


Route::get('/newsletter/subscribers', 'NewsletterController@getSubscribers');
Route::get('/newsletter/subscriber/delete/{id}', 'NewsletterController@getDeletesubscriber');
Route::get('/newsletter/get_newsletter_subscribers/{id}', 'NewsletterController@getNsubscribers');

Route::get('/newsletter/send', 'NewsletterController@send_newsletter');

Route::get('/newsletter/send_newsletter_mails', 'NewsletterController@send_newsletter_mails');

Route::get('/newsletters', 'NewsletterController@getNewsletter');
Route::get('/newsletter/add', 'NewsletterController@getAddnewsletter');
Route::post('/newsletter/add', 'NewsletterController@postAddnewsletter');
Route::get('/newsletter/edit/{id}', 'NewsletterController@getEditnewsletter');
Route::post('/newsletter/edit/{id}', 'NewsletterController@postEditnewsletter');
Route::get('/newsletter/view/{id}', 'NewsletterController@getViewnewsletter');
Route::get('/newsletter/delete/{id}', 'NewsletterController@getDeletenewsletter');


Route::get('/settings/emailtemplates', 'SettingsController@getEmailtemplates');
Route::get('/settings/addemailtemplate', 'SettingsController@getAddemailtemplate');
Route::post('/settings/addemailtemplate', 'SettingsController@postAddemailtemplate');
Route::get('/settings/editemailtemplate/{id}', 'SettingsController@getEditemailtemplate');
Route::post('/settings/editemailtemplate/{id}', 'SettingsController@postEditemailtemplate');
Route::get('/settings/deleteemailtemplate/{id}', 'SettingsController@getDeleteemailtemplate');
Route::get('/settings/viewemailtemplate/{id}', 'SettingsController@getViewemailtemplate');

Route::get('/settings/listpages', 'SettingsController@getListpages');
Route::get('/settings/addpage', 'SettingsController@getAddpage');
Route::post('/settings/addpage', 'SettingsController@postAddpage');
Route::get('/settings/deletepage/{id}', 'SettingsController@getDeletepage');
Route::get('/settings/editpage/{id}', 'SettingsController@getEditpage');
Route::post('/settings/editpage/{id}', 'SettingsController@postEditpage');

Route::controller('/products','ProductsController');

Route::get('webshop/my-account', 'WebshopController@getMyAccount');
Route::get('webshop/contact-shoppings', 'WebshopController@getContactShopping');
Route::post('webshop/contact-shoppings', 'WebshopController@postContactShopping');
Route::get('forgotpassword/resetpassword/{email}/{code}', 'ForgotPasswordController@resetpassword');
Route::get('customer/orderdetail/{id}', 'CustomerController@orderdetail');
Route::get('checkout/getConfirm/{id}', 'CheckoutController@getConfirm');
Route::get('checkout/postConfirm/{id}', 'CheckoutController@postConfirm');

Route::controller('/webshop','WebshopController');
Route::controller('/checkout','CheckoutController');
Route::controller('/customer','CustomerController');
Route::controller('/forgotpassword','ForgotPasswordController');
Route::controller('/bigbuy','BigbuyController');
//Route::post('/checkout/Ajaxlogin','CheckoutController@Ajaxlogin');
Route::get('/postPayment/{total}', 'ConnectmollieController@startTransaction');
//Route::get('postPayment/{total}', 'connectController@startTransaction');
Route::get('/indivsend', 'CheckoutController@indivsend');

Route::controller('/','PropertyController');










