function isScrolledIntoView(el){

var elemTop = el.getBoundingClientRect().top;
var elemBottom = el.getBoundingClientRect().bottom;

var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);

return isVisible;

}

function dnone(){

$('#modal-main-plan').css('display','none');
$('#modal-main-register').css('display','none');
$('#modal-main-confirm').css('display','none');
$('#modal-main-login').css('display','none');
$('#modal-main-lost-password').css('display','none');
$('#modal-main-confirm-password').css('display','none');
$('#gpc-modal-message').html('');
$('#gpc-modal-message').css('padding','0px');
$('.form-control').val('');
}

(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

$(document).ready(function(){

$('#link-confirm-password').on('click',function(e){

e.preventDefault();

var url		= $('#form-confirm-password').attr('action');
var user	= $('#confirm-password-user').val();
var email	= $('#confirm-password-email').val();
var ccode	= $('#confirm-password-ccode').val();
var password	= $('#confirm-password-password').val();
var cpassword = $('#confirm-password-cpassword').val();

$.post(url, {user:user,email:email,ccode:ccode,password:password,cpassword:cpassword}).done(function(d){

var message 	= d.message;
var url		= d.url;
var success	= d.success;

if(success == '0'){

$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px');
$('#gpc-modal-message').html(message);
}
else if(success == '1'){
dnone();
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px');
$('#gpc-modal-message').css('transition','all 1s ease-in');
$('#gpc-modal-message').css('background','rgba(255,0,0,0.4)');
$('#gpc-modal-message').html(message);
$('#gpc-modal-message').css('background','#f9f9f9');
$('#modal-main-login').css('display','block');
}
else if(success == '2'){
}
else{
window.location = '//laravel.inviolateinfotech.com/';
}

}).fail(function(xhr,status,error){

alert(status+' '+error);

});

});


$('#login-lost-password').on('click',function(e){

e.preventDefault();

dnone();
$('#gpc-modal-label').html('Change Password');
$('#modal-main-lost-password').css('display','block');

});


$('#link-lost-password').on('click',function(e){

e.preventDefault();

var url = $('#form-lost-password').attr('action');
var user = $('#lost-password-user').val();
var email = $('#lost-password-email').val();

$.post(url, {user:user,email:email}).done(function(d){

var message 	= d.message;
var url		= d.url;
var success	= d.success;
var errors	= d.errors;

if(success == '0'){

$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px 10px');
for(i in errors){
var html = $('#gpc-modal-message').html();
html += '<br />'+errors[i];
$('#gpc-modal-message').html(html);
}
}
else if(success == '1'){
dnone();
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px');
$('#gpc-modal-message').css('transition','all 1s ease-in');
$('#gpc-modal-message').css('background','rgba(255,0,0,0.4)');
$('#gpc-modal-message').html(message);
$('#gpc-modal-message').css('background','#f9f9f9');
$('#modal-main-confirm-password').css('display','block');
}
else if(success == '2'){
}
else{
window.location = '//laravel.inviolateinfotech.com/';
}

}).fail(function(xhr,status,error){

alert(status+' '+error);

});

});


$('#link-login').on('click',function(e){

e.preventDefault();

var url = $('#form-login').attr('action');
var user = $('#login-user').val();
var password = $('#login-password').val();
var remember = $('#login-remember').is(':checked');

if(remember){remember = 1;}else{remember=0;}

$.post(url, {username:user,password:password,remember:remember}).done(function(d){

var message	= d.message;
var url		= d.url;
var success	= d.success;
var errors	= d.errors;


if(success == '0'){
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px 10px');
$('#gpc-modal-message').html(message);
}
else if(success == '1'){
window.location = url;
}
else if(success == '2'){
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px 10px');
$('#gpc-modal-message').html(message);
$('#modal-main-login').css('display','none');
$('#gpc-modal-label').html('CONFIRM USER');
$('#modal-main-confirm').css('display','block');
}
else{
window.location = '//laravel.inviolateinfotech.com/';
}

});

});

$('#link-register').on('click',function(e){

e.preventDefault();

var url = $('#form-register').attr('action');
var username = $('#register-username').val();
var firstname = $('#register-firstname').val();
var lastname = $('#register-lastname').val();
var password = $('#register-password').val();
var password_confirmation = $('#register-cpassword').val();
var email = $('#register-email').val();
var user_type = $('.register-user-type');

$.post(url, {username:username,first_name:firstname,last_name:lastname,password:password,password_confirmation:password_confirmation,email:email}).done(function(d){

var message 	= d.message;
var url		= d.url;
var success	= d.success;
var errors	= d.errors;

if((success == '0') || (success == '1')){$('#gpc-modal-message').css('display','block');}

if(success == '0'){

$('#gpc-modal-message').css('padding','5px 10px');
$('#gpc-modal-message').html(message);

for(i in errors){
var html = $('#gpc-modal-message').html();
html += '<br />'+errors[i];
$('#gpc-modal-message').html(html);
}
$('#mbody').animate({scrollTop:0},"fast");
}
else if(success == '1'){
$('#gpc-modal-message').html(message);
$('#mbody').animate({scrollTop:0},"fast");
$('#modal-main-register').css('display','none');
$('#gpc-modal-label').html('LOGIN');
$('#modal-main-login').css('display','block');
}
else{window.location = '//laravel.inviolateinfotech.com/confirmuser';}

});

});

$('#link-confirm').on('click',function(e){

e.preventDefault();

var url = $('#form-confirm').attr('action');

var user = $('#confirm-user').val();

var password = $('#confirm-password').val();

var ccode = $('#confirm-code').val();

$.post(url, {user:user,password:password,ccode:ccode}).done(function(d){

var message = d.message;
var success	= d.success;

if(success == '0'){
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px 10px');
$('#gpc-modal-message').html(message);
$('#mbody').animate({scrollTop:0},"fast");
}

if(success == '1'){
window.location = '//laravel.inviolateinfotech.com/';
}


if(success == '2'){
$('#gpc-modal-message').css('display','block');
$('#gpc-modal-message').css('padding','5px 10px');
$('#gpc-modal-message').html('');
$('#gpc-modal-message').html(message);
$('#mbody').animate({scrollTop:0},"fast");
}

});

});

$('#login').on('click',function(e){

e.preventDefault();

dnone();

$('#gpc-modal-message').css('display','none');

$('#gpc-modal-label').html('LOGIN');

$('#modal-main-login').css('display','block');

$('#gpc-modal').modal('show');

});

$('#plans').on('click',function(e){

e.preventDefault();

dnone();

$('#gpc-modal-message').css('display','none');

$('#gpc-modal-label').html('PLANS');

$('#modal-main-plan').css('display','block');

$('#gpc-modal').modal('show');


});

$('.plan-register').on('click',function(e){

e.preventDefault();

dnone();

$('#gpc-modal-message').css('display','none');

$('#gpc-modal-label').html('REGISTER');

$('#modal-main-register').css('display','block');

$('#gpc-modal').modal('show');

$('#mbody').animate({scrollTop:0},"fast");

});
$('.side-menu-link').on('click',function(){

$('.side-menu').toggleClass('unhighlight');

$(this).hide();

});

$('.side-menu-close').on('click',function(){

$('.side-menu').toggleClass('unhighlight');

$('.side-menu-link').show();

});






$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).text().toLowerCase();
  $('#own_type').val(target);
});


$('#own_type').val('');





$(window).scroll(function(){
      if ($(document).scrollTop() > 140) {
          $('#fixed-search-bar').css('position','fixed').css('top','0px').css('background','rgba(0,0,0,0.82)');
      } else {
          $('#fixed-search-bar').css('position','relative').css('background','rgba(0,0,0,0.7)');
      }
  });
  

$('#gpc-chat-close').on('click',function(){ $('.gpc-chat-box').css('display','none'); });

$('.chat-message-link').on('click',function(){ 
	$('#gpc-chat-username').html($('#profile-username').html());
	$('.gpc-chat-box').css('display','block'); 
});


$('#gpc-chat-minimize').on('click',function(){ 
	
	console.log($('#gpc-chat-ce').attr('class'));
	
	if($('#gpc-chat-ce').attr('class') == "fa fa-compress"){
	$('.gpc-chat-box').css('height','40px');
	$('#gpc-chat-ce').removeClass('fa fa-compress');
	$('#gpc-chat-ce').addClass('fa fa-expand');
	
	}
	else{
	$('.gpc-chat-box').css('height','300px');
	$('#gpc-chat-ce').removeClass('fa fa-expand');
	$('#gpc-chat-ce').addClass('fa fa-compress');
		
	}
	
});

$('.gpc-chat-textarea').on('keyup',function(e){ 
	if(e.which == 13){
	var val = $(this).val(); 
	var html = '<section class="gpc-chat-message pull-right"><aside class="gpc-chat-user-message ">'+val+'</section></section>';
	$('.gpc-chat-body').append(html);
	var wtf    = $('.gpc-chat-body');
	var height = wtf[0].scrollHeight;
	wtf.scrollTop(height);
	$(this).val('');
	
	send_ajax_message($('#url').val(), val, $('#to').val(), $('#from').val());
	
	}
});




});


function send_ajax_message(url, message, to, from){
	
	$.post(url, {message:message, to:to, from:from}).done(function(d){
		alert(d);
	});
	
}


$(window).scroll(function(event) {
  
  $(".list-mai").each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } 
  });
  
});



/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
$(document).ready(function(){
	var input = $('#ifile-id');
	var label = $('#ifile-label');
		input.on( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.html(fileName);
		});

		// Firefox bug fix
		input.on( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.on( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
