/************* Template Init Js File ************************
    Template Name: Wakfi - App Landing HTML5 Page
    Author: Themescare
    Version: 1.0
    Copyright 2018
*************************************************************/


$(function () {

	"use strict";
	var wind = $(window);

	/*==================================
	ScrollIt Setup
	====================================*/

	

	/*==================================
	Navbar Scrolling Setup
	====================================*/
	
	wind.on("scroll", function () { 
		var swidth = (screen.width);
		var bodyScroll = wind.scrollTop(),
		header = $(".header")
		if(swidth >1024){
			if (bodyScroll > 400) {
				
				header.addClass("fixed-top");
				$("#menu-toggle").css('display','block');
				$("ul.side_nav").css('display','none');
				
				
			} 
			else {
				header.removeClass("fixed-top");
				$("#menu-toggle").css('display','none');
				$("ul.side_nav").css('display','block');
			}
		}
	});
	
	

});

