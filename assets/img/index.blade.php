@include('webshop.header')
    <section class="side_bar">
      <div class="container-fluid">
        <div class="row">
		
         @include('webshop.sidebar')
		
          <div class="col-sm-9 col-lg-9">
            <section class="slider">
              <div class="">
                <h2>Carousel Example</h2>
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
				   <?php 
				$i=0;
	foreach($catdata as $key=>$val){
	 foreach($val['getbanner'] as $key=>$value){
		 $class='';
	if($i==0){
		$class='active';
	}
		?>
	<div class="item {{$class}}" id="img_ban"> <img src="{{url('assets/img/')}}/{{$value->image}}" alt="Los Angeles" style="width:100%;" class="img_acr"> </div>
		
		<?php
		$i++;
	 }
	}
		 ?>
                    <!--<div class="item active"> <img src="{{url('assets/img/ZARA_slider.jpg')}}" alt="Los Angeles" style="width:100%;"> </div>
                    <div class="item"> <img src="{{url('assets/img/ZARA_slider.jpg')}}" alt="Los Angeles" style="width:100%;"> </div>
                    <div class="item"> <img src="{{url('assets/img/ZARA_slider.jpg')}}" alt="Los Angeles" style="width:100%;"> </div>-->
                  </div>
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a>
                </div>
              </div>
            </section>
            <!---end class slider here--->
			<div class="row">
            <div class="col-lg-12">
				<div class="footer-spot">
				
				</div>
              <div class="col-lg-2"> </div>
              <div class="col-lg-8 ">
                <div class="group ">
					<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
					<label style="text-align:center;width:100%;">Join our newsletter - Enter your email address*</label>
				</div>
				<div class="group" style="float: left;width: 100%;">
				    <input class="check-content" type="checkbox" style="margin-left:180px;">
				    <label style="margin-left: 20px;text-align:center;width:100%;" class="check-button">I have read and accept the Privacy policy.</label>
				</div>
				<button type="button" class="button" style="width:30%;margin:20px auto;display:inherit;">SUBSCRIBE ME</button>
              </div>
              <div class="col-lg-2"> </div>
            </div>
            <div class="col-lg-12">
              <div class="contact_social">
            
    <div class="col-sm-4"> 
<div class="shopping_f">
                    <h6 class="folloe">Payment Methods</h6>
                    <ul class="rhv" style="display: inline-block;">
                        <li><img src="{{url('assets/img/payment/credit.jpg')}}"></li>
                      <li><img src="{{url('assets/img/payment/visa_logo.jpg')}}"></li>
                       
                    </ul>
                  </div>
	</div>
	
		  
				  
    <div class="col-sm-4">
	<div class="shopping_f">
                    <h6 class="folloe">Follow us on</h6>
                    <ul class="rhv">
                      <li><img src="{{url('assets/img/facebook.png')}}"></li>
                      <li><img src="{{url('assets/img/instagram.png')}}"></li>
                      <li><img src="{{url('assets/img/pinterest.png')}}"></li>
                      <li><img src="{{url('assets/img/twitter.png')}}"></li>
                      <li><img src="{{url('assets/img/youtube.png')}}"></li>
                    </ul>
                  </div>
				  </div>
     <div class="col-sm-3">
	 <div class="shopping_fd">
                    <h6 class="folloe">Download our app</h6>
                    <ul class="rhvz">
                      <li><img src="{{url('assets/img/facebook.png')}}"></li>
                      <li><img src="{{url('assets/img/instagram.png')}}"></li>
                    </ul>
                  </div></div>
  </div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
              
				
				   
              </div>
            </div>
            <div class="col-lg-12">
              <div class="contact_formf_g">
                <div class="col-lg-3">
                  <div class="guide_f">
                    <h5 class="gucidr_gjh">Shopping guide</h5>
                    <ul class="contactd">
                      <li><a href="#">Faqs</a></li>
                      <li><a href="#">Purchasing process</a></li>
                      <li><a href="#">Payment</a></li>
                      <li><a href="#">DELIVERY</a></li>
                      <li><a href="#">Returns</a></li>
                      <li><a href="#">Withdrawal</a></li>
                      <li><a href="#">Technology</a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="guide_f">
                    <h5 class="gucidr_gjh">Shopping guide</h5>
                    <ul class="contactd">
                      <li><a href="#">Conditions of purchase</a></li>
                      <li><a href="#">Privacy policy</a></li>
                      <li><a href="#">Cookies policy</a></li>
                      <li><a href="#">Cancellation Form</a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="guide_f">
                    <h5 class="gucidr_gjh">Shopping guide</h5>
                    <ul class="contactd">
                      <li><a href="#">Work with us</a></li>
                      <li><a href="#">Press</a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="guide_f">
                    <h5 class="gucidr_gjh">Shopping guide</h5>
                    <ul class="contactd">
                      <li><a href="#">Contact form</a></li>
                      <li><a href="#">Stores</a></li>
                      <li><a href="#">0800 020 1115</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
			</div>
          </div>
        </div>
      </div>
    </section>
	
	
	
<style>
.error{
	color:red;
}
.modal-header{
	border-bottom:none!important;
	padding:10px !important;
}
.btn.btn-primary.black-background.white {
  background: #ffff none repeat scroll 0 0;
  border: 1px solid #000;
  border-radius: 3px;
  color: #000;
}
.control-group.check {
  font-size: 14px !important;
  font-weight: initial;
  text-decoration: none;
}
.control-group.check >a{
	color:#ccc;
}
.text-line {
    background-color: transparent !important;
    color: #eeeeee !important;
    outline: none !important;
    outline-style: none !important;
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #000 !important;
    padding: 3px 10px!important;
	border-radius:0px !important;
	box-shadow:none !important;
	width:47%!important;
	
}
@media only screen  and (max-width: 360px){
label{
	display:flex;
}
}
</style>
	
	
<div id="my-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><b>NEWSLETTER SUBSCRIPTION</b></h4><br />
				<h6 class="modal-title">Subscribe to our newsletter and receive e-mails all the latest Zara Home products and offers</h6>
            </div>
			
            <div class="modal-body">
			<form name=""  method="POST">
			<div class="control-group">
			<label for ="control-label">E-mail<span class="error_req error">*</span></label>
			
              <div class="controls"> <input type="text" name="email"  id="eml_fld" class="form-control text-line" ></div>
			   </div>
			   <br>
		<div class="control-group check">
               <input type="checkbox" name="checkbox"  value="1" > I have read and accept the <a href="#" class="policy">Privacy Policy</a>
			  </div> 
			  
			  <br><br>
			  <div class="control-group">
			  <input type="submit" name="submit" class="btn btn-primary black-background white" Value="SUBSCRIBE ME">
			  </div>
			   </form>
            </div>
        </div>
    </div> 
</div>

	
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
$(document).ready(function(){
	 var visited = $.cookie("visited");
	  if (visited == null) {

			$('#my-modal').modal('show');
			
			var date = new Date();
			var minutes = 15;
			date.setTime(date.getTime() + (minutes * 60 * 1000));
			
			$.cookie('visited', 'yes', { expires: date, path: '/' });
	  }
});

</script>
	
	
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			 $.ajax({
				 url:'http://inviolateinfotech.com/laravel/webshop/wgbanner',
				 type:'GET',
				 data: 'catid='+id,
				 success: function(result) {
					$('.carousel-inner').html(result);
				 }
				 
				 
			 });
		 }
		 
		 
		  </script>
  </body>

</html>
