<!doctype html>
<html>
	<head>
		<title>Webshop</title>
		<link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=greek" rel="stylesheet">
		<!--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.css')}}">-->
		<!--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">-->
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{url('assets/css/base.css')}}">
		<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			.basket_mini_info{
				position: absolute;
				margin-top: 5%;
				z-index: 1000000;
				width:85%;
			}
			.mini_info{
				width:100%;
				background:red;
				height: 500px;	
			}
			
					
			.loader-container{
				position:absolute;
				background:rgba(0,0,0,0.5);
				width:100%;
				height:100%;
			}
						
			.loader {    
					color: #616161;
					font-size: 50px;
					text-indent: -9999em;
					overflow: hidden;
					width: 1em;
					height: 1em;
					border-radius: 50%;
					margin: 18% auto;
					position: absolute;
					-webkit-transform: translateZ(0);
					-ms-transform: translateZ(0);
					transform: translateZ(0);
					-webkit-animation: load6 1.7s infinite ease, round 1.7s infinite ease;
					animation: load6 1.7s infinite ease, round 1.7s infinite ease;
					left: 50%;
					z-index: 1000000;
			}
			@-webkit-keyframes load6 {
			  0% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			  5%,
			  95% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			  10%,
			  59% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
			  }
			  20% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
			  }
			  38% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
			  }
			  100% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			}
			@keyframes load6 {
			  0% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			  5%,
			  95% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			  10%,
			  59% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
			  }
			  20% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
			  }
			  38% {
				box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
			  }
			  100% {
				box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
			  }
			}
			@-webkit-keyframes round {
			  0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			  }
			  100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
			  }
			}
			@keyframes round {
			  0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			  }
			  100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
			  }
			}


			
			
			
		</style>
	</head>

<body>
<!--------------------------------Header page Start -------------------------->
	<div class="loader" style="display:none;">Loading</div>
	
	<section class="header_main">
	<div class="container-fluid">
		<div class="row">
			<div class="header_zoras">
				<div class="col-lg-1 col-md-2 col-sm-1 col-xs-2 maxon420">
					<div class="logo_zora clearfix ">
						<a title="go back to homepage, press logo" href="{{url('/')}}"><img src="{{url('packages/realestate/site/logo.png')}}"></a>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-8 maxon420"> 
					<a style="margin-top:30% !important;"title="go back to homepage, press logo" href="{{url('/')}}">
						<span >Go back to homepage, press logo</span> 
					</a>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 pull-right">
					<div class="log_sign">
						<ul class="lofgo_sr">
							@if(Auth::check())
							<li>
								<a href="{{url('webshop/logout')}}"><img src="{{url('assets/img/account.png')}}">Log Out</a>
							</li>
							@else
							<li>
								<a href="{{url('webshop/login')}}"><img src="{{url('assets/img/account.png')}}">Log In</a>
							</li>
							@endif
							<li>
								<a href="{{url('webshop/basket')}}"   ><img src="{{url('assets/img/cart.png')}}">Basket (<span id="total_items"><?php if(isset($_SESSION['name'])){echo count($_SESSION['name']);}else{ echo 0; } ?></span>)  </a><div id="mycart"></div>
							</li>
						</ul>
					</div>
				<div class="input-group search_um">
					<input type="text" class="form-control SearchBar" placeholder="Search for...">
					<span class="input-group-btn"   style="border-radius: 1px;border: 1px solid #ccc;">			
						<button class="btn btn-defaul SearchButton" type="button">				
							<span class=" glyphicon glyphicon-search SearchIcon" ></span> 
						</button>
					</span>
				</div>
						<!--<div class="basket_mini_info">
							<div class="mini_info">
								hello
							</div>
						</div>-->
				</div>
			</div>
		</div>
	</div>
</section>

