<style>

.product-inner{
	
    padding: -18px 0 20px 0 !important;
}


</style>
@include('webshop.header')
    <section class="side_bar">
       		<div class="container-fluid">
    			<div class="row">
				  @include('webshop.sidebar')
    				<div class="col-sm-9 col-md-9  col-lg-9">
					<div class="col-sm-9 col-md-9  col-lg-9">
					<nav class="navbar navbar-default">

				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				  </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				  <ul class="nav navbar-nav">
					<li class="dropdown text-uppercase">
					  <a class="dropdown-toggle" data-toggle="dropdown" href="#">dimension<span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<form  class="scroll">
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 35 x 50 cm</a></label>
							</div>
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 x 225 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 120 x 120 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 170 x  225 cm</a></label>
							</div>	
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 120 x  200 cm</a></label>
							</div>
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 100 x  145 cm</a></label>
							</div>			
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 120 x  160 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 45 x  45 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 160 x  250 cm</a></label>
							</div>			
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 x  250 cm</a></label>
							</div>			
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 170 x  350 cm</a></label>
							</div>	
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 120 x  300 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 x  350 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 180 cm</a></label>
							</div>			
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 x  150 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 145.0 x  145.0 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 250 x 250 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 x 300.0 cm</a></label>
							</div>			
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 150 cm</a></label>
							</div>		
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> 165.0 cm</a></label>
							</div>
						 </form>
					  </ul>
					</li>  
					<li class="dropdown text-uppercase">
					  <a class="dropdown-toggle" data-toggle="dropdown" href="#">composition<span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<form>
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> cotton</a></label>
							</div>                                   
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> linen</a></label>
							</div>		                          
							<div class="checkbox">                
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> ramie</a></label>
							</div>		                             
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> polyvinyl chloride</a></label>
							</div>	
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> polyester</a></label>
							</div>
						 </form>
					  </ul>
					</li> 
					<li class="dropdown text-uppercase">
					  <a class="dropdown-toggle" data-toggle="dropdown" href="#">colour<span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<form class="scroll">
							<div class="checkbox">
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> yellow</a></label>
							</div>                                   
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> blue</a></label>
							</div>		                          
							<div class="checkbox">                
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> white</a></label>
							</div>		                             
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> grey</a></label>
							</div>	
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> brown</a></label>
							</div>				
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> natural</a></label>
							</div>			
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> red</a></label>
							</div>				
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> pink</a></label>
							</div>	
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> salmon</a></label>
							</div>	
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> green</a></label>
							</div>		
							<div class="checkbox">                   
							  <label class="lbl_set"><input type="checkbox" value=""><span class="checkmark"></span><a href="#"> others</a></label>
							</div>
						 </form>
					  </ul>
					</li>   
					<p class="navbar-text text-uppercase">price : <a href="#">highest</a> | <a href="#">lowest</a></p>			
					<button class="btn btn-default navbar-btn text-uppercase"><a href="#">clear</a></button>
				  </ul>
				</div>


</nav></div>

	<div class="col-lg-3  col-md-3 col-sm-3 nomrgpad">
    								<div class="filter-right">
	    								<h3>142 items <span>|</span> display</h3>
	    								<nav aria-label="Page navigation">
                                             <ul class="pagination">
                                                <li><a href="index2.html">6</a></li>
                                                <li><a href="index.html">4</a></li>
                                                <li><a href="index1.html">3</a></li>
                                            </ul>
                                        </nav>
									</div>
    							</div>
					
					<div class="gap20"></div>
	
    					<div class="product-inner">
    						<div class="row">
							
							@foreach($products as $product)
    							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 maxon420">
    								<a href="{{url('webshop/product')}}/{{$slug}}/{{ $product->id}}">
    									<img src="{{asset($product->img)}}" alt="{{$product->name}}" class="img-responsive">
									</a>
    								<div class="product-content">
    									<h2><a href="{{url('webshop/categorydetail')}}/{{ $product->id}}">
											{{$product->name}}
    									</a></h2>
    									<p>{{$product->price}}</p>
    								</div>
    							</div>
							@endforeach
							
							<?php if(empty($products)){echo 'No products available.';}?>
							
							
    						</div>
							
    					</div>
						
    			
				
				
			<form method="POST" action="{{url('webshop/subscribe')}}">					
				<div class="row">
				<div class="col-lg-12">
					<div class="footer-spot"></div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-1">
							<div class="group">
								<input class="form_input" type="email" name="email" ><span class="highlight"></span><span class="bar"></span>
								<label style="text-align:center;width:100%;" class="labletxt">Join our newsletter - Enter your email address*</label>
							</div>
				 <!--<div class="group" style="float: left;width: 100%; text-align:center;">
								<input class="check-content" type="checkbox" >
								<label style="margin-left: 20px;" class="check-button">I have read and accept the Privacy policy.</label>
							</div>-->
						<div class="col-lg-12  text-center " >
				  <input  type="submit" class="button" style=" float:none;" name="subscribe" value="SUBSCRIBE ME">
						</div>
						</div>
						
				</div>
				</div>	   	
			</form> 
                    </div>
		        </div>
            </div>
	</section>
@include('webshop.footer')


	
	
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			 $.ajax({
				 url:'http://inviolateinfotech.com/laravel/webshop/wgbanner',
				 type:'GET',
				 data: 'catid='+id,
				 success: function(result) {
					$('.carousel-inner').html(result);
				 }
				 
				 
			 });
		 }
		 
		 
		  </script>
		  
		  
		  
		  
		  
		  
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
          $(function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 0,
              max: 280,
              values: [ 0, 280 ],
              slide: function( event, ui ) {
                $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
              }
            });
            $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
             " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          });
        </script>
		
		
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script>  
		 function click_func(id){
			 $(".sale_now").hide();
			 $(".test-"+id).show(); 
			
		 }
		 
		 
		  </script>
		  
		  
		  
		  
		  
		  
		  
		  
  </body>

</html>

		  
		  
		  
		  
		  
		  
	
