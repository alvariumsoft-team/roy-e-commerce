  jQuery(document).ready(function(){
  window.setTimeout(function(){
      $(".trengo-vue-iframe").contents().find(".material-icons").css("display","none");

  }, 1000);

    $('.account-list').on('click', function(){
      $('.account-list').css('display', 'none');
      $($(this).attr('data-id')).css('display', 'block');
    });

    if($('.idealslider').length > 0){
      var iss = {};
      var is = $('.idealslider');
      for(i in is){
        if(i <= is.length){
          console.log(i);
          var slider = new IdealImageSlider.Slider('#idealslider'+i);
          iss['#idealslider'+i] = slider;
        }
        //slider.start();
      }
      $(document).on('click', '.iscolor', function(){
        //console.log([$(this).parents('.swatches').attr('data-id')]);
        //console.log(iss);
        $swatches = $(this).parents('.swatches');
        var id = $(this).attr('data-id');
        jQuery.ajax({
          method: "POST",
          data:{

          },
          dataType: "json",
          url: baseurl+"/colorvariation/"+id,
          success:function(response) {
            if((response.success != undefined) && (response.success == true)){
              var slii = $swatches.attr('data-id');
              iss[slii].destroy();
              var url = $swatches.parents('.maxon420').find('.idealslider').find("a").attr("href");
              var html = '';
              $swatches.parents('.maxon420').find('.idealslider').html(html);
              for(i in response.images){
                html += '<a href="'+url+'"><img data-src-2x="'+response.images[i]+'" src="'+response.images[i]+'" alt="" class="img-" /></a>';

                  }
                  $swatches.parents('.maxon420').find('.idealslider').html(html);
              var slider = new IdealImageSlider.Slider(slii);
              iss[slii] = slider;
            }
          }
          });
      });
    }



    $(".have_promo_code").on('click', function(){
      $("#promo_code_modal").modal('show');
    });
    jQuery(".quantity_remove").on('click', function(){
		var id = jQuery(this).attr('data-id');
		var pquant = parseInt(jQuery(this).parents('.bskt_list').find('.prod_quant').val());
		var q = parseInt(jQuery(this).parents('.bskt_list').find('.product_quantity'+id).val());
		var p1 = parseFloat(jQuery(this).parents('.bskt_list').find('.product_price'+id).val());
		var tax = parseFloat(jQuery(this).parents('.bskt_list').find('.product_tax'+id).val());
		if(q==1){
			return false;
		}
		if(isNaN(q)){q=0;}
		if(isNaN(p1)){p1=0;}
		p0 = parseFloat(p1*q);
		tax0=parseFloat((p0*tax/100));
		if((q-1)>pquant){
			alert('Maximum available stock for this product has been added in to cart.');
			q=pquant;
		}
		else{
			q = q-1;
		}
		if(q < 0){q = 0;}
		jQuery(this).parents('.bskt_list').find('.product_quantity'+id).val(q);
		p = p1*q;
		jQuery(this).parents('.basket_parent').find('.final-price-single'+id).html(currency + parseFloat(p).toFixed(2));
		var finalSingleTotal = (p * tax)/100;
		var totalsingle= (p+finalSingleTotal);
		jQuery(this).parents('.basket_parent').find('.final-price-single-tax'+id).html(currency + parseFloat(totalsingle).toFixed(2));
		jQuery(this).parents('.basket_parent').find('.total-single-tax'+id).html(currency + parseFloat(finalSingleTotal).toFixed(2));
		var t = parseFloat(jQuery('.product-total').val());
		var ttax = parseFloat(jQuery('.product-tax-total').val());
		if(isNaN(t)){t=0;}
		t = t-p0;
		if(t < 0){t = 0;}
		p=parseFloat(p);
		t = t+p;
		jQuery('.product-total').val(parseFloat(t).toFixed(2));
		jQuery('.final-price').html(currency + parseFloat(t).toFixed(2));
		var finalGrandTotal = parseFloat((ttax - tax0 + parseFloat(finalSingleTotal)));
		jQuery('.product-tax-total').val(parseFloat(finalGrandTotal).toFixed(2));
        jQuery('.total-tax').html(currency + parseFloat(finalGrandTotal).toFixed(2));
        var finalPriceIncTax = parseFloat(finalGrandTotal+parseFloat(t));
        jQuery('.final-price-tax').html(currency + parseFloat(finalPriceIncTax).toFixed(2));
		var ti = parseInt(jQuery('.total-items-val').val());
		if(isNaN(ti)){ti=0;}
		if((q-1)>pquant){
			ti = ti;
		}
		else{
			ti = ti-1;
		}
		jQuery('.total-items-val').val(ti);
		jQuery('#total_items').html(ti);
		jQuery('.total-items').html('('+ti+' items)');
		jQuery.ajax({
			method: "GET",
			data:{
			  quantity:q
			},
			dataType: "json",
			url: baseurl+"/updatecartproduct/"+id,
			success:function(response) {
			}
        });
	});

    jQuery(".quantity_add").on('click', function(){
		var id = jQuery(this).attr('data-id');
		console.log(id);
		var pquant = parseInt(jQuery(this).parents('.bskt_list').find('.prod_quant').val());
		var q = parseInt(jQuery(this).parents('.bskt_list').find('.product_quantity'+id).val());
		var p1 = parseFloat(jQuery(this).parents('.bskt_list').find('.product_price'+id).val());
		var tax = parseFloat(jQuery(this).parents('.bskt_list').find('.product_tax'+id).val());
		if(isNaN(q)){q=0;}
		if(isNaN(p1)){p1=0;}
		p0 = parseFloat(p1*q);
		tax0=parseFloat((p0*tax/100));
		if((q+1)>pquant){
			alert('Maximum available stock for this product has been added in to cart.');
			q=pquant;
		}
		else{
			q = q+1;
		}
		if(q < 0){q = 0;}
		jQuery(this).parents('.bskt_list').find('.product_quantity'+id).val(q);
		p = p1*q;
		jQuery(this).parents('.basket_parent').find('.final-price-single'+id).html(currency + parseFloat(p).toFixed(2));
		var finalSingleTotal = (p * tax)/100;
		var totalsingle= (p+finalSingleTotal);
		jQuery(this).parents('.basket_parent').find('.final-price-single-tax'+id).html(currency + parseFloat(totalsingle).toFixed(2));
		jQuery(this).parents('.basket_parent').find('.total-single-tax'+id).html(currency + parseFloat(finalSingleTotal).toFixed(2));

		var t = parseFloat((jQuery('.product-total').val()));
		var ttax = parseFloat(jQuery('.product-tax-total').val());
		if(isNaN(t)){t=0;}
		t = t-p0;
		if(t < 0){t = 0;}
		p=parseFloat(p);
		t = t+p;
		jQuery('.product-total').val(parseFloat(t).toFixed(2));
		jQuery('.final-price').html(currency + parseFloat(t).toFixed(2));
		var finalGrandTotal = parseFloat((ttax - tax0 + parseFloat(finalSingleTotal)));
		jQuery('.product-tax-total').val(parseFloat(finalGrandTotal).toFixed(2));
        jQuery('.total-tax').html(currency + parseFloat(finalGrandTotal).toFixed(2));
        var finalPriceIncTax = parseFloat(finalGrandTotal+parseFloat(t));
        jQuery('.final-price-tax').html(currency + parseFloat(finalPriceIncTax).toFixed(2));
		var ti = parseInt(jQuery('.total-items-val').val());
		if(isNaN(ti)){ti=0;}
		if((q+1)>pquant){
			ti = ti;
		}
		else{
			ti = ti+1;
		}
		jQuery('.total-items-val').val(ti);

		jQuery('#total_items').html(ti);
		jQuery('.total-items').html('('+ti+' items)');
		jQuery.ajax({
			method: "GET",
			data:{
			  quantity:q
			},
			dataType: "json",
			url: baseurl+"/updatecartproduct/"+id,
			success:function(response) {

			}
        });
	});

    jQuery(".bskt_remove_product").on('click', function(){
      jQuery(this).parents('.basket_parent').hide(1000);
      var id = jQuery(this).attr('data-id');
      jQuery.ajax({
        method: "GET",
        data:{
        },
        dataType: "json",
        url: baseurl+"/deletecartproduct/"+id,
        success:function(response) {
          window.location = baseurl+"/basket";
        }
        });
    });
    jQuery(".bskt_empty").on('click', function(){
      jQuery.ajax({
        method: "GET",
        data:{
        },
        dataType: "json",
        url: baseurl+"/emptycart",
        success:function(response) {
          window.location = baseurl+"/basket";
        }
        });
    });





      jQuery.ajax({
        method: "POST",
        data:{

        },
        dataType: "json",
        url: baseurl+'/cart',
        success:function(response) {
          //$("#total_items").text(response.len);

        }
        });


    jQuery(".sale_nowfg").on('click', function(){
      if(!jQuery(this).next().hasClass('shown')){
        setTimeout(function () {
          jQuery(".loader").css('display', 'block');
        }, 2500);
        jQuery(".loader").css('display', 'block');
        jQuery(".sale_nowfg").next().html('');
        $catid = jQuery(this).attr("data-id");
        $this = jQuery(this);
        jQuery.ajax({
          url:baseurl+"/categorydata/",
          type:'POST',
          data: 'catid='+$catid,
          success: function(result) {
            result = JSON.parse(result);
            if(!jQuery.isEmptyObject(result.childrens)){
              var html = '';
              for(i in result.childrens){
                html += '<li><a href="'+baseurl+'/categories/'+result.childrens[i].slug+'">'+result.childrens[i].categoryname+'</a></li>';
              }
              setTimeout(function () {
                if(!jQuery.isEmptyObject(result.parent)){
                  htmlimg = '<div class="item active"> <img src="';
                  htmlimg += "assets/img/"+result.parent.imgurl+'" alt="" style="width:100%;" class="img_acr"></div>';
                  jQuery('.carousel-inner').html(htmlimg);
                }
                jQuery($this).next().addClass('shown');
                jQuery($this).next().html(html);
                jQuery(".loader").css('display', 'none');
              }, 2500);
            }
          }
        });
      }
      else{
        jQuery(this).next().css('display', 'none');
        jQuery(this).next().removeClass('shown');
      }
    });
    if($('form').length > 0){
      //jQuery('form').validate();
    }
  });
