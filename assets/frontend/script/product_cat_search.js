
var page = 1;
var alldone=0;
var scroll; 
var processing;

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}
var category = $.urlParam('search');

$(document).ready(function(){
	/* -----start------ */  
	var product_order = 'default';
	
	$('#product_price_lowest').on('click', function(){
	  product_order = 'lowest';
	   $.ajax({
		url:base_url+'webshop/allphighlowsearch',
		type:"POST",
		cache:false,
		data:{
		  order:product_order,
		  slug:category
		},
		success: function (res) {
		  var res = JSON.parse(res);
		  if(res.html != ''){
			$('#product_row').html(res.html);
			page = 1;
			alldone=0;
		  }
		},
		error: function (jqXHR, exception) {
		  
		},

	  })
	});

	$('#product_price_highest').on('click', function(){
	  product_order = 'highest';
	   $.ajax({
		url:base_url+'webshop/allphighlowsearch',
		type:"POST",
		cache:false,
		data:{
		  order:product_order,
		  slug:category
		},
		success: function (res) {
		  var res = JSON.parse(res);
		  if(res.html != ''){
			$('#product_row').html(res.html);
			page = 1;
			alldone=0;
		  }
		},
		error: function (jqXHR, exception) {
		  
		},

	  })
	});
	
	$('#clear_search_terms').on('click', function(){
	  product_order = 'default';
	   $.ajax({
		url:base_url+'webshop/allpsearch',
		type:"POST",
		cache:false,
		data:{
		  order:product_order,
		  slug:category
		},
		success: function (res) {
		  var res = JSON.parse(res);
		  if(res.html != ''){
			$('#product_row').html(res.html);
			page = 1;
			alldone=0;
		  }
		},
		error: function (jqXHR, exception) {
		  
		},

	  })
	});

	$(window).on("scroll", function() {	
	  if (processing)
		return false;
		if(alldone==0){
			if ($(window).scrollTop() > $('body').height() / 2){
				processing = true;
				page++;	
				$.ajax({
					url:base_url+'webshop/allpsearch/?page='+page,
					type:"POST",
					cache:false,
					data:{
						order:product_order,
						slug:category,
					},
					beforeSend: function()
					{
						$('.ajax-load').show();
					} 
				})
				.done(function(res){
					var res = JSON.parse(res);
					if(res.html != ''){
						$('#product_row').append(res.html);
						$('.ajax-load').hide();
						var className = $('#display_2').attr('class');
						if(className=='dis_sel'){
							$('.display_2_4').attr('class', 'col-md-6 col-sm-6 padd-sec display_2_4');
						}
						else{
							$('.display_2_4').attr('class', 'col-md-3 col-sm-3 padd-sec display_2_4');
						}
					}
					else{
						$('.ajax-load').hide();
						alldone=1;
					}
					processing = false;
				})
			}
		}
	});

});



$('#display_2').on('click', function(){
  $('#display_4').attr('class', 'dis_lnk');
  $('#display_2').attr('class', 'dis_sel');
  $('.display_2_4').attr('class', 'col-md-6 col-sm-6 padd-sec display_2_4');
});
$('#display_4').on('click', function(){
  $('#display_2').attr('class', 'dis_lnk');
  $('#display_4').attr('class', 'dis_sel');
  $('.display_2_4').attr('class', 'col-md-3 col-sm-3 padd-sec display_2_4');
});






/* -----end------ */      
 